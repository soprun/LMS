<?php

namespace App\Service;

use App\Entity\PointsShop\Product;
use App\Entity\User;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Psr\Log\LoggerInterface;

class LikeAdminHelper
{

    private $likeItAdminToken;
    private $logger;
    private $client;

    public function __construct(
        string $_likeItAdminToken,
        LoggerInterface $logger
    ) {
        $this->likeItAdminToken = $_likeItAdminToken;
        $this->logger = $logger;

        $this->client = new Client([
            'base_uri' => 'https://admin.likecentre.ru'
        ]);
    }

    /**
     * Генерация промокода для продукта
     * @param Product $product
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function generatePromocode(Product $product, User $user): array
    {
        $response = null;
        $errors = [];

        try {
            $request = $this->client->request('POST', 'admincontroller', [
                'headers'  => [
                    'authme' => $this->likeItAdminToken
                ],
                'json' => [
                    'table' => 'promocodes',
                    'code' => 'promocodes',
                    'format' => 'json',
                    'promocode' => [
                        'name' => "LMS points shop. LmsUserId " . $user->getId(),
                        'product' => [
                            'id' => $product->getLikeProductId(),
                        ],
                        'package' => [
                            'id' => $product->getLikePackageId(),
                        ],
                        'hashtag' => 'lmsPointsShop',
                        'date' => [
                            'start' => null,
                            'end' => null,
                        ],
                        'reusable' => [
                            'name' => null,
                            'value' => false,
                            'amount' => "1",
                        ],
                        'value' => [
                            'type' => "percent",
                            'amount' => $product->getDiscountAmount(),
                        ]
                    ]
                ]
            ]);
            $response = json_decode($request->getBody()->getContents());
        } catch (RequestException $e) {
            $errorMessage = null;
            if (!$e->hasResponse()) {
                $errors[] = [
                    'field' => 'authServer',
                    'message' => 'Ошибка authServer'
                ];

                $this->logger->critical('LikeAdminHelper::generatePromocode exception', [
                    'uri' => (string) $e->getRequest()->getUri()
                ]);
            } else {
                $response = json_decode($e->getResponse()->getBody()->getContents());
            }
        }

        if (!is_array($response)) {
            $errors[] = [
                'unknown' => '',
                'message' => $response
            ];

            $this->logger->critical('LikeAdminHelper::generatePromocode isn array', [
                'response' => $response
            ]);
        }

        return [$response, $errors];
    }

}

<?php

namespace App\Service;

use App\Entity\Interfaces\FileInterface;
use League\Flysystem\AdapterInterface;
use League\Flysystem\FileExistsException;
use League\Flysystem\FilesystemInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploaderService
{
    /** @var FilesystemInterface */
    private $uploadFilesystem;
    /** @var string  */
    private $uploadedAssetsBaseUrl;


    public function __construct(FilesystemInterface $uploadFilesystem, string $uploadedAssetsBaseUrl)
    {
        $this->uploadFilesystem = $uploadFilesystem;
        $this->uploadedAssetsBaseUrl = $uploadedAssetsBaseUrl;
    }

    /**
     * @param  UploadedFile  $uploadedFile
     * @param  string  $directory
     * @param  bool  $public
     * @return string
     */
    public function uploadFile(UploadedFile $uploadedFile, string $directory, bool $public): string
    {
        $originalFilename = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);
        $newFilenameBody = md5($originalFilename . microtime()) . '-' . uniqid('', false);
        $newFilename = $newFilenameBody . '.' . $uploadedFile->guessExtension();
        $stream = fopen($uploadedFile->getPathname(), 'rb');

        if ($stream) {
            $this->deleteFile([$directory, $newFilename]);
            // @todo: обернуть в try catch, удаление сделать в catch
            $this->uploadFilesystem->writeStream(
                $directory . '/' . $newFilename,
                $stream,
                [
                    'visibility' => $public
                        ? AdapterInterface::VISIBILITY_PUBLIC
                        : AdapterInterface::VISIBILITY_PRIVATE,
                ]
            );
            fclose($stream);
        }

        return $newFilename;
    }

    /**
     * @param string|array $path
     * @return bool
     */
    public function deleteFile($path): bool
    {
        // @todo: обернуть в try catch и, возможно, убрать проверку has
        if ($this->uploadFilesystem->has($this->preparePath($path))) {
            return $this->uploadFilesystem->delete($this->preparePath($path));
        }
        return true;
    }

    public function prepareFile(FileInterface $file, string $directory): FileInterface
    {
        return $file->setPublicPath(
            $this->getPublicPath([$directory, $file->getFilename()])
        );
    }

    /**
     * @param string|array $path
     * @return string
     */
    public function getPublicPath($path): string
    {
        return $this->uploadedAssetsBaseUrl . '/' . $this->preparePath($path);
    }

    /**
     * @param string|array $path
     * @return string
     */
    protected function preparePath($path): string
    {
        if (is_array($path)) {
            $path = implode('/', $path);
        }
        return $path;
    }
}

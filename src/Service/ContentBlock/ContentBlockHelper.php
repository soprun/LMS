<?php

namespace App\Service\ContentBlock;

use App\Entity\BaseBlock\BaseBlock1;
use App\Entity\BaseBlock\BaseBlock2;
use App\Entity\BaseBlock\BaseBlock3;
use App\Entity\BaseBlock\BaseBlock4;
use App\Entity\BaseBlock\BaseBlock5;
use App\Entity\BaseBlock\BaseBlock6;
use App\Entity\BaseBlock\BaseBlock7;
use App\Entity\BaseBlock\BaseBlockNps;
use App\Entity\BaseBlock\BaseFiles;
use App\Entity\BaseBlock\BaseBlockTaskWithCheck;
use App\Entity\Task\AnswerTaskType5;
use App\Entity\Task\TaskScript;
use App\Entity\Task\TaskSet;
use App\Entity\Task\TaskTest;
use App\Entity\Task\TaskTestQuestion;
use App\Entity\Task\TaskTestQuestionAnswer;
use App\Entity\Task\TaskTestQuestionUserAnswer;
use App\Entity\Task\TaskTestResult;
use App\Entity\Task\TaskType1;
use App\Entity\Task\TaskType5;
use App\Entity\User;
use App\Service\FormHelper;
use App\Service\Course\FileHelper;
use App\Service\Course\TaskHelper;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @deprecated
 * Кодим тут App\Service\LessonContentService
 */
class ContentBlockHelper
{
    private static $BLOCKS = [
        1 => [ // Контентные блоки
            1 => ['header'], // Блок с подзаголовком
            2 => ['header', 'text'], // Блок с текстом
            3 => ['videoUrl', 'img', 'typeId'], // Блок с видео
            4 => ['img'], // Блок с фото
            5 => ['audioUrl'], // Блок с аудио
            6 => ['header', 'files'], // Блок с файлом
            7 => ['buttonText', 'buttonUrl', 'isBlank', 'openInAppDefaultBrowser'], // Блок с кнопкой
        ],
        2 => [ // Задания с проверкой
            1 => [
                'systemName',
                'question',
                'description',
                'dateOfStart',
                'ddl',
                'isAutoCheck',
                'typeOfTask',
                'minPoints',
                'maxPoints',
                'pointsBeforeDdl',
                'pointsAfterDdl',
            ],
        ],
        3 => [ // Сбор NPS
            1 => ['systemName', 'question', 'additionalQuestion', 'img'],
        ],
        4 => [ // Тест
            // делай, если знаешь зачем
            // тут не знаю зачем, не делаю
            // 2 => ['preview', 'results', 'questions', 'points'] // Игра для франчей
        ],
        5 => [], // Задания с вводом правильного ответа из списка
        7 => [ // Кастомные штуки, которые хранятся чисто на фронте
            1 => [] // калькулятор unit factor
        ],
    ];
    private $em;
    private $formHelper;
    private $fileHelper;
    private $taskHelper;

    public function __construct(
        EntityManagerInterface $em,
        FormHelper $formHelper,
        FileHelper $fileHelper,
        TaskHelper $taskHelper
    ) {
        $this->em = $em;
        $this->formHelper = $formHelper;
        $this->fileHelper = $fileHelper;
        $this->taskHelper = $taskHelper;
    }

    /**
     * @param  int  $type
     * @param  int  $variety
     * @param  int  $blockId
     * @param  User|null  $user  – Используем, если нужно получить ответы на задания
     * @return array
     */
    public function parseBlock(int $type, int $variety, int $blockId, ?User $user = null)
    {
        $block = $this->getBlockById($type, $variety, $blockId);

        $data = [
            'type' => $type,
            'variety' => $variety,
            'id' => $blockId,
        ];

        switch ($type) {
            case 1:
                /** @var string $field */
                foreach (static::$BLOCKS[$type][$variety] as $field) {
                    switch ($field) {
                        // Контентные блоки
                        case 'header':
                            $data['content'][$field] = $block->getHeader();
                            break;
                        case 'text':
                            $data['content'][$field] = $block->getText();
                            break;
                        case 'videoUrl':
                            $data['content'][$field] = $block->getVideoUrl();
                            break;
                        case 'typeId':
                            $data['content'][$field] = $block->getTypeId();
                            break;
                        case 'img':
                            $img = new \stdClass();

                            /** @var BaseFiles $imgEntity */
                            if ($imgEntity = $block->getImgFile()) {
                                $img = [
                                    'id' => $imgEntity->getId(),
                                    'url' => $this->fileHelper->getFileUrl($imgEntity),
                                    'fileName' => $imgEntity->getFileName(),
                                ];
                            }
                            $data['content'][$field] = $img;
                            break;
                        case 'audioUrl':
                            $data['content'][$field] = $block->getAudioUrl();
                            break;
                        case 'files':
                            $data['content'][$field] = [];
                            $files = $block->getFiles();

                            /** @var BaseFiles $file */
                            foreach ($files as $file) {
                                $data['content'][$field][] = [
                                    'id' => $file->getId(),
                                    'fileName' => $file->getFileName(),
                                    'url' => $this->fileHelper->getFileUrl($file),
                                    'mimeType' => $file->getMimeType(),
                                ];
                            }
                            break;
                        case 'buttonText':
                            $data['content'][$field] = $block->getButtonText();
                            break;
                        case 'buttonUrl':
                            $data['content'][$field] = $block->getButtonUrl();
                            break;
                        case 'isBlank':
                            $data['content'][$field] = $block->getIsBlank();
                            break;
                        case 'openInAppDefaultBrowser':
                            $data['content'][$field] = $block->getOpenInAppDefaultBrowser();
                            break;
                    }
                }
                break;
            case 2:
                $data['content'] = $this->taskHelper->getTaskArray($block, true);
                break;

            case 3:
                /** @var string $field */
                foreach (static::$BLOCKS[$type][$variety] as $field) {
                    switch ($field) {
                        case 'systemName':
                            $data['content'][$field] = $block->getSystemName();
                            break;
                        case 'question':
                            $data['content'][$field] = $block->getQuestion();
                            break;
                        case 'additionalQuestion':
                            $data['content'][$field] = $block->getAdditionalQuestion();
                            break;
                        case 'img':
                            $data['content'][$field] = $block->getImg();
                            break;
                    }
                }
                break;

            case 4:
                /** @var TaskTest $block */

                /* preview */
                $data['content']['preview']['title'] = $block->getTitle();
                $data['content']['preview']['description'] = $block->getDescription() ?? '';

                $img = new \stdClass();
                if ($imgEntity = $block->getImgFile()) {
                    $img = [
                        'id' => $imgEntity->getId(),
                        'url' => $this->fileHelper->getFileUrl($imgEntity),
                        'fileName' => $imgEntity->getFileName(),
                    ];
                }
                $data['content']['preview']['img'] = $img;

                /* results */
                foreach ($block->getTaskTestResults() as $testResult) {
                    if ($testResult->isDeleted()) {
                        continue;
                    }
                    $img = new \stdClass();
                    if ($imgEntity = $testResult->getImgFile()) {
                        $img = [
                            'id' => $imgEntity->getId(),
                            'url' => $this->fileHelper->getFileUrl($imgEntity),
                            'fileName' => $imgEntity->getFileName(),
                        ];
                    }
                    $data['content']['results'][] = [
                        'id' => $testResult->getId(),
                        'pointsFrom' => $testResult->getPointsFrom(),
                        'pointsTo' => $testResult->getPointsTo(),
                        'text' => $testResult->getText(),
                        'img' => $img,
                        'isDeleted' => $testResult->isDeleted(),
                    ];
                }

                /* questions */
                // берем ответы пользователя на тест: questionId => answerId
                $questionUserAnswers = [];
                if ($user) {
                    $questionUserAnswers = $this->em->getRepository(TaskTestQuestionUserAnswer::class)
                        ->getTestQuestionUserAnswers($user, $block);
                }

                $taskTestQuestions = $this->em->getRepository(TaskTestQuestion::class)->findByTaskTest($block);
                foreach ($taskTestQuestions as $testQuestion) {
                    $testQuestionId = $testQuestion->getId();
                    $userAnswerIds = array_key_exists($testQuestionId, $questionUserAnswers)
                        ? $questionUserAnswers[$testQuestionId]
                        : [];

                    $questionArray = [
                        'id' => $testQuestionId,
                        'text' => $testQuestion->getText(),
                        'isMultipleAnswers' => $testQuestion->getIsMultipleAnswers(),
                        'isDeleted' => $testQuestion->isDeleted(),
                        'position' => $testQuestion->getPosition(),
                        'img' => [],
                        'answers' => [],
                        'userAnswerIds' => $userAnswerIds,
                    ];

                    if ($imgEntity = $testQuestion->getImgFile()) {
                        $questionArray['img'] = [
                            'id' => $imgEntity->getId(),
                            'url' => $this->fileHelper->getFileUrl($imgEntity),
                            'fileName' => $imgEntity->getFileName(),
                        ];
                    }

                    foreach ($testQuestion->getTaskTestQuestionAnswers() as $testQuestionAnswer) {
                        if ($testQuestionAnswer->isDeleted()) {
                            continue;
                        }
                        $questionArray['answers'][] = [
                            'id' => $testQuestionAnswer->getId(),
                            'text' => $testQuestionAnswer->getText(),
                            'isCorrect' => $testQuestionAnswer->getIsCorrect(),
                            'isDeleted' => $testQuestionAnswer->isDeleted(),
                            'position' => $testQuestionAnswer->getPosition(),
                            'points' => $testQuestionAnswer->getPoints(),
                        ];
                    }
                    $data['content']['questions'][] = $questionArray;
                }

                /* points */
                $points = '';
                // считаем баллы, если ответил на все вопросы
                if (count($questionUserAnswers) >= count($taskTestQuestions)) {
                    $points = $this->em->getRepository(TaskTestQuestionUserAnswer::class)
                        ->getSumOfTestUserAnswers($user, $block);
                }

                $data['content']['points'] = $points;

                break;

            case 5:
                /** @var TaskType5 $block */

                $ddl = '';
                if ($block->getDdl()) {
                    $ddl = (new \DateTime(
                        $block->getDdl()->format("Y-m-d H:i:s"),
                        new \DateTimeZone('Europe/Moscow')
                    ))->format("Y-m-d H:i:s");
                }

                $data['content'] = [
                    'title' => $block->getTitle(),
                    'description' => $block->getDescription() ?? '',
                    'ddl' => $ddl,
                    'correctAnswers' => $block->getCorrectAnswers(),
                    'pointsBeforeDdl' => $block->getPointsBeforeDdl() ?? '',
                    'pointsAfterDdl' => $block->getPointsAfterDdl() ?? '',
                    'additionalCorrectAnswers' => $block->getAdditionalCorrectAnswers(),
                    'addCorAnsPointsBeforeDdl' => $block->getAddCorAnsPointsBeforeDdl() ?? '',
                    'addCorAnsPointsAfterDdl' => $block->getAddCorAnsPointsAfterDdl() ?? '',
                    'answer' => new \stdClass(),
                ];

                if ($user) {
                    $answerTaskType5 = $this->em->getRepository(AnswerTaskType5::class)->findOneBy(
                        [
                            'user' => $user,
                            'task' => $block,
                        ]
                    );
                    if ($answerTaskType5) {
                        /** @var AnswerTaskType5 $answerTaskType5 */
                        $data['content']['answer'] = [
                            'answerText' => $answerTaskType5->getAnswerText(),
                            'validatingText' => $answerTaskType5->getValidatingText(),
                            'isCorrect' => $answerTaskType5->getIsCorrect(),
                        ];
                    }
                }
                break;
        }

        return $data;
    }

    public function getBlockById(int $type, int $variety, int $blockId)
    {
        // Определяем тип блока
        // 1 - контент
        // 2 - задания с правильным ответом,
        // 3 - задания с выбором траектории)
        // 4 - тесты
        // 5 - задания с вводом правильного ответа из списка

        $repositoryName = null;
        switch ($type) {
            case 1:
                $repositoryName = "App\Entity\BaseBlock\BaseBlock" . $variety;
                break;
            case 2:
                $repositoryName = BaseBlockTaskWithCheck::class;
                break;
            case 3:
                $repositoryName = BaseBlockNps::class;
                break;
            case 4:
                $repositoryName = TaskTest::class;
                break;
            case 5:
                $repositoryName = TaskType5::class;
                break;

            default:
                return null;
                break;
        }

        return $this->em->getRepository($repositoryName)->find($blockId);
    }

    public function newBlock(array $blockArray)
    {
        $blockEntity = null;

        // Создаем сущность блока в зависимости от типа и вариации
        switch ($blockArray['type']) {
            case 1:
                switch ($blockArray['variety']) {
                    case 1:
                        $blockEntity = new BaseBlock1();
                        break;
                    case 2:
                        $blockEntity = new BaseBlock2();
                        break;
                    case 3:
                        $blockEntity = new BaseBlock3();
                        break;
                    case 4:
                        $blockEntity = new BaseBlock4();
                        break;
                    case 5:
                        $blockEntity = new BaseBlock5();
                        break;
                    case 6:
                        $blockEntity = new BaseBlock6();
                        break;
                    case 7:
                        $blockEntity = new BaseBlock7();
                        break;
                }
                break;
            case 2:
                switch ($blockArray['variety']) {
                    case 1:
                        $blockEntity = new BaseBlockTaskWithCheck();
                        break;
                }
                break;
            case 3:
                switch ($blockArray['variety']) {
                    case 1:
                        $blockEntity = new BaseBlockNps();
                        break;
                }
                break;
            case 4:
                switch ($blockArray['variety']) {
                    case 2:
                        $blockEntity = new TaskTest();
                        break;
                }
                break;
            case 5:
                switch ($blockArray['variety']) {
                    case 1: // без numbers
                    case 2: // с numbers
                        $blockEntity = new TaskType5();
                        break;
                }
                break;
        }

        $blockEntity = $this->setBlockFromArray($blockEntity, $blockArray);

        $this->em->persist($blockEntity);
        $this->em->flush();

        return $blockEntity->getId();
    }

    public function editBlock(array $blockArray)
    {
        $blockEntity = $this->getBlockById($blockArray['type'], $blockArray['variety'], $blockArray['id']);

        $blockEntity = $this->setBlockFromArray($blockEntity, $blockArray);

        $this->em->persist($blockEntity);
        $this->em->flush();

        return $blockEntity->getId();
    }

    public function setBlockFromArray($blockEntity, array $blockArray)
    {
        $content = $blockArray['content'];

        switch ($blockArray['type']) {
            case 1:
                switch ($blockArray['variety']) {
                    case 1:
                        $blockEntity->setHeader(
                            $this->formHelper->toRealStr($content['header'], 1000)
                        );
                        break;
                    case 2:
                        $blockEntity->setHeader(
                            $this->formHelper->toRealStr($content['header'], 1000)
                        );
                        $blockEntity->setText(
                            $this->formHelper->toRealStr($content['text'], 4000)
                        );
                        break;
                    case 3:
                        $blockEntity->setVideoUrl(
                            $this->formHelper->toStrWithoutGaps($content['videoUrl'])
                        );
                        $blockEntity->setTypeId(
                            $this->formHelper->toInt($content['typeId'])
                        );
                        if (!empty($content['img'])) {
                            /** @var BaseFiles $img */
                            $img = $this->em
                                ->getRepository(BaseFiles::class)
                                ->findOneBy(['id' => (int)$content['img']['id']]);

                            $blockEntity->setImgFile($img);
                        } else {
                            $blockEntity->setImgFile(null);
                        }
                        break;
                    case 4:
                        /** @var BaseFiles $img */
                        $img = $this->em
                            ->getRepository(BaseFiles::class)
                            ->findOneBy(['id' => (int)$content['img']['id']]);

                        $blockEntity->setImgFile($img);
                        break;
                    case 5:
                        $blockEntity->setAudioUrl(
                            $this->formHelper->toStrWithoutGaps($content['audioUrl'])
                        );
                        break;
                    case 6:
                        foreach ($content['files'] as $file) {
                            $fileEntity = $this->em
                                ->getRepository(BaseFiles::class)->findOneBy(['id' => $file['id']]);
                            if ($fileEntity) {
                                $blockEntity->addFile($fileEntity);
                            }
                        }
                        $blockEntity->setHeader(
                            $this->formHelper->toRealStr($content['header'])
                        );
                        break;
                    case 7:
                        /** @var BaseBlock7 $blockEntity */
                        $blockEntity->setButtonText(
                            $this->formHelper->toRealStr($content['buttonText'])
                        );
                        $blockEntity->setButtonUrl(
                            $this->formHelper->toStrWithoutGaps($content['buttonUrl'])
                        );
                        $blockEntity->setIsBlank($content['isBlank'] ? true : false);
                        // костыль, пока не обновили фронт
                        if (!array_key_exists('openInAppDefaultBrowser', $content)) {
                            $content['openInAppDefaultBrowser'] = false;
                        }
                        $blockEntity->setOpenInAppDefaultBrowser($content['openInAppDefaultBrowser'] ? true : false);
                        break;
                }
                break;
            case 2:
                switch ($blockArray['variety']) {
                    case 1:
                        $blockEntity->setSystemName(
                            $this->formHelper->toRealStr($content['taskBlock']['systemName'])
                        );
                        if ($content['taskBlock']['ddl'] == "") {
                            $blockEntity->setDdl(null);
                        } else {
                            $blockEntity->setDdl(new \DateTime($content['taskBlock']['ddl']));
                        }

                        $this->em->persist($blockEntity);

                        foreach ($content['questionArray'] as $question) {
                            $question = $question['question'];
                            switch ($question['variety']) {
                                case 1:
                                    $taskId = key_exists('id', $question) ? $question['id'] ?: null : null;

                                    if ($taskId) {
                                        $task = $this->em->getRepository(TaskType1::class)->findOneBy(
                                            ['id' => $question['id']]
                                        );
                                    } else {
                                        $task = new TaskType1();
                                    }

                                    $task->setQuestion(
                                        $this->formHelper->toRealStr($question['question'], 2000)
                                    );
                                    $task->setTextBeforeDdl(
                                        $this->formHelper->toRealStr($question['textBeforeDdl'], 500)
                                    );
                                    $task->setTextAfterDdl(
                                        $this->formHelper->toRealStr($question['textAfterDdl'], 500)
                                    );
                                    $task->setTypeOfTask(
                                        $this->formHelper->toRealStr($question['typeOfTask'])
                                    );
                                    $task->setMinPoints(
                                        $this->formHelper->toInt($question['minPoints'], 'minPoints', '0', '100')
                                    );
                                    $task->setMaxPoints(
                                        $this->formHelper->toInt($question['maxPoints'], 'maxPoints', '0', '100')
                                    );
                                    $task->setPointsBeforeDdl(
                                        $this->formHelper->toInt(
                                            $question['pointsBeforeDdl'],
                                            'pointsBeforeDdl',
                                            '0',
                                            '100'
                                        )
                                    );
                                    $task->setPointsAfterDdl(
                                        $this->formHelper->toInt(
                                            $question['pointsAfterDdl'],
                                            'pointsAfterDdl',
                                            '0',
                                            '100'
                                        )
                                    );
                                    $task->setIsAutoCheck($question['isAutoCheck'] ? true : false);
                                    $task->setIsLinkForbidden($question['isLinkForbidden'] ? true : false);
                                    $task->setIsFileAnswerAllow($question['isFileAnswerAllow'] ? true : false);
                                    $task->setReviewAfterDdl($question['isReviewAfterDdl'] ? true : false);

                                    $this->em->persist($task);
                                    $this->em->flush();
                            }

                            if (!$taskId) {
                                $set = new TaskSet();
                                $set->setVariety($question['variety']);
                                $set->setBlockId($task->getId());
                                $set->setBlockTaskWithCheck($blockEntity);

                                $lastPosition = $this->em->getRepository(TaskSet::class)
                                    ->findByVarietyAndBlockIdLastPosition($question['variety'], $task->getId());

                                $set->setPositionInTask($lastPosition + 1);
                                $this->em->persist($set);
                            }
                        }
                        $this->em->flush();

                        break;
                }
                break;
            case 3:
                switch ($blockArray['variety']) {
                    case 1:
                        $blockEntity->setSystemName(
                            $this->formHelper->toRealStr($content['systemName'])
                        );
                        $blockEntity->setQuestion(
                            $this->formHelper->toRealStr($content['question'])
                        );
                        $blockEntity->setAdditionalQuestion(
                            $this->formHelper->toRealStr($content['additionalQuestion'])
                        );
                        $blockEntity->setImg(
                            $this->formHelper->toStrWithoutGaps($content['img'])
                        );
                        break;
                }
                break;
            case 4: // тесты
                /** @var TaskTest $blockEntity */
                switch ($blockArray['variety']) {
                    case 2: // игра для франчей
                        $blockEntity->setTitle(
                            $this->formHelper->toRealStr($content['preview']['title'])
                        );
                        $blockEntity->setDescription(
                            $this->formHelper->toRealStr($content['preview']['description'], 2000)
                        );
                        if (!empty($content['preview']['img'])) {
                            if (
                                $imgFile = $this->em->getRepository(BaseFiles::class)
                                    ->find($content['preview']['img']['id'])
                            ) {
                                /** @var BaseFiles $imgFile */
                                $blockEntity->setImgFile($imgFile);
                            } else {
                                $this->formHelper->addError('imgFile', 'Файл с таким id не найден');
                            }
                        }

                        // результаты теста
                        foreach ($content['results'] as $testResultData) {
                            $testResult = (array_key_exists('id', $testResultData) && !empty($testResultData['id']))
                                ? $this->em->getRepository(TaskTestResult::class)->find($testResultData['id'])
                                : new TaskTestResult();
                            $testResult->setTest($blockEntity);
                            $testResult->setText(
                                $this->formHelper->toRealStr($testResultData['text'], 2000)
                            );
                            $testResult->setPointsFrom(
                                $this->formHelper->toInt($testResultData['pointsFrom'])
                            );
                            $testResult->setPointsTo(
                                $this->formHelper->toInt($testResultData['pointsTo'])
                            );
                            if (!empty($testResultData['img'])) {
                                if (
                                    $imgFile = $this->em->getRepository(BaseFiles::class)
                                        ->find($testResultData['img']['id'])
                                ) {
                                    /** @var BaseFiles $imgFile */
                                    $testResult->setImgFile($imgFile);
                                } else {
                                    $this->formHelper->addError('imgFile', 'Файл с таким id не найден');
                                }
                            }
                            $testResult->setDeleted($testResultData['isDeleted']);
                            $this->em->persist($testResult);
                        }

                        // вопросы теста
                        foreach ($content['questions'] as $questionData) {
                            $question = (array_key_exists('id', $questionData) && !empty($questionData['id']))
                                ? $this->em->getRepository(TaskTestQuestion::class)->find($questionData['id'])
                                : new TaskTestQuestion();
                            $question->setTest($blockEntity);
                            $question->setText(
                                $this->formHelper->toRealStr($questionData['text'], 2000)
                            );
                            $question->setIsMultipleAnswers($questionData['isMultipleAnswers']);
                            $question->setPosition(
                                $this->formHelper->toInt($questionData['position'])
                            );
                            $question->setDeleted($questionData['isDeleted']);
                            if (!empty($questionData['img'])) {
                                if (
                                    $imgFile = $this->em->getRepository(BaseFiles::class)
                                        ->find($questionData['img']['id'])
                                ) {
                                    /** @var BaseFiles $imgFile */
                                    $question->setImgFile($imgFile);
                                } else {
                                    $this->formHelper->addError('imgFile', 'Файл с таким id не найден');
                                }
                            }
                            $this->em->persist($question);

                            // ответы на вопрос
                            foreach ($questionData['answers'] as $answerData) {
                                $answer = (array_key_exists('id', $answerData) && !empty($answerData['id']))
                                    ? $this->em->getRepository(TaskTestQuestionAnswer::class)->find($answerData['id'])
                                    : new TaskTestQuestionAnswer();
                                $answer->setQuestion($question);
                                $answer->setText(
                                    $this->formHelper->toRealStr($answerData['text'])
                                );
                                $answer->setIsCorrect($answerData['isCorrect']);
                                $answer->setPosition(
                                    $this->formHelper->toInt($answerData['position'])
                                );
                                $answer->setDeleted($answerData['isDeleted']);

                                $answer->setPoints(
                                    $this->formHelper->toInt($answerData['points'])
                                );
                                $this->em->persist($answer);
                            }
                        }
                        break;
                }
                break;

            case 5: // задания с вводом правильного ответа из списка
                /** @var TaskType5 $blockEntity */
                switch ($blockArray['variety']) {
                    case 1:
                    case 2:
                        $blockEntity->setTitle(
                            $this->formHelper->toRealStr($content['title'])
                        );
                        $blockEntity->setDescription(
                            $this->formHelper->toRealStr($content['description'], 2000)
                        );
                        if (!empty($content['ddl'])) {
                            $blockEntity->setDdl(new \DateTime($content['ddl']));
                        }

                        $blockEntity->setCorrectAnswers(
                            mb_strtolower(
                                $this->formHelper->toRealStr($content['correctAnswers'], 1000)
                            )
                        );
                        $blockEntity->setPointsBeforeDdl(
                            $this->formHelper->toInt($content['pointsBeforeDdl'], 'pointsBeforeDdl', '0')
                        );
                        $blockEntity->setPointsAfterDdl(
                            $this->formHelper->toInt($content['pointsAfterDdl'], 'pointsBeforeDdl', '0')
                        );

                        $blockEntity->setAdditionalCorrectAnswers(
                            mb_strtolower(
                                $this->formHelper->toRealStr($content['additionalCorrectAnswers'], 1000)
                            )
                        );
                        $blockEntity->setAddCorAnsPointsBeforeDdl(
                            $this->formHelper->toInt(
                                $content['addCorAnsPointsBeforeDdl'],
                                'addCorAnsPointsBeforeDdl',
                                '0'
                            )
                        );
                        $blockEntity->setAddCorAnsPointsAfterDdl(
                            $this->formHelper->toInt(
                                $content['addCorAnsPointsAfterDdl'],
                                'addCorAnsPointsAfterDdl',
                                '0'
                            )
                        );
                        break;
                }
                break;
        }

        return $blockEntity;
    }

    public function setBlockFromArrayOnDuplicate($blockEntity, array $blockArray)
    {
        $content = $blockArray['content'];

        switch ($blockArray['type']) {
            case 1:
                switch ($blockArray['variety']) {
                    case 1:
                        $blockEntity->setHeader(
                            $this->formHelper->toRealStr($content['header'], 1000)
                        );
                        break;
                    case 2:
                        $blockEntity->setHeader(
                            $this->formHelper->toRealStr($content['header'], 1000)
                        );
                        $blockEntity->setText(
                            $this->formHelper->toRealStr($content['text'], 4000)
                        );
                        break;
                    case 3:
                        $blockEntity->setVideoUrl(
                            $this->formHelper->toStrWithoutGaps($content['videoUrl'])
                        );
                        $blockEntity->setTypeId(
                            $this->formHelper->toInt($content['typeId'])
                        );
                        if (!empty($content['img']) && array_key_exists('id', $content['img'])) {
                            if ($idImg = (int)$content['img']['id']) {
                                /** @var BaseFiles $img */
                                $img = $this->em
                                    ->getRepository(BaseFiles::class)
                                    ->find($idImg);

                                $blockEntity->setImgFile($this->duplicateFileEntity($img));
                            }
                        } else {
                            $blockEntity->setImgFile(null);
                        }
                        break;
                    case 4:
                        /** @var BaseFiles $img */
                        $img = $this->em
                            ->getRepository(BaseFiles::class)
                            ->findOneBy(['id' => (int)$content['img']['id']]);

                        $blockEntity->setImgFile($this->duplicateFileEntity($img));
                        break;
                    case 5:
                        $blockEntity->setAudioUrl(
                            $this->formHelper->toStrWithoutGaps($content['audioUrl'])
                        );
                        break;
                    case 6:
                        foreach ($content['files'] as $file) {
                            $fileEntity = $this->em
                                ->getRepository(BaseFiles::class)->findOneBy(['id' => $file['id']]);
                            if ($fileEntity) {
                                $blockEntity->addFile($this->duplicateFileEntity($fileEntity));
                            }
                        }
                        $blockEntity->setHeader(
                            $this->formHelper->toRealStr($content['header'])
                        );
                        break;
                    case 7:
                        $blockEntity->setButtonText(
                            $this->formHelper->toRealStr($content['buttonText'])
                        );
                        $blockEntity->setButtonUrl(
                            $this->formHelper->toStrWithoutGaps($content['buttonUrl'])
                        );
                        $blockEntity->setIsBlank($content['isBlank'] ? true : false);
                        // костыль, пока не обновили фронт
                        if (!array_key_exists('openInAppDefaultBrowser', $content)) {
                            $content['openInAppDefaultBrowser'] = false;
                        }
                        $blockEntity->setOpenInAppDefaultBrowser($content['openInAppDefaultBrowser'] ? true : false);
                        break;
                }
                break;
            case 2:
                switch ($blockArray['variety']) {
                    case 1:
                        $blockEntity->setSystemName(
                            $this->formHelper->toRealStr($content['taskBlock']['systemName'])
                        );
                        $blockEntity->setDdl(new \DateTime($content['taskBlock']['ddl']));

                        $this->em->persist($blockEntity);

                        foreach ($content['questionArray'] as $question) {
                            $question = $question['question'];
                            switch ($question['variety']) {
                                case 1:
                                    // Создаем новое задание
                                    $task = new TaskType1();

                                    $task->setQuestion(
                                        $this->formHelper->toRealStr($question['question'], 2000)
                                    );
                                    $task->setTypeOfTask(
                                        $this->formHelper->toRealStr($question['typeOfTask'])
                                    );
                                    $task->setMinPoints(
                                        $this->formHelper->toInt($question['minPoints'], 'minPoints', '0', '100')
                                    );
                                    $task->setMaxPoints(
                                        $this->formHelper->toInt($question['maxPoints'], 'maxPoints', '0', '100')
                                    );
                                    $task->setPointsBeforeDdl(
                                        $this->formHelper->toInt(
                                            $question['pointsBeforeDdl'],
                                            'pointsBeforeDdl',
                                            '0',
                                            '100'
                                        )
                                    );
                                    $task->setPointsAfterDdl(
                                        $this->formHelper->toInt(
                                            $question['pointsAfterDdl'],
                                            'pointsAfterDdl',
                                            '0',
                                            '100'
                                        )
                                    );
                                    $task->setIsAutoCheck($question['isAutoCheck'] ? true : false);
                                    $task->setIsLinkForbidden($question['isLinkForbidden'] ? true : false);
                                    $task->setIsFileAnswerAllow($question['isFileAnswerAllow'] ? true : false);
                                    $task->setReviewAfterDdl($question['isReviewAfterDdl'] ? true : false);

                                    $this->em->persist($task);
                                    $this->em->flush();

                                    /** @var TaskType1 $oldTask */
                                    $oldTask = $this->em->getRepository(TaskType1::class)
                                        ->findOneBy(['id' => $question['id']]);

                                    $scripts = $this->em->getRepository(TaskScript::class)
                                        ->findBy(['taskId' => $oldTask->getId()]);

                                    /** @var TaskScript $script */
                                    foreach ($scripts as $script) {
                                        $newScript = new TaskScript();

                                        $newScript->setName($script->getName());
                                        $newScript->setText($script->getText());

                                        $newScript->setPointsFrom($script->getPointsFrom());
                                        $newScript->setPointsTo($script->getPointsTo());

                                        $newScript->setIsAfterDdl($script->getIsAfterDdl());

                                        $newScript->setTaskType($script->getTaskType());
                                        $newScript->setTaskId($task->getId());
                                        $this->em->persist($newScript);
                                    }

                                    break;
                            }

                            $set = new TaskSet();
                            $set->setVariety($question['variety']);
                            $set->setBlockId($task->getId());
                            $set->setBlockTaskWithCheck($blockEntity);

                            $lastPosition = $this->em->getRepository(TaskSet::class)
                                ->findByVarietyAndBlockIdLastPosition($question['variety'], $task->getId());

                            $set->setPositionInTask($lastPosition + 1);
                            $this->em->persist($set);
                        }
                        $this->em->flush();

                        break;
                }
                break;
            case 3:
                switch ($blockArray['variety']) {
                    case 1:
                        $blockEntity->setSystemName(
                            $this->formHelper->toRealStr($content['systemName'])
                        );
                        $blockEntity->setQuestion(
                            $this->formHelper->toRealStr($content['question'])
                        );
                        $blockEntity->setAdditionalQuestion(
                            $this->formHelper->toRealStr($content['additionalQuestion'])
                        );
                        $blockEntity->setImg(
                            $this->formHelper->toStrWithoutGaps($content['img'])
                        );
                        break;
                }
                break;
            case 4: // тесты
                /** @var TaskTest $blockEntity */
                switch ($blockArray['variety']) {
                    case 2: // игра для франчей
                        $blockEntity->setTitle(
                            $this->formHelper->toRealStr($content['preview']['title'])
                        );
                        $blockEntity->setDescription(
                            $this->formHelper->toRealStr($content['preview']['description'], 2000)
                        );
                        if (!empty((array)$content['preview']['img'])) {
                            if (
                                $imgFile = $this->em->getRepository(BaseFiles::class)
                                    ->find($content['preview']['img']['id'])
                            ) {
                                /** @var BaseFiles $imgFile */
                                $blockEntity->setImgFile($imgFile);
                            } else {
                                $this->formHelper->addError('imgFile', 'Файл с таким id не найден');
                            }
                        }

                        // результаты теста
                        foreach ($content['results'] as $testResultData) {
                            $testResult = new TaskTestResult();
                            $testResult->setTest($blockEntity);
                            $testResult->setText(
                                $this->formHelper->toRealStr($testResultData['text'], 2000)
                            );
                            $testResult->setPointsFrom(
                                $this->formHelper->toInt($testResultData['pointsFrom'])
                            );
                            $testResult->setPointsTo(
                                $this->formHelper->toInt($testResultData['pointsTo'])
                            );
                            if (!empty((array)$testResultData['img'])) {
                                if (
                                    $imgFile = $this->em->getRepository(BaseFiles::class)
                                        ->find($testResultData['img']['id'])
                                ) {
                                    /** @var BaseFiles $imgFile */
                                    $testResult->setImgFile($imgFile);
                                } else {
                                    $this->formHelper->addError('imgFile', 'Файл с таким id не найден');
                                }
                            }
                            $testResult->setDeleted($testResultData['isDeleted']);
                            $this->em->persist($testResult);
                        }

                        // вопросы теста
                        foreach ($content['questions'] as $questionData) {
                            $question = new TaskTestQuestion();
                            $question->setTest($blockEntity);
                            $question->setText(
                                $this->formHelper->toRealStr($questionData['text'], 2000)
                            );
                            $question->setIsMultipleAnswers($questionData['isMultipleAnswers']);
                            $question->setPosition(
                                $this->formHelper->toInt($questionData['position'])
                            );
                            $question->setDeleted($questionData['isDeleted']);
                            if (!empty((array)$questionData['img'])) {
                                if (
                                    $imgFile = $this->em->getRepository(BaseFiles::class)
                                        ->find($questionData['img']['id'])
                                ) {
                                    /** @var BaseFiles $imgFile */
                                    $question->setImgFile($imgFile);
                                } else {
                                    $this->formHelper->addError('imgFile', 'Файл с таким id не найден');
                                }
                            }
                            $this->em->persist($question);

                            // ответы на вопрос
                            foreach ($questionData['answers'] as $answerData) {
                                $answer = new TaskTestQuestionAnswer();
                                $answer->setQuestion($question);
                                $answer->setText(
                                    $this->formHelper->toRealStr($answerData['text'])
                                );
                                $answer->setIsCorrect($answerData['isCorrect']);
                                $answer->setPosition(
                                    $this->formHelper->toInt($answerData['position'])
                                );
                                $answer->setDeleted($answerData['isDeleted']);

                                $answer->setPoints(
                                    $this->formHelper->toInt($answerData['points'])
                                );
                                $this->em->persist($answer);
                            }
                        }
                        break;
                }
                break;

            case 5: // задания с вводом правильного ответа из списка
                /** @var TaskType5 $blockEntity */
                switch ($blockArray['variety']) {
                    case 1:
                    case 2:
                        $blockEntity->setTitle(
                            $this->formHelper->toRealStr($content['title'])
                        );
                        $blockEntity->setDescription(
                            $this->formHelper->toRealStr($content['description'], 2000)
                        );
                        if (!empty($content['ddl'])) {
                            $blockEntity->setDdl(new \DateTime($content['ddl']));
                        }

                        $blockEntity->setCorrectAnswers(
                            mb_strtolower(
                                $this->formHelper->toRealStr($content['correctAnswers'], 1000)
                            )
                        );
                        $blockEntity->setPointsBeforeDdl(
                            $this->formHelper->toInt($content['pointsBeforeDdl'], 'pointsBeforeDdl', '0')
                        );
                        $blockEntity->setPointsAfterDdl(
                            $this->formHelper->toInt($content['pointsAfterDdl'], 'pointsBeforeDdl', '0')
                        );

                        $blockEntity->setAdditionalCorrectAnswers(
                            mb_strtolower(
                                $this->formHelper->toRealStr($content['additionalCorrectAnswers'], 1000)
                            )
                        );
                        $blockEntity->setAddCorAnsPointsBeforeDdl(
                            $this->formHelper->toInt(
                                $content['addCorAnsPointsBeforeDdl'],
                                'addCorAnsPointsBeforeDdl',
                                '0'
                            )
                        );
                        $blockEntity->setAddCorAnsPointsAfterDdl(
                            $this->formHelper->toInt(
                                $content['addCorAnsPointsAfterDdl'],
                                'addCorAnsPointsAfterDdl',
                                '0'
                            )
                        );
                        break;
                }
                break;
        }

        return $blockEntity;
    }

    public function duplicateFileEntity(BaseFiles $fileEntity)
    {
        $newFileEntity = new BaseFiles();
        $newFileEntity->setMimeType($fileEntity->getMimeType());
        $newFileEntity->setFileName($fileEntity->getFileName());
        $newFileEntity->setFilePath($fileEntity->getFilePath());
        $this->em->persist($newFileEntity);
        $this->em->flush();

        return $newFileEntity;
    }

    public function duplicateBlock(int $type, int $variety, int $blockId)
    {
        $blockArray = $this->parseBlock($type, $variety, $blockId);

        $blockEntity = null;

        // Создаем сущность блока в зависимости от типа и вариации
        switch ($blockArray['type']) {
            case 1:
                switch ($blockArray['variety']) {
                    case 1:
                        $blockEntity = new BaseBlock1();
                        break;
                    case 2:
                        $blockEntity = new BaseBlock2();
                        break;
                    case 3:
                        $blockEntity = new BaseBlock3();
                        break;
                    case 4:
                        $blockEntity = new BaseBlock4();
                        break;
                    case 5:
                        $blockEntity = new BaseBlock5();
                        break;
                    case 6:
                        $blockEntity = new BaseBlock6();
                        break;
                    case 7:
                        $blockEntity = new BaseBlock7();
                        break;
                }
                break;
            case 2:
                switch ($blockArray['variety']) {
                    case 1:
                        $blockEntity = new BaseBlockTaskWithCheck();
                        break;
                }
                break;
            case 3:
                switch ($blockArray['variety']) {
                    case 1:
                        $blockEntity = new BaseBlockNps();
                        break;
                }
                break;
            case 4:
                switch ($blockArray['variety']) {
                    case 2:
                        $blockEntity = new TaskTest();
                        break;
                }
                break;
            case 5:
                switch ($blockArray['variety']) {
                    case 1:
                    case 2:
                        $blockEntity = new TaskType5();
                        break;
                }
                break;
        }
        $blockEntity = $this->setBlockFromArrayOnDuplicate($blockEntity, $blockArray);

        $this->em->persist($blockEntity);
        $this->em->flush();

        return $blockEntity->getId();
    }

}

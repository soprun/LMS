<?php

namespace App\Service;

use App\Entity\SmartSender\SmartSenderUser;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Psr\Log\LoggerInterface;

class SmartSenderHelper
{
    public const EVENT_PRODUCT_UPDATE = 'toolbox_updateProduct';
    public const EVENT_FACULTY_UPDATE = 'toolbox_updateFaculty';
    public const EVENT_GROUP_UPDATE = 'toolbox_updateGroup';
    public const EVENT_CHANGE_COURSE = 'toolbox_changeCourse';
    public const EVENT_DROP_USER = 'toolbox_dropUser';
    public const EVENT_FIX_EMPTY_PRODUCT = 'toolbox_fixEmptyProduct'; // LMS-233

    private $logger;
    private $client;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;

        $this->client = new Client(
            [
                'base_uri' => 'https://api.smartsender.eu',
            ]
        );
    }

    public function updateContact(SmartSenderUser $smartSenderUser, array $data)
    {
        try {
            $request = $this->client->request(
                'PUT',
                '/v1/contacts/' . $smartSenderUser->getSmartSenderUserId(),
                [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $smartSenderUser->getSmartSenderBot()->getToken(),
                    ],
                    'json' => [
                        'values' => $data,
                    ],
                ]
            );
            $response = $request->getBody()->getContents();

            $this->logger->info(
                'SmartSenderHelper::updateContact response',
                [
                    'smartSenderUserId' => $smartSenderUser->getId(),
                    'response' => $response,
                ]
            );

            return json_decode($response, true);
        } catch (RequestException $e) {
            $this->logger->critical(
                'SmartSenderHelper::updateContact exception',
                [
                    'smartSenderUserId' => $smartSenderUser->getId(),
                    'code' => $e->getCode(),
                    'message' => $e->getMessage(),
                ]
            );
        }

        return null;
    }

    public function triggerEventOnContact(SmartSenderUser $smartSenderUser, string $event)
    {
        $uri = sprintf(
            "/v1/contacts/%s/fire",
            $smartSenderUser->getSmartSenderUserId()
        );

        try {
            $request = $this->client->request(
                'POST',
                $uri,
                [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $smartSenderUser->getSmartSenderBot()->getToken(),
                    ],
                    'json' => [
                        'name' => $event,
                    ],
                ]
            );

            $response = $request->getBody()->getContents();

            $this->logger->info(
                'SmartSenderHelper::updateContact response',
                [
                    'smartSenderUserId' => $smartSenderUser->getId(),
                    'response' => $response,
                ]
            );

            return json_decode($response, true);
        } catch (RequestException $e) {
            $this->logger->critical(
                'SmartSenderHelper::triggerEventOnContact exception',
                [
                    'smartSenderUserId' => $smartSenderUser->getId(),
                    'code' => $e->getCode(),
                    'message' => $e->getMessage(),
                    'event' => $event,
                ]
            );
        }

        return null;
    }

    public function getContact(SmartSenderUser $smartSenderUser)
    {
        $request = $this->client->request(
            'GET',
            '/v1/contacts/' . $smartSenderUser->getSmartSenderUserId(),
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $smartSenderUser->getSmartSenderBot()->getToken(),
                ],
            ]
        );
        $response = $request->getBody()->getContents();

        return json_decode($response, true);
    }
}

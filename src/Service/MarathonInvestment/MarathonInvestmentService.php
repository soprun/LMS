<?php

namespace App\Service\MarathonInvestment;

use App\DTO\MarathonInvestment\MarathonInvestmentMembersFiltersDTO;
use App\Repository\UserRepository;
use App\Service\AuthServerService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;

class MarathonInvestmentService
{
    /** @var UserRepository */
    private $userRepository;

    /** @var AuthServerService */
    private $authServer;

    public function __construct(UserRepository $userRepository, AuthServerService $authServer)
    {
        $this->userRepository = $userRepository;
        $this->authServer = $authServer;
    }

    public function getMembers(MarathonInvestmentMembersFiltersDTO $filtersDTO): ArrayCollection
    {
        $members = $this->userRepository->getMarathonInvestmentMembers($filtersDTO);
        $this->authServer->refillUsers($members, null, true);
        $orderBy = [];
        foreach ($filtersDTO->sorts as $field) {
            if (str_starts_with($field, '-')) {
                $sort = 'DESC';
                $field = substr($field, 1);
            } else {
                $sort = 'ASC';
            }
            if (in_array($field, ['totalAmount', 'totalPassed'])) {
                $orderBy[$field] = $sort;
            }
        }

        return $members->matching(Criteria::create()->orderBy($orderBy));
    }

    public function getMembersCount(MarathonInvestmentMembersFiltersDTO $filtersDTO): int
    {
        return $this->userRepository->getMarathonInvestmentMembersCount($filtersDTO);
    }

}

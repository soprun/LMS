<?php

namespace App\Service;

use App\Entity\Timezone;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Throwable;

/** @deprecated use \App\Service\AuthServerService instead */
class AuthServerHelper
{

    public const ERROR_INVALID_TOKEN = 'INVALID_TOKEN'; // невалидный токен
    public const ERROR_INVALID_SESSION = 'INVALID_SESSION'; // сессия не найдена, но возможно она когда-то была
    public const ERROR_TOKEN_EXPIRED = 'TOKEN_EXPIRED'; // сессия была, но устарела

    private $em;
    private $formHelper;
    private $authServerToken;
    private $logger;
    private $client;

    public function __construct(
        EntityManagerInterface $em,
        FormHelper $formHelper,
        string $_authServerToken,
        string $_authServerUrl,
        LoggerInterface $logger
    ) {
        $this->em = $em;
        $this->formHelper = $formHelper;
        $this->authServerToken = $_authServerToken;
        $this->logger = $logger;

        /* Инициируем Guzzle Client */
        $guzzleParams = [
            'base_uri' => $_authServerUrl,
        ];
        // проксируем и отключаем проверку SSL на деве
        if ($_authServerUrl == 'https://auth.toolbox.wip') {
            $guzzleParams = array_merge(
                $guzzleParams,
                [
                    'proxy' => 'http://127.0.0.1:7080',
                    'verify' => false,
                ]
            );
        }
        $this->client = new Client($guzzleParams);
    }

    /**
     * Получение юзера по его AccessToken
     * @param  string  $accessToken
     * @return User
     * @throws GuzzleException
     *
     * @deprecated use \App\Service\AuthServerService instead
     */
    public function getUserByAccessToken(string $accessToken)
    {
        @trigger_error(sprintf('This "%s()" method is deprecated.', __METHOD__), E_USER_DEPRECATED);

        try {
            $request = $this->client->request(
                'GET',
                'auth/lms/session/get',
                [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $accessToken,
                    ],
                ]
            );
        } catch (RequestException $e) {
            $authError = null;
            if ($e->hasResponse()) {
                $response = json_decode($e->getResponse()->getBody()->getContents());
                if (!empty($response->errors)) {
                    // отдаем ошибку authServer как есть
                    $authError = reset($response->errors)->message;
                }
            }

            $this->logger->critical(
                'AuthServerHelper->getUserByAccessToken exception',
                [
                    'requestException' => $authError,
                    'uri' => (string)$e->getRequest()->getUri(),
                    'accessToken' => $accessToken,
                ]
            );

            return (new User())->setAuthError($authError);
        }

        $response = json_decode($request->getBody()->getContents());
        if ($response->status != 'success') {
            $this->logger->critical('AuthServerHelper->getUserByAccessToken: response', $response);

            return (new User())->setAuthError('Ошибка authServer');
        }

        $authUser = $response->data->authUser;

        /** @var User $user */
        $user = $this->em->getRepository(User::class)->findOneBy(
            [
                'id' => $authUser->lmsUserId,
                'authUserId' => $authUser->id,
            ]
        );

        // юзера нет
        if (!$user) {
            return (new User())->setAuthError('Пользователь не найден');
        }

        // наполняем Entity
        $user->setRoles($authUser->roles);
        $user->setEmail($authUser->email);
        $user->setTimezone(
            $this->em->getRepository(Timezone::class)->find($authUser->timezoneId)
        );
        $user->setName($authUser->name);
        $user->setLastname($authUser->lastname);
        $user->setPatronymic($authUser->patronymic);
        $user->setAvatar($authUser->avatar);
        $user->setIsAdmin($authUser->isAdmin);
        $user->setCountry($authUser->country);
        $user->setCity($authUser->city);
        $user->setPhone($authUser->phone);
        $user->setDateOfBirth($authUser->dateOfBirth);
        $user->setVkLink($authUser->vkLink);
        $user->setFbLink($authUser->fbLink);
        $user->setInstaLink($authUser->instaLink);
        $user->setSiteLink($authUser->siteLink);
        $user->setTelegram($authUser->telegram);
        $user->setBusinessInfo($authUser->businessInfo);
        $user->setBusinessCategory($authUser->businessCategory);
        $user->setBusinessCategoryId($authUser->businessCategoryId ?: null);
        $user->setBusinessCategoryName($authUser->businessCategoryName ?: null);
        $user->setNiche($authUser->niche);
        $user->setCompany($authUser->company);
        $user->setBusinessLocationTypeId($authUser->businessLocationTypeId);
        $user->setBusinessCity($authUser->businessCity);
        $user->setProfitPerWeek($authUser->profitPerWeek);
        $user->setProfitPerYear($authUser->profitPerYear);
        $user->setProfitPerLastYear($authUser->profitPerLastYear);
        $user->setFranchises($authUser->franchises);
        $user->setIsPasswordEmpty($authUser->isPasswordEmpty);

        return $user;
    }

    /** @deprecated use \App\Service\AuthServerService instead */
    public function registerLmsUser(Request $request)
    {
        @trigger_error(sprintf('This "%s()" method is deprecated.', __METHOD__), E_USER_DEPRECATED);

        // проверяем токен
        if (!$this->isServerAuthTokenValid($request)) {
            $this->formHelper->addError('serverToken', 'Невалидный токен');

            return $this->formHelper->getResponse('registerLmsUser');
        }

        $data = [];
        $authUserId = $request->request->get('authUserId', null);

        if ($authUserId) {
            // проверяем на повтор
            $lmsUser = $this->em->getRepository(User::class)->findOneBy(['authUserId' => (int)$authUserId]);

            if (!$lmsUser) {
                // создаем LmsUser
                $lmsUser = new User();
                $lmsUser->setAuthUserId($authUserId);
                $this->em->persist($lmsUser);
                $this->em->flush();

                $this->logger->info(
                    sprintf(
                        "SecurityHelper->registerLmsUser: зарегали lmsUserId %s – authUserId %s",
                        $lmsUser->getId(),
                        $authUserId
                    )
                );
            }
            $data['lmsUserId'] = $lmsUser->getId();
        } else {
            $this->formHelper->addError('user', 'Нет идентификатора пользователя');
        }

        return $this->formHelper->getResponse('registerLmsUser', $data);
    }

    /**
     * Проверка валидности токена ServerToken
     * @param  Request  $request
     * @return bool
     *
     * @deprecated use \App\Service\AuthServerService instead
     */
    public function isServerAuthTokenValid(Request $request): bool
    {
        @trigger_error(sprintf('This "%s()" method is deprecated.', __METHOD__), E_USER_DEPRECATED);

        $authorizationHeader = $request->headers->get('Authorization');
        if (!$authorizationHeader || strpos($authorizationHeader, 'ServerToken ') !== 0) {
            return false;
        }

        return (substr($authorizationHeader, 12) == $this->authServerToken);
    }

    /**
     * Получение userInfo о пользователях
     * Лучше не использовать. Альтернатива – getUsers()
     * @param  array  $userIds
     * @param  string|null  $search
     * @param  string|null  $page
     * @return mixed
     * @throws GuzzleException
     * @todo: выпилить
     *
     * @deprecated use \App\Service\AuthServerService instead
     */
    public function getUsersInfo(array $userIds, ?string $search = null, ?string $page = null)
    {
        @trigger_error(sprintf('This "%s()" method is deprecated.', __METHOD__), E_USER_DEPRECATED);

        $userIds = array_unique($userIds);

        try {
            $request = $this->client->request(
                'POST',
                'auth/users/info/get',
                [
                    'headers' => [
                        'Authorization' => 'ServerToken ' . $this->authServerToken,
                    ],
                    'json' => [
                        'userIds' => $userIds,
                        'search' => $search,
                        'page' => $page,
                    ],
                ]
            );
        } catch (RequestException $e) {
            $errorMessage = null;
            if ($e->hasResponse()) {
                $errorMessage = json_decode($e->getResponse()->getBody()->getContents());
            }
            $this->logger->critical(
                'AuthServerHelper->getUsersInfo exception',
                [
                    'requestException' => $errorMessage,
                    'uri' => (string)$e->getRequest()->getUri(),
                ]
            );

            return [];
        }

        $response = json_decode($request->getBody()->getContents());
        if ($response->status != 'success') {
            $this->logger->critical('AuthServerHelper->getUsersInfo return not success', $response);

            return [];
        }

        return $response->data;
    }

    /**
     * Регистрация пользователя в Auth и LMS
     * @param  array  $user
     * @param  bool  $needEmail
     * @param  string  $courseName
     * @return array
     * @throws GuzzleException
     *
     * @deprecated use \App\Service\AuthServerService instead
     */
    public function registerAuthUser(array $user, bool $needEmail = true, string $courseName = 'withoutCourse')
    {
        @trigger_error(sprintf('This "%s()" method is deprecated.', __METHOD__), E_USER_DEPRECATED);

        $response = null;
        $errors = [];

        try {
            $request = $this->client->request(
                'POST',
                'auth/registerLmsUserAsAdmin',
                [
                    'headers' => [
                        'Authorization' => 'ServerToken ' . $this->authServerToken,
                    ],
                    'json' => [
                        'user' => $user,
                        'needEmail' => $needEmail,
                        'courseName' => $courseName,
                    ],
                ]
            );
            $response = json_decode($request->getBody()->getContents());
        } catch (RequestException $e) {
            if (!$e->hasResponse()) {
                $errors[] = [
                    'field' => 'authServer',
                    'message' => 'Ошибка authServer',
                ];

                $this->logger->critical(
                    'AuthServerHelper::registerAuthUser authServer exception',
                    [
                        'uri' => (string)$e->getRequest()->getUri(),
                    ]
                );
            } else {
                $response = json_decode($e->getResponse()->getBody()->getContents());
            }
        }

        if ($response && $response->status !== 'success') {
            foreach ($response->errors as $error) {
                $errors[] = [
                    'field' => $error->field,
                    'message' => $error->message,
                ];
            }
        }

        return [$response->data, $errors];
    }

    /**
     * Отправка сертификата пользователю
     * @param  array  $user
     * @param  string  $courseName
     * @return array
     * @throws GuzzleException
     *
     * @deprecated use \App\Service\AuthServerService instead
     */
    public function sendCertificate(array $user, string $courseName = 'speed')
    {
        @trigger_error(sprintf('This "%s()" method is deprecated.', __METHOD__), E_USER_DEPRECATED);

        $response = null;
        $data = $errors = [];
        try {
            $request = $this->client->request(
                'POST',
                'auth/sendMailCertificate',
                [
                    'headers' => [
                        'Authorization' => 'ServerToken ' . $this->authServerToken,
                    ],
                    'json' => [
                        'user' => $user,
                        'courseName' => $courseName,
                    ],
                ]
            );
            $response = json_decode($request->getBody()->getContents());
        } catch (RequestException $e) {
            $errorMessage = null;
            if (!$e->hasResponse()) {
                $errors[] = [
                    'field' => 'authServer',
                    'message' => 'Ошибка authServer',
                ];

                $this->logger->critical(
                    'AuthServerHelper::sendMailCertificate authServer exception',
                    [
                        'uri' => (string)$e->getRequest()->getUri(),
                    ]
                );
            } else {
                $response = json_decode($e->getResponse()->getBody()->getContents());
            }
        }

        if ($response && $response->status != 'success') {
            foreach ($response->errors as $error) {
                $errors[] = [
                    'field' => $error->field,
                    'message' => $error->message,
                ];
            }
        }
        if ($response) {
            $data = $response->data;
        }

        return [$data, $errors];
    }

    /**
     * Поиск AuthUser по емейлц
     * @param  string  $email
     * @return array
     * @throws GuzzleException
     *
     * @deprecated use \App\Service\AuthServerService instead
     */
    public function getUserInfoByEmail(string $email)
    {
        @trigger_error(sprintf('This "%s()" method is deprecated.', __METHOD__), E_USER_DEPRECATED);

        $response = null;
        $errors = [];

        try {
            $request = $this->client->request(
                'POST',
                'auth/user/info/getByEmail',
                [
                    'headers' => [
                        'Authorization' => 'ServerToken ' . $this->authServerToken,
                    ],
                    'json' => [
                        'email' => $email,
                    ],
                ]
            );
            $response = json_decode($request->getBody()->getContents());
        } catch (RequestException $e) {
            $errorMessage = null;
            if (!$e->hasResponse()) {
                $errors[] = [
                    'field' => 'authServer',
                    'message' => 'Ошибка authServer',
                ];

                $this->logger->critical(
                    'AuthServerHelper::getUserInfoByEmail authServer exception',
                    [
                        'uri' => (string)$e->getRequest()->getUri(),
                    ]
                );
            } else {
                $response = json_decode($e->getResponse()->getBody()->getContents());
            }
        }

        if ($response && $response->status != 'success') {
            foreach ($response->errors as $error) {
                $errors[] = [
                    'field' => $error->field,
                    'message' => $error->message,
                ];
            }
        }

        return [$response->data, $errors];
    }

    /**
     * Получение персональной ссылки на сброс пароля админом для пользователя
     * @param  int  $adminAuthUserId
     * @param $authUserId
     * @return array
     * @throws GuzzleException
     *
     * @deprecated use \App\Service\AuthServerService instead
     */
    public function getResetPasswordLink(int $adminAuthUserId, int $authUserId): array
    {
        @trigger_error(sprintf('This "%s()" method is deprecated.', __METHOD__), E_USER_DEPRECATED);

        $response = null;
        $errors = [];

        try {
            $request = $this->client->request(
                'POST',
                'auth/getResetPasswordLink',
                [
                    'headers' => [
                        'Authorization' => 'ServerToken ' . $this->authServerToken,
                    ],
                    'json' => [
                        'adminAuthUserId' => $adminAuthUserId,
                        'authUserId' => $authUserId,
                    ],
                ]
            );
            $response = json_decode($request->getBody()->getContents());
        } catch (RequestException $e) {
            $errorMessage = null;
            if (!$e->hasResponse()) {
                $errors[] = [
                    'field' => 'authServer',
                    'message' => 'Ошибка authServer',
                ];

                $this->logger->critical(
                    'AuthServerHelper::getResetPasswordLink authServer exception',
                    [
                        'uri' => (string)$e->getRequest()->getUri(),
                    ]
                );
            } else {
                $response = json_decode($e->getResponse()->getBody()->getContents());
            }
        }

        if ($response && $response->status != 'success') {
            foreach ($response->errors as $error) {
                $errors[] = [
                    'field' => $error->field,
                    'message' => $error->message,
                ];
            }
        }

        return [$response->data, $errors];
    }

    /**
     * Edit user on authServer
     * @param  int  $adminAuthUserId
     * @param $user
     * @return array
     * @throws GuzzleException
     *
     * @deprecated use \App\Service\AuthServerService instead
     */
    public function editUserByServer(array $user): array
    {
        @trigger_error(sprintf('This "%s()" method is deprecated.', __METHOD__), E_USER_DEPRECATED);

        $response = null;
        $errors = [];

        try {
            $request = $this->client->request(
                'POST',
                'auth/user/server/edit',
                [
                    'headers' => [
                        'Authorization' => 'ServerToken ' . $this->authServerToken,
                    ],
                    'json' => [
                        'user' => $user,
                    ],
                ]
            );
            $response = json_decode($request->getBody()->getContents());
        } catch (RequestException $e) {
            $errorMessage = null;
            if (!$e->hasResponse()) {
                $errors[] = [
                    'field' => 'editUserByServer',
                    'message' => 'Ошибка authServer',
                ];

                $this->logger->critical(
                    'AuthServerHelper::editUserByServer authServer exception',
                    [
                        'uri' => (string)$e->getRequest()->getUri(),
                    ]
                );
            } else {
                $response = json_decode($e->getResponse()->getBody()->getContents());
            }
        }

        if ($response && $response->status != 'success') {
            foreach ($response->errors as $error) {
                $errors[] = [
                    'field' => $error->field,
                    'message' => $error->message,
                ];
            }
        }

        return [$response->data, $errors];
    }

    /**
     * @deprecated use \App\Service\AuthServerService instead
     * Еще один метод для получения пользователей с аутха. В этот раз с необходимыми полями
     * @param  array  $authIds
     * @param  array  $fields
     * @param  string|null  $where
     * @param  null|int  $page
     * @param  null|int  $pageSize
     * @param  null|array  $params
     * @return array
     */
    public function getUsers(
        array $authIds,
        array $fields,
        ?string $where = '',
        ?int $page = null,
        ?int $pageSize = null,
        ?array $params = []
    ): array {
        @trigger_error(sprintf('This "%s()" method is deprecated.', __METHOD__), E_USER_DEPRECATED);

        try {
            $request = $this->client->request(
                'POST',
                'auth/users_with_concrete_fields',
                [
                    'headers' => [
                        'Authorization' => 'ServerToken ' . $this->authServerToken,
                    ],
                    'json' => [
                        'authIds' => $authIds,
                        'fields' => $fields,
                        'where' => $where,
                        'platform' => 'lms',
                        'page' => $page,
                        'pageSize' => $pageSize,
                        'params' => $params
                    ],
                ]
            );
        } catch (Throwable $e) {
            $this->logger->critical(
                'AuthServerHelper->getUsers exception',
                [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage(),
                ]
            );

            return [];
        }

        $response = json_decode($request->getBody()->getContents(), true);
        if ($response['status'] !== 'success') {
            $this->logger->critical('AuthServerHelper->getUsers return not success', $response);

            return [];
        }

        return $response['data'];
    }

    public function getProfileProperties(): array
    {
        try {
            $request = $this->client->request(
                'GET',
                'properties/profile',
            );

            return json_decode($request->getBody()->getContents(), true)['data'];
        } catch (\Throwable $e) {
            $this->logger->critical(
                'AuthServerHelper->getProfileProperties exception',
                [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage(),
                ]
            );

            return [
                'communityStatuses' => [],
                'businessCategories' => [],
            ];
        }
    }

    /** @deprecated use \App\Service\AuthServerService instead */
    public function implode(array $array): string
    {
        @trigger_error(sprintf('This "%s()" method is deprecated.', __METHOD__), E_USER_DEPRECATED);

        return implode(
            ',',
            array_map(
                static function (string $value) {
                    return sprintf(
                        "'%s'",
                        $value
                    );
                },
                $array
            )
        );
    }

}

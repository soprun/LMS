<?php

declare(strict_types=1);

namespace App\Service;

use AmoCRM\Client\AmoCRMApiClient as AmoClient;
use AmoCRM\Collections\NotesCollection;
use AmoCRM\Exceptions\AmoCRMApiException;
use AmoCRM\Helpers\EntityTypesInterface;
use AmoCRM\Models\LeadModel;
use AmoCRM\Models\NoteType\ServiceMessageNote;
use App\Entity\AmoAccount;
use App\Entity\AmoCallbackGroupIdByAmoProductId;
use App\Entity\User;
use App\Entity\UserGroup;
use App\Entity\UserGroupUser;
use App\Repository\AmoCallbackGroupIdByAmoProductIdRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Token\AccessTokenInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;

/**
 * Временный хелпер для добавления пользователей в ЛМС и группу после покупки курса.
 * Можно удалить, когда сделаем нормальную оплату
 */
class AmoHelper implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    private $em;
    private $authServerHelper;
    private $amoClient;

    /**
     * @var string
     * @readonly
     */
    private $clientId;
    /** @var string */
    private $clientSecret;
    /** @var string */
    private $clientRedirectUri;

    // поля в амо
    private $fieldIds = [
        'product' => 1740719,
        'cityNameFromForm' => 1473157,
        'phone' => 1473119,
        'email' => 1473121,
        'ticketLevel' => 1803270, // Категория билета
    ];

    private $ticketLevelIds = [
        'minimal' => '4187150',
        'standard' => '4187142',
        'business' => '4187152',
        'premium' => '4187154',
        'vip' => '4187144',
        'like-family' => '4188362',
        'special' => '4187731',
    ];

    public const PIPELINE_HUNDRED = 4315395;
    public const PIPELINE_SPEED = 1709019;
    public const PIPELINE_CONCENTRAT = 1751832;
    public const PIPELINE_LIKE_FAMILY = 990339;
    public const PIPELINE_MSA = 2291757;
    public const PIPELINE_WEBINARS = 3244548;
    public const PIPELINE_SALE = 2187924;
    public const PIPELINE_TRIPWIRE = 730809;

    public const PRODUCT_MSA = 4171816;

    public const PRODUCT_HUNDRED4 = 4188324;
    public const PRODUCT_HUNDRED_5 = 4188348;
    public const PRODUCT_SPEED_CLUB_2 = 4188370;
    public const PRODUCT_MARATHON_INVESTMENT_5 = 4188424;

    public const PRODUCT_HUNDRED_6 = 4188466;
    public const PRODUCT_SPEED_CLUB_3 = 4188398;

    // LMS-316
    public const PRODUCT_HUNDRED_7 = 4188470;
    public const PRODUCT_SPEED_CLUB_4 = 4188400;

    public const PRODUCT_HUNDRED_8 = 4188564;
    public const PRODUCT_HUNDRED_8_LF = 4188566;
    public const PRODUCT_HUNDRED_8_MSA = 4188568;
    public const PRODUCT_SPEED_CLUB_5 = 4188554;
    public const PRODUCT_SPEED_CLUB_5_LF = 4188570;
    public const PRODUCT_SPEED_CLUB_5_MSA = 4188572;
    public const PRODUCT_SPEED_CLUB_5_FRIEND = 4188574;
    public const PRODUCT_HUNDRED_8_SALE = 4188580;
    public const PRODUCT_SPEED_CLUB_5_SALE_LF = 4188596;
    public const PRODUCT_SPEED_CLUB_5_SALE_BUSINESS = 4188594;
    public const PRODUCT_SPEED_MSA_LITE_BUSINESS = 4188622;

    public const PRODUCT_HUNDRED_9 = 4188560;
    public const PRODUCT_SPEED_CLUB_6 = 4188562;
    public const PRODUCT_SPEED_CLUB_6_1 = 4188662;

    public const PRODUCT_LESSONS_SALE_11_11 = 4188592;

    public const PRODUCT_SPEED_INFINITY = 4188546;
    public const PRODUCT_MSA_5_YEARS = 4188550;

    public const PRODUCT_WEBINAR_KITCHEN_2809 = 4188524;

    public const PRODUCT_MEDITATIONS_SALE = 4188614;
    public const PRODUCT_SPEED_BUSINESS_MI_SALE = 4188612;
    public const PRODUCT_SPEED_LF_MI_SALE = 4188616;
    public const PRODUCT_HUNDRED_BD_SALE = 4188618;

    public function __construct(
        EntityManagerInterface $em,
        AuthServerHelper $authServerHelper,
        LoggerInterface $logger,
        string $clientId,
        string $clientSecret,
        string $clientRedirectUri
    ) {
        $this->em = $em;
        $this->authServerHelper = $authServerHelper;
        $this->logger = $logger;
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->clientRedirectUri = $clientRedirectUri;
        $this->amoClient = $this->getAmoClient();
    }

    /**
     * @description Поиск сделки в амо по ее id
     *
     * @param int $leadId
     *
     * @return string[]
     * @psalm-ignore-nullable-return
     */
    public function getLeadById(int $leadId): ?array
    {
        $lead = null;

        if (!$this->amoClient) {
            $this->logger->critical('{method}: {message}', [
                'method' => __METHOD__,
                'message' => 'An error has occurred, amo client is not initialized!',
                'leadId' => $leadId,
            ]);

            return null;
        }

        try {
            // берем все сделки по емейлу из амо
            $leads = $this->amoClient->leads()->getOne($leadId, [
                LeadModel::CONTACTS,
                LeadModel::CATALOG_ELEMENTS,
            ]);

            if ($leads !== null) {
                // добавляем дополнительную инфу
                $productId = $closedAt = $cityName = $ticketLevelId = $leadTags = $mainContactId = null;

                foreach ($leads->getCustomFieldsValues()->toArray() as $customField) {
                    if ($customField['field_id'] == $this->fieldIds['product']) {
                        $productId = reset($customField['values'])['enum_id'];
                    }
                    if ($customField['field_id'] == $this->fieldIds['cityNameFromForm']) {
                        $cityName = reset($customField['values'])['value'];
                    }
                    if ($customField['field_id'] == $this->fieldIds['ticketLevel']) {
                        $ticketLevelId = reset($customField['values'])['enum_id'];
                    }
                }
                if ($leads->getClosedAt() !== null) {
                    $closedAt = (new \DateTime())->setTimestamp($leads->getClosedAt());
                }

                if ($leads->getTags() !== null) {
                    $leadTags = $leads->getTags()->toArray();
                }

                if ($leads->getContacts() !== null) {
                    if ($leads->getContacts()->getBy('isMain', true) !== null) {
                        $mainContactId = $leads->getContacts()->getBy('isMain', true)->getId();
                    } else {
                        $mainContactId = $leads->getContacts()->first()->getId();
                    }
                }

                $lead = array_merge(
                    $leads->toArray(),
                    [
                        'productId' => $productId,
                        'closedAt' => $closedAt,
                        'cityName' => $cityName,
                        'ticketLevelId' => $ticketLevelId,
                        'main_contact_id' => $mainContactId,
                        'tags' => $leadTags,
                        'price' => $leads->getPrice(),
                    ]
                );
            }

            return $lead;
        } catch (Exception $exception) {
            $this->logger->critical('{method}: {message} #{leadId}', [
                'method' => __METHOD__,
                'message' => $exception->getMessage(),
                'code' => $exception->getCode(),
                'leadId' => $leadId,
                'trace' => $exception->getTrace(),
            ]);
        }
    }

    /**
     * Поиск контакта в амо по его id
     *
     * @param int $contactId
     *
     * @return array|null
     */
    public function getContactById(int $contactId): ?array
    {
        $contact = null;

        try {
            if (!$this->amoClient) {
                $this->logger->critical('{method}: {message}', [
                    'method' => __METHOD__,
                    'message' => 'An error has occurred, amo client is not initialized!',
                    'contactId' => $contactId,
                ]);

                return null;
            }

            // берем все сделки по емейлу из амо
            $contacts = $this->amoClient->contacts()->getOne($contactId);

            if ($contacts !== null) {
                // добавляем дополнительную инфу
                $phone = $email = null;
                foreach ($contacts->getCustomFieldsValues()->toArray() as $customField) {
                    if ($customField['field_id'] == $this->fieldIds['phone']) {
                        $phone = reset($customField['values'])['value'];
                    }
                    if ($customField['field_id'] == $this->fieldIds['email']) {
                        $email = reset($customField['values'])['value'];
                    }
                }

                $contact = array_merge(
                    $contacts->toArray(),
                    [
                        'phone' => $phone,
                        'email' => $email,
                    ]
                );
            }
        } catch (Exception $e) {
            $this->logger->critical(
                'AmoHelper::getContactById exception',
                [
                    'contactId' => $contactId,
                    'code' => $e->getCode(),
                    'message' => $e->getMessage(),
                ]
            );
        }

        return $contact;
    }

    private function createNote(int $leadId, string $text): void
    {
        //Создадим примечания
        $notesCollection = new NotesCollection();
        $serviceMessageNote = new ServiceMessageNote();
        $serviceMessageNote->setEntityId($leadId)
            ->setText($text)
            ->setService('Api LMS')
            ->setCreatedBy(0);

        $notesCollection->add($serviceMessageNote);

        try {
            $leadNotesService = $this->amoClient->notes(EntityTypesInterface::LEADS);
            $leadNotesService->add($notesCollection);
        } catch (AmoCRMApiException $e) {
            $this->logger->critical(
                'AmoHelper::createNote exception',
                [
                    'leadId' => $leadId,
                    'code' => $e->getCode(),
                    'message' => $e->getMessage(),
                ]
            );
        }
    }

    /**
     * @param int $leadId
     * @param string $courseName
     * @param null|int $groupId
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function giveAccessToCourseByLeadId(int $leadId, ?string $courseName, ?int $groupId = null): void
    {
        $courseName = (string)$courseName;

        // ищем лид
        $lead = $this->getLeadById($leadId);

        if (empty($lead)) {
            $this->logger->info(
                "AmoHelper::giveAccessToCourseByLeadId не нашли лид",
                [
                    'leadId' => $leadId,
                ]
            );

            return;
        }

        // ищем контакт
        $contact = $this->getContactById($lead['main_contact_id']);
        if (!$contact) {
            $this->logger->info(
                "AmoHelper::giveAccessToCourseByLeadId не нашли контакт",
                [
                    'leadId' => $leadId,
                ]
            );

            return;
        }

        // подготавливаем данные
        $userArray = [
            'name' => $contact['name'],
            'email' => $contact['email'],
            'phone' => $contact['phone'],
        ];

        // если айди группы не передан явно, ищем по продукту
        if (!$groupId) {
            $groupId = $this->getGroupIdByAmoProductId($lead);
        }

        if (!$groupId) {
            $this->logger->critical(
                "AmoHelper::giveAccessToCourseByLeadId не нашли группу",
                [
                    'productId' => $lead['productId'],
                    'leadId' => $leadId,
                ]
            );

            return;
        }

        // регистриурем на аутх
        [$authUser, $errors] = $this->authServerHelper->registerAuthUser($userArray, true, $courseName);
        if (!empty($errors) && !isset($authUser->lmsUserId)) {
            $this->logger->critical("AmoHelper::giveAccessToCourseByLeadId не смогли зарегать на аутх", $errors);

            return;
        }

        // берем пользователя
        /** @var User|null $user */
        $user = $this->em->getRepository(User::class)->find($authUser->lmsUserId);
        if (!$user) {
            $this->logger->critical(
                "AmoHelper::giveAccessToCourseByLeadId не нашли LmsUser",
                [
                    'lmsUserId' => $authUser->lmsUserId,
                ]
            );

            return;
        }

        // берем группу
        /** @var UserGroup|null $group */
        $group = $this->em->getRepository(UserGroup::class)->find($groupId);
        if (!$group) {
            $this->logger->info(
                "AmoHelper::giveAccessToCourseByLeadId не нашли группу",
                [
                    'groupId' => $groupId,
                ]
            );

            return;
        }

        // проверяем, что уже состоит в группе
        $existUserInGroup = $group->getUserRelations()
            ->exists(
                function (UserGroupUser $value) use ($user) {
                    return $user === $value->getUser();
                }
            );
        if ($existUserInGroup) {
            $this->logger->info(
                "AmoHelper::giveAccessToCourseByLeadId уже в группе",
                [
                    'lmsUserId' => $user->getId(),
                ]
            );

            return;
        }

        // добавляем в группу
        $group->addAmoUser($user, $leadId, $lead['price']);
        $this->em->persist($group);
        $this->em->flush();

        // создаем коммент, что добавили
        $this->createNote(
            $leadId,
            sprintf(
                "lms.toolbox.bz: отправили приглашение и добавили в группу «%s»",
                $group->getName()
            )
        );

        $this->logger->info(
            "AmoHelper::giveAccessToCourseByLeadId добавили",
            [
                'leadId' => $leadId,
                'lmsUserId' => $user->getId(),
                'groupId' => $group->getId(),
            ]
        );
    }

    public function removeAccessFromCourseByLeadId(int $leadId): void
    {
        // TODO: Возврат делать из всех связанных групп, в том числе из предоплат. Сейчас только из основных

        // ищем лид
        $lead = $this->getLeadById($leadId);

        if (empty($lead)) {
            $this->logger->info(
                "{method} не нашли лид",
                [
                    'method' => __METHOD__,
                    'leadId' => $leadId,
                ]
            );

            return;
        }

        // ищем контакт
        $contact = $this->getContactById($lead['main_contact_id']);

        if (empty($contact)) {
            $this->logger->info(
                "{method} не нашли контакт",
                [
                    'method' => __METHOD__,
                    'leadId' => $leadId,
                ]
            );

            return;
        }

        // подготавливаем данные
        $userArray = [
            'name' => $contact['name'],
            'email' => $contact['email'],
            'phone' => $contact['phone'],
        ];

        // если айди группы не передан явно, ищем по продукту
        $groupId = $this->getGroupIdByAmoProductId($lead);

        if (empty($groupId)) {
            $this->logger->info(
                "{method} не нашли группу",
                [
                    'method' => __METHOD__,
                    'productId' => $lead['productId'],
                ]
            );

            return;
        }

        // ищем на аутх
        [$authUser, $errors] = $this->authServerHelper->getUserInfoByEmail($userArray['email']);

        if (!empty($errors) && !isset($authUser->lmsUserId)) {
            $this->logger->critical(
                '{method} не смогли найти на аутах',
                [
                    'method' => __METHOD__,
                    'errors' => $errors,
                ]
            );

            return;
        }

        // берем пользователя
        /** @var User|null $user */
        $user = $this->em->getRepository(User::class)->find($authUser->lmsUserId);

        if (!$user) {
            $this->logger->critical(
                '{method} не нашли LmsUser',
                [
                    'method' => __METHOD__,
                    'lmsUserId' => $authUser->lmsUserId,
                ]
            );

            return;
        }

        // берем группу
        /** @var UserGroup|null $group */
        $group = $this->em->getRepository(UserGroup::class)->find($groupId);
        if (!$group) {
            $this->logger->info(
                "{method} не нашли группу",
                [
                    'method' => __METHOD__,
                    'groupId' => $groupId,
                ]
            );

            return;
        }

        // проверяем, что уже состоит в группе
        /** @var null|UserGroupUser $userGroupUser */
        $userGroupUser = $this->em->getRepository(UserGroupUser::class)->findOneBy(
            [
                'user' => $user,
                'userGroup' => $group,
            ]
        );

        if (!$userGroupUser) {
            $altGroup = $group->getAlternativeGroup();

            if (!$altGroup) {
                $this->logger->critical(
                    '{method} не нашли в группе',
                    [
                        'method' => __METHOD__,
                        'leadId' => $leadId,
                        'group' => $group->getId(),
                        'lmsUser' => $user->getId(),
                    ]
                );

                return;
            }

            $group = $altGroup;

            // ищем в альтернативной группе
            /** @var null|UserGroupUser $userGroupUser */
            $userGroupUser = $this->em->getRepository(UserGroupUser::class)->findOneBy(
                [
                    'user' => $user,
                    'userGroup' => $altGroup,
                ]
            );

            if (!$userGroupUser) {
                $this->logger->critical(
                    '{method} не нашли и в альтернативной группе',
                    [
                        'method' => __METHOD__,
                        'leadId' => $leadId,
                        'group' => $altGroup->getId(),
                        'lmsUser' => $user->getId(),
                    ]
                );

                return;
            }
        }

        // удаляем из группы
        $userGroupUser->setDeleted(true);
        $this->em->persist($userGroupUser);
        $this->em->flush();

        // создаем коммент, что удалили
        $this->createNote(
            $leadId,
            sprintf(
                "lms.toolbox.bz: удалили из группы «%s»",
                $group->getName()
            )
        );

        $this->logger->info(
            '{method} удалили',
            [
                'method' => __METHOD__,
                'leadId' => $leadId,
                'lmsUserId' => $user->getId(),
                'groupId' => $group->getId(),
            ]
        );
    }

    private function getGroupIdByAmoProductId(array $lead): ?int
    {
        /** @var AmoCallbackGroupIdByAmoProductId|null $group */
        $group = $this->em->getRepository(AmoCallbackGroupIdByAmoProductIdRepository::class)
            ->findOneByProductIdField($lead['productId']);

        if ($group === null) {
            $this->logger->error('{method}: {message}', [
                'method' => __METHOD__,
                'message' => 'An error occurred, the group could not be found by the product ID.',
                'product_id' => $lead['productId']
            ]);

            return null;
        }

        foreach ($this->ticketLevelIds as $id) {
            if (isset($group->getTicketLevelIds()[$id])) {
                return (int)$group->getTicketLevelIds()[$id];
            }
        }

        return $group->getDefaultGroupId();
    }


    public function saveToken(array $accessToken)
    {
        if (
            isset($accessToken)
            && isset($accessToken['accessToken'])
            && isset($accessToken['refreshToken'])
            && isset($accessToken['expires'])
            && isset($accessToken['baseDomain'])
        ) {
            $accountToken = new AmoAccount($accessToken);
            $this->em->persist($accountToken);
            $this->em->flush();
        } else {
            $this->logger->critical(
                'AmoHelper::saveToken exception',
                [
                    'Invalid access token ' => var_export($accessToken, true),
                ]
            );
        }
    }

    /**
     * @return AccessToken|null
     */
    private function getToken(): ?AccessToken
    {
        $accessToken = $this->em->getRepository(AmoAccount::class)->findBy([], ['createdAt' => 'desc'], 1);

        if (isset($accessToken) && array_key_exists(0, $accessToken)) {
            return $accessToken[0]->getAmoAccessToken();
        }

        $this->logger->critical(
            '{method}: {message}',
            [
                'method' => __METHOD__,
                'message' => 'Invalid access token!',
                'accessToken' => var_export($accessToken, true),
            ]
        );

        return null;
    }

    private function getAmoClient(): ?AmoClient
    {
        $apiClient = new AmoClient(
            $this->clientId,
            $this->clientSecret,
            $this->clientRedirectUri
        );
        $accessToken = $this->getToken();

        if (!$accessToken) {
            $message = 'An error occurred issuing an access token from amoCRM!';

            $this->logger->error('{method}: {message}', [
                'method' => __METHOD__,
                'message' => $message,
                'clientId' => $this->clientId,
            ]);

            @trigger_error(
                sprintf(
                    '%s: %s clientId:#%s',
                    self::class,
                    $message,
                    $this->clientId
                ),
                E_USER_WARNING
            );

            return null;
        }

        $apiClient->setAccessToken($accessToken)
            ->setAccountBaseDomain($accessToken->getValues()['baseDomain'])
            ->onAccessTokenRefresh(
                function (AccessTokenInterface $accessToken, string $baseDomain) {
                    $this->saveToken(
                        [
                            'accessToken' => $accessToken->getToken(),
                            'refreshToken' => $accessToken->getRefreshToken(),
                            'expires' => $accessToken->getExpires(),
                            'baseDomain' => $baseDomain,
                        ]
                    );
                }
            );

        return $apiClient;
    }

}

<?php

namespace App\Service;

use App\Entity\Timezone;
use App\Entity\User;
use App\Exceptions\AuthServerServiceException;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Google\Cloud\Core\Exception\BadRequestException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use InvalidArgumentException;
use JsonException;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Throwable;

final class AuthServerService
{
    private $em;
    private $logger;
    private $client;
    /** @var Container */
    private $container;
    /**
     * @var UserGroupHelper
     */
    private $userGroupHelper;

    protected $timezones = [];

    /**
     * @var string
     */
    private $userAccessToken;

    /**
     * @return string
     */
    public function getUserAccessToken(): ?string
    {
        return $this->userAccessToken;
    }

    /**
     * @param string $userAccessToken
     *
     * @return self
     */
    public function setUserAccessToken(string $userAccessToken): self
    {
        $this->userAccessToken = $userAccessToken;

        return $this;
    }


    public function __construct(
        EntityManagerInterface $em,
        ContainerInterface $container,
        LoggerInterface $logger,
        UserGroupHelper $userGroupHelper
    ) {
        $this->em = $em;
        $this->container = $container;
        $this->logger = $logger;

        /* Инициируем Guzzle Client */
        $guzzleParams = [
            'base_uri' => $container->getParameter('AUTH_SERVER_URL'),
        ];
        // @todo: убрать когда Сема поставит ссл
        if ('https://auth.pgtb.ru' === $container->getParameter('AUTH_SERVER_URL')) {
            $guzzleParams['verify'] = false;
        }
        // проксируем и отключаем проверку SSL на деве
        if ('https://auth.toolbox.wip' === $container->getParameter('AUTH_SERVER_URL')) {
            $guzzleParams['verify'] = false;

            if ($container->getParameter('HTTPS_PROXY')) {
                $guzzleParams['proxy'] = $container->getParameter('HTTPS_PROXY');
            }
        }
        $this->client = new Client($guzzleParams);
        $this->userGroupHelper = $userGroupHelper;
    }

    protected function getTimezone(int $id): ?Timezone
    {
        if (!isset($this->timezones[$id])) {
            $this->timezones[$id] = $this->em->getRepository(Timezone::class)->find($id);
        }

        return $this->timezones[$id];
    }

    /**
     * @param array $formData
     *
     * @return mixed
     * @todo: Не проверено в ЛМС
     * Регистрация или получение id существующего пользователя
     */
    public function createUser(array $formData)
    {
        try {
            $request = $this->client->request('POST', 'user/create', [
                'headers' => [
                    'Authorization' => 'ServerToken ' . $this->container->getParameter('AUTH_SERVER_TOKEN'),
                ],
                'json' => $formData,
            ]);
        } catch (GuzzleException $e) {
            $errorMessage = null;
            if ($e->getResponse() !== null) {
                $errorMessage = json_decode($e->getResponse()->getBody()->getContents(), true);
            }
            $this->logger->critical('AuthServerService->simpleRegister exception', [
                'requestException' => $errorMessage,
                'uri' => (string)$e->getRequest()->getUri(),
            ]);

            return [];
        }

        $response = json_decode($request->getBody()->getContents(), true);
        if ($response['status'] !== 'success') {
            $this->logger->critical('AuthServerService->simpleRegister return not success', $response);

            return [];
        }

        return $response['data'];
    }

    /**
     * @param int $id
     * @param array $formData
     *
     * @return mixed
     * @todo: Не проверено в ЛМС
     * Изменение пользователя
     */
    public function updateUser(int $id, array $formData)
    {
        try {
            $request = $this->client->request('POST', "user/update/{$id}", [
                'headers' => [
                    'Authorization' => 'ServerToken ' . $this->container->getParameter('AUTH_SERVER_TOKEN'),
                ],
                'json' => $formData,
            ]);
        } catch (GuzzleException $e) {
            $errorMessage = null;
            if ($e->getResponse() !== null) {
                $errorMessage = json_decode($e->getResponse()->getBody()->getContents(), true);
            }
            $this->logger->critical('AuthServerService->updateUser exception', [
                'requestException' => $errorMessage,
                'uri' => (string)$e->getRequest()->getUri(),
            ]);

            return [];
        }

        $response = json_decode($request->getBody()->getContents(), true);
        if ($response['status'] !== 'success') {
            $this->logger->debug('AuthServerService->updateUser return not success', $response);

            return [];
        }

        return $response['data'];
    }

    /**
     * @param User $user
     *
     * @return mixed|string[]
     * @todo: Не проверено в ЛМС
     */
    public function getTwoFaSecret(User $user)
    {
        return $this->send("/api/internal/auth/two-fa/secret/{$user->getAuthUserId()}", null, 'GET');
    }

    /**
     * @param User $user
     * @param string $code
     *
     * @return mixed|string[]
     * @todo: Не проверено в ЛМС
     */
    public function enableTwoFa(User $user, string $code)
    {
        return $this->send("/api/internal/auth/two-fa/enable/{$user->getAuthUserId()}/{$code}", null, 'GET');
    }

    /**
     * @param User $user
     * @param string $code
     *
     * @return mixed|string[]
     * @todo: Не проверено в ЛМС
     */
    public function disableTwoFa(User $user, string $code)
    {
        return $this->send("/api/internal/auth/two-fa/disable/{$user->getAuthUserId()}/{$code}", null, 'GET');
    }

    /**
     * @param string $url
     * @param null|array $formData
     * @param string $method
     *
     * @return mixed
     */
    public function send(string $url, ?array $formData = [], string $method = 'POST')
    {
        try {
            $options = [
                'headers' => [
                    'Authorization' => 'ServerToken ' . $this->container->getParameter('AUTH_SERVER_TOKEN'),
                ],
            ];
            if ($formData) {
                $options['json'] = $formData;
            }
            $request = $this->client->request($method, $url, $options);
        } catch (GuzzleException $e) {
            $errorMessage = null;
            if ($e->getResponse() !== null) {
                $errorMessage = json_decode($e->getResponse()->getBody()->getContents(), true);
            }
            $this->logger->critical("AuthServerService->{$url} exception", [
                'requestException' => $errorMessage ?: $e->getMessage(),
                'uri' => (string)$e->getRequest()->getUri(),
                'formData' => $formData,
            ]);

            return ['status' => 'error'];
        }

        try {
            return json_decode($request->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            throw new RuntimeException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * @throws GuzzleException
     */
    public function sendRealityGreeting(User $user): void
    {
        $response = $this->client->request(
            'POST',
            '/api/internal/user/' . $user->getAuthUserId() . '/greeting/reality',
            [
                'headers' => [
                    'Authorization' => 'ServerToken ' . $this->container->getParameter('AUTH_SERVER_TOKEN'),
                ],
            ]
        );
        if ($response->getStatusCode() !== Response::HTTP_OK) {
            throw new RuntimeException('auth server return wrong response code ' . $response->getStatusCode());
        }
    }

    /**
     * @param null|array $ids
     * @param string|null $search
     * @param int $pageSize
     * @param int $page
     *
     * @return array
     * @deprecated
     *
     */
    public function getUsers(?array $ids = null, ?string $search = null, int $pageSize = 1, int $page = 1): array
    {
        $formData = ['pageSize' => $pageSize, 'page' => $page];
        if ($ids) {
            $formData['ids'] = $ids;
        }
        if ($search) {
            $formData['search'] = $search;
        }
        $response = $this->send('user/list', $formData);
        if ($response['status'] !== 'success') {
            $this->logger->debug('AuthServerService->getUsers return not success', $response);

            return [];
        }

        return $response['data'];
    }

    /**
     * @param array{ids: array<int|User>, emails: array<string>, pageSize: int, page: int} $formData
     *
     * @return array
     */
    public function getInternalUsers(array $formData = []): array
    {
        if (isset($formData['ids'])) {
            if (reset($formData['ids']) instanceof User) {
                $ids = array_map(static function (User $user) {
                    return $user->getAuthUserId();
                }, $formData['ids']);
                $formData['ids'] = $ids;
            } elseif (is_array(reset($formData['ids']))) {
                $ids = array_map(static function (array $user) {
                    if (!isset($user['authUserId'])) {
                        throw new InvalidArgumentException('Должен быть ключ authUserId');
                    }

                    return $user['authUserId'];
                }, $formData['ids']);
                $formData['ids'] = $ids;
            }
        }
        $response = $this->send('api/internal/user/list', $formData);
        if ($response['status'] !== 'success') {
            $this->logger->debug('AuthServerService->getInternalUsers return not success', $response);

            return ['list' => [], 'count' => 0];
        }

        return $response['data'];
    }

    /**
     * @param User[]|Collection $users
     * @param null|string $search
     * @param null|bool $saveOrder
     *
     * @return array
     */
    public function refillUsers($users, ?string $search = null, ?bool $saveOrder = null): array
    {
        $serviceUsers = [];
        foreach ($users as $user) {
            $serviceUsers[$user->getAuthUserId()] = $user;
        }

//        if (count($serviceUsers) > 50) {
//            throw new RuntimeException('refill user limit 50');
//        }
//        $serviceUsers = array_slice($serviceUsers, 0, 50, true);

        $formData = [
            'ids' => array_keys($serviceUsers),
            'pageSize' => count($serviceUsers),
        ];
        if ($search !== null) {
            $formData['search'] = $search;
        }
        if ($saveOrder !== null) {
            $formData['save-order-by-ids'] = $saveOrder;
        }
        $result = $this->getInternalUsers($formData);

        foreach ($result['list'] as $authUser) {
            if (isset($serviceUsers[$authUser['id']])) {
                $this->mapUser($serviceUsers[$authUser['id']], $authUser);
            }
        }

        return array_values($serviceUsers);
    }

    /**
     * @param array $authUsers
     * @param null|QueryBuilder $qb
     * @param null|string $userAlias
     * @param bool $indexed
     *
     * @return int|mixed|string
     */
    public function rebuildUsers(
        array $authUsers,
        ?QueryBuilder $qb = null,
        ?string $userAlias = 'user',
        bool $indexed = false
    ) {
        $authUserIds = [];
        foreach ($authUsers as $authUser) {
            $authUserIds[] = $authUser['id'];
        }

        if (!$qb) {
            $qb = $this->em->getRepository(User::class)
                ->createQueryBuilder($userAlias, "{$userAlias}.authUserId");
            $indexed = true;
        }

        $serviceUsers = $qb->andWhere("{$userAlias}.authUserId in (:as_{$userAlias}_authUserIds)")
            ->setParameter("as_{$userAlias}_authUserIds", $authUserIds)
            ->getQuery()
            ->getResult();

        $result = [];
        foreach ($authUsers as $authUser) {
            $serviceUser = null;
            if ($indexed) {
                if (isset($serviceUsers[$authUser['id']])) {
                    $serviceUser = $serviceUsers[$authUser['id']];
                }
            } else {
                $serviceUsersFiltered = array_filter(
                    $serviceUsers,
                    static function (User $serviceUser) use ($authUser) {
                        return $serviceUser->getAuthUserId() === (int)$authUser['id'];
                    }
                );
                if (!empty($serviceUsersFiltered)) {
                    $serviceUser = reset($serviceUsersFiltered);
                }
            }
            if ($serviceUser) {
                $this->mapUser($serviceUser, $authUser);
                $result[] = $serviceUser;
            }
        }

        return $result;
    }

    /**
     * @param User $serviceUser
     * @param array $authUser
     */
    private function mapUser(User $serviceUser, array $authUser): void
    {
        $serviceUser->setAuthUserId($authUser['id']);
        $serviceUser->setEmail($authUser['email']);
        $serviceUser->setTimezone($this->getTimezone($authUser['timezoneId']));
        $serviceUser->setName($authUser['name'] ?? '');
        $serviceUser->setLastname($authUser['lastname'] ?? '');
        $serviceUser->setAvatar($authUser['avatar'] ?? '');
        $serviceUser->setCountry($authUser['country'] ?? '');
        $serviceUser->setCity($authUser['city'] ?? '');
        $serviceUser->setPhone($authUser['phone'] ?? '');
        $serviceUser->setDateOfBirth($authUser['dateOfBirth'] ?? null);
        $serviceUser->setNiche($authUser['niche'] ?? null);
    }

    /**
     * Поиск пользователей и получение мета-информации
     *
     * @param array $filter
     * - $filter[ids]          - массив IDs пользователей;
     * - $filter[withNullIds]  - Флаг, вернуть ID даже если оно null;
     * - $filter[exclude]      - массив IDs пользователей;
     * - $filter[name]         - имя пользователя;
     * - $filter[lastname]     - фамилия пользователя;
     * - $filter[username]     - никнейм пользователя;
     * - $filter[email]        - почта пользователя;
     * - $filter[strict]       - Режим сравнения: 0 строгий, 1 мягкий (❗️INTEGER❗️).
     * - $filter[_resultLimit] - Лимит выборки: 0 неограничен, 1 ограничен (❗️INTEGER❗️).
     * @param array $groups
     * Для получения ID пользователя используются отдельные группы:
     *  - app;
     *  - lms;
     *  - erp;
     *  - auth.
     * @param int $page 1
     * @param int $pageSize 10
     *  - "-1" - не использовать LIMIT
     *
     * @return array
     */
    public function filterUsers(array $filter, array $groups, int $page = 1, int $pageSize = 10): array
    {
        try {
            $json = [];
            if (!empty($filter['ids'])) {
                $json = ['ids' => $filter['ids']];
                unset($filter['ids']);
            }
            $query = array_merge($filter, [
                'page' => $page,
                'pageSize' => $pageSize,
                'groups' => $groups,
            ]);
            /** @var GuzzleResponse $response */
            $response = $this->client->request('GET', 'api/public/user/filter', [
                'headers' => [
                    'Authorization' => $this->getUserAccessToken(),
                ],
                'query' => $query,
                'json' => $json,
            ]);
            if ($response->getStatusCode() === Response::HTTP_OK) {
                $data = json_decode($response->getBody()->getContents(), true);

                return $data;
            }
        } catch (ClientException $e) {
            $response = $e->getResponse();
            if ($response->getStatusCode() === Response::HTTP_BAD_REQUEST) {
                $this->logger->error('Не валидные параметры запроса', [
                    'filter' => $filter,
                    'groups' => $groups,
                    'page' => $page,
                    'pageSize' => $pageSize,
                    'exception' => BadRequestException::class,
                    'method' => __METHOD__,
                ]);
                throw new BadRequestException('auth');
            } elseif ($response->getStatusCode() === Response::HTTP_FORBIDDEN) {
                $this->logger->error('Отказано в доступе', [
                    'exception' => AccessDeniedHttpException::class,
                    'method' => __METHOD__,
                ]);
                throw new AccessDeniedHttpException('auth');
            }

            $this->logger->error('Ошибка клиента.', [
                'exception' => get_class($e),
                'method' => __METHOD__,
                'trace' => $e->getTrace(),
                'filter' => $filter,
                'groups' => $groups,
                'page' => $page,
                'pageSize' => $pageSize,
            ]);

            throw new AuthServerServiceException('Не удалось выполнить запрос поиска пользователей');
        }

        return [];
    }

    /**
     * @param int $authUserId
     * @param array{niche: string, businessCategoryId: int} $data
     *
     * @return array
     * @throws Throwable
     */
    public function editUserInfo(int $authUserId, array $data): array
    {
        $response = $this->client->request(
            'PATCH',
            '/api/internal/user/' . $authUserId . '/info',
            [
                'headers' => ['Authorization' => 'ServerToken ' . $this->container->getParameter('AUTH_SERVER_TOKEN')],
                'json' => $data,
            ]
        );
        return json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);
    }

    /**
     * @param array $userIds
     *
     * @return array []
     * @throws AuthServerServiceException
     * @throws GuzzleException
     */
    public function checkSessionByUserId(array $userIds): array
    {
        try {
            /** @var GuzzleResponse $response */
            $response = $this->client->request('POST', 'api/public/user/check-session', [
                'headers' => [
                    'Authorization' => $this->getUserAccessToken(),
                ],
                'json' => [
                    'ids' => $userIds,
                    'type' => 'lms'
                ],
            ]);
            if ($response->getStatusCode() === Response::HTTP_OK) {
                return json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);
            }
        } catch (ClientException $exception) {
            $this->logger->error('{method}: {message}', [
                'method' => __METHOD__,
                'message' => $exception->getMessage(),
                'trace' => $exception->getTrace(),
            ]);
            throw new AuthServerServiceException(
                'Не удалось выполнить запрос поиска сессий пользователей',
                $exception->getCode(),
                $exception
            );
        } catch (JsonException $exception) {
            $this->logger->error('{method}: {message}', [
                'method' => __METHOD__,
                'message' => $exception->getMessage(),
                'trace' => $exception->getTrace(),
            ]);
            throw new AuthServerServiceException(
                $exception->getMessage(),
                $exception->getCode(),
                $exception
            );
        }
    }
}

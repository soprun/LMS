<?php

namespace App\Service;

use App\Entity\Courses\Course;
use App\Entity\Courses\CourseFolder;
use App\Entity\GroupPermission;
use App\Entity\User;
use App\Entity\UserGroup;
use App\Entity\UserGroupFolder;
use App\Entity\UserGroupUser;
use App\Repository\UserGroupRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\Cache\TagAwareCacheInterface;

class UserGroupHelper
{

    private $em;
    private $security;
    private $formHelper;
    private $userHelper;
    /** @var TagAwareCacheInterface */
    private $cache;

    public function __construct(
        EntityManagerInterface $em,
        Security $security,
        FormHelper $formHelper,
        UserHelper $userHelper,
        TagAwareCacheInterface $cache
    ) {
        $this->em = $em;
        $this->security = $security;
        $this->formHelper = $formHelper;
        $this->userHelper = $userHelper;
        $this->cache = $cache;
    }

    public function getUserGroupList(): JsonResponse
    {
        // Забираем пользователя
        /** @var User $user */
        $user = $this->security->getUser();

        // Забираем права на группы для того, чтобы понять нужно ли показывать все группы
        $permissions = $this->userHelper->getUserPermissionsByField($user, 'userGroupModuleAdmin');

        $isAdmin = ($permissions['isAdmin'] && $permissions['userGroupModuleAdmin']['read'] == 2);


        return $this->cache->get(
            "getUserGroupList_{$user->getId()}_{$isAdmin}",
            function (ItemInterface $item) use ($user, $isAdmin) {
                /** @var UserGroupRepository $repository */
                $repository = $this->em->getRepository(UserGroup::class);

                if ($isAdmin) {
                    $userGroupsWithoutFolder = $repository->findByGroupedWithoutFolder();
                } else {
                    // Забираем группы, к которым у пользователя есть доступ
                    $userGroupsWithoutFolder = $repository->findByGroupedWithoutFolderByUser($user);
                }

                $data['folders'][] = [
                    'folder' => [
                        'id' => 0,
                        'name' => 'Без папки',
                    ],
                    'groupArray' => $this->parseToGroupArray($userGroupsWithoutFolder),
                    'childrenFolders' => [],
                ];

                if ($isAdmin) {
                    // Собираем папки верхнего уровня
                    $folders = $this->em->getRepository(UserGroupFolder::class)->findAllFoldersWithoutParent();
                } else {
                    // Собираем папки верхнего уровня, к которым у пользователя есть доступ
                    $folders = $this->em->getRepository(UserGroupFolder::class)->findAllFoldersWithoutParentByUser(
                        $user
                    );
                }
                /** @var UserGroupFolder $folder */
                foreach ($folders as $folder) {
                    $childrenFolders = $this->childrenBuild($folder, $repository, $isAdmin);

                    $data['folders'][] = [
                        'folder' => [
                            'id' => $folder->getId(),
                            'name' => $folder->getName(),
                        ],
                        'groupArray' => $this->parseToGroupArray(
                            $repository->findBy(['groupFolder' => $folder])
                        ),
                        'childrenFolders' => $childrenFolders,
                    ];
                }

                $item->expiresAfter(3600);
                $item->tag('getUserGroupList');

                return $this->formHelper->getResponse('getUserGroupList', $data);
            }
        );
    }

    public function childrenBuild(UserGroupFolder $folder, UserGroupRepository $repository, bool $isAdmin)
    {
        $childrenFolders = [];
        $children = [];
        $children = $folder->getChildren();

        foreach ($children as $child) {
            $childrenFolders[] = [
                'folder' => [
                    'id' => $child->getId(),
                    'name' => $child->getName(),
                ],
                'groupArray' => $this->parseToGroupArray(
                    $repository->findBy(['groupFolder' => $child])
                ),
                'childrenFolders' => $this->childrenBuild($child, $repository, $isAdmin),
            ];
        }

        return $childrenFolders;
    }

    public function parseToGroupArray($groups)
    {
        $groupArray = [];

        if (!$groups) {
            return $groupArray;
        }

        foreach ($groups as $group) {
            $groupArray[] = [
                'id' => $group->getId(),
                'name' => $group->getName(),
                'count' => $this->em->getRepository(UserGroup::class)->findByGroupCountOfUsers($group),
            ];
        }

        return $groupArray;
    }

    public function editUserGroup(Request $request, UserGroup $userGroup): JsonResponse
    {
        $userGroupArray = $request->request->get('userGroup', []);

        $userGroupEntity = $this->setGroupFromArray($userGroup, $userGroupArray);

        $this->em->persist($userGroupEntity);
        $this->em->flush();

        return $this->formHelper->getResponse('editUserGroup', ['id' => $userGroupEntity->getId()]);
    }

    public function deleteUserGroup(UserGroup $userGroup)
    {
        $id = $userGroup->getId();

        $userGroup = $this->removeUserGroup($userGroup);

        $this->em->persist($userGroup);
        $this->em->remove($userGroup);
        $this->em->flush();

        return $this->formHelper->getResponse('deleteUserGroup', ['id' => $id]);
    }

    public function setGroupFromArray(UserGroup $userGroupEntity, array $group)
    {
        foreach ($group as $item => $value) {
            switch ($item) {
                case 'name':
                    $userGroupEntity->setName($this->formHelper->toRealStr($value));
                    break;
                case 'folderId':
                    $userGroupEntity->setGroupFolder(
                        $this->em->getRepository(UserGroupFolder::class)->find($value)
                    );
                    break;
                case 'permissions':
                    $permissionEntity = $this->setUserGroupPermissionFromArray(
                        $userGroupEntity->getGroupPermissions() ?? new GroupPermission(),
                        $value
                    );

                    $this->em->persist($permissionEntity);

                    $userGroupEntity->setGroupPermissions($permissionEntity);
                    break;
                case 'courses':
                    foreach ($value as $key => $courseId) {
                        $userGroupEntity->addCourse(
                            $this->em->getRepository(Course::class)->findOneBy(['id' => $courseId])
                        );
                    }
                    break;
                case 'courseFolders':
                    foreach ($value as $key => $courseFolderId) {
                        $userGroupEntity->addCourseFolder(
                            $this->em->getRepository(CourseFolder::class)->findOneBy(['id' => $courseFolderId])
                        );
                    }
                    break;
            }
        }

        return $userGroupEntity;
    }

    public function setUserGroupPermissionFromArray(GroupPermission $userGroupPermissionEntity, array $permission)
    {
        foreach ($permission as $item => $value) {
            switch ($item) {
                case 'isAdmin':
                    $userGroupPermissionEntity->setIsAdmin($this->formHelper->toRealStr($value));
                    break;
                case 'folderModuleAdmin':
                    $userGroupPermissionEntity->setFolderModuleAdmin($this->combineArrayToString($value));
                    break;
                case 'courseModuleAdmin':
                    $userGroupPermissionEntity->setCourseModuleAdmin($this->combineArrayToString($value));
                    break;
                case 'lessonModuleAdmin':
                    $userGroupPermissionEntity->setLessonModuleAdmin($this->combineArrayToString($value));
                    break;
                case 'taskAnswerCheckAdmin':
                    $userGroupPermissionEntity->setTaskAnswerCheckAdmin($this->combineArrayToString($value));
                    break;
                case 'userGroupModuleAdmin':
                    $userGroupPermissionEntity->setUserGroupModuleAdmin($this->combineArrayToString($value));
                    break;
                case 'userModuleAdmin':
                    $userGroupPermissionEntity->setUserModuleAdmin($this->combineArrayToString($value));
                    break;
                case 'analyticsModelAdmin':
                    $userGroupPermissionEntity->setAnalyticsModelAdmin($this->combineArrayToString($value));
                    break;
                case 'userTeamModuleAdmin':
                    $userGroupPermissionEntity->setUserTeamModuleAdmin($this->combineArrayToString($value));
                    break;
            }
        }

        return $userGroupPermissionEntity;
    }

    public function combineArrayToString(array $arr): string
    {
        return $arr['create'] . $arr['read'] . $arr['update'] . $arr['delete'];
    }

    public function addUsersToGroup(Request $request, UserGroup $userGroup): JsonResponse
    {
        // проверяем на добавление в группу факультета
        if (!$userGroup->getFaculties()->isEmpty()) {
            return $this->formHelper
                ->addError('group', 'Нельзя добавить в группу факультета напрямую, используй модуль Команды')
                ->getResponse('addUsersToGroup');
        }

        // @todo Переделать на множественное добавление
        $userId = $request->request->get('users')[0];
        /** @var User $user */
        $user = $this->em->getRepository(User::class)->find($userId);

        if ($courseStream = $userGroup->getCourseStream()) {
            // проверяем на добавление в другие группы потока
            foreach ($courseStream->getUserGroups() as $courseStreamGroup) {
                if ($this->addedToGroup($courseStreamGroup, $user)) {
                    return $this->formHelper
                        ->addError(
                            'group',
                            sprintf(
                                "Ученик уже в другой группе потока – «%s»",
                                $courseStreamGroup->getName()
                            )
                        )
                        ->getResponse('addUsersToGroup');
                }

                // проверяем на добавление в группы альтернативного потока
                if (
                    ($altGroup = $courseStreamGroup->getAlternativeGroup())
                    && $this->addedToGroup($altGroup, $user)
                ) {
                    return $this->formHelper
                        ->addError(
                            'group',
                            sprintf(
                                "Ученик уже в другой группе альтернативного потока – «%s»",
                                $altGroup->getName()
                            )
                        )
                        ->getResponse('addUsersToGroup');
                }
            }
        }

        /** @var User $currentUser */
        $currentUser = $this->security->getUser();
        $userGroup->addUser($user, $currentUser);

        $this->em->persist($userGroup);
        $this->em->flush();

        return $this->formHelper->getResponse('addUsersToGroup', ['id' => $userGroup->getId()]);
    }

    public function removeUsersFromGroup(Request $request, UserGroup $userGroup): JsonResponse
    {
        $users = $request->request->get('users', []);

        foreach ($users as $value) {
            // @todo: переделать на множественность: removeUsers(User[] $users) (c) Русанов

            /** @var User $user */
            $user = $this->em->getRepository(User::class)->find($value);
            $userGroup->removeUser($user);
            $this->em->persist($userGroup);


            // @todo: скорее всего не актуальная херня
            // удаляем из команд этой группы
            /*foreach ($user->getTeams() as $team) {
                if ($team->getUserGroup() === $userGroup) {
                    $team->removeUser($user);
                    $this->em->persist($team);
               }
            }*/
        }
        $this->em->flush();

        return $this->formHelper->getResponse('removeUsersFromGroup', ['id' => $userGroup->getId()]);
    }

    /**
     * @param Request $request
     * @param UserGroupFolder|null $groupFolder
     *
     * @return JsonResponse
     */
    public function addOrEditGroupFolder(Request $request, ?UserGroupFolder $groupFolder = null)
    {
        $action = 'editGroupFolder';

        $groupFolderData = $request->get('groupFolder', []);

        if (!$groupFolder) {
            $action = 'newGroupFolder';
            $groupFolder = new UserGroupFolder();
        }

        foreach ($groupFolderData as $key => $value) {
            switch ($key) {
                case 'name':
                    $groupFolder->setName($this->formHelper->toRealStr($value, 255));
                    break;
                case 'parentId':
                    /** @var UserGroupFolder $parentFolder */
                    $parentFolder = $this->em->getRepository(UserGroupFolder::class)
                        ->find($this->formHelper->toInt($value));
                    $groupFolder->setParent($parentFolder);
                    break;
            }
        }

        $this->em->persist($groupFolder);
        $this->em->flush();

        $data = [
            'groupFolderId' => $groupFolder->getId(),
        ];

        return $this->formHelper->getResponse(
            $action,
            $data
        );
    }

    /**
     * @param UserGroupFolder|null $groupFolder
     *
     * @return JsonResponse
     */
    public function deleteGroupFolder(UserGroupFolder $groupFolder)
    {
        $action = 'deleteGroupFolder';

        $data = [
            'groupFolderId' => $groupFolder->getId(),
        ];

        $this->removeChildGroupFolder($groupFolder);

        foreach ($groupFolder->getUserGroups() as $userGroup) {
            $this->removeUserGroup($userGroup);
        }

        $this->em->remove($groupFolder);
        $this->em->flush();

        return $this->formHelper->getResponse(
            $action,
            $data
        );
    }

    public function removeUserGroup(UserGroup $userGroup)
    {
        // удаляем права, если есть
        if ($groupPermission = $userGroup->getGroupPermissions()) {
            $this->em->remove($groupPermission);
            $this->em->flush();
        }

        foreach ($userGroup->getCourseFolders() as $courseFolder) {
            $userGroup->removeCourseFolder($courseFolder);
        }

        foreach ($userGroup->getCourses() as $course) {
            $userGroup->removeCourse($course);
        }

        return $userGroup;
    }

    public function removeChildGroupFolder(UserGroupFolder $groupFolder)
    {
        foreach ($groupFolder->getChildren() as $childFolder) {
            $this->removeChildGroupFolder($childFolder);
            foreach ($childFolder->getUserGroups() as $userGroup) {
                $this->removeUserGroup($userGroup);
            }
            $this->em->remove($childFolder);
            $this->em->flush();
        }
    }

    public function addedToGroup(UserGroup $userGroup, User $user): bool
    {
        /** @var null|UserGroupUser $relation */
        $relation = $this->em->getRepository(UserGroupUser::class)->findOneBy(
            [
                'userGroup' => $userGroup,
                'user' => $user,
            ]
        );

        return ($relation && !$relation->isDeleted());
    }

}

<?php

namespace App\Service;

use Doctrine\DBAL\DriverManager;
use Psr\Container\ContainerInterface;

class LikecentreDatabaseHelper
{
    private $connection;

    public function __construct(ContainerInterface $container)
    {
        $this->connection = DriverManager::getConnection(
            [
                'dbname' => $container->getParameter('DB_LC_DATABASE'),
                'user' => $container->getParameter('DB_LC_USER'),
                'password' => $container->getParameter('DB_LC_PASS'),
                'host' => $container->getParameter('DB_LC_HOST'),
                'driverOptions' => [
                    1009 => $container->get('kernel')->getProjectDir() . '/config/DB_CA.pem',
                ],
                'driver' => 'pdo_mysql',
                'charset' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
            ]
        );
    }

    public function execute(string $sql): array
    {
        $statement = $this->connection->executeQuery($sql);

        return $statement->fetchAllAssociative();
    }

}

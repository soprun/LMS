<?php

namespace App\Service;

use App\Entity\User;
use Exception;
use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;

class LikeItHelper
{

    private $logger;
    private $client;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;

        $this->client = new Client([
            'base_uri' => 'https://likecentre.ru'
        ]);
    }

    /**
     * Получение городов по емейлу франчайзи. Вернет пустой массив, если не франчайзи.
     * @param User $user
     * @return array
     */
    public function getFranchiseeCities(User $user): array
    {
        if (!str_ends_with($user->getEmail(), '@likebz.ru')) {
            return [];
        }

        try {
            $request = $this->client->get('toolbox-lms/franchisee/find', [
                'query' => [
                    'email' => $user->getEmail()
                ]
            ]);
            return json_decode($request->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);
        } catch (Exception $e) {
            $this->logger->critical('LikeItHelper::getFranchiseeCities exception', [
                'uri' => (string) $e->getRequest()->getUri(),
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
            ]);
        }

        return [];
    }

    /**
     * Получение доступных городов для всяких фильтраций
     * @return array
     */
    public function getCities(): array
    {
        try {
            $request = $this->client->get('toolbox-lms/cities/get');
            return json_decode($request->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);
        } catch (Exception $e) {
            $this->logger->critical('LikeItHelper::getCities exception', [
                'uri' => (string) $e->getRequest()->getUri(),
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
            ]);
        }

        return [];
    }

}

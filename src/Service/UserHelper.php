<?php

namespace App\Service;

use App\Entity\AbstractCourse;
use App\Entity\Courses\Course;
use App\Entity\Courses\CourseFolder;
use App\Entity\CourseStream;
use App\Entity\GroupPermission;
use App\Entity\MarathonInvestment\UserNiche;
use App\Entity\Questionnaire\UserAnswer;
use App\Entity\SmartSender\SmartSenderUser;
use App\Entity\User;
use App\Entity\UserGroup;
use App\Entity\UserGroupAttendanceType;
use App\Entity\UserGroupUser;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class UserHelper
{
    private $em;
    private $security;
    private $formHelper;
    private $authServerHelper;
    /** @var LoggerInterface */
    private $logger;

    public function __construct(
        EntityManagerInterface $em,
        Security $security,
        FormHelper $formHelper,
        AuthServerHelper $authServerHelper,
        LoggerInterface $logger
    ) {
        $this->em = $em;
        $this->security = $security;
        $this->formHelper = $formHelper;
        $this->authServerHelper = $authServerHelper;
        $this->logger = $logger;
    }

    public function getUserInfo(): JsonResponse
    {
        /** @var User $user */
        $user = $this->security->getUser();
        $userArray = $this->getUserInfoArray($user);

        return $this->formHelper->getResponse('getUserInfo', ['user' => $userArray]);
    }

    public function getUserInfoArray(User $user): array
    {
        // доступность трекшенов
        $isSpeedCourse = false;
        $isHundredCourse = false;
        $isMSACourse = false;
        $isMarathonInvestment = false;
        $marathonInvestmentCaptain = false;
        $isFastCashCourse = false;

        // необходимость регистрации для марофона инвестиций
        $needRegisterMarathonInvestment = false;

        // доступность анкеты и необходимость ее заполнения
        $stream = $this->em->getRepository(CourseStream::class)->getCurrentStream($user);
        $questionnaireAvailable = false;
        $redirectToQuestionnaire = false;
        if ($stream) {
            $questionnaireAvailable = true;

            $wasInPreviousStreams = false;
            $previousStream = $this->em->getRepository(CourseStream::class)->getPreviousStream($stream);
            if ($previousStream) {
                $previousStreams = [$previousStream];
                if ($previousStream->getAlternativeCourseStream()) {
                    $previousStreams[] = $previousStream->getAlternativeCourseStream();
                }
                $wasInPreviousStreams = $this->em->getRepository(UserGroupUser::class)
                    ->checkUserInStream($user, $previousStreams);
            }

            $answersCount = $this->em->getRepository(UserAnswer::class)
                ->count(['courseStream' => $stream, 'user' => $user]);
            $redirectToQuestionnaire = !$answersCount && !$wasInPreviousStreams;
        }
        // доступность бота и необходимость его подключения
        $telegramAvailable = (bool)$stream;
        $needTelegram = false;

        // доступ к модулю Команды
        $teamsAvailable = false;

        $groups = $this->em->getRepository(UserGroup::class)->getUserGroups($user);
        foreach ($groups as $group) {
            $groupName = $group['name'];

            switch ($group['courseSlug']) {
                case AbstractCourse::SLUG_SPEED:
                case AbstractCourse::SLUG_SPEED_CLUB:
                    $isSpeedCourse = true;
                    break;

                case AbstractCourse::SLUG_FAST_CASH:
                    $isFastCashCourse = true;
                    break;

                case AbstractCourse::SLUG_HUNDRED:
                    $isHundredCourse = true;
                    break;

                case AbstractCourse::SLUG_MARATHON_INVESTMENT:
                    if ($group['activeStream']) {
                        $isMarathonInvestment = true;
                    }

                    if (strripos($group['name'], 'Капитаны')) {
                        $marathonInvestmentCaptain = true;
                    }
                    break;
            }

            if ((int)$group['teamFoldersCount'] > 0) {
                $teamsAvailable = true;
            }

            if (str_starts_with($groupName, 'Новый МСА')) {
                $isMSACourse = true;
            }
        }

        if ($telegramAvailable) {
            $smartSenderUser = $this->em->getRepository(SmartSenderUser::class)
                ->findByUserAndActualStream($user);

            if (!$smartSenderUser || !$smartSenderUser->getSmartSenderUserId()) {
                $needTelegram = true;
            }
        }

        /**
         * Партнерам разрешаем доступ к Командам всегда. Не вынес в пермишены,
         * потому что тогда сработает isAdmin и сломает кучу функционала партнера.
         * Можно будет поменять, когда поменяем работу пермишенов.
         */
        if (!$teamsAvailable && $this->security->isGranted("ROLE_FRANCHISEE")) {
            $teamsAvailable = true;
        }

        // Если есть ниши, то переходим в трекшен
        if ($isMarathonInvestment) {
            /** @var QueryBuilder $qb */
            $qb = $this->em->getRepository(UserNiche::class)->createQueryBuilder('un');
            $niches = $qb
                ->select('count(un.id)')
                ->innerJoin('un.stream', 'cs')
                ->innerJoin('cs.abstractCourse', 'ac')
                ->andWhere('ac.slug = :slug')
                ->andWhere('un.user = :user')
                ->andWhere('cs.active = 1')
                ->setParameters(
                    [
                        'slug' => AbstractCourse::SLUG_MARATHON_INVESTMENT,
                        'user' => $user,
                    ]
                )
                ->getQuery()
                ->getSingleScalarResult();

            if ((int)$niches === 0) {
                $needRegisterMarathonInvestment = true;
            }

            if ($marathonInvestmentCaptain) {
                $needRegisterMarathonInvestment = false;
            }
        }

        $permissions = $this->em->getRepository(GroupPermission::class)
            ->findByUserAndFieldAllPermissionsMenu($user);

        // доступ к странице "Аналитика по командам"
        $teamWorker = $this->security->isGranted('TEAMS_ACCESS');

        return [
            'id' => $user->getId(),
            'email' => $user->getEmail(),
            'timezone' => optional($user->getTimezone())->getId(),
            'name' => $user->getName(),
            'lastname' => $user->getLastname(),
            'patronymic' => $user->getPatronymic(),
            'country' => $user->getCountry(),
            'city' => $user->getCity(),
            'phone' => $user->getPhone(),
            'avatar' => $user->getAvatar(),
            'dateOfBirth' => $user->getDateOfBirth(),
            'vkLink' => $user->getVkLink(),
            'fbLink' => $user->getFbLink(),
            'instaLink' => $user->getInstaLink(),
            'siteLink' => $user->getSiteLink(),
            'telegram' => $user->getTelegram(),
            'businessInfo' => $user->getBusinessInfo(),
            'niche' => $user->getNiche(),
            'businessType' => $user->getBusinessType(),
            'businessLocationTypeId' => $user->getBusinessLocationTypeId(),
            'businessCity' => $user->getBusinessCity(),
            'businessCategory' => $user->getBusinessCategory() ?: '',
            'businessCategoryId' => $user->getBusinessCategoryId() ?: '',
            'profitPerWeek' => $user->getProfitPerWeek(),
            'profitPerYear' => $user->getProfitPerYear(),
            'profitPerLastYear' => $user->getProfitPerLastYear(),
            'franchises' => $user->getFranchises(),
            'company' => $user->getCompany(),
            'isAdmin' => $this->em->getRepository(GroupPermission::class)->findByUserIsAdmin($user),
            'isPasswordEmpty' => $user->getIsPasswordEmpty(),
            'teams' => $teamsAvailable,
            'permissions' => $this->getTopOfPresentPermissions($permissions),
            'teamWorker' => $teamWorker,
            'isSpeedCourse' => $isSpeedCourse,
            'isMSACourse' => $isMSACourse,
            'isHundredCourse' => $isHundredCourse,
            'isMarathonInvestment' => $isMarathonInvestment,
            'isFastCashCourse' => $isFastCashCourse,
            'needRegisterMarathonInvestment' => $needRegisterMarathonInvestment,
            'questionnaireAvailable' => $questionnaireAvailable,
            'redirectToQuestionnaire' => $redirectToQuestionnaire,
            'needTelegram' => $needTelegram,
        ];
    }

    /**
     * Получение инфы о пользователях с authServer
     *
     * @param array $userIds
     * @param string|null $search
     * @param string|null $page
     *
     * @return array
     */
    public function getUserInfoArrayByIds(array $userIds, ?string $search = null, ?string $page = null)
    {
        $users = [];
        $authUsers = $this->authServerHelper->getUsersInfo($userIds, $search, $page);
        foreach ($authUsers as $authUser) {
            /** todo: start temporary fix почему-то есть юзеры, у которых в аутхе не заполнен lmsUserId */
            if (empty($authUser->lmsUserId)) {
                $this->logger->critical('Нет lms_user_id в аутхе у пользователя ' . $authUser->id);
                $lmsUser = $this->em->getRepository(User::class)->findOneBy(['authUserId' => $authUser->id]);
                if ($lmsUser) {
                    $authUser->lmsUserId = $lmsUser->getId();
                } else {
                    $this->logger->critical('Пользователь с authUserId ' . $authUser->id . ' не найден');
                }
            }
            /** todo: end temporary fix */

            $users[] = [
                'id' => $authUser->lmsUserId,
                'authUserId' => $authUser->id,
                'email' => $authUser->email,
                'name' => $authUser->name,
                'lastname' => $authUser->lastname,
                'patronymic' => $authUser->patronymic,
                'avatar' => $authUser->avatar,
                'country' => $authUser->country,
                'city' => $authUser->city,
                'phone' => $authUser->phone,
                'dateOfBirth' => $authUser->dateOfBirth,
                'vkLink' => $authUser->vkLink,
                'fbLink' => $authUser->fbLink,
                'instaLink' => $authUser->instaLink,
                'siteLink' => $authUser->siteLink,
                'businessInfo' => $authUser->businessInfo,
                'niche' => $authUser->niche,
                'businessType' => $authUser->businessType,
                'businessLocationTypeId' => $authUser->businessLocationTypeId,
                'businessCity' => $authUser->businessCity,
                'businessCategory' => $authUser->businessCategory,
                'businessCategoryId' => $authUser->businessCategoryId,
                'profitPerWeek' => $authUser->profitPerWeek,
                'profitPerYear' => $authUser->profitPerYear,
                'profitPerLastYear' => $authUser->profitPerLastYear,
                'franchises' => $authUser->franchises,
            ];
        }

        return $users;
    }

    /**
     * Получить права по конкретному модулю
     */
    public function getUserPermissionsByField(User $user, string $field)
    {
        $permissions = $this->em->getRepository(GroupPermission::class)->findByUserAndFieldAllPermissions(
            $user,
            $field
        );

        return $this->getTopOfPresentPermissions($permissions);
    }

    private $isAdmin;

    /**
     * Получить права по user
     */
    public function isUserAdmin()
    {
        if (is_null($this->isAdmin)) {
            /** @var User $user */
            $user = $this->security->getUser();
            $this->isAdmin = $this->em->getRepository(GroupPermission::class)->findByUserIsAdmin($user);
        }

        return $this->isAdmin;
    }

    /**
     * Получить права для конкретной папки курса
     */
    public function getCourseFolderPermissions(User $user, CourseFolder $courseFolder)
    {
        //Находим все суперправа, которые могут не иметь прямых отсылок на папки
        $superPermissions = $this->em->getRepository(GroupPermission::class)
            ->findByUserAndFieldSuperPermissions($user, 'folderModuleAdmin');

        //Находим права с отсылками на папку по списку
        $permissions = $this->em->getRepository(GroupPermission::class)
            ->findByUserAndCourseFolderPermissionForCourseFolder($user, $courseFolder);


        $permissions = array_merge_recursive(
            $superPermissions,
            $permissions
        );

        return $this->getTopOfPresentPermissions($permissions);
    }

    /**
     * Получить права для конкретной папки курса
     */
    public function getGroupIdPerAdmin(User $user, CourseFolder $courseFolder)
    {
        //Находим права с отсылками на папку по списку
        $groupId = $this->em->getRepository(GroupPermission::class)
            ->findByUserAndCourseFolderPermissionForCourse($user, $courseFolder);
        if (empty($groupId)) {
            return null;
        } else {
            return $groupId[0]['id'];
        }
    }

    /**
     * Получить права для конкретного курса
     */
    public function getCoursePermissions(User $user, Course $course)
    {
        //Находим все суперправа, которые могут не иметь прямых отсылок на папки
        $superPermissions = $this->em->getRepository(GroupPermission::class)
            ->findByUserAndFieldSuperPermissions($user, 'courseModuleAdmin');

        //Находим права с отсылками на курс по списку
        $permissions = $this->em->getRepository(GroupPermission::class)
            ->findByUserAndCoursePermissionForCourse($user, $course);

        $permissions = array_merge_recursive(
            $superPermissions,
            $permissions
        );

        return $this->getTopOfPresentPermissions($permissions);
    }

    /**
     * Получить права для конкретного урока
     */
    public function getLessonPermissionsByCourse(User $user, Course $course)
    {
        //Находим все суперправа, которые могут не иметь прямых отсылок на папки
        $superPermissions = $this->em->getRepository(GroupPermission::class)
            ->findByUserAndFieldSuperPermissions($user, 'lessonModuleAdmin');

        //Находим права с отсылками на курс по списку
        $permissions = $this->em->getRepository(GroupPermission::class)
            ->findByUserAndLessonPermissionForCourse($user, $course);

        $permissions = array_merge_recursive(
            $superPermissions,
            $permissions
        );

        return $this->getTopOfPresentPermissions($permissions);
    }

    /**
     * Получить права для группы
     */
    public function getPermissionsByGroup(UserGroup $userGroup)
    {
        //Находим все суперправа, которые могут не иметь прямых отсылок на папки
        $permissions = $this->em->getRepository(GroupPermission::class)
            ->findByGroupPermissions($userGroup);

        return $this->parsePermissions($permissions);
    }

    public function getTopOfPresentPermissions(array $permissions)
    {
        $isAdmin = false;
        $rules = [
            'create' => 0,
            'read' => 0,
            'update' => 0,
            'delete' => 0,
        ];

        $modules = [
            'folderModuleAdmin',
            'courseModuleAdmin',
            'lessonModuleAdmin',
            'taskAnswerCheckAdmin',
            'userModuleAdmin',
            'userGroupModuleAdmin',
            'userTeamModuleAdmin',
            'analyticsModelAdmin',
        ];

        $data = array_fill_keys($modules, $rules);

        foreach ($permissions as $permission) {
            $flag = $permission['isAdmin'];
            unset($permission['isAdmin']);

            if ($flag && $flag !== $isAdmin) {
                $isAdmin = true;
            }

            foreach ($permission as $index => $item) {
                if ($item == "") {
                    $item = null;
                }

                $module = &$data[$index];

                if ($item[0] > $module['create']) {
                    $module['create'] = intval($item[0]);
                }
                if ($item[1] > $module['read']) {
                    $module['read'] = intval($item[1]);
                }
                if ($item[2] > $module['update']) {
                    $module['update'] = intval($item[2]);
                }
                if ($item[3] > $module['delete']) {
                    $module['delete'] = intval($item[3]);
                }
            }
        }
        $data['isAdmin'] = $isAdmin;

        return $data;
    }

    public function parsePermissions(array $permission)
    {
        if (!$permission) {
            return ['isAdmin' => false];
        }


        $data = [
            'id' => $permission['id'],
            'isAdmin' => $permission['isAdmin'] ? true : false,
        ];

        unset($permission['id']);
        unset($permission['isAdmin']);

        if (key_exists('createdAt', $permission)) {
            unset($permission['createdAt']);
        }
        if (key_exists('updatedAt', $permission)) {
            unset($permission['updatedAt']);
        }

        foreach ($permission as $index => $item) {
            if ($item == "") {
                $item = null;
            }

            $create = key_exists($index, $data) ? key_exists('create', $data[$index]) ? $data[$index]['create'] : 0 : 0;
            $read = key_exists($index, $data) ? key_exists('read', $data[$index]) ? $data[$index]['read'] : 0 : 0;
            $update = key_exists($index, $data) ? key_exists('update', $data[$index]) ? $data[$index]['update'] : 0 : 0;
            $delete = key_exists($index, $data) ? key_exists('delete', $data[$index]) ? $data[$index]['delete'] : 0 : 0;

            $data[$index] = [
                'create' => $create > $item[0] ? $create : $item[0],
                'read' => $read > $item[1] ? $read : $item[1],
                'update' => $update > $item[2] ? $update : $item[2],
                'delete' => $delete > $item[3] ? $delete : $item[3],
            ];
        }

        return $data;
    }

    /**
     * Изменение статуса присутствия пользователя на курсе: онлайн или офлайн
     * Изначально метод использовался для игры франчайзи, которая была выпилена этим коммитом,
     * но получилось переиспользуемо, поэтому пока оставлю
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function setUserGroupAttendanceType(Request $request): JsonResponse
    {
        /** @var User $user */
        $user = $this->security->getUser();

        $typeId = $request->request->get('typeId');
        if (!$typeId || !in_array($typeId, [1, 2])) {
            $this->formHelper->addError('typeId', 'Тип присутствия может быть только 1 или 2');

            return $this->formHelper->getResponse('setUserGroupAttendanceType');
        }

        $group = $this->em->getRepository(UserGroup::class)->find(
            $request->get('groupId')
        );

        // ищем предыдущий ответ
        $userGroupAttendanceType = $this->em->getRepository(UserGroupAttendanceType::class)->findOneBy(
            [
                'user' => $user,
                'userGroup' => $group,
            ]
        );

        if (!$userGroupAttendanceType) {
            $userGroupAttendanceType = new UserGroupAttendanceType();
            $userGroupAttendanceType->setUser($user);
            $userGroupAttendanceType->setUserGroup($group);
        }
        $userGroupAttendanceType->setTypeId($typeId);
        $this->em->persist($userGroupAttendanceType);
        $this->em->flush();

        return $this->formHelper->getResponse('setUserGroupAttendanceType');
    }

}

<?php

namespace App\Service;

use App\Entity\SmartSender\SmartSenderBot;
use App\Entity\SmartSender\SmartSenderUser;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class SmartSenderService
{
    /** @var EntityManagerInterface */
    private $em;
    /** @var FormHelper */
    private $formHelper;
    /** @var AuthServerHelper */
    private $authServer;

    public function __construct(
        EntityManagerInterface $em,
        FormHelper $formHelper,
        AuthServerHelper $authServer
    ) {
        $this->em = $em;
        $this->formHelper = $formHelper;
        $this->authServer = $authServer;
    }

    /**
     * @throws Exception
     */
    public function getConnectedUserWithCode(User $user): SmartSenderUser
    {
        $smartSenderUser = $this->em->getRepository(SmartSenderUser::class)
            ->findByUserAndActualStream($user);

        if (!$smartSenderUser) {
            // ищем актуального бота
            $smartSenderBot = $this->em->getRepository(SmartSenderBot::class)->findBot($user);
            if (!$smartSenderBot) {
                throw new BadRequestHttpException('Бот не найден. Пожалуйста, обратись в поддержку');
            }

            $smartSenderUser = new SmartSenderUser();
            $smartSenderUser->setUser($user);
            $smartSenderUser->setSmartSenderBot($smartSenderBot);
            $smartSenderUser->setCode(random_int(10, 99) . $user->getId());
            $this->em->persist($smartSenderUser);
            $this->em->flush();
        }

        return $smartSenderUser;
    }

    /**
     * @throws GuzzleException
     */
    public function updateUsername(User $user, string $username): void
    {
        $smartSenderUser = $this->em->getRepository(SmartSenderUser::class)
            ->findByUserAndActualStream($user);

        if (!$smartSenderUser) {
            throw new BadRequestHttpException('Пользователь бота ней найден. Пожалуйста, обратись в поддержку');
        }

        $userArray = [
            'authUserId' => $user->getAuthUserId(),
            'telegram' => $this->formHelper->toRealStr($username),
            'courseName' => 'withoutEmail',
        ];

        $this->authServer->editUserByServer($userArray);
    }

}

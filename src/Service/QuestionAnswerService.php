<?php

namespace App\Service;

use App\Entity\Questionnaire\UserAnswer;
use App\Entity\User;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Throwable;

class QuestionAnswerService
{
    /** @var array<string,int> */
    private const BUSINESS_SPHERE_CATEGORY_IDS = [
        'Красота' => 6,
        'Спорт' => 5,
        'Производство' => 12,
        'Консалтинг' => 16,
        'Маркетинг' => 17,
        'Строительство и ремонт' => 7,
        'Продажа товаров' => 3,
        'Продажа услуг' => 18,
        'Общепит' => 4,
        'Образование' => 8,

    ];
    /** @var AuthServerService */
    private $authServer;

    public function __construct(AuthServerService $authServer)
    {
        $this->authServer = $authServer;
    }

    /**
     * @throws Throwable
     */
    public function saveDataInAuthUser(User $user, UserAnswer $answer): void
    {
        switch ($answer->getQuestion()->getSlug()) {
            case 'niche':
                if ($answer->getText() === null) {
                    throw new BadRequestHttpException('Ответ на вопрос должен быть текстом');
                }
                $niche = $answer->getText();
                $this->authServer->editUserInfo($user->getAuthUserId(), ['niche' => $niche]);
                break;
            case 'business_sphere':
                if ($answer->getVariant() === null) {
                    throw new BadRequestHttpException('Этот вопрос должен содержать варианты ответов');
                }
                $bussinesCategoryId = self::BUSINESS_SPHERE_CATEGORY_IDS[$answer->getVariant()->getTitle()];
                $this->authServer->editUserInfo($user->getAuthUserId(), ['businessCategoryId' => $bussinesCategoryId]);
                break;
        }
    }
}

<?php

declare(strict_types=1);

namespace App\Service\AmoCRM;

use App\Repository\AmoCallbackRepository;
use App\Service\AmoHelper;
use Doctrine\ORM\NonUniqueResultException;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Throwable;
use UnexpectedValueException;

/**
 * @property LoggerInterface|NullLogger $logger
 */
final class AmoCRMCallbackService implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var AmoHelper
     */
    private $amoHelper;
    /**
     * @var AmoCallbackRepository
     */
    private $amoCallbackRepository;

    public function __construct(AmoHelper $amoHelper, AmoCallbackRepository $amoCallbackRepository)
    {
        $this->amoHelper = $amoHelper;
        $this->amoCallbackRepository = $amoCallbackRepository;
        $this->logger = new NullLogger();
    }

    /**
     * @psalm-param array{id: string, status_id: string, pipeline_id: string} $callback
     * @psalm-var non-empty-list<string>|null $callback
     * @psalm-assert string[] $callback
     * @throws NonUniqueResultException
     */
    public function execute(array $callback): void
    {
        $leadId = (int)$callback['id'] ?? null;
        $statusId = filter_var($callback['status_id'] ?? null, FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
        $pipelineId = filter_var($callback['pipeline_id'] ?? null, FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);

        $lead = $this->amoHelper->getLeadById($leadId);

        if (empty($lead)) {
            $this->logger->error('{method}: an error has occurred, lead ID #{leadId} is not found!', [
                'method' => __METHOD__,
                'leadId' => $leadId,
                'statusId' => $statusId,
                'pipelineId' => $pipelineId,
            ]);

            // TODO: Возможно вместо ошибки должен быть return!
            // throw new InvalidArgumentException('An error has occurred, lead is not found!');
            return;
        }

        $tags = $this->getTags($lead);

        $items = $this->amoCallbackRepository->findByCriteria(
            $statusId,
            $pipelineId
        );

        $this->logger->info('{method}: lead ID #{leadId}', [
            'method' => __METHOD__,
            'leadId' => $leadId,
            'leadTags' => $tags,
            'statusId' => $statusId,
            'pipelineId' => $pipelineId,
        ]);

        try {
            foreach ($items as $item) {
                if (count($item->getTags()) !== 0) {
                    foreach ($item->getTags() as $tag) {
                        if (in_array($tag, $tags, true) === true) {
                            if ($item->toRemove()) {
                                $this->amoHelper->removeAccessFromCourseByLeadId(
                                    $leadId
                                );
                            } else {
                                $this->amoHelper->giveAccessToCourseByLeadId(
                                    $leadId,
                                    $item->getCourseName(),
                                    $item->getGroupId()
                                );
                            }
                        }
                    }
                } elseif ($item->toRemove()) {
                    $this->amoHelper->removeAccessFromCourseByLeadId(
                        $leadId
                    );
                } else {
                    $this->amoHelper->giveAccessToCourseByLeadId(
                        $leadId,
                        $item->getCourseName(),
                        $item->getGroupId()
                    );
                }
            }
        } catch (Throwable $exception) {
            $this->logger->info('{method}: Exception: {message}, lead ID #{leadId}', [
                'method' => __METHOD__,
                'leadId' => $leadId,
                'message' => $exception->getMessage(),
                'trace' => $exception->getTrace(),
            ]);

            throw new UnexpectedValueException($exception->getMessage(), (int)$exception->getCode(), $exception);
        }
    }

    private function getTags(array $lead): array
    {
        if (!isset($lead['tags'])) {
            return [];
        }

        return array_map(
            static function ($tag): string {
                return mb_strtolower($tag['name']);
            },
            $lead['tags']
        );
    }

}

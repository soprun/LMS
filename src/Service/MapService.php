<?php

namespace App\Service;

use App\DTO\UserLocationDto;
use App\Entity\User;
use CrEOF\Spatial\PHP\Types\Geography\Point;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

final class MapService
{
    private $em;
    private $authServerService;
    private $requestStack;

    public function __construct(
        EntityManagerInterface $em,
        AuthServerService $authServerService,
        RequestStack $requestStack
    ) {
        $this->em = $em;
        $this->authServerService = $authServerService;
        $this->requestStack = $requestStack;
    }

    public function setUserLocation(User $user, Point $point): User
    {
        $user->setLocation($point);
        $this->em->flush();

        return $user;
    }

    public function getClosestUsersToThePoint(UserLocationDto $userLocationDto): array
    {
        $users = $this->em->getRepository(User::class)->getClosestUsersToThePoint($userLocationDto);

        $this->authServerService->setUserAccessToken(
            $this->requestStack->getCurrentRequest()->headers->get('Authorization')
                ?: 'Bearer ' . $this->requestStack->getCurrentRequest()->cookies->get('accessToken')
        );

        $users = collect($users)->groupBy(function ($i) {
            return $i['lon'] . ' ' . $i['lat'];
        })->map(function ($items) {
            $count = count($items);
            $result = [
                'count' => $count,
                'lon' => $items[0]['lon'],
                'lat' => $items[0]['lat'],
            ];

            if ($count === 1) {
                $result['user'] = $this->getUserInfo($items[0][0]);
            }

            return $result;
        })->values()->toArray();

        return $users;
    }

    public function getUsersAtPoint(UserLocationDto $mapDto): array
    {
        $users = $this->em->getRepository(User::class)->getUsersAtPoint($mapDto);

        return array_map(function ($item) {
            return $this->getUserInfo($item);
        }, $users);
    }

    private function getUserInfo(User $user): array
    {
        try {
            $info = $this->authServerService->getInternalUsers([
                'ids' => [$user->getAuthUserId()],
            ])['list'][$user->getAuthUserId()];
            $info['niche'] = optional($user->getNiches()->first())->getTitle() ?? '';

            return $info;
        } catch (\Exception $exception) {
            return [];
        }
    }

}

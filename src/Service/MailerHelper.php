<?php

namespace App\Service;

use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class MailerHelper
{

    private $mailer;
    private $logger;
    private $speedMailFrom;

    public function __construct(
        MailerInterface $mailer,
        LoggerInterface $logger,
        string $_speedMailFrom
    ) {
        $this->mailer = $mailer;
        $this->logger = $logger;
        $this->speedMailFrom = $_speedMailFrom;
    }

    public function sendPointsShop(string $userEmail, int $points, string $url): void
    {
        $email = (new TemplatedEmail())
            ->from(new Address($this->speedMailFrom, 'Курс Скорость'))
            ->to($userEmail)
            ->subject('Подарки участникам Скорости 12')
            ->htmlTemplate('email/pointsShop.html.twig')
            ->context([
                'points' => $points,
                'url' => $url
            ]);

        $email->getHeaders()->addTextHeader('X-Transport', 'speed');

        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {
            $this->logger->critical(
                'MailerHelper::sendPointsShop TransportException',
                [
                    'message' => $e->getMessage(),
                    'email' => $userEmail,
                ]
            );
        } catch (Exception $e) {
            $this->logger->critical(
                'MailerHelper::sendPointsShop Exception',
                [
                    'message' => $e->getMessage(),
                    'email' => $userEmail,
                ]
            );
        }
    }

}

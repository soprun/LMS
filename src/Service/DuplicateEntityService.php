<?php

namespace App\Service;

use App\Entity\BaseBlock\BaseBlock1;
use App\Entity\BaseBlock\BaseBlock2;
use App\Entity\BaseBlock\BaseBlock3;
use App\Entity\BaseBlock\BaseBlock4;
use App\Entity\BaseBlock\BaseBlock5;
use App\Entity\BaseBlock\BaseBlock6;
use App\Entity\BaseBlock\BaseBlock7;
use App\Entity\BaseBlock\BaseBlockNps;
use App\Entity\BaseBlock\BaseBlockTaskWithCheck;
use App\Entity\BaseBlock\BaseFiles;
use App\Entity\Courses\Course;
use App\Entity\Courses\CourseConfiguration;
use App\Entity\Courses\CourseFolder;
use App\Entity\Courses\Lesson;
use App\Entity\Courses\LessonConfiguration;
use App\Entity\Courses\LessonPart;
use App\Entity\Courses\LessonPartBlock;
use App\Entity\Courses\Module;
use App\Entity\Interfaces\TaskSetVarietyInterface;
use App\Entity\Interfaces\LessonContentBlockInterface;
use App\Entity\Task\TaskSet;
use App\Entity\Task\TaskTest;
use App\Entity\Task\TaskTestQuestion;
use App\Entity\Task\TaskType1;
use App\Entity\Task\TaskType5;
use App\Entity\User;
use App\Exception\DuplicateEntityServiceException;
use App\Message\LMS\Duplicate\CourseDuplicateMessage;
use App\Message\LMS\Duplicate\CourseFolderDuplicateMessage;
use App\Message\LMS\Duplicate\LessonDuplicateMessage;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\ConnectionException;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Transport\AmqpExt\AmqpStamp;
use Traversable;

class DuplicateEntityService
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var LessonContentService
     */
    protected $lessonContentService;

    private $bus;

    /**
     * @param EntityManagerInterface $em
     * @param LoggerInterface $logger
     * @param LessonContentService $lessonContentService
     */
    public function __construct(
        EntityManagerInterface $em,
        LoggerInterface $logger,
        LessonContentService $lessonContentService,
        MessageBusInterface $bus
    ) {
        $this->em = $em;
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        $this->logger = $logger;
        $this->lessonContentService = $lessonContentService;
        $this->bus = $bus;
    }

    /**
     * @param CourseFolder $courseFolder
     *
     * @return CourseFolder
     *
     * @throws DuplicateEntityServiceException|ConnectionException
     */
    public function duplicateFolder(CourseFolder $courseFolder, bool $withCourses = true): CourseFolder
    {
        $this->em->getConnection()->beginTransaction();
        try {
            $newCourseFolder = new CourseFolder();
            $newCourseFolder->setName($courseFolder->getName());
            $newCourseFolder->setDescription($courseFolder->getDescription());
            $newCourseFolder->setImgFile(
                $this->duplicateBaseFile(
                    $courseFolder->getImgFile()
                )
            );
            $newCourseFolder->setCoverImgFile(
                $this->duplicateBaseFile(
                    $courseFolder->getCoverImgFile()
                )
            );
            $newCourseFolder->addUserGroups($courseFolder->getUserGroups()->toArray());
            $newCourseFolder->setDeleted($courseFolder->isDeleted());
            $this->em->persist($newCourseFolder);
            $this->em->flush();
            if ($withCourses) {
                $courses = $this->em->getRepository(Course::class)->findBy([
                    'folder' => $courseFolder,
                ]);
                if (!empty($courses)) {
                    $this->duplicateCourses($courses, $newCourseFolder);
                }
            }
        } catch (Exception $e) {
            $this->em->getConnection()->rollBack();

            $this->logger->error('Не удалось продублировать папку курса', [
                'course_folder' => $courseFolder->getId(),
                'with_courses' => $withCourses,
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);

            throw new DuplicateEntityServiceException($e->getMessage());
        }
        $this->em->getConnection()->commit();

        return $newCourseFolder;
    }

    /**
     * @param Course[] $courses
     * @param null|CourseFolder $courseFolder = null
     * @param bool $withDeleted = false
     *
     * @return ArrayCollection
     *
     * @throws DuplicateEntityServiceException
     */
    public function duplicateCourses(
        array $courses,
        CourseFolder $courseFolder = null,
        bool $withDeleted = false
    ): ArrayCollection {
        try {
            $collection = new ArrayCollection();
            foreach ($courses as $course) {
                if (!$withDeleted && $course->isDeleted()) {
                    continue;
                }
                $newCourse = $this->duplicateCourse($course, $courseFolder);
                $collection->add($newCourse);
            }

            return $collection;
        } catch (Exception $e) {
            $this->logger->error('Не удалось продублировать курсы', [
                'courses' => array_map(static function ($course) {
                    return $course->getId();
                }, $courses),
                'course_folder' => $courseFolder ? $courseFolder->getId() : null,
                'with_deleted' => $withDeleted,
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);
            throw new DuplicateEntityServiceException($e->getMessage());
        }
    }

    /**
     * @param Course $course
     * @param null|CourseFolder $courseFolder
     * @param bool $withDeletedLessons
     *
     * @return Course
     *
     * @throws DuplicateEntityServiceException
     * @throws ConnectionException
     */
    public function duplicateCourse(
        Course $course,
        ?CourseFolder $courseFolder = null,
        bool $withDeletedLessons = false
    ): Course {
        $this->em->getConnection()->beginTransaction();
        try {
            $newCourse = new Course();
            $newCourse->setName($course->getName());
            if ($courseFolder && $courseFolder->getId()) {
                $newCourse->setFolder($courseFolder);
            } else {
                $newCourse->setFolder($course->getFolder());
            }
            $newCourse->setDeleted($course->isDeleted());
            $this->em->persist($newCourse);
            $this->em->flush();

            $newCourse->setDescription($course->getDescription());
            if ($course->getImgFile()) {
                $newCourse->setImgFile(
                    $this->duplicateBaseFile(
                        $course->getImgFile()
                    )
                );
            }
            if ($course->getCoverImgFile()) {
                $newCourse->setCoverImgFile(
                    $this->duplicateBaseFile(
                        $course->getCoverImgFile()
                    )
                );
            }
            if ($course->getCourseConfiguration()) {
                $courseConfiguration = $this->duplicateCourseConfiguration(
                    $course->getCourseConfiguration(),
                    $newCourse
                );
                if ($courseConfiguration) {
                    $newCourse->setCourseConfiguration($courseConfiguration);
                }
            }
            $lessons = $this->em->getRepository(Lesson::class)->findByCourse($course, $withDeletedLessons);
            if (!empty($lessons)) {
                $newCourse->addLessons($this->duplicateLessons($lessons, $newCourse));
            }
            $this->em->persist($newCourse);
            $this->em->flush();
        } catch (Exception $e) {
            $this->em->getConnection()->rollBack();

            $this->logger->error('Не удалось продублировать курс', [
                'course' => $course->getId(),
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);

            throw new DuplicateEntityServiceException($e->getMessage());
        }
        $this->em->getConnection()->commit();

        return $newCourse;
    }

    /**
     * @param BaseFiles|null $baseFile
     *
     * @return BaseFiles|null
     *
     * @throws DuplicateEntityServiceException
     */
    public function duplicateBaseFile(?BaseFiles $baseFile): ?BaseFiles
    {
        try {
            if (!$baseFile) {
                return null;
            }
            $newBaseFile = new BaseFiles();
            $newBaseFile->setFileName($baseFile->getFileName());
            $newBaseFile->setFilePath($baseFile->getFilePath());
            $newBaseFile->setMimeType($baseFile->getMimeType());
            $this->em->persist($newBaseFile);
            $this->em->flush();

            return $newBaseFile;
        } catch (Exception $e) {
            $this->logger->error('Не удалось продублировать файл', [
                'base_file' => $baseFile ? $baseFile->getId() : null,
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);
            throw new DuplicateEntityServiceException($e->getMessage());
        }
    }

    /**
     * @param BaseFiles[] $baseFiles
     *
     * @return BaseFiles[]
     *
     * @throws DuplicateEntityServiceException
     */
    public function duplicateBaseFiles(array $baseFiles): array
    {
        try {
            $newBaseFiles = [];
            foreach ($baseFiles as $baseFile) {
                $newBaseFiles[] = $this->duplicateBaseFile($baseFile);
            }

            return $newBaseFiles;
        } catch (Exception $e) {
            $this->logger->error('Не удалось продублировать файлы', [
                'base_files' => array_map(static function ($baseFile) {
                    return $baseFile->getId();
                }, $baseFiles),
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);
            throw new DuplicateEntityServiceException($e->getMessage());
        }
    }

    /**
     * @param CourseConfiguration|null $courseConfiguration
     * @param Course|null $course
     *
     * @return CourseConfiguration|null
     *
     * @throws DuplicateEntityServiceException
     */
    public function duplicateCourseConfiguration(
        ?CourseConfiguration $courseConfiguration,
        ?Course $course
    ): ?CourseConfiguration {
        try {
            if (!$courseConfiguration || !$course) {
                return null;
            }
            $newCourseConfiguration = new CourseConfiguration();
            $newCourseConfiguration->setDateOfStart($courseConfiguration->getDateOfStart());
            $newCourseConfiguration->setDateOfFinish($courseConfiguration->getDateOfFinish());
            $newCourseConfiguration->setCourse($course);
            $this->em->persist($newCourseConfiguration);
            $this->em->flush();

            return $newCourseConfiguration;
        } catch (Exception $e) {
            $this->logger->error('Не удалось продублировать конфигурацию курса', [
                'course_configuration' => $courseConfiguration
                    ? $courseConfiguration->getId()
                    : null,
                'course' => $course ? $course->getId() : null,
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);
            throw new DuplicateEntityServiceException($e->getMessage());
        }
    }

    /**
     * @param Collection|Module[]|Traversable $modules
     * @param Course|null $course = null
     *
     * @return array
     *
     * @throws DuplicateEntityServiceException
     */
    public function duplicateModules(Traversable $modules, ?Course $course = null): array
    {
        try {
            $newModules = [];
            foreach ($modules as $module) {
                $newModules[] = $this->duplicateModule($module, $course);
            }

            return $newModules;
        } catch (Exception $e) {
            $this->logger->error('Не удалось продублировать модули', [
                'course' => $course ? $course->getId() : null,
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);
            throw new DuplicateEntityServiceException($e->getMessage());
        }
    }

    /**
     * @param Module|null $module
     * @param Course|null $course = null
     *
     * @return Module|null
     *
     * @throws DuplicateEntityServiceException
     */
    public function duplicateModule(?Module $module, ?Course $course = null): ?Module
    {
        try {
            if (!$module) {
                return null;
            }
            $newModule = new Module();
            $newModule->setName($module->getName());
            $newModule->setCourse($course);
            $newModule->setStart($module->getStart());
            $newModule->setFinish($module->getFinish());
            $newModule->setPositionInCourse($module->getPositionInCourse());
            $this->em->persist($newModule);
            $this->em->flush();

            return $newModule;
        } catch (Exception $e) {
            $this->logger->error('Не удалось продублировать модуль', [
                'module' => $module ? $module->getId() : null,
                'course' => $course ? $course->getId() : null,
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);
            throw new DuplicateEntityServiceException($e->getMessage());
        }
    }

    /**
     * @param Collection|Lesson[]|Traversable $lessons
     * @param Course|null $course = null
     *
     * @return array
     *
     * @throws DuplicateEntityServiceException
     */
    public function duplicateLessons($lessons, ?Course $course = null): array
    {
        try {
            $newLessons = [];
            foreach ($lessons as $lesson) {
                $newLessons[] = $this->duplicateLesson($lesson, $course);
            }

            return $newLessons;
        } catch (Exception $e) {
            $this->logger->error('Не удалось продублировать уроки', [
                'course' => $course ? $course->getId() : null,
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);
            throw new DuplicateEntityServiceException($e->getMessage());
        }
    }

    /**
     * @param Lesson|null $lesson
     * @param Course|null $course = null
     * @param bool $savePosition
     *
     * @return Lesson|null
     *
     * @throws DuplicateEntityServiceException
     * @throws ConnectionException
     */
    public function duplicateLesson(?Lesson $lesson, ?Course $course = null, bool $savePosition = true): ?Lesson
    {
        $this->em->getConnection()->beginTransaction();
        try {
            if (!$lesson) {
                return null;
            }

            $newLesson = new Lesson();
            $newLesson->setName($lesson->getName());
            $newLesson->setCourse($course);
            $newLesson->setDeleted($lesson->isDeleted());
            $this->em->persist($newLesson);
            $this->em->flush();

            $newLesson->setDescription($lesson->getDescription());
            $newLesson->setIsStopLesson($lesson->isStopLesson());
            $newLesson->setIsImportant($lesson->isImportant());
            $newLesson->setImgText($lesson->getImgText());
            $newLesson->setImgFile($this->duplicateBaseFile($lesson->getImgFile()));
            if ($course && $lesson->getModule()) {
                $course->addModule(
                    $this->duplicateModule(
                        $lesson->getModule(),
                        $course
                    )
                );
            }
            if ($savePosition) {
                $newLesson->setPosition($lesson->getPosition());
            }
            $newLesson->addSpeakers(
                $this->em->getRepository(User::class)->createQueryBuilder('u')
                    ->join('u.lessons', 'l')
                    ->where('l = :lesson')
                    ->setParameter('lesson', $lesson)
                    ->getQuery()
                    ->getResult()
            );
            $lessonConfiguration = $this->duplicateLessonConfiguration(
                $lesson->getLessonConfiguration(),
                $newLesson
            );
            if ($lessonConfiguration) {
                $newLesson->setLessonConfiguration($lessonConfiguration);
            }
            $newLesson->addLessonParts(
                $this->duplicateLessonParts(
                    $lesson->getLessonParts()->toArray(),
                    $lesson
                )
            );
            $this->em->persist($newLesson);
            $this->em->flush();
        } catch (Exception $e) {
            $this->em->getConnection()->rollBack();

            $this->logger->error('Не удалось продублировать урок', [
                'lesson' => $lesson->getId(),
                'course' => $course ? $course->getId() : null,
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);

            throw new DuplicateEntityServiceException($e->getMessage());
        }

        $this->em->getConnection()->commit();

        return $newLesson;
    }

    /**
     * @param LessonConfiguration|null $lessonConfiguration
     * @param Lesson $lesson
     *
     * @return LessonConfiguration|null
     *
     * @throws DuplicateEntityServiceException
     */
    public function duplicateLessonConfiguration(
        ?LessonConfiguration $lessonConfiguration,
        Lesson $lesson
    ): ?LessonConfiguration {
        try {
            if (!$lessonConfiguration) {
                return null;
            }
            $newLessonConfiguration = new LessonConfiguration();
            $newLessonConfiguration->setLesson($lesson);
            $newLessonConfiguration->setIsHidden($lessonConfiguration->isHidden());
            $newLessonConfiguration->setDateOfStart($lessonConfiguration->getDateOfStart());
            $this->em->persist($newLessonConfiguration);
            $this->em->flush();

            return $newLessonConfiguration;
        } catch (Exception $e) {
            $this->logger->error('Не удалось продублировать конфигурацию урока', [
                'lesson_configuration' => $lessonConfiguration
                    ? $lessonConfiguration->getId()
                    : null,
                'lesson' => $lesson->getId(),
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);
            throw new DuplicateEntityServiceException($e->getMessage());
        }
    }

    /**
     * @param LessonPart[] $lessonParts
     * @param Lesson $lesson
     *
     * @return LessonPart[]
     *
     * @throws DuplicateEntityServiceException
     */
    public function duplicateLessonParts(array $lessonParts, Lesson $lesson): array
    {
        try {
            $newLessonParts = [];
            foreach ($lessonParts as $lessonPart) {
                $newLessonParts[] = $this->duplicateLessonPart($lessonPart, $lesson);
            }

            return $newLessonParts;
        } catch (Exception $e) {
            $this->logger->error('Не удалось продублировать части урока', [
                'lesson_parts' => array_map(static function ($lessonPart) {
                    return $lessonPart->getId();
                }, $lessonParts),
                'lesson' => $lesson->getId(),
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);
            throw new DuplicateEntityServiceException($e->getMessage());
        }
    }

    /**
     * @param LessonPart $lessonPart
     * @param Lesson $lesson
     *
     * @return LessonPart
     *
     * @throws DuplicateEntityServiceException
     */
    public function duplicateLessonPart(LessonPart $lessonPart, Lesson $lesson): LessonPart
    {
        try {
            $newLessonPart = new LessonPart();
            $newLessonPart->setLesson($lesson);
            $newLessonPart->setName($lessonPart->getName());
            $newLessonPart->setPositionInLesson($lessonPart->getPositionInLesson());
            $newLessonPart->addLessonPartBlocks(
                $this->duplicateLessonPartBlocks(
                    $lessonPart->getLessonPartBlocks()->toArray(),
                    $lessonPart
                )
            );
            $this->em->persist($newLessonPart);
            $this->em->flush();

            return $newLessonPart;
        } catch (Exception $e) {
            $this->logger->error('Не удалось продублировать часть урока', [
                'lesson_part' => $lessonPart->getId(),
                'lesson' => $lesson->getId(),
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);
            throw new DuplicateEntityServiceException($e->getMessage());
        }
    }

    /**
     * @param LessonPartBlock[] $lessonPartBlocks
     * @param LessonPart|null $lessonPart = null
     *
     * @return LessonPartBlock[]
     *
     * @throws DuplicateEntityServiceException
     */
    public function duplicateLessonPartBlocks(
        array $lessonPartBlocks,
        ?LessonPart $lessonPart = null
    ): array {
        try {
            $newLessonPartBlocks = [];
            foreach ($lessonPartBlocks as $lessonPartBlock) {
                $newLessonPartBlocks[] = $this->duplicateLessonPartBlock(
                    $lessonPartBlock,
                    $lessonPart
                );
            }

            return $newLessonPartBlocks;
        } catch (Exception $e) {
            $this->logger->error('Не удалось продублировать блоки урока', [
                'lesson_part_blocks' => array_map(static function ($lessonPartBlock) {
                    return $lessonPartBlock->getId();
                }, $lessonPartBlocks),
                'lesson_part' => $lessonPart ? $lessonPart->getId() : null,
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);
            throw new DuplicateEntityServiceException($e->getMessage());
        }
    }

    /**
     * @param LessonPartBlock $lessonPartBlock
     * @param LessonPart|null $lessonPart = null
     *
     * @return LessonPartBlock
     *
     * @throws DuplicateEntityServiceException
     */
    public function duplicateLessonPartBlock(
        LessonPartBlock $lessonPartBlock,
        ?LessonPart $lessonPart = null
    ): LessonPartBlock {
        try {
            ini_set('max_execution_time', 0);

            $newLessonPartBlock = new LessonPartBlock();
            $newLessonPartBlock->setPart($lessonPart);
            $newLessonPartBlock->setDeleted($lessonPartBlock->isDeleted());
            $newLessonPartBlock->setPositionInPart($lessonPartBlock->getPositionInPart());
            $newLessonPartBlock->setVariety($lessonPartBlock->getVariety());
            $newLessonPartBlock->setType($lessonPartBlock->getType());
            $block = $this->duplicateLessonContentBlock(
                $lessonPartBlock->getType(),
                $lessonPartBlock->getVariety(),
                $lessonPartBlock->getBlockId()
            );
            if ($block && $block->getId()) {
                $newLessonPartBlock->setBlockId($block->getId());
            }
            $this->em->persist($newLessonPartBlock);
            $this->em->flush();

            return $newLessonPartBlock;
        } catch (Exception $e) {
            $this->logger->error('Не удалось продублировать блок урока', [
                'lesson_part_block' => $lessonPartBlock->getId(),
                'lesson_part' => $lessonPart ? $lessonPart->getId() : null,
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);
            throw new DuplicateEntityServiceException($e->getMessage());
        }
    }

    /**
     * @param int $type
     * @param int $variety
     * @param int $id
     *
     * @return LessonContentBlockInterface|null
     *
     * @throws DuplicateEntityServiceException
     */
    public function duplicateLessonContentBlock(int $type, int $variety, int $id): ?LessonContentBlockInterface
    {
        try {
            $block = $this->lessonContentService->findBlock($type, $variety, $id);
            if ($block) {
                $newBlockClass = $this->lessonContentService->getBlockType($type, $variety);
                if ($newBlockClass) {
                    switch ($newBlockClass) {
                        case BaseBlock1::class:
                            return $this->duplicateBaseBlock1($block);
                        case BaseBlock2::class:
                            return $this->duplicateBaseBlock2($block);
                        case BaseBlock3::class:
                            return $this->duplicateBaseBlock3($block);
                        case BaseBlock4::class:
                            return $this->duplicateBaseBlock4($block);
                        case BaseBlock5::class:
                            return $this->duplicateBaseBlock5($block);
                        case BaseBlock6::class:
                            return $this->duplicateBaseBlock6($block);
                        case BaseBlock7::class:
                            return $this->duplicateBaseBlock7($block);
                        case BaseBlockTaskWithCheck::class:
                            return $this->duplicateBaseBlockTaskWithCheck($block);
                        case BaseBlockNps::class:
                            return $this->duplicateBaseBlockNps($block);
                        case TaskTest::class:
                            return $this->duplicateTaskTest($block);
                        case TaskType5::class:
                            return $this->duplicateTaskType5($block);
                        default:
                            return null;
                    }
                }
            }
        } catch (InvalidArgumentException $e) {
            $this->logger->error('Не удалось определить тип контентного блока', [
                'type' => $type,
                'variety' => $variety,
                'id' => $id,
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);

            return null;
        } catch (Exception $e) {
            $this->logger->error('Не удалось продублировать контентный блок урока', [
                'type' => $type,
                'variety' => $variety,
                'id' => $id,
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);
            throw new DuplicateEntityServiceException($e->getMessage());
        }
    }

    /**
     * @param BaseBlock1 $baseBlock1
     *
     * @return BaseBlock1
     *
     * @throws DuplicateEntityServiceException
     */
    public function duplicateBaseBlock1(BaseBlock1 $baseBlock1): BaseBlock1
    {
        try {
            $newBaseBlock1 = new BaseBlock1();
            $newBaseBlock1->setHeader($baseBlock1->getHeader());
            $this->em->persist($newBaseBlock1);
            $this->em->flush();

            return $newBaseBlock1;
        } catch (Exception $e) {
            $this->logger->error('Не удалось продублировать ' . BaseBlock1::class, [
                'block' => $baseBlock1->getId(),
                'block_type' => BaseBlock1::class,
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);
            throw new DuplicateEntityServiceException($e->getMessage());
        }
    }

    /**
     * @param BaseBlock2 $baseBlock2
     *
     * @return BaseBlock2
     *
     * @throws DuplicateEntityServiceException
     */
    public function duplicateBaseBlock2(BaseBlock2 $baseBlock2): BaseBlock2
    {
        try {
            $newBaseBlock2 = new BaseBlock2();
            $newBaseBlock2->setHeader($baseBlock2->getHeader());
            $newBaseBlock2->setText($baseBlock2->getText());
            $this->em->persist($newBaseBlock2);
            $this->em->flush();

            return $newBaseBlock2;
        } catch (Exception $e) {
            $this->logger->error('Не удалось продублировать ' . BaseBlock2::class, [
                'block' => $baseBlock2->getId(),
                'block_type' => BaseBlock2::class,
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);
            throw new DuplicateEntityServiceException($e->getMessage());
        }
    }

    /**
     * @param BaseBlock3 $baseBlock3
     *
     * @return BaseBlock3
     *
     * @throws DuplicateEntityServiceException
     */
    public function duplicateBaseBlock3(BaseBlock3 $baseBlock3): BaseBlock3
    {
        try {
            $newBaseBlock3 = new BaseBlock3();
            $newBaseBlock3->setVideoUrl($baseBlock3->getVideoUrl());
            $newBaseBlock3->setTypeId($baseBlock3->getTypeId());
            $newBaseBlock3->setImgFile(
                $this->duplicateBaseFile(
                    $baseBlock3->getImgFile()
                )
            );
            $this->em->persist($newBaseBlock3);
            $this->em->flush();

            return $newBaseBlock3;
        } catch (Exception $e) {
            $this->logger->error('Не удалось продублировать ' . BaseBlock3::class, [
                'block' => $baseBlock3->getId(),
                'block_type' => BaseBlock3::class,
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);
            throw new DuplicateEntityServiceException($e->getMessage());
        }
    }

    /**
     * @param BaseBlock4 $baseBlock4
     *
     * @return BaseBlock4
     *
     * @throws DuplicateEntityServiceException
     */
    public function duplicateBaseBlock4(BaseBlock4 $baseBlock4): BaseBlock4
    {
        try {
            $newBaseBlock4 = new BaseBlock4();
            $newBaseBlock4->setImgFile(
                $this->duplicateBaseFile(
                    $baseBlock4->getImgFile()
                )
            );
            $this->em->persist($newBaseBlock4);
            $this->em->flush();

            return $newBaseBlock4;
        } catch (Exception $e) {
            $this->logger->error('Не удалось продублировать ' . BaseBlock4::class, [
                'block' => $baseBlock4->getId(),
                'block_type' => BaseBlock4::class,
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);
            throw new DuplicateEntityServiceException($e->getMessage());
        }
    }

    /**
     * @param BaseBlock5 $baseBlock5
     *
     * @return BaseBlock5
     *
     * @throws DuplicateEntityServiceException
     */
    public function duplicateBaseBlock5(BaseBlock5 $baseBlock5): BaseBlock5
    {
        try {
            $newBaseBlock5 = new BaseBlock5();
            $newBaseBlock5->setAudioUrl($baseBlock5->getAudioUrl());
            $this->em->persist($newBaseBlock5);
            $this->em->flush();

            return $newBaseBlock5;
        } catch (Exception $e) {
            $this->logger->error('Не удалось продублировать ' . BaseBlock5::class, [
                'block' => $baseBlock5->getId(),
                'block_type' => BaseBlock5::class,
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);
            throw new DuplicateEntityServiceException($e->getMessage());
        }
    }

    /**
     * @param BaseBlock6 $baseBlock6
     *
     * @return BaseBlock6
     *
     * @throws DuplicateEntityServiceException
     */
    public function duplicateBaseBlock6(BaseBlock6 $baseBlock6): BaseBlock6
    {
        try {
            $newBaseBlock6 = new BaseBlock6();
            $newBaseBlock6->setHeader($baseBlock6->getHeader());
            $newBaseBlock6->addFiles(
                $this->duplicateBaseFiles(
                    $baseBlock6->getFiles()->toArray()
                )
            );
            $this->em->persist($newBaseBlock6);
            $this->em->flush();

            return $newBaseBlock6;
        } catch (Exception $e) {
            $this->logger->error('Не удалось продублировать ' . BaseBlock6::class, [
                'block' => $baseBlock6->getId(),
                'block_type' => BaseBlock6::class,
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);
            throw new DuplicateEntityServiceException($e->getMessage());
        }
    }

    /**
     * @param BaseBlock7 $baseBlock7
     *
     * @return BaseBlock7
     *
     * @throws DuplicateEntityServiceException
     */
    public function duplicateBaseBlock7(BaseBlock7 $baseBlock7): BaseBlock7
    {
        try {
            $newBaseBlock7 = new BaseBlock7();
            $newBaseBlock7->setButtonText($baseBlock7->getButtonText());
            $newBaseBlock7->setButtonUrl($baseBlock7->getButtonUrl());
            $newBaseBlock7->setIsBlank($baseBlock7->isBlank());
            $newBaseBlock7->setOpenInAppDefaultBrowser(
                $baseBlock7->getOpenInAppDefaultBrowser()
            );
            $this->em->persist($newBaseBlock7);
            $this->em->flush();

            return $newBaseBlock7;
        } catch (Exception $e) {
            $this->logger->error('Не удалось продублировать ' . BaseBlock7::class, [
                'block' => $baseBlock7->getId(),
                'block_type' => BaseBlock7::class,
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);
            throw new DuplicateEntityServiceException($e->getMessage());
        }
    }

    /**
     * @param BaseBlockTaskWithCheck $baseBlockTaskWithCheck
     *
     * @return BaseBlockTaskWithCheck
     *
     * @throws DuplicateEntityServiceException
     */
    public function duplicateBaseBlockTaskWithCheck(
        BaseBlockTaskWithCheck $baseBlockTaskWithCheck
    ): BaseBlockTaskWithCheck {
        try {
            ini_set('max_execution_time', 0);

            $newBaseBlockTaskWithCheck = new BaseBlockTaskWithCheck();
            $this->em->persist($newBaseBlockTaskWithCheck);
            $this->em->flush();
            $newBaseBlockTaskWithCheck->setSystemName($baseBlockTaskWithCheck->getSystemName());
            $newBaseBlockTaskWithCheck->setDdl($baseBlockTaskWithCheck->getDdl());
            $newBaseBlockTaskWithCheck->addTaskSets(
                $this->duplicateTaskSets(
                    $baseBlockTaskWithCheck->getTaskSets()->toArray(),
                    $newBaseBlockTaskWithCheck
                )
            );
            $this->em->persist($newBaseBlockTaskWithCheck);
            $this->em->flush();

            return $newBaseBlockTaskWithCheck;
        } catch (Exception $e) {
            $this->logger->error('Не удалось продублировать ' . BaseBlockTaskWithCheck::class, [
                'block' => $baseBlockTaskWithCheck->getId(),
                'block_type' => BaseBlockTaskWithCheck::class,
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);
            throw new DuplicateEntityServiceException($e->getMessage());
        }
    }

    /**
     * @param BaseBlockNps|null $baseBlockNps
     *
     * @return BaseBlockNps|null
     *
     * @throws DuplicateEntityServiceException
     */
    public function duplicateBaseBlockNps(?BaseBlockNps $baseBlockNps): ?BaseBlockNps
    {
        try {
            if (!$baseBlockNps) {
                return null;
            }
            $newBaseBlockNps = new BaseBlockNps();
            $newBaseBlockNps->setSystemName($baseBlockNps->getSystemName());
            $newBaseBlockNps->setQuestion($baseBlockNps->getQuestion());
            $newBaseBlockNps->setAdditionalQuestion($baseBlockNps->getAdditionalQuestion());
            $newBaseBlockNps->setImg($baseBlockNps->getImg());
            $this->em->persist($newBaseBlockNps);
            $this->em->flush();

            return $newBaseBlockNps;
        } catch (Exception $e) {
            $this->logger->error('Не удалось продублировать ' . BaseBlockNps::class, [
                'block' => $baseBlockNps->getId(),
                'block_type' => BaseBlockNps::class,
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);
            throw new DuplicateEntityServiceException($e->getMessage());
        }
    }

    /**
     * @param TaskSet[] $taskSets
     * @param BaseBlockTaskWithCheck $baseBlockTaskWithCheck
     *
     * @return TaskSet[]
     *
     * @throws DuplicateEntityServiceException
     */
    public function duplicateTaskSets(
        array $taskSets,
        BaseBlockTaskWithCheck $baseBlockTaskWithCheck
    ): array {
        try {
            $newTaskSets = [];
            foreach ($taskSets as $taskSet) {
                $newTaskSets[] = $this->duplicateTaskSet($taskSet, $baseBlockTaskWithCheck);
            }

            return $newTaskSets;
        } catch (Exception $e) {
            $this->logger->error('Не удалось продублировать задачи', [
                'task_sets' => array_map(static function ($taskSet) {
                    return $taskSet->getId();
                }, $taskSets),
                'block_type' => BaseBlockNps::class,
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);
            throw new DuplicateEntityServiceException($e->getMessage());
        }
    }

    /**
     * @param TaskSet $taskSet
     * @param BaseBlockTaskWithCheck $baseBlockTaskWithCheck
     *
     * @return TaskSet
     *
     * @throws DuplicateEntityServiceException
     */
    public function duplicateTaskSet(
        TaskSet $taskSet,
        BaseBlockTaskWithCheck $baseBlockTaskWithCheck
    ): TaskSet {
        try {
            ini_set('max_execution_time', 0);

            $newTaskSet = new TaskSet();
            $newTaskSet->setVariety($taskSet->getVariety());
            $newTaskSet->setBlockTaskWithCheck($baseBlockTaskWithCheck);
            $block = $this->duplicateTaskSetVariation(
                $taskSet->getVariety(),
                $taskSet->getBlockId()
            );
            if ($block && $block->getId()) {
                $newTaskSet->setBlockId($block->getId());
            }
            $newTaskSet->setDeleted($taskSet->isDeleted());
            $newTaskSet->setPositionInTask($taskSet->getPositionInTask());
            $baseBlockTaskWithCheck->addTaskSet($newTaskSet);
            $this->em->persist($newTaskSet);
            $this->em->flush();

            return $newTaskSet;
        } catch (Exception $e) {
            $this->logger->error('Не удалось продублировать задачу', [
                'task_set' => $taskSet->getId(),
                'base_block_task_with_check' => $baseBlockTaskWithCheck->getId(),
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);
            throw new DuplicateEntityServiceException($e->getMessage());
        }
    }

    /**
     * @param int $variety
     * @param int $blockId
     *
     * @return TaskSetVarietyInterface|null
     *
     * @throws InvalidArgumentException|DuplicateEntityServiceException
     */
    public function duplicateTaskSetVariation(int $variety, int $blockId): ?TaskSetVarietyInterface
    {
        switch ($variety) {
            case LessonPartBlock::TASK_TYPE_1_VARIETY:
                return $this->duplicateTaskType1(
                    $this->lessonContentService->findBlockByClass(
                        TaskType1::class,
                        $blockId
                    )
                );
            default:
                $this->logger->error('Не удалось определить вариацию блока', [
                    'variety' => $variety,
                    'method' => __METHOD__,
                ]);
                throw new InvalidArgumentException('Не удалось определить вариацию блока');
        }
    }

    /**
     * @param TaskType1|null $taskType1
     *
     * @return TaskType1|null
     *
     * @throws DuplicateEntityServiceException
     */
    public function duplicateTaskType1(?TaskType1 $taskType1): ?TaskType1
    {
        try {
            if (!$taskType1) {
                return null;
            }
            $newTaskType1 = new TaskType1();
            $newTaskType1->setQuestion($taskType1->getQuestion());
            $newTaskType1->setTextBeforeDdl($taskType1->getTextBeforeDdl() ?? '');
            $newTaskType1->setTextAfterDdl($taskType1->getTextAfterDdl() ?? '');
            $newTaskType1->setTypeOfTask($taskType1->getTypeOfTask() ?? TaskType1::FIXED_SCORE);
            $newTaskType1->setMinPoints($taskType1->getMinPoints());
            $newTaskType1->setMaxPoints($taskType1->getMaxPoints());
            $newTaskType1->setPointsAfterDdl($taskType1->getPointsAfterDdl());
            $newTaskType1->setPointsBeforeDdl($taskType1->getPointsBeforeDdl());
            $newTaskType1->setIsAutoCheck($taskType1->isAutoCheck());
            $newTaskType1->setIsFileAnswerAllow($taskType1->isFileAnswerAllow());
            $newTaskType1->setIsLinkForbidden($taskType1->isLinkForbidden());
            $newTaskType1->setReviewAfterDdl($taskType1->getReviewAfterDdl());
            $this->em->persist($newTaskType1);
            $this->em->flush();

            return $newTaskType1;
        } catch (Exception $e) {
            $this->logger->error('Не удалось продублировать ' . TaskType1::class, [
                'task_set' => $taskType1->getId(),
                'task_set_variation' => TaskType1::class,
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);
            throw new DuplicateEntityServiceException($e->getMessage());
        }
    }

    /**
     * @param TaskTest|null $taskTest
     *
     * @return TaskTest|null
     *
     * @throws DuplicateEntityServiceException
     */
    public function duplicateTaskTest(?TaskTest $taskTest): ?TaskTest
    {
        try {
            if (!$taskTest) {
                return null;
            }
            ini_set('max_execution_time', 0);

            $newTaskTest = new TaskTest();
            $newTaskTest->setTitle($taskTest->getTitle());
            $newTaskTest->setDescription($taskTest->getDescription());
            $newTaskTest->setImgFile($this->duplicateBaseFile($taskTest->getImgFile()));
            $newTaskTest->addTaskTestQuestions(
                $this->duplicateTaskTestQuestions(
                    $taskTest->getTaskTestQuestions()->toArray(),
                    $taskTest
                )
            );
            $this->em->persist($newTaskTest);
            $this->em->flush();

            return $newTaskTest;
        } catch (Exception $e) {
            $this->logger->error('Не удалось продублировать ' . TaskTest::class, [
                'task_test' => $taskTest->getId(),
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);
            throw new DuplicateEntityServiceException($e->getMessage());
        }
    }

    /**
     * @param TaskType5 $taskType5
     *
     * @return TaskType5
     *
     * @throws DuplicateEntityServiceException
     */
    public function duplicateTaskType5(TaskType5 $taskType5): TaskType5
    {
        try {
            $newTaskType5 = new TaskType5();
            $newTaskType5->setTitle($taskType5->getTitle());
            $newTaskType5->setDescription($taskType5->getDescription());
            $newTaskType5->setDdl($taskType5->getDdl());
            $newTaskType5->setCorrectAnswers($taskType5->getCorrectAnswers());
            $newTaskType5->setAdditionalCorrectAnswers($taskType5->getAdditionalCorrectAnswers());
            $newTaskType5->setPointsBeforeDdl($taskType5->getPointsBeforeDdl());
            $newTaskType5->setPointsAfterDdl($taskType5->getPointsAfterDdl());
            $newTaskType5->setAddCorAnsPointsAfterDdl($taskType5->getAddCorAnsPointsAfterDdl());
            $newTaskType5->setAddCorAnsPointsBeforeDdl($taskType5->getAddCorAnsPointsBeforeDdl());
            $this->em->persist($newTaskType5);
            $this->em->flush();

            return $newTaskType5;
        } catch (Exception $e) {
            $this->logger->error('Не удалось продублировать ' . TaskType5::class, [
                'task_type_5' => $taskType5->getId(),
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);
            throw new DuplicateEntityServiceException($e->getMessage());
        }
    }

    /**
     * @param TaskTestQuestion[] $taskTestQuestions
     * @param TaskTest|null $taskTest = null
     *
     * @return TaskTestQuestion[]
     *
     * @throws DuplicateEntityServiceException
     */
    public function duplicateTaskTestQuestions(
        array $taskTestQuestions,
        ?TaskTest $taskTest = null
    ): array {
        try {
            $newTaskTestQuestions = [];
            foreach ($taskTestQuestions as $taskTestQuestion) {
                $newTaskTestQuestions[] = $this->duplicateTaskTestQuestion(
                    $taskTestQuestion,
                    $taskTest
                );
            }

            return $newTaskTestQuestions;
        } catch (Exception $e) {
            $this->logger->error('Не удалось продублировать вопросы для теста', [
                'task_test_questions' => array_map(static function ($taskTestQuestion) {
                    return $taskTestQuestion->getId();
                }, $taskTestQuestions),
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);
            throw new DuplicateEntityServiceException($e->getMessage());
        }
    }

    /**
     * @param TaskTestQuestion $taskTestQuestion
     * @param TaskTest|null $taskTest = null
     *
     * @return TaskTestQuestion
     *
     * @throws DuplicateEntityServiceException
     */
    public function duplicateTaskTestQuestion(
        TaskTestQuestion $taskTestQuestion,
        ?TaskTest $taskTest = null
    ): TaskTestQuestion {
        try {
            $newTaskTestQuestion = new TaskTestQuestion();
            $newTaskTestQuestion->setTest($taskTest);
            $newTaskTestQuestion->setText($taskTestQuestion->getText());
            $newTaskTestQuestion->setImgFile(
                $this->duplicateBaseFile(
                    $taskTestQuestion->getImgFile()
                )
            );
            $newTaskTestQuestion->setIsMultipleAnswers($taskTestQuestion->isMultipleAnswers());
            $newTaskTestQuestion->setPosition($taskTestQuestion->getPosition());
            $newTaskTestQuestion->setDeleted($taskTestQuestion->isDeleted());
            $this->em->persist($newTaskTestQuestion);
            $this->em->flush();

            return $newTaskTestQuestion;
        } catch (Exception $e) {
            $this->logger->error('Не удалось продублировать вопрос для теста', [
                'task_test_question' => $taskTestQuestion->getId(),
                'task_test' => $taskTest ? $taskTest->getId() : null,
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);
            throw new DuplicateEntityServiceException($e->getMessage());
        }
    }

    public function duplicateFolderAsync(CourseFolder $courseFolder, bool $withCourses = true)
    {
        $this->bus->dispatch(
            new CourseFolderDuplicateMessage($courseFolder->getId(), $withCourses),
            [
                new AmqpStamp('lms'),
            ]
        );
    }

    public function duplicateCourseAsync(Course $course)
    {
        $this->bus->dispatch(
            new CourseDuplicateMessage($course->getId()),
            [
                new AmqpStamp('lms'),
            ]
        );
    }

    public function duplicateLessonAsync(Lesson $lesson, ?Course $course)
    {
        $this->bus->dispatch(
            new LessonDuplicateMessage($lesson->getId(), $course->getId()),
            [
                new AmqpStamp('lms'),
            ]
        );
    }

}

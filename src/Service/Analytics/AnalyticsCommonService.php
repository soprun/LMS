<?php

namespace App\Service\Analytics;

use App\DTO\Document\TractionAnalytics\TractionAnalyticsUserDocument;
use App\DTO\Document\TractionAnalytics\WeekRevenueProfitDocument;
use App\Entity\AbstractCourse;
use App\Entity\CourseStream;
use App\Entity\Faculty;
use App\Entity\Questionnaire\Question;
use App\Entity\Questionnaire\UserAnswer;
use App\Entity\Team;
use App\Entity\Traction\ValueType;
use App\Entity\Traction\Value;
use App\Entity\User;
use App\Entity\UserGroup;
use App\Entity\UserGroupUser;
use App\Exception\AnalyticsCommonServiceException;
use App\Service\AuthServerService;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Модуль для получения общей аналитики
 */
class AnalyticsCommonService
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var AuthServerService
     */
    protected $authServerService;

    /**
     * @param EntityManagerInterface $em
     * @param LoggerInterface $logger
     * @param AuthServerService $authServerService
     * @param string $_authServerToken
     */
    public function __construct(
        EntityManagerInterface $em,
        LoggerInterface $logger,
        AuthServerService $authServerService,
        string $_authServerToken
    ) {
        $this->em = $em;
        $this->logger = $logger;
        $this->authServerService = $authServerService;
        $this->authServerService->setUserAccessToken('ServerToken ' . $_authServerToken);
    }

    /**
     * @param TractionValue $tractionValue
     * @param WeekRevenueProfitDocument[] $documents
     *
     * @return int|null
     */
    protected function getWeekRevenueProfitDocumentIndexByDate(TractionValue $tractionValue, array $documents)
    {
        foreach ($documents as $index => $document) {
            if ($tractionValue->getDate()->getTimestamp() === $document->weekStartDate) {
                return $index;
            }
        }
    }

    /**
     * Группирует и формирует DTO для показателей Выручки/Прибыли по неделям
     *
     * @param User $user
     * @param CourseStream $courseStream = null
     *
     * @return WeekRevenueProfitDocument[]
     */
    public function getTractionByUser(User $user, ?CourseStream $courseStream = null): array
    {
        $weeks = [];
        if (!$user->getTractionValues()->isEmpty()) {
            /** @var TractionValue $tractionValue */
            foreach ($user->getTractionValues() as $tractionValue) {
                $document = new WeekRevenueProfitDocument();
                $document->weekStartDate = $tractionValue->getDate()->getTimestamp();
                $document->user = $user->getId();
                if ($tractionValue->getType()->getTitle() === ValueType::TYPE_PROFIT) {
                    $findedDocumentIndex = $this->getWeekRevenueProfitDocumentIndexByDate($tractionValue, $weeks);
                    $document = $findedDocumentIndex ? $weeks[$findedDocumentIndex] : $document;
                    $document->profit = $tractionValue->getValue();
                }
                if ($tractionValue->getType()->getTitle() === ValueType::TYPE_REVENUE) {
                    $findedDocumentIndex = $this->getWeekRevenueProfitDocumentIndexByDate($tractionValue, $weeks);
                    $document = $findedDocumentIndex ? $weeks[$findedDocumentIndex] : $document;
                    $document->revenue = $tractionValue->getValue();
                }
                if ($courseStream) {
                    $document = $this->defineWeekNumberByCourseStream($courseStream, $document);
                }
                if ($findedDocumentIndex !== 0 && $findedDocumentIndex === null) {
                    $weeks[] = $document;
                }
            }
        }

        return $weeks;
    }

    /**
     * Обертка над $this->getTractionByUser(User $user): array
     *
     * @param User $user
     * @param CourseStream $courseStream = null
     *
     * @return WeekRevenueProfitDocument[]
     */
    public function getTractionByUserId(int $id, ?CourseStream $courseStream = null): array
    {
        $user = $this->em->getRepository(User::class)->find($id);
        if ($user) {
            return $this->getTractionByUser($user, $courseStream);
        }
        $this->logger->error('Пользователь не найден', [
            'user_id' => $id,
            'exception' => AnalyticsCommonServiceException::class,
            'method' => __METHOD__,
        ]);
        throw new AnalyticsCommonServiceException('Пользователь не найден');
    }

    /**
     * Определяет номера недель для App\DTO\Document\TractionAnalytics\WeekRevenueProfitDocument
     * по переданному потоку курса. Если неделя не определена, значит показатель выходит за рамки курса.
     *
     * @param CourseStream $courseStream
     * @param WeekRevenueProfitDocument $document
     *
     * @return WeekRevenueProfitDocument
     */
    public function defineWeekNumberByCourseStream(
        CourseStream $courseStream,
        WeekRevenueProfitDocument $document
    ): WeekRevenueProfitDocument {
        if ($courseStream->getStartDate() && $courseStream->getPeriod() && $document->weekStartDate) {
            $courseStreamDate = new DateTime($courseStream->getStartDate()->format('Y-m-d'));
            $documentDate = new DateTime();
            $documentDate->setTimestamp($document->weekStartDate);
            if ($courseStreamDate->format('Y-m-d') == $documentDate->format('Y-m-d')) {
                $document->week = 1;

                return $document;
            }
            for ($i = 1; $i <= $courseStream->getPeriod(); $i++) {
                if ($courseStreamDate->modify('+7 days')->format('Y-m-d') == $documentDate->format('Y-m-d')) {
                    $document->week = $i + 1;

                    return $document;
                }
            }
            $this->logger->warning('Не смогли определить неделю', [
                'course_stream_id' => $courseStream->getId(),
                'start_date' => $courseStream->getStartDate(),
                'period' => $courseStream->getPeriod(),
                'method' => __METHOD__
            ]);
        } else {
            $this->logger->warning('Не смогли определить неделю', [
                'course_stream_id' => $courseStream->getId(),
                'start_date' => $courseStream->getStartDate(),
                'week_start_date' => $document->weekStartDate,
                'period' => $courseStream->getPeriod(),
                'method' => __METHOD__
            ]);
        }

        return $document;
    }

    /**
     * Вернет пользователей из указанного потока
     *
     * @param CourseStream $courseStream
     * @param array $fields = null
     * @param int $page = 1
     * @param int $pageSize = 10
     *
     * @return User[]|array
     */
    public function getUsersByCourseStream(
        CourseStream $courseStream,
        array $fields = null,
        array $groupBy = null,
        int $page = 1,
        int $pageSize = 10
    ): array {
        try {
            /** @var QueryBuilder $queryBuilder */
            $queryBuilder =  $this->em->getRepository(User::class)->createQueryBuilder('u');
            if ($fields) {
                $queryBuilder->select($fields);
            }
            $queryBuilder->join('u.groupRelations', 'ugu')
                ->join('ugu.userGroup', 'ug')
                ->join('ug.courseStream', 'cs')
                ->where('cs = :courseStream')
                ->setParameter('courseStream', $courseStream);
            if ($groupBy) {
                $queryBuilder->groupBy($groupBy);
            }

            return $queryBuilder
                ->setFirstResult(($page - 1) * $pageSize)
                ->setMaxResults($pageSize)
                ->getQuery()
                ->getResult();
        } catch (\Exception $e) {
            $this->logger->warning('Не смогли получить пользователей по указанному потоку', [
                'course_stream_id' => $courseStream->getId(),
                'method' => __METHOD__,
                'message' => $e->getMessage(),
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);
            throw new AnalyticsCommonServiceException($e->getMessage());
        }
    }

    /**
     * Вернет пользователей из указанной группы
     *
     * @param UserGroup $userGroup
     * @param array $fields = null
     * @param int $page = 1
     * @param int $pageSize = 10
     *
     * @return User[]|array
     */
    public function getUsersByUserGroup(
        UserGroup $userGroup,
        array $fields = null,
        array $groupBy = null,
        int $page = 1,
        int $pageSize = 10
    ): array {
        try {
            /** @var QueryBuilder $queryBuilder */
            $queryBuilder =  $this->em->getRepository(User::class)->createQueryBuilder('u');
            if ($fields) {
                $queryBuilder->select($fields);
            }
            $queryBuilder->join('u.groupRelations', 'ugu')
                ->join('ugu.userGroup', 'ug')
                ->where('ug = :userGroup')
                ->setParameter('userGroup', $userGroup);
            if ($groupBy) {
                $queryBuilder->groupBy($groupBy);
            }

            return $queryBuilder
                ->setFirstResult(($page - 1) * $pageSize)
                ->setMaxResults($pageSize)
                ->getQuery()
                ->getResult();
        } catch (\Exception $e) {
            $this->logger->warning('Не смогли получить пользователей по указанной группе', [
                'user_group_id' => $userGroup->getId(),
                'method' => __METHOD__,
                'message' => $e->getMessage(),
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);
            throw new AnalyticsCommonServiceException($e->getMessage());
        }
    }

    /**
     * @param CourseStream $courseStream
     *
     * @return Faculty
     */
    public function getFacultyByCourseStream(?CourseStream $courseStream): ?Faculty
    {
        if ($courseStream) {
            return $this->em->getRepository(Faculty::class)->findOneBy([
                'courseStream' => $courseStream,
            ]);
        }

        return null;
    }

    /**
     * @param UserGroup $userGroup
     *
     * @return Faculty
     */
    public function getFacultyByUserGroup(?UserGroup $userGroup): ?Faculty
    {
        if ($userGroup) {
            return $this->em->getRepository(Faculty::class)->findOneBy([
                'userGroup' => $userGroup,
            ]);
        }

        return $userGroup;
    }

    /**
     * Вернет пользователей из Скорость Клуба
     * @TODO: надо Скорость Клуб вынести в абстрактный курс
     *
     * @param array $fields = null
     * @param array $groupBy = null
     * @param int $page = 1
     * @param int $pageSize = 10
     *
     * @return User[]|array
     */
    public function getUserFromSpeedClub(
        array $fields = null,
        array $groupBy = null,
        int $page = 1,
        int $pageSize = 10
    ): array {
        try {
            /** @var QueryBuilder $queryBuilder */
            $queryBuilder = $this->em->getRepository(User::class)->createQueryBuilder('u');
            if ($fields) {
                $queryBuilder->select($fields);
            }
            $queryBuilder->join('u.groupRelations', 'ugu')
                ->join('ugu.userGroup', 'ug')
                ->join('ug.courseStream', 'cs')
                ->join('cs.abstractCourse', 'ac')
                ->where('ac.slug in (:slug) AND ugu.deleted = 0')
                ->andWhere('cs.id >= 23 OR cs.id = 19')
                ->setParameter('slug', [AbstractCourse::SLUG_SPEED, AbstractCourse::SLUG_SPEED_CLUB]);
            if ($groupBy) {
                $queryBuilder->groupBy($groupBy);
            }
            return $queryBuilder
                ->setFirstResult(($page - 1) * $pageSize)
                ->setMaxResults($pageSize)
                ->getQuery()
                ->getResult();
        } catch (\Exception $e) {
            $this->logger->warning('Не смогли получить пользователей', [
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'exception' => get_class($e),
                'trace' => $e->getTrace(),
            ]);
            throw new AnalyticsCommonServiceException($e->getMessage());
        }
    }

    /**
     * Возвращает полный отчет с трекшеном по потокам Скорости и Сотке
     *
     * @param CourseStream $courseStream
     * @param int $page = 1
     * @param int $pageSize = 10
     *
     * @return string[]
     *
     * @throws AnalyticsCommonServiceException
     */
    public function getFullReportByStream(
        CourseStream $courseStream,
        int $page = 1,
        int $pageSize = 10
    ): array {
        $availableCoursesStreamsSlugs = [
            AbstractCourse::SLUG_SPEED,
            AbstractCourse::SLUG_SPEED_CLUB,
            AbstractCourse::SLUG_HUNDRED,
        ];
        if (in_array($courseStream->getAbstractCourse()->getSlug(), $availableCoursesStreamsSlugs)) {
            $headers = [$this->getCsvHeader(true)];
            $users = $this->em->getRepository(User::class)
                ->createQueryBuilder('u')
                ->join('u.groupRelations', 'ugu')
                ->join('ugu.userGroup', 'ug')
                ->where('ugu.deleted = 0 and ug.courseStream = :courseStream')
                ->setParameter('courseStream', $courseStream)
                ->setFirstResult(($page - 1) * $pageSize)
                ->setMaxResults($pageSize)
                ->getQuery()
                ->getResult();
            $userIds = array_map(static function (User $user) {
                return $user->getId();
            }, $users);
            $documents = $this->getUsersData($userIds, $courseStream, [], true);
            $documents = array_map(static function ($document) {
                return (string)$document;
            }, $documents);
            $csvReport = array_merge($headers, $documents);

            return $csvReport;
        } else {
            $this->logger->error('Абстрактный курс не соответствует требованиям', [
                'course_stream_id' => $courseStream->getId(),
                'abstract_course_slug' => $courseStream->getAbstractCourse()->getSlug(),
                'available_slugs' => $availableCoursesStreamsSlugs,
                'method' => __METHOD__,
            ]);
            throw new AnalyticsCommonServiceException(
                'Абстрактный курс не соответствует требованиям'
            );
        }
    }

    /**
     * Формирует заголовки CSV, который будет возвращен в отчете
     *
     * @param bool $unlim = false
     */
    private function getCsvHeader(bool $unlim = false): string
    {
        $csv = '';
        foreach (array_keys(get_object_vars(new TractionAnalyticsUserDocument())) as $prop) {
            if ($prop === 'revenue' || $prop === 'profit') {
                foreach (range(1, $unlim ? 20 : 6) as $week) {
                    $csv .= "{$prop}_{$week};";
                }
                continue;
            }
            $csv .= $prop . TractionAnalyticsUserDocument::DEFAULT_SEPARATOR;
        }

        return mb_substr($csv, 0, -1);
    }

    /**
     * Формирует TractionAnalyticsUserDocument по переданным параметрам
     *
     * @param array $userIds
     * @param CourseStream $courseStream
     * @param array $params = []
     *  - folderId
     *  - leadIds
     *  - budgets
     *  - responsible
     * @param bool $unlim = false
     *
     * @throws NotFoundHttpException
     *
     * @return TractionAnalyticsUserDocument[]
     */
    public function getUsersData(
        array $userIds,
        CourseStream $courseStream,
        array $params = [],
        bool $unlim = false
    ) {
        $folderId = null;
        $leadIds = [];
        $budgets = [];
        $responsible = [];
        if (!empty($params)) {
            $folderId = $params['folderId'];
            $leadIds = $params['leadIds'];
            $budgets = $params['budgets'];
            $responsible = $params['responsible'];
        }
        // берем всех учеников и их группы на потоке
        $rows = $this->em->getRepository(UserGroupUser::class)
            ->createQueryBuilder('ugu')
            ->select(
                'u.authUserId',
                'u.id',
                'ug.name',
                'ug.id AS groupId',
                'ugu.amoLeadId',
                'added.authUserId AS addedAuthId',
                'added.id AS addedId'
            )
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ug.courseStream', 'cs')
            ->innerJoin('ugu.user', 'u')
            ->leftJoin('ugu.addedBy', 'added')
            ->andWhere('cs = :courseStream')
            ->setParameter('courseStream', $courseStream)
            ->andWhere('ugu.deleted = 0')
            ->andWhere('u.id IN(:userIds)')
            ->setParameter('userIds', $userIds)
            ->andWhere("ug.name NOT LIKE '%факультет%'")
            ->andWhere("ug.name NOT LIKE '%сборная%'")
            ->groupBy(
                'u.authUserId',
                'u.id',
                'ug.name',
                'ug.id',
                'ugu.amoLeadId',
                'added.authUserId',
                'added.id'
            )
            ->orderBy('ug.id', 'ASC')
            ->getQuery()
            ->getResult();
        $groups = $addedByIds = [];
        foreach ($rows as $row) {
            $userId = $row['id'];
            $groups[$userId] = $row['name'];
            if ($row['addedId']) {
                $addedByIds[$userId] = [
                    'id' => $row['addedId'],
                    'authId' => $row['addedAuthId'],
                ];
            }
        }
        // забираем аутх пользователей
        $authUserIds = array_unique(array_column($rows, 'authUserId'));
        unset($rows);
        $authRows = $this->authServerService->filterUsers(
            ['ids' => $authUserIds],
            ['lms', 'user:preview', 'user:business'],
            1,
            count($authUserIds)
        );
        if (!$authRows['data']['count']) {
            throw new NotFoundHttpException('Не удалось найти пользователей на authServer');
        }
        $authRows = $authRows['data']['users'];
        $ids = array_column($authRows, 'id');

        $results = $this->getUserResults($ids, $courseStream, $unlim);
        $faculties = $this->getUserFaculties($ids, $courseStream);
        $badGroups = $this->getBadGroups($ids);
        [$deletedAlternativeGroups, $alternativeGroups] = $this->getAlternativeGroups($ids, $courseStream);
        $teams = $this->getUserTeams($ids, $courseStream);
        // забираем капитанов
        $captainAuthIds = array_filter(
            array_unique(array_column($teams, 'captainAuthId')),
            static function ($id) {
                return ($id);
            }
        );
        $captains = [];
        if (!empty($captainAuthIds)) {
            $authCaptains = $this->authServerService->filterUsers(
                ['ids' => $captainAuthIds],
                ['lms', 'user:preview'],
                1,
                count($captainAuthIds)
            );
            if (!$authCaptains['data']['count']) {
                throw new NotFoundHttpException('Не удалось найти пользователей на authServer');
            }
            $authCaptains = $authCaptains['data']['users'];
            foreach ($authCaptains as $authCaptain) {
                $captains[$authCaptain['id']] = sprintf(
                    "%s %s",
                    $authCaptain['info']['name'],
                    $authCaptain['info']['lastname']
                );
            }
        }
        // забираем тех, кто добавляет в группы
        $addedByNames = [];
        $addedByAuthIds = array_unique(array_column($addedByIds, 'authId'));
        if (!empty($addedByAuthIds)) {
            $authAddedByRows = $this->authServerService->filterUsers(
                ['ids' => $addedByAuthIds],
                ['lms', 'user:preview'],
                1,
                count($addedByAuthIds)
            );
            if (!$authAddedByRows['data']['count']) {
                throw new NotFoundHttpException('Не удалось найти пользователей на authServer');
            }
            $authAddedByRows = $authAddedByRows['data']['users'];
            foreach ($authAddedByRows as $authAddedByRow) {
                $addedByNames[$authAddedByRow['id']] = sprintf(
                    "%s %s (%s)",
                    $authAddedByRow['info']['name'],
                    $authAddedByRow['info']['lastname'],
                    $authAddedByRow['email'],
                );
            }
        }
        // Финальная сборка отчета
        $users = [];
        foreach ($authRows as $authRow) {
            $user = new TractionAnalyticsUserDocument();
            $user->lmsUserId = $authRow['id'];
            $user->email = $authRow['email'];
            $user->phone = $authRow['info']['phone'];
            $user->name = $authRow['info']['name'];
            $user->lastname = $authRow['info']['lastname'];
            $user->city = $authRow['info']['city'];
            $user->niche = $authRow['info']['niche'];
            $user->amoLeadId = $leadIds[$user->email] ?? null;
            $user->niche = str_replace(
                TractionAnalyticsUserDocument::DEFAULT_SEPARATOR,
                ',',
                $authRow['info']['niche']
            );
            $user->teamName = $teams[$user->lmsUserId]['name'] ?? null;
            $user->teamId = $teams[$user->lmsUserId]['teamId'] ?? null;
            if (isset($teams[$user->lmsUserId], $captains[$teams[$user->lmsUserId]['captainId']])) {
                $user->captainName = $captains[$teams[$user->lmsUserId]['captainId']];
                $user->captainId = $teams[$user->lmsUserId]['captainId'];
            }
            if (isset($addedByIds[$user->lmsUserId], $addedByNames[$addedByIds[$user->lmsUserId]['id']])) {
                $user->addedBy = $addedByNames[$addedByIds[$user->lmsUserId]['id']];
            }
            $user->groupName = $groups[$user->lmsUserId] ?? null;
            $user->facultyName = $faculties[$user->lmsUserId]['name'] ?? null;
            $user->facultyId = $faculties[$user->lmsUserId]['id'] ?? null;
            $user->alternativeGroupName = $alternativeGroups[$user->lmsUserId] ?? null;
            $user->deletedAlternativeGroupName = $deletedAlternativeGroups[$user->lmsUserId] ?? null;
            $user->badGroup = $badGroups[$user->lmsUserId] ?? null;
            $user->budget = $budgets[$user->amoLeadId] ?? null;
            $user->responsiblePartnerId = $responsible[$user->amoLeadId]['id'] ?? null;
            $user->responsiblePartnerName = $responsible[$user->amoLeadId]['name'] ?? null;
            preg_match('#\((.*?)\)#', $user->groupName, $match);
            $user->tariff = $match[1] ?? null;
            foreach ($results[$user->lmsUserId] ?? [] as $resultProperty => $userResults) {
                $user->$resultProperty = $userResults;
            }
            $users[] = $user;
        }

        return $users;
    }

    /**
     * Формирует результаты пользователей в трекшене
     *
     * @param array $userIds
     * @param CourseStream $courseStream
     * @param bool $unlim = false
     *
     * @return array
     */
    public function getUserResults(array $userIds, CourseStream $courseStream, bool $unlim = false): array
    {
        if (!$courseStream->getStartDate()) {
            return [];
        }
        $results = [];
        $channels = [
            ValueType::TYPE_REVENUE => 'revenue',
            ValueType::TYPE_PROFIT => 'profit',
        ];
        foreach ($userIds as $userId) {
            foreach ($channels as $channel) {
                foreach (range(1, $unlim ? 20 : 6) as $week) {
                    $results[$userId][$channel][$week] = 0;
                }
            }
        }
        /** @var Value[] $values */
        $qb = $this->em->getRepository(Value::class)
            ->createQueryBuilder('value')
            ->join('value.type', 'valueType')
            ->where('valueType.title in (:valueTypeTitles)')
            ->andWhere('value.owner in (:userIds) and value.date >= :startDate')
            ->setParameter('userIds', $userIds)
            ->setParameter('valueTypeTitles', array_keys($channels))
            ->setParameter('startDate', $courseStream->getStartDate());

        if (!$unlim) {
            $qb
                ->andWhere("value.date < DATE_ADD(:startDate, :period, 'WEEK')")
                ->setParameter('period', $courseStream->getPeriod() + 1);
        }
        $values = $qb->getQuery()->getResult();

        $firstWeek = (int)$courseStream->getStartDate()->format('W');
        foreach ($values as $value) {
            $week = (int)$value->getDate()->format('W') - $firstWeek + 1;
            $type = $channels[$value->getType()->getTitle()];

            if ($week === 0) {
                $results[$value->getOwnerId()][$type . '_before'] = $value->getValue();
                continue;
            }
            if ($week > 0) {
                $results[$value->getOwnerId()][$type][$week] = $value->getValue() ?? 0;
            }
        }

        /** @var UserAnswer[] $beforeCourseValues */
        $beforeCourseValues = $this->em->getRepository(UserAnswer::class)
            ->createQueryBuilder('answer')
            ->join('answer.question', 'question')
            ->where('answer.courseStream = :courseStream and question.slug in (:slugs)')
            ->setParameter('courseStream', $courseStream)
            ->setParameter('slugs', [Question::MONTH_PROFIT, Question::MONTH_REVENUE])
            ->getQuery()
            ->getResult();

        foreach ($beforeCourseValues as $value) {
            $type = Question::MONTH_PROFIT === $value->getQuestion()->getSlug() ? 'profit' : 'revenue';
            if ($value->getText()) {
                $results[$value->getUser()->getId()][$type . '_before'] = $value->getText();
            }
        }

        return $results;
    }

    /**
     * Вернет инфу о факультетах пользователей
     *
     * @param array $ids
     * @param CourseStream $courseStream
     * @param array $fields = []
     * @param array $groupBy = []
     *
     * @return array
     */
    public function getUserFaculties(
        array $ids,
        CourseStream $courseStream,
        array $fields = [],
        array $groupBy = []
    ): array {
        if (empty($fields)) {
            $fields = ['u.id', 'f.name', 'f.id as facultyId'];
        }
        if (empty($groupBy)) {
            $groupBy = ['u.id', 'f.name', 'facultyId'];
        }
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $this->em->getRepository(Faculty::class)->createQueryBuilder('f');
        $rows = $queryBuilder
            ->select($fields)
            ->innerJoin('f.userGroupUsers', 'ugu')
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ug.courseStream', 'cs')
            ->innerJoin('ugu.user', 'u')
            ->andWhere('ugu.deleted = 0')
            ->andWhere('cs = :courseStream')
            ->andWhere('u.id IN(:ids)')
            ->setParameters([
                'ids' => $ids,
                'courseStream' => $courseStream,
            ])
            ->groupBy(...$groupBy)
            ->getQuery()
            ->getResult();
        $users = [];
        foreach ($rows as $row) {
            $users[$row['id']] = [
                'name' => $row['name'],
                'id' => $row['facultyId']
            ];
        }

        return $users;
    }

    /**
     * @param array $ids
     *
     * @return array
     */
    public function getBadGroups(array $ids): array
    {
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $this->em->getRepository(UserGroupUser::class)->createQueryBuilder('ugu');
        $rows = $queryBuilder
            ->select('u.id', 'ug.name')
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ugu.user', 'u')
            ->where("ug.name IN(:names) OR ug.name like '%Новый МСА%'")
            ->andWhere('ugu.deleted = 0')
            ->andWhere('u.id IN(:userIds)')
            ->setParameter(
                'names',
                [
                    // @TODO: надо отдельную таблицу в БД запилить под такие группы
                    'Like Family',
                    'Обучение Капитанов Скорость (С16)',
                    'Капитаны СК17',
                    'Like Family (карта не действует)',
                    'Капитаны СК19',
                    'Марафон Ушаков',
                    'Капитаны С16 + факультеты',
                    'Старшие капитаны СК17',
                    'Франчайзи',
                    'Франчайзи Скорость / Сотка',
                ]
            )
            ->setParameter('userIds', $ids)
            ->groupBy('u.id', 'ug.name')
            ->getQuery()
            ->getResult();

        $users = [];
        foreach ($rows as $row) {
            $users[$row['id']] = $row['name'];
        }

        return $users;
    }

    /**
     * @param array $userIds
     * @param CourseStream $courseStream
     *
     * @return array
     */
    public function getAlternativeGroups(array $userIds, CourseStream $courseStream): array
    {
        // определяем альтернативные группы
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(UserGroup::class)->createQueryBuilder('ug');
        $rows = $qb
            ->select('aug.id')
            ->innerJoin('ug.courseStream', 'cs')
            ->innerJoin('ug.alternativeGroup', 'aug')
            ->andWhere('cs = :courseStream')
            ->setParameter('courseStream', $courseStream)
            ->getQuery()
            ->getResult();
        $alternativeIds = array_unique(array_column($rows, 'id'));

        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(UserGroupUser::class)->createQueryBuilder('ugu');
        $rows = $qb
            ->select('u.id', 'ug.name', 'ug.id AS groupId', 'ugu.deleted AS isDeleted')
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ugu.user', 'u')
            ->andWhere('u.id IN(:userIds)')
            ->andWhere('ug.id IN(:groupIds)')
            ->setParameters(
                [
                    'userIds' => $userIds,
                    'groupIds' => $alternativeIds,
                ]
            )
            ->groupBy('u.id', 'ug.name', 'ug.id')
            ->orderBy('ug.id', 'ASC')
            ->getQuery()
            ->getResult();

        $deletedAlternativeGroups = $alternativeGroups = [];
        foreach ($rows as $row) {
            $name = $row['name'];
            $userId = $row['id'];

            if ($row['isDeleted'] === true) {
                $deletedAlternativeGroups[$userId] = $name;
            } else {
                $alternativeGroups[$userId] = $name;
            }
        }

        return [$deletedAlternativeGroups, $alternativeGroups];
    }

    /**
     * @param array $userIds
     * @param CourseStream $courseStream
     *
     * @return array
     */
    public function getUserTeams(array $userIds, CourseStream $courseStream): array
    {
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $this->em->getRepository(Team::class)->createQueryBuilder('t');
        $rows = $queryBuilder
            ->select('u.id', 't.id AS teamId', 't.name', 'c.authUserId AS captainAuthId', 'c.id AS captainId')
            ->innerJoin('t.folder', 'f')
            ->innerJoin('t.users', 'u')
            ->leftJoin('t.captain', 'c')
            ->andWhere('f.name = :courseName')
            ->andWhere('u.id IN(:ids)')
            ->setParameters([
                'ids' => $userIds,
                'courseName' => $courseStream->getName(),
            ])
            ->groupBy('u.id', 't.id', 't.name', 'c.authUserId', 'c.id')
            ->getQuery()
            ->getResult();
        $users = [];
        foreach ($rows as $row) {
            $users[$row['id']] = [
                'name' => $row['name'],
                'captainAuthId' => $row['captainAuthId'],
                'captainId' => $row['captainId'],
                'teamId' => $row['teamId'],
            ];
        }

        return $users;
    }

    /**
     * Вернет результат трекшена команды
     *
     * @param Team $team
     *
     * @return array
     */
}

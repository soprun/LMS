<?php

namespace App\Service\Analytics;

use App\Entity\Analytics\ReportMSG;
use App\Entity\Analytics\ReportMSGCell;
use App\Entity\Courses\Course;
use App\Entity\User;
use App\Entity\UserCourseTariff;
use App\Entity\UserGroup;
use App\Entity\UserTariff;
use App\Service\AuthServerHelper;
use App\Service\FormHelper;
use App\Service\UserHelper;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Security\Core\Security;

class AnalyticsMSGHelper
{
    private static $GROUP_FOLDER_MSA_ID = 43; //devLms 43/prod 43
    private static $GROUP_FOLDER_MSG_ID = 33; //devLms 22/prod 33
    private static $COURSE_FOLDER_MSG_ID = 24; //devLMS 24/prod 24
    private static $PERIOD_TO_MONTH = [
        '1' => [1, 2, 9, 10, 11, 12],
        '2' => [3, 4, 5, 6, 7, 8],
    ];
    private $em;
    private $security;
    private $formHelper;
    private $authServerHelper;
    private $userHelper;

    /**
     * AnalyticsMSGHelper constructor.
     * @param  EntityManagerInterface  $em
     * @param  Security  $security
     * @param  FormHelper  $formHelper
     * @param  AuthServerHelper  $authServerHelper
     * @param  UserHelper  $userHelper
     */
    public function __construct(
        EntityManagerInterface $em,
        Security $security,
        FormHelper $formHelper,
        AuthServerHelper $authServerHelper,
        UserHelper $userHelper
    ) {
        $this->em = $em;
        $this->security = $security;
        $this->formHelper = $formHelper;
        $this->authServerHelper = $authServerHelper;
        $this->userHelper = $userHelper;
    }

    /**
     * @param  Request  $request
     * @return JsonResponse
     */
    public function getReportMsgGroups(Request $request)
    {
        $action = 'getReportMsgGroups';
        $data = [];
        $groupsData = [];

        $userGroups = $this->em->getRepository(UserGroup::class)->findBy(['groupFolder' => self::$GROUP_FOLDER_MSG_ID]);

        /** @var UserGroup $userGroup */
        foreach ($userGroups as $userGroup) {
            $groupsData[] = [
                'id' => $userGroup->getId(),
                'name' => $userGroup->getName(),
            ];
        }

        $data['folders'][] = [
            'folder' => [
                'id' => $userGroup->getGroupFolder()->getId(),
                'name' => $userGroup->getGroupFolder()->getName(),
            ],
            'groupArray' => $groupsData,
        ];

        return $this->formHelper->getResponse(
            $action,
            $data
        );
    }

    /**
     * @param  Request  $request
     * @return JsonResponse
     */
    public function getReportMSG(Request $request)
    {
        $action = 'getReportMSG';
        $data = [];
        $fullData = [];
        $userAuthIds = [];
        $users = [];
        $authUsers = [];
        $dateByCourse = [];

        $period = $this->formHelper->toInt($request->get('period', 0));
        $year = $this->formHelper->toInt($request->get('year', 0));

        if ($period > 0 && $year >= 2019 && $year <= date('Y')) {
            $userGroups = [];

            foreach (self::$PERIOD_TO_MONTH[$period] as $month) {
                $monthStart = $month - 1;
                if ($monthStart == 0) {
                    $monthStart = 12;
                }
                $dateStart = DateTime::createFromFormat('j-m-Y', '15-' . $monthStart . '-' . $year);
                $dateEnd = DateTime::createFromFormat('j-m-Y', '15-' . $month . '-' . $year);

                if ($groupsIds = $request->get('groupIds', [])) {
                    $groupsIds = explode(',', $groupsIds);
                    $userGroups[$month] = $this->em->getRepository(UserGroup::class)
                        ->getGroupsByGroupFolderAndDateCreate(
                            self::$GROUP_FOLDER_MSG_ID,
                            $dateStart,
                            $dateEnd,
                            $groupsIds
                        );
                } else {
                    $userGroups[$month] = $this->em->getRepository(UserGroup::class)
                        ->getGroupsByGroupFolderAndDateCreate(self::$GROUP_FOLDER_MSG_ID, $dateStart, $dateEnd);
                }
            }

            foreach ($userGroups as $userGroup) {
                if (!empty($userGroup)) {
                    /** @var UserGroup $uG */
                    foreach ($userGroup as $uG) {
                        foreach ($uG->getUserRelations() as $userRelation) {
                            $users[] = $userRelation->getUser();
                            $dateByCourse[] = $userRelation->getCreatedAt();
                        }
                    }
                }
            }

            $ca = 0;
            /** @var User $user */
            foreach ($users as $user) {
                $userId = $user->getId();
                $userAuthId = $user->getAuthUserId();
                $createdAt = $dateByCourse[$ca]->format('d.m.Y');
                $ca++;
                /** @var UserCourseTariff $userCourseTariff */
                $userCourseTariff = $user->getUserCourseTariffs()->filter(
                    function ($element) {
                        if ($element->getCourse()->getFolder()->getId() == self::$COURSE_FOLDER_MSG_ID) {
                            return $element;
                        }
                    }
                )->first();

                if ($userCourseTariff) {
                    $analytics = $this->getDataReportUser($userId, $period, $year, $createdAt, $userCourseTariff);

                    if (!empty($analytics['analytics']['info']['managerAuthId'])) {
                        $managerAuthId = $analytics['analytics']['info']['managerAuthId'];
                        $userAuthIds[$managerAuthId] = $managerAuthId;
                    }
                    if (!empty($analytics['analytics']['info']['trackerAuthId'])) {
                        $trackerAuthId = $analytics['analytics']['info']['trackerAuthId'];
                        $userAuthIds[$trackerAuthId] = $trackerAuthId;
                    }

                    $userAuthIds[$userAuthId] = $userAuthId;

                    /*// @var ReportMSG $reportMSG
                    $reportMSG = $user->getReportsMSG()->filter(
                        function ($entry) use ($period, $year) {
                            if ($entry->getPeriod() == $period && $entry->getYear() == $year) {
                                return true;
                            } else {
                                return false;
                            }
                        }
                    )->first();*/

                    $data['weeks'][] = [
                        'user' => [
                            'authId' => $userAuthId,
                        ],
                        'analytics' => $analytics['analytics'],
                    ];
                }
            }

            $users = $this->authServerHelper->getUsersInfo(array_keys($userAuthIds));

            $analytics = [];
            if (empty($data)) {
                return $this->formHelper->getResponse(
                    $action,
                    $fullData
                );
            }

            foreach ($data['weeks'] as $week) {
                $authId = $week['user']['authId'];
                $managerAuthId = $week['analytics']['info']['managerAuthId'];
                $trackerAuthId = $week['analytics']['info']['trackerAuthId'];
                $isManager = false;
                $isTracker = false;
                $authUser = null;
                /** @var object $user */
                foreach ($users as $user) {
                    if ($authId === $user->id) {
                        $authUsers[$authId] = $this->getDataReportAuthUser($user, $isManager, $isTracker);
                    } elseif ($managerAuthId === $user->id) {
                        $isManager = true;
                        $authUsers[$managerAuthId] = $this->getDataReportAuthUser($user, $isManager, $isTracker);
                    } elseif ($trackerAuthId === $user->id) {
                        $isTracker = true;
                        $authUsers[$trackerAuthId] = $this->getDataReportAuthUser($user, $isManager, $isTracker);
                    }
                }

                $analytics[] = $week['analytics'];
            }

            $fullData = [
                'users' => array_values($authUsers),
                'analytics' => $analytics,
                'tariffs' => $this->getTariffs(),
                'streams' => $this->getStreams($dateByCourse),
            ];
        }

        return $this->formHelper->getResponse(
            $action,
            $fullData
        );
    }

    /**
     * @param  Request  $request
     * @return JsonResponse
     */
    public function getReportMSA(Request $request)
    {
        /** @var User $viewerUser */
        $viewerUser = $this->security->getUser();

        $action = 'getReportMSA';
        $data = [];
        $fullData = [];
        $userAuthIds = [];
        $authUsers = [];
        $dateByCourse = [];

        $dateStart = $request->query->get('date_start', null);
        $dateEnd = $request->query->get('date_end', null);
        $page = (int)$request->get('page', 1);
        $pageSize = (int)$request->get('pageSize', 10);
        $search = $request->query->get('name');

        if ($dateStart) {
            $dateStart = DateTime::createFromFormat('Y-m-d', $dateStart);
        }
        if ($dateEnd) {
            $dateEnd = DateTime::createFromFormat('Y-m-d', $dateEnd);
        }

        if ($groupsIds = $request->get('groupIds', [])) {
            $groupsIds = explode(',', $groupsIds);
            $userGroups = $this->em->getRepository(UserGroup::class)
                ->getGroupsByGroupFolderAndDateCreateMSA(self::$GROUP_FOLDER_MSA_ID, $dateStart, $dateEnd, $groupsIds);
        } else {
            $userGroups = $this->em->getRepository(UserGroup::class)
                ->getGroupsByGroupFolderAndDateCreateMSA(self::$GROUP_FOLDER_MSA_ID, $dateStart, $dateEnd);
        }

        $count = ($page - 1) * $pageSize;
        if ($page > 1) {
            $maxCount = $page * $pageSize;
        } else {
            $maxCount = $pageSize;
        }

        $ugID = 0;
        $ugNowID = 0;
        $lastPageGroupCount = 0;
        $preLastPageGroupCount = 0;
        /** @var UserGroup $userGroup */
        foreach ($userGroups as $userGroup) {
            if ($search) {
                break;
            }
            if ($count > $maxCount) {
                break;
            }
            $countInfo = 0;
            if (array_key_exists('info', $data)) {
                $countInfo = count($data['info']);
            }
            if ($ugID === $ugNowID) {
                $userRelations = $userGroup->getUserRelations()->slice($count, $pageSize);
                if ($count > 0 && count($userRelations) < $pageSize) {
                    $lastPageGroupCount = count($userRelations);
                    $preLastPageGroupCount = count(
                        $userGroup->getUserRelations()->slice($count - $pageSize, $pageSize)
                    );
                }
            } elseif ($countInfo < $pageSize) {
                $page = ($pageSize - $lastPageGroupCount);
                $prePage = ($pageSize - $preLastPageGroupCount);
                if ($page === 25) {
                    $userRelations = $userGroup->getUserRelations()->slice($prePage, $pageSize);
                } else {
                    $userRelations = $userGroup->getUserRelations()->slice(0, $page);
                }
                $ugNowID = $ugID;
            } else {
                $userRelations = [];
            }

            foreach ($userRelations as $userRelation) {
                if ($page && $pageSize && $count > $maxCount) {
                    break;
                }
                $user = $userRelation->getUser();
                $userAuthId = $user->getAuthUserId();
                $analytics = $this->getDataReportsUserMSA($user);

                $userAuthIds[$userAuthId] = $userAuthId;

                if (array_key_exists('analytics', $analytics)) {
                    $data['info'][] = [
                        'user' => [
                            'authId' => $userAuthId,
                        ],
                        'analytics' => $analytics['analytics'],
                    ];
                }

                $count++;
            }
            $ugID++;
        }

        if ($search && $page === 1) {
            /** @var UserGroup $userGroup */
            foreach ($userGroups as $userGroup) {
                foreach ($userGroup->getUserRelations() as $userRelation) {
                    $userAuthIds[] = $userRelation->getUser()->getAuthUserId();
                }
            }
        }

        $users = $this->authServerHelper->getUsersInfo($userAuthIds, $search);

        if ($search && $page === 1) {
            $lmsUsers = $this->em->getRepository(User::class)->findBy(
                [
                    'id' => array_map(
                        function ($e) {
                            return $e->lmsUserId;
                        },
                        $users
                    ),
                ]
            );
            /** @var User $user */
            foreach ($lmsUsers as $user) {
                $userId = $user->getAuthUserId();
                $analytics = $this->getDataReportsUserMSA($user);

                if (array_key_exists('analytics', $analytics)) {
                    $data['info'][] = [
                        'user' => [
                            'authId' => $userId,
                        ],
                        'analytics' => $analytics['analytics'],
                    ];
                }
            }
        }

        $analytics = [];
        if (empty($data)) {
            return $this->formHelper->getResponse(
                $action,
                $fullData
            );
        }

        foreach ($data['info'] as $info) {
            $authId = $info['user']['authId'];
            /** @var object $user */
            foreach ($users as $user) {
                if ($authId === $user->id) {
                    $authUsers[$authId] = $this->getDataReportAuthUser(
                        $user,
                        false,
                        false,
                        null,
                        $viewerUser
                    );
                }
            }

            $analytics[] = $info['analytics'];
        }

        $fullData = [
            'users' => array_values($authUsers),
            'analytics' => $analytics,
        ];

        return $this->formHelper->getResponse(
            $action,
            $fullData
        );
    }

    /**
     * @param  Request  $request
     * @param  User  $user
     * @return JsonResponse
     */
    public function getReportMSAUser(Request $request, User $user, bool $needAuth = true)
    {
        $year = date('Y');
        $year = $request->get('year', $year);

        $groupInfo = $this->em->getRepository(UserGroup::class)
            ->getCreatedAtByGroupFolderAndUser(self::$GROUP_FOLDER_MSA_ID, $user);

        if (!$groupInfo) {
            return $this->formHelper->getResponse('getReportMSAUser');
        }

        if ($needAuth) {
            $authUser = $this->authServerHelper->getUsers(
                [$user->getAuthUserId()],
                ['id', 'name', 'lastname', 'patronymic', 'avatar', 'email', 'city', 'phone', 'niche']
            );
            $userData = $authUser;
        } else {
            $userData = [
                'id' => $user->getId(),
                'name' => $user->getName(),
                'lastname' => $user->getLastname(),
                'patronymic' => $user->getPatronymic(),
                'avatar' => $user->getAvatar(),
                'email' => $user->getEmail(),
                'city' => $user->getCity(),
                'phone' => $user->getPhone(),
                'niche' => $user->getNiche()
            ];
        }

        $qb = $this->em->getRepository(ReportMSG::class)->createQueryBuilder('rm');
        $qb
            ->select('rm, user, cells')
            ->innerJoin('rm.user', 'user')
            ->innerJoin('rm.cells', 'cells')
            ->where('rm.period = :period')
            ->setParameter(':period', 0)
            ->andWhere('rm.year = :year')
            ->setParameter(':year', $year)
            ->andWhere('rm.user = :user')
            ->setParameter(':user', $user);

        $reportsMSG = $qb->addOrderBy('cells.weekId')->addOrderBy('rm.channelId')->getQuery()->getResult();

        if (count($reportsMSG) == 0) {
            $this->createEmptyRowsReportMSA($user, 0, $year);
            $reportsMSG = $qb->getQuery()->getResult();
        } elseif (count($reportsMSG) < 4) {
            /** @var ReportMSG $reportMSG */
            foreach ($reportsMSG as $reportMSG) {
                $this->em->remove($reportMSG);
                $this->em->flush();
            }
            $this->createEmptyRowsReportMSA($user, 0, $year);
            $reportsMSG = $qb->getQuery()->getResult();
        }

        $months = [];
        /** @var ReportMSG $reportMSG */
        foreach ($reportsMSG as $reportMSG) {
            $cells = $reportMSG->getCells();
            for ($i = 0; $i <= 12; $i++) {
                $cell = $cells[$i];
                $months[] = [
                    'id' => $reportMSG->getId(),
                    'cellId' => $cell->getId(),
                    'mounthId' => $cell->getWeekId(),
                    'channelId' => $reportMSG->getChannelId(),
                    'channel' => $reportMSG->getChannel(),
                    'profit' => $cell->getProfit(),
                ];
            }
        }
        return $this->formHelper->getResponse(
            'getReportMSAUser',
            ['user' => $userData, 'months' => $months, 'actualPeriod' => date('n') - 1]
        );
    }

    /**
     * @param  Request  $request
     * @return JsonResponse
     */
    public function editReportUser(Request $request)
    {
        $action = 'editMSGReportUser';
        $data = [];
        $reportUserId = null;
        $reportMSG = null;

        if ($analytic = $request->get('analytics', [])) {
            if (!empty($analytic['id'])) {
                $reportUserId = $this->formHelper->toInt($analytic['id']);
            } else {
                $this->formHelper->addError(
                    'msgReportUser',
                    'Id not set in request.'
                );
            }

            if ($this->formHelper->isValid()) {
                /** @var ReportMSG $reportMSG */
                $reportMSG = $this->em->getRepository(ReportMSG::class)
                    ->findOneBy(['id' => $reportUserId]);
            }

            if ($this->formHelper->isValid() && !empty($reportMSG)) {
                $cellId = $this->formHelper->toInt($analytic['weekId']);
                $profit = $this->formHelper->toInt($analytic['profit']);
                $reportMSG->getCells()[$cellId]->setProfit($profit);
                $this->em->persist($reportMSG);

                $this->em->flush();

                $data = ['id' => $reportMSG->getId()];
            }
        }

        return $this->formHelper->getResponse(
            $action,
            $data
        );
    }

    /**
     * @param  Request  $request
     * @return JsonResponse
     */
    public function editReportMSAUser(Request $request)
    {
        $action = 'editReportMSAUser';
        $data = [];
        $reportUserId = null;
        $reportMSG = null;
        $cellId = date('n') - 1;

        if ($cellId == 0) {
            $cellId = 11;
        } else {
            $cellId--;
        }

        $isAdmin = $this->userHelper->isUserAdmin();

        if ($analytics = $request->request->all()) {
            foreach ($analytics as $analytic) {
                if (!empty($analytic['id'])) {
                    $reportUserId = $this->formHelper->toInt($analytic['id']);
                } else {
                    $this->formHelper->addError(
                        'ReportMSAUser',
                        'Id not set in request.'
                    );
                }

                if ($this->formHelper->isValid()) {
                    /** @var ReportMSG $reportMSG */
                    $reportMSG = $this->em->getRepository(ReportMSG::class)
                        ->findOneBy(['id' => $reportUserId]);

                    if (!$isAdmin && $this->security->getUser() !== $reportMSG->getUser()) {
                        $this->formHelper->addError('accessDenied', 'accessDenied');

                        return $this->formHelper->getResponse(
                            $action,
                            $data
                        );
                    }

                    if ($this->formHelper->isValid() && !empty($reportMSG)) {
                        $profit = $this->formHelper->toInt($analytic['profit']);
                        /*if ($reportMSG->getCells()[$cellId]->getProfit() > 0) {
                            continue;
                        }*/
                        $reportMSG->getCells()[$cellId]->setProfit($profit);
                        $this->em->persist($reportMSG);

                        $this->em->flush();

                        $data[] = [
                            'monthId' => $cellId,
                            'channelId' => $reportMSG->getChannelId(),
                            'profit' => $profit,
                        ];
                    }
                }
            }
        }

        return $this->formHelper->getResponse(
            $action,
            $data
        );
    }

    /**
     * @param  Request  $request
     * @return JsonResponse
     */
    public function editCellMSAUser(Request $request)
    {
        $action = 'editCellMSAUser';
        $data = [];
        $lmsUserId = null;
        $cellId = null;

        $isAdmin = $this->userHelper->isUserAdmin();

        if ($analytics = $request->request->all()) {
            foreach ($analytics as $analytic) {
                if (
                    array_key_exists('id', $analytic) &&
                    array_key_exists('cellId', $analytic)
                ) {
                    $lmsUserId = $this->formHelper->toInt($analytic['id']);
                    $cellId = $this->formHelper->toInt($analytic['cellId']);
                } else {
                    $this->formHelper->addError(
                        'ReportCellUser',
                        'Parameters not set in request.'
                    );
                }

                if ($this->formHelper->isValid()) {
                    $user = $this->em->getRepository(User::class)
                        ->findOneBy(['id' => $lmsUserId]);

                    if ($user === null) {
                        $this->formHelper->addError(
                            'ReportCellUser',
                            'User not found.'
                        );
                    }

                    if ($this->formHelper->isValid()) {
                        /** @var ReportMSGCell $tractionCell */
                        $tractionCell = $this->em->getRepository(ReportMSGCell::class)
                            ->findOneBy(['id' => $cellId]);

                        if ($tractionCell === null) {
                            $this->formHelper->addError(
                                'ReportCellUser',
                                'Cell not found.'
                            );
                        }

                        if ($this->formHelper->isValid()) {
                            if (!$isAdmin && $this->security->getUser() !== $tractionCell->getReportMSG()->getUser()) {
                                $this->formHelper->addError('accessDenied', 'accessDenied');
                            }

                            if ($this->formHelper->isValid()) {
                                $profit = null;
                                if ($analytic['profit'] !== '') {
                                    $profit = $this->formHelper->toInt($analytic['profit']);
                                }
                                $tractionCell->setProfit($profit);
                                $this->em->persist($tractionCell);

                                $this->em->flush();

                                $data[] = [
                                    'cellId' => $cellId,
                                    'weekId' => $tractionCell->getWeekId(),
                                    'profit' => $profit,
                                ];
                            }
                        }
                    }
                }
            }
        }

        return $this->formHelper->getResponse(
            $action,
            $data
        );
    }

    /**
     * @param  Request  $request
     * @return JsonResponse|StreamedResponse
     */
    public function getReportMsgCsv(Request $request)
    {
        $period = $this->formHelper->toInt($request->get('period', 0));
        $year = $this->formHelper->toInt($request->get('year', 0));

        if ($period > 0 && $year >= 2019 && $year <= date('Y')) {
            $response = new StreamedResponse(
                function () use ($period, $year, $request) {
                    $data = [];
                    $fullData = [];
                    $userAuthIds = [];
                    $users = [];
                    $authUsers = [];
                    $dateByCourse = [];

                    $userGroups = [];
                    $weekIds = [];
                    $chanelName = [];

                    $dataCsv = [];

                    foreach (self::$PERIOD_TO_MONTH[$period] as $month) {
                        $monthStart = $month - 1;
                        if ($monthStart == 0) {
                            $monthStart = 12;
                        }
                        $dateStart = DateTime::createFromFormat('j-m-Y', '15-' . $monthStart . '-' . $year);
                        $dateEnd = DateTime::createFromFormat('j-m-Y', '15-' . $month . '-' . $year);

                        if ($groupsIds = $request->get('groupIds', [])) {
                            $groupsIds = explode(',', $groupsIds);
                            $userGroups[$month] = $this->em->getRepository(UserGroup::class)
                                ->getGroupsByGroupFolderAndDateCreate(
                                    self::$GROUP_FOLDER_MSG_ID,
                                    $dateStart,
                                    $dateEnd,
                                    $groupsIds
                                );
                        } else {
                            $userGroups[$month] = $this->em->getRepository(UserGroup::class)
                                ->getGroupsByGroupFolderAndDateCreate(self::$GROUP_FOLDER_MSG_ID, $dateStart, $dateEnd);
                        }
                    }

                    foreach ($userGroups as $userGroup) {
                        if (!empty($userGroup)) {
                            /** @var UserGroup $uG */
                            foreach ($userGroup as $uG) {
                                foreach ($uG->getUserRelations() as $userRelation) {
                                    $users[] = $userRelation->getUser();
                                    $dateByCourse[] = $userRelation->getCreatedAt();
                                }
                            }
                        }
                    }

                    $ca = 0;
                    /** @var User $user */
                    foreach ($users as $user) {
                        $userId = $user->getId();
                        $userAuthId = $user->getAuthUserId();
                        $createdAt = $dateByCourse[$ca]->format('d.m.Y');
                        $ca++;
                        /** @var UserCourseTariff $userCourseTariff */
                        $userCourseTariff = $user->getUserCourseTariffs()->filter(
                            function ($element) {
                                if ($element->getCourse()->getFolder()->getId() == self::$COURSE_FOLDER_MSG_ID) {
                                    return $element;
                                }
                            }
                        )->first();

                        if ($userCourseTariff) {
                            $analytics = $this->getDataReportUser(
                                $userId,
                                $period,
                                $year,
                                $createdAt,
                                $userCourseTariff
                            );

                            if (!empty($analytics['analytics']['info']['managerAuthId'])) {
                                $managerAuthId = $analytics['analytics']['info']['managerAuthId'];
                                $userAuthIds[$managerAuthId] = $managerAuthId;
                            }
                            if (!empty($analytics['analytics']['info']['trackerAuthId'])) {
                                $trackerAuthId = $analytics['analytics']['info']['trackerAuthId'];
                                $userAuthIds[$trackerAuthId] = $trackerAuthId;
                            }
                            $userAuthIds[$userAuthId] = $userAuthId;

                            $data['weeks'][] = [
                                'user' => [
                                    'authId' => $userAuthId,
                                ],
                                'analytics' => $analytics['analytics'],
                            ];
                        }
                    }

                    $users = $this->authServerHelper->getUsersInfo(array_keys($userAuthIds));

                    if (empty($data)) {
                        return $this->formHelper->getResponse(
                            'getReportMsgCsv',
                            $dataCsv
                        );
                    }

                    foreach ($data['weeks'] as $week) {
                        $analytics = [];
                        $authId = $week['user']['authId'];
                        $managerAuthId = $week['analytics']['info']['managerAuthId'];
                        $trackerAuthId = $week['analytics']['info']['trackerAuthId'];
                        $createdAt = $week['analytics']['info']['createdAt'];
                        $isManager = false;
                        $isTracker = false;
                        $authUser = null;
                        $nameUser = '';
                        $emailUser = '';
                        $vkUser = '';
                        $nameManager = '';
                        $nameTracker = '';
                        $profitStart = 0;
                        $profitAll = 0;
                        /** @var object $user */
                        foreach ($users as $user) {
                            if ($authId === $user->id) {
                                $authUsers[$authId] = $this->getDataReportAuthUser($user, $isManager, $isTracker);
                                $nameUser = $authUsers[$authId]['name'] . " " . $authUsers[$authId]['lastname'];
                                $emailUser = $authUsers[$authId]['email'];
                                $vkUser = $authUsers[$authId]['vkLink'];
                                $profitStart = $this->formHelper->toInt($authUsers[$authId]['profitPerLastYear']);
                            } elseif ($managerAuthId === $user->id) {
                                $isManager = true;
                                $authUsers[$managerAuthId] = $this->getDataReportAuthUser(
                                    $user,
                                    $isManager,
                                    $isTracker
                                );
                                $nameManager = sprintf(
                                    "%s %s",
                                    $authUsers[$managerAuthId]['name'],
                                    $authUsers[$managerAuthId]['lastname']
                                );
                            } elseif ($trackerAuthId === $user->id) {
                                $isTracker = true;
                                $authUsers[$trackerAuthId] = $this->getDataReportAuthUser(
                                    $user,
                                    $isManager,
                                    $isTracker
                                );
                                $nameTracker = sprintf(
                                    "%s %s",
                                    $authUsers[$trackerAuthId]['name'],
                                    $authUsers[$trackerAuthId]['lastname']
                                );
                            }
                        }

                        foreach ($week['analytics']['weeks'] as $cell) {
                            $analytics[] = $cell['profit'];
                            if (!in_array($cell['weekId'], $weekIds)) {
                                $weekIds[] = $cell['weekId'];
                            }
                            if (!in_array($cell['channel'], $chanelName)) {
                                $chanelName[] = $cell['channel'];
                            }
                            $profitAll += (int)$cell['profit'];
                        }
                        $fullData[] = array_merge(
                            [
                                $nameUser,
                                $vkUser,
                                $emailUser,
                                $createdAt,
                                $nameManager,
                                $nameTracker,
                                $profitStart + $profitAll,
                                $profitStart,
                            ],
                            array_values($analytics)
                        );
                    }

                    $weeksData = [];
                    foreach ($chanelName as $cName) {
                        $idMonth = 0;
                        foreach ($weekIds as $wId) {
                            $nowMonth = self::$PERIOD_TO_MONTH[$period][$idMonth];
                            if (($wId % 2) == 0) {
                                $weeksData[] = $cName . ' 1.' . $nowMonth;
                            } else {
                                $weeksData[] = $cName . ' 16.' . $nowMonth;
                                $idMonth++;
                            }
                            if (($wId % 11) === 0) {
                                $idMonth = 0;
                            }
                        }
                    }

                    $dataCsv[0] = [
                        'ФИО',
                        'VK',
                        'email',
                        'На программе с...',
                        'Менеджер участника',
                        'Трекер участника',
                        'Денег заработано всего',
                        'Общий заработок до начала трекинга',
                    ];
                    $dataCsv[0] = array_merge($dataCsv[0], array_values($weeksData));

                    foreach ($fullData as $rowData) {
                        $dataCsv[] = $rowData;
                    }

                    $handle = fopen('php://output', 'w+');
                    fprintf($handle, chr(0xEF) . chr(0xBB) . chr(0xBF));

                    foreach ($dataCsv as $row) {
                        fputcsv($handle, $row, ';');
                    }

                    fclose($handle);
                }
            );

            $response->headers->set('Content-Encoding', 'UTF-8');
            $response->headers->set('Content-Type', 'text/csv; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment; filename=export-msg.csv');

            return $response;
        } else {
            $this->formHelper->addError('getReportMsgCsv', 'Year and Period not set in request');

            return $this->formHelper->getResponse(
                'getReportMsgCsv',
                []
            );
        }
    }

    /**
     * @param  Request  $request
     * @return JsonResponse|StreamedResponse
     */
    public function getReportMsaCsv(Request $request)
    {
        $months = $request->get('months', [date('m') - 1]);
        $year = $this->formHelper->toInt($request->get('year', date('Y')));
        $fullData = [];
        $userAuthIds = [];

        $monthsIds = [];
        $monthIds = [];
        $chanelName = [];
        $monthsData = [];

        if ($months && $year >= 2019 && $year <= date('Y')) {
            foreach ($months as $month) {
                $month = $this->formHelper->toInt($month);
                $monthId = $month - 1;
                if ($monthId < 0) {
                    $monthId = 11;
                }
                $monthsIds[] = $monthId;
            }
            /** @var QueryBuilder $qb */
            $qb = $this->em->getRepository(ReportMSG::class)->createQueryBuilder('r');
            $qb
                ->select('r, user, cells')
                ->innerJoin('r.user', 'user')
                ->innerJoin('r.cells', 'cells')
                ->where('r.period = 0')
                ->andWhere('cells.weekId IN (:monthIds)')
                ->setParameter('monthIds', $monthsIds)
                ->andWhere('r.year = :year')
                ->setParameter('year', $year);

            $reportsMsa = $qb->getQuery()->getResult();
            /** @var ReportMSG $reportMsa */
            foreach ($reportsMsa as $reportMsa) {
                $userAuthId = $reportMsa->getUser()->getAuthUserId();
                if (!in_array($userAuthId, $userAuthIds)) {
                    $userAuthIds[] = $userAuthId;
                }
            }

            $authUsers = $this->authServerHelper->getUsersInfo($userAuthIds);

            foreach ($authUsers as $user) {
                $analytics = [];
                $authId = $user->id;
                $userRM = array_filter(
                    $reportsMsa,
                    function ($e) use ($authId) {
                        if ($e->getUser()->getAuthUserId() === $authId) {
                            return $e;
                        }
                    }
                );
                /** @var ReportMSG $reportMsa */
                foreach ($userRM as $reportMsa) {
                    if (!in_array($reportMsa->getChannel(), $chanelName)) {
                        $chanelName[] = $reportMsa->getChannel();
                    }
                    foreach ($reportMsa->getCells() as $cell) {
                        if (!in_array($cell->getWeekId(), $monthIds)) {
                            $monthIds[] = $cell->getWeekId();
                        }
                        $analytics[] = $cell->getProfit();
                    }
                }

                $fullData[] = array_merge(
                    [
                        $user->lmsUserId,
                        array_key_exists('businessCategoryName', $user) ? $user->businessCategoryName : 'Нет бизнеса',
                        $user->lastname,
                        $user->name,
                        $user->patronymic,
                        $user->vkLink,
                        $user->email,
                    ],
                    array_values($analytics)
                );
            }

            foreach ($chanelName as $cName) {
                foreach ($monthIds as $monthId) {
                    $date_m = [
                        'январь',
                        'февраль',
                        'март',
                        'апрель',
                        'май',
                        'июнь',
                        'июль',
                        'август',
                        'сентябрь',
                        'октябрь',
                        'ноябрь',
                        'декабрь',
                    ];
                    $nowMonth = $date_m[$monthId];
                    $monthsData[] = $cName . ' за ' . $nowMonth;
                }
            }
        }

        if ($monthsData && $fullData) {
            $response = new StreamedResponse(
                function () use ($monthsData, $fullData) {
                    $dataCsv[0] = [
                        'userId',
                        'Сфера Бизнеса',
                        'Фамилия',
                        'Имя',
                        'Отчество',
                        'VK',
                        'email',
                    ];
                    $dataCsv[0] = array_merge($dataCsv[0], array_values($monthsData));

                    foreach ($fullData as $rowData) {
                        $dataCsv[] = $rowData;
                    }

                    $handle = fopen('php://output', 'w+');
                    fprintf($handle, chr(0xEF) . chr(0xBB) . chr(0xBF));

                    foreach ($dataCsv as $row) {
                        fputcsv($handle, $row, ';');
                    }

                    fclose($handle);
                }
            );

            $response->headers->set('Content-Encoding', 'UTF-8');
            $response->headers->set('Content-Type', 'text/csv; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment; filename=export-msa.csv');

            return $response;
        } else {
            $this->formHelper->addError('getReportMsaCsv', 'Year and Period not set in request');

            return $this->formHelper->getResponse(
                'getReportMsaCsv',
                []
            );
        }
    }

    /**
     * @param  int  $userId
     * @param  int  $period
     * @param  int  $year
     * @param  string  $createdAt
     * @param  UserCourseTariff  $userCourseTariff
     * @return array
     */
    private function getDataReportUser(
        int $userId,
        int $period,
        int $year,
        string $createdAt,
        ?UserCourseTariff $userCourseTariff = null
    ) {
        $data = [];

        $reportsMSG = $this->getReportsMSG($userId, $period, $year);
        /** @var ReportMSG $reportMSG */
        foreach ($reportsMSG as $reportMSG) {
            $data['analytics']['info'] = [
                'managerId' => $userCourseTariff ? $userCourseTariff->getManager() ?
                    $userCourseTariff->getManager()->getId() : '' : '',
                'managerAuthId' => $userCourseTariff ? $userCourseTariff->getManager() ?
                    $userCourseTariff->getManager()->getAuthUserId() : '' : '',

                'trackerId' => $userCourseTariff ? $userCourseTariff->getTracker() ?
                    $userCourseTariff->getTracker()->getId() : '' : '',
                'trackerAuthId' => $userCourseTariff ? $userCourseTariff->getTracker() ?
                    $userCourseTariff->getTracker()->getAuthUserId() : '' : '',

                'userId' => $userId,
                'createdAt' => $createdAt,
                'weeksMaxCount' => count($reportMSG->getCells()),

                'tariffId' => $userCourseTariff ? $userCourseTariff->getTariff()->getId() : '',
                'streamId' => $this->getStreamId($createdAt),
                'period' => $reportMSG->getPeriod(),
                'year' => $reportMSG->getYear(),
            ];
            foreach ($reportMSG->getCells() as $cell) {
                $data['analytics']['weeks'][] = [
                    'id' => $reportMSG->getId(),
                    'weekId' => $cell->getWeekId(),
                    'channelId' => $reportMSG->getChannelId(),
                    'channel' => $reportMSG->getChannel(),
                    'profit' => $cell->getProfit(),
                ];
            }
        }

        return $data;
    }

    private function getDataReportsUserMSA(User $user): array
    {
        $data = [];
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(ReportMSG::class)->createQueryBuilder('r');
        $qb
            ->select('r.channel', 'r.channelId', 'c.profit')
            ->innerJoin('r.user', 'u')
            ->innerJoin('r.cells', 'c')
            ->andWhere('r.user = :user')
            ->andWhere('c.weekId != -1')
            ->setParameter('user', $user);

        $reportsMSG = $qb->getQuery()->getResult();

        if (count($reportsMSG) < 4) {
            // не уверен зачем это нужно, но оставлю как было
            // @todo: залогировать такие кейсы и выпилить этот код
            /** @var ReportMSG $reportsMSG */
            $reportsMSG = $this->em->getRepository(ReportMSG::class)->findBy(['user' => $user]);
            foreach ($reportsMSG as $report) {
                // удаляем все невалидные отчеты
                $report->remove();
                $this->em->flush();
            }

            // создаем заново
            $this->createEmptyRowsReportMSA($user);
            $reportsMSG = $qb->getQuery()->getResult();
        }

        $sumRowsNPS = 0;
        $info = [
            ['channel' => 'Выручка', 'channelId' => 0, 'fullProfit' => 0],
            ['channel' => 'Прибыль', 'channelId' => 1, 'fullProfit' => 0],
            ['channel' => 'NPS', 'channelId' => 2, 'fullProfit' => 0],
            ['channel' => 'Кол-во точек', 'channelId' => 3, 'fullProfit' => 0]
        ];

        foreach ($reportsMSG as $report) {
            switch ($report['channelId']) {
                case 0:
                    $info[0]['fullProfit'] += $report['profit'];
                    break;
                case 1:
                    $info[1]['fullProfit'] += $report['profit'];
                    break;
                case 2:
                    $info[2]['fullProfit'] += $report['profit'];
                    if ($report['profit'] > 0) {
                        $sumRowsNPS++;
                    }
                    break;
                case 3:
                    $info[3]['fullProfit'] += $report['profit'];
                    break;
            }
        }

        if ($sumRowsNPS > 0) {
            $info[2]['fullProfit'] = intval($info[2]['fullProfit'] / $sumRowsNPS);
        }

        $data['analytics'] = ['userId' => $user->getId()];
        $data['analytics']['info'] = $info;

        return $data;
    }

    /**
     * @param  int  $userId
     * @param  int  $period
     * @param  int  $year
     * @return array|object[]
     */
    private function getReportsMSG(int $userId, int $period, int $year)
    {
        /** @var array $reportsMSG */
        $reportsMSG = $this->em->getRepository(ReportMSG::class)
            ->findBy(['user' => $userId, 'period' => $period, 'year' => $year]);

        if (count($reportsMSG) == 0) {
            $this->createEmptyRowsReportMSG($userId, $period, $year);
        } elseif (count($reportsMSG) < 4) {
            /** @var ReportMSG $reportMSG */
            foreach ($reportsMSG as $reportMSG) {
                $this->em->remove($reportMSG);
                $this->em->flush();
            }
            $this->createEmptyRowsReportMSG($userId, $period, $year);
        }

        $reportsMSG = $this->em->getRepository(ReportMSG::class)
            ->findBy(['user' => $userId, 'period' => $period, 'year' => $year]);

        return $reportsMSG;
    }

    /**
     * @param  int  $userId
     * @param  int  $period
     * @param  null|int  $year
     */
    private function createEmptyRowsReportMSG(int $userId, ?int $period = null, ?int $year = null): void
    {
        /** @var User $user */
        $user = $this->em->getRepository(User::class)->find($userId);
        $this->createEmptyReportMSG($user, 'Чистая прибыль', 0, $period, $year);
        $this->createEmptyReportMSG($user, 'Выручка', 1, $period, $year);
        $this->createEmptyReportMSG($user, 'Привлечено инвестиций', 2, $period, $year);
        $this->createEmptyReportMSG($user, 'Кэшаут', 3, $period, $year);
        $this->em->flush();
    }

    /**
     * @param  User  $user
     * @param  int  $period
     * @param  null|int  $year
     */
    private function createEmptyRowsReportMSA(User $user, ?int $period = null, ?int $year = null): void
    {
        $this->createEmptyReportMSG($user, 'Выручка', 0, $period, $year);
        $this->createEmptyReportMSG($user, 'Прибыль', 1, $period, $year);
        $this->createEmptyReportMSG($user, 'NPS', 2, $period, $year);
        $this->createEmptyReportMSG($user, 'Кол-во точек', 3, $period, $year);
        $this->em->flush();
    }

    /**
     * @param  User  $user
     * @param  string  $channel
     * @param  int  $channelId
     * @param  null|int  $period
     * @param  null|int  $year
     */
    private function createEmptyReportMSG(
        User $user,
        string $channel,
        int $channelId,
        ?int $period = null,
        ?int $year = null
    ): void {
        if (!$year) {
            $year = (int) date('Y');
        }

        $cnt = -1;
        $reportMSG = new ReportMSG();
        $reportMSG->setUser($user);
        $reportMSG->setChannel($channel);
        $reportMSG->setChannelId($channelId);
        $reportMSG->setPeriod($period ?? 0);
        $reportMSG->setYear($year);
        while ($cnt <= 11) {
            $newCell = new ReportMSGCell();
            $newCell->setWeekId($cnt);
            $newCell->setProfit(null);
            $reportMSG->addCell($newCell);

            $this->em->persist($newCell);
            $cnt++;
        }
        $this->em->persist($reportMSG);
    }

    /**
     * @return array
     */
    private function getTariffs()
    {
        $data = [];
        $courses = $this->em->getRepository(Course::class)->findBy(['folder' => self::$COURSE_FOLDER_MSG_ID]);
        $userTariffs = $this->em->getRepository(UserTariff::class)->findBy(['course' => $courses]);

        /** @var UserTariff $tariff */
        foreach ($userTariffs as $tariff) {
            $data[] = [
                'id' => $tariff->getId(),
                'name' => $tariff->getName(),
            ];
        }

        return $data;
    }

    /**
     * @param  array  $groups
     * @return array
     */
    private function getStreams(array $groups)
    {
        $data = [];
        $monthArray = [];

        /** @var UserTariff $tariff */
        foreach ($groups as $group) {
            $createdAt = $group->format('d.m.Y');
            $dateSplit = explode('.', $createdAt);
            $day = $dateSplit[0];
            $month = $dateSplit[1];
            if ($day > '15') {
                if ($month == 12) {
                    $month = 1;
                } else {
                    $month++;
                }
            }
            if (!in_array($month, $data)) {
                $monthArray[$month] = [
                    'id' => $month,
                    'name' => "Поток " . $month,
                ];
            }
        }

        foreach ($monthArray as $value) {
            $data[] = $value;
        }

        return $data;
    }

    /**
     * @return JsonResponse
     */
    public function getStreamsMSA()
    {
        $data = [];

        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(UserGroup::class)->createQueryBuilder('g');
        $qb
            ->select('g.id, g.name')
            ->where('g.groupFolder = :groupFolderId')
            ->setParameter('groupFolderId', $this::$GROUP_FOLDER_MSA_ID)
            ->andWhere('g.name NOT IN (:names)')
            ->setParameter('names', ['Трекеры МСА', 'Мастера МСА']);

        $userGroups = $qb->getQuery()->getResult();

        foreach ($userGroups as $value) {
            $data[] = [
                'id' => $value['id'],
                'name' => $value['name'],
            ];
        }

        return $this->formHelper->getResponse(
            'getReportMsgCsv',
            $data
        );
    }

    /**
     * @param  string  $createdAt
     * @return int
     */
    private function getStreamId(string $createdAt)
    {
        $data = 0;

        $dateSplit = explode('.', $createdAt);
        $day = $dateSplit[0];
        $month = $dateSplit[1];
        if ($day > '15') {
            if ($month == 12) {
                $data = 1;
            } else {
                $data = $month + 1;
            }
        } else {
            $data = $month;
        }

        return $data;
    }

    /**
     * @param  object  $authUser
     * @param  bool  $isManager
     * @param  bool  $isTracker
     * @param  int|null  $pointsOfSale
     * @return array
     */
    private function getDataReportAuthUser(
        object $authUser,
        bool $isManager,
        bool $isTracker,
        ?int $pointsOfSale = null,
        ?User $user = null
    ) {

        $array = [
            'id' => property_exists($authUser, 'lmsUserId') ? $authUser->lmsUserId : "",
            'authUserId' => property_exists($authUser, 'id') ? $authUser->id : "",
            'name' => property_exists($authUser, 'name') ? $authUser->name : "",
            'lastname' => property_exists($authUser, 'lastname') ? $authUser->lastname : "",
            'patronymic' => property_exists($authUser, 'patronymic') ? $authUser->patronymic : "",
        ];

        if ($isManager || $isTracker || ($user && $user->getId() === $authUser->lmsUserId)) {
            $array = array_merge($array, [
                'avatar' => property_exists($authUser, 'avatar') ? $authUser->avatar : "",
                'email' => property_exists($authUser, 'email') ? $authUser->email : "",
                'vkLink' => property_exists($authUser, 'vkLink') ? $authUser->vkLink : "",
                'fbLink' => property_exists($authUser, 'fbLink') ? $authUser->fbLink : "",
                'instaLink' => property_exists($authUser, 'instaLink') ? $authUser->instaLink : "",
                'siteLink' => property_exists($authUser, 'siteLink') ? $authUser->siteLink : "",
                'niche' => property_exists($authUser, 'niche') ? $authUser->niche : "",
                'businessImg' => property_exists($authUser, 'businessImg') ? $authUser->nicheImg : "",
                'achievement' => property_exists($authUser, 'achievement') ? $authUser->achievement : 0,
                'phone' => property_exists($authUser, 'phone') ? $authUser->phone : "",
                'city' => property_exists($authUser, 'city') ? $authUser->city : "",
                'isManager' => $isManager,
                'isTracker' => $isTracker,
                'profitPerYear' => property_exists($authUser, 'profitPerYear') ? $authUser->profitPerYear : "",
                'profitPerLastYear' => property_exists($authUser, 'profitPerLastYear')
                    ? $authUser->profitPerLastYear
                    : "",
                'pointsOfSale' => ($pointsOfSale) ? $pointsOfSale : 0,
            ]);
        }

        return $array;
    }
}

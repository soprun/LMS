<?php

namespace App\Service\Analytics;

use App\Entity\Questionnaire\Question;
use App\Entity\Questionnaire\UserAnswer;
use App\Entity\SmartSender\SmartSenderUser;
use App\Entity\Team;
use App\Entity\UserGroupUser;
use App\Service\AuthServerHelper;
use App\Service\AuthServerService;
use App\Service\LikecentreDatabaseHelper;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Query\Expr\Join;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Exception;

class TractionReportService
{
    public const CSV_HEADERS = [
        'ID',
        'Email',
        'ФИО',
        'Телефон',
        'Ссылка на сделку в АМО',
        'Группы',
        'Удаленные Группы',
        'Актуальная группа',
        'Факультеты',
        'Факультеты Актуальная группа',
        'Команды',
        'Команды Актуальная группа',
        'Данные анкеты',
        'Купил',
        'Открыт доступ на платформу',
        'Добавлен в группу',
        'Перешел в другую группу',
        'Зашел на платформу',
        'Начал анкету',
        'Завершил анкету (привязан бот)',
        'Распределился на факультет',
        'Альтернативный факультет',
        'Распределился на команду',
        'Альтернативная команда',
        'Сделал возврат',
        'Закрыт доступ',
    ];

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * В AuthServerService не хватает возможности искать только по email
     *
     * @var AuthServerHelper
     */
    private $authServerHelper;

    /**
     * @var AuthServerService
     */
    private $authServerService;

    /**
     * @var LikecentreDatabaseHelper
     */
    private $likecentreDatabaseHelper;

    /**
     * @var LoggerInterface
     */
    private $logger;

    private const REFUND_AMO_ID = 25700235;
    private const CLOSED_AMO_ID = 143;

    /**
     * @param EntityManagerInterface $em
     * @param AuthServerHelper $authServerHelper
     */
    public function __construct(
        EntityManagerInterface $em,
        AuthServerHelper $authServerHelper,
        AuthServerService $authServerService,
        LikecentreDatabaseHelper $likecentreDatabaseHelper,
        LoggerInterface $logger
    ) {
        $this->em = $em;
        $this->authServerHelper = $authServerHelper;
        $this->authServerService = $authServerService;
        $this->likecentreDatabaseHelper = $likecentreDatabaseHelper;
        $this->logger = $logger;
    }

    public function getLCUsers(int $productId): array
    {
        // берем из амо емейлы клиентов
        $sql = <<<SQL
            SELECT DISTINCT c.email, l.amo_lead_id, l.amo_status_id
            FROM leads as l
                INNER JOIN contacts c ON l.contact_id = c.id
                LEFT JOIN lead_status_histories lsh on
                    l.amo_lead_id = lsh.amo_lead_id
            WHERE l.amo_product_id = {$productId}
                AND (
                    l.amo_status_id in(142, 25700235) OR
                    (l.amo_status_id = 143 AND lsh.amo_status_id = 25700235)
                )
                AND l.deleted_at IS NULL
            GROUP BY lsh.id, c.email, l.amo_lead_id, l.amo_status_id
            ORDER BY l.amo_lead_id DESC
        SQL;

        return $this->likecentreDatabaseHelper->execute($sql);
    }

    /**
     * @param array $lcUsers
     * @param int $courseId
     * @param Request $request
     *
     * @return array
     * @throws \App\Exceptions\AuthServerServiceException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getUsersAndCount(array $lcUsers, int $courseId, Request $request): array
    {
        $this->authServerService->setUserAccessToken($request->headers->get('Authorization'));

        $query = "'" . implode("', '", array_column($lcUsers, 'email')) . "'";

        $authUsers = $this->authServerHelper->getUsers(
            [],
            ['id', 'name', 'email', 'lastname', 'patronymic', 'phone'],
            "u.email IN({$query})"
        );
        $userIds = array_column($authUsers, 'id');

        $groups = $this->getUsersGroups($userIds, $courseId);
        $questions = $this->getUsersQuestions($userIds, $courseId);
        $teams = $this->getUsersTeams($userIds, $courseId);
        $alternativeCourseId = array_column($groups, 'alternativeCourseId')[0];
        $alternativeTeams = $this->getUsersTeams($userIds, $alternativeCourseId);
        $bots = $this->getUserBots($userIds);

        try {
            $sessions = $this->authServerService->checkSessionByUserId($userIds)['data']['users'];
        } catch (Exception $exception) {
            $sessions = [];
            $this->logger->error('Не удалось найти сессии', [
                'user_ids' => $userIds,
                'message' => $exception->getMessage(),
                'method' => __METHOD__,
            ]);
        }

        $users = [];
        $count = [
            'access' => 0,
            'hasGroup' => 0,
            'hasAlternativeGroup' => 0,
            'enter' => 0,
            'startQuestionnaire' => 0,
            'endQuestionnaire' => 0,
            'hasFaculty' => 0,
            'hasAlternativeFaculty' => 0,
            'hasTeam' => 0,
            'hasAlternativeTeam' => 0,
            'refund' => 0,
            'deleted' => 0
        ];

        $emails = array_map('mb_strtolower', array_column($authUsers, 'email'));
        foreach ($lcUsers as $lcUser) {
            $authUser = [];
            $group = [];
            $team = [];
            $answers = [];
            $bot = false;
            $authKey = array_search(mb_strtolower($lcUser['email']), $emails);
            if ($authKey !== false) {
                $authUser = $authUsers[$authKey];
                $group = $groups[$authUser['id']] ?? [];
                $team = $teams[$authUser['id']] ?? [];
                $alternativeTeam = $alternativeTeams[$authUser['id']] ?? [];
                $answers = $this->getAnswers($questions, $authUser['id']);
                $bot = $bots[$authUser['id']] ?? false;
            }

            $user = [
                'id' => $authUser['id'] ?? '',
                'email' => str_replace(';', '', $lcUser['email']),
                'name' => str_replace(';', '', sprintf(
                    '%s %s %s',
                    $authUser['name'] ?? '',
                    $authUser['lastname'] ?? '',
                    $authUser['patronymic'] ?? ''
                )),
                'phone' => $authUser['phone'] ?? '',
                'amoLink' => $lcUser['amo_lead_id'] ?
                    sprintf(
                        'https://likebusiness.amocrm.ru/leads/detail/%s',
                        $lcUser['amo_lead_id']
                    )
                    : '',
                'groups' => '',
                'deletedGroups' => '',
                'alternativeGroup' => '',
                'faculties' => '',
                'alternativeFaculties' => '',
                'teams' => '',
                'alternativeTeams' => '',
                'answers' => '',
                'bought' => 1, // Всегда куплено, иначе не попадает в список
                'access' => 0,
                'hasGroup' => 0,
                'hasAlternativeGroup' => 0,
                'enter' => 0,
                'startQuestionnaire' => 0,
                'endQuestionnaire' => 0,
                'hasFaculty' => 0,
                'hasAlternativeFaculty' => 0,
                'hasTeam' => 0,
                'hasAlternativeTeam' => 0,
                'refund' => 0,
                'deleted' => 0
            ];

            // Перечисление всех названий групп
            if (isset($group['groupNames']) && !empty($group['groupNames'])) {
                foreach ($group['groupNames'] as $id => $name) {
                    if ($name) {
                        if (isset($group['deleted'][$id]) && $group['deleted'][$id]) {
                            $user['deletedGroups'] .= $name . ', ';
                        } else {
                            $user['groups'] .= $name . ', ';
                        }
                    }
                }
                $user['groups'] = rtrim($user['groups'], ', ');
                $user['deletedGroups'] = rtrim($user['deletedGroups'], ', ');

                // Если удален из всех перечисленных групп, добавляем альтернативные группы если есть
                if (
                    isset($group['deleted']) &&
                    count(array_filter($group['deleted'])) === count($group['groupNames'])
                ) {
                    if (isset($group['alternativeGroupNames']) && !empty($group['alternativeGroupNames'])) {
                        foreach ($group['alternativeGroupNames'] as $name) {
                            $user['alternativeGroup'] .= $name . ', ';
                        }

                        $user['alternativeGroup'] = rtrim($user['alternativeGroup'], ', ');
                        $user['hasAlternativeGroup'] = 1;
                        $count['hasAlternativeGroup'] += 1;
                    }

                    // Перечисление всех названий факультетов, группы в которую перешел пользователь
                    if (isset($group['alternativeFacultyNames'])) {
                        foreach ($group['alternativeFacultyNames'] as $name) {
                            if ($name) {
                                $user['alternativeFaculties'] .= $name . ', ';
                            }
                        }
                        $user['alternativeFaculties'] = rtrim($user['alternativeFaculties'], ', ');

                        $user['hasAlternativeFaculty'] = 1;
                        $count['hasAlternativeFaculty'] += 1;
                    }

                    // Перечисление всех названий команд, группы в которую перешел пользователь
                    if (isset($alternativeTeam['names'])) {
                        foreach ($alternativeTeam['names'] as $name) {
                            if ($name) {
                                $user['alternativeTeams'] .= $name . ', ';
                            }
                        }
                        $user['alternativeTeams'] = rtrim($user['alternativeTeams'], ', ');

                        $user['hasAlternativeTeam'] = 1;
                        $count['hasAlternativeTeam'] += 1;
                    }
                }

                $user['hasGroup'] = 1;
                $count['hasGroup'] += 1;
            }

            // Перечисление всех названий факультетов
            if (isset($group['facultyNames'])) {
                foreach ($group['facultyNames'] as $name) {
                    if ($name) {
                        $user['faculties'] .= $name . ', ';
                    }
                }
                $user['faculties'] = rtrim($user['faculties'], ', ');

                $user['hasFaculty'] = 1;
                $count['hasFaculty'] += 1;
            }

            // Перечисление всех названий команд
            if (isset($team['names'])) {
                foreach ($team['names'] as $name) {
                    if ($name) {
                        $user['teams'] .= $name . ', ';
                    }
                }
                $user['teams'] = rtrim($user['teams'], ', ');

                $user['hasTeam'] = 1;
                $count['hasTeam'] += 1;
            }

            // Есть ли на аутхе и заходил ли
            if (isset($authUser['id']) && !empty($authUser['id'])) {
                $user['access'] = 1;
                $count['access'] += 1;

                if (in_array($authUser['id'], $sessions)) {
                    $user['enter'] = 1;
                    $count['enter'] += 1;
                }
            }

            // Ответы на анкету
            if (count($answers) > 0) {
                $user['startQuestionnaire'] = 1;
                $count['startQuestionnaire'] += 1;

                /** @var UserAnswer $answer */
                foreach ($answers as $answer) {
                    $user['answers'] .= 'Вопрос: ' . $answer->getQuestion()->getTitle() .
                        ' Ответ: ' .
                        (
                        $answer->getText() !== null ?
                            str_replace(';', ',', str_replace("\n", '', $answer->getText()))
                            : $answer->getVariant()->getTitle()
                        ) . ', ';
                }

                // Если пользователь начал заполнять анкету проверяем есть связка его с ботом
                if ($bot) {
                    $user['endQuestionnaire'] = 1;
                    $count['endQuestionnaire'] += 1;
                }
            }

            // Ставим сделка в возврате или нет
            if (
                (int)$lcUser['amo_status_id'] === self::REFUND_AMO_ID ||
                (int)$lcUser['amo_status_id'] === self::CLOSED_AMO_ID
            ) {
                $user['refund'] = 1;
                $count['refund'] += 1;

                if (!empty($group['alternativeGroupNames'])) {
                    // Если удален из всех альтернативных групп, ставим как удален
                    if (
                        isset($group['alternativeDeleted']) &&
                        count(array_filter($group['alternativeDeleted'])) === count($group['alternativeGroupNames'])
                    ) {
                        $user['deleted'] = 1;
                        $count['deleted'] += 1;
                    }
                } else {
                    // Если удален из всех групп, ставим как удален
                    if (
                        isset($group['deleted']) &&
                        count(array_filter($group['deleted'])) === count($group['groupNames'])
                    ) {
                        $user['deleted'] = 1;
                        $count['deleted'] += 1;
                    }
                }
            }

            $users[] = $user;
        }
        $count['bought'] = count($users);

        return [
            'users' => $users,
            'count' => $count,
        ];
    }

    private function getAnswers($questions, $userId): array
    {
        $answers = [];

        foreach ($questions as $question) {
            foreach ($question->getAnswers() as $answer) {
                if ($answer->getUser()->getId() === $userId) {
                    $answers[] = $answer;
                }
            }
        }

        return $answers;
    }

    /**
     * Получение ответов и вопросов анкеты по пользователям и курсу
     *
     * @param array $userIds
     * @param int $courseId
     *
     * @return array
     */
    private function getUsersQuestions(array $userIds, int $courseId): array
    {
        return $this->em->getRepository(Question::class)
            ->createQueryBuilder('question')
            ->select('question', 'answer', 'courseStream')
            ->leftJoin('question.courseStreams', 'courseStream')
            ->leftJoin(
                'question.answers',
                'answer',
                Join::WITH,
                'answer.user IN (:user) and answer.courseStream = :courseId'
            )
            ->setParameter('user', $userIds)
            ->setParameter('courseId', $courseId)
            ->getQuery()
            ->getResult();
    }

    /**
     * Получение групп по пользователям и курсу
     * Тут же возвращается факультет
     *
     * @param array $userIds
     * @param int $courseId
     *
     * @return array
     */
    private function getUsersGroups(array $userIds, int $courseId): array
    {
        $groups = $this->em->getRepository(UserGroupUser::class)
            ->createQueryBuilder('ugu')
            ->select(
                'user.id as userId',
                'ug.id as groupId',
                'ug.name as groupName',
                'faculty.name as facultyName',
                'ugu.deleted',
                'alternativeGroup.name as alternativeName',
                'alternativeGroup.id as alternativeId'
            )
            ->innerJoin('ugu.user', 'user')
            ->innerJoin(
                'ugu.userGroup',
                'ug',
                Expr\Join::WITH,
                'ug.courseStream = :courseId'
            )
            ->leftJoin('ug.alternativeGroup', 'alternativeGroup')
            ->leftJoin(
                'ugu.faculty',
                'faculty',
                Expr\Join::WITH,
                'faculty.courseStream = :courseId'
            )
            ->andWhere('user.id IN (:userIds)')
            ->setParameters([
                'userIds' => $userIds,
                'courseId' => $courseId
            ])
            ->groupBy('user.id', 'ug.id', 'faculty.id')
            ->getQuery()
            ->getResult();

        // Забираем группы в которые перешли пользователи
        $alternativeIds = array_unique(array_column($groups, 'alternativeId'));
        $alternativeGroups = $this->em->getRepository(UserGroupUser::class)
            ->createQueryBuilder('ugu')
            ->select(
                'user.id as userId',
                'ug.id as groupId',
                'ug.name as groupName',
                'faculty.name as facultyName',
                'stream.id as streamId',
                'ugu.deleted',
            )
            ->innerJoin('ugu.user', 'user')
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ug.courseStream', 'stream')
            ->leftJoin('ugu.faculty', 'faculty')
            ->andWhere('user.id IN (:userIds)')
            ->andWhere('ug.id IN (:groupIds)')
            ->setParameters([
                'userIds' => $userIds,
                'groupIds' => $alternativeIds
            ])
            ->groupBy('user.id', 'ug.id')
            ->getQuery()
            ->getResult();

        $users = [];
        foreach ($groups as $group) {
            $alternativeId = array_search($group['userId'], array_column($alternativeGroups, 'userId'));
            $users[$group['userId']]['groupNames'][$group['groupId']] = $group['groupName'];

            if ($group['facultyName']) {
                $users[$group['userId']]['facultyNames'][] = $group['facultyName'];
            }
            if ($group['deleted']) {
                $users[$group['userId']]['deleted'][$group['groupId']] = $group['deleted'];
            }

            if ($alternativeGroups[$alternativeId]['groupName']) {
                $users[$group['userId']]['alternativeGroupNames'][$group['groupId']] =
                    $alternativeGroups[$alternativeId]['groupName'];
            }
            if ($alternativeGroups[$alternativeId]['facultyName']) {
                $users[$group['userId']]['alternativeFacultyNames'][] =
                    $alternativeGroups[$alternativeId]['facultyName'];
            }
            if ($alternativeGroups[$alternativeId]['deleted']) {
                $users[$group['userId']]['alternativeDeleted'][$group['groupId']] =
                    $alternativeGroups[$alternativeId]['deleted'];
            }
            if ($alternativeGroups[$alternativeId]['streamId']) {
                $users[$group['userId']]['alternativeCourseId'] = $alternativeGroups[$alternativeId]['streamId'];
            }
        }

        return $users;
    }

    /**
     * Получение команд по пользователям и курсу
     *
     * @param array $userIds
     * @param int $courseId
     *
     * @return array
     */
    private function getUsersTeams(array $userIds, int $courseId): array
    {
        $teams = $this->em->getRepository(Team::class)
            ->createQueryBuilder('team')
            ->select('team.name', 'team.id as teamId', 'users.id')
            ->join('team.users', 'users')
            ->leftJoin('team.folder', 'f')
            ->andWhere('f.courseStream = :courseId')
            ->andWhere('users.id IN (:userIds)')
            ->setParameters([
                'userIds' => $userIds,
                'courseId' => $courseId
            ])
            ->groupBy(
                'users.id',
                'team.id'
            )
            ->getQuery()
            ->getResult();

        $users = [];
        foreach ($teams as $team) {
            $users[$team['id']]['names'][] = $team['name'];
        }

        return $users;
    }

    /**
     * Проверяем пользователей которые привязаны к боту
     *
     * @param $userIds
     *
     * @return array
     */
    private function getUserBots($userIds): array
    {
        $bots = $this->em->getRepository(SmartSenderUser::class)
            ->createQueryBuilder('bot')
            ->select('user.id')
            ->innerJoin('bot.user', 'user')
            ->where('user.id IN (:userIds)')
            ->setParameter('userIds', $userIds)
            ->getQuery()
            ->getResult();

        $users = [];
        foreach ($bots as $bot) {
            if (!isset($users[$bot['id']])) {
                $users[$bot['id']] = true;
            }
        }

        return $users;
    }
}

<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class FormHelper
{

    private $errors;
    private $code;
    private $normalizer;

    public function __construct(NormalizerInterface $normalizer)
    {
        $this->errors = [];
        $this->code = 400;
        $this->normalizer = $normalizer;
    }

    public function getResponse(string $action, array $data = [])
    {
        if ($this->errors === []) {
            $status = 'success';
            if ($this->code == 400) {
                $this->code = 200;
            }
        } else {
            $status = 'error';
        }

        return new JsonResponse(
            [
                'action' => $action,
                'status' => $status,
                'errors' => $this->errors,
                'data' => $data,
            ],
            $this->code
        );
    }

    public function addError(string $field, string $message, ?int $code = 400): self
    {
        $this->errors[] = [
            'field' => $field,
            'message' => $message,
        ];

        $this->code = $code;

        return $this;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    public function isValid(): bool
    {
        return $this->errors === [] ? true : false;
    }

    public function setCode(int $code)
    {
        return $this->code = $code;
    }

    public function getFieldFormRequest(Request $request, string $field)
    {
        if ($request->request->has($field)) {
            return $request->request->get($field);
        }

        return null;
    }

    // Оставляем только цифры
    public function toInt(
        ?string $value = '',
        ?string $field = 'number',
        ?int $min = -2000000000,
        ?int $max = 2000000000
    ): int {
        $int = (int)preg_replace("/^[0-9\-]+&/", '', $value);

        if ($int > $max) {
            $this->addError($field, 'Значение не должно быть больше ' . $max);
            $int = $max;
        }

        if ($int < $min) {
            $this->addError($field, 'Значение не должно быть меньше ' . $min);
            $int = $min;
        }

        return $int;
    }

    public function toBigInt(
        string $value,
        ?string $field = 'number',
        ?int $min = -100000000000000,
        ?int $max = 100000000000000
    ): int {
        $int = (int)preg_replace("/^[0-9\-]+&/", '', $value);

        if ($int > $max) {
            $this->addError($field, 'Значение не должно быть больше ' . $max);
            $int = $max;
        }

        if ($int < $min) {
            $this->addError($field, 'Значение не должно быть меньше ' . $min);
            $int = $min;
        }

        return $int;
    }

    // Оставляем только цифры
    public function toPhone(?string $value = '', ?string $field = 'phone'): string
    {
        return (string)$this->toCutStr(preg_replace("/[^0-9]/", '', $value), 30);
    }

    // Перевод в дабл
    public function toDouble(
        ?string $value = '',
        ?string $field = 'float',
        ?int $min = -100000000000000,
        ?int $max = 100000000000000
    ): float {
        $val = number_format($value, 2, ".", "") ?: 0;

        if ($val > $max) {
            $this->addError($field, 'Значение не должно быть больше ' . $max);
            $val = $max;
        }

        if ($val < $min) {
            $this->addError($field, 'Значение не должно быть меньше ' . $min);
            $val = $min;
        }

        return $val;
    }

    // Оставляем только строку удаляя форматирование
    public function toDate(?string $value = ''): ?\DateTime
    {
        try {
            return new \DateTime($value);
        } catch (\Exception $exception) {
            $this->addError('dateTime', $exception->getMessage());
        }

        return null;
    }

    // Оставляем только строку удаляя форматирование
    public function toRealStr(?string $value = '', ?int $length = 255): string
    {
        return $this->toCutStr(
            trim(
                preg_replace(
                    "/\s{2,}/",
                    " ",
                    strip_tags(
                        (string)$value
                    )
                )
            ),
            $length
        );
    }

    public function toStrWithoutGaps(string $value, ?int $length = 255): string
    {
        return $this->toCutStr(
            preg_replace("/\s/", "", $value),
            $length
        );
    }

    // Оставляем только строку сохраняя форматирование
    public function toCutStr(string $value, ?int $length = 255): string
    {
        return
            mb_strimwidth(
                trim(
                    preg_replace("/\s{2,}/", " ", (string)$value)
                ),
                0,
                $length
            );
    }

    // Проверяем емейл
    public function toEmail(string $value): ?string
    {
        if (filter_var($value, FILTER_VALIDATE_EMAIL)) {
            return $value;
        }

        $this->addError('email', 'Неправильный формат email');

        return null;
    }

    public function getTranslit($value)
    {
        $converter = [
            'а' => 'a',
            'б' => 'b',
            'в' => 'v',
            'г' => 'g',
            'д' => 'd',
            'е' => 'e',
            'ё' => 'e',
            'ж' => 'zh',
            'з' => 'z',
            'и' => 'i',
            'й' => 'y',
            'к' => 'k',
            'л' => 'l',
            'м' => 'm',
            'н' => 'n',
            'о' => 'o',
            'п' => 'p',
            'р' => 'r',
            'с' => 's',
            'т' => 't',
            'у' => 'u',
            'ф' => 'f',
            'х' => 'h',
            'ц' => 'c',
            'ч' => 'ch',
            'ш' => 'sh',
            'щ' => 'sch',
            'ь' => '',
            'ы' => 'y',
            'ъ' => '',
            'э' => 'e',
            'ю' => 'yu',
            'я' => 'ya',
        ];

        $value = mb_strtolower($value);
        $value = strtr($value, $converter);
        $value = mb_ereg_replace('[^-0-9a-z]', '-', $value);
        $value = mb_ereg_replace('[-]+', '_', $value);
        $value = trim($value, '-');

        return $value;
    }

    public function normalize($object, array $groups)
    {
        return $this->normalizer->normalize($object, null, ['groups' => $groups]);
    }
}

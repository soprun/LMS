<?php

namespace App\Service\Traction;

use App\DTO\Document\TractionAnalytics\TractionAnalyticsUserDocument;
use App\Entity\CourseStream;
use App\Entity\Faculty;
use App\Entity\Questionnaire\Question;
use App\Entity\Questionnaire\UserAnswer;
use App\Entity\Team;
use App\Entity\Traction\Value;
use App\Entity\Traction\ValueType;
use App\Entity\User;
use App\Entity\UserGroup;
use App\Entity\UserGroupUser;
use App\Service\AuthServerHelper;
use App\Service\AuthServerService;
use DateInterval;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\RequestStack;

final class AnalyticsService
{

    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var AuthServerService
     */
    private $authServerService;
    /**
     * @var RequestStack
     */
    private $requestStack;
    /**
     * @var AuthServerHelper
     */
    private $authServerHelper;

    public function __construct(
        EntityManagerInterface $em,
        AuthServerService $authServerService,
        RequestStack $requestStack,
        AuthServerHelper $authServerHelper
    ) {
        $this->em = $em;
        $this->authServerService = $authServerService;
        $this->requestStack = $requestStack;
        $this->authServerHelper = $authServerHelper;
    }

    public function getFastCashReport(CourseStream $courseStream)
    {
        $this->authServerService->setUserAccessToken(
            $this->requestStack->getCurrentRequest()->cookies->get('accessToken')
                ? 'Bearer ' . $this->requestStack->getCurrentRequest()->cookies->get(
                    'accessToken'
                ) : $this->requestStack->getCurrentRequest()->headers->get('Authorization')
        );

        $csv = $this->getCsvHeader();
        $usersChunks = [];

        $users = $this->em->getRepository(User::class)
            ->getByCourseStream($courseStream);

        $userIds = array_map(static function (User $user) {
            return $user->getId();
        }, $users);

        $usersChunks[] = $this->getUsersData([], $userIds, $courseStream);

        foreach ($usersChunks as $users) {
            foreach ($users as $user) {
                $csv .= "\n" . $user;
            }
        }

        return $csv;
    }

    private function getUsersData(
        array $params,
        array $userIds,
        CourseStream $courseStream
    ) {
        $folderId = null;
        $leadIds = [];
        $budgets = [];
        $responsible = [];
        $revenueMin = null;
        $revenueMax = null;
        $captainName = null;
        $name = null;
        $email = null;
        $city = null;
        $facultyId = null;
        if (!empty($params)) {
            $folderId = $params['folderId'];
            $leadIds = $params['leadIds'];
            $budgets = $params['budgets'];
            $responsible = $params['responsible'];
            $revenueMin = $params['revenueMin'];
            $revenueMax = $params['revenueMax'];
            $captainName = $params['captainName'];
            $name = $params['name'];
            $email = $params['email'];
            $city = $params['city'];
            $facultyId = $params['facultyId'];
        }
        // берем всех учеников и их группы на потоке
        $rows = $this->em->getRepository(UserGroupUser::class)
            ->createQueryBuilder('ugu')
            ->select(
                'u.authUserId',
                'u.id',
                'ug.name',
                'ug.id AS groupId',
                'ugu.amoLeadId',
                'added.authUserId AS addedAuthId',
                'added.id AS addedId'
            )
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ug.courseStream', 'cs')
            ->innerJoin('ugu.user', 'u')
            ->leftJoin('ugu.addedBy', 'added')
            ->andWhere('cs = :course')
            ->setParameter('course', $courseStream)
            ->andWhere('ugu.deleted = 0')
            ->andWhere('u.id IN(:userIds)')
            ->setParameter('userIds', $userIds)
            ->andWhere("ug.name NOT LIKE '%факультет%'")
            ->andWhere("ug.name NOT LIKE '%сборная%'")
            ->groupBy(
                'u.authUserId',
                'u.id',
                'ug.name',
                'ug.id',
                'ugu.amoLeadId',
                'added.authUserId',
                'added.id'
            )
            ->orderBy('ug.id', 'ASC')
            ->getQuery()
            ->getResult();

        $groups = $addedByIds = [];
        foreach ($rows as $row) {
            $userId = $row['id'];
            $groups[$userId] = $row['name'];

            if ($row['addedId']) {
                $addedByIds[$userId] = [
                    'id' => $row['addedId'],
                    'authId' => $row['addedAuthId'],
                ];
            }
        }

        // забираем аутх пользователей
        $authUserIds = array_values(array_unique(array_column($rows, 'authUserId')));
        unset($rows);

        $authRows = $this->authServerService->filterUsers(
            [
                'ids' => $authUserIds,
                'name' => $name,
                'email' => $email,
                'city' => $city,
            ],
            ['lms', 'user:preview', 'user:business'],
            1,
            count($authUserIds)
        )['data']['users'];

        $ids = array_column($authRows, 'id');

        $results = $this->getUserResults($ids, $courseStream);

        $faculties = $this->getUserFaculties($ids, $courseStream);
        $badGroups = $this->getBadGroups($ids);
        [$deletedAlternativeGroups, $alternativeGroups] = $this->getAlternativeGroups($ids, $courseStream);

        $teams = $this->getUserTeams($ids, $courseStream);

        // забираем капитанов
        $captainAuthIds = array_filter(
            array_unique(array_column($teams, 'captainAuthId')),
            static function ($id) {
                return ($id);
            }
        );
        $captains = [];
        if (!empty($captainAuthIds)) {
            try {
                $authCaptains = $this->authServerService->filterUsers(
                    [
                        'ids' => $captainAuthIds,
                        'name' => $captainName,
                    ],
                    ['lms', 'user:preview'],
                    1,
                    count($captainAuthIds)
                )['data']['users'];
                foreach ($authCaptains as $authCaptain) {
                    $captains[$authCaptain['id']] = sprintf(
                        "%s %s",
                        $authCaptain['info']['name'],
                        $authCaptain['info']['lastname']
                    );
                }
            } catch (\Exception $e) {
                // ...
            }
        }

        // забираем тех, кто добавляет в группы
        $addedByNames = [];
        $addedByAuthIds = array_unique(array_column($addedByIds, 'authId'));
        if (!empty($addedByAuthIds)) {
            $authAddedByRows = $this->authServerHelper->getUsers($addedByAuthIds, ['id', 'name', 'lastname', 'email']);
            foreach ($authAddedByRows as $authAddedByRow) {
                $addedByNames[$authAddedByRow['id']] = sprintf(
                    "%s %s (%s)",
                    $authAddedByRow['name'],
                    $authAddedByRow['lastname'],
                    $authAddedByRow['email'],
                );
            }
        }

        // ищем просмотры уроков
        $lessonViews = $this->countLessonViews($userIds, $folderId);

        $users = [];
        foreach ($authRows as $authRow) {
            $user = new TractionAnalyticsUserDocument();
            $user->lmsUserId = $authRow['id'];

            // Если в фильтрах есть поиск по выручке, отсортировывать нужно выручку до
            if ($revenueMin || $revenueMax) {
                if (!isset($results[$authRow['id']]['revenue_before'])) {
                    continue;
                }

                if (
                    ($revenueMin && $results[$authRow['id']]['revenue_before'] < $revenueMin) ||
                    ($revenueMax && $results[$authRow['id']]['revenue_before'] > $revenueMax)
                ) {
                    continue;
                }
            }

            // Фильтруем по найденным капитанам по имени
            if (
                $captainName &&
                (!isset($teams[$user->lmsUserId]) ||
                    !isset($captains[$teams[$user->lmsUserId]['captainId']]) ||
                    $captains[$teams[$user->lmsUserId]['captainId']] === null)
            ) {
                continue;
            }

            // Фильтруем по найденным капитанам по имени
            if (
                $facultyId &&
                (!isset($faculties[$user->lmsUserId]) ||
                    $faculties[$user->lmsUserId]['id'] !== $facultyId)
            ) {
                continue;
            }

            $user->email = $authRow['email'];
            $user->phone = $authRow['info']['phone'];
            $user->name = $authRow['info']['name'];
            $user->lastname = $authRow['info']['lastname'];
            $user->city = $authRow['info']['city'];
            $user->amoLeadId = $leadIds[$user->email] ?? null;
            $user->niche = str_replace(';', ',', $authRow['info']['niche']);
            $user->niche = str_replace(PHP_EOL, ' ', $user->niche);
            $user->lessonViews = $lessonViews[$user->lmsUserId] ?? 0;

            $user->teamName = $teams[$user->lmsUserId]['name'] ?? null;
            $user->teamId = $teams[$user->lmsUserId]['teamId'] ?? null;

            if (isset($teams[$user->lmsUserId], $captains[$teams[$user->lmsUserId]['captainId']])) {
                $user->captainName = $captains[$teams[$user->lmsUserId]['captainId']];
                $user->captainId = $teams[$user->lmsUserId]['captainId'];
            }

            if (isset($addedByIds[$user->lmsUserId], $addedByNames[$addedByIds[$user->lmsUserId]['id']])) {
                $user->addedBy = $addedByNames[$addedByIds[$user->lmsUserId]['id']];
            }

            $user->groupName = $groups[$user->lmsUserId] ?? null;

            $user->facultyName = $faculties[$user->lmsUserId]['name'] ?? null;
            $user->facultyId = $faculties[$user->lmsUserId]['id'] ?? null;
            $user->alternativeGroupName = $alternativeGroups[$user->lmsUserId] ?? null;
            $user->deletedAlternativeGroupName = $deletedAlternativeGroups[$user->lmsUserId] ?? null;
            $user->badGroup = $badGroups[$user->lmsUserId] ?? null;
            $user->budget = $budgets[$user->amoLeadId] ?? null;
            $user->responsiblePartnerId = $responsible[$user->amoLeadId]['id'] ?? null;
            $user->responsiblePartnerName = $responsible[$user->amoLeadId]['name'] ?? null;

            preg_match('#\((.*?)\)#', $user->groupName, $match);
            $user->tariff = $match[1] ?? null;

            foreach ($results[$user->lmsUserId] ?? [] as $resultProperty => $userResults) {
                $user->$resultProperty = $userResults;
            }

            $users[] = $user;
        }

        return $users;
    }

    private function getUserResults(array $userIds, CourseStream $courseStream): array
    {
        $results = [];
        foreach ($userIds as $userId) {
            foreach (ValueType::PROFIT_AND_REVENUE_CODE_NAMES as $channel) {
                foreach (range(1, 14) as $day) {
                    $results[$userId][$channel][$day] = null;
                }
            }
        }

        /** @var Value[] $values */
        $qb = $this->em->getRepository(Value::class)
            ->createQueryBuilder('value')
            ->join('value.type', 'valueType')
            ->where('valueType.title in (:valueTypeTitles)')
            ->andWhere('value.owner in (:userIds) and value.date >= :fromDate')
            ->andWhere("value.date < DATE_ADD(:startDate, :period, 'WEEK')")
            ->setParameter('period', $courseStream->getPeriod())
            ->setParameter('userIds', $userIds)
            ->setParameter('valueTypeTitles', array_keys(ValueType::PROFIT_AND_REVENUE_CODE_NAMES))
            ->setParameter('fromDate', (clone $courseStream->getStartDate())->modify('-13 day'))
            ->setParameter('startDate', $courseStream->getStartDate());

        $values = $qb->getQuery()->getResult();

        foreach ($values as $value) {
            /** @var DateInterval $dateDiff */
            $dateDiff = $courseStream->getStartDate()->diff($value->getDate());

            $type = ValueType::PROFIT_AND_REVENUE_CODE_NAMES[$value->getType()->getTitle()];
            if ($dateDiff->invert === 1) {
                $results[$value->getOwnerId()][$type . '_before'] = $value->getValue();
                continue;
            }
            if ($dateDiff->invert === 0) {
                $results[$value->getOwnerId()][$type][$dateDiff->d + 1] = $value->getValue();
            }
        }

        /** @var UserAnswer[] $beforeCourseValues */
        $beforeCourseValues = $this->em->getRepository(UserAnswer::class)
            ->createQueryBuilder('answer')
            ->join('answer.question', 'question')
            ->where('answer.courseStream = :courseStream and question.slug in (:slugs)')
            ->setParameter('courseStream', $courseStream)
            ->setParameter('slugs', [Question::MONTH_PROFIT, Question::MONTH_REVENUE])
            ->getQuery()
            ->getResult();

        foreach ($beforeCourseValues as $value) {
            $type = Question::MONTH_PROFIT === $value->getQuestion()->getSlug() ? 'profit' : 'revenue';
            if ($value->getText()) {
                $results[$value->getUser()->getId()][$type . '_before'] = $value->getText();
            }
        }

        return $results;
    }

    private function getUserFaculties(array $ids, CourseStream $courseStream): array
    {
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(Faculty::class)->createQueryBuilder('f');

        $rows = $qb
            ->select('u.id', 'f.name', 'f.id as facultyId')
            ->innerJoin('f.userGroupUsers', 'ugu')
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ug.courseStream', 'cs')
            ->innerJoin('ugu.user', 'u')
            ->andWhere('ugu.deleted = 0')
            ->andWhere('cs = :course')
            ->andWhere('u.id IN(:ids)')
            ->setParameters(
                [
                    'ids' => $ids,
                    'course' => $courseStream,
                ]
            )
            ->groupBy('u.id', 'f.name', 'facultyId')
            ->getQuery()
            ->getResult();

        $users = [];
        foreach ($rows as $row) {
            $users[$row['id']] = $row['name'];
            $users[$row['id']] = [
                'name' => $row['name'],
                'id' => $row['facultyId'],
            ];
        }

        return $users;
    }

    private function getBadGroups(array $ids): array
    {
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(UserGroupUser::class)->createQueryBuilder('ugu');
        $rows = $qb
            ->select('u.id', 'ug.name')
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ugu.user', 'u')
            ->where("ug.name IN(:names) OR ug.name like '%Новый МСА%'")
            ->andWhere('ugu.deleted = 0')
            ->andWhere('u.id IN(:userIds)')
            ->setParameter(
                'names',
                [
                    'Like Family',
                    'Обучение Капитанов Скорость (С16)',
                    'Капитаны СК17',
                    'Like Family (карта не действует)',
                    'Капитаны СК19',
                    'Марафон Ушаков',
                    'Капитаны С16 + факультеты',
                    'Старшие капитаны СК17',
                    'Франчайзи',
                    'Франчайзи Скорость / Сотка',
                ]
            )
            ->setParameter('userIds', $ids)
            ->groupBy('u.id', 'ug.name')
            ->getQuery()
            ->getResult();

        $users = [];
        foreach ($rows as $row) {
            $users[$row['id']] = $row['name'];
        }

        return $users;
    }

    private function getAlternativeGroups(array $userIds, CourseStream $courseStream): array
    {
        // определяем альтернативные группы
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(UserGroup::class)->createQueryBuilder('ug');
        $rows = $qb
            ->select('aug.id')
            ->innerJoin('ug.courseStream', 'cs')
            ->innerJoin('ug.alternativeGroup', 'aug')
            ->andWhere('cs = :course')
            ->setParameter('course', $courseStream)
            ->getQuery()
            ->getResult();
        $alternativeIds = array_unique(array_column($rows, 'id'));

        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(UserGroupUser::class)->createQueryBuilder('ugu');
        $rows = $qb
            ->select('u.id', 'ug.name', 'ug.id AS groupId', 'ugu.deleted AS isDeleted')
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ugu.user', 'u')
            ->andWhere('u.id IN(:userIds)')
            ->andWhere('ug.id IN(:groupIds)')
            ->setParameters(
                [
                    'userIds' => $userIds,
                    'groupIds' => $alternativeIds,
                ]
            )
            ->groupBy('u.id', 'ug.name', 'ug.id')
            ->orderBy('ug.id', 'ASC')
            ->getQuery()
            ->getResult();

        $deletedAlternativeGroups = $alternativeGroups = [];
        foreach ($rows as $row) {
            $name = $row['name'];
            $userId = $row['id'];

            if ($row['isDeleted'] === true) {
                $deletedAlternativeGroups[$userId] = $name;
            } else {
                $alternativeGroups[$userId] = $name;
            }
        }

        return [$deletedAlternativeGroups, $alternativeGroups];
    }

    private function getUserTeams(array $ids, CourseStream $courseStream): array
    {
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(Team::class)->createQueryBuilder('t');

        $rows = $qb
            ->select('u.id', 't.id AS teamId', 't.name', 'c.authUserId AS captainAuthId', 'c.id AS captainId')
            ->innerJoin('t.folder', 'f')
            ->innerJoin('t.users', 'u')
            ->leftJoin('t.captain', 'c')
            ->andWhere('f = :course')
            ->andWhere('u.id IN(:ids)')
            ->setParameters(
                [
                    'ids' => $ids,
                    'course' => $courseStream,
                ]
            )
            ->groupBy('u.id', 't.id', 't.name', 'c.authUserId', 'c.id')
            ->getQuery()
            ->getResult();

        $users = [];
        foreach ($rows as $row) {
            $users[$row['id']] = [
                'name' => $row['name'],
                'captainAuthId' => $row['captainAuthId'],
                'captainId' => $row['captainId'],
                'teamId' => $row['teamId'],
            ];
        }

        return $users;
    }

    private function countLessonViews(array $userIds, $folderId): array
    {
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(User::class)->createQueryBuilder('u');

        $rows = $qb
            ->select('u.id', 'COUNT(l.id) AS count')
            ->innerJoin('u.userLessons', 'ul')
            ->innerJoin('ul.lesson', 'l')
            ->innerJoin('l.course', 'c')
            ->innerJoin('c.folder', 'f')
            ->andWhere('u.id IN(:userIds)')
            ->andWhere('f.id = :folderId')
            ->setParameters(
                [
                    'userIds' => $userIds,
                    'folderId' => $folderId,
                ]
            )
            ->groupBy('u.id')
            ->getQuery()
            ->getResult();

        $users = [];
        foreach ($rows as $row) {
            $users[$row['id']] = $row['count'];
        }

        return $users;
    }

    private function getCsvHeader(): string
    {
        $csv = '';
        foreach (array_keys(get_object_vars(new TractionAnalyticsUserDocument())) as $prop) {
            if ($prop === 'revenue' || $prop === 'profit') {
                foreach (range(1, 14) as $day) {
                    $csv .= "{$prop}_{$day};";
                }
                continue;
            }
            $csv .= $prop . ';';
        }

        return mb_substr($csv, 0, -1);
    }

}

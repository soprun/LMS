<?php

namespace App\Service\Traction;

use App\DTO\Traction\UserTractionForSettlementPeriod;
use App\Entity\CourseStream;
use App\Entity\User;
use App\Repository\CourseStreamRepository;
use App\Repository\Traction\ValueRepository;
use DateInterval;
use DatePeriod;
use DateTime;
use DateTimeInterface;
use Exception;
use IntlDateFormatter;
use RuntimeException;
use Throwable;

class TractionService
{
    /** @var CourseStreamRepository */
    private $courseStreamRepository;
    /** @var ValueRepository */
    private $valueRepository;

    public function __construct(CourseStreamRepository $courseStreamRepository, ValueRepository $valueRepository)
    {
        $this->courseStreamRepository = $courseStreamRepository;
        $this->valueRepository = $valueRepository;
    }

    public function makeByStream(CourseStream $stream, array $users): array
    {
        $from = $stream->getStartDate();
        $to = $stream->getEndDate();

        $months = $this->makeWeeksTimeline($from, $to);

        $results = [];
        foreach ($users as $user) {
            $results[$user->getId()] = $this->getPeriods($months, [$stream], $user);
        }

        return $results;
    }

    public function makeByUser(User $user, DateTimeInterface $from, DateTimeInterface $to): array
    {
        $streams = $this->courseStreamRepository->getUserStreamsByDates(
            $user,
            $from,
            (clone $to)->modify('last day of this month')
        );

        $months = $this->makeMonthTimeline($from, $to);
        return $this->getPeriods($months, $streams, $user);
    }

    /**
     * @param CourseStream[] $streams
     *
     * @return UserTractionForSettlementPeriod[]
     */
    private function makePeriods(
        DateTimeInterface $from,
        DateTimeInterface $to,
        string $interval,
        array $streams,
        User $user
    ): array {
        $periods = [];
        $periodDiapasone = new DatePeriod($from, DateInterval::createFromDateString($interval), $to);
        foreach ($periodDiapasone as $date) {
            $endDate = (clone $date)->modify('+' . $interval)->modify('-1 day');
            $periods[] = new UserTractionForSettlementPeriod(
                $date,
                $endDate,
                $date,
                $this->getStreamsBetweenDates($date, $endDate, $streams, $user),
                $this->formatPeriod($date, $endDate, $interval),
                $this->valueRepository->getUserValuesByDate($user, $date)
            );
        }

        return $periods;
    }

    /** @return array<array{from: DateTimeInterface, to: DateTimeInterface}> */
    private function makeMonthTimeline(DateTimeInterface $from, DateTimeInterface $to): array
    {
        $months = [];
        $monthPeriod = new DatePeriod(
            $from,
            DateInterval::createFromDateString('1 month'),
            (clone $to)->modify('+1 month')
        );
        foreach ($monthPeriod as $month) {
            $monthCode = $month->format('Y-m');
            $firstDay = (clone $month)->modify('first day of this month')->modify('monday');
            if ((int)$firstDay->format('d') > 4) {
                $firstDay->modify('- 1 week');
            }
            $lastDay = (clone $month)->modify('last day of this month')->modify('sunday');
            if ((int)$lastDay->format('d') > 3 && $lastDay->format('Y-m') !== $monthCode) {
                $lastDay->modify('- 1 week');
            }
            $months[$monthCode] = ['from' => $firstDay, 'to' => $lastDay];
        }

        return $months;
    }
    /** @return array<array{from: DateTimeInterface, to: DateTimeInterface}> */
    private function makeWeeksTimeline(DateTimeInterface $from, DateTimeInterface $to): array
    {
        $weeks = [];
        $weekPeriod = new DatePeriod(
            $from,
            DateInterval::createFromDateString('1 week'),
            (clone $to)->modify('sunday this week')
        );

        foreach ($weekPeriod as $week) {
            $code = $week->format('Y-m-d');
            $firstDay = (clone $week)->modify('monday this week');
            $lastDay = (clone $week)->modify('sunday this week');
            $weeks[$code] = ['from' => $firstDay, 'to' => $lastDay];
        }

        return $weeks;
    }

    /** @param CourseStream[] $streams */
    private function getMinimalSettlementPeriod(array $streams): ?string
    {
        try {
            uasort($streams, static function (CourseStream $a, CourseStream $b) {
                $format = '%y%m%d%h%i%s%f';

                return DateInterval::createFromDateString($a->getAbstractCourse()->getSettlementPeriod())
                        ->format($format)
                    > DateInterval::createFromDateString($b->getAbstractCourse()->getSettlementPeriod())
                        ->format($format);
            });
        } catch (Throwable $e) {
            throw new RuntimeException('Ошибка при определении минимального расчетного периода', 0, $e);
        }

        $min = current($streams);

        return $min === false ? null : $min->getAbstractCourse()->getSettlementPeriod();
    }

    private function formatPeriod(
        DateTimeInterface $startDate,
        DateTimeInterface $endDate,
        string $settlementPeriod,
        ?string $monthCode = null
    ): string {
        switch ($settlementPeriod) {
            case '1 month':
                $formatter = new IntlDateFormatter('ru', IntlDateFormatter::LONG, IntlDateFormatter::LONG);
                $formatter->setPattern('LLLL');
                try {
                    $month = $formatter->format(new DateTime($monthCode));
                } catch (Exception $e) {
                    throw new RuntimeException('Ошибка при форматировании периода');
                }
                $period = mb_strtoupper(mb_substr($month, 0, 1)) . mb_substr($month, 1);
                break;
            case '1 day':
                $period = $startDate->format('d.m');
                break;
            default:
                $period = $startDate->format('d.m') . ' - ' . $endDate->format('d.m');
        }

        return $period;
    }

    /** @param CourseStream[] $streams */
    private function getStreamsBetweenDates(
        DateTimeInterface $start,
        DateTimeInterface $end,
        array $streams,
        User $user
    ): array {
        $inTimeStreams = [];
        foreach ($streams as $stream) {
            $streamStartDate = $stream->getStartDate() ?? $stream->getUserJoinStartDate($user);
            $streamEndDate = $stream->getStartDate() ? $stream->getEndDate() : $stream->getUserJoinEndDate($user);
            if (
                $this->isDateIntervalIntersect($start, $end, $streamStartDate, $streamEndDate)
            ) {
                $inTimeStreams[] = $stream;
            }
        }

        return $inTimeStreams;
    }

    private function isDateIntervalIntersect(
        DateTimeInterface $startA,
        DateTimeInterface $endA,
        DateTimeInterface $startB,
        DateTimeInterface $endB
    ): bool {
        return ($startB <= $startA && $startA <= $endB)
            || ($startB <= $endA && $endA <= $endB)
            || ($startA <= $startB && $startB <= $endA)
            || ($startA <= $endB && $endB <= $endA);
    }

    public function getPeriods(array $months, array $streams, User $user): array
    {
        $periods = [];
        foreach ($months as $monthYM => $month) {
            $streamsInMonth = $this->getStreamsBetweenDates($month['from'], $month['to'], $streams, $user);
            $settlementPeriod = $this->getMinimalSettlementPeriod($streamsInMonth) ?? '1 month';
            switch ($settlementPeriod) {
                case '1 month':
                    try {
                        $keyDate = (new DateTime($monthYM))->modify('first day of this month');
                    } catch (Exception $e) {
                        throw new RuntimeException('Неверный формат месяца');
                    }
                    $periods[] = new UserTractionForSettlementPeriod(
                        $month['from'],
                        $month['to'],
                        $keyDate,
                        $streamsInMonth,
                        $this->formatPeriod($month['from'], $month['to'], $settlementPeriod, $monthYM),
                        $this->valueRepository->getUserValuesByDate($user, $keyDate)
                    );
                    break;
                case '1 day':
                    foreach (
                        new DatePeriod(
                            $month['from'],
                            DateInterval::createFromDateString('1 week'),
                            $month['to']
                        ) as $week
                    ) {
                        $endOfWeek = (clone $week)->modify('+1 week')->modify('-1 day');
                        $settlementPeriod = $this->getMinimalSettlementPeriod(
                            $this->getStreamsBetweenDates($week, $endOfWeek, $streams, $user)
                        );
                        $settlementPeriod = $settlementPeriod === '1 day' ? '1 day' : '1 week';

                        $newPeriods = $this->makePeriods(
                            $week,
                            (clone $endOfWeek)->modify('+1 day'),
                            $settlementPeriod,
                            $streams,
                            $user
                        );
                        array_push($periods, ...$newPeriods);
                    }
                    break;
                default:
                    $newPeriods = $this->makePeriods(
                        $month['from'],
                        (clone $month['to'])->modify('+1 day'),
                        $settlementPeriod,
                        $streams,
                        $user
                    );
                    array_push($periods, ...$newPeriods);
                    break;
            }
        }

        return $periods;
    }

}

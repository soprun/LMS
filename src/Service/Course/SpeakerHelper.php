<?php

namespace App\Service\Course;

use App\Entity\User;
use App\Service\AuthServerHelper;
use App\Service\FormHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class SpeakerHelper
{

    private $em;
    private $formHelper;
    private $authServerHelper;

    public function __construct(
        EntityManagerInterface $em,
        FormHelper $formHelper,
        AuthServerHelper $authServerHelper
    ) {
        $this->em = $em;
        $this->formHelper = $formHelper;
        $this->authServerHelper = $authServerHelper;
    }

    public function getSpeakers(Request $request)
    {
        $data['speakers'] = [];

        $speakers = $this->getSpeakersArray();

        $name = mb_strtolower($request->query->get('name', ''));

        foreach ($speakers as $speaker) {
            if (
                !empty($name) &&
                (
                    !str_contains(mb_strtolower($speaker['name']), $name) &&
                    !str_contains(mb_strtolower($speaker['lastname']), $name)
                )
            ) {
                continue;
            }
            $data['speakers'][] = $speaker;
        }

        return $this->formHelper->getResponse(
            'getSpeakers',
            $data
        );
    }

    public function getSpeakersArray()
    {
        $speakerIds = $this->em->getRepository(User::class)
            ->findByGroupNameUsers("Спикеры");

        $rawIds = [];
        $ids = [];
        foreach ($speakerIds as $id) {
            $rawIds[] = $id['authUserId'];
            $ids[$id['authUserId']] = $id['id'];
        }

        $res = $this->parseByIds($rawIds);
        for ($i = 0; $i < count($res); $i++) {
            $res[$i]['id'] = $ids[$res[$i]['id']];
        }

        return $res;
    }

    public function parseByIds(?array $rawIds = [])
    {
        $data = [];
        if (!empty($rawIds)) {
            $speakers = $this->authServerHelper->getUsersInfo($rawIds);
            foreach ($speakers as $speaker) {
                $data[] = [
                    'id' => $speaker->id,
                    'name' => property_exists($speaker, 'name') ? $speaker->name : "",
                    'lastname' => property_exists($speaker, 'lastname') ? $speaker->lastname : "",
                    'avatar' => property_exists($speaker, 'avatar') ? $speaker->avatar : "",
                ];
            }
        }

        return $data;
    }

}

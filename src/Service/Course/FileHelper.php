<?php

namespace App\Service\Course;

use App\Entity\BaseBlock\BaseFiles;
use App\Service\FormHelper;
use Doctrine\ORM\EntityManagerInterface;
use League\Flysystem\AdapterInterface;
use League\Flysystem\Exception;
use League\Flysystem\FilesystemInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class FileHelper
{
    private const TYPE_IMAGE = 'images';
    private const TYPE_FILE = 'files';

    private $em;
    private $formHelper;
    private $validator;
    private $uploadFilesystem;
    private $uploadedAssetsBaseUrl;

    public function __construct(
        EntityManagerInterface $em,
        FormHelper $formHelper,
        ValidatorInterface $validator,
        FilesystemInterface $uploadFilesystem,
        string $uploadedAssetsBaseUrl
    ) {
        $this->em = $em;
        $this->formHelper = $formHelper;
        $this->validator = $validator;
        $this->uploadFilesystem = $uploadFilesystem;
        $this->uploadedAssetsBaseUrl = $uploadedAssetsBaseUrl;
        $this->formHelper = $formHelper;
    }

    /**
     * Загрузка одного или нескольких файлов
     * @param  Request  $request
     * @return JsonResponse
     */
    public function addFiles(Request $request): JsonResponse
    {
        $data = [];

        if ($uploadedFiles = $request->files->get('files')) {
            foreach ($uploadedFiles as $uploadedFile) {
                if ($this->isValidFile($uploadedFile)) {
                    $fileName = $this->uploadFile($uploadedFile, self::TYPE_FILE, true);
                    $filePath = self::TYPE_FILE . '/' . $fileName;

                    if ($this->formHelper->isValid()) {
                        $fileEntity = new BaseFiles();
                        $fileEntity->setFileName(
                            $this->formHelper->toRealStr($uploadedFile->getClientOriginalName() ?? $fileName)
                        );
                        $fileEntity->setFilePath($filePath);
                        $fileEntity->setMimeType($uploadedFile->getMimeType() ?? 'application/octet-stream');

                        $this->em->persist($fileEntity);
                        $this->em->flush();

                        $data[] = $this->getFileInfo($fileEntity);
                    }
                }
            }
        } else {
            $this->formHelper->addError('file', 'Объект файла не получен');
        }

        return $this->formHelper->getResponse('addFiles', $data);
    }

    /**
     * Загрузка картинки с соответствующей валидацией
     * @param  Request  $request
     * @return JsonResponse
     */
    public function addImg(Request $request): JsonResponse
    {
        $data = [];

        if ($uploadedFile = $request->files->get('file')) {
            if ($this->isValidImage($uploadedFile)) {
                $fileName = $this->uploadFile($uploadedFile, self::TYPE_IMAGE, true);
                $filePath = self::TYPE_IMAGE . '/' . $fileName;

                if ($this->formHelper->isValid()) {
                    $fileEntity = new BaseFiles();
                    $fileEntity->setFileName(
                        $this->formHelper->toRealStr($uploadedFile->getClientOriginalName() ?? $fileName)
                    );
                    $fileEntity->setFilePath($filePath);
                    $fileEntity->setMimeType($uploadedFile->getMimeType() ?? 'application/octet-stream');

                    $this->em->persist($fileEntity);
                    $this->em->flush();

                    $data = $this->getFileInfo($fileEntity);
                }
            }
        } else {
            $this->formHelper->addError('file', 'Объект файла не получен');
        }

        return $this->formHelper->getResponse('addImg', $data);
    }

    /**
     * Валидация файла
     * @param  UploadedFile|null  $uploadedFile
     * @param  null|int  $maxSize
     * @return bool
     */
    private function isValidFile(?UploadedFile $uploadedFile, ?int $maxSize = 200): bool
    {
        $violations = $this->validator->validate(
            $uploadedFile,
            [
                new File(
                    [
                        'maxSize' => $maxSize . 'M',
                        'mimeTypes' => [
                            'image/*',
                            'application/pdf',
                            'application/msword',
                            'application/pptx',
                            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                            'application/vnd.openxmlformats-officedocument.presentationml.presentation',
                            'application/vnd.ms-excel',
                            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                            'text/plain',
                            'text/csv',
                        ],
                        'mimeTypesMessage' => 'Разрешено отправлять изображения, pptx, PDF и CSV-файлы',
                    ]
                ),
                new NotBlank(['message' => "Файл не должен быть пустым или весить более {$maxSize} Мб"]),
            ]
        );

        foreach ($violations as $violation) {
            $this->formHelper->addError('file', $violation->getMessage());
        }

        return $this->formHelper->isValid();
    }

    /**
     * Валидация картинки
     * @param  UploadedFile|null  $uploadedFile
     * @return bool
     */
    private function isValidImage(?UploadedFile $uploadedFile): bool
    {
        if ($this->isValidFile($uploadedFile)) {
            $violations = $this->validator->validate($uploadedFile, new Image());
            foreach ($violations as $violation) {
                $this->formHelper->addError('file', $violation->getMessage());
            }
        }

        return $this->formHelper->isValid();
    }

    /**
     * Загрузка файла
     * @param  UploadedFile  $uploadedFile
     * @param  string  $directory
     * @param  bool  $isPublic
     * @return string
     */
    private function uploadFile(UploadedFile $uploadedFile, string $directory, bool $isPublic): string
    {
        $originalFilename = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);
        $newFilenameBody = md5($originalFilename . time()) . '-' . uniqid();
        $fileName = $newFilenameBody . '.' . $uploadedFile->guessExtension();
        $stream = fopen($uploadedFile->getPathname(), 'r');

        try {
            $this->uploadFilesystem->writeStream(
                $directory . '/' . $fileName,
                $stream,
                [
                    'visibility' => $isPublic
                        ? AdapterInterface::VISIBILITY_PUBLIC
                        : AdapterInterface::VISIBILITY_PRIVATE,
                ]
            );
        } catch (Exception $e) {
            $this->formHelper->addError('file', $e->getMessage());
        }

        if (is_resource($stream)) {
            fclose($stream);
        }

        return $fileName;
    }

    /**
     * Получение полного URL до загруженного файла
     * @param  BaseFiles  $file
     * @return string
     */
    public function getFileUrl(BaseFiles $file): string
    {
        return $this->uploadedAssetsBaseUrl . '/' . $file->getFilePath();
    }

    /**
     * @param  BaseFiles  $file
     * @return array
     */
    public function getFileInfo(BaseFiles $file): array
    {
        return [
            'id' => $file->getId(),
            'url' => $this->getFileUrl($file),
            'fileName' => $file->getFileName(),
            'mimeType' => $file->getMimeType(),
        ];
    }

}

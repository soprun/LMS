<?php

namespace App\Service\Course;

use App\Entity\BaseBlock\BaseFiles;
use App\Entity\Courses\Course;
use App\Entity\Courses\Lesson;
use App\Entity\Courses\LessonConfiguration;
use App\Entity\Courses\LessonPart;
use App\Entity\Courses\LessonPartBlock;
use App\Entity\Task\AnswerTaskType1;
use App\Entity\User;
use App\Entity\UserGroup;
use App\Entity\UserLesson;
use App\Repository\UserLessonRepository;
use App\Service\ContentBlock\ContentBlockHelper;
use App\Service\FormHelper;
use App\Service\UserHelper;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class LessonHelper
{
    private $em;
    private $security;
    private $formHelper;
    private $contentBlockHelper;
    private $speakerHelper;
    private $fileHelper;
    private $userHelper;
    private $taskHelper;

    public function __construct(
        EntityManagerInterface $em,
        Security $security,
        FormHelper $formHelper,
        ContentBlockHelper $contentBlockHelper,
        SpeakerHelper $speakerHelper,
        FileHelper $fileHelper,
        UserHelper $userHelper,
        TaskHelper $taskHelper
    ) {
        $this->em = $em;
        $this->security = $security;
        $this->formHelper = $formHelper;
        $this->contentBlockHelper = $contentBlockHelper;
        $this->speakerHelper = $speakerHelper;
        $this->fileHelper = $fileHelper;
        $this->userHelper = $userHelper;
        $this->taskHelper = $taskHelper;
    }

    public function getLessonProgressBar(Course $course)
    {
        $data = [];

        /** @var User $user */
        $user = $this->security->getUser();
        $isAdmin = $this->userHelper->isUserAdmin();

        if ($isAdmin) {
            $lessons = $this->em->getRepository(Lesson::class)->findByCourseNotDeletedLessons($course);
        } else {
            $lessons = $this->em->getRepository(Lesson::class)->findByCourseNotDeletedAndNotHiddenLessons(
                $course,
                $user
            );
        }
        $countLessons = count($lessons);

        $userLessons = $this->getIndexedUserLessons($lessons);


        // Распаковываем инфу по курсам
        $partBlocksForLessons = $this->em->getRepository(LessonPartBlock::class)
            ->findByLessonsTaskBlocks($lessons, true);

        $completedLessons = 0;
        /** @var Lesson $lesson */
        foreach ($lessons as $key => $lesson) {
            $lessonPartBlocks = $partBlocksForLessons[$lesson->getId()] ?? [];
            $lessonArray = $this->getLessonArray($lesson, $lessonPartBlocks);

            if (isset($userLessons[$lesson->getId()])) {
                if (
                    $userLessons[$lesson->getId()]->getState() != UserLesson::STATE_NOT_VIEWED
                    && $userLessons[$lesson->getId()]->getState() != UserLesson::STATE_ANSWER_SENT
                    && $lessonArray['task']['isAllChecked']
                ) {
                    $completedLessons++;
                }
            }
        }

        if ($countLessons > 0) {
            $data['progressBar'] = floor($completedLessons / $countLessons * 100);
        } else {
            $data['progressBar'] = 100;
        }

        return $this->formHelper->getResponse(
            'getLessonsProgressBar',
            $data
        );
    }

    public function getLessons(Course $course)
    {
        $data = [];
        $isComplete = false;

        /** @var User $user */
        $user = $this->security->getUser();
        $isAdmin = $this->userHelper->isUserAdmin();

        if ($isAdmin) {
            $lessons = $this->em->getRepository(Lesson::class)->findByCourseNotDeletedLessons($course);
        } else {
            $lessons = $this->em->getRepository(Lesson::class)->findByCourseNotDeletedAndNotHiddenLessons(
                $course,
                $user
            );
        }

        $userLessons = $this->getIndexedUserLessons($lessons);

        $data['folder'] = [];

        if ($folder = $course->getFolder()) {
            $data['folder'] = [
                'id' => $folder->getId(),
                'name' => $folder->getName(),
                'permissions' => $this->userHelper->getCourseFolderPermissions($user, $folder),
            ];
        }

        // Флаг, который показывает, что след уроки надо тормознуть
        $nextStop = false;

        // Распаковываем инфу по курсам
        $partBlocksForLessons = $this->em->getRepository(LessonPartBlock::class)
            ->findByLessonsTaskBlocks($lessons, true);

        $completedLessons = 0;
        $countLessons = 0;
        /** @var Lesson $lesson */
        foreach ($lessons as $key => $lesson) {
            $lessonPartBlocks = $partBlocksForLessons[$lesson->getId()] ?? [];
            $lessonArray = $this->getLessonArray($lesson, $lessonPartBlocks);
            $lessonArray['isStopped'] = $nextStop;
            $lessonArray['position'] = $key + 1;
            $lessonArray['state'] = isset($userLessons[$lesson->getId()])
                ? $userLessons[$lesson->getId()]->getState()
                : UserLesson::STATE_NOT_VIEWED;

            if (isset($userLessons[$lesson->getId()])) {
                if ($lessonArray['task']['isAllChecked'] && $lessonArray['task']['isTaskExist']) {
                    $userLessons[$lesson->getId()]->setState(UserLesson::STATE_ANSWER_CHECKED);
                }

                if (
                    $userLessons[$lesson->getId()]->getState() != UserLesson::STATE_NOT_VIEWED
                    && $userLessons[$lesson->getId()]->getState() != UserLesson::STATE_ANSWER_SENT
                    && $lessonArray['task']['isAllChecked']
                ) {
                    $completedLessons++;
                }
            }

            if (
                !$nextStop && $lesson->getIsStopLesson(
                ) && $lessonArray['task']['isTaskExist'] && !$lessonArray['task']['isAllChecked'] && !$isAdmin
            ) {
                $nextStop = true;
                $data['lessons'][] = $lessonArray;
                $countLessons++;
                break;
            }

            $data['lessons'][] = $lessonArray;
            $countLessons++;
        }

        if (
            $countLessons === count($lessons)
            && $countLessons === $completedLessons
            && !$isAdmin
        ) {
            $isComplete = true;
        }

        $data['course'] = [
            'id' => $course->getId(),
            'name' => $course->getName(),
            'permissions' => $this->userHelper->getCoursePermissions($user, $course),
            'lessonsCount' => $countLessons,
            'groups' => $course->getUserGroupIds(),
            'isComplete' => $isComplete,
        ];

        if ($countLessons > 0) {
            $data['progressBar'] = floor($completedLessons / $countLessons * 100);
        } else {
            $data['progressBar'] = 100;
        }

        return $this->formHelper->getResponse(
            'getLessons',
            $data
        );
    }

    public function getLessonArray(Lesson $lesson, $lessonPartBlocks = null, ?User $user = null)
    {
        $rawIds = [];
        $ids = [];
        foreach ($lesson->getSpeakers() as $speaker) {
            $rawIds[] = $speaker->getAuthUserId();
            $ids[$speaker->getAuthUserId()] = $speaker->getId();
        }

        $speakers = $this->speakerHelper->parseByIds($rawIds);
        for ($i = 0, $iMax = count($speakers); $i < $iMax; $i++) {
            $speakers[$i]['id'] = $ids[$speakers[$i]['id']];
        }

        $dateOfStart = '';
        $isHidden = false;
        if ($lessonConfiguration = $lesson->getLessonConfiguration()) {
            $isHidden = (bool)$lessonConfiguration->getIsHidden();

            if ($lessonConfiguration->getDateOfStart()) {
                $dateOfStart = $lessonConfiguration->getDateOfStart()->format('Y-m-d') ?? "";
            }
        }

        $tags = [];
        $img = [];
        /** @var BaseFiles $imgEntity */
        if ($imgEntity = $lesson->getImgFile()) {
            $img = [
                'id' => $imgEntity->getId(),
                'url' => $this->fileHelper->getFileUrl($imgEntity),
                'fileName' => $imgEntity->getFileName(),
            ];
        }

        $taskArray = [
            'isTaskExist' => false,
            'isAnswerExist' => false,
            'isAllChecked' => true,
            'ddl' => '',
        ];

        $isAdmin = $this->userHelper->isUserAdmin();
        $tasks = $this->taskHelper->getTaskByLesson($lesson, $lessonPartBlocks);
        $user = $user ? $user : $this->security->getUser();
        foreach ($tasks as $task) {
            $answers = $this->em->getRepository(AnswerTaskType1::class)
                ->findBy(['user' => $user, 'task' => $task['task']]);

            $allChecked = true;
            if ($answers) {
                /** @var AnswerTaskType1 $answer */
                foreach ($answers as $answer) {
                    if (!$answer->getIsChecked()) {
                        $allChecked = false;
                        break;
                    }
                }
            } else {
                $allChecked = false;
            }

            $taskArray = [
                'isTaskExist' => true,
                'isAnswerExist' => !$taskArray['isAnswerExist'] ? !empty($answers) : true,
                'isAllChecked' => $allChecked,
                'ddl' => $task && ($task['task'] || $isAdmin) && $task['ddl'] && !$answers ? $task['ddl']->format(
                    "Y-m-d H:i:s"
                ) : "",
            ];
        }

        return [
            'id' => $lesson->getId(),
            'name' => $lesson->getName(),
            'description' => $lesson->getDescription() ?? "",
            'position' => 0,
            'side' => [
                'img' => $img,
                'imgText' => $lesson->getImgText() ?? "",
            ],
            'isStopLesson' => (bool)$lesson->getIsStopLesson(),
            'isImportant' => (bool)$lesson->getIsImportant(),
            'task' => $taskArray,
            'isHidden' => $isHidden,
            'dateOfStart' => $dateOfStart,
            'speakers' => $speakers,
            'tags' => $tags,
            'groups' => $lesson->getUserGroupIds(),
        ];
    }

    public function getMinLessonArray(Lesson $lesson)
    {
        $lessonConfiguration = $lesson->getLessonConfiguration();

        $dateOfStart = "";
        $isHidden = false;
        if ($lessonConfiguration) {
            $isHidden = (bool)$lessonConfiguration->getIsHidden();
            if ($lessonConfiguration->getDateOfStart()) {
                $dateOfStart = $lessonConfiguration->getDateOfStart()->format('Y-m-d') ?? "";
            }
        }

        $tasks = $this->taskHelper->getTaskByLesson($lesson);

        $taskArray = [
            'isTaskExist' => false,
            'isAnswerExist' => false,
            'isAllChecked' => true,
            'ddl' => "",
        ];

        $isAdmin = $this->userHelper->isUserAdmin();

        foreach ($tasks as $task) {
            $answers = $this->em->getRepository(AnswerTaskType1::class)
                ->findOneBy(['user' => $this->security->getUser(), 'task' => $task['task']]);

            $allChecked = true;
            if ($answers) {
                /** @var AnswerTaskType1 $answer */
                foreach ($answers as $answer) {
                    if (!$answer->getIsChecked()) {
                        $allChecked = false;
                        break;
                    }
                }
            } else {
                $allChecked = false;
            }

            $taskArray = [
                'isTaskExist' => true,
                'isAnswerExist' => !$taskArray['isAnswerExist'] ? !empty($answers) : true,
                'isAllChecked' => $allChecked,
                'ddl' => $task ? ($task['task'] || $isAdmin) && $task['ddl'] && !$answers ? $task['ddl']->format(
                    "Y-m-d H:i:s"
                ) : "" : "",
            ];
        }

        if ($isAdmin) {
            $lessons = $this->em->getRepository(Lesson::class)->findByCourseAndNotDeletedLessons($lesson->getCourse());
        } else {
            $lessons = $this->em->getRepository(Lesson::class)->findByCourseNotDeletedAndNotHiddenMinLessons(
                $lesson->getCourse()
            );
        }
        $pos = 0;
        $lessonPosition = 0;
        foreach ($lessons as $posLesson) {
            if ($posLesson['id'] == $lesson->getId()) {
                $lessonPosition = $pos;
            }
            $pos++;
        }

        $data = [
            'id' => $lesson->getId(),
            'name' => $lesson->getName(),
            'description' => $lesson->getDescription() ?? "",
            'position' => $lessonPosition,
            'isStopLesson' => $lesson->getIsStopLesson() ? true : false,
            'isImportant' => $lesson->getIsImportant() ? true : false,
            'task' => $taskArray,
            'isHidden' => $isHidden,
            'dateOfStart' => $dateOfStart,
        ];

        return $data;
    }

    public function newLesson(Course $course, Request $request)
    {
        $lessonArray = $request->request->get('lesson', null);

        if (!$lessonArray) {
            $this->formHelper->addError('lesson', 'Нет данных урока');
        }

        if ($this->formHelper->isValid()) {
            // Новый урок
            $lessonEntity = new Lesson();
            $lessonEntity->setCourse($course);

            $lessonEntity = $this->setLessonFromRequest($lessonEntity, $lessonArray);

            if ($lessonEntity->getId()) {
                $lessonConfigurationEntity = new LessonConfiguration();
                $lessonConfigurationEntity->setLesson($lessonEntity);
                $lessonConfigurationEntity->setDateOfStart(new DateTime($lessonArray['dateOfStart']));
                $lessonConfigurationEntity->setIsHidden($lessonArray['isHidden'] ? true : false);
                $this->em->persist($lessonConfigurationEntity);

                foreach ($lessonArray['speakers'] as $speaker) {
                    /** @var User|null $speakerEntity */
                    $speakerEntity = $this->em
                        ->getRepository(User::class)->findOneBy(['id' => $speaker['id']]);

                    if (
                        $speakerEntity &&
                        !$this->em->getRepository(Lesson::class)
                            ->findByLessonIsSpeakerExists($lessonEntity, $speakerEntity)
                    ) {
                        $lessonEntity->addSpeaker($speakerEntity);
                    }

                    $this->em->persist($lessonEntity);
                }

                $part = new LessonPart();
                $part->setName('Часть 1');
                $part->setLesson($lessonEntity);
                $part->setPositionInLesson(1);
                $this->em->persist($part);
            }
        }

        $this->em->flush();

        return $this->formHelper->getResponse(
            'newLesson',
            ['id' => $lessonEntity->getId()]
        );
    }

    public function editLesson(Lesson $lesson, Request $request)
    {
        $lessonArray = $request->request->get('lesson', null);

        if (!$lessonArray) {
            $this->formHelper->addError('lesson', 'Нет данных урока');
        }

        if ($this->formHelper->isValid()) {
            $lesson = $this->setLessonFromRequest($lesson, $lessonArray);

            if ($lessonConfigurationEntity = $lesson->getLessonConfiguration()) {
                $lessonConfigurationEntity->setDateOfStart(new DateTime($lessonArray['dateOfStart']));
                $lessonConfigurationEntity->setIsHidden($lessonArray['isHidden'] ? true : false);
                $this->em->persist($lessonConfigurationEntity);

                foreach ($lesson->getSpeakers() as $speaker) {
                    $lesson->removeSpeaker($speaker);
                }
                $this->em->persist($lesson);
                $this->em->flush();

                // ToDo: Добавить логику спикеров
                foreach ($lessonArray['speakers'] as $speaker) {
                    /** @var User|null $speakerEntity */
                    $speakerEntity = $this->em
                        ->getRepository(User::class)->findOneBy(['id' => $speaker['id']]);

                    if (
                        $speakerEntity &&
                        !$this->em->getRepository(Lesson::class)
                            ->findByLessonIsSpeakerExists($lesson, $speakerEntity)
                    ) {
                        $lesson->addSpeaker($speakerEntity);
                    }

                    $this->em->persist($lesson);
                }
            }
        }

        $this->em->flush();

        return $this->formHelper->getResponse(
            'editLesson',
            ['id' => $lesson->getId()]
        );
    }

    public function duplicateLesson(Lesson $lesson, Request $request)
    {
        $lessonArray = $request->request->get('lesson', null);

        if ($this->formHelper->isValid()) {
            // Новый урок
            $lessonEntity = new Lesson();
            $lessonEntity->setCourse($lesson->getCourse());

            $lessonEntity = $this->setLessonFromRequest($lessonEntity, $lessonArray);

            if ($lessonEntity->getId()) {
                $lessonConfigurationEntity = new LessonConfiguration();
                $lessonConfigurationEntity->setLesson($lessonEntity);
                $lessonConfigurationEntity->setDateOfStart(new DateTime($lessonArray['dateOfStart']));
                $lessonConfigurationEntity->setIsHidden($lessonArray['isHidden'] ? true : false);
                $this->em->persist($lessonConfigurationEntity);

                // ToDo: Добавить логику спикеров
                foreach ($lessonArray['speakers'] as $speaker) {
                    /** @var User|null $speakerEntity */
                    $speakerEntity = $this->em
                        ->getRepository(User::class)->findOneBy(['id' => $speaker['id']]);

                    if (
                        $speakerEntity &&
                        !$this->em->getRepository(Lesson::class)
                            ->findByLessonIsSpeakerExists($lessonEntity, $speakerEntity)
                    ) {
                        $lessonEntity->addSpeaker($speakerEntity);
                    }

                    $this->em->persist($lessonEntity);
                }
            }
        }

        // Дублировать все блоки

        // Получаем части
        $this->duplicateLessonPart($lesson, $lessonEntity);

        $this->em->flush();

        return $this->formHelper->getResponse(
            'duplicateLesson',
            ['id' => $lessonEntity->getId()]
        );
    }

    public function setLessonFromRequest(Lesson $lesson, array $lessonArray)
    {
        foreach ($lessonArray as $field => $value) {
            switch ($field) {
                case 'name':
                    $lesson->setName($this->formHelper->toRealStr($value));
                    break;
                case 'description':
                    $lesson->setDescription($this->formHelper->toRealStr($value));
                    break;
                case 'side':
                    foreach ($value as $item => $v) {
                        switch ($item) {
                            case 'img':
                                $imgId = is_array($v) ? key_exists('id', $v) ? (int)$v['id'] : "" : "";
                                /** @var BaseFiles $img */
                                $img = $this->em
                                    ->getRepository(BaseFiles::class)
                                    ->findOneBy(['id' => $imgId]);

                                $lesson->setImgFile($img);
                                break;
                            case 'imgText':
                                $lesson->setImgText($this->formHelper->toRealStr($v));
                                break;
                        }
                    }
                    break;
                case 'isStopLesson':
                    $lesson->setIsStopLesson((bool)$value);
                    break;
                case 'isImportant':
                    $lesson->setIsImportant((bool)$value);
                    break;
                case 'groups':
                    $userGroups = $this->em->getRepository(UserGroup::class)->findBy(['id' => $value]);
                    $lesson->clearUserGroups();
                    $lesson->addUserGroups($userGroups);
                    $lesson->setUpdatedAt(new DateTime());
                    break;
            }
        }

        $this->em->persist($lesson);
        $this->em->flush();

        return $lesson;
    }

    public function duplicateLessonPart(Lesson $lesson, Lesson $lessonEntity)
    {
        $parts = $lesson->getLessonParts();

        /** @var LessonPart $part */
        foreach ($parts as $part) {
            $newPart = new LessonPart();
            $newPart->setName($part->getName());
            $newPart->setLesson($lessonEntity);
            $newPart->setPositionInLesson($part->getPositionInLesson());

            $this->em->persist($newPart);

            $blocks = $this->em->getRepository(LessonPartBlock::class)
                ->findByPartNotDeletedBlocks($part);

            /** @var LessonPartBlock $block */
            foreach ($blocks as $block) {
                $newLessonPartBlock = new LessonPartBlock();
                $newLessonPartBlock->setVariety($block->getVariety());
                $newLessonPartBlock->setType($block->getType());
                $newLessonPartBlock->setPositionInPart($block->getPositionInPart());
                $newLessonPartBlock->setPart($newPart);

                $newBlockId = $this->contentBlockHelper->duplicateBlock(
                    $block->getType(),
                    $block->getVariety(),
                    $block->getBlockId()
                );

                $newLessonPartBlock->setBlockId($newBlockId);
                $this->em->persist($newLessonPartBlock);
            }
        }
    }

    public function lessonPositionUp(Lesson $lesson): JsonResponse
    {
        $currentPosition = $lesson->getPosition();

        if ($currentPosition > 0) {
            $lesson->setPosition($currentPosition - 1);
            $this->em->persist($lesson);
            $this->em->flush();
        } else {
            $this->formHelper->addError('position', 'Это уже первая позиция');
        }

        return $this->formHelper->getResponse(
            'lessonPositionUp',
            ['id' => $lesson->getId()]
        );
    }

    public function lessonPositionDown(Lesson $lesson): JsonResponse
    {
        $currentPosition = $lesson->getPosition();

        $lastPosition = $this->em
            ->getRepository(Lesson::class)
            ->findByCourseLastPosition($lesson->getCourse());

        if ($currentPosition !== $lastPosition) {
            $lesson->setPosition($currentPosition + 1);
            $this->em->persist($lesson);
            $this->em->flush();
        } else {
            $this->formHelper->addError('position', 'Это уже последняя позиция');
        }

        return $this->formHelper->getResponse(
            'lessonPositionDown',
            ['id' => $lesson->getId()]
        );
    }

    /**
     * @param $lessons
     *
     * @return UserLesson[]
     */
    protected function getIndexedUserLessons($lessons)
    {
        /** @var UserLessonRepository $repo */
        $repo = $this->em->getRepository(UserLesson::class);
        /** @var UserLesson[] $userLessons */
        $userLessons = $repo
            ->createQueryBuilder('ul')
            ->where('ul.lesson IN (:lessons) and ul.user = :user')
            ->setParameters(
                [
                    'lessons' => $lessons,
                    'user' => $this->security->getUser(),
                ]
            )
            ->getQuery()
            ->getResult();

        return array_combine(
            array_map(
                static function (UserLesson $e) {
                    return $e->getLesson()->getId();
                },
                $userLessons
            ),
            $userLessons
        );
    }

}

<?php

namespace App\Service\Course;

use App\Entity\BaseBlock\BaseFiles;
use App\Entity\Courses\Lesson;
use App\Entity\Courses\LessonPart;
use App\Entity\Courses\LessonPartBlock;
use App\Entity\User;
use App\Entity\UserLesson;
use App\Service\ContentBlock\ContentBlockHelper;
use App\Service\FormHelper;
use App\Service\UserHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class BlockHelper
{
    private $em;
    private $security;
    private $formHelper;
    private $contentBlockHelper;
    private $lessonHelper;
    private $userHelper;

    public function __construct(
        EntityManagerInterface $em,
        Security $security,
        FormHelper $formHelper,
        ContentBlockHelper $contentBlockHelper,
        LessonHelper $lessonHelper,
        UserHelper $userHelper
    ) {
        $this->em = $em;
        $this->security = $security;
        $this->formHelper = $formHelper;
        $this->contentBlockHelper = $contentBlockHelper;
        $this->lessonHelper = $lessonHelper;
        $this->userHelper = $userHelper;
    }

    public function getLessonBlocks(Lesson $lesson)
    {
        /** @var User $user */
        $user = $this->security->getUser();
        $isAdmin = $this->userHelper->isUserAdmin();

        $this->markLessonAsViewed($lesson);

        $data = [];
        if ($isAdmin) {
            $lessons = $this->em->getRepository(Lesson::class)->findByCourseAndNotDeletedLessons($lesson->getCourse());
        } else {
            $lessons = $this->em->getRepository(Lesson::class)->findByCourseNotDeletedAndNotHiddenMinLessons(
                $lesson->getCourse()
            );
        }
        $countLessons = count($lessons);

        $data['course'] = [
            'id' => $lesson->getCourse()->getId(),
            'name' => $lesson->getCourse()->getName(),
            'numOfLessons' => $countLessons,
        ];

        $data['currentLesson'] = $this->lessonHelper->getLessonArray($lesson);
        $nextPosition = null;
        $prevPosition = null;
        foreach ($lessons as $key => $value) {
            if ($value['id'] == $lesson->getId()) {
                $nextPosition = ($key + 1 >= 0 && ($key + 1) < $countLessons) ? $lessons[$key + 1]['id'] : null;
                $prevPosition = ($key - 1 >= 0 && ($key - 1) < $countLessons) ? $lessons[$key - 1]['id'] : null;
                $data['currentLesson']['position'] = $key + 1;
            }
        }

        if ($prevPosition) {
            if ($isAdmin) {
                $prevLesson = $this->em->getRepository(Lesson::class)
                    ->findByPositionNotDeletedLessons($lesson->getCourse(), $prevPosition);
            } else {
                $prevLesson = $this->em->getRepository(Lesson::class)
                    ->findByPositionNotDeletedAndNotHiddenLessons($lesson->getCourse(), $prevPosition);
            }
        }

        $data['prevLesson'] = [];
        if (isset($prevLesson)) {
            $data['prevLesson'] = [
                'id' => $prevLesson['id'],
                'name' => $prevLesson['name'],
            ];
        }

        if ($nextPosition) {
            if ($isAdmin) {
                $nextLesson = $this->em->getRepository(Lesson::class)
                    ->findByPositionNotDeletedLessons($lesson->getCourse(), $nextPosition);
            } else {
                $nextLesson = $this->em->getRepository(Lesson::class)
                    ->findByPositionNotDeletedAndNotHiddenLessons($lesson->getCourse(), $nextPosition);
            }
        }
        $data['nextLesson'] = [];
        if (
            isset($nextLesson) &&
            !(
                $lesson->getIsStopLesson() &&
                $data['currentLesson']['task']['isTaskExist'] &&
                !$data['currentLesson']['task']['isAllChecked'] &&
                !$isAdmin
            )
        ) {
            $data['nextLesson'] = [
                'id' => $nextLesson['id'],
                'name' => $nextLesson['name'],
            ];
        }

        // Получаем части
        $parts = $lesson->getLessonParts();

        foreach ($parts as $part) {
            $blocks = $this->em->getRepository(LessonPartBlock::class)
                ->findByPartNotDeletedBlocks($part);

            // Собираем блоки
            $blocksInPart = [];
            /** @var LessonPartBlock $block */
            foreach ($blocks as $block) {
                $blocksInPart[] = $this->contentBlockHelper->parseBlock(
                    $block->getType(),
                    $block->getVariety(),
                    $block->getBlockId(),
                    $user
                );
            }

            $data['parts'][] = [
                'id' => $part->getId(),
                'blocks' => $blocksInPart,
            ];
        }

        return $this->formHelper->getResponse(
            'getLessonBlocks',
            $data
        );
    }

    public function newLessonBlock(LessonPart $lessonPart, Request $request)
    {
        $blockArray = $request->request->get('block', null);
        $data = [];

        if (!$blockArray) {
            $this->formHelper->addError('block', 'Нет данных урока');
        }

        $this->checkPermission('create', $lessonPart);

        if ($this->formHelper->isValid()) {
            // Новый блок
            $partBlockEntity = new LessonPartBlock();

            $partBlockEntity->setType($blockArray['type']);
            $partBlockEntity->setVariety($blockArray['variety']);

            $partBlockEntity->setPart($lessonPart);

            // Ставим позицию
            if ($prevBlock = $blockArray['prevBlock']) {
                /** @var LessonPartBlock $lessonPrevBlock */
                $lessonPrevBlock = $this->em->getRepository(LessonPartBlock::class)
                    ->findOneBy(
                        [
                            'type' => $prevBlock['type'],
                            'variety' => $prevBlock['variety'],
                            'blockId' => $prevBlock['blockId'],
                        ]
                    );
                $positionInPart = $lessonPrevBlock->getPositionInPart() + 1;

                $blocks = $this->em->getRepository(LessonPartBlock::class)
                    ->findByPartAndPositionNotDeletedBlocks(
                        $lessonPrevBlock->getPart(),
                        $lessonPrevBlock->getPositionInPart()
                    );

                /** @var LessonPartBlock $block */
                foreach ($blocks as $block) {
                    $block->setPositionInPart($block->getPositionInPart() + 1);
                    $this->em->persist($block);
                }
            } else {
                $positionInPart = 1;
            }
            $partBlockEntity->setPositionInPart($positionInPart);

            //Создаем сам блок
            $newBlockId = $this->contentBlockHelper->newBlock($blockArray);
            $partBlockEntity->setBlockId($newBlockId);
            $this->em->persist($partBlockEntity);

            $this->em->flush();
            $data = ['id' => $newBlockId];
        }

        return $this->formHelper->getResponse(
            'newLessonBlock',
            $data
        );
    }

    public function duplicateLessonBlock(Request $request)
    {
        $this->checkIsAdmin();

        $blockArray = $request->request->get('block', null);

        if (!$blockArray) {
            $this->formHelper->addError('block', 'Нет данных урока');
        }

        if ($this->formHelper->isValid()) {
            // Новый урок
            $partBlockEntity = new LessonPartBlock();

            /** @var LessonPartBlock $lessonPartBlock */
            $lessonPartBlock = $this->em->getRepository(LessonPartBlock::class)
                ->findOneBy(
                    [
                        'type' => $blockArray['type'],
                        'variety' => $blockArray['variety'],
                        'blockId' => $blockArray['blockId'],
                    ]
                );

            $partBlockEntity->setPart($lessonPartBlock->getPart());
            $partBlockEntity->setType($blockArray['type']);
            $partBlockEntity->setVariety($blockArray['variety']);

            // Получаем все блоки после дублируемого
            $blocks = $this->em->getRepository(LessonPartBlock::class)
                ->findByPartAndPositionNotDeletedBlocks(
                    $lessonPartBlock->getPart(),
                    $lessonPartBlock->getPositionInPart()
                );

            // Сдвигаем позиции
            /** @var LessonPartBlock $block */
            foreach ($blocks as $block) {
                $block->setPositionInPart($block->getPositionInPart() + 1);
                $this->em->persist($block);
            }

            $partBlockEntity->setPositionInPart($lessonPartBlock->getPositionInPart() + 1);

            //Создаем сам блок
            $newBlockId = $this->contentBlockHelper->newBlock($blockArray);
            $partBlockEntity->setBlockId($newBlockId);
            $this->em->persist($partBlockEntity);
        }

        $this->em->flush();

        return $this->formHelper->getResponse(
            'duplicateLessonBlock',
            ['id' => $newBlockId]
        );
    }

    public function editLessonBlock(Request $request)
    {
        $this->checkIsAdmin();

        $blockArray = $request->request->get('block', null);

        $editedBlockId = null;

        if (!$blockArray) {
            $this->formHelper->addError('block', 'Нет данных урока');
        }

        if ($this->formHelper->isValid()) {
            $editedBlockId = $this->contentBlockHelper->editBlock($blockArray);
        }

        $this->em->flush();

        return $this->formHelper->getResponse(
            'editLessonBlock',
            ['id' => $editedBlockId]
        );
    }

    public function deleteLessonBlock(Request $request)
    {
        $this->checkIsAdmin();

        $blockArray = $request->request->get('block', null);

        if (!$blockArray) {
            $this->formHelper->addError('block', 'Нет данных урока');
        }

        if ($this->formHelper->isValid()) {
            /** @var LessonPartBlock $lessonPartBlock */
            $lessonPartBlock = $this->em->getRepository(LessonPartBlock::class)
                ->findOneBy(
                    [
                        'type' => $blockArray['type'],
                        'variety' => $blockArray['variety'],
                        'blockId' => $blockArray['blockId'],
                    ]
                );

            $blocks = $this->em->getRepository(LessonPartBlock::class)
                ->findByPartAndPositionNotDeletedBlocks(
                    $lessonPartBlock->getPart(),
                    $lessonPartBlock->getPositionInPart()
                );

            /** @var LessonPartBlock $block */
            foreach ($blocks as $block) {
                $block->setPositionInPart($block->getPositionInPart() - 1);
                $this->em->persist($block);
            }

            $lessonPartBlock->setPositionInPart(0);
            $lessonPartBlock->setDeleted(true);
            $this->em->persist($lessonPartBlock);

            $this->em->flush();
        }

        return $this->formHelper->getResponse(
            'deleteLessonBlock',
            ['id' => $lessonPartBlock->getBlockId()]
        );
    }

    public function deleteFileFromBlock(Request $request)
    {
        $this->checkIsAdmin();

        $data = [];
        $files = $request->request->get('files', []);

        foreach ($files as $file) {
            /** @var BaseFiles|null $baseFile */
            $baseFile = $this->em->getRepository(BaseFiles::class)->find($file['id']);
            if (!$baseFile) {
                continue;
            }

            foreach ($baseFile->getBaseBlock6s() as $block6s) {
                $block6s->removeFile($baseFile);
                $this->em->persist($block6s);
            }

            foreach ($baseFile->getTaskTests() as $taskTest) {
                $taskTest->setImgFile(null);
                $this->em->persist($taskTest);
            }

            foreach ($baseFile->getTaskTestQuestions() as $taskTestQuestion) {
                $taskTestQuestion->setImgFile(null);
                $this->em->persist($taskTestQuestion);
            }

            foreach ($baseFile->getTaskTestResults() as $taskTestResult) {
                $taskTestResult->setImgFile(null);
                $this->em->persist($taskTestResult);
            }

            $data[] = [
                'id' => $baseFile->getId(),
            ];
            $this->em->remove($baseFile);
        }
        $this->em->flush();

        return $this->formHelper->getResponse(
            'deleteFileFromBlock',
            $data
        );
    }

    public function upBlock(Request $request)
    {
        $this->checkIsAdmin();

        $blockArray = $request->request->get('block', null);

        if (!$blockArray) {
            $this->formHelper->addError('block', 'Нет данных урока');
        }

        if ($this->formHelper->isValid()) {
            /** @var LessonPartBlock $currentPartBlock */
            $currentPartBlock = $this->em->getRepository(LessonPartBlock::class)
                ->findOneBy(
                    [
                        'type' => $blockArray['type'],
                        'variety' => $blockArray['variety'],
                        'blockId' => $blockArray['blockId'],
                    ]
                );

            $currentPosition = $currentPartBlock->getPositionInPart();

            if ($currentPosition > 1) {
                /** @var LessonPartBlock $prevPartBlock */
                $prevPartBlock = $this->em->getRepository(LessonPartBlock::class)
                    ->findOneBy(
                        [
                            'part' => $currentPartBlock->getPart(),
                            'positionInPart' => $currentPosition - 1,
                        ]
                    );

                $prevPartBlock->setPositionInPart($currentPosition);
                $currentPartBlock->setPositionInPart($currentPosition - 1);

                $this->em->persist($prevPartBlock);
                $this->em->persist($currentPartBlock);
            }
        }

        $this->em->flush();

        return $this->formHelper->getResponse(
            'newLessonBlock',
            ['id' => $currentPartBlock->getId()]
        );
    }

    public function downBlock(Request $request)
    {
        $this->checkIsAdmin();

        $blockArray = $request->request->get('block', null);

        if (!$blockArray) {
            $this->formHelper->addError('block', 'Нет данных урока');
        }

        if ($this->formHelper->isValid()) {
            /** @var LessonPartBlock $currentPartBlock */
            $currentPartBlock = $this->em->getRepository(LessonPartBlock::class)
                ->findOneBy(
                    [
                        'type' => $blockArray['type'],
                        'variety' => $blockArray['variety'],
                        'blockId' => $blockArray['blockId'],
                    ]
                );

            $currentPosition = $currentPartBlock->getPositionInPart();

            $lastPosition = $this->em->getRepository(LessonPartBlock::class)
                ->findByPartLastPosition($currentPartBlock->getPart());

            if ($currentPosition != $lastPosition) {
                /** @var LessonPartBlock $nextPartBlock */
                $nextPartBlock = $this->em->getRepository(LessonPartBlock::class)
                    ->findOneBy(
                        [
                            'part' => $currentPartBlock->getPart(),
                            'positionInPart' => $currentPosition + 1,
                        ]
                    );

                $nextPartBlock->setPositionInPart($currentPosition);
                $currentPartBlock->setPositionInPart($currentPosition + 1);

                $this->em->persist($nextPartBlock);
                $this->em->persist($currentPartBlock);
            }
        }

        $this->em->flush();

        return $this->formHelper->getResponse(
            'downBlock',
            ['id' => $currentPartBlock->getId()]
        );
    }

    public function getBlockInfo(Request $request)
    {
        $data = [];

        /** @var User $user */
        $user = $this->security->getUser();

        $blockId = $request->query->get('id');
        $type = $request->query->get('type');
        $variety = $request->query->get('variety');

        if ($blockId && $type && $variety) {
            $data = $this->contentBlockHelper->parseBlock($type, $variety, $blockId, $user);
        } else {
            $this->formHelper->addError('query', 'Не получены id, type или variety');
        }

        return $this->formHelper->getResponse('getBlockInfo', $data);
    }

    protected function markLessonAsViewed(Lesson $lesson)
    {
        /** @var User $user */
        $user = $this->security->getUser();

        /** @var UserLesson $userLesson */
        $userLesson = $this->em->getRepository(UserLesson::class)->findOneBy(['lesson' => $lesson, 'user' => $user]);
        if (!$userLesson) {
            $userLesson = new UserLesson();
            $userLesson->setUser($user);
            $userLesson->setLesson($lesson);
        }
        if (UserLesson::STATE_NOT_VIEWED === $userLesson->getState()) {
            $userLesson->setState(UserLesson::STATE_VIEWED);
            $this->em->persist($userLesson);
            $this->em->flush();
        }
    }

    private function checkPermission(string $field, LessonPart $lessonPart)
    {
        /** @var User $user */
        $user = $this->security->getUser();
        $checkPermission = $this->userHelper->getLessonPermissionsByCourse(
            $user,
            $lessonPart->getLesson()->getCourse()
        );

        if (!$checkPermission['isAdmin'] || $checkPermission['lessonModuleAdmin'][$field] == 0) {
            return $this->formHelper->addError('block', 'Нет доступа для редактирование этого урока');
        }
    }

    private function checkIsAdmin()
    {
        $isAdmin = $this->userHelper->isUserAdmin();

        if (!$isAdmin) {
            return $this->formHelper->addError('block', 'Нет доступа для редактирование этого урока');
        }
    }

}

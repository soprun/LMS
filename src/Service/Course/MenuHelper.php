<?php

namespace App\Service\Course;

use App\Entity\Courses\Course;
use App\Entity\Courses\Lesson;
use App\Entity\User;
use App\Service\FormHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

class MenuHelper
{

    private $em;
    private $security;
    private $formHelper;
    private $lessonHelper;

    public function __construct(
        EntityManagerInterface $em,
        Security $security,
        FormHelper $formHelper,
        LessonHelper $lessonHelper
    ) {
        $this->em = $em;
        $this->security = $security;
        $this->formHelper = $formHelper;
        $this->lessonHelper = $lessonHelper;
    }

    public function getCourseMenu(Course $course)
    {
        /** @var User $user */
        $user = $this->security->getUser();

        // получение актуального списка уроков
        $dataLessons = [];
        $lessons = $this->em->getRepository(Lesson::class)->findByCourseAndDateLessonsToMenu(
            $course,
            new \DateTime('now')
        );
        foreach ($lessons as $lesson) {
            $dataLessons[] = $this->lessonHelper->getMinLessonArray($lesson);
        }

        // @todo: отрефакторить, раньше тут были numbers от игры, теперь можно сделать проще
        $points = $this->getUserPointsByCourse($user, $course);

        return $this->formHelper->getResponse(
            'getMenu',
            [
                'lessonArray' => $dataLessons,
                'points' => $points,
            ]
        );
    }

    public function getUserPointsByCourse(User $user, Course $course)
    {
        $points =
            $this->em->getRepository(Course::class)->getUserTaskTestNumbers(
                [$course->getId()],
                $user,
                ['points' => 0]
            )['points'] +
            $this->em->getRepository(Course::class)->getUserTaskType5Numbers(
                [$course->getId()],
                $user,
                ['points' => 0]
            )['points'] +
            $this->em->getRepository(Course::class)->getUserPointsSum($course, $user);
        return $points;
    }

}

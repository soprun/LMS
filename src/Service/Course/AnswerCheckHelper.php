<?php

namespace App\Service\Course;

use App\Entity\BaseBlock\BaseBlockTaskWithCheck;
use App\Entity\BaseBlock\BaseFiles;
use App\Entity\Courses\Achievement;
use App\Entity\Courses\Course;
use App\Entity\Courses\CourseFolder;
use App\Entity\Courses\Lesson;
use App\Entity\Courses\LessonPartBlock;
use App\Entity\Notification;
use App\Entity\Task\AnswerComment;
use App\Entity\Task\AnswerTaskType1;
use App\Entity\Task\TaskCheckLog;
use App\Entity\Task\TaskScript;
use App\Entity\Task\TaskSet;
use App\Entity\Task\TaskType1;
use App\Entity\User;
use App\Entity\UserGroup;
use App\Entity\UserLesson;
use App\Service\AuthServerHelper;
use App\Service\FormHelper;
use App\Service\UserHelper;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Security;

class AnswerCheckHelper
{
    private $em;
    private $security;
    private $formHelper;
    private $taskHelper;
    private $fileHelper;
    private $userHelper;
    private $lessonHelper;
    private $certificateService;
    private $authServerHelper;
    private $paginator;
    private $firebasePushHelper;

    public function __construct(
        EntityManagerInterface $em,
        Security $security,
        FormHelper $formHelper,
        TaskHelper $taskHelper,
        FileHelper $fileHelper,
        UserHelper $userHelper,
        LessonHelper $lessonHelper,
        CertificateService $certificateService,
        AuthServerHelper $authServerHelper,
        PaginatorInterface $paginator,
        FirebasePushHelper $firebasePushHelper
    ) {
        $this->em = $em;
        $this->security = $security;
        $this->formHelper = $formHelper;
        $this->taskHelper = $taskHelper;
        $this->fileHelper = $fileHelper;
        $this->userHelper = $userHelper;
        $this->lessonHelper = $lessonHelper;
        $this->certificateService = $certificateService;
        $this->authServerHelper = $authServerHelper;
        $this->paginator = $paginator;
        $this->firebasePushHelper = $firebasePushHelper;
    }

    public function getOneRandomAnswerByTask(Request $request)
    {
        // Парсим Query
        $taskBlockObject = json_decode($request->query->get('taskBlock', null));
        $userInSearch = $request->query->get('userId', null);
        $taskVariety = $taskBlockObject->variety;
        $taskId = $taskBlockObject->id;

        /** @var User $validatingUser */
        $validatingUser = $this->security->getUser();
        $permission = $this->userHelper->getUserPermissionsByField($validatingUser, 'taskAnswerCheckAdmin');

        $data = [
            'taskBlocks' => [],
            'tariffs' => $this->getTariffs(),
        ];

        // Получаем ачивки, если они есть
        $achievementArray = $this->parseAllAchievements();
        if ($achievementArray) {
            $data['achievements'] = $achievementArray;
        }

        $userAnswer = null;
        $taskBlockEntity = null;

        // Проверяем есть ли хоть один непроверенный ответ
        switch ($taskVariety) {
            case 1:
                // Получаем блок с заданием
                /** @var BaseBlockTaskWithCheck $taskBlockEntity */
                $taskBlockEntity = $this->em->getRepository(BaseBlockTaskWithCheck::class)->findOneBy(
                    ['id' => $taskId]
                );
                /** @var AnswerTaskType1 $userAnswer */
                // Смотрим есть ли ответ без проверки
                if ($permission['taskAnswerCheckAdmin']['read'] == 2) {
                    $userAnswer = $this->em->getRepository(AnswerTaskType1::class)
                        ->findByTaskBlockOneUserWithNotCheckedAnswer($taskBlockEntity, $validatingUser, $userInSearch);
                } elseif ($permission['taskAnswerCheckAdmin']['read'] == 1) {
                    $userAnswer = $this->em->getRepository(AnswerTaskType1::class)
                        ->findByTaskBlockAndValidatingOneUserWithNotCheckedAnswer(
                            $taskBlockEntity,
                            $validatingUser,
                            $userInSearch
                        );
                }
                break;
        }
        if ($userAnswer) {
            // Логирование проверяющих
            $checkLog = new TaskCheckLog();
            $checkLog->setReviewer($validatingUser);
            $checkLog->setAnswer($userAnswer);
            /** @var TaskCheckLog[] $lastCheckLogArray */
            $lastCheckLogArray = $this->em->getRepository(TaskCheckLog::class)
                ->findBy(
                    ['answer' => $userAnswer],
                    ['id' => 'DESC'],
                    1
                );
            // Достаём из array[1] первый объект, либо False
            $lastCheckLog = reset($lastCheckLogArray);

            if (!$lastCheckLog) {
                $checkLog->setStatusId(TaskCheckLog::STATUS_NEW_ACCEPTED);
            } elseif (
                $lastCheckLog->getStatusId() === TaskCheckLog::STATUS_NEW_ACCEPTED ||
                $lastCheckLog->getStatusId() === TaskCheckLog::STATUS_TOOK_FROM_ANOTHER
            ) {
                $checkLog->setStatusId(TaskCheckLog::STATUS_TOOK_FROM_ANOTHER);

                $checkLogPrev = new TaskCheckLog();
                $checkLogPrev->setReviewer($validatingUser);
                $checkLogPrev->setAnswer($userAnswer);
                $checkLogPrev->setStatusId(TaskCheckLog::STATUS_PASSED);

                $this->em->persist($checkLogPrev);
                $this->em->flush();
            }

            $this->em->persist($checkLog);
            $this->em->flush();

            $data['taskBlocks'][] = $this->parseTaskBlock($taskBlockEntity, $userAnswer->getUser(), $validatingUser);
        }

        return $this->formHelper->getResponse('getOneAnswerByTask', $data);
    }

    public function getAnswersByTask(Request $request)
    {
        $search = $request->query->get('userId');
        $page = $request->query->get('page', 1);
        $dateStart = $request->query->get('dateStart');
        $dateEnd = $request->query->get('dateEnd');
        $status = $request->query->get('isHasValidator');

        $taskBlockObject = json_decode($request->query->get('taskBlock'));
        $taskVariety = (int)$taskBlockObject->variety;
        $taskId = $taskBlockObject->id;

        $user = $this->security->getUser();
        $permission = $this->userHelper->getUserPermissionsByField($user, 'taskAnswerCheckAdmin');
        $reviewer = null;

        if ($permission['taskAnswerCheckAdmin']['read'] === 1) {
            $reviewer = $user;
        }
        $data = [
            'taskBlocks' => [],
            'tariffs' => $this->getTariffs(),
        ];
        // Получаем ачивки, если они есть
        $achievementArray = $this->parseAllAchievements();
        if ($achievementArray) {
            $data['achievements'] = $achievementArray;
        }

        if ($taskVariety === 1) {
            // Получаем блок с заданием
            $taskBlockEntity = $this->em->getRepository(BaseBlockTaskWithCheck::class)->find($taskId);
            if ($search) {
                $usersWithAnswers['id'] = $search;
            } else {
                $usersWithAnswers = $this->em->getRepository(AnswerTaskType1::class)
                    ->findAnswersByTaskBlock($taskBlockEntity, $reviewer, $status, $dateStart, $dateEnd, $page);
            }

            foreach ($usersWithAnswers as $user) {
                $userFromAnswer = $this->em->getRepository(User::class)->find($user['id']);
                if ($userFromAnswer) {
                    $taskBlock = $this->parseTaskBlock($taskBlockEntity, $userFromAnswer);

                    if ($taskBlock['questionArray'][0]['answer'] != new \stdClass()) {
                        $data['taskBlocks'][] = $this->parseTaskBlock($taskBlockEntity, $userFromAnswer);
                    }
                }
            }
        }
        return $this->formHelper->getResponse('getAnswersByTask', $data);
    }

    public function getCommentsAll(Request $request)
    {
        $data = [];
        $userInSearch = $request->query->get('userId', null);

        // Парсим Query
        $page = $request->query->get('page', 1);
        // Парсим Query
        $paginatorLimit = (int)$request->query->get('pageLimit', 20);

        $state = $request->query->get('state', null);


        /** @var User $validatingUser */
        $validatingUser = $this->security->getUser();
        $permission = $this->userHelper->getUserPermissionsByField($validatingUser, 'taskAnswerCheckAdmin');
        // получаем список пользователей, которых нужно проверить
        $answerCommentRepository = $this->em->getRepository(AnswerComment::class);

        $usersWithComments = [];
        if (!$userInSearch) {
            if ($permission['taskAnswerCheckAdmin']['read'] == 2) {
                $usersWithComments = $answerCommentRepository->getCommentsAll($page, $paginatorLimit, $state);
            } elseif ($permission['taskAnswerCheckAdmin']['read'] == 1) {
                $usersWithComments = $answerCommentRepository->getCommentsAll(
                    $page,
                    $paginatorLimit,
                    $state,
                    $validatingUser
                );
            }
        } else {
            if ($permission['taskAnswerCheckAdmin']['read'] == 2) {
                $usersWithComments = $answerCommentRepository->getCommentsAll(
                    $page,
                    $paginatorLimit,
                    $state,
                    null,
                    $userInSearch
                );
            } elseif ($permission['taskAnswerCheckAdmin']['read'] == 1) {
                $usersWithComments = $answerCommentRepository->getCommentsAll(
                    $page,
                    $paginatorLimit,
                    $state,
                    $validatingUser,
                    $userInSearch
                );
            }
        }

        $usersWithAnswers = [];
        $usersAuthKey = [];
        $answersUsers = [];
        foreach ($usersWithComments as $comment) {
            $usersWithAnswers[$comment['commentator']][] = $comment;

            $usersAuthKey[$comment['commentator']] = 1;
            if (!array_key_exists($comment['ansUserAuth'], $answersUsers)) {
                $answersUsers[$comment['ansUserAuth']] = 1;
            }
        }

        $usersAuthKey = $this->paginator->paginate(
            array_keys($usersAuthKey),
            $page,
            $paginatorLimit
        )->getItems();/**/

        $usersArray = $this->userHelper->getUserInfoArrayByIds($usersAuthKey);
        $answersUsersArray = $this->userHelper->getUserInfoArrayByIds(array_keys($answersUsers));

        foreach ($usersArray as $user) {
            $userId = $user['authUserId'];
            foreach ($usersWithAnswers[$userId] as $comment) {
                /** @var QueryBuilder $qb */
                $qb = $this->em->getRepository(AnswerComment::class)->createQueryBuilder('ac');
                $qb->select('count(ac.id)')
                    ->where('ac.answerId = :answerId')
                    ->setParameter('answerId', $comment['answerId'])
                    ->andWhere('ac.id != :id')
                    ->setParameter('id', $comment['id']);
                $comment['countAnother'] = (int)$qb->getQuery()->getSingleScalarResult();

                $userAuthId = $comment['ansUserAuth'];
                $answerUser = array_values(
                    array_filter(
                        $answersUsersArray,
                        function ($e) use ($userAuthId) {
                            if ($e['authUserId'] === $userAuthId) {
                                return $e;
                            }
                        }
                    )
                );

                $files['commentatorFiles'] = [];
                $commentFiles = $this->em->getRepository(AnswerComment::class)->getCommentFilesId($comment['id']);

                foreach ($commentFiles as $id) {
                    /** @var BaseFiles $file */
                    $file = $this->em
                        ->getRepository(BaseFiles::class)->findOneBy(['id' => $id]);
                    $files['commentatorFiles'][] = [
                        'id' => $file->getId(),
                        'fileName' => $file->getFileName() ?? "",
                        'url' => $this->fileHelper->getFileUrl($file) ?? "",
                        'mimeType' => $file->getMimeType() ?? "",
                    ];
                }

                if ($answerUser) {
                    $answerUser = $answerUser[0];
                }
                $comment['answerUser'] = $answerUser;
                $data['comments'][] = array_merge($comment, $files);
            }
        }

        if (array_key_exists('comments', $data)) {
            $data['comments'] = $this->paginator->paginate(
                array_values($data['comments']),
                $page,
                $paginatorLimit
            )->getItems();

            $data['users'] = $usersArray;
            $data['states'] = [
                AnswerComment::STATE_NOT_VIEWED => 'Не просмотрено',
                AnswerComment::STATE_VIEWED => 'Просмотрено',
            ];
        }

        return $this->formHelper->getResponse('getCommentsAll', $data);
    }

    public function getCommentsByTask(Request $request)
    {
        $data = [];
        $userInSearch = $request->query->get('userId', null);

        // Парсим Query
        $page = $request->query->get('page', 1);
        // Парсим Query
        $paginatorLimit = (int)$request->query->get('pageLimit', 20);
        $taskBlockObject = json_decode($request->query->get('taskBlock', null));
        $taskVariety = $taskBlockObject->variety;
        $taskId = $taskBlockObject->id;

        $state = $request->query->get('state', null);


        /** @var User $validatingUser */
        $validatingUser = $this->security->getUser();
        $permission = $this->userHelper->getUserPermissionsByField($validatingUser, 'taskAnswerCheckAdmin');

        switch ($taskVariety) {
            case 1:
                // Получаем блок с заданием
                /** @var BaseBlockTaskWithCheck $taskBlockEntity */
                $taskBlockEntity = $this->em->getRepository(BaseBlockTaskWithCheck::class)->findOneBy(
                    ['id' => $taskId]
                );
                // получаем список пользователей, которых нужно проверить
                $answerCommentRepository = $this->em->getRepository(AnswerComment::class);

                if (!$userInSearch) {
                    if ($permission['taskAnswerCheckAdmin']['read'] == 2) {
                        $usersWithComments = $answerCommentRepository->getCommentsFeed($taskBlockEntity, $state);
                    } elseif ($permission['taskAnswerCheckAdmin']['read'] == 1) {
                        $usersWithComments = $answerCommentRepository->getCommentsFeed(
                            $taskBlockEntity,
                            $state,
                            $validatingUser
                        );
                    }
                } else {
                    if ($permission['taskAnswerCheckAdmin']['read'] == 2) {
                        $usersWithComments = $answerCommentRepository->getCommentsFeed($state, null, $userInSearch);
                    } elseif ($permission['taskAnswerCheckAdmin']['read'] == 1) {
                        $usersWithComments = $answerCommentRepository->getCommentsFeed(
                            $state,
                            $validatingUser,
                            $userInSearch
                        );
                    }
                }

                $usersWithAnswers = [];
                $usersAuthKey = [];
                $answersUsers = [];
                foreach ($usersWithComments as $comment) {
                    if (array_key_exists($comment['commentator'], $usersWithAnswers)) {
                        if (end($usersWithAnswers[$comment['commentator']])['createdAt'] < $comment['createdAt']) {
                            $usersWithAnswers[$comment['commentator']][] = $comment;
                        }
                    } else {
                        $usersWithAnswers[$comment['commentator']][] = $comment;
                    }
                    $usersAuthKey[$comment['commentator']] = 1;
                    if (!array_key_exists($comment['ansUserAuth'], $answersUsers)) {
                        $answersUsers[$comment['ansUserAuth']] = 1;
                    }
                }

                $usersAuthKey = $this->paginator->paginate(
                    array_keys($usersAuthKey),
                    $page,
                    $paginatorLimit
                )->getItems();

                $usersArray = $this->userHelper->getUserInfoArrayByIds($usersAuthKey);
                $answersUsersArray = $this->userHelper->getUserInfoArrayByIds(array_keys($answersUsers));

                foreach ($usersArray as $user) {
                    $userId = $user['authUserId'];
                    foreach ($usersWithAnswers[$userId] as $comment) {
                        /** @var QueryBuilder $qb */
                        $qb = $this->em->getRepository(AnswerComment::class)->createQueryBuilder('ac');
                        $qb->select('count(ac.id)')
                            ->where('ac.answerId = :answerId')
                            ->setParameter('answerId', $comment['answerId'])
                            ->andWhere('ac.id != :id')
                            ->setParameter('id', $comment['id']);
                        $comment['countAnother'] = (int)$qb->getQuery()->getSingleScalarResult();

                        $userAuthId = $comment['ansUserAuth'];
                        $answerUser = array_values(
                            array_filter(
                                $answersUsersArray,
                                function ($e) use ($userAuthId) {
                                    if ($e['authUserId'] === $userAuthId) {
                                        return $e;
                                    }
                                }
                            )
                        );

                        if ($answerUser) {
                            $answerUser = $answerUser[0];
                        }


                        $files['commentatorFiles'] = [];
                        $commentFiles = $this->em->getRepository(AnswerComment::class)->getCommentFilesId(
                            $comment['id']
                        );

                        foreach ($commentFiles as $id) {
                            /** @var BaseFiles $file */
                            $file = $this->em
                                ->getRepository(BaseFiles::class)->findOneBy(['id' => $id]);
                            $files['commentatorFiles'][] = [
                                'id' => $file->getId(),
                                'fileName' => $file->getFileName() ?? "",
                                'url' => $this->fileHelper->getFileUrl($file) ?? "",
                                'mimeType' => $file->getMimeType() ?? "",
                            ];
                        }

                        $comment['answerUser'] = $answerUser;
                        $data['comments'][] = array_merge($comment, $files);
                        /*Попросили убрать чек на просмотр
                         * if ($validatingUser->getAuthUserId() !== $userId &&
                         * $comment['state'] == AnswerComment::STATE_NOT_VIEWED) {
                            $con = $this->em->getConnection();
                            $con->update('answer_comment', [
                                'state' => AnswerComment::STATE_VIEWED
                            ], [
                                'state' => AnswerComment::STATE_NOT_VIEWED,
                                'id' => $comment['id']
                            ]);
                        }*/
                    }
                }

                if (!array_key_exists('comments', $data)) {
                    break;
                }

                $data['comments'] = $this->paginator->paginate(
                    array_values($data['comments']),
                    $page,
                    $paginatorLimit
                )->getItems();

                $data['users'] = $usersArray;
                $data['states'] = [
                    AnswerComment::STATE_NOT_VIEWED => 'Не просмотрено',
                    AnswerComment::STATE_VIEWED => 'Просмотрено',
                ];
        }

        return $this->formHelper->getResponse('getCommentsByTask', $data);
    }

    public function parseTaskBlock($taskBlockEntity, User $userFromAnswer, ?User $validating = null)
    {
        $questionArrayAndScripts = $this->parseQuestionArrayAndScript($taskBlockEntity, $userFromAnswer, $validating);

        $questionArray = $questionArrayAndScripts['questionArray'];
        $scriptArray = $questionArrayAndScripts['scripts'];

        return [
            'taskBlock' => $this->taskHelper->parseTaskEntity($taskBlockEntity),
            'questionArray' => $questionArray,
            'user' => $this->parseUserFromAnswer($userFromAnswer),
            'scripts' => $scriptArray,
        ];
    }

    public function parseQuestionArrayAndScript($taskBlockEntity, User $userFromAnswer, ?User $validating = null)
    {
        $questionArray['questionArray'] = [];
        $scriptArray = [];

        $sets = $taskBlockEntity->getTaskSets();
        foreach ($sets as $set) {
            switch ($set->getVariety()) {
                case 1:
                    /** @var TaskType1 $task */
                    $task = $this->em->getRepository(TaskType1::class)->findOneBy(
                        [
                            'id' => $set->getBlockId(),
                        ]
                    );

                    $question = $this->taskHelper->parseQuestionEntity($task, $set->getVariety());

                    /** @var AnswerTaskType1 $answerEntity */
                    $answerEntity = $this->em->getRepository(AnswerTaskType1::class)
                        ->findByTask1UserAnswer($task, $userFromAnswer);

                    if ($validating && $answerEntity && !$answerEntity->getIsChecked()) {
                        // Ставим время, когда взято на проверку
                        $answerEntity->setTookToCheck(new \DateTime('now'));
                        $answerEntity->setValidating($validating);
                        $this->em->persist($answerEntity);
                        $this->em->flush();
                    }

                    $answer = $this->taskHelper->parseAnswerEntity($answerEntity, $set->getVariety());

                    $scriptArray = !$scriptArray ? $this->parseScripts(1, $task->getId()) : [];
                    break;
            }

            $questionArray['questionArray'][] = [
                'question' => $question,
                'answer' => $answer,
            ];
        }

        $questionArray['scripts'] = $scriptArray;

        return $questionArray;
    }

    public function parseUserFromAnswer(User $answerUser, ?bool $isOnlyTariffId = false)
    {
        $tariffId = null;
        foreach ($answerUser->getGroupRelations() as $groupRelation) {
            if (
                !$groupRelation->isDeleted()
                && $foundTariffId = $this->getTariffIdByGroup($groupRelation->getUserGroup())
            ) {
                $tariffId = $foundTariffId;
            }
        }

        // берем инфу о юзере
        if (!$isOnlyTariffId) {
            $userArray = $this->userHelper->getUserInfoArrayByIds([$answerUser->getAuthUserId()])[0];
        }
        $userArray['tariffId'] = $tariffId ? $tariffId : '';

        return $userArray;
    }

    public function parseAllAchievements()
    {
        $achievementArray = [];
        /** @var Achievement $achievement */
        foreach ($this->em->getRepository(Achievement::class)->findAll() as $achievement) {
            $imgFile = $achievement->getImgFile();

            $achievementArray[] = [
                'id' => $achievement->getId(),
                'name' => $achievement->getName(),
                'img' => $imgFile ? $this->fileHelper->getFileUrl($imgFile) : "",
            ];
        }

        return $achievementArray;
    }

    public function parseScripts(int $taskType, int $taskId)
    {
        $scripts = $this->em->getRepository(TaskScript::class)
            ->findBy(
                [
                    'taskId' => $taskId,
                    'taskType' => $taskType,
                    'isAfterDdl' => null,
                ],
                [
                    'pointsTo' => 'DESC',
                ]
            );

        $scriptArray = [];
        /** @var TaskScript $script */
        foreach ($scripts as $script) {
            $scriptArray[] = [
                'id' => $script->getId(),
                'name' => $script->getName(),
                'text' => $script->getText(),
                'pointsFrom' => $script->getPointsFrom() ?? 0,
                'pointsTo' => $script->getPointsTo() ?? 0,
            ];
        }

        return $scriptArray;
    }

    /**
     * Список курсов для селекта
     * @return JsonResponse
     */
    public function getCourseList(): JsonResponse
    {
        $data = [];

        foreach ($this->em->getRepository(CourseFolder::class)->findByNotDeleted() as $folder) {
            /** @var CourseFolder $folder */
            $coursesData = [];
            foreach ($folder->getCourses() as $course) {
                $coursesData[] = [
                    'id' => $course->getId(),
                    'name' => $course->getName(),
                ];
            }

            if (!empty($coursesData)) {
                $data[] = [
                    'folderName' => $folder->getName(),
                    'courses' => $coursesData,
                ];
            }
        }

        return $this->formHelper->getResponse('getCourseList', $data);
    }

    /**
     * Список заданий курса для селекта
     * @param  Course  $course
     * @return JsonResponse
     */
    public function getTasksByCourse(Course $course, bool $isChecked = false)
    {
        /** @var User $user */
        $user = $this->security->getUser();
        $permission = $this->userHelper->getUserPermissionsByField($user, 'taskAnswerCheckAdmin');
        $reviewer = null;

        if ($permission['taskAnswerCheckAdmin']['read'] === 1) {
            $reviewer = $user;
        }

        $data = [];
        $lessonPartBlocks = $this->em->getRepository(LessonPartBlock::class)->findByCourseTaskBlocks($course);

        foreach ($lessonPartBlocks as $lessonPartBlock) {
            $blockCount = 0;

            if ($lessonPartBlock['variety']) {
                /** @var BaseBlockTaskWithCheck $taskBlock */
                $taskBlock = $this->em->getRepository(BaseBlockTaskWithCheck::class)
                    ->find($lessonPartBlock['blockId']) ?? [];

                foreach ($taskBlock->getTaskSets() as $set) {
                    if (!$set->isDeleted() && $set->getVariety() === 1) {
                        $task = $this->em->getRepository(TaskType1::class)->find($set->getBlockId());

                        if ($task) {
                            $blockCount += $this->em->getRepository(AnswerTaskType1::class)
                                ->getSumOfAnswersByStatus($task, $reviewer, $isChecked);
                        }
                        if ($blockCount > 0) {
                            $data['tasks'][] = [
                                'id' => $taskBlock->getId(),
                                'systemName' => $taskBlock->getSystemName(),
                                'count' => $blockCount,
                                'variety' => $lessonPartBlock['variety'],
                                'lessonId' => $lessonPartBlock['lessonId'],
                            ];
                        }
                    }
                }
            }
        }
        return $this->formHelper->getResponse('getTasksForValidatingByCourse', $data);
    }

    /**
     * check{
     *      questionArray{
     *          {
     *              answer{
     *                  questionVariety
     *                  answerId
     *              }
     *          },
     *          ...
     *      }
     *      achievements{
     *          id => 1,
     *          id => 2,
     *          ...
     *      }
     * }
     *
     */
    public function checkAnswer(Request $request)
    {
        /** @var User $user */
        $user = $this->security->getUser();
        $data = [];

        $check = $request->request->get('check', null);

        $questionArray = $check['questionArray'];

        foreach ($questionArray as $questionAndAnswer) {
            $answer = $questionAndAnswer['answer'];
            $question = $questionAndAnswer['question'];

            switch ($question['variety']) {
                case 1:
                    /** @var AnswerTaskType1 $answerEntity */
                    $answerEntity = $this->em->getRepository(AnswerTaskType1::class)
                        ->findOneBy(['id' => $answer['id']]);

                    if (!$answerEntity->getValidating() || $answerEntity->getIsChecked() === null) {
                        $answerEntity->setValidating($user);
                        $answerEntity->setIsChecked(true);
                    }
                    $answerEntity->setPoints($answer['points']);
                    $answerEntity->setValidatingText($answer['validatingText']);
                    $data = ['answerId' => $answerEntity->getId()];

                    if (array_key_exists('lessonId', $questionAndAnswer)) {
                        $this->markLessonAsChecked($questionAndAnswer['lessonId']);

                        $answerUser = $answerEntity->getUser();
                        $this->createNotification(
                            'Домашнее задание проверено',
                            "Ваше домашнее задание проверено",
                            Notification::ICON_HOMEWORK,
                            $answerUser,
                            Notification::LINK_TYPE_LESSON,
                            $questionAndAnswer['lessonId'] ?? null
                        );

                        if ($answer['points']) {
                            $this->createNotification(
                                'Баллы начислены',
                                "Вам начислено баллов: {$answer['points']}",
                                Notification::ICON_POINTS,
                                $answerEntity->getUser(),
                                Notification::LINK_TYPE_NO_LINK
                            );
                        }

                        /** @var Course $course */
                        $course = $this->em->getRepository(Course::class)->findByAnswerTaskType1($answerEntity);
                        if ($course) {
                            if ($course->getFolder()->getId() == 2) {
                                $courseName = preg_split('/ /', $course->getName());
                                if (key_exists(0, $courseName) && key_exists(1, $courseName)) {
                                    if ($courseName[0] === 'Скорость' && is_numeric($courseName[1])) {
                                        $allStopLessonsComplete = true;
                                        /** @var Lesson $lesson */
                                        foreach ($course->getLessons() as $key => $lesson) {
                                            if (
                                                $lesson->getIsStopLesson() && !$lesson->getLessonConfiguration(
                                                )->getIsHidden()
                                            ) {
                                                if ($lesson->getId() != $questionAndAnswer['lessonId']) {
                                                    $lessonArray = $this->lessonHelper->getLessonArray(
                                                        $lesson,
                                                        null,
                                                        $answerUser
                                                    );
                                                    if (!$lessonArray['task']['isAllChecked']) {
                                                        $allStopLessonsComplete = false;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        if ($allStopLessonsComplete) {
                                            $certificateData = $this->certificateService->newCertificate(
                                                $answerUser,
                                                $course
                                            );
                                            if (!$certificateData['isSend']) {
                                                $certificateData = $this->certificateService->getCertificate(
                                                    $certificateData['serialNumber']
                                                );
                                                $image = $this->certificateService->createCertificateImg(
                                                    $certificateData
                                                );
                                                $certificateData['image'] = json_encode(
                                                    base64_encode($image->getimageblob())
                                                );
                                                [$authUser, $errors] = $this->authServerHelper->sendCertificate(
                                                    $certificateData
                                                );
                                                if (!empty($errors)) {
                                                    if (!isset($authUser->lmsUserId)) {
                                                        $this->formHelper->addError(
                                                            'lmsUserId',
                                                            'notSendFromAuthServer'
                                                        );
                                                        break;
                                                    } else {
                                                        $this->formHelper->addError(
                                                            $errors[0]['field'],
                                                            $errors[0]['message']
                                                        );
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $this->em->persist($answerEntity);
                    break;
            }
        }
        $this->em->flush();

        return $this->formHelper->getResponse('checkAnswer', $data);
    }

    public function getUsersAnswersByCourse(Course $course)
    {
        // Инфа по курсу
        $data['course'] = [
            'id' => $course->getId(),
            'name' => $course->getName(),
        ];

        $data['taskBlocks'] = [];

        $lessonPartBlocks = $this->em->getRepository(LessonPartBlock::class)->findByCourseTaskBlocks($course);

        /** @var LessonPartBlock $block */
        foreach ($lessonPartBlocks as $block) {
            switch ($block['variety']) { //Вариация блока
                case 1:
                    /** @var BaseBlockTaskWithCheck $taskBlock */
                    $taskBlock = $this->em
                        ->getRepository(BaseBlockTaskWithCheck::class)->findOneBy(['id' => $block['blockId']]);
                    break;
            }

            $dataBlock['taskBlock'] = $this->taskHelper->parseTaskEntity($taskBlock);

            $ansCount = 0;

            /** @var TaskSet $set */
            foreach ($taskBlock->getTaskSets() as $set) { //Проходим по каждому заданию в блоке
                $questionBlock = $this->taskHelper->getQuestionBlockBySet($set, true);

                $question = $questionBlock['questionEntity'];
                $answer = $questionBlock['answerEntity'];

                if ($answer) {
                    $ansCount += 1;
                    $dataBlock['questionArray'][] = [
                        'question' => $this->taskHelper->parseQuestionEntity($question, $set->getVariety()),
                        'answer' => $this->taskHelper->parseAnswerEntity($answer, $set->getVariety()),
                    ];
                }
            }

            if ($ansCount) {
                $data['taskBlocks'][] = $dataBlock;
            }
        }

        return $this->formHelper->getResponse('getAnswersByCourseAndUser', $data);
    }

    /**
     * Определение id тарифа по названию группы
     * @param  UserGroup  $userGroup
     * @return ?int
     */
    public function getTariffIdByGroup(UserGroup $userGroup)
    {
        $groupName = mb_strtolower($userGroup->getName());
        foreach ($this->getTariffs() as $tariff) {
            if (strpos($groupName, mb_strtolower($tariff['name'])) !== false) {
                return $tariff['id'];
            }
        }

        return null;
    }

    public function getTariffs(): array
    {
        return [
            [
                'id' => 1,
                'name' => 'Стандарт',
                'color' => 'A5AFFB',
            ],
            [
                'id' => 2,
                'name' => 'Бизнес',
                'color' => 'D49CFF',
            ],
            [
                'id' => 3,
                'name' => 'Вип',
                'color' => '58D1C1',
            ],
        ];
    }

    private function createNotification(
        $title,
        $description,
        $iconId,
        User $user,
        $linkTypeId = Notification::LINK_TYPE_NO_LINK,
        $linkContent = null,
        $statusId = Notification::STATUS_NEW
    ) {
        if (!$linkContent) {
            $linkTypeId = Notification::LINK_TYPE_NO_LINK;
        }

        $notification = new Notification();
        $notification->setTitle($title);
        $notification->setDescription($description);
        $notification->setStatusId($statusId);
        $notification->setIconId($iconId);
        $notification->setLinkTypeId($linkTypeId);
        $notification->setLinkContent($linkContent);
        $notification->setUser($user);
        $this->em->persist($notification);
        $this->em->flush();

        // отправка уведомления в Firebase
        $this->firebasePushHelper->sendNotificationToFirebase($notification, $user);
    }

    /**
     * @param  int  $lessonId
     */
    protected function markLessonAsChecked(int $lessonId)
    {
        /** @var Lesson $lesson */
        $lesson = $this->em->getRepository(Lesson::class)->find($lessonId);
        if (!$lesson) {
            return;
        }

        /** @var User $user */
        $user = $this->security->getUser();

        /** @var UserLesson $userLesson */
        $userLesson = $this->em->getRepository(UserLesson::class)->findOneBy(['lesson' => $lesson, 'user' => $user]);
        if (!$userLesson) {
            $userLesson = new UserLesson();
            $userLesson->setUser($user);
            $userLesson->setLesson($lesson);
        }
        $userLesson->setState(UserLesson::STATE_ANSWER_CHECKED);
        $this->em->persist($userLesson);
        $this->em->flush();
    }

}

<?php

namespace App\Service\Course;

use App\DTO\Certificate\CertificateImageDto;
use App\Entity\Courses\Course;
use App\Entity\CourseStream;
use App\Entity\User;
use App\Entity\UserCertificates;
use App\Service\FormHelper;
use App\Service\UserHelper;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Imagick;
use ImagickDraw;
use ImagickDrawException;
use ImagickException;
use Symfony\Component\HttpKernel\KernelInterface;

define('SECRET', 'lmsSpeedCertificates2020');

class CertificateService
{
    public const SPEED_CLUB_SLUG = 'speed_club';
    public const HUNDRED_SLUG = 'hundred';
    public const MI_SLUG = 'mi';

    public const HUNDRED_CERT_NAME = 'hundredCert';
    public const MI_CERT_NAME = 'miCert';
    public const SPEED_CERT_NAME = 'speedCert';

    private $em;
    private $formHelper;
    private $userHelper;
    private $appKernel;

    public function __construct(
        EntityManagerInterface $em,
        KernelInterface $appKernel,
        FormHelper $formHelper,
        UserHelper $userHelper
    ) {
        $this->em = $em;
        $this->appKernel = $appKernel;
        $this->formHelper = $formHelper;
        $this->userHelper = $userHelper;
    }

    /**
     * Создаем сертификат для пользователя через поток
     * Если уже создан вернет в массиве isSend = true
     *
     * @param int $userId
     * @param CourseStream $courseStream
     *
     * @return array
     * @throws Exception
     */
    public function newCertificateByStream(int $userId, CourseStream $courseStream): array
    {
        $user = $this->em->getRepository(User::class)->find($userId);
        if (!$user) {
            return [];
        }

        $authUser = $this->userHelper->getUserInfoArrayByIds([$user->getAuthUserId()]);
        if ($authUser) {
            $authUser = $authUser[0];
        } else {
            return [];
        }

        $isSend = true;

        /** @var UserCertificates $qb */
        $userCertificate = $this->em->getRepository(UserCertificates::class)->findOneBy([
            'courseStream' => $courseStream,
            'user' => $user,
        ]);

        if (!$userCertificate) {
            $isSend = false;
            $serial = $courseStream->getStream() . '-' . $userId . $courseStream->getAbstractCourse()->getId();

            $userCertificate = new UserCertificates();

            $userCertificate->setUser($user)
                ->setCourseStream($courseStream)
                ->setSerial($serial)
                ->setSerialNumber($this->newSerialNumber());

            $this->em->persist($userCertificate);
            $this->em->flush();
        }

        return [
            'serialNumber' => $userCertificate->getSerialNumber(),
            'email' => $authUser['email'],
            'isSend' => $isSend,
        ];
    }

    public function newCertificate(User $user, Course $course): array
    {
        $authUser = $this->userHelper->getUserInfoArrayByIds([$user->getAuthUserId()]);
        if ($authUser) {
            $authUser = $authUser[0];
        } else {
            return [];
        }

        $isSend = true;
        /** @var UserCertificates $qb */
        $userCertificate = $this->em->getRepository(UserCertificates::class)->findOneBy([
            'course' => $course,
            'user' => $user,
        ]);

        if (!$userCertificate) {
            /** @var QueryBuilder $qb */
            $qb = $this->em
                ->getRepository(UserCertificates::class)
                ->createQueryBuilder('uc')
                ->select('count(uc.id)')
                ->where('uc.course = :course')
                ->setParameter(':course', $course);

            $countCert = $qb->getQuery()->getSingleScalarResult() + 1;
            $courseName = $course->getName();

            if (mb_stripos($courseName, 'СКОРОСТЬ') !== false) {
                $serial = filter_var($courseName, FILTER_SANITIZE_NUMBER_INT) . '-' . $countCert;
            } else {
                $serial = '555-' . $countCert;
            }

            $userCertificate = new UserCertificates();

            $userCertificate->setUser($user)
                ->setCourse($course)
                ->setSerial($serial)
                ->setSerialNumber($this->newSerialNumber());

            $this->em->persist($userCertificate);
            $this->em->flush();
            $isSend = false;
        }

        return [
            'serialNumber' => $userCertificate->getSerialNumber(),
            'email' => $authUser['email'],
            'isSend' => $isSend,
        ];
    }

    /**
     * @throws NonUniqueResultException
     * @throws Exception
     */
    public function getCertificate($serialNumber): array
    {
        $data = [];

        /** @var QueryBuilder $qb */
        $qb = $this->em
            ->getRepository(UserCertificates::class)
            ->createQueryBuilder('UserCertificates')
            ->innerJoin('UserCertificates.user', 'user')
            ->select('UserCertificates, user')
            ->where('UserCertificates.serialNumber = :serialNumber')
            ->orWhere('UserCertificates.serial = :serialNumber')
            ->setParameter(':serialNumber', $serialNumber);

        /** @var UserCertificates $userCertificate */
        $userCertificate = $qb->getQuery()->getOneOrNullResult();
        if (
            !$userCertificate
            || !$this->checkSerialNumber($userCertificate->getSerialNumber())
        ) {
            return $data;
        }
        if (!$userCertificate->getCourse()) {
            $stream = $userCertificate->getCourseStream();

            return $this->getCertDataFromStream($userCertificate, $stream);
        }

        $course = $userCertificate->getCourse();

        return $this->getCertDataFromCourse($userCertificate, $course);
    }

    private function getCertDataFromStream(UserCertificates $userCertificate, CourseStream $stream): array
    {
        $user = $userCertificate->getUser();
        $authUser = $this->userHelper->getUserInfoArrayByIds([$user->getAuthUserId()]);
        if ($authUser) {
            $authUser = $authUser[0];
        } else {
            $this->formHelper->addError('getCertificate', 'AuthUserNotFound');

            return $this->formHelper->getResponse('getCertificate', []);
        }
        $startDate = clone $stream->getStartDate();

        $dateCreate = $startDate->format('d.m.Y');
        $dateOfFinish = $startDate->modify($stream->getPeriod() . ' week')->format('d.m.Y');
        $serialNumber = $userCertificate->getSerial();


        $serial = $stream->getAbstractCourse()->getCertSerial();
        $slug = $stream->getAbstractCourse()->getSlug();
        $path = $this->appKernel->getProjectDir() . '/public/lms/img/cert_' . $slug . '.jpg';

        $handler = $this->getImageHandler($slug);

        return [
            'serialString' => $serial . $serialNumber . ' от ' . $dateOfFinish,
            'studentNameString' => $authUser['lastname'] . ' ' . $authUser['name'] . ' ' . $authUser['patronymic'],
            'dateString' => 'с ' . $dateCreate . ' г. по ' . $dateOfFinish . ' г. прошел/прошла обучение',
            'path' => $path,
            'serialNumber' => $userCertificate->getSerialNumber(),
            'email' => $authUser['email'],
            'imageHandler' => $handler,
        ];
    }

    private function getCertDataFromCourse(UserCertificates $userCertificate, Course $course): array
    {
        $user = $userCertificate->getUser();
        $authUser = $this->userHelper->getUserInfoArrayByIds([$user->getAuthUserId()]);
        if ($authUser) {
            $authUser = $authUser[0];
        } else {
            $this->formHelper->addError('getCertificate', 'AuthUserNotFound');

            return $this->formHelper->getResponse('getCertificate', []);
        }

        $dateCreate = $userCertificate->getCreatedAt()->format("d.m.Y");
        $defaultStart = $userCertificate->getCreatedAt()->modify("-6 week")->format("d.m.Y");
        $dateOfStart = $course->getCourseConfiguration()->getDateOfStart()
            ? $course->getCourseConfiguration()->getDateOfStart()->format("d.m.Y")
            ?? $defaultStart
            : $defaultStart;
        $dateOfFinish = $course->getCourseConfiguration()->getDateOfFinish()
            ? $course->getCourseConfiguration()->getDateOfFinish()->format("d.m.Y")
            ?? $dateCreate
            : $dateCreate;

        $serialNumber = $userCertificate->getSerial();

        $handler = 'renderSpeedCert';

        return [
            'serialString' => '№ 0' . $serialNumber . ' от ' . $dateCreate,
            'studentNameString' => $authUser['lastname'] . ' ' . $authUser['name'] . ' ' . $authUser['patronymic'],
            'dateString' => 'с ' . $dateOfStart . ' г. по ' . $dateOfFinish . ' г. прошел/прошла обучение',
            'path' => $this->appKernel->getProjectDir() . '/public/lms/img/cert_speed.jpg',
            'serialNumber' => $userCertificate->getSerialNumber(),
            'email' => $authUser['email'],
            'imageHandler' => $handler,
        ];
    }

    /**
     * @throws ImagickException
     * @throws ImagickDrawException
     */
    public function createCertificateImg($dataCertificate): Imagick
    {
        $imageUrl = $dataCertificate['path'];
        $serialString = $dataCertificate['serialString'];
        $studentNameString = $dataCertificate['studentNameString'];
        $dateString = $dataCertificate['dateString'];

        $image = new Imagick($imageUrl);

        $certImageDto = new CertificateImageDto();
        $certImageDto->setImage($image)
            ->setSerialString($serialString)
            ->setStudentNameString($studentNameString)
            ->setDateString($dateString);

        if (method_exists($this, $dataCertificate['imageHandler'])) {
            return $this->{$dataCertificate['imageHandler']}($certImageDto);
        }

        return $this->renderSpeedCert($certImageDto);
    }

    /**
     * @throws ImagickDrawException
     * @throws ImagickException
     */
    private function renderMsaCert(CertificateImageDto $certificateImageDto): Imagick
    {
        $draw = new ImagickDraw();

        $draw->setFillColor('black');

        /* Font properties */
        $draw->setFont($this->appKernel->getProjectDir() . '/public/lms/fonts/Gilroy-Bold.woff');
        $draw->setTextAlignment(Imagick::ALIGN_CENTER);

        $image = $certificateImageDto->getImage();
        $pointX = $image->getImageWidth() / 2;
        $halfHeight = $image->getImageHeight() / 2;

        $serialString = $certificateImageDto->getSerialString();
        $draw->setFontSize(100);
        $draw->annotation($pointX, $halfHeight - 300, $serialString);

        $studentNameString = $certificateImageDto->getStudentNameString();


        $draw->setFontSize(120);
        $draw->annotation($pointX, $halfHeight + 220, $studentNameString);

        $dateString = $certificateImageDto->getDateString();
        $draw->setFont($this->appKernel->getProjectDir() . '/public/lms/fonts/Gilroy-Medium.woff');
        $draw->setFontSize(60);
        $draw->annotation($pointX + 20, $halfHeight + 340, $dateString);
        /* Create text */
        $image->drawImage($draw);

        return $image;
    }

    private function renderMsaLiteCert(CertificateImageDto $certificateImageDto): Imagick
    {
        return $this->renderHundredCert($certificateImageDto);
    }

    /**
     * @throws ImagickDrawException
     * @throws ImagickException
     */
    private function renderSpeedCert(CertificateImageDto $certificateImageDto): Imagick
    {
        $draw = new ImagickDraw();

        $draw->setFillColor('black');

        /* Font properties */
        $draw->setFont($this->appKernel->getProjectDir() . '/public/lms/fonts/Gilroy-Regular.woff');
        $draw->setTextAlignment(Imagick::ALIGN_LEFT);

        $image = $certificateImageDto->getImage();
        $pointX = $image->getImageWidth() / 2;
        $halfHeight = $image->getImageHeight() / 2;

        $serialString = $certificateImageDto->getSerialString();
        $draw->setFontSize(100);
        $draw->annotation($pointX - 420, $halfHeight - 620, $serialString);

        $studentNameString = $certificateImageDto->getStudentNameString();
        $draw->setFont($this->appKernel->getProjectDir() . '/public/lms/fonts/Gilroy-Bold.woff');
        $draw->setFontSize(100);
        $draw->annotation($pointX - 420, $halfHeight - 40, $studentNameString);

        $dateString = $certificateImageDto->getDateString();
        $draw->setFont($this->appKernel->getProjectDir() . '/public/lms/fonts/Gilroy-Regular.woff');
        $draw->setFontSize(64);
        $draw->annotation($pointX - 420, $halfHeight + 186, $dateString);
        /* Create text */
        $image->drawImage($draw);

        return $image;
    }

    /**
     * @throws ImagickDrawException
     * @throws ImagickException
     */
    private function renderHundredCert(CertificateImageDto $certificateImageDto): Imagick
    {
        $draw = new ImagickDraw();

        $draw->setFillColor('white');

        /* Font properties */
        $draw->setFont($this->appKernel->getProjectDir() . '/public/lms/fonts/Gilroy-Regular.woff');
        $draw->setTextAlignment(Imagick::ALIGN_CENTER);

        $image = $certificateImageDto->getImage();
        $pointX = $image->getImageWidth() / 2;
        $halfHeight = $image->getImageHeight() / 2;

        $serialString = $certificateImageDto->getSerialString();
        $draw->setFontSize(100);
        $draw->annotation($pointX, $halfHeight - 500, $serialString);

        $studentNameString = $certificateImageDto->getStudentNameString();
        $draw->setFillColor('black');
        $draw->setFont($this->appKernel->getProjectDir() . '/public/lms/fonts/Gilroy-Bold.woff');
        $draw->setFontSize(120);
        $draw->annotation($pointX, $halfHeight + 260, $studentNameString);

        $dateString = $certificateImageDto->getDateString();
        $draw->setFont($this->appKernel->getProjectDir() . '/public/lms/fonts/Gilroy-Medium.woff');
        $draw->setFontSize(60);
        $draw->annotation($pointX + 20, $halfHeight + 480, $dateString);
        /* Create text */
        $image->drawImage($draw);

        return $image;
    }

    /**
     * @throws ImagickDrawException
     * @throws ImagickException
     */
    private function renderMiCert(CertificateImageDto $certificateImageDto): Imagick
    {
        $draw = new ImagickDraw();

        $draw->setFillColor('black');

        /* Font properties */
        $draw->setFont($this->appKernel->getProjectDir() . '/public/lms/fonts/Gilroy-Regular.woff');
        $draw->setTextAlignment(Imagick::ALIGN_LEFT);

        $image = $certificateImageDto->getImage();
        $halfHeight = $image->getImageHeight() / 2;

        $serialString = $certificateImageDto->getSerialString();
        $draw->setFontSize(122);
        $draw->annotation(251, 1050, $serialString);

        $studentNameString = $certificateImageDto->getStudentNameString();
        $draw->setFont($this->appKernel->getProjectDir() . '/public/lms/fonts/Gilroy-Bold.woff');
        $draw->setFontSize(120);
        $draw->annotation(251, $halfHeight + 360, $studentNameString);

        /* Create text */
        $image->drawImage($draw);

        return $image;
    }

    /**
     * Генератор серийника для админов
     *
     * @return string
     * @throws Exception
     */
    public function generateSerialNumber(): string
    {
        return $this->newSerialNumber();
    }

    /**
     * Создаёт серийный номер с заданной проверочной частью.
     *
     * @param null|string $check Часть серийного номера, используемая для проверки.
     *
     * @return string
     * @throws Exception
     */
    private function newSerialNumber(string $check = null): string
    {
        $template = 'XXX99-XXX99-99XXX-99XXX';

        $parts = explode('-', $template, 2);
        if (!isset($check)) {
            $check = '';
            for ($i = 0, $iMax = strlen($parts[0]); $i < $iMax; $i++) {
                switch ($parts[0][$i]) {
                    case 'X':
                        $check .= chr(random_int(65, 90));
                        break;
                    case '9':
                        $check .= random_int(0, 9);
                        break;
                }
            }
        }

        $serialNumber = $check . '-';
        $hash = hash('sha256', $check . SECRET);

        for ($i = 0, $iMax = strlen($parts[1]); $i < $iMax; $i++) {
            switch ($parts[1][$i]) {
                case 'X':
                    $serialNumber .= chr(65 + ord($hash[$i]) % 26);
                    break;
                case '9':
                    $serialNumber .= ord($hash[$i]) % 10;
                    break;
                case '-':
                    $serialNumber .= '-';
                    break;
            }
        }

        return $serialNumber;
    }

    /**
     * Проверяет серийный номер.
     *
     * @param $serialNumber
     *
     * @return bool
     * @throws Exception
     */
    public function checkSerialNumber($serialNumber): bool
    {
        $parts = explode('-', $serialNumber, 2);

        return ($this->newSerialNumber($parts[0]) === $serialNumber);
    }

    private function getImageHandler(string $slug): string
    {
        return 'render' . str_replace('_', '', ucwords($slug, '_')) . 'Cert';
    }

}

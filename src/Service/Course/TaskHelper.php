<?php

namespace App\Service\Course;

use App\Entity\BaseBlock\BaseBlockTaskWithCheck;
use App\Entity\BaseBlock\BaseFiles;
use App\Entity\Courses\Lesson;
use App\Entity\Courses\LessonPartBlock;
use App\Entity\GroupPermission;
use App\Entity\Notification;
use App\Entity\Task\AnswerComment;
use App\Entity\Task\AnswerTaskType1;
use App\Entity\Task\AnswerTaskType5;
use App\Entity\Task\TaskScript;
use App\Entity\Task\TaskSet;
use App\Entity\Task\TaskTestQuestion;
use App\Entity\Task\TaskTestQuestionAnswer;
use App\Entity\Task\TaskTestQuestionUserAnswer;
use App\Entity\Task\TaskType1;
use App\Entity\Task\TaskType5;
use App\Entity\User;
use App\Entity\UserLesson;
use App\Service\FormHelper;
use App\Service\UserHelper;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class TaskHelper
{
    private $em;
    private $security;
    private $formHelper;
    private $fileHelper;
    private $userHelper;
    private $firebasePushHelper;
    private $logger;
    private $translator;

    public function __construct(
        EntityManagerInterface $em,
        Security $security,
        FormHelper $formHelper,
        FileHelper $fileHelper,
        UserHelper $userHelper,
        FirebasePushHelper $firebasePushHelper,
        LoggerInterface $logger,
        TranslatorInterface $translator
    ) {
        $this->em = $em;
        $this->security = $security;
        $this->formHelper = $formHelper;
        $this->fileHelper = $fileHelper;
        $this->userHelper = $userHelper;
        $this->firebasePushHelper = $firebasePushHelper;
        $this->logger = $logger;
        $this->translator = $translator;
    }

    public function getTaskInfo(Request $request, ?bool $withAnswer = false)
    {
        $taskBlock = json_decode($request->query->get('taskBlock', null));

        $block = $this->getTaskBlockByVarietyAndId($taskBlock->variety, $taskBlock->id);

        $data = $this->getTaskArray($block, $withAnswer);

        return $this->formHelper->getResponse('getTaskInfo', $data);
    }

    public function getTaskArray($block, ?bool $withAnswer = false)
    {
        $data = [
            'taskBlock' => $this->parseTaskEntity($block),
            'questionArray' => $this->getQuestionsArrayByTaskBlock($block, $withAnswer),
        ];

        return $data;
    }

    /**
     * Получить блок с заданием по типу задания и ID блока
     */
    public function getQuestionsArrayByTaskBlock($block, ?bool $withAnswer = false)
    {
        $sets = $block->getTaskSets();

        $questionArray = [];

        /** @var TaskSet $set */
        foreach ($sets as $set) {
            $questionBlock = $this->getQuestionBlockBySet($set, $withAnswer);

            $questionEntity = $questionBlock['questionEntity'];

            $data['question'] = $this->parseQuestionEntity($questionEntity, $set->getVariety());

            if ($withAnswer) {
                $data['answer'] = $this->parseAnswerEntity($questionBlock['answerEntity'], $set->getVariety());
            } else {
                $data['answer'] = new \stdClass();
            }

            $questionArray[] = $data;
        }

        return $questionArray;
    }

    public function parseTaskEntity($taskBlock)
    {
        if ($taskBlock) {
            $data = [
                'id' => $taskBlock->getId(),
                'systemName' => $taskBlock->getSystemName(),
                'ddl' => $taskBlock->getDdl() ? $taskBlock->getDdl()->format("Y-m-d H:i:s") : "",
            ];
        } else {
            $data = new \stdClass();
        }

        return $data;
    }

    public function parseQuestionEntity($questionEntity, int $variety)
    {
        if ($questionEntity) {
            $data = [
                'id' => $questionEntity->getId(),
                'variety' => $variety ?? "",
                'question' => $questionEntity->getQuestion() ?? "",
                'textBeforeDdl' => $questionEntity->getTextBeforeDdl() ?? "",
                'textAfterDdl' => $questionEntity->getTextAfterDdl() ?? "",
                'typeOfTask' => $questionEntity->getTypeOfTask(),
                'isAutoCheck' => $questionEntity->getIsAutoCheck() ? true : false,
                'isReviewAfterDdl' => $questionEntity->getReviewAfterDdl() ? true : false,
                'minPoints' => $questionEntity->getMinPoints() ?? "",
                'maxPoints' => $questionEntity->getMaxPoints() ?? "",
                'pointsBeforeDdl' => $questionEntity->getPointsBeforeDdl() ?? "",
                'pointsAfterDdl' => $questionEntity->getPointsAfterDdl() ?? "",
                'isFileAnswerAllow' => $questionEntity->getIsFileAnswerAllow() ? true : false,
                'isLinkForbidden' => $questionEntity->getIsLinkForbidden() ? true : false,
            ];
        } else {
            $data = new \stdClass();
        }

        return $data;
    }

    public function parseAnswerEntity($answerEntity, $variety)
    {
        if ($answerEntity) {
            $files = [];
            foreach ($answerEntity->getAnswerFiles() as $file) {
                $files[] = [
                    'id' => $file->getId(),
                    'fileName' => $file->getFileName() ?? "",
                    'url' => $this->fileHelper->getFileUrl($file) ?? "",
                    'mimeType' => $file->getMimeType() ?? "",
                ];
            }

            $createdAt = $answerEntity->getCreatedAt();
            $createdAtMoscow = new \DateTime($createdAt->format("Y-m-d H:i:s"));
            $createdAtMoscow->setTimezone(new \DateTimeZone('Europe/Moscow'));

            $validatorName = $validatorAvatar = '';
            if ($validator = $answerEntity->getValidating()) {
                /** @var User $validator */
                $userArray = $this->userHelper->getUserInfoArrayByIds([$validator->getAuthUserId()])[0];
                $validatorName = $userArray['name'] . ' ' . $userArray['lastname'] . ' ' . $userArray['patronymic'];
                $validatorAvatar = $userArray['avatar'];
            }

            $comments = $this->getTaskAnswerComments($answerEntity->getId(), $variety);

            $data = [
                'id' => $answerEntity->getId(),
                'answer' => $answerEntity->getAnswer() ?? "",
                'comments' => $comments,
                'isChecked' => $answerEntity->getIsChecked() ? true : false,
                'points' => $answerEntity->getPoints() ?? "",
                'validatingText' => $answerEntity->getValidatingText() ?? "",
                'validatorName' => $validatorName,
                'validatorAvatar' => $validatorAvatar,
                'createdAt' => $createdAtMoscow ? $createdAtMoscow->format("Y-m-d H:i:s") : "",
                'files' => $files,
            ];
        } else {
            $data = new \stdClass();
        }

        return $data;
    }

    /**
     * Получить блок с заданием по типу задания и ID блока
     */
    public function getTaskBlockByVarietyAndId(int $blockVariety, int $blockId)
    {
        $block = null;
        switch ($blockVariety) {
            case 1:
                $block = $this->em->getRepository(BaseBlockTaskWithCheck::class)->findOneBy(['id' => $blockId]);
                break;
        }

        return $block;
    }

    public function getQuestionBlockBySet(TaskSet $set, ?bool $withAnswer = false)
    {
        $questionBlock = [
            'questionEntity' => null,
            'answerEntity' => null,
        ];

        $user = $this->security->getUser();

        switch ($set->getVariety()) {
            case 1:
                /** @var TaskType1 $questionBlock */
                $questionBlock['questionEntity'] = $this->em->getRepository(TaskType1::class)->findOneBy(
                    [
                        'id' => $set->getBlockId(),
                    ]
                );

                if ($withAnswer) {
                    /** @var AnswerTaskType1 $answerEntity */
                    $questionBlock['answerEntity'] = $this->em->getRepository(AnswerTaskType1::class)
                        ->findOneBy(['task' => $questionBlock['questionEntity'], 'user' => $user]);
                }

                break;
        }

        return $questionBlock;
    }

    public function getAnswerByQuestionAndUser(int $questionVariety, $questionBlock, User $user)
    {
        switch ($questionVariety) {
            case 1:
                /** @var AnswerTaskType1 $answerEntity */
                $answerEntity = $this->em->getRepository(AnswerTaskType1::class)
                    ->findOneBy(['task' => $questionBlock, 'user' => $this->security->getUser()]);

                break;
        }

        return $answerEntity;
    }

    /**
     * answers
     *      taskBlock [
     *              id: 3
     *              type: 2
     *              variety: 1
     *      ],
     *      questionArray[
     *          [
     *              question [
     *                  id: 1
     *                  variety: 1
     *              ],
     *              answer [
     *                  answer = "Текст ответа"
     *              ]
     *          ],
     *          [
     *              ...
     *          ]
     *      ]
     * @param  Request  $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function newTaskAnswer(Request $request)
    {
        $parsedUrl = parse_url($request->headers->get('referer'));
        $path = explode('/', $parsedUrl['path']);
        $lessonId = array_pop($path);
        $userLessonState = UserLesson::STATE_ANSWER_SENT;

        $data = [];

        $answers = $request->request->get('answers');

        /** @var BaseBlockTaskWithCheck $taskBlock */
        $taskBlock = $this->em->getRepository(BaseBlockTaskWithCheck::class)
            ->findOneBy(['id' => $answers['taskBlock']['id']]);

        /** @var User $user */
        $user = $this->security->getUser();

        foreach ($answers['setOfAnswers'] as $ans) {
            $question = $ans['question'];
            $answer = $ans['answer'];

            switch ($question['variety']) {
                case 1:
                    /** @var TaskType1 $task */
                    $task = $this->em->getRepository(TaskType1::class)
                        ->findOneBy(['id' => $question['id']]);

                    $answerExist = $this->em->getRepository(AnswerTaskType1::class)
                        ->findOneBy(['task' => $task, 'user' => $user]);

                    $answerEntity = $answerExist ? $answerExist : new AnswerTaskType1();

                    $answerEntity->setTask($task);
                    $answerEntity->setAnswer($this->formHelper->toRealStr($answer['answer'], 2000));
                    $answerEntity->setUser($user);

                    foreach ($answer['files'] as $file) {
                        /** @var BaseFiles $fileEntity */
                        $fileEntity = $this->em
                            ->getRepository(BaseFiles::class)->findOneBy(['id' => $file['id']]);
                        if ($fileEntity) {
                            $answerEntity->addAnswerFile($fileEntity);
                        }
                    }

                    // Переводим текущее время в московское
                    $now = new \DateTime('now');
                    $now->setTimezone(new \DateTimeZone('Europe/Moscow'));

                    // Мы забираем дату из БД и притворяемся, что там московское время
                    $ddlMoscow = null;
                    if ($ddl = $taskBlock->getDdl()) {
                        $ddlMoscow = new \DateTime($ddl->format("Y-m-d H:i:s"), new \DateTimeZone('Europe/Moscow'));
                    }

                    // Если задание с автопроверкой
                    if ($task->getIsAutoCheck()) {
                        if ($now->getTimestamp() > $ddlMoscow->getTimestamp()) {
                            $answerEntity->setPoints($task->getPointsAfterDdl());
                            $script = $this->em->getRepository(TaskScript::class)
                                ->findOneBy(
                                    [
                                        'taskType' => $question['variety'],
                                        'taskId' => $task->getId(),
                                        'isAfterDdl' => true,
                                    ]
                                );
                            $defaultText = $this->translator->trans('tasks.autoCheckExpired');
                            $validatingText = $defaultText;

                            if ($script) {
                                $validatingText = $script->getText();
                            } elseif ($task->getTextAfterDdl()) {
                                $validatingText = $task->getTextAfterDdl();
                            }

                            $answerEntity->setValidatingText($validatingText);
                        } else {
                            $answerEntity->setPoints($task->getPointsBeforeDdl());
                            $script = $this->em->getRepository(TaskScript::class)
                                ->findOneBy(
                                    [
                                        'taskType' => $question['variety'],
                                        'taskId' => $task->getId(),
                                        'pointsTo' => $task->getPointsBeforeDdl(),
                                    ]
                                );
                            $defaultText = $this->translator->trans('tasks.autoCheck');
                            $validatingText = $defaultText;

                            if ($script) {
                                $validatingText = $script->getText();
                            } elseif ($task->getTextBeforeDdl()) {
                                $validatingText = $task->getTextBeforeDdl();
                            }

                            $answerEntity->setValidatingText($validatingText);
                        }
                        $answerEntity->setIsChecked(true);
                        $userLessonState = UserLesson::STATE_ANSWER_CHECKED;
                    } elseif ($ddlMoscow != null && !$task->getReviewAfterDdl()) {
                        // Если сдаем после DDL и это не автопроверка,
                        // то заданию проставляем 0 балло и оно не попадает на проверку
                        // Но не трогаем если задание надо проверять после DDL
                        if ($now->getTimestamp() > $ddlMoscow->getTimestamp()) {
                            $answerEntity->setPoints(0);

                            $answerEntity->setIsChecked(true);

                            $script = $this->em->getRepository(TaskScript::class)
                                ->findOneBy(
                                    [
                                        'taskType' => $question['variety'],
                                        'taskId' => $task->getId(),
                                        'isAfterDdl' => true,
                                    ]
                                );
                            if ($script) {
                                $answerEntity->setValidatingText($script->getText());
                            }
                            $userLessonState = UserLesson::STATE_ANSWER_CHECKED;
                        }
                    }

                    break;
            }
            $this->markLesson($lessonId, $userLessonState);

            $this->em->persist($answerEntity);
            $this->em->flush();

            $data['answersIds'][] = $answerEntity->getId();
        }

        return $this->formHelper->getResponse(
            'newTaskAnswer',
            $data
        );
    }

    public function editTaskAnswer(Request $request)
    {
        $answers = $request->request->get('answers');

        /** @var BaseBlockTaskWithCheck $taskBlock */
        $taskBlock = $this->em->getRepository(BaseBlockTaskWithCheck::class)
            ->findOneBy(['id' => $answers['taskBlock']['id']]);

        foreach ($answers['setOfAnswers'] as $ans) {
            $question = $ans['question'];
            $answer = $ans['answer'];

            switch ($question['variety']) {
                case 1:
                    $answerEntity = $this->em->getRepository(AnswerTaskType1::class)->findOneBy(
                        ['id' => $answer['id']]
                    );
                    $answerEntity->setAnswer($this->formHelper->toRealStr($answer['answer'], 2000));

                    foreach ($answer['files'] as $file) {
                        /** @var BaseFiles $fileEntity */
                        $fileEntity = $this->em
                            ->getRepository(BaseFiles::class)->findOneBy(['id' => $file['id']]);
                        if ($fileEntity) {
                            $answerEntity->addAnswerFile($fileEntity);
                        }
                    }

                    break;
            }

            $this->em->persist($answerEntity);
        }

        $this->em->flush();

        return $this->formHelper->getResponse(
            'editTaskAnswer',
            ['id' => $taskBlock->getId()]
        );
    }

    /**
     * @param  Request  $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function getTaskAnswerAllComments(Request $request)
    {
        $data = [];

        if ($answer = $request->request->get('answer')) {
            if (array_key_exists('id', $answer) && array_key_exists('variety', $answer)) {
                $data['comments'] = $this->getTaskAnswerComments($answer['id'], $answer['variety']);
            }
        }

        return $this->formHelper->getResponse(
            'getTaskAnswerAllComments',
            $data
        );
    }

    /**
     * @param $answerId
     * @param $variety
     * @return array
     * @throws \Exception
     */
    public function getTaskAnswerComments($answerId, $variety)
    {
        $data = [];

        $comments = $this->em->getRepository(AnswerComment::class)->getCommentsSortedByDate($answerId, $variety);

        $commentators = [];
        foreach ($comments as $comment) {
            $commentators[] = $comment['commentator'];
        }

        /** @var AnswerTaskType1 $answerEntity */
        $answerEntity = $this->em->getRepository(AnswerTaskType1::class)
            ->findOneBy(['id' => $answerId]);
        $userAnswerAuthId = $answerEntity->getUser()->getAuthUserId();
        $userAuthId = $this->security->getUser()->getAuthUserId();

        /** @var User $validator */
        $commentators = $this->userHelper->getUserInfoArrayByIds($commentators);

        foreach ($comments as $comment) {
            $createdAt = $comment['createdAt'];
            $createdAtMoscow = new \DateTime($createdAt->format("Y-m-d H:i:s"));
            $createdAtMoscow->setTimezone(new \DateTimeZone('Europe/Moscow'));

            $commentatorId = $comment['commentator'];

            $commentatorName = '';
            $commentatorAvatar = '';
            $commentatorEmail = '';
            $isValidator = false;

            foreach ($commentators as $commentator) {
                if ($commentatorId === $commentator['authUserId']) {
                    if ($commentatorId !== $userAnswerAuthId) {
                        $isValidator = true;
                    }
                    if ($userAuthId !== $commentatorId && $comment['state'] === AnswerComment::STATE_NOT_VIEWED) {
                        $con = $this->em->getConnection();
                        $con->update(
                            'answer_comment',
                            [
                                'state' => AnswerComment::STATE_VIEWED,
                            ],
                            [
                                'state' => AnswerComment::STATE_NOT_VIEWED,
                                'id' => $comment['id'],
                            ]
                        );
                    }
                    $commentatorName = sprintf(
                        "%s %s %s",
                        $commentator['name'],
                        $commentator['lastname'],
                        $commentator['patronymic']
                    );
                    $commentatorAvatar = $commentator['avatar'];
                    $commentatorEmail = $commentator['email'];
                    break;
                }
            }

            $files = [];
            $commentFiles = $this->em->getRepository(AnswerComment::class)->getCommentFilesId($comment['id']);

            foreach ($commentFiles as $id) {
                /** @var BaseFiles $file */
                $file = $this->em
                    ->getRepository(BaseFiles::class)->findOneBy(['id' => $id]);
                $files[] = [
                    'id' => $file->getId(),
                    'fileName' => $file->getFileName() ?? "",
                    'url' => $this->fileHelper->getFileUrl($file) ?? "",
                    'mimeType' => $file->getMimeType() ?? "",
                ];
            }

            $data[] = [
                'id' => $comment['id'],
                'comment' => $comment['comment'],
                'commentatorName' => $commentatorName,
                'commentatorAvatar' => $commentatorAvatar,
                'commentatorEmail' => $commentatorEmail,
                'isValidator' => $isValidator,
                'createdAt' => $createdAtMoscow ? $createdAtMoscow->format("Y-m-d H:i:s") : "",
                'commentatorFiles' => $files,
                'state' => $comment['state'],
            ];
        }

        return $data;
    }

    /**
     * @param  Request  $request
     * @param  AnswerComment|null  $answerComment
     * @return JsonResponse
     * @throws DBALException
     */
    public function newOrEditTaskAnswerComment(Request $request, ?AnswerComment $answerComment = null)
    {
        $data = [];

        $answer = $request->request->get('answer');
        $question = $request->request->get('question');

        switch ($question['variety']) {
            case 1:
                /** @var User $user */
                $user = $this->security->getUser();
                $isUserAdmin = $this->em->getRepository(GroupPermission::class)->findByUserIsAdmin($user);

                if (!array_key_exists('id', $answer) && !$answerComment) {
                    $this->formHelper->addError('newTaskAnswerComment', 'Вы еще не оставили ответа на эту задачу');
                    break;
                }

                $answerId = $this->formHelper->toInt($answer['id']);
                $comment = $this->formHelper->toRealStr($answer['comments']['text'], 2000);


                if (!$answerComment) {
                    $con = $this->em->getConnection();
                    $con->update(
                        'answer_comment',
                        [
                            'state' => AnswerComment::STATE_ANSWER_SENT,
                        ],
                        [
                            'variety' => $question['variety'],
                            'answer_id' => $answerId,
                        ]
                    );

                    $answerComment = new AnswerComment();
                    $answerComment->setCommentator($user);
                    $answerComment->setAnswerId($answerId);
                    $answerComment->setVariety($question['variety']);
                    $notificationMessage = "Проверяющий оставил коментарий к вашему ответу на задание";
                } else {
                    if ($answerComment->getState() === AnswerComment::STATE_ANSWER_SENT) {
                        $this->formHelper->addError('editTaskAnswerComment', 'Вам уже ответили на этот комментарий!');
                        break;
                    }
                    $notificationMessage = "Проверяющий изменил комментарий к вашему ответу на задание";
                    foreach ($answerComment->getCommentFiles() as $file) {
                        $answerComment->getCommentFiles()->removeElement($file);
                    }
                }
                $answerComment->setComment($comment);

                if (array_key_exists('commentatorFiles', $answer['comments'])) {
                    foreach ($answer['comments']['commentatorFiles'] as $file) {
                        /** @var BaseFiles $fileEntity */
                        $fileEntity = $this->em
                            ->getRepository(BaseFiles::class)->findOneBy(['id' => $file['id']]);
                        if ($fileEntity) {
                            $answerComment->addCommentFile($fileEntity);
                        }
                    }
                }

                $this->em->persist($answerComment);
                $this->em->flush();

                if ($isUserAdmin) {
                    /** @var AnswerTaskType1 $answerEntity */
                    $answerEntity = $this->em->getRepository(AnswerTaskType1::class)
                        ->findOneBy(['id' => $answerId]);
                    $userAnswer = $answerEntity->getUser();

                    if ($userAnswer && $userAnswer !== $user) {
                        $lesson = $this->em->getRepository(Lesson::class)->findByAnswer($answerId);

                        if ($lesson) {
                            $this->createNotification(
                                'Вам оставили комментарий',
                                $notificationMessage,
                                Notification::ICON_HOMEWORK,
                                $userAnswer,
                                Notification::LINK_TYPE_LESSON,
                                $lesson['id']
                            );
                        }
                    }
                }

                break;
        }

        return $this->formHelper->getResponse(
            'newTaskAnswerComment',
            $data
        );
    }

    public function getTaskByLesson(Lesson $lesson, $lessonPartBlocks = null)
    {
        $data = [];

        // Для каждого курса получает список блоков
        if (is_null($lessonPartBlocks)) {
            $lessonPartBlocks = $this->em->getRepository(LessonPartBlock::class)->findByLessonTaskBlocks($lesson);
        }

        /** @var LessonPartBlock $lessonPartBlock */
        foreach ($lessonPartBlocks as $lessonPartBlock) {
            switch ($lessonPartBlock->getVariety()) { //Вариация блока
                case 1:
                    /** @var BaseBlockTaskWithCheck $taskBlock */
                    $taskBlock = $this->em
                        ->getRepository(BaseBlockTaskWithCheck::class)->findOneBy(
                            ['id' => $lessonPartBlock->getBlockId()]
                        );

                    if ($taskBlock) {
                        $sets = $taskBlock->getTaskSets();

                        /** @var TaskSet $set */
                        foreach ($sets as $set) {
                            switch ($set->getVariety()) { //Вариация таски
                                case 1:
                                    /** @var TaskType1 $task */
                                    $task = $this->em->getRepository(TaskType1::class)->findByIdNotAutoChecked(
                                        $set->getBlockId()
                                    );

                                    $data[] = [
                                        'task' => $task,
                                        'ddl' => $taskBlock->getDdl(),
                                    ];
                                    break;
                            }
                        }
                    }
                    break;
            }
        }

        return $data;
    }

    public function newTaskTestAnswer(Request $request)
    {
        /** @var User $user */
        $user = $this->security->getUser();

        $data = [];

        $questionId = $request->request->get('questionId');
        $answerIds = $request->request->get('answerIds', []);
        if (!$questionId || !$answerIds) {
            $this->formHelper->addError('error', 'Не получен questionId или answerIds');

            return $this->formHelper->getResponse('newTaskTestAnswer');
        }

        /** @var TaskTestQuestion $question */
        $question = $this->em->getRepository(TaskTestQuestion::class)->find($questionId);

        /** @var TaskTestQuestionAnswer[] $answers */
        $answers = $this->em->getRepository(TaskTestQuestionAnswer::class)->findBy(['id' => $answerIds]);
        foreach ($answers as $answer) {
            // проверяем, что чел уже отвечал на этот вопрос
            $userAnswer = $this->em->getRepository(TaskTestQuestionUserAnswer::class)->findOneBy(
                [
                    'question' => $question,
                    'answer' => $answer,
                    'user' => $user,
                ]
            );
            if ($userAnswer) {
                continue;
            }

            // проверяем, что ответ относится к вопросу
            if ($answer->getQuestion() !== $question) {
                continue;
            }

            $userAnswer = new TaskTestQuestionUserAnswer();
            $userAnswer->setUser($user);
            $userAnswer->setQuestion($question);
            $userAnswer->setAnswer($answer);
            $this->em->persist($userAnswer);
            $this->em->flush();
        }

        // считаем сумму баллов за тест
        $data = [
            'points' => $this->em->getRepository(TaskTestQuestionUserAnswer::class)
                ->getSumOfTestUserAnswers($user, $question->getTest()),
        ];

        return $this->formHelper->getResponse('newTaskTestAnswer', $data);
    }

    public function newTaskType5Answer(Request $request): JsonResponse
    {
        /** @var User $user */
        $user = $this->security->getUser();

        // собираем и валидируем
        $taskId = $request->request->get('blockId');
        $answerText = $request->request->get('answer');

        if (!$taskId || !$answerText) {
            $this->formHelper->addError('answer', 'Не получен taskId или answer');

            return $this->formHelper->getResponse('newTaskType5Answer');
        }

        $task = $this->em->getRepository(TaskType5::class)->find($taskId);
        if (!$task) {
            $this->formHelper->addError('blockId', 'Задание не найдено');

            return $this->formHelper->getResponse('newTaskType5Answer');
        }

        /** @var TaskType5 $task */

        // ищем предыдущий ответ
        $answer = $this->em->getRepository(AnswerTaskType5::class)->findOneBy(
            [
                'user' => $user,
                'task' => $task,
            ]
        );

        // проверяем ответ
        $answerType = null;
        $text = trim(str_replace('ё', 'е', mb_strtolower($answerText)));
        if (in_array($text, explode('|', $task->getAdditionalCorrectAnswers()))) {
            $answerType = 'additional';
        }
        if (in_array($text, explode('|', $task->getCorrectAnswers()))) {
            $answerType = 'main';
        }

        $validatingText = 'Попытка хорошая, ответ неправильный';
        $isCorrect = false;
        $points = 0;

        if ($answerType) {
            $isCorrect = true;

            $now = (new \DateTime('now'))->setTimezone(new \DateTimeZone('Europe/Moscow'));

            // забираем дату из БД и притворяемся, что там московское время
            $ddl = new \DateTime(
                $task->getDdl()->format("Y-m-d H:i:s"),
                new \DateTimeZone('Europe/Moscow')
            );

            $prefix = $answerType == 'main' ? '' : 'AddCorAns';

            if ($now->getTimestamp() > $ddl->getTimestamp()) { // не успел
                $validatingText = 'Правильно, но в следующий раз уложись в дедлайн';
                $methodName = sprintf('get%sPointsAfterDdl', $prefix);
            } else {
                $validatingText = 'Отлично, правильный ответ!';
                $methodName = sprintf('get%sPointsBeforeDdl', $prefix);
            }
            $points = $task->$methodName();
        }

        if (!$answer) {
            $answer = new AnswerTaskType5();
            $answer->setUser($user);
            $answer->setTask($task);
        }

        $answer->setPoints($points);
        $answer->setIsCorrect($isCorrect);
        $answer->setValidatingText($validatingText);
        $answer->setAnswerText(
            $this->formHelper->toRealStr($answerText)
        );

        $this->em->persist($answer);
        $this->em->flush();

        return $this->formHelper->getResponse(
            'newTaskType5Answer',
            [
                'isCorrect' => $isCorrect,
                'answerText' => $answerText,
                'validatingText' => $validatingText,
            ]
        );
    }

    /**
     * @param  int  $lessonId
     * @param  int  $state
     */
    protected function markLesson(int $lessonId, int $state)
    {
        /** @var Lesson $lesson */
        $lesson = $this->em->getRepository(Lesson::class)->find($lessonId);
        if (!$lesson) {
            return;
        }

        /** @var User $user */
        $user = $this->security->getUser();

        /** @var UserLesson $userLesson */
        $userLesson = $this->em->getRepository(UserLesson::class)->findOneBy(['lesson' => $lesson, 'user' => $user]);
        if (!$userLesson) {
            $userLesson = new UserLesson();
            $userLesson->setUser($user);
            $userLesson->setLesson($lesson);
        }
        $userLesson->setState($state);
        $this->em->persist($userLesson);
        $this->em->flush();
    }


    private function createNotification(
        $title,
        $description,
        $iconId,
        User $user,
        $linkTypeId = Notification::LINK_TYPE_NO_LINK,
        $linkContent = null,
        $statusId = Notification::STATUS_NEW
    ) {
        if (!$linkContent) {
            $linkTypeId = Notification::LINK_TYPE_NO_LINK;
        }

        $notification = new Notification();
        $notification->setTitle($title);
        $notification->setDescription($description);
        $notification->setStatusId($statusId);
        $notification->setIconId($iconId);
        $notification->setLinkTypeId($linkTypeId);
        $notification->setLinkContent($linkContent);
        $notification->setUser($user);
        $this->em->persist($notification);
        $this->em->flush();

        // отправка уведомления в Firebase
        $this->firebasePushHelper->sendNotificationToFirebase($notification, $user);
    }

}

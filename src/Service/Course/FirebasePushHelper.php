<?php

namespace App\Service\Course;

use App\Entity\Notification;
use App\Entity\User;
use App\Entity\UserFirebaseToken;
use Doctrine\ORM\EntityManagerInterface;
use Kreait\Firebase\Exception\FirebaseException;
use Kreait\Firebase\Exception\Messaging\InvalidMessage;
use Kreait\Firebase\Exception\MessagingException;
use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging;
use Kreait\Firebase\Messaging\CloudMessage;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\KernelInterface;

class FirebasePushHelper
{
    private $em;
    private $messaging;
    private $logger;

    public function __construct(EntityManagerInterface $em, KernelInterface $kernel, LoggerInterface $logger)
    {
        $this->em = $em;
        $factory = (new Factory())->withServiceAccount(
            $kernel->getProjectDir() .
            '/config/toolbox-263317-firebase-adminsdk-bq4ha-27979d3623.json'
        );
        $this->messaging = $factory->createMessaging();
        $this->logger = $logger;
    }

    public function sendNotificationToFirebase(Notification $notification, User $user)
    {
        // Достаём токены всех девайсов юзера
        /** @var UserFirebaseToken[] $userDeviceTokens */
        $userDeviceTokens = $this->em->getRepository(UserFirebaseToken::class)->findBy(['user' => $user]);

        foreach ($userDeviceTokens as $userDeviceToken) {
            // Пишем сообщение
            $message = CloudMessage::withTarget('token', $userDeviceToken->getToken())
                ->withNotification(
                    Messaging\Notification::create(
                        $notification->getTitle(),
                        $notification->getDescription()
                    )
                );
            // Проверяем сообщение
            try {
                $this->messaging->validate($message);
            } catch (InvalidMessage $e) {
                $errors = $e->errors();
                // Достаём код ошибки
                $errorCode = reset(reset($errors)['details'])['errorCode'];
                // Токен устарел
                if ($errorCode === 'UNREGISTERED') {
                    // ищем все такие токены в базе
                    $oldTokens = $this->em->getRepository(UserFirebaseToken::class)->findBy(
                        [
                            'token' => $userDeviceToken->getToken(),
                        ]
                    );
                    foreach ($oldTokens as $oldToken) {
                        $this->em->remove($oldToken);
                        $this->em->flush();
                    }
                    continue;
                } else {
                    $this->logger->critical($e->getMessage());
                }
            } catch (MessagingException $e) {
                $this->logger->critical($e->getMessage());
            } catch (FirebaseException $e) {
                $this->logger->critical($e->getMessage());
            }
            // Если всё ок, отправляем сообщение
            $this->messaging->send($message);
        }
    }

}

<?php

namespace App\Service\Course;

use App\Entity\BaseBlock\BaseFiles;
use App\Entity\Courses\Course;
use App\Entity\Courses\CourseConfiguration;
use App\Entity\Courses\CourseFolder;
use App\Entity\Courses\Lesson;
use App\Entity\Courses\LessonConfiguration;
use App\Entity\User;
use App\Entity\UserGroup;
use App\Service\FormHelper;
use App\Service\UserHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class CourseHelper
{
    private $em;
    private $security;
    private $formHelper;
    private $fileHelper;
    private $userHelper;

    public function __construct(
        EntityManagerInterface $em,
        Security $security,
        FormHelper $formHelper,
        FileHelper $fileHelper,
        UserHelper $userHelper
    ) {
        $this->em = $em;
        $this->security = $security;
        $this->formHelper = $formHelper;
        $this->fileHelper = $fileHelper;
        $this->userHelper = $userHelper;
    }

    public function getCourseFolders(Request $request)
    {
        $data = [];

        // Определяем платформу
        $platformName = $request->query->get('platform');
        $isMobileApp = (in_array($platformName, ['app']));

        // Забираем пользователя
        /** @var User $user */
        $user = $this->security->getUser();

        // Забираем права на папки для того, чтобы понять нужно ли показывать все папки
        $permissions = $this->userHelper->getUserPermissionsByField($user, 'folderModuleAdmin');

        $isAdmin = ($permissions['isAdmin'] && $permissions['folderModuleAdmin']['read'] == 2);
        if ($isAdmin) {
            $courseFolders = $this->em->getRepository(CourseFolder::class)->findAll();
        } else {
            // Забираем папки с курсами, к которым у пользователя есть доступ
            $courseFolders = $this->em->getRepository(CourseFolder::class)->findByUser($user);
        }

        // Распаковываем инфу по курсам
        /** @var CourseFolder $folder */
        foreach ($courseFolders as $folder) {
            if ($folder->isDeleted()) {
                continue;
            }

            $img = [];
            /** @var BaseFiles $imgEntity */
            if ($imgEntity = $folder->getImgFile()) {
                $img = [
                    'id' => $imgEntity->getId(),
                    'url' => $this->fileHelper->getFileUrl($imgEntity),
                    'fileName' => $imgEntity->getFileName(),
                    'mimeType' => $imgEntity->getMimeType(),
                ];
            }

            $coverImg = [];
            /** @var BaseFiles $imgEntity */
            if ($coverImgEntity = $folder->getCoverImgFile()) {
                $coverImg = [
                    'id' => $coverImgEntity->getId(),
                    'url' => $this->fileHelper->getFileUrl($coverImgEntity),
                    'fileName' => $coverImgEntity->getFileName(),
                    'mimeType' => $coverImgEntity->getMimeType(),
                ];
            }

            $countActiveCourse = 0;
            // Забираем права на папки для того, чтобы понять нужно ли показывать все папки
            $permissions = $this->userHelper->getUserPermissionsByField($user, 'courseModuleAdmin');

            /** @var Course[] $courses */
            if ($permissions['isAdmin'] && $permissions['courseModuleAdmin']['read'] == 2) {
                $courses = $this->em->getRepository(Course::class)->findBy(['folder' => $folder]);
            } else {
                // Забираем курсы, к которым у пользователя есть доступ
                $courses = $this->em->getRepository(Course::class)->findByUserAndFolder($user, $folder);
            }
            /** @var Course $course */
            foreach ($courses as $course) {
                if (!$course->isDeleted()) {
                    $countActiveCourse++;
                }
            }
            if ($countActiveCourse != 1) {
                $firstCourseId = "";
            } else {
                $firstCourseId = $courses[0]->getId();
            }
            $data['courseFolders'][] = [
                'id' => $folder->getId(),
                'name' => $folder->getName(),
                'description' => $folder->getDescription(),
                'img' => $img,
                'coverImg' => $coverImg,
                'courseCount' => $countActiveCourse,
                'firstCourseId' => $firstCourseId,
                'permissions' => $this->userHelper->getCourseFolderPermissions($user, $folder),
            ];
        }

        return $this->formHelper->getResponse(
            'getCourseFolders',
            $data
        );
    }

    public function getCoursesInFolder(CourseFolder $courseFolder)
    {
        /** @var User $user */
        $user = $this->security->getUser();

        // Забираем права на папки для того, чтобы понять нужно ли показывать все папки
        $permissions = $this->userHelper->getUserPermissionsByField($user, 'courseModuleAdmin');

        if ($permissions['isAdmin'] && $permissions['courseModuleAdmin']['read'] == 2) {
            $courses = $this->em->getRepository(Course::class)->findBy(['folder' => $courseFolder]);
        } else {
            // Забираем курсы, к которым у пользователя есть доступ
            $courses = $this->em->getRepository(Course::class)->findByUserAndFolder($user, $courseFolder);
        }
        $data['folder'] = [
            'id' => $courseFolder->getId(),
            'name' => $courseFolder->getName(),
            'permissions' => $this->userHelper->getCourseFolderPermissions($user, $courseFolder),
            'groups' => $courseFolder->getUserGroupIds(),
        ];

        // Распаковываем инфу по курсам
        /** @var Course $course */
        foreach ($courses as $course) {
            if ($course->isDeleted()) {
                continue;
            }

            // Получаем права на конкретную папку
            $perm = $this->userHelper->getCoursePermissions($user, $course);

            $img = [];
            $coverImg = [];

            /** @var BaseFiles $imgEntity */
            if ($imgEntity = $course->getImgFile()) {
                $img = [
                    'id' => $imgEntity->getId(),
                    'url' => $this->fileHelper->getFileUrl($imgEntity),
                    'fileName' => $imgEntity->getFileName(),
                ];
            }

            /** @var BaseFiles $coverImgEntity */
            if ($coverImgEntity = $course->getCoverImgFile()) {
                $coverImg = [
                    'id' => $coverImgEntity->getId(),
                    'url' => $this->fileHelper->getFileUrl($coverImgEntity),
                    'fileName' => $coverImgEntity->getFileName(),
                ];
            }


            // костыль для создания конфигурации для курса, если он не был создан до этого
            // TODO: найти и пофиксить места, где CourseConfiguration может не создаваться + удалить этот костыль
            /** @var CourseConfiguration|null $courseConfiguration */
            $courseConfiguration = $course->getCourseConfiguration();
            if (!$course->getCourseConfiguration()) {
                $courseConfiguration = new CourseConfiguration();
                $courseConfiguration->setCourse($course);
                $this->em->persist($courseConfiguration);
                $this->em->flush();
            }

            $data['courses'][] = [
                'id' => $course->getId(),
                'name' => $course->getName(),
                'description' => $course->getDescription(),
                'img' => $img,
                'coverImg' => $coverImg,
                'lessonCount' => $perm['isAdmin'] ?
                    $this->em->getRepository(Lesson::class)->findByCourseCountNotDeletedLessons($course) :
                    $this->em->getRepository(Lesson::class)->findByCourseCountNotDeletedAndNotHiddenLessons(
                        $course,
                        $user
                    ),
                'dateOfStart' => $courseConfiguration->getDateOfStart()
                    ? $courseConfiguration->getDateOfStart()->format("Y-m-d")
                    ?? ""
                    : "",
                'dateOfFinish' => $courseConfiguration->getDateOfFinish()
                    ? $courseConfiguration->getDateOfFinish()->format("Y-m-d")
                    ?? ""
                    : "",
                'permissions' => $perm,
                'groups' => $course->getUserGroupIds(),
            ];
        }

        return $this->formHelper->getResponse(
            'getCoursesInFolder',
            $data
        );
    }

    public function getCourseSelect()
    {
        $folders = $this->em->getRepository(CourseFolder::class)->findBy(['deleted' => 0]);

        $data = [];

        foreach ($folders as $courseFolder) {
            $courseArray = [];
            /** @var Course $course */
            foreach ($courseFolder->getCourses() as $course) {
                if ($course->isDeleted()) {
                    continue;
                }
                $courseArray[] = [
                    'id' => $course->getId(),
                    'name' => $course->getName(),
                ];
            }

            $data['folders'][] = [
                'folder' => [
                    'id' => $courseFolder->getId(),
                    'name' => $courseFolder->getName(),
                ],
                'courseArray' => $courseArray,
            ];
        }

        return $this->formHelper->getResponse(
            'getCourseSelect',
            $data
        );
    }

    /*
     * CourseFolder {name, description, img}
     * {"folder": { "name": "TestFolderByFerenus", "description": "test", "img": ""}}
     */
    public function newOrEditCourseFolder(Request $request, CourseFolder $courseFolder = null)
    {
        $data = [];
        $action = "editCourseFolder";

        if ($folderArray = $request->request->get('folder', null)) {
            if (!$courseFolder) {
                $courseFolder = new CourseFolder();
                $action = "newCourseFolder";
            }

            $courseFolder = $this->setCourseFolderFromRequest($courseFolder, $folderArray);
            $data['id'] = $courseFolder->getId();
        } else {
            $this->formHelper->addError('folder', 'Нет данных папки');
        }

        return $this->formHelper->getResponse(
            $action,
            $data
        );
    }

    public function setCourseFolderFromRequest(CourseFolder $courseFolder, array $courseFolderArray)
    {
        //EXCEPTION Схожий код, но создание объектов разный, можно будет сократить если не будет расширяемости
        foreach ($courseFolderArray as $field => $value) {
            switch ($field) {
                case 'name':
                    $courseFolder->setName($this->formHelper->toRealStr($value));
                    break;

                case 'description':
                    $courseFolder->setDescription($this->formHelper->toRealStr($value));
                    break;

                case 'img':
                    if (!$value) {
                        $courseFolder->setImgFile(null);
                        break;
                    }
                    /** @var BaseFiles $img */
                    $img = $this->em
                        ->getRepository(BaseFiles::class)
                        ->find($value);
                    $courseFolder->setImgFile($img);
                    break;

                case 'coverImg':
                    if (!$value) {
                        $courseFolder->setCoverImgFile(null);
                        break;
                    }

                    /** @var BaseFiles $coverImg */
                    $coverImg = $this->em
                        ->getRepository(BaseFiles::class)
                        ->find($value);
                    $courseFolder->setCoverImgFile($coverImg);
                    break;
            }
        }

        $this->em->persist($courseFolder);
        $this->em->flush();

        return $courseFolder;
    }

    public function deleteCourseFolder(Request $request, CourseFolder $courseFolder)
    {
        $data = [];
        $data['id'] = $courseFolder->getId();

        $courseFolder->setDeleted(true);
        $this->em->persist($courseFolder);

        $this->em->flush();

        return $this->formHelper->getResponse(
            'deleteCourseFolder',
            $data
        );
    }

    public function newOrEditCourseInCoursesFolder(
        Request $request,
        CourseFolder $courseFolder = null,
        Course $course = null
    ) {
        $data = [];
        $action = "EditCourseInCoursesFolder";

        if ($courseArray = $request->request->get('course', null)) {
            if (!$course) {
                $action = "newCourseInCoursesFolder";
                $course = new Course();
                $course->setFolder($courseFolder);

                /** @var User $user */
                $user = $this->security->getUser();
                $checkPermission = $this->userHelper->getCourseFolderPermissions($user, $courseFolder);
                if ($checkPermission['folderModuleAdmin']['create'] == 1) {
                    $groupId = $this->userHelper->getGroupIdPerAdmin($user, $courseFolder);
                    if (!empty($groupId)) {
                        /** @var UserGroup $group */
                        $group = $this->em->getRepository(UserGroup::class)->findOneBy(['id' => $groupId]);
                        $group->addCourse($course);
                        $this->em->persist($group);
                    }
                }
            }
            $course = $this->setCourseInCoursesFolderFromRequest($course, $courseArray);

            if ($action == "newCourseInCoursesFolder") {
                $courseConfiguration = new CourseConfiguration();
                $courseConfiguration->setCourse($course);
            } else {
                $courseConfiguration = $course->getCourseConfiguration();
            }

            $courseConfiguration = $this->setCourseConfigurationFromRequest($courseConfiguration, $courseArray);

            $data['id'] = $course->getId();
        } else {
            $this->formHelper->addError('course', 'Нет данных курса');
        }

        return $this->formHelper->getResponse(
            $action,
            $data
        );
    }

    public function setCourseInCoursesFolderFromRequest(Course $course, array $courseArray)
    {
        //EXCEPTION Схожий код, но создание объектов разный, можно будет сократить если не будет расширяемости
        foreach ($courseArray as $field => $value) {
            switch ($field) {
                case 'name':
                    $course->setName($this->formHelper->toRealStr($value));
                    break;

                case 'description':
                    $course->setDescription($this->formHelper->toRealStr($value));
                    break;

                case 'img':
                    if (!$value) {
                        $course->setImgFile(null);
                        break;
                    }

                    /** @var BaseFiles $img */
                    $img = $this->em
                        ->getRepository(BaseFiles::class)
                        ->find($value);
                    $course->setImgFile($img);
                    break;

                case 'coverImg':
                    if (!$value) {
                        $course->setCoverImgFile(null);
                        break;
                    }

                    /** @var BaseFiles $coverImg */
                    $coverImg = $this->em
                        ->getRepository(BaseFiles::class)
                        ->find($value);
                    $course->setCoverImgFile($coverImg);
                    break;
                case 'groups':
                    $userGroups = $this->em->getRepository(UserGroup::class)->findBy(['id' => $value]);
                    $oldUserGroups = $course->getUserGroups()->toArray();
                    $course->clearUserGroups();
                    $course->addUserGroups($userGroups);

                    $addedUserGroups = array_diff($userGroups, $oldUserGroups);
                    $deletedUserGroups = array_diff($oldUserGroups, $userGroups);
                    foreach ($course->getLessons() as $lesson) {
                        $lesson->addUserGroups($addedUserGroups);
                        $lesson->removeUserGroups($deletedUserGroups);
                    }
                    break;
            }
        }

        $this->em->persist($course);
        $this->em->flush();

        return $course;
    }

    public function setCourseConfigurationFromRequest(
        CourseConfiguration $courseConfiguration,
        array $courseConfigurationArray
    ) {
        foreach ($courseConfigurationArray as $field => $value) {
            switch ($field) {
                case 'dateOfStart':
                    if ($value) {
                        $courseConfiguration->setDateOfStart($this->formHelper->toDate($value));
                    } else {
                        $courseConfiguration->setDateOfStart(null);
                    }
                    break;
                case 'dateOfFinish':
                    if ($value) {
                        $courseConfiguration->setDateOfFinish($this->formHelper->toDate($value));
                    } else {
                        $courseConfiguration->setDateOfFinish(null);
                    }
                    break;
            }
        }

        $this->em->persist($courseConfiguration);
        $this->em->flush();

        return $courseConfiguration;
    }


    public function deleteCourseInCourseFolder(Course $course)
    {
        $data = [];
        $data['id'] = $course->getId();

        $course->setDeleted(true);
        $this->em->persist($course);

        $this->em->flush();

        return $this->formHelper->getResponse(
            'deleteCourseInCourseFolder',
            $data
        );
    }

    public function duplicateCourse(Request $request, Course $course, LessonHelper $lessonHelper)
    {
        $data = [];
        $action = "duplicateCourseInCoursesFolder";

        if ($courseArray = $request->request->get('course', null)) {
            $courseNew = new Course();
            $courseNew->setFolder($course->getFolder());

            $courseNew = $this->setCourseInCoursesFolderFromRequest($courseNew, $courseArray);

            $courseConfiguration = new CourseConfiguration();
            $courseConfiguration->setCourse($courseNew);

            $courseConfiguration = $this->setCourseConfigurationFromRequest($courseConfiguration, $courseArray);

            if ($courseNew->getId()) {
                $lessons = $course->getLessons();

                /** @var Lesson $lesson */
                foreach ($lessons as $lesson) {
                    //если урок уже записан как удалённый то не создаём его
                    if ($lesson->isDeleted()) {
                        continue;
                    }
                    // Новый урок
                    $lessonEntity = new Lesson();

                    $lessonEntity->setCourse($courseNew);

                    $lessonEntity->setName($lesson->getName());
                    $lessonEntity->setPosition($lesson->getPosition());
                    $lessonEntity->setDescription($lesson->getDescription());
                    $lessonEntity->setImgFile($lesson->getImgFile());
                    $lessonEntity->setImgText($lesson->getImgText());
                    $lessonEntity->setIsStopLesson($lesson->getIsStopLesson());
                    $lessonEntity->setIsImportant($lesson->getIsImportant());
                    $this->em->persist($lessonEntity);
                    $this->em->flush();

                    if ($lessonEntity->getId()) {
                        $lessonConfigurationEntity = new LessonConfiguration();
                        $lessonConfigurationEntity->setLesson($lessonEntity);
                        $lessonConfigurationEntity->setDateOfStart($lesson->getLessonConfiguration()->getDateOfStart());
                        $lessonConfigurationEntity->setIsHidden($lesson->getLessonConfiguration()->getIsHidden());
                        $this->em->persist($lessonConfigurationEntity);

                        // ToDo: Добавить логику спикеров
                        foreach ($lesson->getSpeakers() as $speaker) {
                            /** @var User|null $speakerEntity */
                            $speakerEntity = $this->em
                                ->getRepository(User::class)->find($speaker->getId());

                            if (
                                $speakerEntity &&
                                !$this->em->getRepository(Lesson::class)
                                    ->findByLessonIsSpeakerExists($lessonEntity, $speakerEntity)
                            ) {
                                $lessonEntity->addSpeaker($speakerEntity);
                            }

                            $this->em->persist($lessonEntity);
                        }
                    }
                    $this->em->flush();

                    // Дублировать все блоки

                    // Получаем части
                    $lessonHelper->duplicateLessonPart($lesson, $lessonEntity);

                    $this->em->flush();
                }

                $data['id'] = $courseNew->getId();
            }
        } else {
            $this->formHelper->addError('course', 'Нет данных курса');
        }

        return $this->formHelper->getResponse(
            $action,
            $data
        );
    }

    /*
     * CourseFolder {name, description, img}
     */
    public function duplicateCourseFolder(Request $request, CourseFolder $courseFolder, LessonHelper $lessonHelper)
    {
        $data = [];
        $action = "duplicateCourseFolder";

        if ($folderArray = $request->request->get('folder', null)) {
            $newCourseFolder = new CourseFolder();

            $newCourseFolder = $this->setCourseFolderFromRequest($newCourseFolder, $folderArray);

            /** @var Course $course */
            foreach ($courseFolder->getCourses() as $course) {
                $request = new Request();
                $reqArray = [];
                $reqArray['course']['name'] = $course->getName();
                $reqArray['course']['description'] = $course->getDescription();

                $reqArray['course']['img'] = "";
                if ($imgFile = $course->getImgFile()) {
                    $reqArray['course']['img'] = $imgFile->getId();
                }

                $reqArray['course']['coverImg'] = "";
                if ($coverImgFile = $course->getCoverImgFile()) {
                    $reqArray['course']['coverImg'] = $coverImgFile->getId();
                }

                if (!empty($course->getCourseConfiguration())) {
                    if (!empty($course->getCourseConfiguration()->getDateOfStart())) {
                        $reqArray['course']['dateOfStart'] = $course->getCourseConfiguration()->getDateOfStart(
                        )->format("Y-m-d");
                    } else {
                        $reqArray['course']['dateOfStart'] = "";
                    }
                    if (!empty($course->getCourseConfiguration()->getDateOfFinish())) {
                        $reqArray['course']['dateOfFinish'] = $course->getCourseConfiguration()->getDateOfFinish(
                        )->format("Y-m-d");
                    } else {
                        $reqArray['course']['dateOfFinish'] = "";
                    }
                } else {
                    $reqArray['course']['dateOfStart'] = "";
                    $reqArray['course']['dateOfFinish'] = "";
                }

                $request->request->add($reqArray);
                $oldCourseFolder = $course->getFolder();
                $course->setFolder($newCourseFolder);

                $this->duplicateCourse($request, $course, $lessonHelper);
                $course->setFolder($oldCourseFolder);
                $this->em->flush();
            }

            $data['id'] = $newCourseFolder->getId();
        } else {
            $this->formHelper->addError('folder', 'Нет данных папки');
        }

        return $this->formHelper->getResponse(
            $action,
            $data
        );
    }
}

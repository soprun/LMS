<?php

namespace App\Service;

use App\Entity\BaseBlock\BaseBlockTaskWithCheck;
use App\Entity\Courses\Course;
use App\Entity\Courses\LessonPartBlock;
use App\Entity\Task\AnswerTaskType1;
use App\Entity\Task\AnswerTaskType5;
use App\Entity\Task\TaskTestQuestionUserAnswer;
use App\Entity\User;
use App\Entity\UserCourseTariff;
use App\Entity\UserGroup;
use App\Entity\UserGroupUser;
use App\Entity\UserTariff;
use App\Service\Course\AnswerCheckHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class UserAdminHelper
{
    private $em;
    private $security;
    private $formHelper;
    private $userHelper;
    private $authServerHelper;
    private $answerCheckHelper;

    public function __construct(
        EntityManagerInterface $em,
        Security $security,
        FormHelper $formHelper,
        UserHelper $userHelper,
        AuthServerHelper $authServerHelper,
        AnswerCheckHelper $answerCheckHelper
    ) {
        $this->em = $em;
        $this->security = $security;
        $this->formHelper = $formHelper;
        $this->userHelper = $userHelper;
        $this->authServerHelper = $authServerHelper;
        $this->answerCheckHelper = $answerCheckHelper;
    }

    /**
     * Получение всех пользователей LMS
     * @param  Request  $request
     * @return JsonResponse
     */
    public function getUserList(Request $request): JsonResponse
    {
        // TODO: костыль, пока не прикрутили эластик
        ini_set('memory_limit', '1G');
        // TODO: проверка на админа

        $search = $request->query->get('name');
        $page = $request->query->get('page', 1);

        // составляем массив, чтобы потом подгрузить группу каждого пользователя
        $userGroupIds = [];
        foreach ($this->em->getRepository(User::class)->getAllUsersWithGroupsArray() as $user) {
            $userGroupIds[$user['authUserId']][] = $user['groupId'];
        }

        $authUserIds = array_keys($userGroupIds);
        $usersArray = $this->userHelper->getUserInfoArrayByIds($authUserIds, $search, $page);

        // по каждому юзеру добавляем группы, в которых он состоит
        foreach ($usersArray as $key => $value) {
            $usersArray[$key]['groupIds'] = (array_key_exists(
                $value['authUserId'],
                $userGroupIds
            )) ? $userGroupIds[$value['authUserId']] : [];
        }

        // берем id и названия групп для фронта
        $groupsArray = array_map(
            function (UserGroup $userGroup) {
                return [
                    'id' => $userGroup->getId(),
                    'name' => $userGroup->getName(),
                ];
            },
            $this->em->getRepository(UserGroup::class)->findAll()
        );

        return $this->formHelper->getResponse(
            'getUserList',
            [
                'users' => $usersArray,
                'groups' => $groupsArray,
            ]
        );
    }

    /**
     * Получение пользователя LMS и их овтетов
     * @param  Request  $request
     * @return JsonResponse
     */
    public function getUserAndAnswers(Request $request): JsonResponse
    {
        // TODO: проверка на админа

        $search = $request->query->get('name');
        $page = $request->query->get('page');
        $courseId = $request->query->get('courseId', null);

        // составляем массив, чтобы потом подгрузить группу каждого пользователя
        $userGroupIds = [];
        foreach ($this->em->getRepository(User::class)->getAllUsersWithGroupsArray() as $user) {
            $userGroupIds[$user['authUserId']][] = $user['groupId'];
        }

        $authUserIds = array_keys($userGroupIds);
        $usersArray = $this->userHelper->getUserInfoArrayByIds($authUserIds, $search, $page);

        // по каждому юзеру добавляем группы, в которых он состоит
        foreach ($usersArray as $key => $value) {
            $usersArray[$key]['groupIds'] = (array_key_exists(
                $value['authUserId'],
                $userGroupIds
            )) ? $userGroupIds[$value['authUserId']] : [];
        }

        /** @var User $user */
        $user = $this->security->getUser();

        $data = [];
        $dataTask = [];
        $groupIds = $usersArray[0]['groupIds'];

        foreach ($groupIds as $groupId) {
            if (!$groupId) {
                continue;
            }
            /** @var UserGroup $userGroup */
            $userGroup = $this->em->getRepository(UserGroup::class)->find($groupId);

            foreach ($userGroup->getCourses() as $course) {
                if ($courseId) {
                    if ($courseId != $course->getId()) {
                        continue;
                    }
                }

                $dataCourses = [];
                $taskBlocks = [];
                $folder = $course->getFolder();

                // Для каждого курса получает список блоков
                $lessonPartBlocks = $this->em->getRepository(LessonPartBlock::class)->findByCourseTaskBlocks($course);

                foreach ($lessonPartBlocks as $lessonPartBlock) {
                    switch ($lessonPartBlock['variety']) { //Вариация блока
                        case 1:
                            /** @var BaseBlockTaskWithCheck $taskBlock */
                            $taskBlock = $this->em
                                ->getRepository(BaseBlockTaskWithCheck::class)->findOneBy(
                                    ['id' => $lessonPartBlock['blockId']]
                                );

                            $userFromAnswer = $this->em->getRepository(User::class)
                                ->findOneBy(['id' => $usersArray[0]['id']]);
                            if ($userFromAnswer) {
                                $answerTaskBlock = $this->answerCheckHelper->parseTaskBlock(
                                    $taskBlock,
                                    $userFromAnswer
                                );

                                if ($answerTaskBlock['questionArray'][0]['answer'] != new \stdClass()) {
                                    $dataTask[] = $this->answerCheckHelper->parseTaskBlock($taskBlock, $userFromAnswer);
                                    $taskBlocks[] = [
                                        'id' => $taskBlock->getId(),
                                    ];
                                }
                            }
                            break;
                    }
                }

                $dataCourses[] = [
                    'course' => [
                        'id' => $course->getId(),
                        'name' => $course->getName(),
                    ],
                    'taskId' => $taskBlocks,
                ];

                $data[] = [
                    'folder' => [
                        'id' => $folder->getId(),
                        'name' => $folder->getName(),
                    ],
                    'courses' => $dataCourses,
                ];
            }
        }

        return $this->formHelper->getResponse(
            'getUserList',
            [
                'tariffs' => $this->answerCheckHelper->getTariffs(),
                'dataFolder' => $data,
                'taskBlocks' => $dataTask,
            ]
        );
    }

    /**
     * Получение пользователей и их баллов для выгрузки
     * @param  Request  $request
     * @return JsonResponse
     */
    public function getUserPoints(Request $request): JsonResponse
    {
        // TODO: проверка на админа

        $startTime = microtime(true);

        $parameters = [
            'groupIds' => $request->query->get('groupIds', ''),
            'date1' => $request->query->get('date1', date("Y-01-01")),
            'date2' => $request->query->get('date2', date("Y-12-31")),
        ];
        $users = [];

        // проходимся по каждой группе
        foreach (
            $this->em->getRepository(UserGroup::class)->findBy(
                ['id' => explode(',', $parameters['groupIds'])]
            ) as $group
        ) {
            /** @var UserGroup $group */

            // собираем айди юзеров группы
            $authUserIds = $group->getUserRelations()->map(
                function (UserGroupUser $userRelation) {
                    return $userRelation->getUser()->getAuthUserId();
                }
            )->toArray();

            // берем инфу с Аутх
            $usersArray = $this->userHelper->getUserInfoArrayByIds($authUserIds);

            // берем баллы
            $lmsUserIds = array_map(
                function (array $user) {
                    return $user['id'];
                },
                $usersArray
            );
            $points = $this->em->getRepository(AnswerTaskType1::class)->getUserPointsSumArray($lmsUserIds, $parameters);
            foreach ($points as $point) {
                $userArrayKey = array_search($point['userId'], array_column($usersArray, 'id'));
                $usersArray[$userArrayKey]['points'] = $point['sum'];
            }

            $points = $this->em->getRepository(AnswerTaskType5::class)->getUserPointsSumArray($lmsUserIds, $parameters);
            foreach ($points as $point) {
                $userArrayKey = array_search($point['userId'], array_column($usersArray, 'id'));
                if (array_key_exists('points', $usersArray[$userArrayKey])) {
                    $usersArray[$userArrayKey]['points'] += $point['sum'];
                } else {
                    $usersArray[$userArrayKey]['points'] = $point['sum'];
                }
            }

            $points = $this->em->getRepository(TaskTestQuestionUserAnswer::class)->getUserPointsSumArray(
                $lmsUserIds,
                $parameters
            );
            foreach ($points as $point) {
                $userArrayKey = array_search($point['userId'], array_column($usersArray, 'id'));
                if (array_key_exists('points', $usersArray[$userArrayKey])) {
                    $usersArray[$userArrayKey]['points'] += $point['sum'];
                } else {
                    $usersArray[$userArrayKey]['points'] = $point['sum'];
                }
            }

            // добавляем каждого пользователя
            foreach ($usersArray as $user) {
                $users[] = [
                    'groupName' => $group->getName(),
                    'fullName' => $user['name'] . ' ' . $user['lastname'],
                    'email' => $user['email'],
                    'city' => $user['city'],
                    'points' => array_key_exists('points', $user) ? (int)$user['points'] : 0,
                ];
            }
        }
        $points = array_column($users, 'points');

        // Сортируем данные по points по убыванию
        // Добавляем $data в качестве последнего параметра, для сортировки по общему ключу
        array_multisort($points, SORT_DESC, $users);

        return $this->formHelper->getResponse(
            'getUserPoints',
            [
                'users' => $users,
                'timeSpent' => round(microtime(true) - $startTime, 2),
                'parameters' => $parameters,
            ]
        );
    }

    public function getAnalyticsAnswersValidators(Request $request): JsonResponse
    {
        $data = [];

        $parameters = [
            'course' => $request->query->get('course', null),
            'date1' => $request->query->get('date_start', date("Y-01-01")),
            'date2' => $request->query->get('date_end', date("Y-12-31")),
        ];

        $userParameters = [
            'search' => $request->query->get('name'),
        ];

        $answers = $this->em->getRepository(AnswerTaskType1::class)->getTaskOrderByValidators($parameters);

        if (empty($answers)) {
            return $this->formHelper->getResponse('getAnalyticsAnswersValidators', $data);
        }

        $analyticsUser = [];
        $studentAuthUserIds = [];
        $usersData = [];
        $analyticsUserCountValidate = [];
        $analyticsUserDateStart = [];
        $analyticsUserDateEnd = [];
        $analyticsUserData = [];
        $analyticsCourseData = [];
        $countedAnswerIds = [];

        foreach ($answers as $answer) {
            // TODO: cделать нормальную группировку
            if (array_key_exists($answer['answerId'], $countedAnswerIds)) {
                continue;
            }
            $countedAnswerIds[$answer['answerId']] = null;

            // берем айди проверяющего
            $validatorAuthUserId = $answer['validatorAuthUserId'];
            // добавляем authUserId для получения инфы по нему
            $analyticsUser[] = $validatorAuthUserId;

            // берем authUserId ученика
            $studentAuthUserId = $answer['studentAuthUserId'];
            if (!array_key_exists($studentAuthUserId, $studentAuthUserIds)) {
                $studentAuthUserIds[$studentAuthUserId] = null;
            }

            if (array_key_exists($validatorAuthUserId, $analyticsUserCountValidate)) {
                $analyticsUserCountValidate[$validatorAuthUserId]++;
            } else {
                $analyticsUserCountValidate[$validatorAuthUserId] = 1;
            }
            $dateToCheck = $answer['tookToCheck']->format('Y-m-d');
            $analyticsUserData[$validatorAuthUserId] = $studentAuthUserId;
            $analyticsUserDateStart[$validatorAuthUserId] = $dateToCheck;
            $analyticsUserDateEnd[$validatorAuthUserId] = $dateToCheck;

            $analyticsCourseData[$validatorAuthUserId][] = [
                'authUserId' => $studentAuthUserId,
                'dateToCheck' => $dateToCheck,
                'courseId' => $answer['courseId'],
                'courseName' => $answer['courseName'],
                'groupId' => $answer['groupId'],
                'groupName' => $answer['groupName'],
                'lessonId' => $answer['lessonId'],
                'lessonName' => $answer['lessonName'],
                'taskBlockId' => $answer['taskBlockId'],
                'taskBlockVariety' => $answer['taskBlockVariety'],
            ];
        }

        // берем инфу о проверяющих
        $analyticsUsers = $this->userHelper->getUserInfoArrayByIds($analyticsUser, $userParameters['search']);

        // берем инфу об учениках
        $users = $this->userHelper->getUserInfoArrayByIds(array_keys($studentAuthUserIds));
        $entityUsers = $this->em->createQueryBuilder()
            ->from(User::class, 'u')
            ->select(['u', 'ur', 'ug'])
            ->innerJoin('u.groupRelations', 'ur')
            ->innerJoin('ur.userGroup', 'ug')
            ->andWhere('u.authUserId IN(:authUserIds)')
            ->setParameter('authUserIds', array_keys($studentAuthUserIds))
            ->getQuery()
            ->getResult();

        foreach ($entityUsers as $entityUser) {
            /** @var User $entityUser */
            $userArrayKey = array_search($entityUser->getId(), array_column($users, 'id'));
            $user = $users[$userArrayKey];

            $user['tariffId'] = $this->answerCheckHelper->parseUserFromAnswer($entityUser, true)['tariffId'];
            $usersData[] = $user;
        }

        foreach ($analyticsUsers as $analyticsUser) {
            if ($analyticsUser['id'] === null) {
                continue;
            }
            $data['validatorsList'][] = [
                'user' => $analyticsUser['id'], // lmsUserId проверяющего
                'count' => $analyticsUserCountValidate[$analyticsUser['authUserId']],
                'dateStart' => $analyticsUserDateStart[$analyticsUser['authUserId']],
                'dateEnd' => $analyticsUserDateEnd[$analyticsUser['authUserId']],
                'analyticsCourseData' => $analyticsCourseData[$analyticsUser['authUserId']],
                'info' => $analyticsUser,
            ];
        }
        $data['users'] = $usersData;
        $data['tariffs'] = $this->answerCheckHelper->getTariffs();

        $arrayColumn = array_column($data['validatorsList'], 'count');
        array_multisort($arrayColumn, SORT_DESC, $data['validatorsList']);

        return $this->formHelper->getResponse('getAnalyticsAnswersValidators', $data);
    }

    /**
     * Регистрация AuthUser
     * @param  Request  $request
     * @return JsonResponse
     */
    public function registerAuthUser(Request $request): JsonResponse
    {
        // TODO: проверка прав

        $userArray = $request->request->get('user');
        $courseName = $request->request->get('courseName');

        $data = [];
        if ($userArray && $courseName) {
            [$authUser, $errors] = $this->authServerHelper->registerAuthUser($userArray, true, $courseName);

            foreach ($errors as $error) {
                $this->formHelper->addError($error['field'], $error['message']);
            }
            if ($this->formHelper->isValid()) {
                $data = (array)$authUser;
            }
        } else {
            $this->formHelper->addError('user', 'Не получен объект пользователя или тип письма');
        }

        return $this->formHelper->getResponse('registerAuthUser', $data);
    }

    /**
     * Получение персональной ссылки на сброс пароля админом для пользователя
     * @param  Request  $request
     * @return JsonResponse
     */
    public function getResetPasswordLink(Request $request): JsonResponse
    {
        // TODO: проверка прав

        /** @var User $user */
        $user = $this->security->getUser();

        $data = [];
        if ($authUserId = $request->request->get('authUserId')) {
            [$link, $errors] = $this->authServerHelper->getResetPasswordLink($user->getAuthUserId(), $authUserId);
            foreach ($errors as $error) {
                $this->formHelper->addError($error['field'], $error['message']);
            }
            if ($this->formHelper->isValid()) {
                $data['link'] = $link->link;
            }
        } else {
            $this->formHelper->addError('authUserId', 'ID пользователя не получен');
        }

        return $this->formHelper->getResponse('registerAuthUser', $data);
    }

    /**
     * Edit user on authServer
     * @param  Request  $request
     * @return JsonResponse
     */
    public function editUserByServer(Request $request): JsonResponse
    {
        $data = [];
        if ($user = $request->request->get('user')) {
            [$dataResponse, $errors] = $this->authServerHelper->editUserByServer($user);
            foreach ($errors as $error) {
                $this->formHelper->addError($error['field'], $error['message']);
            }
            if ($this->formHelper->isValid()) {
                $data['user'] = $dataResponse;
            }
        } else {
            $this->formHelper->addError('authUserId', 'ID пользователя не получен');
        }

        return $this->formHelper->getResponse('editUserByServer', $data);
    }

    public function getCoursesForEmail(): JsonResponse
    {
        $data = [
            'courses' => [
                [
                    'name' => 'Письмо без курса',
                    'value' => 'withoutCourse',
                ],
                [
                    'name' => 'Скорость',
                    'value' => 'speed',
                ],
                [
                    'name' => 'Сотка',
                    'value' => 'hundred',
                ],
                [
                    'name' => 'Предоплата Сотки',
                    'value' => 'hundredPrepay',
                ],
                [
                    'name' => 'Предоплата Скорости',
                    'value' => 'speedPrepay',
                ],
                [
                    'name' => 'Марафон инвестиций',
                    'value' => 'mi',
                ],
                [
                    'name' => 'Миллион с Аязом',
                    'value' => 'msa',
                ],
            ],
        ];

        return $this->formHelper->getResponse('getCoursesForEmail', $data);
    }

    /**
     * @param  Request  $request
     * @return JsonResponse
     */
    public function getUserCourseTariff(Request $request)
    {
        $action = 'getUserCourseTariff';
        $data = [];

        if ($userId = $request->get('userId')) {
            /** @var User $user */
            $user = $this->em->getRepository(User::class)->findOneBy(['id' => $userId]);
            $userCourseTariffs = $user->getUserCourseTariffs();
            /** @var UserCourseTariff $userCourseTariff */
            foreach ($userCourseTariffs as $userCourseTariff) {
                $usersAuthArray = [];
                $managerAuthId = null;
                $trackerAuthId = null;
                if ($manager = $userCourseTariff->getManager()) {
                    $managerAuthId = $manager->getAuthUserId();
                    $usersAuthArray[] = $managerAuthId;
                }
                if ($tracker = $userCourseTariff->getTracker()) {
                    $trackerAuthId = $tracker->getAuthUserId();
                    $usersAuthArray[] = $trackerAuthId;
                }

                $usersArray = $this->authServerHelper->getUsersInfo($usersAuthArray);

                $managerAuth = [];
                $trackerAuth = [];
                foreach ($usersArray as $users) {
                    if ($managerAuthId === $users->id) {
                        $managerAuth = $this->getDataReportAuthUser($users);
                    } elseif ($trackerAuthId === $users->id) {
                        $trackerAuth = $this->getDataReportAuthUser($users);
                    }
                }

                $data['userCourseTariffs'][] = [
                    'id' => $userCourseTariff->getId(),
                    'tariffId' => $userCourseTariff->getTariff()->getId(),
                    'courseId' => $userCourseTariff->getCourse()->getId(),
                    'faculty' => $userCourseTariff->getFaculty() ? $userCourseTariff->getFaculty() : '',
                    'manager' => $managerAuth,
                    'tracker' => $trackerAuth,
                ];
            }
        }

        return $this->formHelper->getResponse($action, $data);
    }

    /**
     * @param  Request  $request
     * @return JsonResponse
     */
    public function getCourseTariffs(Request $request)
    {
        $action = 'getCourseTariffs';
        $data = [];

        if ($courseId = $request->get('courseId')) {
            $userTariffs = $this->em->getRepository(UserTariff::class)->findByCourseId($courseId);
            /** @var UserTariff $userTariff */
            foreach ($userTariffs as $userTariff) {
                $data['tariffs'][] = [
                    'id' => $userTariff->getId(),
                    'name' => $userTariff->getName(),
                ];
            }
        }

        return $this->formHelper->getResponse($action, $data);
    }

    /**
     * @param  Request  $request
     * @return JsonResponse
     */
    public function addOrEditUserCourseTariff(Request $request)
    {
        $action = 'userCourseTariff';
        $data = [];
        $userCourseTariffs = $request->request->get('userCourseTariffs', []);
        /** @var UserCourseTariff $userCourseTariffEntity */
        $userCourseTariffEntity = null;

        foreach ($userCourseTariffs as $userCourseTariff) {
            if (array_key_exists('id', $userCourseTariff)) {
                if ($id = $userCourseTariff['id']) {
                    if (
                        !$userCourseTariffEntity = $this->em->getRepository(UserCourseTariff::class)
                            ->findOneBy(['id' => $id])
                    ) {
                        $this->formHelper->addError('userCourseTariff', 'Entity not found!');
                    }
                }
            } else {
                if (array_key_exists('userId', $userCourseTariff)) {
                    $userCourseTariffEntity = new UserCourseTariff();
                    if ($userId = $userCourseTariff['userId']) {
                        if ($user = $this->em->getRepository(User::class)->findOneBy(['id' => $userId])) {
                            $userCourseTariffEntity->setUser($user);
                        } else {
                            $this->formHelper->addError('userCourseTariff', 'User not found!');
                        }
                    }
                } else {
                    $this->formHelper->addError('userCourseTariff', 'User not set in request!');
                }
            }

            if ($this->formHelper->isValid()) {
                foreach ($userCourseTariff as $key => $value) {
                    switch ($key) {
                        case 'courseId':
                            /** @var Course $course */
                            if ($course = $this->em->getRepository(Course::class)->findOneBy(['id' => $value])) {
                                $userCourseTariffEntity->setCourse($course);
                            }
                            break;
                        case 'tariffId':
                            /** @var UserTariff $tariff */
                            if ($tariff = $this->em->getRepository(UserTariff::class)->findOneBy(['id' => $value])) {
                                $userCourseTariffEntity->setTariff($tariff);
                            }
                            break;
                        case 'managerId':
                            /** @var User $manager */
                            if ($manager = $this->em->getRepository(User::class)->findOneBy(['id' => $value])) {
                                $userCourseTariffEntity->setManager($manager);
                            }
                            break;
                        case 'trackerId':
                            /** @var User $tracker */
                            if ($tracker = $this->em->getRepository(User::class)->findOneBy(['id' => $value])) {
                                $userCourseTariffEntity->setTracker($tracker);
                            }
                            break;
                        case 'faculty':
                            if ($faculty = $this->formHelper->toStrWithoutGaps($value)) {
                                $userCourseTariffEntity->setFaculty($faculty);
                            }
                            break;
                    }
                }

                $this->em->persist($userCourseTariffEntity);
                $this->em->flush();
                $data[] = [
                    'id' => $userCourseTariffEntity->getId(),
                ];
            }
        }

        return $this->formHelper->getResponse($action, $data);
    }


    /**
     * @param  object  $authUser
     * @return array
     */
    private function getDataReportAuthUser(object $authUser)
    {
        return [
            'id' => property_exists($authUser, 'lmsUserId') ? $authUser->lmsUserId : "",
            'authUserId' => property_exists($authUser, 'id') ? $authUser->id : "",
            'name' => property_exists($authUser, 'name') ? $authUser->name : "",
            'lastname' => property_exists($authUser, 'lastname') ? $authUser->lastname : "",
            'avatar' => property_exists($authUser, 'avatar') ? $authUser->avatar : "",
            'email' => property_exists($authUser, 'email') ? $authUser->email : "",
            'vkLink' => property_exists($authUser, 'vkLink') ? $authUser->vkLink : "",
            'fbLink' => property_exists($authUser, 'fbLink') ? $authUser->fbLink : "",
            'instaLink' => property_exists($authUser, 'instaLink') ? $authUser->instaLink : "",
            'siteLink' => property_exists($authUser, 'siteLink') ? $authUser->siteLink : "",
            'phone' => property_exists($authUser, 'phone') ? $authUser->phone : "",
        ];
    }

}

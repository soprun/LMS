<?php

namespace App\Service;

use App\Entity\BaseBlock\BaseBlock1;
use App\Entity\BaseBlock\BaseBlock2;
use App\Entity\BaseBlock\BaseBlock3;
use App\Entity\BaseBlock\BaseBlock4;
use App\Entity\BaseBlock\BaseBlock5;
use App\Entity\BaseBlock\BaseBlock6;
use App\Entity\BaseBlock\BaseBlock7;
use App\Entity\BaseBlock\BaseBlockNps;
use App\Entity\BaseBlock\BaseBlockTaskWithCheck;
use App\Entity\Courses\LessonPartBlock;
use App\Entity\Interfaces\LessonContentBlockInterface;
use App\Entity\Task\TaskTest;
use App\Entity\Task\TaskType1;
use App\Entity\Task\TaskType5;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;

class LessonContentService
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @param EntityManagerInterface $em
     * @param LoggerInterface $logger
     */
    public function __construct(EntityManagerInterface $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
    }

    /**
     * @param int $type
     * @param int $variety
     * @param int $id
     *
     * @return LessonContentBlockInterface|null
     */
    public function findBlock(int $type, int $variety, int $id): ?LessonContentBlockInterface
    {
        return $this->em->getRepository($this->getBlockType($type, $variety))->find($id);
    }

    /**
     * @param string $class
     * @param int $id
     *
     * @return LessonContentBlockInterface|null
     */
    public function findBlockByClass(string $class, int $id): ?LessonContentBlockInterface
    {
        return $this->em->getRepository($class)->find($id);
    }

    /**
     * @param int $type
     * @param int $variety
     *
     * @return LessonContentBlockInterface
     */
    public function createBlock(int $type, int $variety): LessonContentBlockInterface
    {
        $blockName = $this->getBlockType($type, $variety);

        return new $blockName();
    }

    /**
     * @param int $type
     * @param int $variety
     *
     * @return string
     *
     * @throws InvalidArgumentException
     */
    public function getBlockType(int $type, int $variety): ?string
    {
        switch ($type) {
            case LessonPartBlock::CONTENT_TYPE:
                return $this->getContentBlockTypeByVariety($variety);
            case LessonPartBlock::TASK_WITH_CHECK_TYPE:
                return $this->getTaskBlockTypeByVariety($variety);
            case LessonPartBlock::TASK_NPS_TYPE:
                return $this->getNpsBlockTypeByVariety($variety);
            case LessonPartBlock::TEST_TYPE:
                return $this->getTestBlockTypeByVariety($variety);
            case LessonPartBlock::TASK_WITH_CHOICES_TYPE:
                return $this->getTestWithChoicesBlockTypeByVariety($variety);
            default:
                $this->logger->error('Не удалось определить тип блока', [
                    'type' => $type,
                    'method' => __METHOD__,
                ]);
                throw new InvalidArgumentException('Не удалось определить тип блока');
        }
    }

    /**
     * @param int $variety
     *
     * @return string
     *
     * @throws InvalidArgumentException
     */
    private function getContentBlockTypeByVariety(int $variety): ?string
    {
        switch ($variety) {
            case LessonPartBlock::HEADER_VARIETY:
                return BaseBlock1::class;
            case LessonPartBlock::TEXT_VARIETY:
                return BaseBlock2::class;
            case LessonPartBlock::VIDEO_VARIETY:
                return BaseBlock3::class;
            case LessonPartBlock::IMAGE_VARIETY:
                return BaseBlock4::class;
            case LessonPartBlock::AUDIO_VARIETY:
                return BaseBlock5::class;
            case LessonPartBlock::FILE_VARIETY:
                return BaseBlock6::class;
            case LessonPartBlock::BUTTON_VARIETY:
                return BaseBlock7::class;
            default:
                $this->logger->error('Не удалось определить вариацию блока', [
                    'variety' => $variety,
                    'method' => __METHOD__,
                ]);
                throw new InvalidArgumentException('Не удалось определить вариацию блока');
        }
    }

    /**
     * @param int $variety
     *
     * @return string
     *
     * @throws InvalidArgumentException
     */
    private function getTaskBlockTypeByVariety(int $variety): ?string
    {
        switch ($variety) {
            case LessonPartBlock::TASK_CHECK_VARIETY:
                return BaseBlockTaskWithCheck::class;
            default:
                $this->logger->error('Не удалось определить вариацию блока', [
                    'variety' => $variety,
                    'method' => __METHOD__,
                ]);
                throw new InvalidArgumentException('Не удалось определить вариацию блока');
        }
    }

    /**
     * @param int $variety
     *
     * @return string
     *
     * @throws InvalidArgumentException
     */
    private function getNpsBlockTypeByVariety(int $variety): ?string
    {
        switch ($variety) {
            case LessonPartBlock::NPS_VARIETY:
                return BaseBlockNps::class;
            default:
                $this->logger->error('Не удалось определить вариацию блока', [
                    'variety' => $variety,
                    'method' => __METHOD__,
                ]);
                throw new InvalidArgumentException('Не удалось определить вариацию блока');
        }
    }

    /**
     * @param int $variety
     *
     * @return string
     *
     * @throws InvalidArgumentException
     */
    private function getTestBlockTypeByVariety(int $variety): ?string
    {
        switch ($variety) {
            case LessonPartBlock::TEST_TASK_VARIETY:
                return TaskTest::class;
            default:
                $this->logger->error('Не удалось определить вариацию блока', [
                    'variety' => $variety,
                    'method' => __METHOD__,
                ]);
                throw new InvalidArgumentException('Не удалось определить вариацию блока');
        }
    }

    /**
     * @param int $variety
     *
     * @return string
     *
     * @throws InvalidArgumentException
     */
    public function getTestWithChoicesBlockTypeByVariety(int $variety): ?string
    {
        switch ($variety) {
            case LessonPartBlock::TEST_WITH_NUMBERS_VARIETY:
                return TaskType5::class;
            default:
                $this->logger->error('Не удалось определить вариацию блока', [
                    'variety' => $variety,
                    'method' => __METHOD__,
                ]);
                throw new InvalidArgumentException('Не удалось определить вариацию блока');
        }
    }

    /**
     * @param int $variety
     *
     * @return string
     *
     * @throws InvalidArgumentException
     */
    public function getTaskSetVarietyBlock(int $variety): string
    {
        switch ($variety) {
            case LessonPartBlock::TASK_TYPE_1_VARIETY:
                return TaskType1::class;
            default:
                $this->logger->error('Не удалось определить вариацию блока', [
                    'variety' => $variety,
                    'method' => __METHOD__,
                ]);
                throw new InvalidArgumentException('Не удалось определить вариацию блока');
        }
    }
}

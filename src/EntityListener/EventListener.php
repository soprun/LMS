<?php

namespace App\EntityListener;

use App\Entity\Analytics\EventSearchFilter;
use App\Entity\Event;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class EventListener
{
    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function postPersist(Event $event, LifecycleEventArgs $args): void
    {
        if ($event->getType() === Event::TYPE_CLUB_SEARCH) {
            foreach ($event->getData() as $field => $value) {
                if ($field !== 'params') {
                    $filter = new EventSearchFilter();
                    $filter->setEvent($event);
                    $filter->setField($field);
                    $filter->setValue($value);
                    $args->getEntityManager()->persist($filter);
                } else {
                    foreach ($event->getData()['params'] as $paramsField => $paramsValue) {
                        $filter = new EventSearchFilter();
                        $filter->setEvent($event);
                        $filter->setField('params.' . $paramsField);
                        $filter->setValue($paramsValue);
                        $args->getEntityManager()->persist($filter);
                    }
                }
            }
            $args->getEntityManager()->flush();
        }
    }

}

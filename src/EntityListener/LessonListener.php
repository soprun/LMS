<?php

namespace App\EntityListener;

use App\Entity\Courses\Lesson;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

final class LessonListener
{
    /**
     * @param Lesson $lesson
     * @param LifecycleEventArgs $event
     *
     * @throws ORMException
     */
    public function postPersist(Lesson $lesson, LifecycleEventArgs $event): void
    {
        $this->processAddedUserGroups($lesson, $event);
    }

    /**
     * @param Lesson $lesson
     * @param LifecycleEventArgs $event
     *
     * @throws ORMException
     */
    public function postUpdate(Lesson $lesson, LifecycleEventArgs $event): void
    {
        $this->processAddedUserGroups($lesson, $event);
        $this->processDeletedUserGroups($lesson, $event);
    }

    /**
     * @param Lesson $lesson
     * @param LifecycleEventArgs $event
     *
     * @throws ORMException
     */
    public function preRemove(Lesson $lesson, LifecycleEventArgs $event): void
    {
        $this->processDeletedUserGroups($lesson, $event, []);
    }

    /**
     * @param Lesson $lesson
     * @param LifecycleEventArgs $event
     *
     * @throws ORMException
     */
    public function processAddedUserGroups(Lesson $lesson, LifecycleEventArgs $event): void
    {
        $course = $lesson->getCourse();
        if (!$course) {
            return;
        }

        $courseUserGroups = $course->getUserGroups()->toArray();
        $lessonUserGroups = $lesson->getUserGroups()->toArray();

        $addedUserGroups = array_diff($lessonUserGroups, $courseUserGroups);

        if (count($addedUserGroups) > 0) {
            $course->addUserGroups($addedUserGroups);
            $event->getEntityManager()->persist($course);
            $event->getEntityManager()->flush();
        }
    }

    /**
     * @param Lesson $lesson
     * @param LifecycleEventArgs $event
     * @param array|null $lessonUserGroups
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function processDeletedUserGroups(
        Lesson $lesson,
        LifecycleEventArgs $event,
        ?array $lessonUserGroups = null
    ): void {
        $course = $lesson->getCourse();
        if (!$course) {
            return;
        }

        $courseUserGroups = $course->getUserGroups()->toArray();
        if (is_null($lessonUserGroups)) {
            $lessonUserGroups = $lesson->getUserGroups()->toArray();
        }

        $deletedUserGroups = array_diff($courseUserGroups, $lessonUserGroups);

        if (count($deletedUserGroups) > 0) {
            foreach ($deletedUserGroups as $deletedUserGroup) {
                foreach ($course->getLessons() as $lessonOfCourse) {
                    if (
                        $lesson !== $lessonOfCourse
                        && $lessonOfCourse->getUserGroups()->contains($deletedUserGroup)
                    ) {
                        continue 2;
                    }
                }

                $course->removeUserGroup($deletedUserGroup);
            }

            $event->getEntityManager()->persist($course);
            $event->getEntityManager()->flush();
        }
    }

}

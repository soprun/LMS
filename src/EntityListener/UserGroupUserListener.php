<?php

namespace App\EntityListener;

use App\Entity\UserGroupUser;
use App\Service\AuthServerService;
use Doctrine\ORM\Event\LifecycleEventArgs;
use GuzzleHttp\Exception\GuzzleException;

final class UserGroupUserListener
{
    /** @var AuthServerService */
    private $authServer;

    public function __construct(AuthServerService $authServer)
    {
        $this->authServer = $authServer;
    }

    private const REALITY_USER_GROUPS = [
        793,
        794,
        795,
    ];

    /**
     * @throws GuzzleException
     */
    public function postPersist(UserGroupUser $userGroupUser, LifecycleEventArgs $args): void
    {
        if (in_array($userGroupUser->getUserGroup()->getId(), self::REALITY_USER_GROUPS, true)) {
            $this->authServer->sendRealityGreeting($userGroupUser->getUser());
        }
    }

}

<?php

namespace App\EntityListener;

use App\Entity\DeletingLog;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Security;

final class DeletingEntityListener
{
    /** @var EntityManagerInterface */
    private $em;
    /** @var User|null */
    private $user;

    public function __construct(
        EntityManagerInterface $em,
        Security $security
    ) {
        $this->em = $em;
        $this->user = $security->getUser();
    }

    public function postRemove($entity, LifecycleEventArgs $args): void
    {
        $this->log($entity);
    }

    public function postUpdate($entity, LifecycleEventArgs $args): void
    {
        /**
         * @var array<string, array{
         *     0: mixed, //prev value
         *     1: mixed, //new value
         * }> $changedFields
         */
        $changedFields = $args->getEntityManager()->getUnitOfWork()->getEntityChangeSet($entity);
        if (!empty($changedFields['deleted']) && $changedFields['deleted'][1]) {
            $this->log($entity);
        }
    }

    public function log(object $entity): void
    {
        $log = new DeletingLog(get_class($entity), $this->getIdentifiers($entity), $this->user);
        $this->em->persist($log);
        $this->em->flush();
    }

    /** @return array<string, mixed> */
    private function getIdentifiers(object $entity): array
    {
        $ids = $this->em->getClassMetadata(get_class($entity))->getIdentifierValues($entity);
        foreach ($ids as $field => $value) {
            if (is_object($value)) {
                $ids[$field] = $this->getIdentifiers($value);
            }
        }

        return $ids;
    }

}

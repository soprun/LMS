<?php

namespace App\Entity;

use App\Repository\TeamRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass=TeamRepository::class)
 */
class Team
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=TeamFolder::class, inversedBy="teams")
     * @ORM\JoinColumn(nullable=false)
     */
    private $folder;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private $leader;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private $captain;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private $seniorCaptain;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="teams")
     */
    private $users;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="teamsCreated")
     */
    private $createdBy;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFolder(): ?TeamFolder
    {
        return $this->folder;
    }

    public function setFolder(?TeamFolder $folder): self
    {
        $this->folder = $folder;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLeader(): ?User
    {
        return $this->leader;
    }

    public function setLeader(?User $leader): self
    {
        $this->leader = $leader;

        return $this;
    }

    public function getCaptain(): ?User
    {
        return $this->captain;
    }

    public function setCaptain(?User $captain): self
    {
        $this->captain = $captain;

        return $this;
    }

    public function getSeniorCaptain(): ?User
    {
        return $this->seniorCaptain;
    }

    public function setSeniorCaptain(?User $seniorCaptain): self
    {
        $this->seniorCaptain = $seniorCaptain;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
        }

        return $this;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?User $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

}

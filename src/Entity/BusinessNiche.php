<?php

namespace App\Entity;

use App\Repository\BusinessNicheRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use TypeError;

/**
 * @ORM\Entity(repositoryClass=BusinessNicheRepository::class)
 * @ORM\Table(uniqueConstraints={ @ORM\UniqueConstraint(columns={"user_id", "main"}) })
 */
class BusinessNiche
{
    public const TYPES = [
        'offline',
        'online',
        'combined',
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("business:niche", "marathon-investment:member")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="businessNiches")
     * @ORM\JoinColumn(nullable=false)
     * @Groups("business:niche")
     * @Assert\NotBlank
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("business:niche", "marathon-investment:member")
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=2000, nullable=true)
     * @Groups("business:niche", "marathon-investment:member")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     * @Groups("business:niche")
     */
    private $franchises;

    /**
     * @ORM\Column(type="string", length=8, nullable=true)
     * @Groups("business:niche")
     * @Assert\Choice(choices=BusinessNiche::TYPES)
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity=BusinessArea::class)
     * @Groups("business:niche")
     */
    private $area;

    /**
     * @ORM\OneToMany(targetEntity=BusinessNicheComment::class, mappedBy="niche", orphanRemoval=true)
     * @Groups("business:niche")
     */
    private $comments;

    /**
     * @ORM\Column(type="boolean")
     * @Groups("business:niche", "marathon-investment:member")
     */
    private $hasRivals = false;

    /**
     * @ORM\Column(type="boolean")
     * @Groups("business:niche", "marathon-investment:member")
     */
    private $verified = false;

    /**
     * @ORM\Column(type="boolean", nullable=true, columnDefinition="tinyint(1) CHECK (main = true OR main IS NULL)")
     * @Groups("business:niche", "marathon-investment:member")
     * @Assert\Choice(choices={true})
     */
    private $main;

    final public function __construct()
    {
        $this->comments = new ArrayCollection();
    }

    final public function getId(): int
    {
        return $this->id;
    }

    final public function getUser(): User
    {
        return $this->user;
    }

    final public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    final public function getName(): ?string
    {
        return $this->name;
    }

    final public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    final public function getDescription(): ?string
    {
        return $this->description;
    }

    final public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    final public function getFranchises(): ?string
    {
        return $this->franchises;
    }

    final public function setFranchises(?string $franchises): self
    {
        $this->franchises = $franchises;

        return $this;
    }

    final public function getType(): ?string
    {
        return $this->type;
    }

    final public function setType(?string $type): self
    {
        if ($type !== null && !in_array($type, self::TYPES, true)) {
            throw new TypeError('wrong business type ' . $type);
        }
        $this->type = $type;

        return $this;
    }

    final public function getArea(): ?BusinessArea
    {
        return $this->area;
    }

    final public function setArea(?BusinessArea $area): self
    {
        $this->area = $area;

        return $this;
    }

    /**
     * @return Collection|BusinessNicheComment[]
     */
    final public function getComments(): Collection
    {
        return $this->comments;
    }

    final public function addComment(BusinessNicheComment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setNiche($this);
        }

        return $this;
    }

    final public function removeComment(BusinessNicheComment $comment): self
    {
        if ($this->comments->removeElement($comment) && $comment->getNiche() === $this) {
            $comment->setNiche(null);
        }

        return $this;
    }

    final public function getHasRivals(): ?bool
    {
        return $this->hasRivals;
    }

    final public function setHasRivals(bool $hasRivals): self
    {
        $this->hasRivals = $hasRivals;

        return $this;
    }

    final public function getVerified(): ?bool
    {
        return $this->verified;
    }

    final public function setVerified(bool $verified): self
    {
        $this->verified = $verified;

        return $this;
    }

    final public function isMain(): ?bool
    {
        return (bool)$this->main;
    }

    final public function setMain(bool $main): self
    {
        $this->main = $main ? true : null;

        return $this;
    }

}

<?php

namespace App\Entity\Task;

use App\Entity\Interfaces\LessonContentBlockInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Задание с вводом правильного ответа из списка
 * @ORM\Entity(repositoryClass="App\Repository\Task\TaskType5Repository")
 * @ORM\Table("courses_task_type_5")
 */
class TaskType5 implements LessonContentBlockInterface
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=2000, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $ddl;

    /**
     * @ORM\Column(type="string", length=1000)
     */
    private $correctAnswers;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $additionalCorrectAnswers;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $pointsBeforeDdl;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $pointsAfterDdl;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $addCorAnsPointsBeforeDdl;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $addCorAnsPointsAfterDdl;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Task\AnswerTaskType5", mappedBy="task")
     */
    private $answerTaskType5s;

    public function __construct()
    {
        $this->answerTaskType5s = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDdl(): ?\DateTimeInterface
    {
        return $this->ddl;
    }

    public function setDdl(?\DateTimeInterface $ddl): self
    {
        $this->ddl = $ddl;

        return $this;
    }

    public function getCorrectAnswers(): ?string
    {
        return $this->correctAnswers;
    }

    public function setCorrectAnswers(string $correctAnswers): self
    {
        $this->correctAnswers = $correctAnswers;

        return $this;
    }

    public function getAdditionalCorrectAnswers(): ?string
    {
        return $this->additionalCorrectAnswers;
    }

    public function setAdditionalCorrectAnswers(?string $additionalCorrectAnswers): self
    {
        $this->additionalCorrectAnswers = $additionalCorrectAnswers;

        return $this;
    }

    public function getPointsBeforeDdl(): ?int
    {
        return $this->pointsBeforeDdl;
    }

    public function setPointsBeforeDdl(?int $pointsBeforeDdl): self
    {
        $this->pointsBeforeDdl = $pointsBeforeDdl;

        return $this;
    }

    public function getPointsAfterDdl(): ?int
    {
        return $this->pointsAfterDdl;
    }

    public function setPointsAfterDdl(?int $pointsAfterDdl): self
    {
        $this->pointsAfterDdl = $pointsAfterDdl;

        return $this;
    }

    public function getAddCorAnsPointsBeforeDdl(): ?int
    {
        return $this->addCorAnsPointsBeforeDdl;
    }

    public function setAddCorAnsPointsBeforeDdl(?int $addCorAnsPointsBeforeDdl): self
    {
        $this->addCorAnsPointsBeforeDdl = $addCorAnsPointsBeforeDdl;

        return $this;
    }

    public function getAddCorAnsPointsAfterDdl(): ?int
    {
        return $this->addCorAnsPointsAfterDdl;
    }

    public function setAddCorAnsPointsAfterDdl(?int $addCorAnsPointsAfterDdl): self
    {
        $this->addCorAnsPointsAfterDdl = $addCorAnsPointsAfterDdl;

        return $this;
    }

    /**
     * @return Collection|AnswerTaskType5[]
     */
    public function getAnswerTaskType5s(): Collection
    {
        return $this->answerTaskType5s;
    }

    public function addAnswerTaskType5(AnswerTaskType5 $answerTaskType5): self
    {
        if (!$this->answerTaskType5s->contains($answerTaskType5)) {
            $this->answerTaskType5s[] = $answerTaskType5;
            $answerTaskType5->setTask($this);
        }

        return $this;
    }

    public function removeAnswerTaskType5(AnswerTaskType5 $answerTaskType5): self
    {
        if ($this->answerTaskType5s->contains($answerTaskType5)) {
            $this->answerTaskType5s->removeElement($answerTaskType5);
            // set the owning side to null (unless already changed)
            if ($answerTaskType5->getTask() === $this) {
                $answerTaskType5->setTask(null);
            }
        }

        return $this;
    }

}

<?php

namespace App\Entity\Task;

use App\Entity\BaseBlock\BaseFiles;
use App\Entity\Interfaces\LessonContentBlockInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Task\TaskTestRepository")
 * @ORM\Table("courses_task_test")
 */
class TaskTest implements LessonContentBlockInterface
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=2000, nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BaseBlock\BaseFiles", inversedBy="taskTests")
     */
    private $imgFile;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Task\TaskTestQuestion", mappedBy="test")
     */
    private $taskTestQuestions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Task\TaskTestResult", mappedBy="test")
     */
    private $taskTestResults;

    public function __construct()
    {
        $this->taskTestQuestions = new ArrayCollection();
        $this->taskTestResults = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImgFile(): ?BaseFiles
    {
        return $this->imgFile;
    }

    public function setImgFile(?BaseFiles $imgFile): self
    {
        $this->imgFile = $imgFile;

        return $this;
    }

    /**
     * @return Collection|TaskTestQuestion[]
     */
    public function getTaskTestQuestions(): Collection
    {
        return $this->taskTestQuestions;
    }

    public function addTaskTestQuestion(TaskTestQuestion $taskTestQuestion): self
    {
        if (!$this->taskTestQuestions->contains($taskTestQuestion)) {
            $this->taskTestQuestions[] = $taskTestQuestion;
            $taskTestQuestion->setTest($this);
        }

        return $this;
    }

    public function addTaskTestQuestions(array $taskTestQuestions): self
    {
        foreach ($taskTestQuestions as $taskTestQuestion) {
            $this->addTaskTestQuestion($taskTestQuestion);
        }

        return $this;
    }

    public function removeTaskTestQuestion(TaskTestQuestion $taskTestQuestion): self
    {
        if ($this->taskTestQuestions->contains($taskTestQuestion)) {
            $this->taskTestQuestions->removeElement($taskTestQuestion);
            // set the owning side to null (unless already changed)
            if ($taskTestQuestion->getTest() === $this) {
                $taskTestQuestion->setTest(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TaskTestResult[]
     */
    public function getTaskTestResults(): Collection
    {
        return $this->taskTestResults;
    }

    public function addTaskTestResult(TaskTestResult $taskTestResult): self
    {
        if (!$this->taskTestResults->contains($taskTestResult)) {
            $this->taskTestResults[] = $taskTestResult;
            $taskTestResult->setTest($this);
        }

        return $this;
    }

    public function removeTaskTestResult(TaskTestResult $taskTestResult): self
    {
        if ($this->taskTestResults->contains($taskTestResult)) {
            $this->taskTestResults->removeElement($taskTestResult);
            // set the owning side to null (unless already changed)
            if ($taskTestResult->getTest() === $this) {
                $taskTestResult->setTest(null);
            }
        }

        return $this;
    }
}

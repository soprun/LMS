<?php

namespace App\Entity\Task;

use App\Entity\BaseBlock\BaseFiles;
use App\Entity\User;
use App\Repository\Task\AnswerCommentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass=AnswerCommentRepository::class)
 */
class AnswerComment
{
    use TimestampableEntity;

    public const STATE_NOT_VIEWED = 0;
    public const STATE_VIEWED = 1;
    public const STATE_ANSWER_SENT = 2;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $answerId;

    /**
     * @ORM\Column(type="integer")
     */
    private $variety;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="answerComments")
     * @ORM\JoinColumn(nullable=false)
     */
    private $commentator;

    /**
     * @ORM\Column(type="string", length=2000)
     */
    private $comment;

    /**
     * @ORM\ManyToMany(targetEntity=BaseFiles::class, inversedBy="answerComments")
     */
    private $commentFiles;

    /**
     * @ORM\Column(type="integer")
     */
    private $state;

    public function __construct()
    {
        $this->commentFiles = new ArrayCollection();
        $this->state = AnswerComment::STATE_NOT_VIEWED;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAnswerId(): ?int
    {
        return $this->answerId;
    }

    public function setAnswerId(int $answerId): self
    {
        $this->answerId = $answerId;

        return $this;
    }

    public function getVariety(): ?int
    {
        return $this->variety;
    }

    public function setVariety(int $variety): self
    {
        $this->variety = $variety;

        return $this;
    }

    public function getCommentator(): ?User
    {
        return $this->commentator;
    }

    public function setCommentator(?User $commentator): self
    {
        $this->commentator = $commentator;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @return Collection|BaseFiles[]
     */
    public function getCommentFiles(): Collection
    {
        return $this->commentFiles;
    }

    public function addCommentFile(BaseFiles $commentFile): self
    {
        if (!$this->commentFiles->contains($commentFile)) {
            $this->commentFiles[] = $commentFile;
        }

        return $this;
    }

    public function removeCommentFile(BaseFiles $commentFile): self
    {
        if ($this->commentFiles->contains($commentFile)) {
            $this->commentFiles->removeElement($commentFile);
        }

        return $this;
    }

    public function getState(): ?int
    {
        return $this->state;
    }

    public function setState(int $state): self
    {
        $this->state = $state;

        return $this;
    }

}

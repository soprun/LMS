<?php

namespace App\Entity\Task;

use App\Entity\User;
use App\Repository\Task\TaskCheckLogRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass=TaskCheckLogRepository::class)
 * @ORM\Table("courses_task_check_log")
 */
class TaskCheckLog
{
    use TimestampableEntity;

    public const STATUS_NEW_ACCEPTED = 1;
    public const STATUS_TOOK_FROM_ANOTHER = 2;
    public const STATUS_PASSED = 3;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $reviewer;

    /**
     * @ORM\Column(type="integer")
     */
    private $statusId;

    /**
     * @ORM\ManyToOne(targetEntity=AnswerTaskType1::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $answer;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReviewer(): ?User
    {
        return $this->reviewer;
    }

    public function setReviewer(?User $reviewer): self
    {
        $this->reviewer = $reviewer;

        return $this;
    }

    public function getStatusId(): ?int
    {
        return $this->statusId;
    }

    public function setStatusId(int $statusId): self
    {
        $this->statusId = $statusId;

        return $this;
    }

    public function getAnswer(): ?AnswerTaskType1
    {
        return $this->answer;
    }

    public function setAnswer(?AnswerTaskType1 $answer): self
    {
        $this->answer = $answer;

        return $this;
    }

}

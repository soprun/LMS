<?php

namespace App\Entity\Task;

use App\Entity\BaseBlock\BaseFiles;
use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Task\AnswerTaskType1Repository")
 *
 * @ORM\Table("courses_answer_task_type_1")
 */
class AnswerTaskType1
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Task\TaskType1", inversedBy="answers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $task;

    /**
     * @ORM\Column(type="string", length=2000)
     */
    private $answer;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isChecked;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $points;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="answerTaskType1s")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="validatingAnswers")
     * @ORM\JoinColumn(nullable=true)
     */
    private $validating;

    /**
     * @ORM\Column(type="string", length=5000, nullable=true)
     */
    private $validatingText;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\BaseBlock\BaseFiles", inversedBy="answerTaskType1s")
     */
    private $answerFiles;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $tookToCheck;

    public function __construct()
    {
        $this->answerFiles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTask(): ?TaskType1
    {
        return $this->task;
    }

    public function setTask(?TaskType1 $task): self
    {
        $this->task = $task;

        return $this;
    }

    public function getAnswer(): ?string
    {
        return $this->answer;
    }

    public function setAnswer(string $answer): self
    {
        $this->answer = $answer;

        return $this;
    }

    public function getIsChecked(): ?bool
    {
        return $this->isChecked;
    }

    public function setIsChecked(?bool $isChecked): self
    {
        $this->isChecked = $isChecked;

        return $this;
    }

    public function getPoints(): ?int
    {
        return $this->points;
    }

    public function setPoints(?int $points): self
    {
        $this->points = $points;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getValidating(): ?User
    {
        return $this->validating;
    }

    public function setValidating(?User $validating): self
    {
        $this->validating = $validating;

        return $this;
    }

    public function getValidatingText(): ?string
    {
        return $this->validatingText;
    }

    public function setValidatingText(?string $validatingText): self
    {
        $this->validatingText = $validatingText;

        return $this;
    }

    /**
     * @return Collection|BaseFiles[]
     */
    public function getAnswerFiles(): Collection
    {
        return $this->answerFiles;
    }

    public function addAnswerFile(BaseFiles $answerFile): self
    {
        if (!$this->answerFiles->contains($answerFile)) {
            $this->answerFiles[] = $answerFile;
        }

        return $this;
    }

    public function removeAnswerFile(BaseFiles $answerFile): self
    {
        if ($this->answerFiles->contains($answerFile)) {
            $this->answerFiles->removeElement($answerFile);
        }

        return $this;
    }

    public function getTookToCheck(): ?\DateTimeInterface
    {
        return $this->tookToCheck;
    }

    public function setTookToCheck(?\DateTimeInterface $tookToCheck): self
    {
        $this->tookToCheck = $tookToCheck;

        return $this;
    }
}

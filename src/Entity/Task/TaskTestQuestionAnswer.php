<?php

namespace App\Entity\Task;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Task\TaskTestQuestionAnswerRepository")
 * @ORM\Table("courses_task_test_question_answer")
 */
class TaskTestQuestionAnswer
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Task\TaskTestQuestion", inversedBy="taskTestQuestionAnswers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $question;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $text;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isCorrect;

    /**
     * @ORM\Column(type="integer")
     */
    private $position;

    /**
     * @ORM\Column(type="boolean", options={"default": 0})
     */
    private $deleted = false;

    /**
     * @ORM\Column(type="integer")
     */
    private $points;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Task\TaskTestQuestionUserAnswer", mappedBy="answer")
     */
    private $taskTestQuestionUserAnswers;

    public function __construct()
    {
        $this->taskTestQuestionUserAnswers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuestion(): ?TaskTestQuestion
    {
        return $this->question;
    }

    public function setQuestion(?TaskTestQuestion $question): self
    {
        $this->question = $question;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getIsCorrect(): ?bool
    {
        return $this->isCorrect;
    }

    public function setIsCorrect(bool $isCorrect): self
    {
        $this->isCorrect = $isCorrect;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function isDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    public function getPoints(): ?int
    {
        return $this->points;
    }

    public function setPoints(int $points): self
    {
        $this->points = $points;

        return $this;
    }

    /**
     * @return Collection|TaskTestQuestionUserAnswer[]
     */
    public function getTaskTestQuestionUserAnswers(): Collection
    {
        return $this->taskTestQuestionUserAnswers;
    }

    public function addTaskTestQuestionUserAnswer(TaskTestQuestionUserAnswer $taskTestQuestionUserAnswer): self
    {
        if (!$this->taskTestQuestionUserAnswers->contains($taskTestQuestionUserAnswer)) {
            $this->taskTestQuestionUserAnswers[] = $taskTestQuestionUserAnswer;
            $taskTestQuestionUserAnswer->setAnswer($this);
        }

        return $this;
    }

    public function removeTaskTestQuestionUserAnswer(TaskTestQuestionUserAnswer $taskTestQuestionUserAnswer): self
    {
        if ($this->taskTestQuestionUserAnswers->contains($taskTestQuestionUserAnswer)) {
            $this->taskTestQuestionUserAnswers->removeElement($taskTestQuestionUserAnswer);
            // set the owning side to null (unless already changed)
            if ($taskTestQuestionUserAnswer->getAnswer() === $this) {
                $taskTestQuestionUserAnswer->setAnswer(null);
            }
        }

        return $this;
    }

}

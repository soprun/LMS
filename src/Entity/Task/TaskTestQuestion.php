<?php

namespace App\Entity\Task;

use App\Entity\BaseBlock\BaseFiles;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Task\TaskTestQuestionRepository")
 * @ORM\Table("courses_task_test_question")
 */
class TaskTestQuestion
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Task\TaskTest", inversedBy="taskTestQuestions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $test;

    /**
     * @ORM\Column(type="string", length=2000)
     */
    private $text;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BaseBlock\BaseFiles", inversedBy="taskTestQuestions")
     */
    private $imgFile;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isMultipleAnswers;

    /**
     * @ORM\Column(type="integer")
     */
    private $position = 0;

    /**
     * @ORM\Column(type="boolean", options={"default": 0})
     */
    private $deleted = false;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Task\TaskTestQuestionAnswer", mappedBy="question")
     */
    private $taskTestQuestionAnswers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Task\TaskTestQuestionUserAnswer", mappedBy="question")
     */
    private $taskTestQuestionUserAnswers;

    public function __construct()
    {
        $this->taskTestQuestionAnswers = new ArrayCollection();
        $this->taskTestQuestionUserAnswers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTest(): ?TaskTest
    {
        return $this->test;
    }

    public function setTest(?TaskTest $test): self
    {
        $this->test = $test;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getImgFile(): ?BaseFiles
    {
        return $this->imgFile;
    }

    public function setImgFile(?BaseFiles $imgFile): self
    {
        $this->imgFile = $imgFile;

        return $this;
    }

    /**
     * @deprecated
     */
    public function getIsMultipleAnswers(): ?bool
    {
        return $this->isMultipleAnswers;
    }

    public function isMultipleAnswers(): ?bool
    {
        return $this->isMultipleAnswers;
    }

    public function setIsMultipleAnswers(bool $isMultipleAnswers): self
    {
        $this->isMultipleAnswers = $isMultipleAnswers;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function isDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * @return Collection|TaskTestQuestionAnswer[]
     */
    public function getTaskTestQuestionAnswers(): Collection
    {
        return $this->taskTestQuestionAnswers;
    }

    public function addTaskTestQuestionAnswer(TaskTestQuestionAnswer $taskTestQuestionAnswer): self
    {
        if (!$this->taskTestQuestionAnswers->contains($taskTestQuestionAnswer)) {
            $this->taskTestQuestionAnswers[] = $taskTestQuestionAnswer;
            $taskTestQuestionAnswer->setQuestion($this);
        }

        return $this;
    }

    public function removeTaskTestQuestionAnswer(TaskTestQuestionAnswer $taskTestQuestionAnswer): self
    {
        if ($this->taskTestQuestionAnswers->contains($taskTestQuestionAnswer)) {
            $this->taskTestQuestionAnswers->removeElement($taskTestQuestionAnswer);
            // set the owning side to null (unless already changed)
            if ($taskTestQuestionAnswer->getQuestion() === $this) {
                $taskTestQuestionAnswer->setQuestion(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TaskTestQuestionUserAnswer[]
     */
    public function getTaskTestQuestionUserAnswers(): Collection
    {
        return $this->taskTestQuestionUserAnswers;
    }

    public function addTaskTestQuestionUserAnswer(TaskTestQuestionUserAnswer $taskTestQuestionUserAnswer): self
    {
        if (!$this->taskTestQuestionUserAnswers->contains($taskTestQuestionUserAnswer)) {
            $this->taskTestQuestionUserAnswers[] = $taskTestQuestionUserAnswer;
            $taskTestQuestionUserAnswer->setQuestion($this);
        }

        return $this;
    }

    public function removeTaskTestQuestionUserAnswer(TaskTestQuestionUserAnswer $taskTestQuestionUserAnswer): self
    {
        if ($this->taskTestQuestionUserAnswers->contains($taskTestQuestionUserAnswer)) {
            $this->taskTestQuestionUserAnswers->removeElement($taskTestQuestionUserAnswer);
            // set the owning side to null (unless already changed)
            if ($taskTestQuestionUserAnswer->getQuestion() === $this) {
                $taskTestQuestionUserAnswer->setQuestion(null);
            }
        }

        return $this;
    }

}

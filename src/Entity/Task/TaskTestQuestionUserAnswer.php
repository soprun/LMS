<?php

namespace App\Entity\Task;

use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Task\TaskTestQuestionUserAnswerRepository")
 * @ORM\Table("courses_task_test_question_user_answer")
 */
class TaskTestQuestionUserAnswer
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Task\TaskTestQuestion", inversedBy="taskTestQuestionUserAnswers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $question;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Task\TaskTestQuestionAnswer", inversedBy="taskTestQuestionUserAnswers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $answer;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="taskTestQuestionUserAnswers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuestion(): ?TaskTestQuestion
    {
        return $this->question;
    }

    public function setQuestion(?TaskTestQuestion $question): self
    {
        $this->question = $question;

        return $this;
    }

    public function getAnswer(): ?TaskTestQuestionAnswer
    {
        return $this->answer;
    }

    public function setAnswer(?TaskTestQuestionAnswer $answer): self
    {
        $this->answer = $answer;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

}

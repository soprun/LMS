<?php

namespace App\Entity\Task;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Task\TaskScriptRepository")
 */
class TaskScript
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=4000)
     */
    private $text;

    /**
     * @ORM\Column(type="smallint")
     */
    private $taskType;

    /**
     * @ORM\Column(type="integer")
     */
    private $taskId;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $pointsFrom;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $pointsTo;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isAfterDdl;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getTaskType(): ?int
    {
        return $this->taskType;
    }

    public function setTaskType(int $taskType): self
    {
        $this->taskType = $taskType;

        return $this;
    }

    public function getTaskId(): ?int
    {
        return $this->taskId;
    }

    public function setTaskId(int $taskId): self
    {
        $this->taskId = $taskId;

        return $this;
    }

    public function getPointsFrom(): ?int
    {
        return $this->pointsFrom;
    }

    public function setPointsFrom(?int $pointsFrom): self
    {
        $this->pointsFrom = $pointsFrom;

        return $this;
    }

    public function getPointsTo(): ?int
    {
        return $this->pointsTo;
    }

    public function setPointsTo(?int $pointsTo): self
    {
        $this->pointsTo = $pointsTo;

        return $this;
    }

    public function getIsAfterDdl(): ?bool
    {
        return $this->isAfterDdl;
    }

    public function setIsAfterDdl(?bool $isAfterDdl): self
    {
        $this->isAfterDdl = $isAfterDdl;

        return $this;
    }

}

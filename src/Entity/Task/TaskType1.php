<?php

namespace App\Entity\Task;

use App\Entity\Interfaces\LessonContentBlockInterface;
use App\Entity\Interfaces\TaskSetVarietyInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Task\TaskType1Repository")
 *
 * @ORM\Table("courses_task_type_1")
 */
class TaskType1 implements TaskSetVarietyInterface, LessonContentBlockInterface
{
    use TimestampableEntity;

    public const FIXED_SCORE = 1;   // фиксированный балл
    public const DYNAMIC_SCORE = 2; // выбор балла

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $question;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $textBeforeDdl;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $textAfterDdl;

    /**
     * @ORM\Column(type="smallint")
     *
     * 1 - фиксированный балл
     * 2 - выбор балла
     */
    private $typeOfTask;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $minPoints;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $maxPoints;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $pointsBeforeDdl;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $pointsAfterDdl;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isAutoCheck;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Task\AnswerTaskType1", mappedBy="task")
     */
    private $answers;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isFileAnswerAllow;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isLinkForbidden;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $reviewAfterDdl;

    public function __construct()
    {
        $this->answers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuestion(): ?string
    {
        return $this->question;
    }

    public function setQuestion(string $question): self
    {
        $this->question = $question;

        return $this;
    }

    public function getTextBeforeDdl(): ?string
    {
        return $this->textBeforeDdl;
    }

    public function setTextBeforeDdl(string $textBeforeDdl): self
    {
        $this->textBeforeDdl = $textBeforeDdl;

        return $this;
    }

    public function getTextAfterDdl(): ?string
    {
        return $this->textAfterDdl;
    }

    public function setTextAfterDdl(string $textAfterDdl): self
    {
        $this->textAfterDdl = $textAfterDdl;

        return $this;
    }

    public function getTypeOfTask(): ?int
    {
        return $this->typeOfTask;
    }

    public function setTypeOfTask(int $typeOfTask): self
    {
        $this->typeOfTask = $typeOfTask;

        return $this;
    }

    public function getMinPoints(): ?int
    {
        return $this->minPoints;
    }

    public function setMinPoints(?int $minPoints): self
    {
        $this->minPoints = $minPoints;

        return $this;
    }

    public function getMaxPoints(): ?int
    {
        return $this->maxPoints;
    }

    public function setMaxPoints(?int $maxPoints): self
    {
        $this->maxPoints = $maxPoints;

        return $this;
    }

    public function getPointsBeforeDdl(): ?int
    {
        return $this->pointsBeforeDdl;
    }

    public function setPointsBeforeDdl(?int $pointsBeforeDdl): self
    {
        $this->pointsBeforeDdl = $pointsBeforeDdl;

        return $this;
    }

    public function getPointsAfterDdl(): ?int
    {
        return $this->pointsAfterDdl;
    }

    public function setPointsAfterDdl(?int $pointsAfterDdl): self
    {
        $this->pointsAfterDdl = $pointsAfterDdl;

        return $this;
    }

    /**
     * @deprecated
     */
    public function getIsAutoCheck(): ?bool
    {
        return $this->isAutoCheck;
    }

    public function isAutoCheck(): ?bool
    {
        return $this->isAutoCheck;
    }

    public function setIsAutoCheck(?bool $isAutoCheck): self
    {
        $this->isAutoCheck = $isAutoCheck;

        return $this;
    }

    public function getReviewAfterDdl(): ?bool
    {
        return $this->reviewAfterDdl;
    }

    public function setReviewAfterDdl(?bool $reviewAfterDdl): self
    {
        $this->reviewAfterDdl = $reviewAfterDdl;

        return $this;
    }

    /**
     * @return Collection|AnswerTaskType1[]
     */
    public function getAnswers(): Collection
    {
        return $this->answers;
    }

    public function addAnswer(AnswerTaskType1 $answer): self
    {
        if (!$this->answers->contains($answer)) {
            $this->answers[] = $answer;
            $answer->setTask($this);
        }

        return $this;
    }

    public function removeAnswer(AnswerTaskType1 $answer): self
    {
        if ($this->answers->contains($answer)) {
            $this->answers->removeElement($answer);
            // set the owning side to null (unless already changed)
            if ($answer->getTask() === $this) {
                $answer->setTask(null);
            }
        }

        return $this;
    }

    /**
     * @deprecated
     */
    public function getIsFileAnswerAllow(): ?bool
    {
        return $this->isFileAnswerAllow;
    }

    public function isFileAnswerAllow(): ?bool
    {
        return $this->isFileAnswerAllow;
    }

    public function setIsFileAnswerAllow(?bool $isFileAnswerAllow): self
    {
        $this->isFileAnswerAllow = $isFileAnswerAllow;

        return $this;
    }

    /**
     * @deprecated
     */
    public function getIsLinkForbidden(): ?bool
    {
        return $this->isLinkForbidden;
    }

    public function isLinkForbidden(): ?bool
    {
        return $this->isLinkForbidden;
    }

    public function setIsLinkForbidden(?bool $isLinkForbidden): self
    {
        $this->isLinkForbidden = $isLinkForbidden;

        return $this;
    }

}

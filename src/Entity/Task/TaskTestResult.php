<?php

namespace App\Entity\Task;

use App\Entity\BaseBlock\BaseFiles;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Task\TaskTestResultRepository")
 * @ORM\Table("courses_task_test_result")
 */
class TaskTestResult
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Task\TaskTest", inversedBy="taskTestResults")
     * @ORM\JoinColumn(nullable=false)
     */
    private $test;

    /**
     * @ORM\Column(type="string", length=2000)
     */
    private $text;

    /**
     * @ORM\Column(type="integer")
     */
    private $pointsFrom;

    /**
     * @ORM\Column(type="integer")
     */
    private $pointsTo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BaseBlock\BaseFiles", inversedBy="taskTestResults")
     */
    private $imgFile;

    /**
     * @ORM\Column(type="boolean", options={"default": 0})
     */
    private $deleted = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTest(): ?TaskTest
    {
        return $this->test;
    }

    public function setTest(?TaskTest $test): self
    {
        $this->test = $test;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getPointsFrom(): ?int
    {
        return $this->pointsFrom;
    }

    public function setPointsFrom(int $pointsFrom): self
    {
        $this->pointsFrom = $pointsFrom;

        return $this;
    }

    public function getPointsTo(): ?int
    {
        return $this->pointsTo;
    }

    public function setPointsTo(int $pointsTo): self
    {
        $this->pointsTo = $pointsTo;

        return $this;
    }

    public function getImgFile(): ?BaseFiles
    {
        return $this->imgFile;
    }

    public function setImgFile(?BaseFiles $imgFile): self
    {
        $this->imgFile = $imgFile;

        return $this;
    }

    public function isDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

}

<?php

namespace App\Entity\Task;

use App\Entity\BaseBlock\BaseBlockTaskWithCheck;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Task\TaskSetRepository")
 *
 * @ORM\Table("courses_task_set")
 */
class TaskSet
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BaseBlock\BaseBlockTaskWithCheck", inversedBy="taskSets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $blockTaskWithCheck;

    /**
     * @ORM\Column(type="smallint")
     */
    private $variety;

    /**
     * @ORM\Column(type="smallint")
     */
    private $blockId;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $positionInTask = false;

    /**
     * @ORM\Column(type="boolean", options={"default": 0})
     */
    private $deleted = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBlockTaskWithCheck(): ?BaseBlockTaskWithCheck
    {
        return $this->blockTaskWithCheck;
    }

    public function setBlockTaskWithCheck(?BaseBlockTaskWithCheck $blockTaskWithCheck): self
    {
        $this->blockTaskWithCheck = $blockTaskWithCheck;

        return $this;
    }

    public function getVariety(): ?int
    {
        return $this->variety;
    }

    public function setVariety(int $variety): self
    {
        $this->variety = $variety;

        return $this;
    }

    public function getBlockId(): ?int
    {
        return $this->blockId;
    }

    public function setBlockId(int $blockId): self
    {
        $this->blockId = $blockId;

        return $this;
    }

    public function getPositionInTask(): ?int
    {
        return $this->positionInTask;
    }

    public function setPositionInTask(?int $positionInTask): self
    {
        $this->positionInTask = $positionInTask;

        return $this;
    }

    public function isDeleted(): bool
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

}

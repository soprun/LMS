<?php

namespace App\Entity;

use App\Entity\MarathonInvestment\Target;
use App\Repository\CourseStreamRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use RuntimeException;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

/**
 * @ORM\Entity(repositoryClass=CourseStreamRepository::class)
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 */
class CourseStream
{
    use SoftDeleteableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("marathon-investment:list", "traction", "traction:filters", "courseStream:list", "faculty:list")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=AbstractCourse::class, inversedBy="streams")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Groups({"traction", "courseStream:list"})
     */
    private $abstractCourse;

    /**
     * @ORM\Column(type="integer")
     *
     * @Groups({"traction", "traction:filters", "courseStream:list", "faculty:list"})
     */
    private $stream;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"traction", "traction:filters", "user:traction", "courseStream:list", "faculty:list"})
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=UserGroup::class, mappedBy="courseStream")
     *
     * @Groups({"traction:filters"})
     */
    private $userGroups;

    /**
     * @ORM\OneToMany(targetEntity=Target::class, mappedBy="stream")
     */
    private $targets;

    /**
     * @ORM\Column(type="date", nullable=true)
     *
     * @Groups({"traction", "courseStream:list"})
     * @var DateTime
     */
    private $startDate;

    /**
     * Период действия потока в неделях
     *
     * @ORM\Column(type="integer", nullable=true)
     *
     * @Groups({"traction", "courseStream:list"})
     */
    private $period;

    /**
     * Статус доступности механик курса: анкета, бот и тд
     * @ORM\Column(type="boolean")
     *
     * @Groups({"courseStream:list"})
     */
    private $active;

    /**
     * @ORM\OneToMany(targetEntity=Faculty::class, mappedBy="courseStream")
     */
    private $faculties;

    /**
     * @ORM\OneToMany(targetEntity=TeamFolder::class, mappedBy="courseStream")
     */
    private $teamFolders;

    /**
     * @ORM\ManyToOne(targetEntity=CourseStream::class)
     */
    private $alternativeCourseStream;

    /**
     * @ORM\OneToMany(targetEntity=UserCertificates::class, mappedBy="courseStream")
     */
    private $userCertificates;

    public function __construct()
    {
        $this->userGroups = new ArrayCollection();
        $this->targets = new ArrayCollection();
        $this->faculties = new ArrayCollection();
        $this->teamFolders = new ArrayCollection();
        $this->userCertificates = new ArrayCollection();

        $this->active = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAbstractCourse(): ?AbstractCourse
    {
        return $this->abstractCourse;
    }

    public function setAbstractCourse(?AbstractCourse $abstractCourse): self
    {
        $this->abstractCourse = $abstractCourse;

        return $this;
    }

    public function getStream(): ?int
    {
        return $this->stream;
    }

    public function setStream(int $stream): self
    {
        $this->stream = $stream;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|UserGroup[]
     */
    public function getUserGroups(): Collection
    {
        return $this->userGroups;
    }

    public function addUserGroup(UserGroup $userGroup): self
    {
        if (!$this->userGroups->contains($userGroup)) {
            $this->userGroups[] = $userGroup;
            $userGroup->setCourseStream($this);
        }

        return $this;
    }

    public function removeUserGroup(UserGroup $userGroup): self
    {
        // set the owning side to null (unless already changed)
        if ($this->userGroups->removeElement($userGroup) && $userGroup->getCourseStream() === $this) {
            $userGroup->setCourseStream(null);
        }

        return $this;
    }

    /**
     * @return Collection|Target[]
     */
    public function getTargets(): Collection
    {
        return $this->targets;
    }

    public function addTarget(Target $target): self
    {
        if (!$this->targets->contains($target)) {
            $this->targets[] = $target;
            $target->setStream($this);
        }

        return $this;
    }

    public function removeTarget(Target $target): self
    {
        // set the owning side to null (unless already changed)
        if ($this->targets->removeElement($target) && $target->getStream() === $this) {
            $target->setStream(null);
        }

        return $this;
    }

    public function getStartDate(): ?DateTime
    {
        return $this->startDate;
    }

    public function setStartDate(DateTime $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): DateTime
    {
        //Период измеряется в неделях, при этом дата начала считается включительно, поэтому 1 день вычитаем
        return (clone $this->startDate)->modify('+' . $this->period . ' weeks')->modify('-1 day');
    }

    /** @throws RuntimeException user must be joined to stream */
    public function getUserJoinStartDate(User $user): DateTime
    {
        /** @var UserGroupUser $userGroupUser */
        $userGroupUser = $user->getGroupRelations()->filter(function (UserGroupUser $ugu) {
            return $ugu->getUserGroup()->getCourseStreamId() === $this->getId();
        })->first();
        if ($userGroupUser) {
            return $userGroupUser->getCreatedAt();
        }
        throw new RuntimeException('Пользователь не является участником потока');
    }

    /** @throws RuntimeException user must be joined to stream */
    public function getUserJoinEndDate(User $user): DateTime
    {
        return (clone $this->getUserJoinStartDate($user))->modify('+ ' . $this->period . ' weeks')->modify('- 1 day');
    }

    public function getPeriod(): ?int
    {
        return $this->period;
    }

    public function setPeriod(int $period): self
    {
        $this->period = $period;

        return $this;
    }

    public function getActivePeriod(string $slug): int
    {
        $now = new DateTime("NOW");
        $startDate = $this->startDate;
        $active = -1;

        if ($startDate && $startDate <= $now) {
            $period = $this->period;

            switch ($slug) {
                case "mi":
                    // $startDate->modify('+2 day');
                    // $interval = $startDate->diff($now);
                    // $active = (int)($interval->days / $period);
                    break;
                case "speed":
                    $startDate->modify('-1 day');
                    $interval = $startDate->diff($now);
                    $active = (int)($interval->days / $period);
                    break;
                case "hundred":
                    $startDate->modify('-2 day');
                    $interval = $startDate->diff($now);
                    $active = (int)($interval->days / $period);
                    break;
            }
        }

        return $active;
    }

    public function isActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return Collection|Faculty[]
     */
    public function getFaculties(): Collection
    {
        return $this->faculties;
    }

    public function addFaculty(Faculty $faculty): self
    {
        if (!$this->faculties->contains($faculty)) {
            $this->faculties[] = $faculty;
            $faculty->setCourseStream($this);
        }

        return $this;
    }

    public function removeFaculty(Faculty $faculty): self
    {
        // set the owning side to null (unless already changed)
        if ($this->faculties->removeElement($faculty) && $faculty->getCourseStream() === $this) {
            $faculty->setCourseStream(null);
        }

        return $this;
    }

    /**
     * @return Collection|TeamFolder[]
     */
    public function getTeamFolders(): Collection
    {
        return $this->teamFolders;
    }

    public function addTeamFolder(TeamFolder $teamFolder): self
    {
        if (!$this->teamFolders->contains($teamFolder)) {
            $this->teamFolders[] = $teamFolder;
            $teamFolder->setCourseStream($this);
        }

        return $this;
    }

    public function removeTeamFolder(TeamFolder $teamFolder): self
    {
        // set the owning side to null (unless already changed)
        if ($this->teamFolders->removeElement($teamFolder) && $teamFolder->getCourseStream() === $this) {
            $teamFolder->setCourseStream(null);
        }

        return $this;
    }


    /**
     * @return Collection|UserCertificates[]
     */
    public function getUserCertificates(): Collection
    {
        return $this->userCertificates;
    }

    public function addUserCertificates(UserCertificates $userCertificates): self
    {
        if (!$this->userCertificates->contains($userCertificates)) {
            $this->userCertificates[] = $userCertificates;
            $userCertificates->setCourseStream($this);
        }

        return $this;
    }

    public function removeUserCertificates(UserCertificates $userCertificates): self
    {
        // set the owning side to null (unless already changed)
        if (
            $this->userCertificates->removeElement($userCertificates) && $userCertificates->getCourseStream() === $this
        ) {
            $userCertificates->setCourseStream(null);
        }

        return $this;
    }

    /**
     * @return DateTime
     * @Groups({"traction:user"})
     * @SerializedName("connectedDate")
     */
    public function getUserConnectedDate(): DateTime
    {
        $date = new DateTime();

        foreach ($this->getUserGroups() as $userGroup) {
            /** @var UserGroupUser $userRel */
            $userRel = $userGroup->getUserRelations()->current();
            if ($date > $userRel->getCreatedAt()) {
                $date = $userRel->getCreatedAt();
            }
        }

        return $date;
    }

    public function getAlternativeCourseStream(): ?self
    {
        return $this->alternativeCourseStream;
    }

    public function setAlternativeCourseStream(?self $alternativeCourseStream): self
    {
        $this->alternativeCourseStream = $alternativeCourseStream;

        return $this;
    }

    /**
     * @Groups("user:traction")
     * @SerializedName("slug")
     */
    public function getCourseSlug(): string
    {
        return $this->abstractCourse->getSlug();
    }

}

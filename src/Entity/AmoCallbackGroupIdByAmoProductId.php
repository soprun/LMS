<?php

namespace App\Entity;

use App\Repository\AmoCallbackGroupIdByAmoProductIdRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AmoCallbackGroupIdByAmoProductIdRepository::class)
 */
class AmoCallbackGroupIdByAmoProductId
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     * @psalm-readonly
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $productId;

    /**
     * @ORM\Column(type="json", nullable=false)
     */
    private $ticketLevelIds = [];

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $defaultGroupId;

    final public function getId(): ?int
    {
        return $this->id;
    }

    final public function getProductId(): ?int
    {
        return $this->productId;
    }

    final public function setProductId(int $productId): self
    {
        $this->productId = $productId;

        return $this;
    }

    final public function getTicketLevelIds(): array
    {
        return $this->ticketLevelIds;
    }

    final public function setTicketLevelIds(array $ticketLevelIds): self
    {
        $this->ticketLevelIds = $ticketLevelIds;

        return $this;
    }

    final public function getDefaultGroupId(): ?int
    {
        return $this->defaultGroupId;
    }

    final public function setDefaultGroupId(?int $defaultGroupId): self
    {
        $this->defaultGroupId = $defaultGroupId;

        return $this;
    }
}

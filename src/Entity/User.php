<?php

namespace App\Entity;

use App\Entity\Analytics\ReportMSG;
use App\Entity\Courses\Achievement;
use App\Entity\Courses\Lesson;
use App\Entity\MarathonInvestment\MarathonInvestment;
use App\Entity\MarathonInvestment\MarathonInvestmentConfiguration;
use App\Entity\MarathonInvestment\Target;
use App\Entity\MarathonInvestment\UserNiche;
use App\Entity\PointsShop\Order;
use App\Entity\Questionnaire\UserAnswer;
use App\Entity\SmartSender\SmartSenderUser;
use App\Entity\Task\AnswerComment;
use App\Entity\Task\AnswerTaskType1;
use App\Entity\Task\TaskTestQuestionUserAnswer;
use App\Entity\Traction\Value;
use CrEOF\Spatial\PHP\Types\Geography\Point;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    use TimestampableEntity;

    // сервисный юзер для разных логов: d.smirnov@likebz.ru
    public const SERVICE_USER = 11;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups(
     *     "marathon-investment:list",
     *     "location",
     *     "promocode:user:info",
     *     "traction", "traction:user",
     *     "business:niche",
     *     "marathon-investment:member",
     *     "business:niche"
     * )
     */
    private $id;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups("traction", "traction:user")
     */
    private $authUserId;

    /**
     * @ORM\Column(type="boolean", options={"default": 0})
     * @Groups("verification")
     */
    private $verified = false;

    /**
     * @ORM\Column(type="point", nullable=true)
     */
    private $location;

    /**
     * @Groups("location")
     */
    private $longitude;

    /**
     * @Groups("location")
     */
    private $latitude;

    /**
     * @var Timezone
     * @Groups("user")
     */
    private $timezone;
    /**
     * @var string
     * @Groups("traction:user")
     */
    private $email;
    /**
     * @var array
     */
    private $roles = [];
    /**
     * @var string The hashed password
     */
    private $password;
    /**
     * @var string
     * @Groups(
     *     "marathon-investment:list",
     *     "promocode:user:info",
     *     "traction",
     *     "traction:user",
     *     "marathon-investment:member",
     *     "business:niche"
     * )
     */
    private $name;
    /**
     * @var string
     * @Groups(
     *     "marathon-investment:list",
     *     "promocode:user:info",
     *     "traction",
     *     "traction:user",
     *     "marathon-investment:member",
     *     "business:niche"
     * )
     */
    private $lastname;
    /**
     * @var string
     * @Groups("marathon-investment:list", "traction", "traction:user", "marathon-investment:member", "business:niche")
     */
    private $patronymic;
    /**
     * @var string
     */
    private $country;
    /**
     * @var string
     * @Groups("marathon-investment:list", "traction:user", "marathon-investment:member")
     */
    private $city;
    /**
     * @var string
     * @Groups("traction:user")
     */
    private $phone;
    /**
     * @var string
     */
    private $dateOfBirth;
    /**
     * @var string
     */
    private $vkLink;
    /**
     * @var string
     */
    private $fbLink;
    /**
     * @var string
     */
    private $instaLink;
    /**
     * @var string
     */
    private $siteLink;
    /**
     * @var string
     * @Groups("traction:user")
     */
    private $telegram;
    /**
     * @var string
     */
    private $businessInfo;
    /**
     * @var string
     * @Groups("traction", "traction:user")
     */
    private $niche;
    /**
     * @var string
     */
    private $businessType;
    /**
     * @var string
     */
    private $businessLocationTypeId;
    /**
     * @var string
     */
    private $businessCity;
    /**
     * @var string
     */
    private $businessCategory;
    /**
     * @var int
     * @Groups("traction:user")
     */
    private $businessCategoryId;
    /**
     * @var string
     */
    private $businessCategoryName;
    /**
     * @var string
     */
    private $profitPerWeek;
    /**
     * @var string
     */
    private $profitPerYear;
    /**
     * @var string
     */
    private $profitPerLastYear;
    /**
     * @var string
     */
    private $franchises;
    /**
     * @var string
     *
     * @Groups("traction:user","business:niche")
     */
    private $avatar;
    /**
     * @var bool
     */
    private $isPasswordEmpty = false;
    /**
     * @var bool
     */
    private $isAdmin = false;
    /**
     * @var string
     */
    private $authError;

    private $company;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Courses\Lesson", mappedBy="speakers")
     * @var Collection|Lesson[]
     */
    private $lessons;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Task\AnswerTaskType1", mappedBy="user")
     * @var Collection|AnswerTaskType1[]
     */
    private $answerTaskType1s;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Task\AnswerTaskType1", mappedBy="validating")
     * @var Collection|AnswerTaskType1[]
     */
    private $validatingAnswers;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Courses\Achievement", inversedBy="users")
     * @var Collection|Achievement[]
     */
    private $achievements;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserGroup", mappedBy="creator")
     * @var Collection|UserGroup[]
     */
    private $createdUserGroups;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Task\TaskTestQuestionUserAnswer", mappedBy="user")
     * @var Collection|TaskTestQuestionUserAnswer[]
     */
    private $taskTestQuestionUserAnswers;

    /**
     * @ORM\OneToMany(targetEntity=UserLesson::class, mappedBy="user", orphanRemoval=true)
     * @var Collection|UserLesson[]
     */
    private $userLessons;

    /**
     * @ORM\OneToMany(targetEntity=Order::class, mappedBy="user")
     * @var Collection|Order[]
     */
    private $pointsShopOrders;

    /**
     * @ORM\OneToMany(targetEntity=UserGroupUser::class, mappedBy="user", cascade={"persist"}, orphanRemoval=true)
     * @ORM\OrderBy({"faculty" = "DESC"})
     * @Groups("marathon-investment:list")
     * @var Collection|UserGroupUser[]
     */
    private $groupRelations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Analytics\ReportMSG", mappedBy="user", orphanRemoval=true)
     * @var Collection|ReportMSG[]
     */
    private $reportsMSG;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserCourseTariff", mappedBy="manager")
     * @var Collection|UserCourseTariff[]
     */
    private $managerCourseTariffs;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserCourseTariff", mappedBy="tracker")
     * @var Collection|UserCourseTariff[]
     */
    private $trackerCourseTariffs;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserCourseTariff", mappedBy="user")
     * @var Collection|UserCourseTariff[]
     */
    private $userCourseTariffs;

    /**
     * @ORM\OneToMany(targetEntity=UserCourseTariff::class, mappedBy="mentor")
     * @var Collection|UserCourseTariff[]
     */
    private $mentorCourseTariffs;

    /**
     * @ORM\OneToMany(targetEntity=AnswerComment::class, mappedBy="commentator", orphanRemoval=true)
     * @var Collection|AnswerComment[]
     */
    private $answerComments;

    /**
     * @ORM\OneToMany(targetEntity=UserCertificates::class, mappedBy="user", orphanRemoval=true)
     * @var Collection|UserCertificates[]
     */
    private $userCertificates;

    /**
     * @ORM\OneToMany(targetEntity=SmartSenderUser::class, mappedBy="user")
     * @var Collection|SmartSenderUser[]
     */
    private $smartSenderUsers;

    /**
     * @ORM\OneToMany(targetEntity=UserAnswer::class, mappedBy="user")
     * @var Collection|UserAnswer[]
     */
    private $questionnaireUserAnswers;

    /**
     * @ORM\ManyToMany(targetEntity=Team::class, mappedBy="users")
     * @var Collection|Team[]
     */
    private $teams;

    /**
     * @ORM\OneToMany(targetEntity=Team::class, mappedBy="createdBy")
     * @var Collection|Team[]
     */
    private $teamsCreated;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\MarathonInvestment\Target", inversedBy="users")
     * @ORM\JoinTable(name="user_target")
     * @var Collection|Target[]
     */
    private $tractionTargets;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MarathonInvestment\UserNiche", mappedBy="user", cascade={"persist"})
     * @Groups({"marathon-investment:list", "marathon-investment:niches:create"})
     * @var Collection|UserNiche[]
     */
    private $niches;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MarathonInvestment\MarathonInvestmentConfiguration", mappedBy="user")
     * @SerializedName("configurations")
     * @Groups("marathon-investment:list", "marathon-investment:member")
     * @var Collection|MarathonInvestmentConfiguration[]
     */
    private $marathonConfigurations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MarathonInvestment\MarathonInvestment", mappedBy="user")
     * @var Collection|MarathonInvestment[]
     */
    private $marathonInvestments;

    /**
     * @var int
     * @Groups("marathon-investment:list", "marathon-investment:member")
     */
    private $totalAmount;

    /**
     * @var int
     * @Groups("marathon-investment:list", "marathon-investment:member")
     */
    private $totalPlanned;

    /**
     * @var int
     * @Groups("marathon-investment:list", "marathon-investment:member")
     */
    private $totalPassed;

    /**
     * @ORM\OneToMany(targetEntity=Promocode::class, mappedBy="user")
     * @var Collection|Promocode[]
     */
    private $promocodes;

    /**
     * @ORM\OneToMany(targetEntity=AttemptToGetPromocode::class, mappedBy="user")
     * @var Collection|AttemptToGetPromocode[]
     */
    private $attemptToGetPromocodes;

    /**
     * @ORM\OneToMany(targetEntity=Value::class, mappedBy="owner")
     * @var Collection|Value[]
     */
    private $tractionValues;

    /**
     * @ORM\OneToMany(targetEntity=BusinessPhoto::class, mappedBy="owner", orphanRemoval=true)
     * @var Collection|BusinessPhoto[]
     */
    private $businessPhotos;

    /**
     * @ORM\OneToMany(targetEntity=BusinessNiche::class, mappedBy="user", orphanRemoval=true)
     * @Groups("marathon-investment:member")
     * @var Collection|BusinessNiche[]
     */
    private $businessNiches;


    public function __construct()
    {
        $this->lessons = new ArrayCollection();
        $this->answerTaskType1s = new ArrayCollection();
        $this->validatingAnswers = new ArrayCollection();
        $this->achievements = new ArrayCollection();
        $this->createdUserGroups = new ArrayCollection();
        $this->taskTestQuestionUserAnswers = new ArrayCollection();
        $this->userLessons = new ArrayCollection();
        $this->pointsShopOrders = new ArrayCollection();
        $this->groupRelations = new ArrayCollection();
        $this->reportsMSG = new ArrayCollection();
        $this->userCourseTariffs = new ArrayCollection();
        $this->mentorCourseTariffs = new ArrayCollection();
        $this->answerComments = new ArrayCollection();
        $this->userCertificates = new ArrayCollection();
        $this->smartSenderUsers = new ArrayCollection();
        $this->questionnaireUserAnswers = new ArrayCollection();
        $this->teams = new ArrayCollection();
        $this->teamsCreated = new ArrayCollection();
        $this->tractionTargets = new ArrayCollection();
        $this->niches = new ArrayCollection();
        $this->marathonConfigurations = new ArrayCollection();
        $this->marathonInvestments = new ArrayCollection();
        $this->promocodes = new ArrayCollection();
        $this->attemptToGetPromocodes = new ArrayCollection();
        $this->tractionValues = new ArrayCollection();
        $this->businessPhotos = new ArrayCollection();
        $this->businessNiches = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAuthUserId(): ?int
    {
        return $this->authUserId;
    }

    public function setAuthUserId(?int $authUserId): self
    {
        $this->authUserId = $authUserId;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string)$this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        return array_unique($this->roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getTimezone(): ?Timezone
    {
        return $this->timezone;
    }

    public function setTimezone(?Timezone $timezone): self
    {
        $this->timezone = $timezone;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(?string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getPatronymic(): ?string
    {
        return $this->patronymic;
    }

    public function setPatronymic(?string $patronymic): self
    {
        $this->patronymic = $patronymic;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getDateOfBirth(): ?string
    {
        return $this->dateOfBirth;
    }

    public function setDateOfBirth(?string $dateOfBirth): self
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    public function getVkLink(): ?string
    {
        return $this->vkLink;
    }

    public function setVkLink(?string $vkLink): self
    {
        $this->vkLink = $vkLink;

        return $this;
    }

    public function getFbLink(): ?string
    {
        return $this->fbLink;
    }

    public function setFbLink(?string $fbLink): self
    {
        $this->fbLink = $fbLink;

        return $this;
    }

    public function getInstaLink(): ?string
    {
        return $this->instaLink;
    }

    public function setInstaLink(?string $instaLink): self
    {
        $this->instaLink = $instaLink;

        return $this;
    }

    public function getSiteLink(): ?string
    {
        return $this->siteLink;
    }

    public function setSiteLink(?string $siteLink): self
    {
        $this->siteLink = $siteLink;

        return $this;
    }

    public function getTelegram(): string
    {
        $part = explode("|", $this->telegram);

        return end($part);
    }

    public function setTelegram(string $telegram): self
    {
        $this->telegram = $telegram;

        return $this;
    }

    public function getBusinessInfo(): ?string
    {
        return $this->businessInfo;
    }

    public function setBusinessInfo(?string $businessInfo): self
    {
        $this->businessInfo = $businessInfo;

        return $this;
    }

    public function getBusinessCategory(): ?string
    {
        return $this->businessCategory;
    }

    public function setBusinessCategory(?string $businessCategory): self
    {
        $this->businessCategory = $businessCategory;

        return $this;
    }

    public function getBusinessCategoryId(): ?int
    {
        return $this->businessCategoryId;
    }

    public function setBusinessCategoryId(?int $businessCategoryId): self
    {
        $this->businessCategoryId = $businessCategoryId;

        return $this;
    }

    public function getBusinessCategoryName(): ?string
    {
        return $this->businessCategoryName;
    }

    public function setBusinessCategoryName(?string $businessCategoryName): self
    {
        $this->businessCategoryName = $businessCategoryName;

        return $this;
    }

    public function getNiche(): ?string
    {
        return $this->niche;
    }

    public function setNiche(?string $niche): self
    {
        $this->niche = $niche;

        return $this;
    }

    public function getBusinessType(): ?string
    {
        return $this->businessType;
    }

    public function setBusinessType(?string $businessType): self
    {
        $this->businessType = $businessType;

        return $this;
    }

    public function getBusinessLocationTypeId(): ?string
    {
        return $this->businessLocationTypeId;
    }

    public function setBusinessLocationTypeId(?string $businessLocationTypeId): self
    {
        $this->businessLocationTypeId = $businessLocationTypeId;

        return $this;
    }

    public function getBusinessCity(): ?string
    {
        return $this->businessCity;
    }

    public function setBusinessCity(?string $businessCity): self
    {
        $this->businessCity = $businessCity;

        return $this;
    }

    public function getProfitPerWeek(): ?string
    {
        return $this->profitPerWeek;
    }

    public function setProfitPerWeek(?string $profitPerWeek): self
    {
        $this->profitPerWeek = $profitPerWeek;

        return $this;
    }

    public function getProfitPerYear(): ?string
    {
        return $this->profitPerYear;
    }

    public function setProfitPerYear(?string $profitPerYear): self
    {
        $this->profitPerYear = $profitPerYear;

        return $this;
    }

    public function getProfitPerLastYear(): ?string
    {
        return $this->profitPerLastYear;
    }

    public function setProfitPerLastYear(?string $profitPerLastYear): self
    {
        $this->profitPerLastYear = $profitPerLastYear;

        return $this;
    }

    public function getFranchises(): ?string
    {
        return $this->franchises;
    }

    public function setFranchises(?string $franchises): self
    {
        $this->franchises = $franchises;

        return $this;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function setAvatar(?string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function getIsPasswordEmpty(): bool
    {
        return $this->isPasswordEmpty;
    }

    public function setIsPasswordEmpty(bool $isPasswordEmpty): self
    {
        $this->isPasswordEmpty = $isPasswordEmpty;

        return $this;
    }

    public function getIsAdmin(): bool
    {
        return $this->isAdmin;
    }

    public function setIsAdmin(bool $isAdmin): self
    {
        $this->isAdmin = $isAdmin;

        return $this;
    }

    public function getAuthError(): ?string
    {
        return $this->authError;
    }

    public function setAuthError(?string $authError): self
    {
        $this->authError = $authError;

        return $this;
    }

    public function __toString(): string
    {
        return $this->email ?? 'withoutEmail';
    }

    /**
     * @return Collection|Lesson[]
     */
    public function getLessons(): Collection
    {
        return $this->lessons;
    }

    public function addLesson(Lesson $lesson): self
    {
        if (!$this->lessons->contains($lesson)) {
            $this->lessons[] = $lesson;
            $lesson->addSpeaker($this);
        }

        return $this;
    }

    public function removeLesson(Lesson $lesson): self
    {
        if ($this->lessons->contains($lesson)) {
            $this->lessons->removeElement($lesson);
            $lesson->removeSpeaker($this);
        }

        return $this;
    }

    /**
     * @return Collection|AnswerTaskType1[]
     */
    public function getAnswerTaskType1s(): Collection
    {
        return $this->answerTaskType1s;
    }

    public function addAnswerTaskType1(AnswerTaskType1 $answerTaskType1): self
    {
        if (!$this->answerTaskType1s->contains($answerTaskType1)) {
            $this->answerTaskType1s[] = $answerTaskType1;
            $answerTaskType1->setUser($this);
        }

        return $this;
    }

    public function removeAnswerTaskType1(AnswerTaskType1 $answerTaskType1): self
    {
        if ($this->answerTaskType1s->contains($answerTaskType1)) {
            $this->answerTaskType1s->removeElement($answerTaskType1);
            // set the owning side to null (unless already changed)
            if ($answerTaskType1->getUser() === $this) {
                $answerTaskType1->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|AnswerTaskType1[]
     */
    public function getValidatingAnswers(): Collection
    {
        return $this->validatingAnswers;
    }

    public function addValidatingAnswer(AnswerTaskType1 $validatingAnswer): self
    {
        if (!$this->validatingAnswers->contains($validatingAnswer)) {
            $this->validatingAnswers[] = $validatingAnswer;
            $validatingAnswer->setValidating($this);
        }

        return $this;
    }

    public function removeValidatingAnswer(AnswerTaskType1 $validatingAnswer): self
    {
        if ($this->validatingAnswers->contains($validatingAnswer)) {
            $this->validatingAnswers->removeElement($validatingAnswer);
            // set the owning side to null (unless already changed)
            if ($validatingAnswer->getValidating() === $this) {
                $validatingAnswer->setValidating(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Achievement[]
     */
    public function getAchievements(): Collection
    {
        return $this->achievements;
    }

    public function addAchievement(Achievement $achievement): self
    {
        if (!$this->achievements->contains($achievement)) {
            $this->achievements[] = $achievement;
        }

        return $this;
    }

    public function removeAchievement(Achievement $achievement): self
    {
        if ($this->achievements->contains($achievement)) {
            $this->achievements->removeElement($achievement);
        }

        return $this;
    }

    /**
     * @return Collection|UserGroup[]
     */
    public function getCreatedUserGroups(): Collection
    {
        return $this->createdUserGroups;
    }

    public function addCreatedUserGroup(UserGroup $createdUserGroup): self
    {
        if (!$this->createdUserGroups->contains($createdUserGroup)) {
            $this->createdUserGroups[] = $createdUserGroup;
            $createdUserGroup->setCreator($this);
        }

        return $this;
    }

    public function removeCreatedUserGroup(UserGroup $createdUserGroup): self
    {
        if ($this->createdUserGroups->contains($createdUserGroup)) {
            $this->createdUserGroups->removeElement($createdUserGroup);
            // set the owning side to null (unless already changed)
            if ($createdUserGroup->getCreator() === $this) {
                $createdUserGroup->setCreator(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TaskTestQuestionUserAnswer[]
     */
    public function getTaskTestQuestionUserAnswers(): Collection
    {
        return $this->taskTestQuestionUserAnswers;
    }

    public function addTaskTestQuestionUserAnswer(TaskTestQuestionUserAnswer $taskTestQuestionUserAnswer): self
    {
        if (!$this->taskTestQuestionUserAnswers->contains($taskTestQuestionUserAnswer)) {
            $this->taskTestQuestionUserAnswers[] = $taskTestQuestionUserAnswer;
            $taskTestQuestionUserAnswer->setUser($this);
        }

        return $this;
    }

    public function removeTaskTestQuestionUserAnswer(TaskTestQuestionUserAnswer $taskTestQuestionUserAnswer): self
    {
        if ($this->taskTestQuestionUserAnswers->contains($taskTestQuestionUserAnswer)) {
            $this->taskTestQuestionUserAnswers->removeElement($taskTestQuestionUserAnswer);
            // set the owning side to null (unless already changed)
            if ($taskTestQuestionUserAnswer->getUser() === $this) {
                $taskTestQuestionUserAnswer->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserLesson[]
     */
    public function getUserLessons(): Collection
    {
        return $this->userLessons;
    }

    public function addUserLesson(UserLesson $userLesson): self
    {
        if (!$this->userLessons->contains($userLesson)) {
            $this->userLessons[] = $userLesson;
            $userLesson->setUser($this);
        }

        return $this;
    }

    public function removeUserLesson(UserLesson $userLesson): self
    {
        if ($this->userLessons->contains($userLesson)) {
            $this->userLessons->removeElement($userLesson);
            // set the owning side to null (unless already changed)
            if ($userLesson->getUser() === $this) {
                $userLesson->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Order[]
     */
    public function getPointsShopOrders(): Collection
    {
        return $this->pointsShopOrders;
    }

    public function addPointsShopOrder(Order $pointsShopOrder): self
    {
        if (!$this->pointsShopOrders->contains($pointsShopOrder)) {
            $this->pointsShopOrders[] = $pointsShopOrder;
            $pointsShopOrder->setUser($this);
        }

        return $this;
    }

    public function removePointsShopOrder(Order $pointsShopOrder): self
    {
        if ($this->pointsShopOrders->contains($pointsShopOrder)) {
            $this->pointsShopOrders->removeElement($pointsShopOrder);
            // set the owning side to null (unless already changed)
            if ($pointsShopOrder->getUser() === $this) {
                $pointsShopOrder->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserGroupUser[]
     */
    public function getGroupRelations(): Collection
    {
        return $this->groupRelations;
    }

    public function addGroupRelation(UserGroupUser $groupRelation): self
    {
        if (!$this->groupRelations->contains($groupRelation)) {
            $this->groupRelations[] = $groupRelation;
            $groupRelation->setUser($this);
        }

        return $this;
    }

    public function removeGroupRelation(UserGroupUser $groupRelation): self
    {
        if ($this->groupRelations->contains($groupRelation)) {
            $groupRelation->setDeleted(true);
        }

        return $this;
    }

    /**
     * @return Collection|ReportMSG[]
     */
    public function getReportsMSG(): Collection
    {
        return $this->reportsMSG;
    }

    public function addReportsMSG(ReportMSG $reportsMSG): self
    {
        if (!$this->reportsMSG->contains($reportsMSG)) {
            $this->reportsMSG[] = $reportsMSG;
            $reportsMSG->setUser($this);
        }

        return $this;
    }

    public function removeReportsMSG(ReportMSG $reportsMSG): self
    {
        if ($this->reportsMSG->contains($reportsMSG)) {
            $this->reportsMSG->removeElement($reportsMSG);
            // set the owning side to null (unless already changed)
            if ($reportsMSG->getUser() === $this) {
                $reportsMSG->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserCourseTariff[]
     */
    public function getUserCourseTariffs(): Collection
    {
        return $this->userCourseTariffs;
    }

    public function addUserCourseTariff(UserCourseTariff $userCourseTariff): self
    {
        if (!$this->userCourseTariffs->contains($userCourseTariff)) {
            $this->userCourseTariffs[] = $userCourseTariff;
            $userCourseTariff->setManager($this);
        }

        return $this;
    }

    public function removeUserCourseTariff(UserCourseTariff $userCourseTariff): self
    {
        if ($this->userCourseTariffs->contains($userCourseTariff)) {
            $this->userCourseTariffs->removeElement($userCourseTariff);
            // set the owning side to null (unless already changed)
            if ($userCourseTariff->getManager() === $this) {
                $userCourseTariff->setManager(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserCourseTariff[]
     */
    public function getManagerCourseTariffs(): Collection
    {
        return $this->managerCourseTariffs;
    }

    public function addManagerCourseTariff(UserCourseTariff $managerCourseTariffs): self
    {
        if (!$this->managerCourseTariffs->contains($managerCourseTariffs)) {
            $this->managerCourseTariffs[] = $managerCourseTariffs;
            $managerCourseTariffs->setManager($this);
        }

        return $this;
    }

    public function removeManagerCourseTariff(UserCourseTariff $managerCourseTariffs): self
    {
        if ($this->managerCourseTariffs->contains($managerCourseTariffs)) {
            $this->managerCourseTariffs->removeElement($managerCourseTariffs);
            // set the owning side to null (unless already changed)
            if ($managerCourseTariffs->getManager() === $this) {
                $managerCourseTariffs->setManager(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserCourseTariff[]
     */
    public function getTrackerCourseTariffs(): Collection
    {
        return $this->trackerCourseTariffs;
    }

    public function addTrackerCourseTariff(UserCourseTariff $trackerCourseTariffs): self
    {
        if (!$this->trackerCourseTariffs->contains($trackerCourseTariffs)) {
            $this->trackerCourseTariffs[] = $trackerCourseTariffs;
            $trackerCourseTariffs->setManager($this);
        }

        return $this;
    }

    public function removeTrackerCourseTariff(UserCourseTariff $trackerCourseTariffs): self
    {
        if ($this->trackerCourseTariffs->contains($trackerCourseTariffs)) {
            $this->trackerCourseTariffs->removeElement($trackerCourseTariffs);
            // set the owning side to null (unless already changed)
            if ($trackerCourseTariffs->getManager() === $this) {
                $trackerCourseTariffs->setManager(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserCourseTariff[]
     */
    public function getMentorCourseTariffs(): Collection
    {
        return $this->mentorCourseTariffs;
    }

    public function addMentorCourseTariff(UserCourseTariff $mentorCourseTariff): self
    {
        if (!$this->mentorCourseTariffs->contains($mentorCourseTariff)) {
            $this->mentorCourseTariffs[] = $mentorCourseTariff;
            $mentorCourseTariff->setMentor($this);
        }

        return $this;
    }

    public function removeMentorCourseTariff(UserCourseTariff $mentorCourseTariff): self
    {
        if ($this->mentorCourseTariffs->contains($mentorCourseTariff)) {
            $this->mentorCourseTariffs->removeElement($mentorCourseTariff);
            // set the owning side to null (unless already changed)
            if ($mentorCourseTariff->getMentor() === $this) {
                $mentorCourseTariff->setMentor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|AnswerComment[]
     */
    public function getAnswerComments(): Collection
    {
        return $this->answerComments;
    }

    public function addAnswerComment(AnswerComment $answerComment): self
    {
        if (!$this->answerComments->contains($answerComment)) {
            $this->answerComments[] = $answerComment;
            $answerComment->setCommentator($this);
        }

        return $this;
    }

    public function removeAnswerComment(AnswerComment $answerComment): self
    {
        if ($this->answerComments->contains($answerComment)) {
            $this->answerComments->removeElement($answerComment);
            // set the owning side to null (unless already changed)
            if ($answerComment->getCommentator() === $this) {
                $answerComment->setCommentator(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserCertificates[]
     */
    public function getUserCertificates(): Collection
    {
        return $this->userCertificates;
    }

    public function addUserCertificate(UserCertificates $userCertificate): self
    {
        if (!$this->userCertificates->contains($userCertificate)) {
            $this->userCertificates[] = $userCertificate;
            $userCertificate->setUser($this);
        }

        return $this;
    }

    public function removeUserCertificate(UserCertificates $userCertificate): self
    {
        if ($this->userCertificates->contains($userCertificate)) {
            $this->userCertificates->removeElement($userCertificate);
            // set the owning side to null (unless already changed)
            if ($userCertificate->getUser() === $this) {
                $userCertificate->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SmartSenderUser[]
     */
    public function getSmartSenderUsers(): Collection
    {
        return $this->smartSenderUsers;
    }

    public function addSmartSenderUser(SmartSenderUser $smartSenderUser): self
    {
        if (!$this->smartSenderUsers->contains($smartSenderUser)) {
            $this->smartSenderUsers[] = $smartSenderUser;
            $smartSenderUser->setUser($this);
        }

        return $this;
    }

    public function removeSmartSenderUser(SmartSenderUser $smartSenderUser): self
    {
        if ($this->smartSenderUsers->contains($smartSenderUser)) {
            $this->smartSenderUsers->removeElement($smartSenderUser);
            // set the owning side to null (unless already changed)
            if ($smartSenderUser->getUser() === $this) {
                $smartSenderUser->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserAnswer[]
     */
    public function getQuestionnaireUserAnswers(): Collection
    {
        return $this->questionnaireUserAnswers;
    }

    public function addQuestionnaireUserAnswer(UserAnswer $questionnaireUserAnswer): self
    {
        if (!$this->questionnaireUserAnswers->contains($questionnaireUserAnswer)) {
            $this->questionnaireUserAnswers[] = $questionnaireUserAnswer;
            $questionnaireUserAnswer->setUser($this);
        }

        return $this;
    }

    public function removeQuestionnaireUserAnswer(UserAnswer $questionnaireUserAnswer): self
    {
        if ($this->questionnaireUserAnswers->contains($questionnaireUserAnswer)) {
            $this->questionnaireUserAnswers->removeElement($questionnaireUserAnswer);
            // set the owning side to null (unless already changed)
            if ($questionnaireUserAnswer->getUser() === $this) {
                $questionnaireUserAnswer->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Team[]
     */
    public function getTeams(): Collection
    {
        return $this->teams;
    }

    public function addTeam(Team $team): self
    {
        if (!$this->teams->contains($team)) {
            $this->teams[] = $team;
            $team->addUser($this);
        }

        return $this;
    }

    public function removeTeam(Team $team): self
    {
        if ($this->teams->contains($team)) {
            $this->teams->removeElement($team);
            $team->removeUser($this);
        }

        return $this;
    }

    /**
     * @return Collection|Team[]
     */
    public function getTeamsCreated(): Collection
    {
        return $this->teamsCreated;
    }

    public function addTeamsCreated(Team $teamsCreated): self
    {
        if (!$this->teamsCreated->contains($teamsCreated)) {
            $this->teamsCreated[] = $teamsCreated;
            $teamsCreated->setCreatedBy($this);
        }

        return $this;
    }

    public function removeTeamsCreated(Team $teamsCreated): self
    {
        if ($this->teamsCreated->removeElement($teamsCreated)) {
            // set the owning side to null (unless already changed)
            if ($teamsCreated->getCreatedBy() === $this) {
                $teamsCreated->setCreatedBy(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Target[]
     */
    public function getTractionTargets(): Collection
    {
        return $this->tractionTargets;
    }

    public function addTractionTarget(Target $target): self
    {
        if (!$this->tractionTargets->contains($target)) {
            $this->tractionTargets[] = $target;
        }

        return $this;
    }

    public function removeTractionTarget(Target $target): self
    {
        if ($this->tractionTargets->contains($target)) {
            $this->tractionTargets->removeElement($target);
        }

        return $this;
    }

    /**
     * @return Collection|UserNiche[]
     */
    public function getNiches(): Collection
    {
        return $this->niches;
    }

    public function addUserNiche(UserNiche $niche): self
    {
        if (!$this->niches->contains($niche)) {
            $this->niches[] = $niche;
            $niche->setUser($this);
        }

        return $this;
    }

    public function removeNiche(UserNiche $niche): self
    {
        $this->niches->removeElement($niche);

        return $this;
    }

    /**
     * @return Collection|MarathonInvestmentConfiguration[]
     */
    public function getMarathonConfigurations(): Collection
    {
        return $this->marathonConfigurations;
    }

    public function addMarathonConfiguration(MarathonInvestmentConfiguration $marathonConfiguration): self
    {
        if (!$this->marathonConfigurations->contains($marathonConfiguration)) {
            $this->marathonConfigurations[] = $marathonConfiguration;
            $marathonConfiguration->setUser($this);
        }

        return $this;
    }

    public function removeMarathonConfiguration(MarathonInvestmentConfiguration $marathonConfiguration): self
    {
        $this->marathonConfigurations->removeElement($marathonConfiguration);

        return $this;
    }

    /**
     * @return Collection|MarathonInvestment[]
     */
    public function getMarathonInvestments(): Collection
    {
        return $this->marathonInvestments;
    }

    public function getTotalAmount()
    {
        $amount = 0;
        foreach ($this->marathonInvestments as $record) {
            $amount += $record->getAmount();
        }

        return $amount;
    }

    public function getTotalPlanned()
    {
        $planned = 0;
        foreach ($this->marathonInvestments as $record) {
            $planned += $record->getPlanned();
        }

        return $planned;
    }

    public function getTotalPassed()
    {
        $passed = 0;
        foreach ($this->marathonInvestments as $record) {
            $passed += $record->getPassed();
        }

        return $passed;
    }

    /**
     * @return Collection|Promocode[]
     */
    public function getPromocodes(): Collection
    {
        return $this->promocodes;
    }

    public function addPromocode(Promocode $promocode): self
    {
        if (!$this->promocodes->contains($promocode)) {
            $this->promocodes[] = $promocode;
            $promocode->setUser($this);
        }

        return $this;
    }

    public function removePromocode(Promocode $promocode): self
    {
        if ($this->promocodes->removeElement($promocode)) {
            // set the owning side to null (unless already changed)
            if ($promocode->getUser() === $this) {
                $promocode->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|AttemptToGetPromocode[]
     */
    public function getAttemptToGetPromocodes(): Collection
    {
        return $this->attemptToGetPromocodes;
    }

    public function addAttemptToGetPromocode(AttemptToGetPromocode $attemptToGetPromocode): self
    {
        if (!$this->attemptToGetPromocodes->contains($attemptToGetPromocode)) {
            $this->attemptToGetPromocodes[] = $attemptToGetPromocode;
            $attemptToGetPromocode->setUser($this);
        }

        return $this;
    }

    public function removeAttemptToGetPromocode(AttemptToGetPromocode $attemptToGetPromocode): self
    {
        if ($this->attemptToGetPromocodes->removeElement($attemptToGetPromocode)) {
            // set the owning side to null (unless already changed)
            if ($attemptToGetPromocode->getUser() === $this) {
                $attemptToGetPromocode->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Value[]
     */
    public function getTractionValues(): Collection
    {
        return $this->tractionValues;
    }

    public function addTractionValue(Value $tractionValue): self
    {
        if (!$this->tractionValues->contains($tractionValue)) {
            $this->tractionValues[] = $tractionValue;
        }

        return $this;
    }

    public function removeTractionValue(Value $tractionValue): self
    {
        $this->tractionValues->removeElement($tractionValue);

        return $this;
    }

    /**
     * @return integer[]
     * @Groups({"traction"})
     * @SerializedName("tractionSums")
     */
    public function getTractionValuesSumByTypes(): array
    {
        $sums = [];
        foreach ($this->getTractionValues() as $tractionValue) {
            if (!isset($sums[$tractionValue->getTypeId()])) {
                $sums[$tractionValue->getTypeId()] = 0;
            }
            $sums[$tractionValue->getTypeId()] += $tractionValue->getValue();
        }

        return $sums;
    }

    /**
     * @return Faculty[]
     * @Groups({"traction:user"})
     * @SerializedName("faculties")
     */
    public function getFaculties(): array
    {
        $faculties = [];
        foreach ($this->getGroupRelations() as $groupRelation) {
            if ($groupRelation->getFaculty() && !$groupRelation->isDeleted()) {
                $faculties[] = $groupRelation->getFaculty();
            }
        }

        return $faculties;
    }

    /**
     * @return Collection|BusinessPhoto[]
     */
    public function getBusinessPhotos(): Collection
    {
        return $this->businessPhotos;
    }

    public function addBusinessPhoto(BusinessPhoto $businessPhoto): self
    {
        if (!$this->businessPhotos->contains($businessPhoto)) {
            $this->businessPhotos[] = $businessPhoto;
            $businessPhoto->setOwner($this);
        }

        return $this;
    }

    public function removeBusinessPhoto(BusinessPhoto $businessPhoto): self
    {
        if ($this->businessPhotos->removeElement($businessPhoto)) {
            // set the owning side to null (unless already changed)
            if ($businessPhoto->getOwner() === $this) {
                $businessPhoto->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCompany(): ?string
    {
        return $this->company;
    }

    /**
     * @param string $company
     */
    public function setCompany(string $company): void
    {
        $this->company = $company;
    }

    /**
     * @return Collection|BusinessNiche[]
     */
    public function getBusinessNiches(): Collection
    {
        return $this->businessNiches;
    }

    public function addBusinessNiche(BusinessNiche $businessNiche): self
    {
        if (!$this->businessNiches->contains($businessNiche)) {
            $this->businessNiches[] = $businessNiche;
            $businessNiche->setUser($this);
        }

        return $this;
    }

    public function removeBusinessNiche(BusinessNiche $businessNiche): self
    {
        if ($this->businessNiches->removeElement($businessNiche)) {
            // set the owning side to null (unless already changed)
            if ($businessNiche->getUser() === $this) {
                $businessNiche->setUser(null);
            }
        }

        return $this;
    }

    public function getLocation(): Point
    {
        return $this->location;
    }


    public function setLocation(Point $location): void
    {
        $this->location = $location;
    }

    public function getLongitude(): ?float
    {
        return optional($this->location)->getX();
    }

    public function setLongitude(float $longitude)
    {
        $this->longitude = $longitude;
    }

    public function getLatitude(): ?float
    {
        return optional($this->location)->getY();
    }

    public function setLatitude(float $latitude)
    {
        $this->latitude = $latitude;
    }

    public function isVerified(): bool
    {
        return $this->verified;
    }

    public function setVerified(bool $verified): void
    {
        $this->verified = $verified;
    }

}

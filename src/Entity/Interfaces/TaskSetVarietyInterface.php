<?php

namespace App\Entity\Interfaces;

interface TaskSetVarietyInterface
{
    /**
     * @return int|null
     */
    public function getId(): ?int;
}

<?php

namespace App\Entity\Interfaces;

interface LessonContentBlockInterface
{
    /**
     * @return int|null
     */
    public function getId(): ?int;
}

<?php

namespace App\Entity\Interfaces;

interface FileInterface
{
    public function setPublicPath(string $publicPath);
    public function getFilename();
}

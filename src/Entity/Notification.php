<?php

namespace App\Entity;

use App\Entity\Task\AnswerTaskType1;
use App\Repository\NotificationRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=NotificationRepository::class)
 * @ORM\Table("notification")
 */
class Notification
{
    use TimestampableEntity;

    public const STATUS_NEW = 1;
    public const STATUS_READ = 2;

    public const ICON_DEFAULT = 0;
    public const ICON_TIME = 1;
    public const ICON_DDL = 2;
    public const ICON_HOMEWORK = 3;
    public const ICON_POINTS = 4;

    public const LINK_TYPE_NO_LINK = 0;
    public const LINK_TYPE_SIMPLE_LINK = 1;
    public const LINK_TYPE_LESSON = 2;
    public const LINK_TYPE_PRODUCT = 3;

    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=250)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=2000)
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="icon_id", type="integer")
     */
    private $iconId;

    /**
     * @var int
     *
     * @ORM\Column(name="status_id", type="integer")
     */
    private $statusId;

    /**
     * @var int
     *
     * @ORM\Column(name="link_type_id", type="integer", nullable=true)
     */
    private $linkTypeId;

    /**
     * @var string
     *
     * @ORM\Column(name="link_content", type="string", length=1000, nullable=true)
     */
    private $linkContent;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    public function __construct()
    {
        $this->title = '';
        $this->description = '';
        $this->statusId = self::STATUS_NEW;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param  string  $description
     * @return $this
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getIconId(): ?int
    {
        return $this->iconId;
    }

    /**
     * @param  int  $iconId
     * @return $this
     */
    public function setIconId(int $iconId): self
    {
        $this->iconId = $iconId;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatusId(): int
    {
        return $this->statusId;
    }

    /**
     * @param  int  $statusId
     * @return $this
     */
    public function setStatusId(int $statusId): self
    {
        $this->statusId = $statusId;

        return $this;
    }

    /**
     * @return bool
     */
    public function isRead(): bool
    {
        return $this->statusId === self::STATUS_READ;
    }

    /**
     * @return int
     */
    public function getLinkTypeId(): int
    {
        return $this->linkTypeId;
    }

    /**
     * @param  int  $linkTypeId
     * @return Notification
     */
    public function setLinkTypeId(int $linkTypeId): self
    {
        $this->linkTypeId = $linkTypeId;

        return $this;
    }

    /**
     * @return string
     */
    public function getLinkContent(): ?string
    {
        return $this->linkContent;
    }

    /**
     * @param  string|null  $linkContent
     * @return Notification
     */
    public function setLinkContent(?string $linkContent): self
    {
        $this->linkContent = $linkContent;

        return $this;
    }

    public function getUser(): ?UserInterface
    {
        return $this->user;
    }

    public function setUser(?UserInterface $user): self
    {
        $this->user = $user;

        return $this;
    }

}

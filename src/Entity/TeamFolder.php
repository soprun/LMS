<?php

namespace App\Entity;

use App\Repository\TeamFolderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TeamFolderRepository::class)
 */
class TeamFolder
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity=UserGroup::class, inversedBy="teamFolders")
     */
    private $userGroups;

    /**
     * @ORM\OneToMany(targetEntity=Team::class, mappedBy="folder")
     */
    private $teams;

    /**
     * @ORM\OneToMany(targetEntity=Faculty::class, mappedBy="teamFolder")
     */
    private $faculties;

    /**
     * @ORM\ManyToOne(targetEntity=CourseStream::class, inversedBy="teamFolders")
     */
    private $courseStream;

    public function __construct()
    {
        $this->userGroups = new ArrayCollection();
        $this->teams = new ArrayCollection();
        $this->faculties = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|UserGroup[]
     */
    public function getUserGroups(): Collection
    {
        return $this->userGroups;
    }

    public function addUserGroup(UserGroup $userGroup): self
    {
        if (!$this->userGroups->contains($userGroup)) {
            $this->userGroups[] = $userGroup;
        }

        return $this;
    }

    public function removeUserGroup(UserGroup $userGroup): self
    {
        $this->userGroups->removeElement($userGroup);

        return $this;
    }

    /**
     * @return Collection|Team[]
     */
    public function getTeams(): Collection
    {
        return $this->teams;
    }

    public function addTeam(Team $team): self
    {
        if (!$this->teams->contains($team)) {
            $this->teams[] = $team;
            $team->setFolder($this);
        }

        return $this;
    }

    public function removeTeam(Team $team): self
    {
        if ($this->teams->removeElement($team)) {
            // set the owning side to null (unless already changed)
            if ($team->getFolder() === $this) {
                $team->setFolder(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Faculty[]
     */
    public function getFaculties(): Collection
    {
        return $this->faculties;
    }

    public function addFaculty(Faculty $faculty): self
    {
        if (!$this->faculties->contains($faculty)) {
            $this->faculties[] = $faculty;
            $faculty->setTeamFolder($this);
        }

        return $this;
    }

    public function removeFaculty(Faculty $faculty): self
    {
        if ($this->faculties->removeElement($faculty)) {
            // set the owning side to null (unless already changed)
            if ($faculty->getTeamFolder() === $this) {
                $faculty->setTeamFolder(null);
            }
        }

        return $this;
    }

    public function getCourseStream(): ?CourseStream
    {
        return $this->courseStream;
    }

    public function setCourseStream(?CourseStream $courseStream): self
    {
        $this->courseStream = $courseStream;

        return $this;
    }

}

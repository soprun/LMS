<?php

namespace App\Entity;

use App\Entity\Courses\Lesson;
use App\Repository\UserLessonRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass=UserLessonRepository::class)
 */
class UserLesson
{
    use TimestampableEntity;

    public const STATE_NOT_VIEWED = 1;
    public const STATE_VIEWED = 2;
    public const STATE_ANSWER_SENT = 3;
    public const STATE_ANSWER_CHECKED = 4;

    /**
     * @ORM\Id()
     *
     * @ORM\ManyToOne(targetEntity=Lesson::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $lesson;

    /**
     * @ORM\Id()
     *
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="userLessons")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="integer")
     */
    private $state;

    public function __construct()
    {
        $this->state = UserLesson::STATE_NOT_VIEWED;
    }

    public function getLesson(): ?Lesson
    {
        return $this->lesson;
    }

    public function setLesson(?Lesson $lesson): self
    {
        $this->lesson = $lesson;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getState(): ?int
    {
        return $this->state;
    }

    public function setState(int $state): self
    {
        $this->state = $state;

        return $this;
    }

}

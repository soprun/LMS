<?php

namespace App\Entity;

use App\Entity\SmartSender\SmartSenderBot;
use App\Entity\Traction\ValueType;
use App\Repository\AbstractCourseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass=AbstractCourseRepository::class)
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 */
class AbstractCourse
{
    use SoftDeleteableEntity;

    public const SLUG_SPEED = 'speed';
    public const SLUG_SPEED_CLUB = 'speed_club';
    public const SLUG_HUNDRED = 'hundred';
    public const SLUG_MARATHON_INVESTMENT = 'mi';
    public const SLUG_MSA = 'msa';
    public const SLUG_FAST_CASH = 'fastcash';

    public const TITLES = [
        self::SLUG_SPEED => 'Скорость',
        self::SLUG_SPEED_CLUB => 'Скорость Клуб',
        self::SLUG_HUNDRED => 'Сотка',
        self::SLUG_MARATHON_INVESTMENT => 'МИ',
        self::SLUG_MSA => 'МсА',
        self::SLUG_FAST_CASH => 'Быстрые деньги',
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("abstractCourse:info", "courseStream:list")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"traction", "abstractCourse:info", "courseStream:list"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"traction", "abstractCourse:info"})
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $certSerial;

    /**
     * @ORM\OneToMany(targetEntity=CourseStream::class, mappedBy="abstractCourse")
     */
    private $streams;

    /**
     * @ORM\OneToMany(targetEntity=SmartSenderBot::class, mappedBy="abstractCourse")
     */
    private $smartSenderBots;

    /**
     * @ORM\ManyToMany(targetEntity=ValueType::class, mappedBy="abstractCourses")
     *
     * @Groups({"traction"})
     */
    private $tractionValueTypes;

    /**
     * Относительная строка времени в формате, поддерживаемом конструктором DateTime
     * https://www.php.net/manual/ru/datetime.formats.relative.php
     *
     * @ORM\Column(type="string", length=10, options={"default":"1 month"})
     * @Groups({"abstractCourse:info"})
     */
    private $settlementPeriod;

    public function __construct()
    {
        $this->streams = new ArrayCollection();
        $this->smartSenderBots = new ArrayCollection();
        $this->tractionValueTypes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection|CourseStream[]
     */
    public function getStreams(): Collection
    {
        return $this->streams;
    }

    public function addStream(CourseStream $stream): self
    {
        if (!$this->streams->contains($stream)) {
            $this->streams[] = $stream;
            $stream->setAbstractCourse($this);
        }

        return $this;
    }

    public function removeStream(CourseStream $stream): self
    {
        if ($this->streams->removeElement($stream)) {
            // set the owning side to null (unless already changed)
            if ($stream->getAbstractCourse() === $this) {
                $stream->setAbstractCourse(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SmartSenderBot[]
     */
    public function getSmartSenderBots(): Collection
    {
        return $this->smartSenderBots;
    }

    public function addSmartSenderBot(SmartSenderBot $smartSenderBot): self
    {
        if (!$this->smartSenderBots->contains($smartSenderBot)) {
            $this->smartSenderBots[] = $smartSenderBot;
            $smartSenderBot->setAbstractCourse($this);
        }

        return $this;
    }

    public function removeSmartSenderBot(SmartSenderBot $smartSenderBot): self
    {
        if ($this->smartSenderBots->removeElement($smartSenderBot)) {
            // set the owning side to null (unless already changed)
            if ($smartSenderBot->getAbstractCourse() === $this) {
                $smartSenderBot->setAbstractCourse(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ValueType[]
     */
    public function getTractionValueTypes(): Collection
    {
        return $this->tractionValueTypes;
    }

    public function addTractionValueType(ValueType $tractionValueType): self
    {
        if (!$this->tractionValueTypes->contains($tractionValueType)) {
            $this->tractionValueTypes[] = $tractionValueType;
            $tractionValueType->addAbstractCourse($this);
        }

        return $this;
    }

    public function removeTractionValueType(ValueType $tractionValueType): self
    {
        if ($this->tractionValueTypes->removeElement($tractionValueType)) {
            $tractionValueType->removeAbstractCourse($this);
        }

        return $this;
    }

    public function getSettlementPeriod(): ?string
    {
        return $this->settlementPeriod;
    }

    public function setSettlementPeriod(string $settlementPeriod): self
    {
        $this->settlementPeriod = $settlementPeriod;

        return $this;
    }

    public function getCertSerial(): ?string
    {
        return $this->certSerial;
    }

    public function setCertSerial($certSerial): self
    {
        $this->certSerial = $certSerial;

        return $this;
    }

}

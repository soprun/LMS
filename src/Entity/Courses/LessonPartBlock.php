<?php

namespace App\Entity\Courses;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Courses\LessonPartBlockRepository")
 *
 * @ORM\Table("courses_lesson_part_block")
 */
class LessonPartBlock
{
    use TimestampableEntity;

    // типы блоков
    public const CONTENT_TYPE = 1;           // контент
    public const TASK_WITH_CHECK_TYPE = 2;   // Задания с проверкой
    public const TASK_NPS_TYPE = 3;          // Сбор NPS
    public const TEST_TYPE = 4;              // Тест
    public const TASK_WITH_CHOICES_TYPE = 5; // Задания с вводом правильного ответа из списка
    // вариации блоков типа self::CONTENT_TYPE
    public const HEADER_VARIETY = 1;
    public const TEXT_VARIETY = 2;
    public const VIDEO_VARIETY = 3;
    public const IMAGE_VARIETY = 4;
    public const AUDIO_VARIETY = 5;
    public const FILE_VARIETY = 6;
    public const BUTTON_VARIETY = 7;
    // вариации блоков типа self::TASK_WITH_CHECK_TYPE
    public const TASK_CHECK_VARIETY = 1;
    // вариации блоков типа self::TASK_CHECK_VARIETY
    public const TASK_TYPE_1_VARIETY = 1;
    // вариации блоков типа self::TASK_NPS_TYPE
    public const NPS_VARIETY = 1;
    // вариации блоков типа self::QUIZ_TYPE
    public const TEST_TASK_VARIETY = 2;
    // вариации блоков типа self::TASK_WITH_CHOICES_TYPE
    public const TEST_WITH_NUMBERS_VARIETY = 2;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Courses\LessonPart", inversedBy="lessonPartBlocks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $part;

    /**
     * @ORM\Column(type="smallint")
     */
    private $type;

    /**
     * @ORM\Column(type="smallint")
     */
    private $variety;

    /**
     * @ORM\Column(type="integer")
     */
    private $blockId;

    /**
     * @ORM\Column(type="smallint")
     */
    private $positionInPart;

    /**
     * @ORM\Column(type="boolean", options={"default": 0})
     */
    private $deleted = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPart(): ?LessonPart
    {
        return $this->part;
    }

    public function setPart(?LessonPart $part): self
    {
        $this->part = $part;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getVariety(): ?int
    {
        return $this->variety;
    }

    public function setVariety(int $variety): self
    {
        $this->variety = $variety;

        return $this;
    }

    public function getBlockId(): ?int
    {
        return $this->blockId;
    }

    public function setBlockId(int $blockId): self
    {
        $this->blockId = $blockId;

        return $this;
    }

    public function getPositionInPart(): ?int
    {
        return $this->positionInPart;
    }

    public function setPositionInPart(int $positionInPart): self
    {
        $this->positionInPart = $positionInPart;

        return $this;
    }

    public function isDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(?bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

}

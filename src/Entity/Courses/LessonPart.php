<?php

namespace App\Entity\Courses;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Courses\LessonPartRepository")
 *
 * @ORM\Table("courses_lesson_part")
 */
class LessonPart
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="smallint")
     */
    private $positionInLesson;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Courses\Lesson", inversedBy="lessonParts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $lesson;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Courses\LessonPartBlock", mappedBy="part")
     */
    private $lessonPartBlocks;

    public function __construct()
    {
        $this->lessonPartBlocks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPositionInLesson(): ?int
    {
        return $this->positionInLesson;
    }

    public function setPositionInLesson(int $positionInLesson): self
    {
        $this->positionInLesson = $positionInLesson;

        return $this;
    }

    public function getLesson(): ?Lesson
    {
        return $this->lesson;
    }

    public function setLesson(?Lesson $lesson): self
    {
        $this->lesson = $lesson;

        return $this;
    }

    /**
     * @return Collection|LessonPartBlock[]
     */
    public function getLessonPartBlocks(): Collection
    {
        return $this->lessonPartBlocks;
    }

    public function addLessonPartBlock(LessonPartBlock $lessonPartBlock): self
    {
        if (!$this->lessonPartBlocks->contains($lessonPartBlock)) {
            $this->lessonPartBlocks[] = $lessonPartBlock;
            $lessonPartBlock->setPart($this);
        }

        return $this;
    }

    /**
     * @param LessonPartBlock[] $lessonPartBlocks
     *
     * @return self
     */
    public function addLessonPartBlocks(array $lessonPartBlocks): self
    {
        foreach ($lessonPartBlocks as $lessonPartBlock) {
            $this->addLessonPartBlock($lessonPartBlock);
        }

        return $this;
    }

    public function removeLessonPartBlock(LessonPartBlock $lessonPartBlock): self
    {
        if ($this->lessonPartBlocks->contains($lessonPartBlock)) {
            $this->lessonPartBlocks->removeElement($lessonPartBlock);
            // set the owning side to null (unless already changed)
            if ($lessonPartBlock->getPart() === $this) {
                $lessonPartBlock->setPart(null);
            }
        }

        return $this;
    }

}

<?php

namespace App\Entity\Courses;

use App\Entity\BaseBlock\BaseFiles;
use App\Entity\UserCertificates;
use App\Entity\UserGroup;
use App\Entity\UserTariff;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Courses\CourseRepository")
 * @ORM\EntityListeners({"App\EntityListener\DeletingEntityListener"})
 *
 * @ORM\Table("courses_course")
 */
class Course
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @Groups("course:info")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups("course:info")
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Courses\CourseFolder", inversedBy="courses")
     */
    private $folder;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     *
     * @Groups("course:info")
     */
    private $description;

    /**
     * @ORM\OneToOne(
     *     targetEntity="App\Entity\Courses\CourseConfiguration",
     *     mappedBy="course",
     *     cascade={"persist", "remove"}
     * )
     */
    private $courseConfiguration;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Courses\Module", mappedBy="course")
     */
    private $modules;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Courses\Lesson", mappedBy="course")
     */
    private $lessons;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\UserGroup", mappedBy="courses", cascade={"persist", "remove"})
     */
    private $userGroups;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BaseBlock\BaseFiles")
     */
    private $imgFile;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BaseBlock\BaseFiles")
     */
    private $coverImgFile;

    /**
     * @ORM\Column(type="boolean", options={"default": 0})
     */
    private $deleted = false;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserTariff", mappedBy="course", orphanRemoval=true)
     */
    private $userTariffs;

    /**
     * @ORM\OneToMany(targetEntity=UserCertificates::class, mappedBy="course")
     */
    private $userCertificates;


    public function __construct()
    {
        $this->modules = new ArrayCollection();
        $this->lessons = new ArrayCollection();
        $this->userGroups = new ArrayCollection();
        $this->userTariffs = new ArrayCollection();
        $this->userCertificates = new ArrayCollection();
    }

    public function __toString()
    {
        return "#{$this->getId()} - {$this->getName()}";
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFolder(): ?CourseFolder
    {
        return $this->folder;
    }

    public function setFolder(?CourseFolder $folder): self
    {
        $this->folder = $folder;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCourseConfiguration(): ?CourseConfiguration
    {
        return $this->courseConfiguration;
    }

    public function setCourseConfiguration(CourseConfiguration $courseConfiguration): self
    {
        $this->courseConfiguration = $courseConfiguration;

        // set the owning side of the relation if necessary
        if ($courseConfiguration->getCourse() !== $this) {
            $courseConfiguration->setCourse($this);
        }

        return $this;
    }

    /**
     * @return Collection|Module[]
     */
    public function getModules(): Collection
    {
        return $this->modules;
    }

    public function addModule(Module $module): self
    {
        if (!$this->modules->contains($module)) {
            $this->modules[] = $module;
            $module->setCourse($this);
        }

        return $this;
    }

    /**
     * @param Module[] $modules
     *
     * @return self
     */
    public function addModules(array $modules): self
    {
        foreach ($modules as $module) {
            $this->addModule($module);
        }

        return $this;
    }

    public function removeModule(Module $module): self
    {
        if ($this->modules->contains($module)) {
            $this->modules->removeElement($module);
            // set the owning side to null (unless already changed)
            if ($module->getCourse() === $this) {
                $module->setCourse(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Lesson[]
     */
    public function getLessons(): Collection
    {
        return $this->lessons;
    }

    public function addLesson(Lesson $lesson): self
    {
        if (!$this->lessons->contains($lesson)) {
            $this->lessons[] = $lesson;
            $lesson->setCourse($this);
        }

        return $this;
    }

    /**
     * @param Lesson[] $lessons
     *
     * @return self
     */
    public function addLessons(array $lessons): self
    {
        foreach ($lessons as $lesson) {
            $this->addLesson($lesson);
        }

        return $this;
    }

    public function removeLesson(Lesson $lesson): self
    {
        if ($this->lessons->contains($lesson)) {
            $this->lessons->removeElement($lesson);
            // set the owning side to null (unless already changed)
            if ($lesson->getCourse() === $this) {
                $lesson->setCourse(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserGroup[]
     */
    public function getUserGroups(): Collection
    {
        return $this->userGroups;
    }

    public function getUserGroupIds(): array
    {
        return $this->userGroups->map(
            function (UserGroup $e) {
                return $e->getId();
            }
        )->toArray();
    }

    public function clearUserGroups(): self
    {
        /** @var UserGroup $userGroup */
        foreach ($this->userGroups as $userGroup) {
            $this->removeUserGroup($userGroup);
        }

        return $this;
    }

    public function addUserGroup(UserGroup $userGroup): self
    {
        if (!$this->userGroups->contains($userGroup)) {
            $this->userGroups[] = $userGroup;
            $userGroup->addCourse($this);
        }

        return $this;
    }

    public function addUserGroups($userGroups): self
    {
        foreach ($userGroups as $userGroup) {
            $this->addUserGroup($userGroup);
        }

        return $this;
    }

    public function removeUserGroup(UserGroup $userGroup): self
    {
        if ($this->userGroups->contains($userGroup)) {
            $this->userGroups->removeElement($userGroup);
            $userGroup->removeCourse($this);
        }

        return $this;
    }

    public function getImgFile(): ?BaseFiles
    {
        return $this->imgFile;
    }

    public function setImgFile(?BaseFiles $imgFile): self
    {
        $this->imgFile = $imgFile;

        return $this;
    }

    public function getCoverImgFile(): ?BaseFiles
    {
        return $this->coverImgFile;
    }

    public function setCoverImgFile(?BaseFiles $coverImgFile): self
    {
        $this->coverImgFile = $coverImgFile;

        return $this;
    }

    public function isDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * @return Collection|UserTariff[]
     */
    public function getUserTariffs(): Collection
    {
        return $this->userTariffs;
    }

    public function addUserTariff(UserTariff $userTariff): self
    {
        if (!$this->userTariffs->contains($userTariff)) {
            $this->userTariffs[] = $userTariff;
            $userTariff->setCourse($this);
        }

        return $this;
    }

    public function removeUserTariff(UserTariff $userTariff): self
    {
        if ($this->userTariffs->contains($userTariff)) {
            $this->userTariffs->removeElement($userTariff);
            // set the owning side to null (unless already changed)
            if ($userTariff->getCourse() === $this) {
                $userTariff->setCourse(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserCertificates[]
     */
    public function getUserCertificates(): Collection
    {
        return $this->userCertificates;
    }

    public function addUserCertificate(UserCertificates $userCertificate): self
    {
        if (!$this->userCertificates->contains($userCertificate)) {
            $this->userCertificates[] = $userCertificate;
            $userCertificate->setCourse($this);
        }

        return $this;
    }

    public function removeUserCertificate(UserCertificates $userCertificate): self
    {
        if ($this->userCertificates->contains($userCertificate)) {
            $this->userCertificates->removeElement($userCertificate);
            // set the owning side to null (unless already changed)
            if ($userCertificate->getCourse() === $this) {
                $userCertificate->setCourse(null);
            }
        }

        return $this;
    }

}

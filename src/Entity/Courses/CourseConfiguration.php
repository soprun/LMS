<?php

namespace App\Entity\Courses;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Courses\CourseConfigurationRepository")
 *
 * @ORM\Table("courses_course_configutarion")
 */
class CourseConfiguration
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(
     *     targetEntity="App\Entity\Courses\Course",
     *     inversedBy="courseConfiguration",
     *     cascade={"persist", "remove"}
     * )
     * @ORM\JoinColumn(nullable=false)
     */
    private $course;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateOfStart;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateOfFinish;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCourse(): ?Course
    {
        return $this->course;
    }

    public function setCourse(Course $course): self
    {
        $this->course = $course;

        return $this;
    }

    public function getDateOfStart(): ?\DateTimeInterface
    {
        return $this->dateOfStart;
    }

    public function setDateOfStart(?\DateTimeInterface $dateOfStart): self
    {
        $this->dateOfStart = $dateOfStart;

        return $this;
    }

    public function getDateOfFinish(): ?\DateTimeInterface
    {
        return $this->dateOfFinish;
    }

    public function setDateOfFinish(?\DateTimeInterface $dateOfFinish): self
    {
        $this->dateOfFinish = $dateOfFinish;

        return $this;
    }
}

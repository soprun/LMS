<?php

namespace App\Entity\Courses;

use App\Entity\BaseBlock\BaseFiles;
use App\Entity\User;
use App\Entity\UserGroup;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Courses\LessonRepository")
 * @ORM\EntityListeners({
 *     "App\EntityListener\LessonListener",
 *     "App\EntityListener\DeletingEntityListener"
 * })
 * @ORM\Table("courses_lesson")
 */
class Lesson
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @Groups("lesson:info")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups("lesson:info")
     */
    private $name;

    /**
     * @ORM\Column(type="smallint")
     *
     * @Groups("lesson:info")
     *
     * @Gedmo\SortablePosition
     */
    private $position;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Courses\Course", inversedBy="lessons")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Gedmo\SortableGroup
     */
    private $course;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Courses\Module", inversedBy="lessons")
     */
    private $module;

    /**
     * @ORM\OneToOne(
     *     targetEntity="App\Entity\Courses\LessonConfiguration",
     *     mappedBy="lesson",
     *     cascade={"persist", "remove"}
     * )
     */
    private $lessonConfiguration;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     *
     * @Groups("lesson:info")
     */
    private $isStopLesson;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $imgText;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     *
     * @Groups("lesson:info")
     */
    private $isImportant;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     *
     * @Groups("lesson:info")
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Courses\LessonPart", mappedBy="lesson")
     */
    private $lessonParts;

    /**
     * @ORM\Column(type="boolean", options={"default": 0})
     *
     * @Gedmo\SortableGroup
     */
    private $deleted = false;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="lessons")
     * @ORM\JoinTable(name="lesson_speakers")
     */
    private $speakers;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BaseBlock\BaseFiles")
     */
    private $imgFile;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\UserGroup", mappedBy="lessons", cascade={"persist", "remove"})
     */
    private $userGroups;

    public function __construct()
    {
        $this->lessonParts = new ArrayCollection();
        $this->speakers = new ArrayCollection();
        $this->userGroups = new ArrayCollection();
    }

    public function __toString()
    {
        return "#{$this->getId()} - {$this->getName()}";
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCourse(): ?Course
    {
        return $this->course;
    }

    public function setCourse(?Course $course): self
    {
        $this->course = $course;

        return $this;
    }

    public function getModule(): ?Module
    {
        return $this->module;
    }

    public function setModule(?Module $module): self
    {
        $this->module = $module;

        return $this;
    }

    public function getLessonConfiguration(): ?LessonConfiguration
    {
        return $this->lessonConfiguration;
    }

    public function setLessonConfiguration(LessonConfiguration $lessonConfiguration): self
    {
        $this->lessonConfiguration = $lessonConfiguration;

        // set the owning side of the relation if necessary
        if ($lessonConfiguration->getLesson() !== $this) {
            $lessonConfiguration->setLesson($this);
        }

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(?int $position): self
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @deprecated
     */
    public function getIsStopLesson(): ?bool
    {
        return $this->isStopLesson;
    }

    public function isStopLesson(): ?bool
    {
        return $this->isStopLesson;
    }

    public function setIsStopLesson(?bool $isStopLesson): self
    {
        $this->isStopLesson = $isStopLesson;

        return $this;
    }

    public function getImgText(): ?string
    {
        return $this->imgText;
    }

    public function setImgText(?string $imgText): self
    {
        $this->imgText = $imgText;

        return $this;
    }

    /**
     * @deprecated
     */
    public function getIsImportant(): ?bool
    {
        return $this->isImportant;
    }

    public function isImportant(): ?bool
    {
        return $this->isImportant;
    }

    public function setIsImportant(?bool $isImportant): self
    {
        $this->isImportant = $isImportant;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|LessonPart[]
     */
    public function getLessonParts(): Collection
    {
        return $this->lessonParts;
    }

    public function addLessonPart(LessonPart $lessonPart): self
    {
        if (!$this->lessonParts->contains($lessonPart)) {
            $this->lessonParts[] = $lessonPart;
            $lessonPart->setLesson($this);
        }

        return $this;
    }

    /**
     * @param LessonPart[] $lessonParts
     *
     * @return self
     */
    public function addLessonParts(array $lessonParts): self
    {
        foreach ($lessonParts as $lessonPart) {
            $this->addLessonPart($lessonPart);
        }

        return $this;
    }

    public function removeLessonPart(LessonPart $lessonPart): self
    {
        if ($this->lessonParts->contains($lessonPart)) {
            $this->lessonParts->removeElement($lessonPart);
            // set the owning side to null (unless already changed)
            if ($lessonPart->getLesson() === $this) {
                $lessonPart->setLesson(null);
            }
        }

        return $this;
    }

    public function isDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(?bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getSpeakers(): Collection
    {
        return $this->speakers;
    }

    public function addSpeaker(User $speaker): self
    {
        if (!$this->speakers->contains($speaker)) {
            $this->speakers[] = $speaker;
        }

        return $this;
    }

    /**
     * @param User[] $speakers
     *
     * @return self
     */
    public function addSpeakers(array $speakers): self
    {
        foreach ($speakers as $speaker) {
            $this->addSpeaker($speaker);
        }

        return $this;
    }

    public function removeSpeaker(User $speaker): self
    {
        if ($this->speakers->contains($speaker)) {
            $this->speakers->removeElement($speaker);
        }

        return $this;
    }

    public function getImgFile(): ?BaseFiles
    {
        return $this->imgFile;
    }

    public function setImgFile(?BaseFiles $imgFile): self
    {
        $this->imgFile = $imgFile;

        return $this;
    }

    /**
     * @return Collection|UserGroup[]
     */
    public function getUserGroups(): Collection
    {
        return $this->userGroups;
    }

    public function getUserGroupIds(): array
    {
        return $this->userGroups->map(
            function (UserGroup $e) {
                return $e->getId();
            }
        )->toArray();
    }

    public function clearUserGroups(): self
    {
        /** @var UserGroup $userGroup */
        foreach ($this->userGroups as $userGroup) {
            $this->removeUserGroup($userGroup);
        }

        return $this;
    }

    public function addUserGroup(UserGroup $userGroup): self
    {
        if (!$this->userGroups->contains($userGroup)) {
            $this->userGroups[] = $userGroup;
            $userGroup->addLesson($this);
        }

        return $this;
    }

    public function addUserGroups($userGroups): self
    {
        foreach ($userGroups as $userGroup) {
            $this->addUserGroup($userGroup);
        }

        return $this;
    }

    public function removeUserGroup(UserGroup $userGroup): self
    {
        if ($this->userGroups->contains($userGroup)) {
            $this->userGroups->removeElement($userGroup);
            $userGroup->removeLesson($this);
        }

        return $this;
    }

    public function removeUserGroups($userGroups): self
    {
        foreach ($userGroups as $userGroup) {
            $this->removeUserGroup($userGroup);
        }

        return $this;
    }

}

<?php

namespace App\Entity\Courses;

use App\Entity\BaseBlock\BaseFiles;
use App\Entity\UserGroup;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Courses\CourseFolderRepository")
 * @ORM\EntityListeners({"App\EntityListener\DeletingEntityListener"})
 *
 * @ORM\Table("courses_course_folder")
 */
class CourseFolder
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @Groups("course_folder:info")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups("course_folder:info")
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Courses\Course", mappedBy="folder")
     */
    private $courses;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     *
     * @Groups("course_folder:info")
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BaseBlock\BaseFiles")
     */
    private $imgFile;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BaseBlock\BaseFiles")
     */
    private $coverImgFile;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\UserGroup", mappedBy="courseFolders")
     */
    private $userGroups;

    /**
     * @ORM\Column(type="boolean", options={"default": 0})
     */
    private $deleted = false;

    public function __construct()
    {
        $this->courses = new ArrayCollection();
        $this->userGroups = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Course[]
     */
    public function getCourses(): Collection
    {
        return $this->courses;
    }

    public function addCourse(Course $course): self
    {
        if (!$this->courses->contains($course)) {
            $this->courses[] = $course;
            $course->setFolder($this);
        }

        return $this;
    }

    public function removeCourse(Course $course): self
    {
        if ($this->courses->contains($course)) {
            $this->courses->removeElement($course);
            // set the owning side to null (unless already changed)
            if ($course->getFolder() === $this) {
                $course->setFolder(null);
            }
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImgFile(): ?BaseFiles
    {
        return $this->imgFile;
    }

    public function setImgFile(?BaseFiles $imgFile): self
    {
        $this->imgFile = $imgFile;

        return $this;
    }

    public function getCoverImgFile(): ?BaseFiles
    {
        return $this->coverImgFile;
    }

    public function setCoverImgFile(?BaseFiles $coverImgFile): self
    {
        $this->coverImgFile = $coverImgFile;

        return $this;
    }

    /**
     * @return Collection|UserGroup[]
     */
    public function getUserGroups(): Collection
    {
        return $this->userGroups;
    }

    public function getUserGroupIds(): array
    {
        return $this->userGroups->map(
            function (UserGroup $e) {
                return $e->getId();
            }
        )->toArray();
    }

    public function clearUserGroups(): self
    {
        /** @var UserGroup $userGroup */
        foreach ($this->userGroups as $userGroup) {
            $this->removeUserGroup($userGroup);
        }

        return $this;
    }

    public function addUserGroup(UserGroup $userGroup): self
    {
        if (!$this->userGroups->contains($userGroup)) {
            $this->userGroups[] = $userGroup;
            $userGroup->addCourseFolder($this);
        }

        return $this;
    }

    public function addUserGroups(array $userGroups): self
    {
        foreach ($userGroups as $userGroup) {
            $this->addUserGroup($userGroup);
        }

        return $this;
    }

    public function removeUserGroup(UserGroup $userGroup): self
    {
        if ($this->userGroups->contains($userGroup)) {
            $this->userGroups->removeElement($userGroup);
            $userGroup->removeCourseFolder($this);
        }

        return $this;
    }

    public function isDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

}

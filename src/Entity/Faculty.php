<?php

namespace App\Entity;

use App\Repository\FacultyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass=FacultyRepository::class)
 */
class Faculty
{
    public const TYPE_SPEED = 'faculty';
    public const TYPE_HUNDRED = 'hundred';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"traction", "faculty:list"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=CourseStream::class, inversedBy="faculties")
     * @Groups({"faculty:list"})
     */
    private $courseStream;

    /**
     * @deprecated
     * @todo: убрать
     * @ORM\ManyToOne(targetEntity=TeamFolder::class, inversedBy="faculties")
     * @ORM\JoinColumn(nullable=false)
     */
    private $teamFolder;

    /**
     * @ORM\ManyToOne(targetEntity=UserGroup::class, inversedBy="faculties")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"faculty:list"})
     */
    private $userGroup;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"traction", "traction:user", "faculty:list"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"faculty:list"})
     */
    private $redirectUrl;

    /**
     * @ORM\OneToMany(targetEntity=UserGroupUser::class, mappedBy="faculty")
     */
    private $userGroupUsers;

    public function __construct()
    {
        $this->userGroupUsers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCourseStream(): ?CourseStream
    {
        return $this->courseStream;
    }

    /**
     * @return null|int
     * @Groups({"traction:user"})
     * @SerializedName("courseStreamId")
     */
    public function getCourseStreamId(): ?int
    {
        return $this->getCourseStream() ? $this->getCourseStream()->getId() : null;
    }

    public function setCourseStream(?CourseStream $courseStream): self
    {
        $this->courseStream = $courseStream;

        return $this;
    }

    public function getTeamFolder(): ?TeamFolder
    {
        return $this->teamFolder;
    }

    public function setTeamFolder(?TeamFolder $teamFolder): self
    {
        $this->teamFolder = $teamFolder;

        return $this;
    }

    public function getUserGroup(): ?UserGroup
    {
        return $this->userGroup;
    }

    public function setUserGroup(?UserGroup $userGroup): self
    {
        $this->userGroup = $userGroup;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getRedirectUrl(): ?string
    {
        return $this->redirectUrl;
    }

    public function setRedirectUrl(?string $redirectUrl): self
    {
        $this->redirectUrl = $redirectUrl;

        return $this;
    }

    /**
     * @return Collection|UserGroupUser[]
     */
    public function getUserGroupUsers(): Collection
    {
        return $this->userGroupUsers;
    }

    public function addUserGroupUser(UserGroupUser $userGroupUser): self
    {
        if (!$this->userGroupUsers->contains($userGroupUser)) {
            $this->userGroupUsers[] = $userGroupUser;
            $userGroupUser->setFaculty($this);
        }

        return $this;
    }

    public function removeUserGroupUser(UserGroupUser $userGroupUser): self
    {
        if ($this->userGroupUsers->removeElement($userGroupUser)) {
            // set the owning side to null (unless already changed)
            if ($userGroupUser->getFaculty() === $this) {
                $userGroupUser->setFaculty(null);
            }
        }

        return $this;
    }

}

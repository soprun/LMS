<?php

namespace App\Entity;

use App\Entity\Courses\Course;
use App\Repository\UserCertificatesRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserCertificatesRepository", repositoryClass=UserCertificatesRepository::class)
 */
class UserCertificates
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="userCertificates")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Course::class, inversedBy="userCertificates")
     * @ORM\JoinColumn(nullable=true)
     */
    private $course;

    /**
     * @ORM\ManyToOne(targetEntity=CourseStream::class, inversedBy="userCertificates")
     * @ORM\JoinColumn(nullable=true)
     */
    private $courseStream;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $serial;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $serialNumber;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCourse(): ?Course
    {
        return $this->course;
    }

    public function setCourse(?Course $course): self
    {
        $this->course = $course;

        return $this;
    }

    public function getSerial(): ?string
    {
        return $this->serial;
    }

    public function setSerial(string $serial): self
    {
        $this->serial = $serial;

        return $this;
    }

    public function getSerialNumber(): ?string
    {
        return $this->serialNumber;
    }

    public function setSerialNumber(string $serialNumber): self
    {
        $this->serialNumber = $serialNumber;

        return $this;
    }

    public function getCourseStream(): ?CourseStream
    {
        return $this->courseStream;
    }

    public function setCourseStream(?CourseStream $courseStream): self
    {
        $this->courseStream = $courseStream;

        return $this;
    }

}

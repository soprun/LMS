<?php

namespace App\Entity\BaseBlock;

use App\Entity\Interfaces\LessonContentBlockInterface;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BaseBlock\BaseBlock3Repository")
 *
 * @ORM\Table("base_block_3")
 */
class BaseBlock3 implements LessonContentBlockInterface
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $videoUrl;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $typeId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BaseBlock\BaseFiles")
     * @ORM\JoinColumn(nullable=true)
     */
    private $imgFile;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVideoUrl(): ?string
    {
        return $this->videoUrl;
    }

    public function setVideoUrl(string $videoUrl): self
    {
        $this->videoUrl = $videoUrl;

        return $this;
    }

    public function getTypeId(): ?int
    {
        return $this->typeId;
    }

    public function setTypeId(int $typeId): self
    {
        $this->typeId = $typeId;

        return $this;
    }

    public function getImgFile(): ?BaseFiles
    {
        return $this->imgFile;
    }

    public function setImgFile(?BaseFiles $imgFile): self
    {
        $this->imgFile = $imgFile;

        return $this;
    }

}

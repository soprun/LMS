<?php

namespace App\Entity\BaseBlock;

use App\Entity\Interfaces\LessonContentBlockInterface;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BaseBlock\BaseBlock5Repository")
 *
 * @ORM\Table("base_block_5")
 */
class BaseBlock5 implements LessonContentBlockInterface
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $audioUrl;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAudioUrl(): ?string
    {
        return $this->audioUrl;
    }

    public function setAudioUrl(string $audioUrl): self
    {
        $this->audioUrl = $audioUrl;

        return $this;
    }

}

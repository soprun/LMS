<?php

namespace App\Entity\BaseBlock;

use App\Entity\Interfaces\LessonContentBlockInterface;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BaseBlock\BaseBlock7Repository")
 *
 * @ORM\Table("base_block_7")
 */
class BaseBlock7 implements LessonContentBlockInterface
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $buttonText;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $buttonUrl;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isBlank;

    /**
     * @ORM\Column(type="boolean")
     */
    private $openInAppDefaultBrowser = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getButtonText(): ?string
    {
        return $this->buttonText;
    }

    public function setButtonText(string $buttonText): self
    {
        $this->buttonText = $buttonText;

        return $this;
    }

    public function getButtonUrl(): ?string
    {
        return $this->buttonUrl;
    }

    public function setButtonUrl(string $buttonUrl): self
    {
        $this->buttonUrl = $buttonUrl;

        return $this;
    }

    /**
     * @deprecated
     */
    public function getIsBlank(): ?bool
    {
        return $this->isBlank;
    }

    public function isBlank(): ?bool
    {
        return $this->isBlank;
    }

    public function setIsBlank(?bool $isBlank): self
    {
        $this->isBlank = $isBlank;

        return $this;
    }

    public function getOpenInAppDefaultBrowser(): ?bool
    {
        return $this->openInAppDefaultBrowser;
    }

    public function setOpenInAppDefaultBrowser(bool $openInAppDefaultBrowser): self
    {
        $this->openInAppDefaultBrowser = $openInAppDefaultBrowser;

        return $this;
    }
}

<?php

namespace App\Entity\BaseBlock;

use App\Entity\Interfaces\LessonContentBlockInterface;
use App\Entity\Task\TaskSet;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BaseBlock\BaseBlockTaskWithCheckRepository")
 *
 * @ORM\Table("base_block_task_with_check")
 */
class BaseBlockTaskWithCheck implements LessonContentBlockInterface
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $systemName;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $ddl;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Task\TaskSet", mappedBy="blockTaskWithCheck", cascade={"persist"})
     */
    private $taskSets;


    public function __construct()
    {
        $this->taskSets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSystemName(): ?string
    {
        return $this->systemName;
    }

    public function setSystemName(?string $systemName): self
    {
        $this->systemName = $systemName;

        return $this;
    }

    public function getDdl(): ?\DateTimeInterface
    {
        return $this->ddl;
    }

    public function setDdl(?\DateTimeInterface $ddl): self
    {
        $this->ddl = $ddl;

        return $this;
    }

    /**
     * @return Collection|TaskSet[]
     */
    public function getTaskSets(): Collection
    {
        return $this->taskSets;
    }

    public function addTaskSet(TaskSet $taskSet): self
    {
        if (!$this->taskSets->contains($taskSet)) {
            $this->taskSets[] = $taskSet;
            $taskSet->setBlockTaskWithCheck($this);
        }

        return $this;
    }

    public function addTaskSets(array $taskSets): self
    {
        foreach ($taskSets as $taskSet) {
            $this->addTaskSet($taskSet);
        }

        return $this;
    }

    public function removeTaskSet(TaskSet $taskSet): self
    {
        if ($this->taskSets->contains($taskSet)) {
            $this->taskSets->removeElement($taskSet);
            // set the owning side to null (unless already changed)
            if ($taskSet->getBlockTaskWithCheck() === $this) {
                $taskSet->setBlockTaskWithCheck(null);
            }
        }

        return $this;
    }
}

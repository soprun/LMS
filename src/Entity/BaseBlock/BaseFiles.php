<?php

namespace App\Entity\BaseBlock;

use App\Entity\PointsShop\Product;
use App\Entity\Task\AnswerComment;
use App\Entity\Task\AnswerTaskType1;
use App\Entity\Task\TaskTest;
use App\Entity\Task\TaskTestQuestion;
use App\Entity\Task\TaskTestResult;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BaseBlock\BaseFilesRepository")
 */
class BaseFiles
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fileName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mimeType;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\BaseBlock\BaseBlock6", mappedBy="files")
     */
    private $baseBlock6s;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $filePath;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Task\AnswerTaskType1", mappedBy="answerFiles")
     */
    private $answerTaskType1s;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Task\TaskTest", mappedBy="imgFile")
     */
    private $taskTests;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Task\TaskTestQuestion", mappedBy="imgFile")
     */
    private $taskTestQuestions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Task\TaskTestResult", mappedBy="imgFile")
     */
    private $taskTestResults;

    /**
     * @ORM\OneToMany(targetEntity=Product::class, mappedBy="coverImg")
     */
    private $pointsShopProducts;

    /**
     * @ORM\ManyToMany(targetEntity=AnswerComment::class, mappedBy="commentFiles")
     */
    private $answerComments;

    public function __construct()
    {
        $this->baseBlock6s = new ArrayCollection();
        $this->answerTaskType1s = new ArrayCollection();
        $this->taskTests = new ArrayCollection();
        $this->taskTestQuestions = new ArrayCollection();
        $this->taskTestResults = new ArrayCollection();
        $this->pointsShopProducts = new ArrayCollection();
        $this->answerComments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setBlockId(?int $blockId): self
    {
        $this->blockId = $blockId;

        return $this;
    }

    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    public function setFileName(string $fileName): self
    {
        $this->fileName = $fileName;

        return $this;
    }

    public function getMimeType(): ?string
    {
        return $this->mimeType;
    }

    public function setMimeType(string $mimeType): self
    {
        $this->mimeType = $mimeType;

        return $this;
    }

    /**
     * @return Collection|BaseBlock6[]
     */
    public function getBaseBlock6s(): Collection
    {
        return $this->baseBlock6s;
    }

    public function addBaseBlock6(BaseBlock6 $baseBlock6): self
    {
        if (!$this->baseBlock6s->contains($baseBlock6)) {
            $this->baseBlock6s[] = $baseBlock6;
            $baseBlock6->addFile($this);
        }

        return $this;
    }

    public function removeBaseBlock6(BaseBlock6 $baseBlock6): self
    {
        if ($this->baseBlock6s->contains($baseBlock6)) {
            $this->baseBlock6s->removeElement($baseBlock6);
            $baseBlock6->removeFile($this);
        }

        return $this;
    }

    public function getFilePath(): ?string
    {
        return $this->filePath;
    }

    public function setFilePath(string $filePath): self
    {
        $this->filePath = $filePath;

        return $this;
    }

    /**
     * @return Collection|AnswerTaskType1[]
     */
    public function getAnswerTaskType1s(): Collection
    {
        return $this->answerTaskType1s;
    }

    public function addAnswerTaskType1(AnswerTaskType1 $answerTaskType1): self
    {
        if (!$this->answerTaskType1s->contains($answerTaskType1)) {
            $this->answerTaskType1s[] = $answerTaskType1;
            $answerTaskType1->addAnswerFile($this);
        }

        return $this;
    }

    public function removeAnswerTaskType1(AnswerTaskType1 $answerTaskType1): self
    {
        if ($this->answerTaskType1s->contains($answerTaskType1)) {
            $this->answerTaskType1s->removeElement($answerTaskType1);
            $answerTaskType1->removeAnswerFile($this);
        }

        return $this;
    }

    /**
     * @return Collection|TaskTest[]
     */
    public function getTaskTests(): Collection
    {
        return $this->taskTests;
    }

    public function addTaskTest(TaskTest $taskTest): self
    {
        if (!$this->taskTests->contains($taskTest)) {
            $this->taskTests[] = $taskTest;
            $taskTest->setImgFile($this);
        }

        return $this;
    }

    public function removeTaskTest(TaskTest $taskTest): self
    {
        if ($this->taskTests->contains($taskTest)) {
            $this->taskTests->removeElement($taskTest);
            // set the owning side to null (unless already changed)
            if ($taskTest->getImgFile() === $this) {
                $taskTest->setImgFile(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TaskTestQuestion[]
     */
    public function getTaskTestQuestions(): Collection
    {
        return $this->taskTestQuestions;
    }

    public function addTaskTestQuestion(TaskTestQuestion $taskTestQuestion): self
    {
        if (!$this->taskTestQuestions->contains($taskTestQuestion)) {
            $this->taskTestQuestions[] = $taskTestQuestion;
            $taskTestQuestion->setImgFile($this);
        }

        return $this;
    }

    public function removeTaskTestQuestion(TaskTestQuestion $taskTestQuestion): self
    {
        if ($this->taskTestQuestions->contains($taskTestQuestion)) {
            $this->taskTestQuestions->removeElement($taskTestQuestion);
            // set the owning side to null (unless already changed)
            if ($taskTestQuestion->getImgFile() === $this) {
                $taskTestQuestion->setImgFile(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TaskTestResult[]
     */
    public function getTaskTestResults(): Collection
    {
        return $this->taskTestResults;
    }

    public function addTaskTestResult(TaskTestResult $taskTestResult): self
    {
        if (!$this->taskTestResults->contains($taskTestResult)) {
            $this->taskTestResults[] = $taskTestResult;
            $taskTestResult->setImgFile($this);
        }

        return $this;
    }

    public function removeTaskTestResult(TaskTestResult $taskTestResult): self
    {
        if ($this->taskTestResults->contains($taskTestResult)) {
            $this->taskTestResults->removeElement($taskTestResult);
            // set the owning side to null (unless already changed)
            if ($taskTestResult->getImgFile() === $this) {
                $taskTestResult->setImgFile(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getPointsShopProducts(): Collection
    {
        return $this->pointsShopProducts;
    }

    public function addPointsShopProduct(Product $pointsShopProduct): self
    {
        if (!$this->pointsShopProducts->contains($pointsShopProduct)) {
            $this->pointsShopProducts[] = $pointsShopProduct;
            $pointsShopProduct->setCoverImg($this);
        }

        return $this;
    }

    public function removePointsShopProduct(Product $pointsShopProduct): self
    {
        if ($this->pointsShopProducts->contains($pointsShopProduct)) {
            $this->pointsShopProducts->removeElement($pointsShopProduct);
            // set the owning side to null (unless already changed)
            if ($pointsShopProduct->getCoverImg() === $this) {
                $pointsShopProduct->setCoverImg(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|AnswerComment[]
     */
    public function getAnswerComments(): Collection
    {
        return $this->answerComments;
    }

    public function addAnswerComment(AnswerComment $answerComment): self
    {
        if (!$this->answerComments->contains($answerComment)) {
            $this->answerComments[] = $answerComment;
            $answerComment->addCommentFile($this);
        }

        return $this;
    }

    public function removeAnswerComment(AnswerComment $answerComment): self
    {
        if ($this->answerComments->contains($answerComment)) {
            $this->answerComments->removeElement($answerComment);
            $answerComment->removeCommentFile($this);
        }

        return $this;
    }

}

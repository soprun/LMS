<?php

namespace App\Entity\BaseBlock;

use App\Entity\Interfaces\LessonContentBlockInterface;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BaseBlock\BaseBlock4Repository")
 *
 * @ORM\Table("base_block_4")
 */
class BaseBlock4 implements LessonContentBlockInterface
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BaseBlock\BaseFiles")
     */
    private $imgFile;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImgFile(): ?BaseFiles
    {
        return $this->imgFile;
    }

    public function setImgFile(?BaseFiles $imgFile): self
    {
        $this->imgFile = $imgFile;

        return $this;
    }

}

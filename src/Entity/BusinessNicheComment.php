<?php

namespace App\Entity;

use App\Repository\BusinessNicheCommentRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=BusinessNicheCommentRepository::class)
 */
class BusinessNicheComment
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("business:niche")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank
     * @Groups("business:niche")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=BusinessNiche::class, inversedBy="comments")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank
     */
    private $niche;

    /**
     * @ORM\Column(type="string", length=300)
     * @Assert\NotBlank
     * @Groups("business:niche")
     */
    private $text;

    final public function getId(): ?int
    {
        return $this->id;
    }

    final public function getUser(): User
    {
        return $this->user;
    }

    final public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    final public function getNiche(): ?BusinessNiche
    {
        return $this->niche;
    }

    final public function setNiche(?BusinessNiche $niche): self
    {
        $this->niche = $niche;

        return $this;
    }

    final public function getText(): ?string
    {
        return $this->text;
    }

    final public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @Groups("business:niche")
     */
    final public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

}

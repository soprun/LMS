<?php

namespace App\Entity\Analytics;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Table;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Analytics\ReportMSGCellRepository")
 * @Table(name="report_msg_cell")
 */
class ReportMSGCell
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Analytics\ReportMSG", inversedBy="cells")
     * @ORM\JoinColumn(nullable=false)
     */
    private $reportMSG;

    /**
     * @ORM\Column(type="integer")
     */
    private $weekId;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $profit;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    protected $updatedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReportMSG(): ?ReportMSG
    {
        return $this->reportMSG;
    }

    public function setReportMSG(?ReportMSG $reportMSG): self
    {
        $this->reportMSG = $reportMSG;

        return $this;
    }

    public function getWeekId(): ?int
    {
        return $this->weekId;
    }

    public function setWeekId(int $weekId): self
    {
        $this->weekId = $weekId;

        return $this;
    }

    public function getProfit(): ?int
    {
        return $this->profit;
    }

    public function setProfit(?int $profit): self
    {
        $this->profit = $profit;

        return $this;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

}

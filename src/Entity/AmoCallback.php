<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\AmoCallbackRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AmoCallbackRepository::class)
 */
class AmoCallback
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     * @psalm-readonly
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $statusId;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $pipelineId;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $productId;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    private $courseName;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $groupId;

    /**
     * @ORM\Column(type="json", nullable=false)
     */
    private $tags;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $toRemove = false;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var int|null
     */
    private $ticketLevelId;

    final public function getId(): int
    {
        return $this->id;
    }

    final public function getStatusId(): ?int
    {
        return $this->statusId;
    }

    final public function setStatusId(?int $statusId): self
    {
        $this->statusId = $statusId;

        return $this;
    }

    final public function getPipelineId(): ?int
    {
        return $this->pipelineId;
    }

    final public function setPipelineId(?int $pipelineId): self
    {
        $this->pipelineId = $pipelineId;

        return $this;
    }

    final public function getProductId(): ?int
    {
        return $this->productId;
    }

    final public function setProductId(?int $productId): void
    {
        $this->productId = $productId;
    }

    final public function getCourseName(): ?string
    {
        return $this->courseName;
    }

    final public function setCourseName(?string $courseName): self
    {
        $this->courseName = $courseName;

        return $this;
    }

    final public function getGroupId(): ?int
    {
        return $this->groupId;
    }

    final public function setGroupId(?int $groupId): self
    {
        $this->groupId = $groupId;

        return $this;
    }

    final public function getTags(): array
    {
        return $this->tags;
    }

    final public function setTags(array $tags): self
    {
        $this->tags = $tags;

        return $this;
    }

    final public function toRemove(): bool
    {
        return $this->toRemove;
    }

    final public function setToRemove(bool $toRemove): void
    {
        $this->toRemove = $toRemove;
    }

    final public function getTicketLevelId(): ?int
    {
        return $this->ticketLevelId;
    }

    final public function setTicketLevelId(?int $ticketLevelId): void
    {
        $this->ticketLevelId = $ticketLevelId;
    }


}

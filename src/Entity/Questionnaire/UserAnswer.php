<?php

namespace App\Entity\Questionnaire;

use App\Entity\CourseStream;
use App\Entity\User;
use App\Repository\Questionnaire\UserAnswerRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Table(name="questionnaire_user_answer")
 * @ORM\Entity(repositoryClass=UserAnswerRepository::class)
 */
class UserAnswer
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="questionnaireUserAnswers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Question::class, inversedBy="answers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $question;

    /**
     * @ORM\ManyToOne(targetEntity=QuestionAnswerVariant::class, inversedBy="answers")
     */
    private $variant;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @Groups({"questionnaire:user"})
     */
    private $text;

    /**
     * @ORM\ManyToOne(targetEntity=CourseStream::class)
     */
    private $courseStream;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getQuestion(): Question
    {
        return $this->question;
    }

    public function setQuestion(?Question $question): self
    {
        $this->question = $question;

        return $this;
    }

    public function getVariant(): ?QuestionAnswerVariant
    {
        return $this->variant;
    }

    /**
     * @return null|QuestionAnswerVariant
     *
     * @Groups({"questionnaire:user"})
     * @SerializedName("variant")
     */
    public function getVariantId(): ?int
    {
        return $this->getVariant() ? $this->getVariant()->getId() : null;
    }

    public function setVariant(?QuestionAnswerVariant $variant): self
    {
        $this->variant = $variant;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(?string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getCourseStream(): ?CourseStream
    {
        return $this->courseStream;
    }

    public function setCourseStream(?CourseStream $courseStream): self
    {
        $this->courseStream = $courseStream;

        return $this;
    }

}

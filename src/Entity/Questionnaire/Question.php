<?php

namespace App\Entity\Questionnaire;

use App\Entity\CourseStream;
use App\Repository\Questionnaire\QuestionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Table(name="questionnaire_question")
 * @ORM\Entity(repositoryClass=QuestionRepository::class)
 */
class Question
{
    public const WANT_LEADER_SLUG = 'want_leader';
    public const BUSINESS_SLUG = 'business';
    public const MONTH_PROFIT = 'month_profit';
    public const MONTH_REVENUE = 'month_revenue';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @Groups({"questionnaire"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     *
     * @Groups({"questionnaire"})
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"questionnaire"})
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     * @Groups({"questionnaire"})
     */
    private $userProperty;

    /**
     * @ORM\ManyToOne(targetEntity=Question::class)
     */
    private $nextQuestion;

    /**
     * @ORM\OneToMany(targetEntity=QuestionAnswerVariant::class, mappedBy="question", indexBy="id")
     *
     * @Groups({"questionnaire"})
     */
    private $variants;

    /**
     * @ORM\OneToMany(targetEntity=UserAnswer::class, mappedBy="question")
     */
    private $answers;

    /**
     * @ORM\ManyToMany(targetEntity=CourseStream::class)
     */
    private $courseStreams;

    /**
     * @ORM\Column(type="boolean", options={"default": 0})
     *
     * @Groups({"questionnaire"})
     */
    private $first;

    public function __construct()
    {
        $this->variants = new ArrayCollection();
        $this->answers = new ArrayCollection();
        $this->courseStreams = new ArrayCollection();
        $this->first = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getUserProperty(): ?string
    {
        return $this->userProperty;
    }

    public function setUserProperty(?string $userProperty): self
    {
        $this->userProperty = $userProperty;

        return $this;
    }

    public function getNextQuestion(): ?self
    {
        return $this->nextQuestion;
    }

    /**
     * @return int
     *
     * @Groups({"questionnaire"})
     * @SerializedName("nextQuestion")
     */
    public function getNextQuestionId(): ?int
    {
        return $this->getNextQuestion() ? $this->getNextQuestion()->getId() : null;
    }

    public function setNextQuestion(?self $nextQuestion): self
    {
        $this->nextQuestion = $nextQuestion;

        return $this;
    }

    /**
     * @return Collection|QuestionAnswerVariant[]
     */
    public function getVariants(): Collection
    {
        return $this->variants;
    }

    public function addVariant(QuestionAnswerVariant $variant): self
    {
        if (!$this->variants->contains($variant)) {
            $this->variants[] = $variant;
            $variant->setQuestion($this);
        }

        return $this;
    }

    public function removeVariant(QuestionAnswerVariant $variant): self
    {
        if ($this->variants->contains($variant)) {
            $this->variants->removeElement($variant);
            // set the owning side to null (unless already changed)
            if ($variant->getQuestion() === $this) {
                $variant->setQuestion(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserAnswer[]
     */
    public function getAnswers(): Collection
    {
        return $this->answers;
    }

    /**
     * @return ?UserAnswer
     *
     * @Groups({"questionnaire:user"})
     * @SerializedName("answer")
     */
    public function getAnswer(): ?UserAnswer
    {
        return $this->getAnswers()->current() ?: null;
    }

    public function addAnswer(UserAnswer $answer): self
    {
        if (!$this->answers->contains($answer)) {
            $this->answers[] = $answer;
            $answer->setQuestion($this);
        }

        return $this;
    }

    public function removeAnswer(UserAnswer $answer): self
    {
        if ($this->answers->contains($answer)) {
            $this->answers->removeElement($answer);
            // set the owning side to null (unless already changed)
            if ($answer->getQuestion() === $this) {
                $answer->setQuestion(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CourseStream[]
     */
    public function getCourseStreams(): Collection
    {
        return $this->courseStreams;
    }

    public function addCourseStream(CourseStream $courseStream): self
    {
        if (!$this->courseStreams->contains($courseStream)) {
            $this->courseStreams[] = $courseStream;
        }

        return $this;
    }

    public function removeCourseStream(CourseStream $courseStream): self
    {
        $this->courseStreams->removeElement($courseStream);

        return $this;
    }

    public function getFirst(): ?bool
    {
        return $this->first;
    }

    public function setFirst(bool $first): self
    {
        $this->first = $first;

        return $this;
    }

}

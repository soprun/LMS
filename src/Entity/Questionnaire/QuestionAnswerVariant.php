<?php

namespace App\Entity\Questionnaire;

use App\Repository\Questionnaire\QuestionAnswerVariantRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Table(name="questionnaire_question_answer_variant")
 * @ORM\Entity(repositoryClass=QuestionAnswerVariantRepository::class)
 */
class QuestionAnswerVariant
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @Groups({"questionnaire"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"questionnaire"})
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity=Question::class, inversedBy="variants")
     * @ORM\JoinColumn(nullable=false)
     */
    private $question;

    /**
     * @ORM\OneToMany(targetEntity=UserAnswer::class, mappedBy="variant")
     */
    private $answers;

    /**
     * @ORM\ManyToOne(targetEntity=Question::class)
     */
    private $nextQuestion;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $parameters = [];

    public function __construct()
    {
        $this->answers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getQuestion(): ?Question
    {
        return $this->question;
    }

    public function setQuestion(?Question $question): self
    {
        $this->question = $question;

        return $this;
    }

    /**
     * @return Collection|UserAnswer[]
     */
    public function getAnswers(): Collection
    {
        return $this->answers;
    }

    public function addAnswer(UserAnswer $answer): self
    {
        if (!$this->answers->contains($answer)) {
            $this->answers[] = $answer;
            $answer->setVariant($this);
        }

        return $this;
    }

    public function removeAnswer(UserAnswer $answer): self
    {
        if ($this->answers->contains($answer)) {
            $this->answers->removeElement($answer);
            // set the owning side to null (unless already changed)
            if ($answer->getVariant() === $this) {
                $answer->setVariant(null);
            }
        }

        return $this;
    }

    public function getNextQuestion(): ?Question
    {
        return $this->nextQuestion;
    }

    /**
     * @return int
     *
     * @Groups({"questionnaire"})
     * @SerializedName("nextQuestion")
     */
    public function getNextQuestionId(): ?int
    {
        return $this->getNextQuestion() ? $this->getNextQuestion()->getId() : null;
    }

    public function setNextQuestion(?Question $nextQuestion): self
    {
        $this->nextQuestion = $nextQuestion;

        return $this;
    }

    public function getParameters(): ?array
    {
        return $this->parameters;
    }

    /**
     * @param  string  $index
     * @param  null  $default
     * @return null|mixed
     */
    public function getParameter(string $index, $default = null)
    {
        return $this->getParameters()[$index] ?? $default;
    }

    public function setParameters(?array $parameters): self
    {
        $this->parameters = $parameters;

        return $this;
    }

}

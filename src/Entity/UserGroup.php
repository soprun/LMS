<?php

namespace App\Entity;

use App\Entity\Courses\Course;
use App\Entity\Courses\CourseFolder;
use App\Entity\Courses\Lesson;
use App\Entity\SmartSender\SmartSenderBotGroup;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserGroupRepository")
 */
class UserGroup
{
    use TimestampableEntity;

    public const CAPTAINTS_GROUP_ID = 413;
    public const SENIOR_CAPTAINTS_GROUP_ID = 414;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @Groups({"traction:filters", "faculty:list"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"traction:filters", "faculty:list"})
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Courses\Course", inversedBy="userGroups", cascade={"persist", "remove"})
     */
    private $courses;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Courses\Lesson", inversedBy="userGroups", cascade={"persist", "remove"})
     */
    private $lessons;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\GroupPermission", inversedBy="userGroup", cascade={"persist", "remove"})
     */
    private $groupPermissions;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Courses\CourseFolder", inversedBy="userGroups")
     */
    private $courseFolders;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserGroupFolder", inversedBy="userGroups")
     */
    private $groupFolder;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="createdUserGroups")
     */
    private $creator;

    /**
     * @ORM\OneToMany(
     *     targetEntity=UserGroupUser::class,
     *     mappedBy="userGroup",
     *     cascade={"persist", "remove"},
     *     orphanRemoval=true
     * )
     */
    private $userRelations;

    /**
     * @ORM\ManyToMany(targetEntity=TeamFolder::class, mappedBy="userGroups")
     */
    private $teamFolders;

    /**
     * @ORM\OneToMany(targetEntity=Faculty::class, mappedBy="userGroup")
     */
    private $faculties;

    /**
     * @ORM\ManyToOne(targetEntity=CourseStream::class, inversedBy="userGroups")
     * @Groups("marathon-investment:list")
     */
    private $courseStream;

    /**
     * @ORM\ManyToOne(targetEntity=UserGroup::class)
     */
    private $alternativeGroup;

    public function __construct()
    {
        $this->courses = new ArrayCollection();
        $this->lessons = new ArrayCollection();
        $this->courseFolders = new ArrayCollection();
        $this->userRelations = new ArrayCollection();
        $this->teamFolders = new ArrayCollection();
        $this->faculties = new ArrayCollection();
    }

    public function __toString()
    {
        return "#{$this->getId()} - {$this->getName()}";
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Course[]
     */
    public function getCourses(): Collection
    {
        return $this->courses;
    }

    public function addCourse(Course $course): self
    {
        if (!$this->courses->contains($course)) {
            $this->courses[] = $course;
        }

        return $this;
    }

    public function removeCourse(Course $course): self
    {
        if ($this->courses->contains($course)) {
            $this->courses->removeElement($course);
        }

        return $this;
    }

    /**
     * @return Collection|Lesson[]
     */
    public function getLessons(): Collection
    {
        return $this->lessons;
    }

    public function addLesson(Lesson $lesson): self
    {
        if (!$this->lessons->contains($lesson)) {
            $this->lessons[] = $lesson;
        }

        return $this;
    }

    public function removeLesson(Lesson $lesson): self
    {
        if ($this->lessons->contains($lesson)) {
            $this->lessons->removeElement($lesson);
        }

        return $this;
    }

    public function getGroupPermissions(): ?GroupPermission
    {
        return $this->groupPermissions;
    }

    public function setGroupPermissions(?GroupPermission $groupPermissions): self
    {
        $this->groupPermissions = $groupPermissions;

        return $this;
    }

    /**
     * @return Collection|CourseFolder[]
     */
    public function getCourseFolders(): Collection
    {
        return $this->courseFolders;
    }

    public function addCourseFolder(CourseFolder $courseFolder): self
    {
        if (!$this->courseFolders->contains($courseFolder)) {
            $this->courseFolders[] = $courseFolder;
        }

        return $this;
    }

    public function removeCourseFolder(CourseFolder $courseFolder): self
    {
        if ($this->courseFolders->contains($courseFolder)) {
            $this->courseFolders->removeElement($courseFolder);
        }

        return $this;
    }

    public function getGroupFolder(): ?UserGroupFolder
    {
        return $this->groupFolder;
    }

    public function setGroupFolder(?UserGroupFolder $groupFolder): self
    {
        $this->groupFolder = $groupFolder;

        return $this;
    }

    public function getCreator(): ?User
    {
        return $this->creator;
    }

    public function setCreator(?User $creator): self
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * @return Collection|UserGroupUser[]
     */
    public function getUserRelations(): Collection
    {
        return $this->userRelations->filter(function (UserGroupUser $userRelation) {
            return (!$userRelation->isDeleted());
        });
    }

    public function addUserRelation(UserGroupUser $userRelation): self
    {
        if (!$this->userRelations->contains($userRelation)) {
            $this->userRelations[] = $userRelation;
            $userRelation->setUserGroup($this);
        }

        return $this;
    }

    public function removeUserRelation(UserGroupUser $userRelation): self
    {
        if ($this->userRelations->contains($userRelation)) {
            $userRelation->setDeleted(true);
        }

        return $this;
    }

    public function addUser(User $user, ?User $addedBy = null, ?int $budget = null, ?int $amoLeadId = null): self
    {
        $existUserInGroup = $this->userRelations
            ->exists(function ($key, UserGroupUser $value) use ($user) {
                return $user === $value->getUser();
            });
        if (!$existUserInGroup) {
            $relation = new UserGroupUser();
            $relation->setUser($user);
            $relation->setUserGroup($this);
            $relation->setAddedBy($addedBy);
            $relation->setBudget($budget);
            $relation->setAmoLeadId($amoLeadId);

            $this->addUserRelation($relation);
            $user->addGroupRelation($relation);
        } else {
            /** @var UserGroupUser $userRelation */
            $userRelation = $this->userRelations
                ->filter(function (UserGroupUser $value) use ($user) {
                    if ($user === $value->getUser()) {
                        return $value;
                    }
                })->first();
            $userRelation->setDeleted(false);
        }

        return $this;
    }

    public function addAmoUser(User $user, int $amoLeadId, int $budget): self
    {
        $existUserInGroup = $this->userRelations
            ->exists(function ($key, UserGroupUser $value) use ($user) {
                return $user === $value->getUser();
            });
        if (!$existUserInGroup) {
            $relation = new UserGroupUser();
            $relation->setUser($user);
            $relation->setUserGroup($this);
            $relation->setAmoLeadId($amoLeadId);
            $relation->setBudget($budget);

            $this->addUserRelation($relation);
            $user->addGroupRelation($relation);
        } else {
            /** @var UserGroupUser $userRelation */
            $userRelation = $this->userRelations
                ->filter(function (UserGroupUser $value) use ($user) {
                    if ($user === $value->getUser()) {
                        return $value;
                    }
                })->first();
            $userRelation->setDeleted(false);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        $relations = $this->getUserRelations()
            ->filter(function (UserGroupUser $value) use ($user) {
                return $user === $value->getUser();
            });
        if (!$relations->isEmpty()) {
            foreach ($relations as $relation) {
                $this->removeUserRelation($relation);
                $user->removeGroupRelation($relation);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TeamFolder[]
     */
    public function getTeamFolders(): Collection
    {
        return $this->teamFolders;
    }

    public function addTeamFolder(TeamFolder $teamFolder): self
    {
        if (!$this->teamFolders->contains($teamFolder)) {
            $this->teamFolders[] = $teamFolder;
            $teamFolder->addUserGroup($this);
        }

        return $this;
    }

    public function removeTeamFolder(TeamFolder $teamFolder): self
    {
        if ($this->teamFolders->removeElement($teamFolder)) {
            $teamFolder->removeUserGroup($this);
        }

        return $this;
    }

    /**
     * @return Collection|Faculty[]
     */
    public function getFaculties(): Collection
    {
        return $this->faculties;
    }

    public function addFaculty(Faculty $faculty): self
    {
        if (!$this->faculties->contains($faculty)) {
            $this->faculties[] = $faculty;
            $faculty->setUserGroup($this);
        }

        return $this;
    }

    public function removeFaculty(Faculty $faculty): self
    {
        if ($this->faculties->removeElement($faculty)) {
            // set the owning side to null (unless already changed)
            if ($faculty->getUserGroup() === $this) {
                $faculty->setUserGroup(null);
            }
        }

        return $this;
    }

    public function getCourseStream(): ?CourseStream
    {
        return $this->courseStream;
    }

    /**
     * @Groups({"traction:filters"})
     * @SerializedName("courseStreamId")
     *
     * @return null|int
     */
    public function getCourseStreamId(): ?int
    {
        return $this->getCourseStream() ? $this->getCourseStream()->getId() : null;
    }

    public function setCourseStream(?CourseStream $courseStream): self
    {
        $this->courseStream = $courseStream;

        return $this;
    }

    public function getAlternativeGroup(): ?self
    {
        return $this->alternativeGroup;
    }

    public function setAlternativeGroup(?self $alternativeGroup): self
    {
        $this->alternativeGroup = $alternativeGroup;

        return $this;
    }
}

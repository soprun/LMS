<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GroupPermissionRepository")
 */
class GroupPermission
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isAdmin;

    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private $folderModuleAdmin;

    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private $courseModuleAdmin;

    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private $lessonModuleAdmin;

    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private $taskAnswerCheckAdmin;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\UserGroup", mappedBy="groupPermissions", cascade={"persist", "remove"})
     */
    private $userGroup;

    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private $userModuleAdmin;

    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private $userGroupModuleAdmin;

    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private $userTeamModuleAdmin;

    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private $analyticsModelAdmin;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsAdmin(): ?bool
    {
        return $this->isAdmin;
    }

    public function setIsAdmin(?bool $isAdmin): self
    {
        $this->isAdmin = $isAdmin;

        return $this;
    }

    public function getFolderModuleAdmin(): ?string
    {
        return $this->folderModuleAdmin;
    }

    public function setFolderModuleAdmin(?string $folderModuleAdmin): self
    {
        $this->folderModuleAdmin = $folderModuleAdmin;

        return $this;
    }

    public function getCourseModuleAdmin(): ?string
    {
        return $this->courseModuleAdmin;
    }

    public function setCourseModuleAdmin(?string $courseModuleAdmin): self
    {
        $this->courseModuleAdmin = $courseModuleAdmin;

        return $this;
    }

    public function getLessonModuleAdmin(): ?string
    {
        return $this->lessonModuleAdmin;
    }

    public function setLessonModuleAdmin(?string $lessonModuleAdmin): self
    {
        $this->lessonModuleAdmin = $lessonModuleAdmin;

        return $this;
    }

    public function getTaskAnswerCheckAdmin(): ?string
    {
        return $this->taskAnswerCheckAdmin;
    }

    public function setTaskAnswerCheckAdmin(?string $taskAnswerCheckAdmin): self
    {
        $this->taskAnswerCheckAdmin = $taskAnswerCheckAdmin;

        return $this;
    }

    public function getUserGroup(): ?UserGroup
    {
        return $this->userGroup;
    }

    public function setUserGroup(?UserGroup $userGroup): self
    {
        $this->userGroup = $userGroup;

        // set (or unset) the owning side of the relation if necessary
        $newGroupPermissions = null === $userGroup ? null : $this;
        if ($userGroup->getGroupPermissions() !== $newGroupPermissions) {
            $userGroup->setGroupPermissions($newGroupPermissions);
        }

        return $this;
    }

    public function getUserModuleAdmin(): ?string
    {
        return $this->userModuleAdmin;
    }

    public function setUserModuleAdmin(?string $userModuleAdmin): self
    {
        $this->userModuleAdmin = $userModuleAdmin;

        return $this;
    }

    public function getUserGroupModuleAdmin(): ?string
    {
        return $this->userGroupModuleAdmin;
    }

    public function setUserGroupModuleAdmin(?string $userGroupModuleAdmin): self
    {
        $this->userGroupModuleAdmin = $userGroupModuleAdmin;

        return $this;
    }

    public function getUserTeamModuleAdmin(): ?string
    {
        return $this->userTeamModuleAdmin;
    }

    public function setUserTeamModuleAdmin(?string $userTeamModuleAdmin): self
    {
        $this->userTeamModuleAdmin = $userTeamModuleAdmin;

        return $this;
    }

    public function getAnalyticsModelAdmin(): ?string
    {
        return $this->analyticsModelAdmin;
    }

    public function setAnalyticsModelAdmin(?string $analyticsModelAdmin): self
    {
        $this->analyticsModelAdmin = $analyticsModelAdmin;

        return $this;
    }
}

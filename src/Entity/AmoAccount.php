<?php

namespace App\Entity;

use App\Repository\AmoAccountRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use League\OAuth2\Client\Token\AccessToken;

/**
 * @ORM\Entity(repositoryClass=AmoAccountRepository::class)
 */
class AmoAccount
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $baseDomain;

    /**
     * @ORM\Column(type="string", length=1024)
     */
    private $accessToken;

    /**
     * @ORM\Column(type="string", length=1024)
     */
    private $refreshToken;

    /**
     * @ORM\Column(type="integer")
     */
    private $tokenExpires;

    public function __construct(?array $dataToken)
    {
        if (
            isset($dataToken)
            && isset($dataToken['accessToken'])
            && isset($dataToken['refreshToken'])
            && isset($dataToken['expires'])
            && isset($dataToken['baseDomain'])
        ) {
            $this->setAccessToken($dataToken['accessToken']);
            $this->setTokenExpires($dataToken['expires']);
            $this->setRefreshToken($dataToken['refreshToken']);
            $this->setBaseDomain($dataToken['baseDomain']);
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBaseDomain(): ?string
    {
        return $this->baseDomain;
    }

    public function setBaseDomain(string $baseDomain): self
    {
        $this->baseDomain = $baseDomain;

        return $this;
    }

    public function getAccessToken(): ?string
    {
        return $this->accessToken;
    }

    public function setAccessToken(string $accessToken): self
    {
        $this->accessToken = $accessToken;

        return $this;
    }

    public function getRefreshToken(): ?string
    {
        return $this->refreshToken;
    }

    public function setRefreshToken(string $refreshToken): self
    {
        $this->refreshToken = $refreshToken;

        return $this;
    }

    public function getTokenExpires(): ?int
    {
        return $this->tokenExpires;
    }

    public function setTokenExpires(int $tokenExpires): self
    {
        $this->tokenExpires = $tokenExpires;

        return $this;
    }

    public function getAmoAccessToken(): AccessToken
    {
        return new AccessToken(
            [
                'access_token' => $this->accessToken,
                'refresh_token' => $this->refreshToken,
                'expires' => $this->tokenExpires,
                'baseDomain' => $this->baseDomain,
            ]
        );
    }

}

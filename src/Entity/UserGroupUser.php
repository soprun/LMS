<?php

namespace App\Entity;

use App\Repository\UserGroupUserRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;

use function Aws\boolean_value;

/**
 * @ORM\Entity(repositoryClass=UserGroupUserRepository::class)
 * @ORM\EntityListeners({"App\EntityListener\UserGroupUserListener", "App\EntityListener\DeletingEntityListener"})
 */
class UserGroupUser
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     *
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="groupRelations", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Id()
     *
     * @ORM\ManyToOne(targetEntity=UserGroup::class, inversedBy="userRelations", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @Groups("marathon-investment:list")
     */
    private $userGroup;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private $addedBy;

    /**
     * @ORM\Column(type="boolean", options={"default": 0})
     */
    private $deleted = false;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $amoLeadId;

    /**
     * @ORM\ManyToOne(targetEntity=Faculty::class, inversedBy="userGroupUsers")
     */
    private $faculty;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $budget;

    /**
     * @param  null|User  $user
     * @param  null|UserGroup  $userGroup
     */
    public function __construct(?User $user = null, ?UserGroup $userGroup = null)
    {
        if ($user) {
            $this->setUser($user);
        }
        if ($userGroup) {
            $this->setUserGroup($userGroup);
        }
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getUserGroup(): UserGroup
    {
        return $this->userGroup;
    }

    public function setUserGroup(?UserGroup $userGroup): self
    {
        $this->userGroup = $userGroup;

        return $this;
    }

    public function getAddedBy(): ?User
    {
        return $this->addedBy;
    }

    public function setAddedBy(?User $addedBy): self
    {
        $this->addedBy = $addedBy;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDeleted(): bool
    {
        return $this->deleted;
    }

    /**
     * @param  bool  $deleted
     * @return $this
     */
    public function setDeleted(bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    public function getAmoLeadId(): ?int
    {
        return $this->amoLeadId;
    }

    public function setAmoLeadId(?int $amoLeadId): self
    {
        $this->amoLeadId = $amoLeadId;

        return $this;
    }

    public function getFaculty(): ?Faculty
    {
        return $this->faculty;
    }

    public function setFaculty(?Faculty $faculty): self
    {
        $this->faculty = $faculty;

        return $this;
    }

    public function getBudget(): ?int
    {
        return $this->budget;
    }

    public function setBudget(?int $budget): self
    {
        $this->budget = $budget;

        return $this;
    }

}

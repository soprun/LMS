<?php

namespace App\Entity;

use App\Repository\BusinessAreaRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=BusinessAreaRepository::class)
 */
class BusinessArea
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("business:niche")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("business:niche")
     */
    private $name;

    final public function getId(): ?int
    {
        return $this->id;
    }

    final public function getName(): ?string
    {
        return $this->name;
    }

    final public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

}

<?php

namespace App\Entity;

use App\Repository\EventRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass=EventRepository::class)
 * @ORM\EntityListeners({"App\EntityListener\EventListener"})
 */
class Event
{
    use TimestampableEntity;

    public const TYPE_VIEW_CLUB_PROFILE = 1;
    public const TYPE_UPLOAD_CLUB_AVATAR = 2;
    public const TYPE_UPLOAD_BUSINESS_PHOTO = 3;
    public const TYPE_UPDATE_TELEGRAM = 4;
    public const TYPE_UPDATE_INSTAGRAM = 5;
    public const TYPE_UPDATE_FACEBOOK = 6;
    public const TYPE_UPDATE_VK = 7;
    public const TYPE_UPDATE_BUSINESS_SPHERE = 8;
    public const TYPE_UPDATE_BUSINESS_NICHE = 9;
    public const TYPE_UPDATE_COMPANY_NAME = 10;
    public const TYPE_CLUB_SEARCH = 11;

    public const TYPES = [
        self::TYPE_VIEW_CLUB_PROFILE => 'view_club_profile',
        self::TYPE_UPLOAD_CLUB_AVATAR => 'upload_club_avatar',
        self::TYPE_UPLOAD_BUSINESS_PHOTO => 'upload_business_photo',
        self::TYPE_UPDATE_TELEGRAM => 'upload_telegram',
        self::TYPE_UPDATE_INSTAGRAM => 'upload_instagram',
        self::TYPE_UPDATE_FACEBOOK => 'upload_fb',
        self::TYPE_UPDATE_VK => 'upload_vk',
        self::TYPE_UPDATE_BUSINESS_SPHERE => 'upload_business_sphere',
        self::TYPE_UPDATE_BUSINESS_NICHE => 'upload_business_niche',
        self::TYPE_UPDATE_COMPANY_NAME => 'upload_company_name',
        self::TYPE_CLUB_SEARCH => 'club_search',
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private $object;

    /**
     * @ORM\Column(type="integer")
     */
    private $type;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $data = [];

    public function __construct(User $user, int $type = null, ?User $object = null, $data = [])
    {
        $this->setUser($user);
        if ($type) {
            $this->setType($type)
                ->setObject($object)
                ->setData($data);
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getObject(): ?User
    {
        return $this->object;
    }

    public function setObject(?User $object): self
    {
        $this->object = $object;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getData(): ?array
    {
        return $this->data;
    }

    public function setData(?array $data): self
    {
        $this->data = $data;

        return $this;
    }
}

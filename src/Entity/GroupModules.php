<?php

namespace App\Entity;

use App\Entity\Courses\Course;
use App\Entity\Courses\Module;
use App\Repository\GroupModulesRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=GroupModulesRepository::class)
 * @UniqueEntity(
 *     fields={"relatedGroup", "module", "course", "userTariff", "courseStream"}
 * )
 */
class GroupModules
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=UserGroup::class)
     */
    private $relatedGroup;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $module;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $course;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tariff;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $stream;

    /**
     * @ORM\Column(type="boolean", options={"default": 0})
     */
    private $deleted;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRelatedGroup(): ?UserGroup
    {
        return $this->relatedGroup;
    }

    public function setRelatedGroup(?UserGroup $relatedGroup): self
    {
        $this->relatedGroup = $relatedGroup;

        return $this;
    }

    public function getModule(): ?string
    {
        return $this->module;
    }

    public function setModule(?string $module): self
    {
        $this->module = $module;

        return $this;
    }

    public function getCourse(): ?Course
    {
        return $this->course;
    }

    public function setCourse(?Course $course): self
    {
        $this->course = $course;

        return $this;
    }

    public function getTariff(): ?string
    {
        return $this->tariff;
    }

    public function setTariff(?string $tariff): self
    {
        $this->tariff = $tariff;

        return $this;
    }

    public function getStream(): ?int
    {
        return $this->stream;
    }

    public function setStream(?int $stream): self
    {
        $this->stream = $stream;

        return $this;
    }

    public function isDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }
}

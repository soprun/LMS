<?php

namespace App\Entity;

use App\Repository\PromocodeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=PromocodeRepository::class)
 */
class Promocode
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     * @Groups("promocode:info")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Partner::class, inversedBy="promocodes")
     *
     * @Groups("promocode:info")
     */
    private $partner;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @Groups("promocode:info")
     */
    private $expiresIn;

    /**
     * @ORM\Column(type="string")
     *
     * @Groups("promocode:info")
     */
    private $code;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isUnique;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="promocodes")
     * @ORM\JoinColumn(nullable=true)
     *
     * @Groups("promocode:info")
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPartner(): ?Partner
    {
        return $this->partner;
    }

    public function setPartner(?Partner $partner): self
    {
        $this->partner = $partner;

        return $this;
    }

    public function getExpiresIn(): ?\DateTimeInterface
    {
        return $this->expiresIn;
    }

    public function setExpiresIn(?\DateTimeInterface $expiresIn): self
    {
        $this->expiresIn = $expiresIn;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function isUnique(): ?bool
    {
        return $this->isUnique;
    }

    public function setIsUnique(?bool $isUnique): self
    {
        $this->isUnique = $isUnique;

        return $this;
    }

    public function __construct()
    {
        $this->isUnique = false;
    }
}

<?php

namespace App\Entity\SmartSender;

use App\Entity\AbstractCourse;
use App\Repository\SmartSender\SmartSenderBotRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SmartSenderBotRepository::class)
 */
class SmartSenderBot
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $token;

    /**
     * @ORM\ManyToOne(targetEntity=AbstractCourse::class, inversedBy="smartSenderBots")
     */
    private $abstractCourse;

    /**
     * @ORM\OneToMany(targetEntity=SmartSenderBotGroup::class, mappedBy="smartSenderBot")
     */
    private $smartSenderBotGroups;

    /**
     * @ORM\OneToMany(targetEntity=SmartSenderUser::class, mappedBy="smartSenderBot")
     */
    private $smartSenderUsers;

    public function __construct()
    {
        $this->smartSenderBotGroups = new ArrayCollection();
        $this->smartSenderUsers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getAbstractCourse(): ?AbstractCourse
    {
        return $this->abstractCourse;
    }

    public function setAbstractCourse(?AbstractCourse $abstractCourse): self
    {
        $this->abstractCourse = $abstractCourse;

        return $this;
    }

    /**
     * @return Collection|SmartSenderBotGroup[]
     */
    public function getSmartSenderBotGroups(): Collection
    {
        return $this->smartSenderBotGroups;
    }

    public function addSmartSenderBotGroup(SmartSenderBotGroup $smartSenderBotGroup): self
    {
        if (!$this->smartSenderBotGroups->contains($smartSenderBotGroup)) {
            $this->smartSenderBotGroups[] = $smartSenderBotGroup;
            $smartSenderBotGroup->setSmartSenderBot($this);
        }

        return $this;
    }

    public function removeSmartSenderBotGroup(SmartSenderBotGroup $smartSenderBotGroup): self
    {
        if ($this->smartSenderBotGroups->contains($smartSenderBotGroup)) {
            $this->smartSenderBotGroups->removeElement($smartSenderBotGroup);
            // set the owning side to null (unless already changed)
            if ($smartSenderBotGroup->getSmartSenderBot() === $this) {
                $smartSenderBotGroup->setSmartSenderBot(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SmartSenderUser[]
     */
    public function getSmartSenderUsers(): Collection
    {
        return $this->smartSenderUsers;
    }

    public function addSmartSenderUser(SmartSenderUser $smartSenderUser): self
    {
        if (!$this->smartSenderUsers->contains($smartSenderUser)) {
            $this->smartSenderUsers[] = $smartSenderUser;
            $smartSenderUser->setSmartSenderBot($this);
        }

        return $this;
    }

    public function removeSmartSenderUser(SmartSenderUser $smartSenderUser): self
    {
        if ($this->smartSenderUsers->contains($smartSenderUser)) {
            $this->smartSenderUsers->removeElement($smartSenderUser);
            // set the owning side to null (unless already changed)
            if ($smartSenderUser->getSmartSenderBot() === $this) {
                $smartSenderUser->setSmartSenderBot(null);
            }
        }

        return $this;
    }

}

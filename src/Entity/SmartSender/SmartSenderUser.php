<?php

namespace App\Entity\SmartSender;

use App\Entity\User;
use App\Repository\SmartSender\SmartSenderUserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass=SmartSenderUserRepository::class)
 */
class SmartSenderUser
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=SmartSenderBot::class, inversedBy="smartSenderUsers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $smartSenderBot;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="smartSenderUsers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $smartSenderUserId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $code;

    /**
     * @deprecated Перенесли на auth
     * @todo: выпилить
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $username;

    /**
     * @ORM\OneToMany(targetEntity=SmartSenderForm::class, mappedBy="smartSenderUser")
     */
    private $smartSenderForms;

    public function __construct()
    {
        $this->smartSenderForms = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSmartSenderBot(): ?SmartSenderBot
    {
        return $this->smartSenderBot;
    }

    public function setSmartSenderBot(?SmartSenderBot $smartSenderBot): self
    {
        $this->smartSenderBot = $smartSenderBot;

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getSmartSenderUserId(): ?int
    {
        return $this->smartSenderUserId;
    }

    public function setSmartSenderUserId(?int $smartSenderUserId): self
    {
        $this->smartSenderUserId = $smartSenderUserId;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(?string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return Collection|SmartSenderForm[]
     */
    public function getSmartSenderForms(): Collection
    {
        return $this->smartSenderForms;
    }

    public function addSmartSenderForm(SmartSenderForm $smartSenderForm): self
    {
        if (!$this->smartSenderForms->contains($smartSenderForm)) {
            $this->smartSenderForms[] = $smartSenderForm;
            $smartSenderForm->setSmartSenderUser($this);
        }

        return $this;
    }

    public function removeSmartSenderForm(SmartSenderForm $smartSenderForm): self
    {
        if ($this->smartSenderForms->removeElement($smartSenderForm)) {
            // set the owning side to null (unless already changed)
            if ($smartSenderForm->getSmartSenderUser() === $this) {
                $smartSenderForm->setSmartSenderUser(null);
            }
        }

        return $this;
    }

}

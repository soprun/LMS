<?php

namespace App\Entity\SmartSender;

use App\Repository\SmartSender\SmartSenderFormRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass=SmartSenderFormRepository::class)
 */
class SmartSenderForm
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=SmartSenderUser::class, inversedBy="smartSenderForms")
     */
    private $smartSenderUser;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $formName;

    /**
     * @ORM\Column(type="text")
     */
    private $question;

    /**
     * @ORM\Column(type="text")
     */
    private $answer;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSmartSenderUser(): ?SmartSenderUser
    {
        return $this->smartSenderUser;
    }

    public function setSmartSenderUser(?SmartSenderUser $smartSenderUser): self
    {
        $this->smartSenderUser = $smartSenderUser;

        return $this;
    }

    public function getFormName(): ?string
    {
        return $this->formName;
    }

    public function setFormName(?string $formName): self
    {
        $this->formName = $formName;

        return $this;
    }

    public function getQuestion(): ?string
    {
        return $this->question;
    }

    public function setQuestion(string $question): self
    {
        $this->question = $question;

        return $this;
    }

    public function getAnswer(): ?string
    {
        return $this->answer;
    }

    public function setAnswer(string $answer): self
    {
        $this->answer = $answer;

        return $this;
    }
}

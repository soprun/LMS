<?php

namespace App\Entity\SmartSender;

use App\Entity\UserGroup;
use App\Repository\SmartSender\SmartSenderBotGroupRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SmartSenderBotGroupRepository::class)
 */
class SmartSenderBotGroup
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=SmartSenderBot::class, inversedBy="smartSenderBotGroups")
     * @ORM\JoinColumn(nullable=false)
     */
    private $smartSenderBot;

    /**
     * @ORM\OneToOne(targetEntity=UserGroup::class, inversedBy="smartSenderBotGroups", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $userGroup;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSmartSenderBot(): ?SmartSenderBot
    {
        return $this->smartSenderBot;
    }

    public function setSmartSenderBot(?SmartSenderBot $smartSenderBot): self
    {
        $this->smartSenderBot = $smartSenderBot;

        return $this;
    }

    public function getUserGroup(): ?UserGroup
    {
        return $this->userGroup;
    }

    public function setUserGroup(UserGroup $userGroup): self
    {
        $this->userGroup = $userGroup;

        return $this;
    }

}

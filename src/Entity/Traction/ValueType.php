<?php

namespace App\Entity\Traction;

use App\Entity\AbstractCourse;
use App\Repository\Traction\ValueTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(name="traction_value_type")
 * @ORM\Entity(repositoryClass=ValueTypeRepository::class)
 */
class ValueType
{
    use TimestampableEntity;

    public const TYPE_PROFIT = 'Прибыль';
    public const TYPE_REVENUE = 'Выручка';
    public const PROFIT_AND_REVENUE_CODE_NAMES = [
        self::TYPE_REVENUE => 'revenue',
        self::TYPE_PROFIT => 'profit',
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     * @Groups({"traction","user:traction"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"traction","user:traction"})
     */
    private $title;

    /**
     * @ORM\Column(type="integer")
     *
     * @Groups({"traction"})
     */
    private $position;

    /**
     * @ORM\ManyToMany(targetEntity=AbstractCourse::class, inversedBy="valueTypes", orphanRemoval=true)
     * @ORM\JoinTable(name="traction_value_type_abstract_course")
     */
    private $abstractCourses;

    /**
     * @ORM\OneToMany(targetEntity=Value::class, mappedBy="type")
     */
    private $values;

    public function __construct()
    {
        $this->abstractCourses = new ArrayCollection();
        $this->values = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Collection|AbstractCourse[]
     */
    public function getAbstractCourses(): Collection
    {
        return $this->abstractCourses;
    }

    public function addAbstractCourse(AbstractCourse $abstractCourse): self
    {
        if (!$this->abstractCourses->contains($abstractCourse)) {
            $this->abstractCourses[] = $abstractCourse;
        }

        return $this;
    }

    public function removeAbstractCourse(AbstractCourse $abstractCourse): self
    {
        $this->abstractCourses->removeElement($abstractCourse);

        return $this;
    }

    /**
     * @return Collection|Value[]
     */
    public function getValues(): Collection
    {
        return $this->values;
    }

    public function addValue(Value $value): self
    {
        if (!$this->values->contains($value)) {
            $this->values[] = $value;
        }

        return $this;
    }

    public function removeValue(Value $value): self
    {
        $this->values->removeElement($value);

        return $this;
    }

    public function getPosition(): int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

}

<?php

namespace App\Entity\Traction;

use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Repository\Traction\CheckpointValueRepository;

/**
 * @ORM\Entity(repositoryClass=CheckpointValueRepository::class)
 * @ORM\Table(
 *     name="traction_checkpoint_value",
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(columns={"user_id","checkpoint_id"})
 *     })
 */
class CheckpointValue
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("checkpoint:user_value")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Checkpoint::class, inversedBy="values")
     * @ORM\JoinColumn(nullable=false)
     */
    private $checkpoint;

    /**
     * @ORM\Column(type="boolean")
     * @Groups("checkpoint:user_value")
     */
    private $value;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("checkpoint:user_value")
     */
    private $filename;

    public function __construct(?Checkpoint $checkpoint = null, ?User $user = null)
    {
        if ($checkpoint) {
            $this->setCheckpoint($checkpoint);
        }
        if ($user) {
            $this->setUser($user);
        }
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCheckpoint(): ?Checkpoint
    {
        return $this->checkpoint;
    }

    public function setCheckpoint(Checkpoint $checkpoint): self
    {
        $this->checkpoint = $checkpoint;

        return $this;
    }

    public function getValue(): ?bool
    {
        return $this->value;
    }

    public function setValue(bool $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function setFilename(string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }

}

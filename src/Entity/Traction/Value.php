<?php

namespace App\Entity\Traction;

use App\Doctrine\StringableDateTime;
use App\Entity\User;
use App\Repository\Traction\ValueRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @todo: индексы
 * @ORM\Table(name="traction_value")
 * @ORM\Entity(repositoryClass=ValueRepository::class)
 */
class Value
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="date_pk")
     *
     * @Groups({"traction","user:traction"})
     */
    private $date;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity=ValueType::class, inversedBy="values")
     * @ORM\JoinColumn(nullable=false)
     */
    private $type;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="tractionValues")
     * @ORM\JoinColumn(nullable=false)
     */
    private $owner;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @Groups({"traction","user:traction"})
     */
    private $value;

    public function getDate(): StringableDateTime
    {
        return $this->date;
    }

    public function setDate(StringableDateTime $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getOwner(): User
    {
        return $this->owner;
    }

    /**
     * @SerializedName("owner")
     * @Groups({"traction"})
     *
     * @return null|int
     */
    public function getOwnerId(): ?int
    {
        return $this->getOwner() ? $this->getOwner()->getId() : null;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getType(): ValueType
    {
        return $this->type;
    }

    /**
     * @SerializedName("type")
     * @Groups({"traction","user:traction"})
     *
     * @return null|int
     */
    public function getTypeId(): ?int
    {
        return $this->getType() ? $this->getType()->getId() : null;
    }

    public function setType(?ValueType $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getValue(): ?int
    {
        return $this->value;
    }

    public function setValue(?int $value): self
    {
        $this->value = $value;

        return $this;
    }
}

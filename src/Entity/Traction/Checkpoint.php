<?php

namespace App\Entity\Traction;

use App\Entity\CourseStream;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use App\Repository\Traction\CheckpointRepository;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass=CheckpointRepository::class)
 * @ORM\Table(
 *     name="traction_checkpoint",
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(columns={"stream_id","priority"})
 *     })
 */
class Checkpoint
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"checkpoint:user_value"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=CourseStream::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $stream;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"checkpoint:user_value"})
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=CheckpointValue::class, mappedBy="checkpoint", orphanRemoval=true)
     * @var ArrayCollection|CheckpointValue[]
     */
    private $values;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"checkpoint:user_value"})
     */
    private $priority;

    public function getId(): int
    {
        return $this->id;
    }

    public function getStream(): CourseStream
    {
        return $this->stream;
    }

    public function setStream(CourseStream $stream): self
    {
        $this->stream = $stream;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPriority(): int
    {
        return $this->priority;
    }

    public function setPriority(int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * @Groups({"checkpoint:user_value"})
     * @SerializedName("value")
     */
    public function getValue(): ?CheckpointValue
    {
        $checkpointValue = $this->values->first();

        return $checkpointValue ?: null;
    }

}

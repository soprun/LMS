<?php

namespace App\Entity;

use App\Entity\Courses\Course;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserCourseTariffRepository")
 */
class UserCourseTariff
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Courses\Course")
     * @ORM\JoinColumn(nullable=false)
     */
    private $course;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userCourseTariffs")
     */
    private $manager;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $tracker;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserTariff", inversedBy="userCourseTariffs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $tariff;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $faculty;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCourse(): ?Course
    {
        return $this->course;
    }

    public function setCourse(?Course $course): self
    {
        $this->course = $course;

        return $this;
    }

    public function getManager(): ?User
    {
        return $this->manager;
    }

    public function setManager(?User $manager): self
    {
        $this->manager = $manager;

        return $this;
    }

    public function getTracker(): ?User
    {
        return $this->tracker;
    }

    public function setTracker(?User $tracker): self
    {
        $this->tracker = $tracker;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getTariff(): ?UserTariff
    {
        return $this->tariff;
    }

    public function setTariff(?UserTariff $tariff): self
    {
        $this->tariff = $tariff;

        return $this;
    }

    public function getFaculty(): ?string
    {
        return $this->faculty;
    }

    public function setFaculty(?string $faculty): self
    {
        $this->faculty = $faculty;

        return $this;
    }

}

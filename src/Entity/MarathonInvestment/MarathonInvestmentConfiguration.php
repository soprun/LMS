<?php

namespace App\Entity\MarathonInvestment;

use App\Entity\CourseStream;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MarathonInvestment\MarathonInvestmentConfigurationRepository")
 * @ORM\Table(name="traction_marathon_investment_configuration")
 */
class MarathonInvestmentConfiguration
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("marathon-investment:create")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="marathonConfigurations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CourseStream")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank()
     */
    private $stream;

    /**
     * @ORM\Column(type="bigint")
     * @Assert\NotBlank()
     * @Groups("marathon-investment:list", "marathon-investment:create", "marathon-investment:member")
     */
    private $pointA;

    /**
     * @ORM\Column(type="bigint")
     * @Assert\NotBlank()
     * @Groups("marathon-investment:list", "marathon-investment:create", "marathon-investment:member")
     */
    private $profit;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getStream(): ?CourseStream
    {
        return $this->stream;
    }

    public function setStream(CourseStream $stream): self
    {
        $this->stream = $stream;

        return $this;
    }

    public function getPointA(): ?int
    {
        return $this->pointA;
    }

    public function setPointA(int $pointA): self
    {
        $this->pointA = $pointA;

        return $this;
    }

    public function getProfit(): ?int
    {
        return $this->profit;
    }

    public function setProfit(int $profit): self
    {
        $this->profit = $profit;

        return $this;
    }
}

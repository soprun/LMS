<?php

namespace App\Entity\MarathonInvestment;

use App\Entity\User;
use App\Repository\MarathonInvestment\UserNicheCommentRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MarathonInvestment\UserNicheCommentRepository")
 * @ORM\Table(name="user_niche_comment")
 */
class UserNicheComment
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=UserNiche::class, inversedBy="comments", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false, onDelete="cascade")
     */
    private $niche;

    /**
     * @ORM\Column(type="string", length=300)
     */
    private $text;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getNiche(): ?UserNiche
    {
        return $this->niche;
    }

    public function setNiche(?UserNiche $niche): self
    {
        $this->niche = $niche;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }
}

<?php

namespace App\Entity\MarathonInvestment;

use App\Entity\CourseStream;
use App\Entity\User;
use App\Repository\MarathonInvestment\UserNicheRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MarathonInvestment\UserNicheRepository")
 * @ORM\Table(name="user_niche")
 */
class UserNiche
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"marathon-investment:list", "marathon-investment:niches:create"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="niches", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=CourseStream::class)
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank()
     */
    private $stream;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     * @Groups({"marathon-investment:list", "marathon-investment:niches:create"})
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=300)
     * @Assert\NotBlank()
     * @Groups({"marathon-investment:list", "marathon-investment:niches:create"})
     */
    private $description = "";

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"marathon-investment:list", "marathon-investment:niches:create"})
     */
    private $rival = false;

    /**
     * @ORM\Column(type="boolean")
     * @Groups("marathon-investment:list")
     */
    private $marked = false;

    /**
     * @ORM\OneToMany(targetEntity=UserNicheComment::class, mappedBy="niche", cascade={"persist"})
     */
    private $comments;

    /**
     * @Groups("marathon-investment:list")
     */
    private $count;

    /**
     * @ORM\Column(type="boolean")
     * @Groups("marathon-investment:list")
     */
    private $main = false;

    public function __construct()
    {
        $this->comments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getStream(): ?CourseStream
    {
        return $this->stream;
    }

    public function setStream(CourseStream $stream): self
    {
        $this->stream = $stream;

        return $this;
    }

    public function getTitle(): string
    {
        return (string)$this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): string
    {
        return (string)$this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function isRival(): ?bool
    {
        return $this->rival;
    }

    public function setRival(bool $rival): self
    {
        $this->rival = $rival;

        return $this;
    }

    public function isMarked(): ?bool
    {
        return $this->marked;
    }

    public function setMarked(bool $marked): self
    {
        $this->marked = $marked;

        return $this;
    }

    /**
     * @return Collection|UserNicheComment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(UserNicheComment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setNiche($this);
        }

        return $this;
    }

    public function removeComment(UserNicheComment $comment): self
    {
        $this->comments->removeElement($comment);

        return $this;
    }

    public function getCount(): int
    {
        return $this->comments->count();
    }

    public function isMain(): ?bool
    {
        return $this->main;
    }

    public function setMain(bool $main): self
    {
        $this->main = $main;

        return $this;
    }
}

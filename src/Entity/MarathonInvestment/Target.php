<?php

namespace App\Entity\MarathonInvestment;

use App\Entity\CourseStream;
use App\Repository\MarathonInvestment\TargetRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MarathonInvestment\TargetRepository")
 * @ORM\Table(name="traction_target")
 */
class Target
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="tractionTargets")
     */
    private $users;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CourseStream", inversedBy="targets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $stream;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return (string)$this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getStream(): ?CourseStream
    {
        return $this->stream;
    }

    public function setStream(?CourseStream $stream): self
    {
        $this->stream = $stream;

        return $this;
    }
}

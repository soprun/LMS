<?php

namespace App\Entity\MarathonInvestment;

use App\Entity\CourseStream;
use App\Entity\User;
use App\Repository\MarathonInvestment\MarathonInvestmentRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MarathonInvestment\MarathonInvestmentRepository")
 * @ORM\Table(name="traction_marathon_investment")
 */
class MarathonInvestment
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CourseStream")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank()
     */
    private $stream;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="marathonInvestments")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank()
     */
    private $user;

    /**
     * Номер недели
     *
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    private $week;

    /**
     * Назначено встреч
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $planned;

    /**
     * Проведено встреч
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $passed;

    /**
     * Привлечено инвестиций
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $amount;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStream(): ?CourseStream
    {
        return $this->stream;
    }

    public function setStream(CourseStream $stream): self
    {
        $this->stream = $stream;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getWeek(): ?int
    {
        return $this->week;
    }

    public function setWeek(int $week): self
    {
        $this->week = $week;

        return $this;
    }

    public function getPlanned(): ?int
    {
        return $this->planned;
    }

    public function setPlanned(?int $planned): self
    {
        $this->planned = $planned;

        return $this;
    }

    public function getPassed(): ?int
    {
        return $this->passed;
    }

    public function setPassed(?int $passed): self
    {
        $this->passed = $passed;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(?int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }
}

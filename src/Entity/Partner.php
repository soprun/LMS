<?php

namespace App\Entity;

use App\Repository\PartnerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=PartnerRepository::class)
 */
class Partner
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     * @Groups("partner:info")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups("partner:info")
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups("partner:info")
     */
    private $name;

    /**
     * @ORM\Column(type="boolean", options={"default": 0})
     */
    private $deleted = false;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups("partner:info")
     */
    private $url;

    /**
     * @ORM\OneToMany(targetEntity=Promocode::class, mappedBy="partner")
     */
    private $promocodes;

    /**
     * @ORM\OneToMany(targetEntity=AttemptToGetPromocode::class, mappedBy="partner")
     */
    private $attemptToGetPromocodes;

    /**
     * @ORM\Column(type="string", length=100)
     *
     * @Groups("partner:info")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=50)
     *
     * @Groups("partner:info")
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     *
     * @Groups("partner:info")
     */
    private $subtitle;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups("partner:info")
     */
    private $logo;

    public function __construct()
    {
        $this->promocodes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function isDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * @return Collection|Promocode[]
     */
    public function getPromocodes(): Collection
    {
        return $this->promocodes;
    }

    public function addPromocode(Promocode $promocode): self
    {
        if (!$this->promocodes->contains($promocode)) {
            $this->promocodes[] = $promocode;
            $promocode->setPartner($this);
        }

        return $this;
    }

    public function removePromocode(Promocode $promocode): self
    {
        if ($this->promocodes->removeElement($promocode)) {
            // set the owning side to null (unless already changed)
            if ($promocode->getPartner() === $this) {
                $promocode->setPartner(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|AttemptToGetPromocode[]
     */
    public function getAttemptToGetPromocodes(): Collection
    {
        return $this->attemptToGetPromocodes;
    }

    public function addAttemptToGetPromocode(AttemptToGetPromocode $attemptToGetPromocode): self
    {
        if (!$this->attemptToGetPromocodes->contains($attemptToGetPromocode)) {
            $this->attemptToGetPromocodes[] = $attemptToGetPromocode;
            $attemptToGetPromocode->setPartner($this);
        }

        return $this;
    }

    public function removeAttemptToGetPromocode(AttemptToGetPromocode $attemptToGetPromocode): self
    {
        if ($this->attemptToGetPromocodes->removeElement($attemptToGetPromocode)) {
            // set the owning side to null (unless already changed)
            if ($attemptToGetPromocode->getPartner() === $this) {
                $attemptToGetPromocode->setPartner(null);
            }
        }

        return $this;
    }

    public function getFreePromocode(): ?ArrayCollection
    {
        return $this->getPromocodes()->filter(static function ($promocode) {
            return $promocode->getUser() === null;
        });
    }

    public function getUserPromocode(User $user): ?ArrayCollection
    {
        return $this->getPromocodes()->filter(static function ($promocode) use ($user) {
            return $promocode->getUser() === $user;
        });
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSubtitle(): ?string
    {
        return $this->subtitle;
    }

    public function setSubtitle(?string $subtitle): self
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

}

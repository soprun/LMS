<?php

namespace App\Entity;

use App\Entity\Courses\Course;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserTariffRepository")
 */
class UserTariff
{
    public const STANDART_TARIFF_SLUG = 'standart';
    public const VIP_TARIFF_SLUG = 'vip';
    public const LF_TARIFF_SLUG = 'lf';
    public const MSA_TARIFF_SLUG = 'msa';
    public const BUSINESS_TARIFF_SLUG = 'business';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Courses\Course", inversedBy="userTariffs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $course;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserCourseTariff", mappedBy="tariff", orphanRemoval=true)
     */
    private $userCourseTariffs;

    public function __construct()
    {
        $this->userCourseTariffs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCourse(): ?Course
    {
        return $this->course;
    }

    public function setCourse(?Course $course): self
    {
        $this->course = $course;

        return $this;
    }

    /**
     * @return Collection|UserCourseTariff[]
     */
    public function getUserCourseTariffs(): Collection
    {
        return $this->userCourseTariffs;
    }

    public function addUserCourseTariff(UserCourseTariff $userCourseTariff): self
    {
        if (!$this->userCourseTariffs->contains($userCourseTariff)) {
            $this->userCourseTariffs[] = $userCourseTariff;
            $userCourseTariff->setTariff($this);
        }

        return $this;
    }

    public function removeUserCourseTariff(UserCourseTariff $userCourseTariff): self
    {
        if ($this->userCourseTariffs->contains($userCourseTariff)) {
            $this->userCourseTariffs->removeElement($userCourseTariff);
            // set the owning side to null (unless already changed)
            if ($userCourseTariff->getTariff() === $this) {
                $userCourseTariff->setTariff(null);
            }
        }

        return $this;
    }
}

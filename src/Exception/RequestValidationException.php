<?php

namespace App\Exception;

use RuntimeException;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\Validator\ConstraintViolationInterface;

class RequestValidationException extends RuntimeException
{
    /** @var FormErrorIterator */
    private $errors;
    /** @var array<string, array> */
    private $messages = [];

    public function __construct(FormErrorIterator $errors, $message = '', $code = 0, $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->errors = $errors;
        $this->aggregateMessages();
    }

    private function aggregateMessages(): void
    {
        $this->messages = [];
        foreach ($this->errors as $error) {
            $this->messages[] = ['field' => self::getErrorPath($error), 'message' => $error->getMessage()];
        }
    }

    public function getErrors(): FormErrorIterator
    {
        return $this->errors;
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return $this->messages;
    }

    public static function getErrorPath(FormError $error): string
    {
        if ($error->getCause() instanceof ConstraintViolationInterface) {
            $propertyPath = $error->getCause()->getPropertyPath();
            if (preg_match('/children\[[A-z._]+]/', $propertyPath) === 1) {
                $regex = '/(^children\[)|(][A-z\d.\]\[]*$)/';
            } else {
                $regex = '/(^data\.)|([\[][\dA-z]+[]])/';
            }
            $path = preg_replace($regex, '', $propertyPath);
        }

        return !empty($path) ? $path : 'common';
    }

}

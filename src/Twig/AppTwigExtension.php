<?php

namespace App\Twig;

use Symfony\Component\HttpKernel\KernelInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppTwigExtension extends AbstractExtension
{

    private $kernel;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    public function getFilters(): array
    {
        return [];
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('get_asset_ts', [$this, 'getAssetTs']),
        ];
    }

    public function getAssetTs($path)
    {
        $absolutePath = $this->kernel->getProjectDir() . '/public/' . $path;

        return filemtime($absolutePath);
    }

}

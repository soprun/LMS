<?php

namespace App\Repository;

use App\Entity\TeamFolder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TeamFolder|null find($id, $lockMode = null, $lockVersion = null)
 * @method TeamFolder|null findOneBy(array $criteria, array $orderBy = null)
 * @method TeamFolder[]    findAll()
 * @method TeamFolder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TeamFolderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TeamFolder::class);
    }

    // /**
    //  * @return TeamFolder[] Returns an array of TeamFolder objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TeamFolder
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

<?php

namespace App\Repository;

use App\Entity\AbstractCourse;
use App\Entity\Team;
use App\Entity\User;
use App\Service\AuthServerHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Team|null find($id, $lockMode = null, $lockVersion = null)
 * @method Team|null findOneBy(array $criteria, array $orderBy = null)
 * @method Team[]    findAll()
 * @method Team[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TeamRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Team::class);
    }

    public function getGroupsData(
        AuthServerHelper $authServerHelper,
        ?string $faculty,
        ?int $captainId,
        ?array $folderIds,
        ?array $filterCities,
        ?string $search,
        User $user,
        bool $findByUser,
        ?int $pageSize,
        ?int $page
    ): array {
        $qb = $this->createQueryBuilder('t');
        $qb->select(
            'f.id AS folderId',
            'f.name AS folderName',
            't.id AS teamId',
            't.name AS teamName',
            'c.id AS captainId',
            'COUNT(DISTINCT ut.id) AS count'
        )
            ->innerJoin('t.folder', 'f')
            ->leftJoin('t.users', 'u') // для поиска
            ->leftJoin('t.users', 'ut') // для суммирования
            ->leftJoin('t.captain', 'c')
        ;
        if ($faculty) {
            $findByUser = false;
            $qb->innerJoin('u.groupRelations', 'ugu')
                ->innerJoin('ugu.userGroup', 'ug')
                ->innerJoin('ug.faculties', 'faculty')
                ->andWhere('(ugu.deleted = 0)')
                ->andWhere('faculty.name = :faculty')
                ->setParameter('faculty', $faculty);
        }

        if ($captainId) {
            $findByUser = false;
            $qb->andWhere('t.captain = :captainId')
                ->setParameter('captainId', $captainId);
        }

        if ($folderIds) {
            $findByUser = false;
            $qb->andWhere('f.id IN(:folderIds)')
                ->setParameter('folderIds', $folderIds);
        }

        if ($filterCities || $search) {
            $findByUser = false;
            $userIds = $this->getFilteredUserIds($authServerHelper, $filterCities, $search);

            if (!empty($userIds)) {
                $qb->andWhere('u.id IN(:userIds)')
                    ->setParameter('userIds', $userIds);
            }

            $qb->orWhere('t.createdBy = :user')->setParameter('user', $user);
        }

        if ($findByUser) {
            $qb->andWhere('u = :user OR t.captain = :user OR t.seniorCaptain = :user')
                ->setParameter('user', $user);
        }

        $qb->groupBy('t.id');
        if ($pageSize) {
            $qb->setMaxResults($pageSize);
        }
        if ($page) {
            $qb->setFirstResult(($page - 1) * $pageSize);
        }

        return $qb->getQuery()->getResult();
    }

    public function getUserInActualCoursesIds(): array
    {
        $data = $this->createQueryBuilder('t')
            ->select('u.id')
            ->innerJoin('t.users', 'u')
            ->innerJoin('t.folder', 'tf')
            ->innerJoin('tf.courseStream', 'cs')
            ->innerJoin('cs.abstractCourse', 'ac')
            ->where('ac.slug IN(:slugs)')
            ->andWhere('cs.active = 1')
            ->setParameters(
                ['slugs' => [AbstractCourse::SLUG_SPEED, AbstractCourse::SLUG_SPEED_CLUB, AbstractCourse::SLUG_HUNDRED]]
            )
            ->groupBy('u.id')
            ->getQuery()
            ->getResult();

        return array_column($data, 'id');
    }

    public function prepareSearch(?array $cities = null, ?string $string = null): string
    {
        $search = '';
        if ($cities) {
            $search = sprintf(
                'ui.city IN(%s)',
                implode(
                    ',',
                    array_map(
                        static function ($city) {
                            return sprintf("'%s'", $city);
                        },
                        $cities
                    )
                )
            );
        }

        if ($string) {
            if (!empty($search)) {
                $search .= " AND ";
            }

            $search .= sprintf(
                "(ui.name LIKE '%%%s%%' OR ui.lastname LIKE '%%%s%%' OR u.email LIKE '%%%s%%')",
                $string,
                $string,
                $string
            );
        }

        return $search;
    }

    public function getFilteredUserIds(
        AuthServerHelper $authServerHelper,
        ?array $cities = null,
        ?string $string = null
    ): array {
        // берем всех людей в командах
        $qb = $this->createQueryBuilder('t');

        $ids = $qb
            ->select('u.authUserId')
            ->leftJoin('t.users', 'u')
            ->groupBy('u.authUserId')
            ->getQuery()
            ->getResult();

        if (empty($ids)) {
            return [];
        }

        $authIds = array_column($ids, 'authUserId');

        $search = $this->prepareSearch($cities, $string);

        $users = $authServerHelper->getUsers($authIds, ['id'], $search);

        return array_column($users, 'id');
    }

}

<?php

namespace App\Repository\Task;

use App\Entity\BaseBlock\BaseBlockTaskWithCheck;
use App\Entity\Courses\LessonPartBlock;
use App\Entity\Task\AnswerComment;
use App\Entity\Task\AnswerTaskType1;
use App\Entity\Task\TaskSet;
use App\Entity\Task\TaskType1;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AnswerComment|null find($id, $lockMode = null, $lockVersion = null)
 * @method AnswerComment|null findOneBy(array $criteria, array $orderBy = null)
 * @method AnswerComment[]    findAll()
 * @method AnswerComment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnswerCommentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AnswerComment::class);
    }

    public function getCommentFilesId($commentId)
    {
        $res = $this->createQueryBuilder('a')
            ->select('files.id')
            ->innerJoin('a.commentFiles', 'files')
            ->andWhere('a.id = :commentId')
            ->setParameter('commentId', $commentId)
            ->getQuery();

        return $res->getResult();
    }

    public function getCommentsSortedByDate($answerId, $variety)
    {
        $res = $this->createQueryBuilder('a')
            ->select('a.id, a.comment, user.authUserId as commentator, a.state, a.createdAt')
            ->innerJoin('a.commentator', 'user')
            ->andWhere('a.answerId = :answerId')
            ->setParameter('answerId', $answerId)
            ->andWhere('a.variety = :variety')
            ->setParameter('variety', $variety)
            ->orderBy('a.createdAt', 'ASC')
            ->getQuery();

        return $res->getResult();
    }

    public function getCommentsFeed(
        BaseBlockTaskWithCheck $taskBlock,
        $state = null,
        ?User $validating = null,
        ?int $userAnswerId = null
    ) {
        $res = $this->createQueryBuilder('a')
            ->select(
                'a.id',
                'a.answerId',
                'answerUser.authUserId as ansUserAuth',
                't.id as questionId',
                'a.comment',
                'user.authUserId as commentator',
                'a.state',
                'a.createdAt'
            )
            ->innerJoin('a.commentator', 'user')
            ->leftJoin(
                AnswerTaskType1::class,
                'answer',
                Join::WITH,
                'a.answerId = answer.id'
            )
            // Находим ответ по блоку
            ->innerJoin('answer.task', 't')
            ->innerJoin(TaskType1::class, 'task1', Join::WITH, 'task1 = answer.task')
            ->innerJoin(TaskSet::class, 's', Join::WITH, 's.blockId = task1.id')
            ->innerJoin('s.blockTaskWithCheck', 'tb', Join::WITH, 'tb = :taskBlock')
            ->innerJoin('answer.user', 'answerUser')
            ->setParameter('taskBlock', $taskBlock);

        if ($validating) {
            // Сужаем поиск по валидатору   a.comment, user.authUserId as commentator, a.state,
            $res->andWhere('a.validating = :validating')
                ->setParameter('validating', $validating);
        }

        if ($userAnswerId) {
            // Сужаем поиск по валидатору   a.comment, user.authUserId as commentator, a.state,
            $res->andWhere('answerUser.id = :userAnswerId')
                ->setParameter('userAnswerId', $userAnswerId);
        }

        if ($state != null) {
            $res->andWhere('a.state = :state')
                ->setParameter('state', $state);
        } else {
            $res->andWhere(
                '(a.state = ' . AnswerComment::STATE_NOT_VIEWED . ' OR a.state = ' . AnswerComment::STATE_VIEWED . ')'
            );
        }

        return $res->getQuery()->getResult();
    }

    public function getCommentsAll(
        $page,
        $pageLimit,
        $state = null,
        ?User $validating = null,
        ?int $userAnswerId = null
    ) {
        $res = $this->createQueryBuilder('a')
            ->select(
                'a.id',
                'a.answerId',
                'answerUser.authUserId as ansUserAuth',
                't.id as questionId',
                's.variety as variety',
                'tb.id as taskBlockId',
                'course.id as courseId',
                'course.name as courseName',
                'a.comment',
                'user.authUserId as commentator',
                'a.state',
                'a.createdAt'
            )
            ->innerJoin('a.commentator', 'user')
            ->innerJoin(
                AnswerTaskType1::class,
                'answer',
                Join::WITH,
                'answer.id = a.answerId'
            )
            ->innerJoin('answer.task', 't')
            ->innerJoin('answer.user', 'answerUser')
            ->innerJoin(TaskType1::class, 'task1', Join::WITH, 'task1 = answer.task')
            ->innerJoin(TaskSet::class, 's', Join::WITH, 's.blockId = task1.id')
            ->innerJoin('s.blockTaskWithCheck', 'tb', Join::WITH, 'tb.id = s.blockTaskWithCheck')
            ->innerJoin(LessonPartBlock::class, 'lpb', Join::WITH, 'lpb.blockId = tb.id  and lpb.type = 2')
            ->innerJoin('lpb.part', 'part')
            ->innerJoin('part.lesson', 'lesson')
            ->innerJoin('lesson.course', 'course');

        if ($validating) {
            // Сужаем поиск по валидатору   a.comment, user.authUserId as commentator, a.state,
            $res->andWhere('a.validating = :validating')
                ->setParameter('validating', $validating);
        }

        if ($userAnswerId) {
            // Сужаем поиск по валидатору   a.comment, user.authUserId as commentator, a.state,
            $res->andWhere('answerUser.id = :userAnswerId')
                ->setParameter('userAnswerId', $userAnswerId);
        }

        if ($state != null) {
            $res->andWhere('a.state = :state')
                ->setParameter('state', $state);
        } else {
            $res->andWhere('a.state != ' . AnswerComment::STATE_ANSWER_SENT);
        }

        // now get one page's items:
        return $res
            ->getQuery()
            //->setFirstResult($pageLimit * ($page - 1)) // set the offset
            //->setMaxResults(null)// set the limit
            ->getArrayResult();
    }

}

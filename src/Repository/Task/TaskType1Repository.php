<?php

namespace App\Repository\Task;

use App\Entity\Task\TaskType1;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TaskType1|null find($id, $lockMode = null, $lockVersion = null)
 * @method TaskType1|null findOneBy(array $criteria, array $orderBy = null)
 * @method TaskType1[]    findAll()
 * @method TaskType1[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskType1Repository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TaskType1::class);
    }


    public function findByIdNotAutoChecked(int $id)
    {
        return $this->createQueryBuilder('t')
            //->andWhere('t.isAutoCheck is null OR t.isAutoCheck = 0')

            ->andWhere('t.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }

}

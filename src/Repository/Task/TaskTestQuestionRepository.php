<?php

namespace App\Repository\Task;

use App\Entity\Task\TaskTest;
use App\Entity\Task\TaskTestQuestion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TaskTestQuestion|null find($id, $lockMode = null, $lockVersion = null)
 * @method TaskTestQuestion|null findOneBy(array $criteria, array $orderBy = null)
 * @method TaskTestQuestion[]    findAll()
 * @method TaskTestQuestion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskTestQuestionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TaskTestQuestion::class);
    }

    /**
     * @param  TaskTest  $taskTest
     * @return TaskTestQuestion[]
     */
    public function findByTaskTest(TaskTest $taskTest)
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.deleted = 0')
            ->andWhere('q.test = :taskTest')
            ->setParameter('taskTest', $taskTest)
            ->orderBy('q.position', 'ASC')
            ->getQuery()
            ->getResult();
    }

}

<?php

namespace App\Repository\Task;

use App\Entity\Task\TaskTest;
use App\Entity\Task\TaskTestQuestionUserAnswer;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

class TaskTestQuestionUserAnswerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TaskTestQuestionUserAnswer::class);
    }

    public function getUserPointsSumArray(array $userIds, array $parameters)
    {
        return $this->createQueryBuilder('a')
            ->select('u.id AS userId, SUM(answer.points) AS sum')
            ->innerJoin('a.user', 'u')
            ->innerJoin('a.answer', 'answer')
            ->andWhere('a.user IN(:userIds)')
            ->setParameter('userIds', $userIds)
            ->andWhere('a.createdAt >= :date1')
            ->setParameter('date1', $parameters['date1'])
            ->andWhere('a.createdAt <= :date2')
            ->setParameter('date2', $parameters['date2'])
            ->groupBy('a.user')
            ->getQuery()
            ->getResult();
    }

    public function getSumOfTestUserAnswers(User $user, TaskTest $taskTest): int
    {
        $result = $this->createQueryBuilder('ua')
            ->select('SUM(a.points) as sum')
            ->innerJoin('ua.question', 'q')
            ->innerJoin('ua.answer', 'a')
            ->andWhere('ua.user = :user')
            ->setParameter('user', $user)
            ->andWhere('q.test = :taskTest')
            ->setParameter('taskTest', $taskTest)
            ->getQuery()
            ->getResult();

        return $result[0]['sum'] ?? 0;
    }

    public function getTestQuestionUserAnswers(User $user, TaskTest $taskTest): array
    {
        $questionUserAnswers = [];
        $rows = $this->createQueryBuilder('ua')
            ->select('q.id AS questionId, a.id AS answerId')
            ->innerJoin('ua.question', 'q')
            ->innerJoin('ua.answer', 'a')
            ->andWhere('ua.user = :user')
            ->setParameter('user', $user)
            ->andWhere('q.test = :taskTest')
            ->setParameter('taskTest', $taskTest)
            ->getQuery()
            ->getResult();

        foreach ($rows as $row) {
            $questionId = $row['questionId'];
            $answerId = $row['answerId'];

            if (array_key_exists($questionId, $questionUserAnswers)) {
                $questionUserAnswers[$questionId][] = $answerId;
            } else {
                $questionUserAnswers[$questionId] = [$answerId];
            }
        }

        return $questionUserAnswers;
    }
}

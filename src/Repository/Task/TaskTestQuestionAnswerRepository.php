<?php

namespace App\Repository\Task;

use App\Entity\Task\TaskTestQuestionAnswer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TaskTestQuestionAnswer|null find($id, $lockMode = null, $lockVersion = null)
 * @method TaskTestQuestionAnswer|null findOneBy(array $criteria, array $orderBy = null)
 * @method TaskTestQuestionAnswer[]    findAll()
 * @method TaskTestQuestionAnswer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskTestQuestionAnswerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TaskTestQuestionAnswer::class);
    }

    // /**
    //  * @return TaskTestQuestionAnswer[] Returns an array of TaskTestQuestionAnswer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TaskTestQuestionAnswer
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

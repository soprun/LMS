<?php

namespace App\Repository\Task;

use App\Entity\BaseBlock\BaseBlockTaskWithCheck;
use App\Entity\Task\TaskSet;
use App\Entity\Task\TaskType1;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TaskSet|null find($id, $lockMode = null, $lockVersion = null)
 * @method TaskSet|null findOneBy(array $criteria, array $orderBy = null)
 * @method TaskSet[]    findAll()
 * @method TaskSet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskSetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TaskSet::class);
    }

    public function findNotDeleted(int $id)
    {
        return $this->createQueryBuilder('ts')
            ->select('ts.blockId, ts.variety')
            ->where('ts.deleted = 0')
            ->andWhere('ts.id = :id')
            ->setParameter('id', $id)
            ->andWhere('ts.variety = 1')
            ->groupBy('ts.blockId')
            ->getQuery()
            ->getResult();
    }

    public function findByVarietyAndBlockIdLastPosition(int $variety, int $blockId)
    {
        $res = $this->createQueryBuilder('ts')
            ->select('ts.positionInTask')
            ->andWhere('ts.variety = :variety')
            ->setParameter('variety', $variety)
            ->andWhere('ts.blockId = :blockId')
            ->setParameter('blockId', $blockId)
            ->andWhere('ts.deleted = 0')
            ->orderBy('ts.positionInTask')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        return $res[0] ?? 0;
    }

}

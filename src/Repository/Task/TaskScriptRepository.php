<?php

namespace App\Repository\Task;

use App\Entity\Task\TaskScript;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TaskScript|null find($id, $lockMode = null, $lockVersion = null)
 * @method TaskScript|null findOneBy(array $criteria, array $orderBy = null)
 * @method TaskScript[]    findAll()
 * @method TaskScript[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskScriptRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TaskScript::class);
    }

    // /**
    //  * @return TaskScript[] Returns an array of TaskScript objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TaskScript
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

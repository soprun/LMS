<?php

namespace App\Repository\Task;

use App\Entity\BaseBlock\BaseBlockTaskWithCheck;
use App\Entity\Courses\Course;
use App\Entity\Task\AnswerTaskType1;
use App\Entity\Task\TaskSet;
use App\Entity\Task\TaskType1;
use App\Entity\User;
use App\Entity\UserGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\Expr\Join;

/**
 * @method AnswerTaskType1|null find($id, $lockMode = null, $lockVersion = null)
 * @method AnswerTaskType1|null findOneBy(array $criteria, array $orderBy = null)
 * @method AnswerTaskType1[]    findAll()
 * @method AnswerTaskType1[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnswerTaskType1Repository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AnswerTaskType1::class);
    }

    public function getSumOfAnswersByStatus(TaskType1 $task, ?User $reviewer, bool $isChecked = false)
    {
        $query = $this->createQueryBuilder('a')
            ->select('COUNT(a.id)')
            ->andWhere('a.task = :task')
            ->setParameter('task', $task);

        if ($isChecked) {
            $query->andWhere('a.isChecked is null OR a.isChecked = 0');
        }

        if ($reviewer) {
            $query->andWhere('a.validating = :validating')
                ->setParameter('validating', $reviewer);
        }

        try {
            return $query->getQuery()->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
            return 0;
        } catch (NoResultException $e) {
            return 0;
        }
    }

    public function findByTask1IdCountOfCheckedAnswers(int $idTaskBlock)
    {
        $result = $this->createQueryBuilder('a')
            ->innerJoin('a.task', 'task')
            ->select('COUNT(a.id)')
            ->andWhere('task.id = :idTaskBlock')
            ->setParameter('idTaskBlock', $idTaskBlock)
            ->andWhere('a.isChecked = 1')
            ->getQuery();

        try {
            return $result->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
            return 0;
        } catch (NoResultException $e) {
            return 0;
        }
    }

    public function findByTask1IdAndValidatingCountOfCheckedAnswers(int $idTaskBlock, User $validating)
    {
        $result = $this->createQueryBuilder('a')
            ->innerJoin('a.task', 't')
            ->innerJoin('a.user', 'u')
            ->select('COUNT(a.id)')
            ->andWhere('t.id = :idTaskBlock')
            ->setParameter('idTaskBlock', $idTaskBlock)
            ->andWhere('a.validating = :validating')
            ->setParameter('validating', $validating)
            ->andWhere('a.isChecked = 1')
            ->getQuery();

        try {
            return $result->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
            return 0;
        } catch (NoResultException $e) {
            return 0;
        }
    }

    public function findByTask1IdOneAnswers(TaskType1 $task)
    {
        $res = $this->createQueryBuilder('a')
            ->andWhere('a.task = :task')
            ->setParameter('task', $task)
            ->andWhere('a.isChecked is null OR a.isChecked = 0')
            ->orderBy('a.createdAt', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        return $res ? $res[0] : null;
    }


    public function findByTask1UserAnswer(TaskType1 $task, User $userInAnswer)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.task = :task')
            ->setParameter('task', $task)
            ->andWhere('a.user = :user')
            ->setParameter('user', $userInAnswer)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findByTaskBlockOneUserWithNotCheckedAnswer(
        BaseBlockTaskWithCheck $taskBlock,
        User $validating,
        ?int $userInSearch = null
    ) {
        $date = new \DateTime();
        $date->sub(new \DateInterval('PT30M'));

        $res = $this->createQueryBuilder('a')

            //->select('u.id')

            // Находим ответ по блоку
            ->innerJoin('a.task', 't')
            ->innerJoin(TaskSet::class, 's', 'WITH', 's.blockId = t.id')
            ->innerJoin('s.blockTaskWithCheck', 'tb')
            ->innerJoin('a.user', 'u')
            ->andWhere('tb = :taskBlock')
            ->setParameter('taskBlock', $taskBlock)
            ->andWhere('a.isChecked is null OR a.isChecked = 0')
            ->andWhere('a.tookToCheck is null OR a.validating = :validating OR a.tookToCheck <= :tookToCheck')
            ->setParameter('tookToCheck', $date)
            ->setParameter('validating', $validating)
            ->orderBy('a.createdAt', 'ASC')
            ->setMaxResults(1);
        if ($userInSearch !== null) {
            $res->andWhere('u.id = :userId')
                ->setParameter('userId', $userInSearch);
        }

        try {
            return $res->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findByTaskBlockAndValidatingOneUserWithNotCheckedAnswer(
        BaseBlockTaskWithCheck $taskBlock,
        User $validating,
        ?int $userInSearch = null
    ) {
        $date = new \DateTime();
        $date->sub(new \DateInterval('PT30M'));

        $res = $this->createQueryBuilder('a')

            //->select('u.id')

            // Находим ответ по блоку
            ->innerJoin('a.task', 't')
            ->innerJoin(TaskSet::class, 's', 'WITH', 's.blockId = t.id')
            ->innerJoin('s.blockTaskWithCheck', 'tb')
            ->andWhere('tb = :taskBlock')
            ->setParameter('taskBlock', $taskBlock)
            ->andWhere('a.isChecked is null OR a.isChecked = 0')
            ->andWhere('a.tookToCheck is null OR a.validating = :validating OR a.tookToCheck <= :tookToCheck')
            ->setParameter('tookToCheck', $date)
            ->setParameter('validating', $validating)
            ->orderBy('a.createdAt', 'ASC')
            ->setMaxResults(1);
        if ($userInSearch !== null) {
            $res->andWhere('u.id = :userId')
                ->setParameter('userId', $userInSearch);
        }

        try {
            return $res->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findAnswersByTaskBlock(
        BaseBlockTaskWithCheck $taskBlock,
        ?User $reviewer,
        ?string $status = null,
        $dateStart = null,
        $dateEnd = null,
        $page = 1
    ) {
        /** @var QueryBuilder $query  */
        $query = $this->createQueryBuilder('ans')
            ->select('DISTINCT(u.id) as id', 'ans.createdAt')
            ->innerJoin('ans.task', 't')
            ->innerJoin('ans.user', 'u')
            ->innerJoin(TaskSet::class, 's', 'WITH', 's.blockId = t.id')
            ->innerJoin('s.blockTaskWithCheck', 'tb')
            ->andWhere('tb = :taskBlock')
            ->setParameter('taskBlock', $taskBlock);

        if ($reviewer) {
            $query->andWhere('ans.validating = :validating')
                ->setParameter('validating', $reviewer);
        }

        if ($dateStart) {
            $query->andWhere('ans.createdAt >= :dateStart')
                ->setParameter('dateStart', $dateStart);
        }

        if ($dateEnd) {
            $query->andWhere('ans.createdAt <= :dateEnd')
                ->setParameter('dateEnd', $dateEnd);
        }

        if (!is_null($status)) {
            if ($status === 'true') {
                $query->andWhere('ans.isChecked = 1');
            } else {
                $query->andWhere('ans.isChecked = 0 AND ans.isChecked IS NULL');
            }
        }

        return $query->orderBy('ans.createdAt', 'DESC')
            ->setFirstResult(($page - 1) * 20)
            ->setMaxResults(20)
            ->getQuery()
            ->getResult();
    }

    public function getUserPointsSumArray(array $userIds, array $parameters)
    {
        return $this->createQueryBuilder('a')
            ->select('u.id AS userId, SUM(a.points) AS sum')
            ->innerJoin('a.user', 'u')
            ->andWhere('a.user IN(:userIds)')
            ->setParameter('userIds', $userIds)
            ->andWhere('a.createdAt >= :date1')
            ->setParameter('date1', $parameters['date1'])
            ->andWhere('a.createdAt <= :date2')
            ->setParameter('date2', $parameters['date2'])
            ->groupBy('a.user')
            ->getQuery()
            ->getResult();
    }

    public function getTaskOrderByValidators(array $parameters)
    {
        $query = $this->createQueryBuilder('a')
            ->select(
                'a.id AS answerId',
                'student.authUserId AS studentAuthUserId',
                'validator.authUserId AS validatorAuthUserId',
                'a.tookToCheck AS tookToCheck',
                'courses_course.id AS courseId',
                'courses_course.name AS courseName',
                'g.id AS groupId',
                'g.name AS groupName',
                'courses_lesson.id AS lessonId',
                'courses_lesson.name AS lessonName',
                'block_task_with_check.id AS taskBlockId',
                'task_sets.variety AS taskBlockVariety',
            )
            ->where('a.tookToCheck >= :date1')
            ->setParameter('date1', $parameters['date1'] . ' 00:00:00')
            ->andWhere('a.tookToCheck <= :date2')
            ->setParameter('date2', $parameters['date2'] . ' 23:59:59')
            ->innerJoin('a.user', 'student')
            ->innerJoin('a.validating', 'validator')
            ->innerJoin(
                Course::class,
                'courses_course',
                Join::WITH,
                'courses_course.deleted = 0'
            )
            ->innerJoin(
                'courses_course.lessons',
                'courses_lesson',
                Join::WITH,
                'courses_lesson.deleted = 0'
            )
            ->innerJoin('courses_lesson.lessonParts', 'courses_lesson_part')
            ->innerJoin(
                'courses_lesson_part.lessonPartBlocks',
                'courses_lesson_part_block',
                Join::WITH,
                'courses_lesson_part_block.deleted = 0 AND
                courses_lesson_part_block.type = 2 AND courses_lesson_part_block.variety = 1'
            )
            ->innerJoin(
                BaseBlockTaskWithCheck::class,
                'block_task_with_check',
                Join::WITH,
                'block_task_with_check.id = courses_lesson_part_block.blockId'
            )
            ->innerJoin(
                TaskSet::class,
                'task_sets',
                Join::WITH,
                'task_sets.blockTaskWithCheck = block_task_with_check AND task_sets.variety = 1'
            )
            ->innerJoin(
                TaskType1::class,
                'courses_task_type_1',
                Join::WITH,
                'courses_task_type_1.id = task_sets.blockId AND courses_task_type_1.isAutoCheck = false'
            )
            ->innerJoin('courses_task_type_1.answers', 'cat', Join::WITH, 'a.id = cat.id')
            ->innerJoin(UserGroup::class, 'g')
            ->innerJoin(
                'g.userRelations',
                'gu',
                Join::WITH,
                'gu.user = student and gu.deleted = 0'
            )
            ->innerJoin('g.courses', 'c', Join::WITH, 'c = courses_course')
            ->andWhere('task_sets.deleted = 0')
            ->andWhere('validator IS NOT null');


        if ($parameters['course'] != null) {
            $query->andWhere('courses_course.id = :course');
            $query->setParameter('course', $parameters['course']);
        }

        return $query->getQuery()->getResult();
    }
}

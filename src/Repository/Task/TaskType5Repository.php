<?php

namespace App\Repository\Task;

use App\Entity\Task\TaskType5;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TaskType5|null find($id, $lockMode = null, $lockVersion = null)
 * @method TaskType5|null findOneBy(array $criteria, array $orderBy = null)
 * @method TaskType5[]    findAll()
 * @method TaskType5[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskType5Repository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TaskType5::class);
    }

    // /**
    //  * @return TaskType5[] Returns an array of TaskType5 objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TaskType5
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

<?php

namespace App\Repository\Task;

use App\Entity\Task\AnswerTaskType5;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class AnswerTaskType5Repository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AnswerTaskType5::class);
    }

    public function getUserPointsSumArray(array $userIds, array $parameters)
    {
        return $this->createQueryBuilder('a')
            ->select('u.id AS userId, SUM(a.points) AS sum')
            ->innerJoin('a.user', 'u')
            ->andWhere('a.user IN(:userIds)')
            ->setParameter('userIds', $userIds)
            ->andWhere('a.createdAt >= :date1')
            ->setParameter('date1', $parameters['date1'])
            ->andWhere('a.createdAt <= :date2')
            ->setParameter('date2', $parameters['date2'])
            ->groupBy('a.user')
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return AnswerTaskType5[] Returns an array of AnswerTaskType5 objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AnswerTaskType5
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

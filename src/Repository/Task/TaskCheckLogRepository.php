<?php

namespace App\Repository\Task;

use App\Entity\Task\TaskCheckLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TaskCheckLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method TaskCheckLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method TaskCheckLog[]    findAll()
 * @method TaskCheckLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskCheckLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TaskCheckLog::class);
    }

    // /**
    //  * @return TaskCheckLog[] Returns an array of TaskCheckLog objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TaskCheckLog
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

<?php

namespace App\Repository\Task;

use App\Entity\Task\TaskTest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TaskTest|null find($id, $lockMode = null, $lockVersion = null)
 * @method TaskTest|null findOneBy(array $criteria, array $orderBy = null)
 * @method TaskTest[]    findAll()
 * @method TaskTest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskTestRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TaskTest::class);
    }

    // /**
    //  * @return TaskTest[] Returns an array of TaskTest objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TaskTest
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

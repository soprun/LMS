<?php

namespace App\Repository;

use App\Entity\User;
use App\Entity\UserGroupFolder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method UserGroupFolder|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserGroupFolder|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserGroupFolder[]    findAll()
 * @method UserGroupFolder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserGroupFolderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserGroupFolder::class);
    }

    public function findAllFoldersWithoutParent()
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.parent is null')
            ->getQuery()
            ->getResult();
    }

    public function findAllFoldersWithoutParentByUser(User $user)
    {
        return $this->createQueryBuilder('f')
            ->innerJoin('f.userGroups', 'g')
            ->innerJoin('g.userRelations', 'ur')
            ->andWhere('f.parent is null')
            ->andWhere('ur.user = :user')
            ->setParameter('user', $user)
            ->andWhere('ur.deleted = 0')
            ->getQuery()
            ->getResult();
    }

}

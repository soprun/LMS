<?php

namespace App\Repository;

use App\Entity\BusinessPhoto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BusinessPhoto|null find($id, $lockMode = null, $lockVersion = null)
 * @method BusinessPhoto|null findOneBy(array $criteria, array $orderBy = null)
 * @method BusinessPhoto[]    findAll()
 * @method BusinessPhoto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BusinessPhotoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BusinessPhoto::class);
    }

    // /**
    //  * @return BusinessPhoto[] Returns an array of BusinessPhoto objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BusinessPhoto
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

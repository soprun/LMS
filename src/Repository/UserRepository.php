<?php

namespace App\Repository;

use App\DTO\MarathonInvestment\MarathonInvestmentMembersFiltersDTO;
use App\DTO\UserLocationDto;
use App\Entity\AbstractCourse;
use App\Entity\CourseStream;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Найти спикеров по имени
     */
    public function findByGroupNameUsers(string $groupName)
    {
        $qb = $this->createQueryBuilder('u')
            ->select('u.authUserId', 'u.id')
            ->innerJoin('u.groupRelations', 'ur')
            ->innerJoin('ur.userGroup', 'ug')
            ->andWhere('ug.name = :groupName')
            ->andWhere('ur.deleted = 0')
            ->setParameter('groupName', $groupName);

        return $qb->getQuery()->getResult();
    }

    /**
     * @return array
     */
    public function getAllUsersWithGroupsArray(): array
    {
        return $this->createQueryBuilder('u')
            ->select('u.authUserId, ug.id as groupId')
            ->leftJoin('u.groupRelations', 'ur', Join::WITH, 'ur.deleted = 0')
            ->leftJoin('ur.userGroup', 'ug')
            ->getQuery()->getResult();
    }

    /**
     * @param null|int $stream
     * @param bool $withoutMarkedNiche
     * @param array $sorted
     * @param $page
     * @param $pageSize
     *
     * @return ArrayCollection|User[]
     * @deprecated use getMarathonInvestmentMembers instead
     */
    public function getUsersInMarafonInvesment(
        ?int $stream,
        bool $withoutMarkedNiche,
        array $sorted,
        $page,
        $pageSize
    ): ArrayCollection {
        $query = $this->getUserInMarafonInvestementQuery($stream, $withoutMarkedNiche);
        if ($sorted) {
            foreach ($sorted as $key => $value) {
                if (in_array($key, ['pointA', 'profit']) && in_array($value, ['DESC', 'ASC'])) {
                    $query->addOrderBy('conf.' . $key, (string)$value);
                }
            }
        }
        $query
            ->setFirstResult(($page - 1) * $pageSize)
            ->setMaxResults($pageSize)
            ->addOrderBy('conf.id', 'ASC');

        return new ArrayCollection($query->getQuery()->getResult());
    }

    /**
     * @return string[]
     * @deprecated use getMarathonInvestmentMembers instead
     */
    public function getUsersInMarafonInvesmentAuthIds(?int $stream, bool $withoutMarkedNiche): array
    {
        $data = $this->getUserInMarafonInvestementQuery($stream, $withoutMarkedNiche)
            ->select('u.authUserId')->getQuery()->getScalarResult();

        return array_column($data, 'authUserId');
    }

    /** @deprecated */
    private function getUserInMarafonInvestementQuery(?int $stream, bool $withoutMarkedNiche): QueryBuilder
    {
        $query = $this
            ->createQueryBuilder('u')
            ->innerJoin('u.groupRelations', 'ur')
            ->innerJoin('ur.userGroup', 'ug')
            ->innerJoin('ug.courseStream', 'cs')
            ->innerJoin('u.marathonConfigurations', 'conf')
            ->where('ur.deleted = 0');

        if ($withoutMarkedNiche) {
            $query->leftJoin('u.niches', 'niches')
                ->having('COUNT(CASE niches.marked WHEN 1 THEN 1 ELSE :null END) = 0')
                ->setParameter('null', null)
                ->groupBy('u.id', 'conf.id');
        }

        if ($stream) {
            $query
                ->andWhere('cs.id = :stream')
                ->setParameter('stream', $stream);
        } else {
            $query
                ->innerJoin('cs.abstractCourse', 'ac')
                ->andWhere('ac.slug = :slug')
                ->setParameter('slug', AbstractCourse::SLUG_MARATHON_INVESTMENT);
        }

        return $query;
    }

    public function getMarathonInvestmentMembers(MarathonInvestmentMembersFiltersDTO $filtersDTO): ArrayCollection
    {
        $qb = $this->makeMarathonMembersQuery($filtersDTO);
        foreach ($filtersDTO->sorts as $field) {
            if (str_starts_with($field, '-')) {
                $sort = 'DESC';
                $field = substr($field, 1);
            } else {
                $sort = 'ASC';
            }
            if (in_array($field, ['pointA', 'profit'])) {
                $qb->addOrderBy('conf.' . $field, $sort);
            }
        }
        $qb
            ->setFirstResult($filtersDTO->getFirstResult())
            ->setMaxResults($filtersDTO->pageSize)
            ->addOrderBy('conf.id', 'ASC');

        return new ArrayCollection($qb->getQuery()->getResult());
    }

    public function makeMarathonMembersQuery(MarathonInvestmentMembersFiltersDTO $filtersDTO): QueryBuilder
    {
        $qb = $this->createQueryBuilder('u')
            ->innerJoin('u.groupRelations', 'ur')
            ->innerJoin('ur.userGroup', 'ug')
            ->innerJoin('ug.courseStream', 'cs')
            ->innerJoin('u.marathonConfigurations', 'conf')
            ->leftJoin('u.businessNiches', 'businessNiches')
            ->where('ur.deleted = 0')
            ->innerJoin('cs.abstractCourse', 'ac')
            ->andWhere('ac.slug = :slug')
            ->setParameter('slug', AbstractCourse::SLUG_MARATHON_INVESTMENT);

        if ($filtersDTO->hasVerified !== null) {
            $qb
                ->having('COUNT(CASE businessNiches.verified WHEN 1 THEN 1 ELSE :null END) <> 0')
                ->setParameter('null', null)
                ->groupBy('u.id', 'conf.id');
        }
        if ($filtersDTO->stream !== null) {
            $qb
                ->andWhere('cs.id = :stream')
                ->setParameter('stream', $filtersDTO->stream);
        }

        return $qb;
    }

    public function getMarathonInvestmentMembersCount(MarathonInvestmentMembersFiltersDTO $filtersDTO): int
    {
        $qb = $this->makeMarathonMembersQuery($filtersDTO);
        $qb->select('COUNT(u.id)');

        /** @noinspection PhpUnhandledExceptionInspection always has 1 scalar result in this context */
        return $qb->getQuery()->getSingleScalarResult();
    }

    /** @return int[] */
    public function getUserIdsWhichHasTractionValue(DateTime $from, DateTime $to): array
    {
        $qb = $this->createQueryBuilder('user')
            ->select('user.id')
            ->innerJoin('user.tractionValues', 'tractionValues')
            ->where('tractionValues.date BETWEEN :from AND :to')
            ->setParameter('from', $from)
            ->setParameter('to', $to)
            ->addGroupBy('tractionValues.owner');

        return array_column($qb->getQuery()->getResult(), 'id');
    }

    public function getClosestUsersToThePoint(UserLocationDto $userLocationDto): array
    {
        $longitudeFrom = $userLocationDto->getLongitude() - $userLocationDto->getLongitudeDelta();
        $longitudeTo = $userLocationDto->getLongitude() + $userLocationDto->getLongitudeDelta();
        $latitudeFrom = $userLocationDto->getLatitude() - $userLocationDto->getLatitudeDelta();
        $latitudeTo = $userLocationDto->getLatitude() + $userLocationDto->getLatitudeDelta();
        $pointSizeLongitudeFactor = (int)strpos(strrev($userLocationDto->getPointSizeLongitude()), ".");
        $pointSizeLatitudeFactor = (int)strpos(strrev($userLocationDto->getPointSizeLatitude()), ".");

        return $this->createQueryBuilder('u')
            ->addSelect("round(x(u.location),$pointSizeLongitudeFactor) as lon")
            ->addSelect("round(y(u.location),$pointSizeLatitudeFactor) as lat")
            ->where('x(u.location) between :xFrom and :xTo')
            ->andWhere('y(u.location) between :yFrom and :yTo')
            ->setParameter('xFrom', $longitudeFrom)
            ->setParameter('xTo', $longitudeTo)
            ->setParameter('yFrom', $latitudeFrom)
            ->setParameter('yTo', $latitudeTo)
            ->getQuery()
            ->getResult();
    }

    public function getUsersAtPoint(UserLocationDto $userLocationDto): array
    {
        $longitudeRounded = $userLocationDto->getLongitude();
        $latitudeRounded = $userLocationDto->getLatitude();
        $pointSizeLongitudeFactor = (int)strpos(strrev($userLocationDto->getPointSizeLongitude()), ".");
        $pointSizeLatitudeFactor = (int)strpos(strrev($userLocationDto->getPointSizeLatitude()), ".");

        return $this->createQueryBuilder('u')
            ->where("round(x(u.location),$pointSizeLongitudeFactor) = :x")
            ->andWhere("round(y(u.location),$pointSizeLatitudeFactor) = :y")
            ->setParameter('x', $longitudeRounded)
            ->setParameter('y', $latitudeRounded)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return CourseStream[]
     */
    public function getByCourseStream(CourseStream $stream): array
    {
        return $this->createQueryBuilder('user')
            ->join('user.groupRelations', 'groupRelation')
            ->join('groupRelation.userGroup', 'userGroup')
            ->where('groupRelation.deleted = 0 and userGroup.courseStream = :courseStream')
            ->setParameter('courseStream', $stream)
            ->getQuery()
            ->getResult();
    }

}

<?php

namespace App\Repository;

use App\Entity\BusinessArea;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BusinessArea|null find($id, $lockMode = null, $lockVersion = null)
 * @method BusinessArea|null findOneBy(array $criteria, array $orderBy = null)
 * @method BusinessArea[]    findAll()
 * @method BusinessArea[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class BusinessAreaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BusinessArea::class);
    }
}

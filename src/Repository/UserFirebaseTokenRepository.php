<?php

namespace App\Repository;

use App\Entity\UserFirebaseToken;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserFirebaseToken|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserFirebaseToken|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserFirebaseToken[]    findAll()
 * @method UserFirebaseToken[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserFirebaseTokenRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserFirebaseToken::class);
    }

    // /**
    //  * @return UserFirebaseToken[] Returns an array of UserFirebaseToken objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserFirebaseToken
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

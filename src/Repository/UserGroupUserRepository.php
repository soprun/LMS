<?php

namespace App\Repository;

use App\Entity\AbstractCourse;
use App\Entity\User;
use App\Entity\UserGroupUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserGroupUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserGroupUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserGroupUser[]    findAll()
 * @method UserGroupUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserGroupUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserGroupUser::class);
    }

    /**
     * @param User $user
     * @param array $previousStreams
     *
     * @return bool
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function checkUserInStream(User $user, array $previousStreams): bool
    {
        return (bool)$this->createQueryBuilder('userRel')
            ->select('count(userRel.userGroup)')
            ->innerJoin('userRel.userGroup', 'userGroup')
            ->where('userRel.deleted = 0')
            ->andWhere('userRel.user = :user and userGroup.courseStream in (:courseStreams)')
            ->setParameter('user', $user)
            ->setParameter('courseStreams', $previousStreams)
            ->getQuery()
            ->getSingleScalarResult();
    }


    public function getUserInGroupButNotTeamInfo(
        array $userInTeamIds,
        ?int $courseStreamId = null,
        ?string $facultyName = null
    ): array {
        $qb = $this->createQueryBuilder('ugu')
            ->select(
                'u.id AS userId',
                'u.authUserId AS authUserId',
                'ug.id AS groupId',
                'ug.name AS groupName',
                'f.name AS faculty',
                'u.verified as verified'
            )
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ug.courseStream', 'cs')
            ->innerJoin('cs.abstractCourse', 'ac')
            ->innerJoin('ugu.user', 'u')
            ->leftJoin('ugu.faculty', 'f')
            ->andWhere("ac.slug IN(:slugs)")
            ->andWhere('cs.active = 1')
            ->andWhere('ugu.deleted = 0')
            ->andWhere("ug.name NOT LIKE '%предоплат%'")
            ->andWhere("ug.name NOT LIKE '%сборн%' and ug.name NOT LIKE '%факульт%'")
            ->setParameters(
                ['slugs' => [AbstractCourse::SLUG_HUNDRED, AbstractCourse::SLUG_SPEED, AbstractCourse::SLUG_SPEED_CLUB]]
            );

        if (!empty($userInTeamIds)) {
            $qb->andWhere('u.id NOT IN(:inTeams)')
                ->setParameter('inTeams', $userInTeamIds);
        }

        if ($courseStreamId) {
            $qb->andWhere('cs.id = :csId')
                ->setParameter('csId', $courseStreamId);
        }

        if ($facultyName) {
            $qb->andWhere('f.name = :facultyName')
                ->setParameter('facultyName', $facultyName);
        }

        return $qb->getQuery()->getResult();
    }


    /** @return int[] */
    public function getAuthUserIdsByGroupId(int $groupId): array
    {
        $data = $this->createQueryBuilder('ugu')
            ->select('u.authUserId')
            ->innerJoin('ugu.user', 'u')
            ->innerJoin('ugu.userGroup', 'ug')
            ->andWhere('ugu.deleted = 0')
            ->andWhere('ug.id = :groupId')
            ->setParameter('groupId', $groupId)
            ->groupBy('u.authUserId')
            ->getQuery()
            ->getResult();

        return array_column($data, 'authUserId');
    }

}

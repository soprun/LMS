<?php

namespace App\Repository;

use App\Entity\UserCertificates;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserCertificates|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserCertificates|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserCertificates[]    findAll()
 * @method UserCertificates[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserCertificatesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserCertificates::class);
    }

    // /**
    //  * @return UserCertificates[] Returns an array of UserCertificates objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserCertificates
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

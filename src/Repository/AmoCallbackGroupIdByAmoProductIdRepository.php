<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\AmoCallbackGroupIdByAmoProductId;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\ParameterType;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AmoCallbackGroupIdByAmoProductId|null find($id, $lockMode = null, $lockVersion = null)
 * @method AmoCallbackGroupIdByAmoProductId|null findOneBy(array $criteria, array $orderBy = null)
 * @method AmoCallbackGroupIdByAmoProductId[] findAll()
 * @method AmoCallbackGroupIdByAmoProductId[] findBy()
 */
final class AmoCallbackGroupIdByAmoProductIdRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AmoCallbackGroupIdByAmoProductId::class);
    }

    /**
     * @param int $productId
     *
     * @return AmoCallbackGroupIdByAmoProductId|null
     * @throws NonUniqueResultException
     */
    public function findOneByProductIdField(int $productId): ?AmoCallbackGroupIdByAmoProductId
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.productId = :productId')
            ->setParameter('productId', $productId, ParameterType::INTEGER)
            ->getQuery()
            ->getOneOrNullResult(AbstractQuery::HYDRATE_OBJECT);
    }

}

<?php

namespace App\Repository;

use App\Entity\Faculty;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Faculty|null find($id, $lockMode = null, $lockVersion = null)
 * @method Faculty|null findOneBy(array $criteria, array $orderBy = null)
 * @method Faculty[]    findAll()
 * @method Faculty[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FacultyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Faculty::class);
    }

    /** @return array<string, string>[] */
    public function getUniqueFacultyNames(): array
    {
        return $this->createQueryBuilder('f')
            ->select('DISTINCT(f.name) as name')
            ->getQuery()
            ->getResult();
    }

    /** @return array<string, string>[] */
    public function getFacultyNamesOfCourseStream(int $courseStreamId): array
    {
        return $this->createQueryBuilder('f')
            ->select('DISTINCT(f.name) as name')
            ->join('f.courseStream', 'cs')
            ->andWhere('cs.id = :csId')
            ->setParameter('csId', $courseStreamId)
            ->getQuery()
            ->getResult();
    }

}

<?php

namespace App\Repository\Traction;

use App\Entity\Traction\Value;
use App\Entity\User;
use DateTimeInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Value|null find($id, $lockMode = null, $lockVersion = null)
 * @method Value|null findOneBy(array $criteria, array $orderBy = null)
 * @method Value[]    findAll()
 * @method Value[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ValueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Value::class);
    }

    /** @return Value[] */
    public function getUserValuesByDate(User $user, DateTimeInterface $from, DateTimeInterface $to = null): array
    {
        if ($to === null) {
            $to = $from;
        }
        $qb = $this->createQueryBuilder('value')
            ->andWhere('value.owner = :owner')->setParameter('owner', $user)
            ->andWhere('value.date BETWEEN :from AND :to')
            ->setParameter('from', $from)
            ->setParameter('to', $to);

        return $qb->getQuery()->getResult();
    }

    public function getTractionValuesByTeams(array $teamIds): array
    {
        $qb = $this->createQueryBuilder('value');
        $qb->select('teams.id AS teamId, type.title AS valueType, SUM(value.value) AS sum')
            ->innerJoin('value.owner', 'owner')
            ->innerJoin('owner.teams', 'teams')
            ->innerJoin('value.type', 'type')
            ->andWhere('teams IN (:teamsIds)')
            ->setParameter('teamsIds', $teamIds)
            ->addGroupBy('teams.id')
            ->addGroupBy('type.title');

        return $qb->getQuery()->getResult();
    }

    public function getTractionValuesByUsers(array $userIds): array
    {
        $qb = $this->createQueryBuilder('value');
        $qb->select('owner.id AS userId, type.title AS valueType, SUM(value.value) AS sum')
            ->innerJoin('value.owner', 'owner')
            ->innerJoin('value.type', 'type')
            ->andWhere('owner IN (:userIds)')
            ->setParameter('userIds', $userIds)
            ->addGroupBy('owner.id')
            ->addGroupBy('type.title');

        return $qb->getQuery()->getResult();
    }

}

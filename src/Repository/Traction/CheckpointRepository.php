<?php

namespace App\Repository\Traction;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Traction\Checkpoint;
use Doctrine\ORM\Query\Expr\Join;
use App\Entity\CourseStream;
use App\Entity\User;

/**
 * @method Checkpoint|null find($id, $lockMode = null, $lockVersion = null)
 * @method Checkpoint|null findOneBy(array $criteria, array $orderBy = null)
 * @method Checkpoint[]    findAll()
 * @method Checkpoint[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CheckpointRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Checkpoint::class);
    }

    /**
     * @param  CourseStream  $stream
     * @param  null|User  $user
     * @return Checkpoint[]
     */
    public function getCheckpointByUserAndStream(CourseStream $stream, User $user = null): array
    {
        return $this->createQueryBuilder('checkpoint')
            ->addSelect('value')
            ->leftJoin('checkpoint.values', 'value', Join::WITH, 'value.user = :user')
            ->where('checkpoint.stream = :stream')
            ->setParameter('user', $user)
            ->setParameter('stream', $stream)
            ->orderBy('checkpoint.priority', 'DESC')
            ->getQuery()
            ->getResult();
    }

}

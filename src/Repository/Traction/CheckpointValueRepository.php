<?php

namespace App\Repository\Traction;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Traction\CheckpointValue;

/**
 * @method CheckpointValue|null find($id, $lockMode = null, $lockVersion = null)
 * @method CheckpointValue|null findOneBy(array $criteria, array $orderBy = null)
 * @method CheckpointValue[]    findAll()
 * @method CheckpointValue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CheckpointValueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CheckpointValue::class);
    }

}

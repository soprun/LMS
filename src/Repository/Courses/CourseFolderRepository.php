<?php

namespace App\Repository\Courses;

use App\Entity\Courses\CourseFolder;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CourseFolder|null find($id, $lockMode = null, $lockVersion = null)
 * @method CourseFolder|null findOneBy(array $criteria, array $orderBy = null)
 * @method CourseFolder[]    findAll()
 * @method CourseFolder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CourseFolderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CourseFolder::class);
    }

    public function findByUser(User $user)
    {
        return $this->createQueryBuilder('f')
            ->innerJoin('f.courses', 'c')
            ->innerJoin('c.userGroups', 'g')
            ->innerJoin('g.userRelations', 'ur')
            ->andWhere('ur.user = :user')
            ->setParameter('user', $user)
            ->andWhere('ur.deleted = 0')
            ->getQuery()
            ->getResult();
    }

    public function findByNotDeleted()
    {
        return $this->createQueryBuilder('f')
            ->where('f.deleted = 0')
            ->getQuery()
            ->getResult();
    }

    public function findByUserAndFolderIfExist(User $user, CourseFolder $courseFolder)
    {
        return $this->createQueryBuilder('f')
            ->innerJoin('f.courses', 'c')
            ->innerJoin('c.userGroups', 'g')
            ->innerJoin('g.userRelations', 'ur')
            ->andWhere('ur.user = :user')
            ->setParameter('user', $user)
            ->andWhere('ur.deleted = 0')
            ->andWhere('f = :courseFolder')
            ->setParameter('courseFolder', $courseFolder)
            ->getQuery()
            ->getResult();
    }

}

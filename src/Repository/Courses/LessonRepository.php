<?php

namespace App\Repository\Courses;

use App\Entity\BaseBlock\BaseBlockTaskWithCheck;
use App\Entity\Courses\Course;
use App\Entity\Courses\Lesson;
use App\Entity\Task\TaskSet;
use App\Entity\Task\TaskType1;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\Expr\Join;

class LessonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Lesson::class);
    }

    public function findByCourseLastPosition(Course $course)
    {
        $res = $this->createQueryBuilder('l')
            ->select('l.position')
            ->orderBy('l.position', 'DESC')
            ->andWhere('l.course = :course')
            ->setParameter('course', $course)
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        return $res[0]['position'] ?? 0;
    }

    /**
     * @param Course $course
     * @param bool $withDeleted
     *
     * @return array
     */
    public function findByCourse(Course $course, bool $withDeleted = false): array
    {
        $criteria = ['course' => $course];
        if (!$withDeleted) {
            $criteria['deleted'] = 0;
        }

        return $this->findBy($criteria);
    }

    public function findByCourseNotDeletedLessons(Course $course)
    {
        return $this->createQueryBuilder('l')
            ->select('l, lc, s, if, ug')
            ->leftJoin('l.lessonConfiguration', 'lc')
            ->leftJoin('l.speakers', 's')
            ->leftJoin('l.imgFile', 'if')
            ->leftJoin('l.userGroups', 'ug')
            ->andWhere('l.course = :course')
            ->setParameter('course', $course)
            ->andWhere('l.deleted = 0')
            ->orderBy('l.position', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findByCourseNotDeletedAndNotHiddenLessons(Course $course, User $user)
    {
        return $this->createQueryBuilder('l')
            ->select('l, lc, s, if, ug')
            ->innerJoin('l.lessonConfiguration', 'lc')
            ->leftJoin('l.speakers', 's')
            ->leftJoin('l.imgFile', 'if')
            ->leftJoin('l.userGroups', 'ug')
            ->leftJoin('ug.userRelations', 'ur')
            ->andWhere('l.course = :course')
            ->setParameter('course', $course)
            ->andWhere('ur.user = :user')
            ->setParameter('user', $user)
            ->andWhere('ur.deleted = 0')
            ->andWhere('l.deleted = 0')
            ->andWhere('lc.isHidden is null OR lc.isHidden = 0')
            ->orderBy('l.position', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findByCourseCountNotDeletedLessons(Course $course)
    {
        try {
            $res = $this->createQueryBuilder('l')
                ->select("COUNT(l.id)")
                ->andWhere('l.course = :course')
                ->setParameter('course', $course)
                ->andWhere('l.deleted = 0')
                ->getQuery()
                ->getSingleScalarResult();
        } catch (\Exception $e) {
            $res = null;
        }


        return $res;
    }

    public function findByCourseCountNotDeletedAndNotHiddenLessons(Course $course, User $user)
    {
        try {
            $res = $this->createQueryBuilder('l')
                ->innerJoin('l.lessonConfiguration', 'c')
                ->innerJoin('l.userGroups', 'ug')
                ->innerJoin('ug.userRelations', 'ur')
                ->select("COUNT(DISTINCT l.id)")
                ->andWhere('l.course = :course')
                ->setParameter('course', $course)
                ->andWhere('ur.user = :user')
                ->setParameter('user', $user)
                ->andWhere('ur.deleted = 0')
                ->andWhere('l.deleted = 0')
                ->andWhere('c.isHidden is null OR c.isHidden = 0')
                ->getQuery()
                ->getSingleScalarResult();
        } catch (\Exception $e) {
            $res = null;
        }


        return $res;
    }

    public function findByPositionNotDeletedLessons(Course $course, int $position)
    {
        try {
            $res = $this->createQueryBuilder('l')
                ->innerJoin('l.lessonConfiguration', 'c')
                ->select("l.id, l.name")
                ->andWhere('l.course = :course')
                ->setParameter('course', $course)
                ->andWhere('l.id = :position')
                ->setParameter('position', $position)
                ->andWhere('l.deleted = 0')
                ->getQuery()
                ->getSingleResult();
        } catch (\Exception $e) {
            $res = null;
        }

        return $res;
    }

    public function findByPositionNotDeletedAndNotHiddenLessons(Course $course, int $position)
    {
        try {
            $res = $this->createQueryBuilder('l')
                ->innerJoin('l.lessonConfiguration', 'c')
                ->select("l.id, l.name")
                ->andWhere('l.course = :course')
                ->setParameter('course', $course)
                ->andWhere('l.id = :position')
                ->setParameter('position', $position)
                ->andWhere('l.deleted = 0')
                ->andWhere('c.isHidden is null OR c.isHidden = 0')
                ->getQuery()
                ->getSingleResult();
        } catch (\Exception $e) {
            $res = null;
        }

        return $res;
    }

    public function findByCourseAndNotDeletedLessons(Course $course)
    {
        return $this->createQueryBuilder('l')
            ->innerJoin('l.lessonConfiguration', 'c')
            ->select("l.id, l.position")
            ->andWhere('l.course = :course')
            ->setParameter('course', $course)
            ->andWhere('l.deleted = 0')
            ->orderBy('l.position', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findByCourseNotDeletedAndNotHiddenMinLessons(Course $course)
    {
        return $this->createQueryBuilder('l')
            ->innerJoin('l.lessonConfiguration', 'c')
            ->select("l.id, l.position")
            ->andWhere('l.course = :course')
            ->setParameter('course', $course)
            ->andWhere('l.deleted = 0')
            ->andWhere('c.isHidden is null OR c.isHidden = 0')
            ->orderBy('l.position', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findByCourseAndPositionNotDeletedLessons(Course $course, int $position)
    {
        return $this->createQueryBuilder('l')
            ->orderBy('l.position', 'ASC')
            ->andWhere('l.course = :course')
            ->setParameter('course', $course)
            ->andWhere('l.position > :position')
            ->setParameter('position', $position)
            ->andWhere('l.deleted = 0')
            ->getQuery()
            ->getResult();
    }

    /**
     * Проверить существование спикера
     */
    public function findByLessonIsSpeakerExists(Lesson $lesson, User $user)
    {
        $qb = $this->createQueryBuilder('l')
            ->innerJoin("l.speakers", 's')
            ->andWhere('l = :lesson')
            ->setParameter('lesson', $lesson)
            ->andWhere('s = :speaker')
            ->setParameter('speaker', $user);

        return $qb->getQuery()->getResult();
    }

    public function findByCourseAndDateLessonsToMenu(Course $course, \DateTime $dateTime)
    {
        return $this->createQueryBuilder('l')
            ->innerJoin('l.lessonConfiguration', 'c')
            ->innerJoin('l.module', 'm')
            ->orderBy('l.position', 'ASC')
            ->andWhere('m.course = :course')
            ->setParameter('course', $course)
            ->andWhere('m.start <= :datetime AND m.finish >= :datetime')
            ->setParameter('datetime', $dateTime)
            ->orderBy('m.positionInCourse', 'ASC')
            ->andWhere('l.deleted = 0')
            ->andWhere('c.isHidden is null OR c.isHidden = 0')
            ->getQuery()
            ->getResult();
    }

    public function findByAnswer(int $answerId): ?array
    {
        return $this->createQueryBuilder('courses_lesson')
            ->select('courses_lesson.id')
            ->innerJoin('courses_lesson.lessonParts', 'courses_lesson_part')
            ->innerJoin(
                'courses_lesson_part.lessonPartBlocks',
                'courses_lesson_part_block',
                Join::WITH,
                'courses_lesson_part_block.type = 2 AND courses_lesson_part_block.variety = 1'
            )
            ->innerJoin(
                BaseBlockTaskWithCheck::class,
                'block_task_with_check',
                Join::WITH,
                'block_task_with_check.id = courses_lesson_part_block.blockId'
            )
            ->innerJoin(
                TaskSet::class,
                'task_sets',
                Join::WITH,
                'task_sets.blockTaskWithCheck = block_task_with_check AND task_sets.variety = 1'
            )
            ->innerJoin(
                TaskType1::class,
                'courses_task_type_1',
                Join::WITH,
                'courses_task_type_1.id = task_sets.blockId AND courses_task_type_1.isAutoCheck = false'
            )
            ->innerJoin('courses_task_type_1.answers', 'cat', Join::WITH, 'cat.id = :answerId')
            ->where('courses_lesson.deleted = 0')
            ->andWhere('courses_lesson_part_block.deleted = 0')
            ->andWhere('task_sets.deleted = 0')
            ->setParameter('answerId', $answerId)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param Course $course
     * @param User $user
     *
     * @return int
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getCountLessonsByCourse(Course $course, User $user): int
    {
        return $this->createQueryBuilder('l')
            ->select('count(distinct l.id)')
            ->leftJoin('l.userGroups', 'ug')
            ->leftJoin('ug.userRelations', 'ur')
            ->andWhere('l.course = :course')
            ->setParameter('course', $course)
            ->andWhere('ur.user = :user')
            ->setParameter('user', $user)
            ->andWhere('ur.deleted = 0')
            ->andWhere('l.deleted = 0')
            ->getQuery()
            ->getSingleScalarResult();
    }

}

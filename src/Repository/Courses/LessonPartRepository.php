<?php

namespace App\Repository\Courses;

use App\Entity\Courses\LessonPart;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method LessonPart|null find($id, $lockMode = null, $lockVersion = null)
 * @method LessonPart|null findOneBy(array $criteria, array $orderBy = null)
 * @method LessonPart[]    findAll()
 * @method LessonPart[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LessonPartRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LessonPart::class);
    }

}

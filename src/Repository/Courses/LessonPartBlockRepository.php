<?php

namespace App\Repository\Courses;

use App\Entity\Courses\Course;
use App\Entity\Courses\Lesson;
use App\Entity\Courses\LessonPart;
use App\Entity\Courses\LessonPartBlock;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method LessonPartBlock|null find($id, $lockMode = null, $lockVersion = null)
 * @method LessonPartBlock|null findOneBy(array $criteria, array $orderBy = null)
 * @method LessonPartBlock[]    findAll()
 * @method LessonPartBlock[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LessonPartBlockRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LessonPartBlock::class);
    }

    public function findByPartLastPosition(LessonPart $part)
    {
        $res = $this->createQueryBuilder('lpb')
            ->select('lpb.positionInPart')
            ->orderBy('lpb.positionInPart', 'DESC')
            ->andWhere('lpb.part = :part')
            ->setParameter('part', $part)
            ->andWhere('lpb.deleted = 0')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        return $res[0]['positionInPart'] ?? 0;
    }

    public function findByPartNotDeletedBlocks(LessonPart $part)
    {
        return $this->createQueryBuilder('lpb')
            ->orderBy('lpb.positionInPart', 'ASC')
            ->andWhere('lpb.part = :part')
            ->setParameter('part', $part)
            ->andWhere('lpb.deleted = 0')
            ->getQuery()
            ->getResult();
    }

    public function findByPartAndPositionNotDeletedBlocks(LessonPart $part, int $position)
    {
        return $this->createQueryBuilder('lpb')
            ->orderBy('lpb.positionInPart', 'ASC')
            ->andWhere('lpb.part = :part')
            ->setParameter('part', $part)
            ->andWhere('lpb.positionInPart > :position')
            ->setParameter('position', $position)
            ->andWhere('lpb.deleted = 0')
            ->getQuery()
            ->getResult();
    }

    public function findByCourseTaskBlocks(Course $course)
    {
        return $this->createQueryBuilder('lpb')
            ->select('l.id as lessonId, lpb.blockId', 'lpb.variety', 'l.name')
            ->innerJoin('lpb.part', 'lp')
            ->innerJoin('lp.lesson', 'l')
            ->where('l.course = :course')
            ->setParameter('course', $course)
            ->andWhere('l.deleted = 0')
            ->andWhere('lpb.deleted = 0')
            ->andWhere('lpb.type = 2')
            ->getQuery()
            ->getResult();
    }

    public function findByLessonTaskBlocks(Lesson $lesson)
    {
        return $this->findByLessonsTaskBlocks([$lesson]);
    }

    public function findByLessonsTaskBlocks($lessons, bool $indexing = false)
    {
        $result = $this->createQueryBuilder('lpb')
            ->select('lpb, lp, l')
            ->innerJoin('lpb.part', 'lp')
            ->innerJoin('lp.lesson', 'l')
            ->andWhere('l IN (:lessons)')
            ->setParameter('lessons', $lessons)
            ->andWhere('lpb.deleted = 0')
            ->andWhere('lpb.type = 2')
            ->getQuery()
            ->getResult();
        if ($indexing) {
            $indexedResult = [];
            /** @var LessonPartBlock $item */
            foreach ($result as $item) {
                $indexedResult[$item->getPart()->getLesson()->getId()][] = $item;
            }

            return $indexedResult;
        }

        return $result;
    }

}

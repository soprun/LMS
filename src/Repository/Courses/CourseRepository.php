<?php

namespace App\Repository\Courses;

use App\Entity\Courses\Course;
use App\Entity\Courses\CourseFolder;
use App\Entity\Courses\LessonPartBlock;
use App\Entity\Task\AnswerTaskType1;
use App\Entity\Task\TaskSet;
use App\Entity\Task\TaskTest;
use App\Entity\Task\TaskType1;
use App\Entity\Task\TaskType5;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\Expr\Join;

/**
 * @method Course|null find($id, $lockMode = null, $lockVersion = null)
 * @method Course|null findOneBy(array $criteria, array $orderBy = null)
 * @method Course[]    findAll()
 * @method Course[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CourseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Course::class);
    }

    public function findByUserAndFolder(User $user, CourseFolder $courseFolder)
    {
        return $this->createQueryBuilder('c')
            ->innerJoin('c.userGroups', 'g')
            ->innerJoin('g.userRelations', 'ur')
            ->andWhere('ur.user = :user')
            ->setParameter('user', $user)
            ->andWhere('ur.deleted = 0')
            ->andWhere('c.folder = :folder')
            ->setParameter('folder', $courseFolder)
            ->andWhere('c.deleted = 0')
            ->getQuery()
            ->getResult();
    }

    public function findByUserAndCourseIfExist(User $user, Course $course)
    {
        return $this->createQueryBuilder('c')
            ->innerJoin('c.userGroups', 'g')
            ->innerJoin('g.userRelations', 'ur')
            ->andWhere('ur.user = :user')
            ->setParameter('user', $user)
            ->andWhere('ur.deleted = 0')
            ->andWhere('c = :course')
            ->setParameter('course', $course)
            ->getQuery()
            ->getResult();
    }

    /**
     * Считаем баллы пользователя для нужного курса
     * @param  Course  $course
     * @param  User  $user
     * @return int
     */
    public function getUserPointsSum(Course $course, User $user): int
    {
        $res = $this->createQueryBuilder('courses_course')
            ->select('sum(courses_answer_task_type_1.points)')
            ->innerJoin('courses_course.lessons', 'courses_lesson')
            ->innerJoin('courses_lesson.lessonParts', 'courses_lesson_part')
            ->innerJoin('courses_lesson_part.lessonPartBlocks', 'courses_lesson_part_block')
            ->innerJoin(
                TaskSet::class,
                'task_set',
                Join::WITH,
                'task_set.blockTaskWithCheck = courses_lesson_part_block.blockId'
            )
            ->innerJoin(
                TaskType1::class,
                'courses_task_type_1',
                Join::WITH,
                'courses_task_type_1.id = task_set.blockId'
            )
            ->innerJoin('courses_task_type_1.answers', 'courses_answer_task_type_1')
            ->innerJoin('courses_answer_task_type_1.user', 'user')
            ->andWhere('courses_answer_task_type_1.user = :user')
            ->setParameter('user', $user)
            ->andWhere('courses_course = :course')
            ->setParameter('course', $course);
        try {
            return $res->getQuery()->getSingleScalarResult() ? $res->getQuery()->getSingleScalarResult() : 0;
        } catch (NoResultException $e) {
            return 0;
        } catch (NonUniqueResultException $e) {
            return 0;
        }
    }

    /**
     * Считаем баллы и другие параметры тестов
     * @param  array  $courseIds
     * @param  User  $user
     * @param  array  $numbers
     * @return array
     */
    public function getUserTaskTestNumbers(array $courseIds, User $user, array $numbers)
    {
        $select = [];
        foreach ($numbers as $key => $value) {
            $select[] = sprintf(
                'SUM(courses_task_test_question_answer.%s) as %s',
                $key,
                $key
            );
        }

        $result = $this->createQueryBuilder('courses_course')
            ->select($select)
            ->innerJoin('courses_course.lessons', 'courses_lesson')
            ->innerJoin('courses_lesson.lessonParts', 'courses_lesson_part')
            ->innerJoin('courses_lesson_part.lessonPartBlocks', 'courses_lesson_part_block')
            ->innerJoin(
                TaskTest::class,
                'courses_task_test',
                Join::WITH,
                'courses_task_test.id = courses_lesson_part_block.blockId'
            )
            ->innerJoin('courses_task_test.taskTestQuestions', 'courses_task_test_question')
            ->innerJoin(
                'courses_task_test_question.taskTestQuestionUserAnswers',
                'courses_task_test_question_user_answer'
            )
            ->innerJoin('courses_task_test_question_user_answer.answer', 'courses_task_test_question_answer')
            ->andWhere('courses_task_test_question_user_answer.user = :user')
            ->setParameter('user', $user)
            ->andWhere('courses_course IN(:courseIds)')
            ->setParameter('courseIds', $courseIds)
            ->getQuery()
            ->getResult();

        foreach (reset($result) as $key => $value) {
            if ($value) {
                $numbers[$key] += (int)$value;
            }
        }

        return $numbers;
    }

    /**
     * Считаем баллы и другие параметры заданий с правильными ответами
     * @param  array  $courseIds
     * @param  User  $user
     * @param  array  $numbers
     * @return array
     */
    public function getUserTaskType5Numbers(array $courseIds, User $user, array $numbers)
    {
        $select = [];
        foreach ($numbers as $key => $value) {
            $select[] = sprintf(
                'SUM(courses_answer_task_type_5.%s) as %s',
                $key,
                $key
            );
        }

        $result = $this->createQueryBuilder('courses_course')
            ->select($select)
            ->innerJoin('courses_course.lessons', 'courses_lesson')
            ->innerJoin('courses_lesson.lessonParts', 'courses_lesson_part')
            ->innerJoin('courses_lesson_part.lessonPartBlocks', 'courses_lesson_part_block')
            ->innerJoin(
                TaskType5::class,
                'courses_task_type_5',
                Join::WITH,
                'courses_task_type_5.id = courses_lesson_part_block.blockId'
            )
            ->innerJoin('courses_task_type_5.answerTaskType5s', 'courses_answer_task_type_5')
            ->andWhere('courses_answer_task_type_5.user = :user')
            ->setParameter('user', $user)
            ->andWhere('courses_course IN(:courseIds)')
            ->setParameter('courseIds', $courseIds)
            ->getQuery()
            ->getResult();

        foreach (reset($result) as $key => $value) {
            if ($value) {
                $numbers[$key] += (int)$value;
            }
        }

        return $numbers;
    }

    public function findByAnswerTaskType1(AnswerTaskType1 $answer)
    {
        $res = $this->createQueryBuilder('course')
            ->select('course, lessons, folder')
            ->innerJoin('course.folder', 'folder')
            ->innerJoin('course.lessons', 'lessons', Join::WITH, 'lessons.isStopLesson = 1')
            ->innerJoin(AnswerTaskType1::class, 'answer', Join::WITH, 'answer = :answer')
            ->innerJoin(TaskType1::class, 'task1', Join::WITH, 'task1 = answer.task')
            ->innerJoin(
                TaskSet::class,
                'task_set',
                Join::WITH,
                'task_set.blockId = task1.id and task_set.deleted = 0'
            )
            ->innerJoin(
                'task_set.blockTaskWithCheck',
                'task_with_check',
                Join::WITH,
                'task_with_check.id = task_set.blockTaskWithCheck'
            )
            ->innerJoin(LessonPartBlock::class, 'lpb', Join::WITH, 'lpb.blockId = task_with_check.id and lpb.type = 2')
            ->innerJoin('lpb.part', 'lp')
            ->innerJoin('lp.lesson', 'lesson')
            ->where('course = lesson.course')
            ->setParameter('answer', $answer);

        try {
            return $res->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

}

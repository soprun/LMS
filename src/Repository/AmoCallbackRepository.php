<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\AmoCallback;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\Persistence\ManagerRegistry;
use Generator;

/**
 * @method AmoCallback|null find($id, $lockMode = null, $lockVersion = null)
 * @method AmoCallback|null findOneBy(array $criteria, array $orderBy = null)
 * @method AmoCallback[]    findAll()
 * @method AmoCallback[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AmoCallbackRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AmoCallback::class);
    }

    /**
     * @param ?int $statusId
     * @param ?int $pipelineId
     *
     * @return iterable<int, AmoCallback> Returns an array of AmoCallback objects
     */
    final public function findByCriteria(?int $statusId, ?int $pipelineId): iterable
    {
        yield $this->createQueryBuilder('a')
            ->andWhere('a.statusId = :statusId')
            ->andWhere('a.pipelineId = :pipelineId')
            ->getQuery()
            ->iterate([
                'statusId' => $statusId,
                'pipelineId' => $pipelineId,
            ], AbstractQuery::HYDRATE_OBJECT);
    }
}

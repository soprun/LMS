<?php

namespace App\Repository;

use App\Entity\User;
use App\Entity\UserLesson;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class UserLessonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserLesson::class);
    }

    public function getStatusForLessonsCourse(User $user, array $courseLessons)
    {
        $rows = $this->createQueryBuilder('ul')
            ->select('l.id as lessonId, ul.state AS state')
            ->innerJoin('ul.user', 'u')
            ->innerJoin('ul.lesson', 'l')
            ->andWhere('ul.user = :user')
            ->andWhere('l.id IN(:lessons)')
            ->setParameters([
                'user' => $user,
                'lessons' => $courseLessons,
            ])
            ->groupBy('l.id', 'ul.state')
            ->getQuery()
            ->getResult();

        $result = [];

        foreach ($rows as $row) {
            $result[$row['lessonId']] = $row['state'];
        }
        return $result;
    }
}

<?php

namespace App\Repository;

use App\Entity\Courses\Course;
use App\Entity\Courses\CourseFolder;
use App\Entity\GroupPermission;
use App\Entity\User;
use App\Entity\UserGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method GroupPermission|null find($id, $lockMode = null, $lockVersion = null)
 * @method GroupPermission|null findOneBy(array $criteria, array $orderBy = null)
 * @method GroupPermission[]    findAll()
 * @method GroupPermission[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GroupPermissionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GroupPermission::class);
    }

    public function findByUserIsAdmin(?User $user = null): bool
    {
        if (!$user || !$user->getId()) {
            return false;
        }

        // TODO: ускорить
        $adminGroups = $this->createQueryBuilder('p')
            ->innerJoin('p.userGroup', 'g')
            ->innerJoin('g.userRelations', 'ur')
            ->andWhere('ur.user = :user')
            ->setParameter('user', $user)
            ->andWhere('ur.deleted = 0')
            ->andWhere('p.isAdmin = :state')
            ->setParameter('state', true)
            ->getQuery()
            ->getResult();


        return count($adminGroups) > 0;
    }

    public function findByUserAndFieldSuperPermissions(User $user, string $field)
    {
        if (!$user || !$user->getId()) {
            return [];
        }

        return $this->createQueryBuilder('p')
            ->select("p." . $field)
            ->addSelect('p.isAdmin')
            ->innerJoin('p.userGroup', 'g')
            ->innerJoin('g.userRelations', 'ur')
            ->andWhere('ur.user = :user')
            ->setParameter('user', $user)
            ->andWhere('ur.deleted = 0')
            ->andWhere('p.' . $field . ' LIKE :two')
            ->setParameter('two', '%2%')
            ->getQuery()
            ->getResult();
    }

    public function findByGroupPermissions(UserGroup $userGroup)
    {
        $res = $this->createQueryBuilder('p')
            ->innerJoin('p.userGroup', 'g')
            ->andWhere('g = :userGroup')
            ->setParameter('userGroup', $userGroup)
            ->getQuery()
            ->getArrayResult();

        return $res ? $res[0] : [];
    }

    public function findByUserAndFieldAllPermissions(User $user, string $field)
    {
        if (!$user || !$user->getId()) {
            return [];
        }

        return $this->createQueryBuilder('p')
            ->select("p." . $field)
            ->addSelect('p.isAdmin')
            ->innerJoin('p.userGroup', 'g')
            ->innerJoin('g.userRelations', 'ur')
            ->andWhere('ur.user = :user')
            ->setParameter('user', $user)
            ->andWhere('ur.deleted = 0')
            ->getQuery()
            ->getResult();
    }

    public function findByUserAndFieldAllPermissionsMenu(User $user)
    {
        if (!$user || !$user->getId()) {
            return [];
        }

        return $this->createQueryBuilder('p')
            ->select(
                "p.isAdmin",
                "p.folderModuleAdmin",
                "p.courseModuleAdmin",
                "p.lessonModuleAdmin",
                "p.taskAnswerCheckAdmin",
                "p.userModuleAdmin",
                "p.userGroupModuleAdmin",
                "p.userTeamModuleAdmin",
                "p.analyticsModelAdmin"
            )
            ->innerJoin('p.userGroup', 'g')
            ->innerJoin('g.userRelations', 'ur')
            ->andWhere('ur.user = :user')
            ->setParameter('user', $user)
            ->andWhere('ur.deleted = 0')
            ->getQuery()
            ->getResult();
    }

    public function findByUserAndCourseFolderPermissionForCourseFolder(User $user, CourseFolder $courseFolder)
    {
        if (!$user || !$user->getId()) {
            return [];
        }

        return $this->createQueryBuilder('p')
            ->select("p.folderModuleAdmin, p.isAdmin")
            ->innerJoin('p.userGroup', 'g')
            ->innerJoin('g.courseFolders', 'f')
            ->innerJoin('g.userRelations', 'ur')
            ->andWhere('ur.user = :user')
            ->setParameter('user', $user)
            ->andWhere('ur.deleted = 0')
            ->andWhere('f = :folder')
            ->setParameter('folder', $courseFolder)
            ->getQuery()
            ->getResult();
    }

    public function findByUserAndCoursePermissionForCourse(User $user, Course $course)
    {
        if (!$user || !$user->getId()) {
            return [];
        }

        return $this->createQueryBuilder('p')
            ->select("p.courseModuleAdmin, p.isAdmin")
            ->innerJoin('p.userGroup', 'g')
            ->innerJoin('g.courses', 'c')
            ->innerJoin('g.userRelations', 'ur')
            ->andWhere('ur.user = :user')
            ->setParameter('user', $user)
            ->andWhere('ur.deleted = 0')
            ->andWhere('c = :course')
            ->setParameter('course', $course)
            ->getQuery()
            ->getResult();
    }

    public function findByUserAndCourseFolderPermissionForCourse(User $user, CourseFolder $courseFolder)
    {
        if (!$user || !$user->getId()) {
            return [];
        }

        return $this->createQueryBuilder('p')
            ->select("g.id")
            ->innerJoin('p.userGroup', 'g')
            ->innerJoin('g.courseFolders', 'cf')
            ->innerJoin('g.userRelations', 'ur')
            ->andWhere('ur.user = :user')
            ->setParameter('user', $user)
            ->andWhere('ur.deleted = 0')
            ->andWhere('cf = :courseFolder')
            ->setParameter('courseFolder', $courseFolder)
            ->andWhere('p.lessonModuleAdmin LIKE :permission')
            ->setParameter(':permission', '1%')
            ->getQuery()
            ->getResult();
    }

    public function findByUserAndLessonPermissionForCourse(User $user, Course $course)
    {
        if (!$user || !$user->getId()) {
            return [];
        }

        return $this->createQueryBuilder('p')
            ->select("p.lessonModuleAdmin, p.isAdmin")
            ->innerJoin('p.userGroup', 'g')
            ->innerJoin('g.courses', 'c')
            ->innerJoin('g.userRelations', 'ur')
            ->andWhere('ur.user = :user')
            ->setParameter('user', $user)
            ->andWhere('ur.deleted = 0')
            ->andWhere('c = :course')
            ->setParameter('course', $course)
            ->getQuery()
            ->getResult();
    }

}

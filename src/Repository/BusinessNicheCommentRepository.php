<?php

namespace App\Repository;

use App\Entity\BusinessNicheComment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BusinessNicheComment|null find($id, $lockMode = null, $lockVersion = null)
 * @method BusinessNicheComment|null findOneBy(array $criteria, array $orderBy = null)
 * @method BusinessNicheComment[]    findAll()
 * @method BusinessNicheComment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class BusinessNicheCommentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BusinessNicheComment::class);
    }
}

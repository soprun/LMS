<?php

namespace App\Repository\SmartSender;

use App\Entity\SmartSender\SmartSenderForm;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SmartSenderForm|null find($id, $lockMode = null, $lockVersion = null)
 * @method SmartSenderForm|null findOneBy(array $criteria, array $orderBy = null)
 * @method SmartSenderForm[]    findAll()
 * @method SmartSenderForm[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SmartSenderFormRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SmartSenderForm::class);
    }

    // /**
    //  * @return SmartSenderForm[] Returns an array of SmartSenderForm objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SmartSenderForm
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

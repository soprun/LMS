<?php

namespace App\Repository\SmartSender;

use App\Entity\SmartSender\SmartSenderBotGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SmartSenderBotGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method SmartSenderBotGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method SmartSenderBotGroup[]    findAll()
 * @method SmartSenderBotGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SmartSenderBotGroupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SmartSenderBotGroup::class);
    }

}

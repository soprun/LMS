<?php

namespace App\Repository\SmartSender;

use App\Entity\AbstractCourse;
use App\Entity\SmartSender\SmartSenderBot;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SmartSenderBot|null find($id, $lockMode = null, $lockVersion = null)
 * @method SmartSenderBot|null findOneBy(array $criteria, array $orderBy = null)
 * @method SmartSenderBot[]    findAll()
 * @method SmartSenderBot[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SmartSenderBotRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SmartSenderBot::class);
    }

    public function findBot(User $user): ?SmartSenderBot
    {
        return $this->createQueryBuilder('ssb')
            ->innerJoin('ssb.abstractCourse', 'ac')
            ->innerJoin('ac.streams', 'cs')
            ->innerJoin('cs.userGroups', 'ug')
            ->innerJoin('ug.userRelations', 'ugu')
            ->where('ugu.user = :user')
            ->andWhere('ugu.deleted = 0')
            ->andWhere('cs.active = 1')
            ->andWhere('ac.slug IN(:slugs)')
            ->setParameters(
                [
                    'user' => $user,
                    'slugs' => [
                        AbstractCourse::SLUG_HUNDRED,
                        AbstractCourse::SLUG_SPEED,
                        AbstractCourse::SLUG_SPEED_CLUB,
                    ],
                ]
            )
            ->orderBy('cs.stream', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

}

<?php

namespace App\Repository\SmartSender;

use App\Entity\AbstractCourse;
use App\Entity\SmartSender\SmartSenderUser;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SmartSenderUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method SmartSenderUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method SmartSenderUser[]    findAll()
 * @method SmartSenderUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SmartSenderUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SmartSenderUser::class);
    }

    /**
     * @param User $user
     *
     * @return null|SmartSenderUser
     * @throws NonUniqueResultException
     */
    public function findByUserAndActualStream(User $user): ?SmartSenderUser
    {
        return $this->createQueryBuilder('ssu')
            ->innerJoin('ssu.smartSenderBot', 'ssb')
            ->innerJoin('ssb.abstractCourse', 'ac')
            ->innerJoin('ac.streams', 'cs')
            ->innerJoin('cs.userGroups', 'ug')
            ->innerJoin('ug.userRelations', 'ugu')
            ->where('ssu.user = :user')
            ->andWhere('ugu.user = :user')
            ->andWhere('cs.active = 1')
            ->andWhere('ac.slug IN (:slugs)')
            ->andWhere('ugu.deleted = 0')
            ->setParameters(
                [
                    'user' => $user,
                    'slugs' => [
                        AbstractCourse::SLUG_HUNDRED,
                        AbstractCourse::SLUG_SPEED,
                        AbstractCourse::SLUG_SPEED_CLUB,
                    ],
                ]
            )
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();
    }

    public function findByAuthIds(array $authIds): array
    {
        $qb = $this->createQueryBuilder('smartSenderUser');
        $qb
            ->innerJoin('smartSenderUser.user', 'user')
            ->where($qb->expr()->in('user.authUserId', $authIds));

        return $qb->getQuery()->getResult();
    }

}

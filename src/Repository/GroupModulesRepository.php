<?php

namespace App\Repository;

use App\Entity\GroupModules;
use App\Message\LMS\GrantingAccessMessage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GroupModules|null find($id, $lockMode = null, $lockVersion = null)
 * @method GroupModules|null findOneBy(array $criteria, array $orderBy = null)
 * @method GroupModules[]    findAll()
 * @method GroupModules[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GroupModulesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GroupModules::class);
    }

    /** @return GroupModules[] */
    public function getGroupModulesByMessage(GrantingAccessMessage $message): array
    {
        $qb = $this->createQueryBuilder('gm');
        if ($message->getModule() !== null) {
            $qb->andWhere('gm.module = :module')->setParameter('module', $message->getModule());
        }
        if ($message->getCourse() !== null) {
            if ($message->getStream() !== null) {
                if ($message->getTariff() !== null) {
                    $qb->orWhere('(gm.course = :course AND gm.stream = :stream AND gm.tariff = :tariff)')
                        ->setParameter('tariff', $message->getTariff());
                }
                $qb->orWhere('(gm.course = :course AND gm.stream = :stream AND gm.tariff IS NULL)')
                    ->setParameter('stream', $message->getStream());
            }
            $qb->orWhere('(gm.course = :course AND gm.stream IS NULL AND gm.tariff IS NULL)')
                ->setParameter('course', $message->getCourse());
        }

        return $qb->getQuery()->getResult();
    }
}

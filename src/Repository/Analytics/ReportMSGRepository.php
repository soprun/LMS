<?php

namespace App\Repository\Analytics;

use App\Entity\Analytics\ReportMSG;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ReportMSG|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReportMSG|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReportMSG[]    findAll()
 * @method ReportMSG[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReportMSGRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ReportMSG::class);
    }

    // /**
    //  * @return ReportMSG[] Returns an array of ReportMSG objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ReportMSG
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

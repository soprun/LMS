<?php

namespace App\Repository\Analytics;

use App\Entity\Analytics\ReportMSGCell;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ReportMSGCell|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReportMSGCell|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReportMSGCell[]    findAll()
 * @method ReportMSGCell[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReportMSGCellRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ReportMSGCell::class);
    }

    // /**
    //  * @return ReportMSGCell[] Returns an array of ReportMSGCell objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ReportMSGCell
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

<?php

namespace App\Repository;

use App\Entity\AttemptToGetPromocode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AttemptToGetPromocode|null find($id, $lockMode = null, $lockVersion = null)
 * @method AttemptToGetPromocode|null findOneBy(array $criteria, array $orderBy = null)
 * @method AttemptToGetPromocode[]    findAll()
 * @method AttemptToGetPromocode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AttemptToGetPromocodeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AttemptToGetPromocode::class);
    }

    // /**
    //  * @return AttemptToGetPromocode[] Returns an array of AttemptToGetPromocode objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AttemptToGetPromocode
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

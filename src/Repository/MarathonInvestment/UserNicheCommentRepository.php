<?php

namespace App\Repository\MarathonInvestment;

use App\Entity\MarathonInvestment\UserNicheComment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class UserNicheCommentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserNicheComment::class);
    }
}

<?php

namespace App\Repository\MarathonInvestment;

use App\Entity\MarathonInvestment\MarathonInvestmentConfiguration;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class MarathonInvestmentConfigurationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MarathonInvestmentConfiguration::class);
    }
}

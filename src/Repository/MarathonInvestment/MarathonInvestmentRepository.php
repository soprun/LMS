<?php

namespace App\Repository\MarathonInvestment;

use App\Entity\MarathonInvestment\MarathonInvestment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class MarathonInvestmentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MarathonInvestment::class);
    }
}

<?php

namespace App\Repository\MarathonInvestment;

use App\Entity\MarathonInvestment\UserNiche;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class UserNicheRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserNiche::class);
    }
}

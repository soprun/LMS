<?php

namespace App\Repository\Questionnaire;

use App\Entity\CourseStream;
use App\Entity\Questionnaire\Question;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Question|null find($id, $lockMode = null, $lockVersion = null)
 * @method Question|null findOneBy(array $criteria, array $orderBy = null)
 * @method Question[]    findAll()
 * @method Question[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuestionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Question::class);
    }

    public function findOneBySlug(string $slug)
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @return Question[]
     */
    public function findWithVariants(): array
    {
        return $this->createQueryBuilder('q')
            ->leftJoin('q.variants', 'v')
            ->leftJoin('v.nextQuestion', 'vnq')
            ->leftJoin('q.nextQuestion', 'qnq')
            ->getQuery()
            ->getResult();
    }

    public function getWantBeLeaders(array $userIds): array
    {
        $leaders = $this->createQueryBuilder('qq')
            ->select('u.id')
            ->innerJoin('qq.variants', 'v')
            ->innerJoin('v.answers', 'a')
            ->innerJoin('a.user', 'u')
            ->andWhere("qq.slug = :slug")
            ->andWhere("v.title = :answer")
            ->andWhere('u.id IN(:userIds)')
            ->setParameters(
                [
                    'userIds' => $userIds,
                    'slug' => Question::WANT_LEADER_SLUG,
                    'answer' => 'Да',
                ]
            )
            ->groupBy('u.id')
            ->getQuery()
            ->getResult();

        return array_map(
            static function ($leader) {
                return $leader['id'];
            },
            $leaders
        );
    }

    /**
     * @param  string  $slug
     * @param  CourseStream  $courseStream
     * @return null|Question
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findBySlugAndStream(string $slug, CourseStream $courseStream): ?Question
    {
        return $this->createQueryBuilder('question')
            ->leftJoin('question.courseStreams', 'courseStream')
            ->where('courseStream = :courseStream and question.slug = :slug')
            ->setParameter('courseStream', $courseStream)
            ->setParameter('slug', $slug)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

}

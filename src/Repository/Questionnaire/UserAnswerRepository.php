<?php

namespace App\Repository\Questionnaire;

use App\Entity\Questionnaire\Question;
use App\Entity\Questionnaire\UserAnswer;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserAnswer|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserAnswer|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserAnswer[]    findAll()
 * @method UserAnswer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserAnswerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserAnswer::class);
    }

    public function getArrayByUser(User $user): array
    {
        $rows = $this->createQueryBuilder('a')
            ->select(
                [
                    'q.id AS questionId',
                    'v.id AS variantId',
                    'a.text AS text',
                ]
            )
            ->innerJoin('a.question', 'q')
            ->leftJoin('a.variant', 'v')
            ->andWhere('a.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();

        $answers = [];
        foreach ($rows as $row) {
            // ищем текст или вариант ответа
            $answers[$row['questionId']] = $row['text'] ?? $row['variantId'];
        }

        return $answers;
    }

    public function getVariant(User $user, string $slug): ?string
    {
        $result = $this->createQueryBuilder('a')
            ->select('v.title')
            ->innerJoin('a.question', 'q')
            ->leftJoin('a.variant', 'v')
            ->leftJoin('a.user', 'u')
            ->andWhere('a.user = :user')
            ->andWhere('q.slug = :slug')
            ->setParameters(
                [
                    'user' => $user,
                    'slug' => $slug,
                ]
            )
            ->orderBy('a.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        return is_array($result) ? $result['title'] : null;
    }

}

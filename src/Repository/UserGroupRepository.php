<?php

namespace App\Repository;

use App\Entity\User;
use App\Entity\UserGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;

class UserGroupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserGroup::class);
    }

    public function findByGroupedWithoutFolder()
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.groupFolder is null')
            ->getQuery()
            ->getResult();
    }

    public function findByGroupedWithoutFolderByUser(User $user)
    {
        return $this->createQueryBuilder('g')
            ->innerJoin('g.userRelations', 'ur')
            ->andWhere('g.groupFolder is null')
            ->andWhere('ur.user = :user')
            ->setParameter('user', $user)
            ->andWhere('ur.deleted = 0)')
            ->getQuery()
            ->getResult();
    }

    public function getUserGroups(User $user): array
    {
        return $this->createQueryBuilder('g')
            ->select(
                'g.id',
                'g.name',
                // так определяем что модуль Команды доступен
                'COUNT(tf.id) AS teamFoldersCount',
                'ac.slug AS courseSlug',
                'cs.stream AS stream',
                'cs.active AS activeStream',
            )
            ->innerJoin('g.userRelations', 'ur')
            ->leftJoin('g.teamFolders', 'tf')
            ->leftJoin('g.courseStream', 'cs')
            ->leftJoin('cs.abstractCourse', 'ac')
            ->where('ur.user = :user')
            ->setParameter('user', $user)
            ->andWhere('ur.deleted = 0')
            ->groupBy('g.id')
            ->getQuery()
            ->getResult();
    }

    public function getUserGroupsInActiveCourseStream(User $user): array
    {
        return $this->createQueryBuilder('g')
            ->select(
                'g.id',
                'g.name',
                // так определяем что модуль Команды доступен
                'COUNT(tf.id) AS teamFoldersCount',
                'ac.slug AS courseSlug',
                'cs.stream AS stream',
                'cs.active AS activeStream',
            )
            ->innerJoin('g.userRelations', 'ur')
            ->leftJoin('g.teamFolders', 'tf')
            ->leftJoin('g.courseStream', 'cs')
            ->leftJoin('cs.abstractCourse', 'ac')
            ->where('ur.user = :user')
            ->setParameter('user', $user)
            ->andWhere('ur.deleted = 0 and cs.active = 1')
            ->andWhere("g.name not like '%сборн%' and g.name not like '%факультет%'")
            ->groupBy('g.id')
            ->getQuery()
            ->getResult();
    }

    public function findByGroupCountOfUsers(UserGroup $userGroup)
    {
        $res = $this->createQueryBuilder('g')
            ->innerJoin('g.userRelations', 'ur')
            ->innerJoin('ur.user', 'u')
            ->select('COUNT(u.id)')
            ->andWhere('g = :group')
            ->andWhere('ur.deleted = 0')
            ->setParameter('group', $userGroup)
            ->getQuery()
            ->getResult();

        return $res[0][1];
    }

    public function getIsInGroupByGroupId(User $user, int $groupId)
    {
        $result = $this->createQueryBuilder('g')
            ->andWhere('g.id = :groupId')
            ->setParameter('groupId', $groupId)
            ->innerJoin('g.userRelations', 'ur')
            ->andWhere('ur.user = :user')
            ->setParameter('user', $user)
            ->andWhere('ur.deleted = 0')
            ->getQuery()
            ->getResult();

        return !empty($result);
    }

    public function getGroupsByGroupFolderAndDateCreate(
        int $groupFolderId,
        \DateTime $dateStart,
        \DateTime $dateEnd,
        ?array $groupIds = []
    ) {
        $result = $this->createQueryBuilder('g')
            ->andWhere('g.groupFolder = :groupFolderId')
            ->setParameter('groupFolderId', $groupFolderId)
            ->andWhere('g.createdAt >= :dateStart')
            ->setParameter(':dateStart', $dateStart)
            ->andWhere('g.createdAt <= :dateEnd')
            ->setParameter(':dateEnd', $dateEnd);

        if ($groupIds) {
            $result->andWhere('g.id IN (:groupIds)');
            $result->setParameter(':groupIds', $groupIds);
        }

        return $result->getQuery()->getResult();
    }

    public function getGroupsByGroupFolderAndDateCreateMSA(
        int $groupsFolderIds,
        ?\DateTime $dateStart,
        ?\DateTime $dateEnd,
        ?array $groupIds = []
    ) {
        $result = $this->createQueryBuilder('g')
            ->select('g, userRelations, user')
            ->innerJoin('g.userRelations', 'userRelations')
            ->innerJoin('userRelations.user', 'user')
            ->andWhere('userRelations.deleted = 0')
            ->andWhere('g.groupFolder in (:groupFolderId)')
            ->setParameter('groupFolderId', $groupsFolderIds)
            ->andWhere('g.name NOT IN (:names)')
            ->setParameter('names', ['Трекеры МСА', 'Мастера МСА']);

        if ($groupIds) {
            $result
                ->andWhere('g.id IN (:groupIds)')
                ->setParameter(':groupIds', $groupIds);
        }

        if ($dateStart && $dateEnd) {
            $result
                ->andWhere('userRelations.createdAt >= :dateStart')
                ->setParameter(':dateStart', $dateStart)
                ->andWhere('userRelations.createdAt <= :dateEnd')
                ->setParameter(':dateEnd', $dateEnd);
        }

        return $result->getQuery()->getResult();
    }

    public function getGroupsByGroupFolderAndDateCreateTraction(
        array $groupIds,
        $page,
        $pageSize,
        ?\DateTime $dateStart,
        ?\DateTime $dateEnd,
        ?array $usersIds = []
    ) {
        $result = $this->createQueryBuilder('g')
            ->select(
                'DISTINCT user.id, user.authUserId, MAX(userRelations.createdAt) as createdAt, MAX(g.id) as groupId'
            )
            ->innerJoin('g.userRelations', 'userRelations')
            ->innerJoin('userRelations.user', 'user')
            ->where('g.id IN (:groupIds)')
            ->setParameter(':groupIds', $groupIds)
            ->andWhere('userRelations.deleted');

        if ($dateStart && $dateEnd) {
            $result
                ->andWhere('userRelations.createdAt >= :dateStart')
                ->setParameter(':dateStart', $dateStart)
                ->andWhere('userRelations.createdAt <= :dateEnd')
                ->setParameter(':dateEnd', $dateEnd);
        }
        if ($usersIds != []) {
            $result
                ->andWhere('user.id IN (:usersIds)')
                ->setParameter(':usersIds', $usersIds);
        } else {
            $result
                ->setFirstResult(($page - 1) * $pageSize)
                ->setMaxResults($pageSize);
        }

        $result
            ->groupBy('user.id')
            ->orderBy('user.id', 'ASC');

        return $result->getQuery()->getResult();
    }

    public function getCreatedAtByGroupFolderAndUser(int $groupFolderId, User $user)
    {
        $result = $this->createQueryBuilder('g')
            ->select('userRelation.createdAt, g.id')
            ->innerJoin('g.userRelations', 'userRelation')
            ->where('g.groupFolder = :groupFolderId')
            ->setParameter('groupFolderId', $groupFolderId)
            ->andWhere('userRelation.user = :user')
            ->setParameter('user', $user)
            ->andWhere('userRelation.deleted = 0')
            ->andWhere('g.name NOT IN (:names)')
            ->setParameter('names', ['Трекеры МСА', 'Мастера МСА'])
            ->orderBy('userRelation.createdAt', 'DESC');

        $result = $result->getQuery()->getResult();

        return current($result);
    }
}

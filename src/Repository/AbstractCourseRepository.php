<?php

namespace App\Repository;

use App\Entity\AbstractCourse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AbstractCourse|null find($id, $lockMode = null, $lockVersion = null)
 * @method AbstractCourse|null findOneBy(array $criteria, array $orderBy = null)
 * @method AbstractCourse[]    findAll()
 * @method AbstractCourse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AbstractCourseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AbstractCourse::class);
    }

    // /**
    //  * @return AbstractCourse[] Returns an array of AbstractCourse objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AbstractCourse
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

<?php

namespace App\Repository;

use App\Entity\UserGroup;
use App\Entity\UserGroupAttendanceType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method UserGroupAttendanceType|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserGroupAttendanceType|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserGroupAttendanceType[]    findAll()
 * @method UserGroupAttendanceType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserGroupAttendanceTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserGroupAttendanceType::class);
    }

    public function getTypesByUserIds(array $userIds, UserGroup $group)
    {
        $rows = $this->createQueryBuilder('at')
            ->select('u.id AS userId, at.typeId')
            ->innerJoin('at.user', 'u')
            ->andWhere('at.user IN(:userIds)')
            ->setParameter('userIds', $userIds)
            ->andWhere('at.userGroup = :group')
            ->setParameter('group', $group)
            ->getQuery()
            ->getResult();

        $array = [];
        foreach ($rows as $row) {
            $array[$row['userId']] = $row['typeId'];
        }

        return $array;
    }

    /*
    public function findOneBySomeField($value): ?UserGroupAttendanceType
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

<?php

namespace App\Repository\BaseBlock;

use App\Entity\BaseBlock\BaseNps1;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method BaseNps1|null find($id, $lockMode = null, $lockVersion = null)
 * @method BaseNps1|null findOneBy(array $criteria, array $orderBy = null)
 * @method BaseNps1[]    findAll()
 * @method BaseNps1[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BaseBlockNpsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BaseNps1::class);
    }

    // /**
    //  * @return BaseNps1[] Returns an array of BaseNps1 objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BaseNps1
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

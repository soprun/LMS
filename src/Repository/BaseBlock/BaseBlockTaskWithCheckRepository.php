<?php

namespace App\Repository\BaseBlock;

use App\Entity\BaseBlock\BaseBlockTaskWithCheck;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;

/**
 * @method BaseBlockTaskWithCheck|null find($id, $lockMode = null, $lockVersion = null)
 * @method BaseBlockTaskWithCheck|null findOneBy(array $criteria, array $orderBy = null)
 * @method BaseBlockTaskWithCheck[]    findAll()
 * @method BaseBlockTaskWithCheck[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BaseBlockTaskWithCheckRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BaseBlockTaskWithCheck::class);
    }

    public function findOneById(int $id)
    {
        $result = $this->createQueryBuilder('t')
            ->select('t.id', 'taskSet.id as taskSetId', 't.systemName')
            ->innerJoin('t.taskSets', 'taskSet')
            ->where('t.id = :id')
            ->setParameter('id', $id)
            ->getQuery();
        try {
            return $result->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }

}

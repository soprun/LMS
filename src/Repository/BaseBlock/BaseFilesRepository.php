<?php

namespace App\Repository\BaseBlock;

use App\Entity\BaseBlock\BaseFiles;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method BaseFiles|null find($id, $lockMode = null, $lockVersion = null)
 * @method BaseFiles|null findOneBy(array $criteria, array $orderBy = null)
 * @method BaseFiles[]    findAll()
 * @method BaseFiles[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BaseFilesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BaseFiles::class);
    }

    // /**
    //  * @return BaseFiles[] Returns an array of BaseFiles objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BaseFiles
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

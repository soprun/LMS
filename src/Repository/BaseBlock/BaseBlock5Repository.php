<?php

namespace App\Repository\BaseBlock;

use App\Entity\BaseBlock\BaseBlock5;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method BaseBlock5|null find($id, $lockMode = null, $lockVersion = null)
 * @method BaseBlock5|null findOneBy(array $criteria, array $orderBy = null)
 * @method BaseBlock5[]    findAll()
 * @method BaseBlock5[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BaseBlock5Repository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BaseBlock5::class);
    }

    // /**
    //  * @return BaseBlock5[] Returns an array of BaseBlock5 objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BaseBlock5
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

<?php

namespace App\Repository\BaseBlock;

use App\Entity\BaseBlock\BaseBlock7;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method BaseBlock7|null find($id, $lockMode = null, $lockVersion = null)
 * @method BaseBlock7|null findOneBy(array $criteria, array $orderBy = null)
 * @method BaseBlock7[]    findAll()
 * @method BaseBlock7[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BaseBlock7Repository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BaseBlock7::class);
    }

    // /**
    //  * @return BaseBlock7[] Returns an array of BaseBlock7 objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BaseBlock7
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

<?php

namespace App\Repository\BaseBlock;

use App\Entity\BaseBlock\BaseBlock1;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method BaseBlock1|null find($id, $lockMode = null, $lockVersion = null)
 * @method BaseBlock1|null findOneBy(array $criteria, array $orderBy = null)
 * @method BaseBlock1[]    findAll()
 * @method BaseBlock1[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BaseBlock1Repository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BaseBlock1::class);
    }

    // /**
    //  * @return BaseBlock1[] Returns an array of BaseBlock1 objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BaseBlock1
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

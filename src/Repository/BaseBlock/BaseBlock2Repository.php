<?php

namespace App\Repository\BaseBlock;

use App\Entity\BaseBlock\BaseBlock2;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method BaseBlock2|null find($id, $lockMode = null, $lockVersion = null)
 * @method BaseBlock2|null findOneBy(array $criteria, array $orderBy = null)
 * @method BaseBlock2[]    findAll()
 * @method BaseBlock2[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BaseBlock2Repository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BaseBlock2::class);
    }

    // /**
    //  * @return BaseBlock2[] Returns an array of BaseBlock2 objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BaseBlock2
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

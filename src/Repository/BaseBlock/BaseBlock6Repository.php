<?php

namespace App\Repository\BaseBlock;

use App\Entity\BaseBlock\BaseBlock6;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method BaseBlock6|null find($id, $lockMode = null, $lockVersion = null)
 * @method BaseBlock6|null findOneBy(array $criteria, array $orderBy = null)
 * @method BaseBlock6[]    findAll()
 * @method BaseBlock6[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BaseBlock6Repository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BaseBlock6::class);
    }

    // /**
    //  * @return BaseBlock6[] Returns an array of BaseBlock6 objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BaseBlock6
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

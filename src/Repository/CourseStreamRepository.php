<?php

namespace App\Repository;

use App\Entity\AbstractCourse;
use App\Entity\CourseStream;
use App\Entity\User;
use DateTimeInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CourseStream|null find($id, $lockMode = null, $lockVersion = null)
 * @method CourseStream|null findOneBy(array $criteria, array $orderBy = null)
 * @method CourseStream[]    findAll()
 * @method CourseStream[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CourseStreamRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CourseStream::class);
    }

    /**
     * @param string $abstractCourseSlug
     *
     * @return int[]
     */
    public function getActualStreams(string $abstractCourseSlug): array
    {
        $rows = $this->createQueryBuilder('cs')
            ->select('cs.stream')
            ->innerJoin('cs.abstractCourse', 'ac')
            ->andWhere('cs.active = 1')
            ->andWhere('ac.slug = :slug')
            ->setParameter('slug', $abstractCourseSlug)
            ->getQuery()
            ->getResult();

        return array_column($rows, 'stream');
    }

    /**
     * @param User $user
     * @param DateTimeInterface $from
     * @param DateTimeInterface $to
     *
     * @return CourseStream[]
     */
    public function getUserStreamsByDates(User $user, DateTimeInterface $from, DateTimeInterface $to): array
    {
        $qb = $this->createQueryBuilder('courseStream')
            ->innerJoin('courseStream.abstractCourse', 'abstractCourse')
            ->join('courseStream.userGroups', 'userGroup')
            ->join('userGroup.userRelations', 'userRelation')
            ->where('userRelation.deleted = 0')
            ->andWhere('userRelation.user = :user')
            ->andWhere('courseStream.period IS NOT NULL')
            ->setParameter('user', $user)
            ->addOrderBy('courseStream.startDate');

        $streams = $qb->getQuery()->getResult();

        return array_filter($streams, static function (CourseStream $s) use ($from, $to, $user) {
            $streamStartDate = $s->getStartDate() ?? $s->getUserJoinStartDate($user);
            $streamEndDate = $s->getStartDate() ? $s->getEndDate() : $s->getUserJoinEndDate($user);

            return ($streamStartDate <= $from && $from <= $streamEndDate)
                || ($streamStartDate <= $to && $to <= $streamEndDate)
                || ($from <= $streamStartDate && $streamStartDate <= $to)
                || ($from <= $streamEndDate && $streamEndDate <= $to);
        });
    }

    /**
     * @param User Ё$user
     * @param array $abstractCourseSlugs
     *
     * @return null|CourseStream
     * @throws NonUniqueResultException
     */
    public function getCurrentStream(
        User $user,
        array $abstractCourseSlugs = [
            AbstractCourse::SLUG_HUNDRED,
            AbstractCourse::SLUG_SPEED_CLUB,
            AbstractCourse::SLUG_SPEED,
        ]
    ): ?CourseStream {
        return $this->createQueryBuilder('courseStream')
            ->addSelect(['userGroup'])
            ->innerJoin('courseStream.abstractCourse', 'abstractCourse')
            ->join('courseStream.userGroups', 'userGroup')
            ->join('userGroup.userRelations', 'userRelation')
            ->where('courseStream.active = 1 and userRelation.deleted = 0')
            ->andWhere('userRelation.user = :user and abstractCourse.slug in (:abstractCourseSlugs)')
            ->setParameter('user', $user)
            ->setParameter('abstractCourseSlugs', $abstractCourseSlugs)
            ->orderBy('courseStream.startDate', 'desc')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param CourseStream $courseStream
     *
     * @return null|CourseStream
     * @throws NonUniqueResultException
     */
    public function getPreviousStream(CourseStream $courseStream): ?CourseStream
    {
        return $this->createQueryBuilder('courseStream')
            ->innerJoin('courseStream.abstractCourse', 'abstractCourse')
            ->where('abstractCourse.slug  = :abstractCourseSlug and courseStream.startDate < :date')
            ->setParameter('abstractCourseSlug', $courseStream->getAbstractCourse()->getSlug())
            ->setParameter('date', $courseStream->getStartDate())
            ->orderBy('courseStream.startDate', 'desc')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @return CourseStream[]
     */
    public function getSpeedAndHundredStreams(): array
    {
        return $this->createQueryBuilder('cs')
            ->join('cs.abstractCourse', 'ac')
            ->andWhere('ac.slug in (:slugs)')
            ->setParameter(
                'slugs',
                [AbstractCourse::SLUG_SPEED, AbstractCourse::SLUG_HUNDRED, AbstractCourse::SLUG_SPEED_CLUB]
            )
            ->getQuery()
            ->getResult();
    }

}

<?php

namespace App\Repository;

use App\Entity\BusinessNiche;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BusinessNiche|null find($id, $lockMode = null, $lockVersion = null)
 * @method BusinessNiche|null findOneBy(array $criteria, array $orderBy = null)
 * @method BusinessNiche[]    findAll()
 * @method BusinessNiche[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class BusinessNicheRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BusinessNiche::class);
    }
}

<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use Sentry\State\Scope;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

use function Sentry\captureException;
use function Sentry\withScope;

final class ExceptionSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelException',
        ];
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        withScope(function (Scope $scope) use ($exception) {
            // https://docs.sentry.io/platforms/php/usage/sdk-fingerprinting/
            // $scope->setFingerprint([
            //     '{{ default }}',
            //     $exception->getFunctionName(),
            //     $exception->getStatusCode(),
            // ]);

            captureException($exception);
        });
    }

}

<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use Sentry\Breadcrumb;
use Sentry\State\Scope;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

use function Sentry\withScope;

final class RequestSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => [
                ['onKernelRequest'],
                ['onPreflightedRequests', 9999],
            ],
            KernelEvents::RESPONSE => [
                ['onKernelResponse', 9999],
            ],
        ];
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        if ($event->isMasterRequest() === false) {
            // don't do anything if it's not the main request
            return;
        }

        $request = $event->getRequest();

        $request->attributes->set('is_api', str_starts_with($request->getPathInfo(), '/api/'));

        withScope(function (Scope $scope) use ($request) {
            $scope->addBreadcrumb(
                new Breadcrumb(
                    Breadcrumb::LEVEL_INFO,
                    Breadcrumb::TYPE_DEFAULT,
                    'route',
                    $request->attributes->get('_route'),
                    [
                        'method' => $request->getRealMethod(),
                        'route' => $request->attributes->get('_route'),
                        'route_params' => $request->attributes->get('_route_params'),
                        'controller' => $request->attributes->get('_controller'),
                    ]
                )
            );

            $scope->setTag('is_api', $request->attributes->getBoolean('is_api') ? 'true' : 'false');
            $scope->setTag('route', $request->attributes->get('_route'));
            $scope->setTag('path', $request->getPathInfo());
            $scope->setTag('locale', $request->getLocale());
            $scope->setTag('http_host', $request->getHttpHost());
            $scope->setTag('http_method', $request->getMethod());
            $scope->setTag('type', 'backend');
        });
    }

    public function onKernelResponse(ResponseEvent $event): void
    {
        if ($event->isMasterRequest() === false) {
            // don't do anything if it's not the main request
            return;
        }

        $response = $event->getResponse();

        withScope(function (Scope $scope) use ($response) {
            $scope->setTag('http_status_code', (string)$response->getStatusCode());
        });
    }

    public function onPreflightedRequests(RequestEvent $event): void
    {
        if ($event->isMasterRequest() === false) {
            // don't do anything if it's not the main request
            return;
        }

        // TODO: it's temp methods, needs fix
    }

}

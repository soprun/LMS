<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use App\Entity\User;
use Sentry\State\Scope;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\AuthenticationEvents;
use Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;
use Symfony\Component\Security\Core\Event\AuthenticationSuccessEvent;

use function Sentry\withScope;

final class AuthenticationSubscriber implements EventSubscriberInterface
{
    /**
     * @var RequestStack
     */
    private $request;

    public function __construct(RequestStack $request)
    {
        $this->request = $request;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            AuthenticationEvents::AUTHENTICATION_SUCCESS => 'onAuthenticationSuccess',
            AuthenticationEvents::AUTHENTICATION_FAILURE => 'onAuthenticationFailure',
        ];
    }

    public function onAuthenticationSuccess(AuthenticationSuccessEvent $event): void
    {
        $token = $event->getAuthenticationToken();
        $currentRequest = $this->request->getCurrentRequest();

        withScope(function (Scope $scope) use ($token, $currentRequest) {
            $user = $token->getUser();

            if (!$user instanceof User) {
                return;
            }

            $scope->setUser([
                'id' => $user->getId(),
                'username' => $user->getUsername(),
                'email' => $user->getEmail(),
                'ip_address' => $currentRequest ? $currentRequest->getClientIp() : null,
            ]);

            $scope->setContext('auth', [
                'user_id' => $user->getId(),
                'auth_id' => $user->getAuthUserId(),
                'is_admin' => $user->getIsAdmin(),
                'roles' => $user->getRoles(),
                'email' => $user->getEmail(),
                'company' => $user->getCompany(),
            ]);
        });
    }

    public function onAuthenticationFailure(AuthenticationFailureEvent $event): void
    {
        // TODO: it's temp methods, needs fix
    }

}

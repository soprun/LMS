<?php

declare(strict_types=1);

namespace App\Message\LMS;

final class GrantingAccessMessage
{
    /**
     * @var int
     */
    private $deal;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $email;
    /**
     * @var string
     */
    private $phone;
    /**
     * @var null|string
     */
    private $course;
    /**
     * @var null|int
     */
    private $stream;
    /**
     * @var null|string
     */
    private $tariff;
    /**
     * @var null|string
     */
    private $module;
    /**
     * @var string
     */
    private $operation;

    public function __construct(
        int $deal,
        ?string $name,
        ?string $email,
        ?string $phone,
        string $operation,
        ?string $course = null,
        ?int $stream = null,
        ?string $tariff = null,
        ?string $module = null
    ) {
        $this->deal = $deal;
        $this->name = $name;
        $this->email = $email;
        $this->phone = $phone;
        $this->course = $course;
        $this->stream = $stream;
        $this->tariff = $tariff;
        $this->module = $module;
        $this->operation = $operation;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function getCourse(): ?string
    {
        return $this->course;
    }

    public function getStream(): ?int
    {
        return $this->stream;
    }

    public function getTariff(): ?string
    {
        return $this->tariff;
    }

    public function getModule(): ?string
    {
        return $this->module;
    }

    public function getOperation(): string
    {
        return $this->operation;
    }

    public function getDeal(): int
    {
        return $this->deal;
    }
}

<?php

declare(strict_types=1);

namespace App\Message\LMS;

final class ReportMessage
{
    /**
     * @var int
     */
    private $deal;

    /**
     * @var null|string
     */
    private $course_title;

    /**
     * @var null|string
     */
    private $module_title;

    /**
     * @var string
     */
    private $status;

    public function __construct(
        int $deal,
        string $status,
        ?string $course_title = null,
        ?string $module_title = null
    ) {
        $this->deal = $deal;
        $this->status = $status;
        $this->course_title = $course_title;
        $this->module_title = $module_title;
    }

    public function getDeal(): int
    {
        return $this->deal;
    }

    public function getCourseTitle(): ?string
    {
        return $this->course_title;
    }

    public function getModuleTitle(): ?string
    {
        return $this->module_title;
    }

    public function getStatus(): string
    {
        return $this->status;
    }
}

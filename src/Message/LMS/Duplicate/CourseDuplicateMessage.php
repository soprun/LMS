<?php

declare(strict_types=1);

namespace App\Message\LMS\Duplicate;

final class CourseDuplicateMessage
{
    private $courseId;

    public function __construct(int $courseId)
    {
        $this->courseId = $courseId;
    }

    public function getCourseId(): int
    {
        return $this->courseId;
    }

}

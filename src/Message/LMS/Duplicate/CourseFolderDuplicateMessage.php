<?php

declare(strict_types=1);

namespace App\Message\LMS\Duplicate;

final class CourseFolderDuplicateMessage
{
    private $courseFolderId;

    private $withCourses;

    public function __construct(int $courseFolderId, bool $withCourses = true)
    {
        $this->courseFolderId = $courseFolderId;
        $this->withCourses = $withCourses;
    }

    public function getCourseFolderId(): int
    {
        return $this->courseFolderId;
    }

    public function getWithCourses(): bool
    {
        return $this->withCourses;
    }

}

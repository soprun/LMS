<?php

declare(strict_types=1);

namespace App\Message\LMS\Duplicate;

final class LessonDuplicateMessage
{
    private $lessonId;
    private $courseId;

    public function __construct(int $lessonId, ?int $courseId)
    {
        $this->lessonId = $lessonId;
        $this->courseId = $courseId;
    }

    public function getLessonId(): int
    {
        return $this->lessonId;
    }


    public function getCourseId(): ?int
    {
        return $this->courseId;
    }

}

<?php

namespace App\Command\SmartSender;

use App\Entity\SmartSender\SmartSenderUser;
use App\Entity\UserGroup;
use App\Service\AuthServerService;
use App\Service\SmartSenderHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateWrongProductsForUserFromFileCommand extends Command
{
    protected static $defaultName = 'smart-sender:update-wrong-products-from-file';

    /** @var EntityManagerInterface */
    private $em;
    /** @var SmartSenderHelper */
    private $smartSenderHelper;
    /** @var AuthServerService */
    private $authServer;

    public function __construct(
        EntityManagerInterface $em,
        SmartSenderHelper $smartSenderHelper,
        AuthServerService $authServer
    ) {
        parent::__construct();
        $this->em = $em;
        $this->smartSenderHelper = $smartSenderHelper;
        $this->authServer = $authServer;
    }

    protected function configure(): void
    {
        $this->addArgument('file', InputArgument::REQUIRED, 'путь к txt/csv файлу с пользователями');
        $this->setDescription('Обновить неверные продукты в боте для пользователей из файла');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $botSlug = 'speed';

        $smartSenderUsers = $this->getSmartSenderUsersFormFile($input->getArgument('file'));

        $count = 0;
        foreach ($smartSenderUsers as $smartSenderUser) {
            $count++;
            $output->writeln(
                sprintf(
                    "%s) uid %s sid %s",
                    $count,
                    $smartSenderUser->getUser()->getId(),
                    $smartSenderUser->getSmartSenderUserId()
                )
            );

            $user = $this->smartSenderHelper->getContact($smartSenderUser);
            $botProduct = null;
            $botPackage = null;

            foreach ($user['variables'] as $v) {
                switch ($v['name']) {
                    case 'product':
                        $botProduct = $v['value'];
                        break;

                    case 'package':
                        $botPackage = $v['value'];
                        break;
                }
            }


            $product = $package = '';
            $groups = $this->em->getRepository(UserGroup::class)
                ->getUserGroupsInActiveCourseStream($smartSenderUser->getUser());
            foreach ($groups as $group) {
                $groupName = $group['name'];

                if ($group['activeStream'] && $group['courseSlug'] === $botSlug) {
                    $words = explode('(', $groupName);
                    $product = trim(reset($words));

                    preg_match('#\((.*?)\)#', $groupName, $match);
                    if (isset($match[1])) {
                        $package = $match[1];
                    }
                }
            }

            if ($botProduct !== $product || $botPackage !== $package) {
                $output->writeln("{$botProduct}->{$product} | {$botPackage}->{$package}");
                $result = $this->smartSenderHelper->updateContact(
                    $smartSenderUser,
                    [
                        'product' => $product,
                        'package' => $package,
                    ]
                );
                $output->writeln(print_r($result, true));

                $result = $this->smartSenderHelper->triggerEventOnContact(
                    $smartSenderUser,
                    SmartSenderHelper::EVENT_FIX_EMPTY_PRODUCT
                );
                $output->writeln(print_r($result, true));
            }
        }

        return 0;
    }

    /** @return SmartSenderUser[] */
    private function getSmartSenderUsersFormFile(string $path): array
    {
        $file = fopen($path, 'rb');
        $emails = [];
        while (($email = fgets($file)) !== false) {
            $emails[] = trim($email);
        }

        $users = $this->authServer->getInternalUsers(['emails' => $emails]);
        $authIds = array_column($users['list'], 'id');

        return $this->em->getRepository(SmartSenderUser::class)->findByAuthIds($authIds);
    }

}

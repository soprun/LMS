<?php

namespace App\Command\SmartSender;

use App\Entity\SmartSender\SmartSenderUser;
use App\Entity\UserGroup;
use App\Service\SmartSenderHelper;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateWrongProductsCommand extends Command
{

    protected static $defaultName = 'smart-sender:update-wrong-products';

    private $em;
    private $smartSenderHelper;

    public function __construct(EntityManagerInterface $em, SmartSenderHelper $smartSenderHelper)
    {
        parent::__construct();
        $this->em = $em;
        $this->smartSenderHelper = $smartSenderHelper;
    }

    protected function configure()
    {
        $this->setDescription('Обновить неверные продукты в боте');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $botSlug = 'speed';
        $botName = 'Скорость';
        $courseName = 'Скорость Клуб 4';

        // забираем подключившихся пользователей
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(SmartSenderUser::class)->createQueryBuilder('ssu');
        /** @var SmartSenderUser[] $smartSenderUsers */
        $smartSenderUsers = $qb
            ->addSelect('ssb', 'u')
            ->innerJoin('ssu.smartSenderBot', 'ssb')
            ->innerJoin('ssu.user', 'u')
            ->innerJoin('u.groupRelations', 'ugu')
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ug.courseStream', 'cs')
            ->where('ssu.smartSenderUserId IS NOT NULL')
            ->andWhere('ssb.name = :botName')
            ->andWhere('ugu.deleted = 0')
            ->andWhere('cs.name = :courseName')
            ->setParameters(
                [
                    'botName' => $botName,
                    'courseName' => $courseName,
                ]
            )
            ->getQuery()
            ->getResult();

        $output->writeln(count($smartSenderUsers));

        $count = 0;
        foreach ($smartSenderUsers as $smartSenderUser) {
            $count++;
            $output->writeln(
                sprintf(
                    "%s) uid %s sid %s",
                    $count,
                    $smartSenderUser->getUser()->getId(),
                    $smartSenderUser->getSmartSenderUserId()
                )
            );

            $user = $this->smartSenderHelper->getContact($smartSenderUser);
            $botProduct = null;
            $botPackage = null;

            foreach ($user['variables'] as $v) {
                switch ($v['name']) {
                    case 'product':
                        $botProduct = $v['value'];
                        break;

                    case 'package':
                        $botPackage = $v['value'];
                        break;
                }
            }


            $product = $package = '';
            $groups = $this->em->getRepository(UserGroup::class)
                ->getUserGroupsInActiveCourseStream($smartSenderUser->getUser());
            foreach ($groups as $group) {
                $groupName = $group['name'];

                if ($group['activeStream'] && $group['courseSlug'] === $botSlug) {
                    $words = explode('(', $groupName);
                    $product = trim(reset($words));

                    preg_match('#\((.*?)\)#', $groupName, $match);
                    if (isset($match[1])) {
                        $package = $match[1];
                    }
                }
            }

            if ($botProduct !== $product || $botPackage !== $package) {
                $output->writeln("{$botProduct}->{$product} | {$botPackage}->{$package}");
                $result = $this->smartSenderHelper->updateContact(
                    $smartSenderUser,
                    [
                        'product' => $product,
                        'package' => $package,
                    ]
                );
                dump($result);

                $result = $this->smartSenderHelper->triggerEventOnContact(
                    $smartSenderUser,
                    SmartSenderHelper::EVENT_FIX_EMPTY_PRODUCT
                );
                dump($result);
            }
        }

        return 0;
    }

}

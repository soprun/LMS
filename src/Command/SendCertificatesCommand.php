<?php

namespace App\Command;

use App\Entity\Courses\Course;
use App\Entity\Courses\Lesson;
use App\Entity\UserGroup;
use App\Service\AuthServerHelper;
use App\Service\Course\CertificateService;
use App\Service\Course\LessonHelper;
use App\Service\FormHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendCertificatesCommand extends Command
{

    protected static $defaultName = 'app:send-certificates';

    private $em;
    private $formHelper;
    private $authServerHelper;
    private $certificateService;
    private $lessonHelper;

    public function __construct(
        EntityManagerInterface $em,
        FormHelper $formHelper,
        AuthServerHelper $authServerHelper,
        CertificateService $certificateService,
        LessonHelper $lessonHelper
    ) {
        parent::__construct();
        $this->em = $em;
        $this->formHelper = $formHelper;
        $this->authServerHelper = $authServerHelper;
        $this->certificateService = $certificateService;
        $this->lessonHelper = $lessonHelper;
    }

    protected function configure()
    {
        $this->setDescription('Отправка сертификатам пользователям')
            ->addArgument('courseId', InputArgument::REQUIRED)
            ->addArgument('groupId', InputArgument::REQUIRED); //айди курса и айди группы пользователей
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $courseId = $input->getArgument('courseId');//357
        $groupId = $input->getArgument('groupId');//240
        $course = $this->em->getRepository(Course::class)->find($courseId);
        /** @var UserGroup $groupUser */
        $groupUser = $this->em->getRepository(UserGroup::class)->find($groupId);

        $courseName = preg_split('/ /', $course->getName());
        foreach ($groupUser->getUserRelations() as $userRelation) {
            $answerUser = $userRelation->getUser();
            if (key_exists(0, $courseName) && key_exists(1, $courseName)) {
                if ($courseName[0] === 'Скорость' && is_numeric($courseName[1])) {
                    $allStopLessonsComplete = true;
                    /** @var Lesson $lesson */
                    foreach ($course->getLessons() as $key => $lesson) {
                        if ($lesson->getIsStopLesson() && !$lesson->getLessonConfiguration()->getIsHidden()) {
                            $lessonArray = $this->lessonHelper->getLessonArray($lesson, null, $answerUser);
                            if (!$lessonArray['task']['isAllChecked']) {
                                $allStopLessonsComplete = false;
                                break;
                            }
                        }
                    }
                    if ($allStopLessonsComplete) {
                        $certificateData = $this->certificateService->newCertificate($answerUser, $course);
                        if (!$certificateData['isSend']) {
                            $certificateData = $this->certificateService->getCertificate(
                                $certificateData['serialNumber']
                            );
                            $image = $this->certificateService->createCertificateImg($certificateData);
                            $certificateData['image'] = json_encode(base64_encode($image->getimageblob()));
                            [$authUser, $errors] = $this->authServerHelper->sendCertificate($certificateData);
                            if (!empty($errors)) {
                                if (!isset($authUser->lmsUserId)) {
                                    $this->formHelper->addError('lmsUserId', 'notSendFromAuthServer');
                                } else {
                                    $this->formHelper->addError($errors[0]['field'], $errors[0]['message']);
                                }
                            }
                        }
                        $output->writeln('Успешно отправили: ' . $answerUser->getId());
                    }
                }
            }
        }

        return 1;
    }

}

<?php

namespace App\Command\Group;

use App\Entity\User;
use App\Entity\UserGroup;
use App\Entity\UserGroupUser;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CopyUsersFromGroupCommand extends Command
{

    protected static $defaultName = 'group:copy-users';

    private $em;

    public function __construct(
        EntityManagerInterface $em
    ) {
        parent::__construct();
        $this->em = $em;
    }

    protected function configure()
    {
        $this->setDescription('Копирование участников из одной группы в другую')
            ->addArgument('targetGroup', InputArgument::REQUIRED, 'Куда копируем')
            ->addArgument('sourceGroup', InputArgument::REQUIRED, 'Откуда копируем');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var UserGroup $targetGroup */
        $targetGroup = $this->em->getRepository(UserGroup::class)->find($input->getArgument('targetGroup'));

        /** @var UserGroup $sourceGroup */
        $sourceGroup = $this->em->getRepository(UserGroup::class)->find($input->getArgument('sourceGroup'));

        $output->writeln(
            sprintf(
                "Переливаем из '%s' в '%s'",
                $sourceGroup->getName(),
                $targetGroup->getName()
            )
        );

        $addedBy = $this->em->getRepository(User::class)->find(User::SERVICE_USER);

        $progress = new ProgressBar($output);
        $progress->setMaxSteps($sourceGroup->getUserRelations()->count() + 1);
        $progress->setFormat('debug');
        $progress->display();

        sleep(3);

        foreach ($sourceGroup->getUserRelations() as $sourceRelation) {
            if ($sourceRelation->isDeleted()) {
                continue;
            }
            $progress->advance();

            $targetRelation = $this->em->getRepository(UserGroupUser::class)->findOneBy(
                [
                    'userGroup' => $targetGroup,
                    'user' => $sourceRelation->getUser(),
                ]
            );

            if ($targetRelation) {
                if ($targetRelation->isDeleted()) {
                    $targetRelation->setDeleted(false);
                    $this->em->persist($targetRelation);
                }
                continue;
            }

            $targetRelation = (new UserGroupUser())
                ->setAddedBy($addedBy)
                ->setAmoLeadId($sourceRelation->getAmoLeadId())
                ->setUser($sourceRelation->getUser())
                ->setUserGroup($targetGroup);
            $this->em->persist($targetRelation);
        }

        $this->em->flush();

        $progress->finish();

        return 0;
    }

}

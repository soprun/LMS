<?php

namespace App\Command\Group;

use App\Entity\UserGroupUser;
use App\Service\AuthServerHelper;
use App\Service\LikecentreDatabaseHelper;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CheckRefundsCommand extends Command
{

    protected static $defaultName = 'group:check-refunds';

    private $em;
    private $likecentreDatabaseHelper;
    public $authServerHelper;

    public function __construct(
        EntityManagerInterface $em,
        LikecentreDatabaseHelper $likecentreDatabaseHelper,
        AuthServerHelper $authServerHelper
    ) {
        parent::__construct();

        $this->em = $em;
        $this->likecentreDatabaseHelper = $likecentreDatabaseHelper;
        $this->authServerHelper = $authServerHelper;
    }

    protected function configure()
    {
        $this->setDescription('Проверка того, что забрали доступы');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $amoStatusId = 25700235;
        $courses = ['Сотка 4', 'Скорость. Клуб'];

        // ищем емейлы покупателей
        $emails = $this->getEmails($courses, $amoStatusId);
        $output->writeln('Всего покупателей: ' . count($emails));

        // в строчку
        $emails = implode(
            ',',
            array_map(
                static function (string $email) {
                    return sprintf(
                        "'%s'",
                        $email
                    );
                },
                $emails
            )
        );


        // ищем айди юзеров
        $authUsers = $this->authServerHelper->getUsers([], ['id'], "u.email IN({$emails})");
        $ids = array_column($authUsers, 'id');
        $output->writeln('Нашли юзеров: ' . count($ids));

        // ищем челов в группах
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(UserGroupUser::class)->createQueryBuilder('ugu');
        /** @var UserGroupUser[] $relations */
        $relations = $qb
            ->innerJoin('ugu.user', 'u')
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ug.courseStream', 'cs')
            ->where('cs.name IN(:names)')
            ->andWhere('ugu.deleted = 0')
            ->andWhere('u.id IN(:userIds)')
            ->setParameter('names', $courses)
            ->setParameter('userIds', $ids)
            ->getQuery()
            ->getResult();

        $output->writeln(
            sprintf(
                "До сих пор в группах: %s",
                count($relations)
            )
        );

        foreach ($relations as $relation) {
            $relation->setDeleted(true);
            $this->em->persist($relation);
            $output->writeln($relation->getUserGroup()->getName());
        }
        $this->em->flush();

        return 0;
    }

    private function getEmails(array $courses, int $amoStatusId): array
    {
        $names = implode(
            ',',
            array_map(
                static function (string $name) {
                    return sprintf(
                        "'%s'",
                        $name
                    );
                },
                $courses
            )
        );


        $sql = <<<SQL
            SELECT c.email
            FROM leads l
                     INNER JOIN amo_products ap ON l.amo_product_id = ap.amo_product_id
                     INNER JOIN contacts c ON l.contact_id = c.id
            WHERE l.amo_status_id = {$amoStatusId}
              AND l.deleted_at IS NULL
              AND ap.amo_product_name IN ({$names})
            GROUP BY c.email
        SQL;

        $result = $this->likecentreDatabaseHelper->execute($sql);

        return array_column($result, 'email');
    }

}

<?php

namespace App\Command\Group;

use App\Entity\UserGroup;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RemoveUsersFromGroupCommand extends Command
{

    protected static $defaultName = 'group:remove-users-from-group';

    private $em;

    public function __construct(
        EntityManagerInterface $em
    ) {
        parent::__construct();
        $this->em = $em;
    }

    protected function configure()
    {
        $this->setDescription('Жесткое удаление всех пользователей из группы')
            ->addArgument('groupId', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var UserGroup $group */
        $group = $this->em->getRepository(UserGroup::class)->find($input->getArgument('groupId'));
        $output->writeln('Группа ' . $group->getName());
        sleep(1);

        $progress = new ProgressBar($output);
        $progress->setFormat('debug');
        $progress->setMaxSteps($group->getUserRelations()->count());
        $progress->display();

        foreach ($group->getUserRelations() as $userRelation) {
            $this->em->remove($userRelation);
            $this->em->flush();
        }

        $progress->finish();

        return 0;
    }

}

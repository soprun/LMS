<?php

namespace App\Command\Group;

use App\Entity\Traction\Value;
use App\Entity\User;
use App\Entity\UserGroup;
use App\Entity\UserGroupUser;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TransferToNewStreamByTractionCommand extends Command
{

    protected static $defaultName = 'group:transfer-to-new-stream-by-traction';

    private $em;

    private $hundred = 'Сотка 7';
    private $speed = 'Скорость Клуб 4';

    // кто куда идет
    private $groups = [
        'Сотка 7 (бизнес)' => [
            'hundred' => 'Сотка 8 (бизнес)',
            'speed' => 'Скорость Клуб 5 (Бизнес)',
            'repeatGroup' => 'Сотка 8 Повторники (Бизнес)',
        ],
        'Сотка 7 (стандарт)' => [
            'hundred' => 'Сотка 8 (стандарт)',
            'speed' => 'Скорость Клуб 5 (стандарт)',
            'repeatGroup' => 'Сотка 8 Повторники (Стандарт)',
        ],
        'Сотка 7 (вип)' => [
            'hundred' => 'Сотка 8 (вип)',
            'speed' => 'Скорость Клуб 5 (ЛФ)',
            'repeatGroup' => 'Сотка 8 Повторники (Вип)',
        ],
    ];

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    protected function configure()
    {
        $this->setDescription('Перенос учеников на новый поток исходя из трекшена')
            ->addArgument('isRepeatGroup', InputArgument::OPTIONAL);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // все юзеры на Сотке
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(User::class)->createQueryBuilder('u');
        $rows = $qb
            ->select('DISTINCT u.id')
            // ищем челов в Сотке
            ->innerJoin('u.groupRelations', 'ugu')
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ug.courseStream', 'cs')
            ->where("cs.name = '{$this->hundred}'")
            ->andWhere('ugu.deleted = 0')
            ->andWhere("ug.name NOT LIKE '%сборная%'")
            ->getQuery()
            ->getResult();
        $allUsers = array_column($rows, 'id');
        $output->writeln('Всего на Сотке: ' . count($allUsers));

        // ищем тех, кто покупал Скорость
        $qb = $this->em->getRepository(User::class)->createQueryBuilder('u');
        $rows = $qb
            ->select('DISTINCT u.id')
            ->innerJoin('u.groupRelations', 'ugu')
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ug.courseStream', 'cs')
            ->where("cs.name = '{$this->speed}'")
            ->andWhere('ugu.deleted = 1')
            ->andWhere("ug.name NOT LIKE '%факультет%'")
            ->andWhere('u.id IN(:userIds)')
            ->setParameter('userIds', $allUsers)
            ->getQuery()
            ->getResult();
        $speedUsers = array_column($rows, 'id');
        $output->writeln('Покупали Скорость: ' . count($speedUsers));

        $goodIds = $this->findUsersByTraction($speedUsers);
        $output->writeln('Из них сделали сотку: ' . count($goodIds));

        $added = $this->addToGroup($goodIds, 'speed');
        $output->writeln('Добавили в следующий поток Скорости: ' . $added);


        $badIds = array_diff($speedUsers, $goodIds);
        $output->writeln('Не сделали сотку: ' . count($badIds));
        $added = $this->addToGroup($badIds, 'hundred');
        $output->writeln('Добавили в следующий поток Сотки: ' . $added);

        //добавление в группу повторников
        if ($input->getArgument('isRepeatGroup')) {
            $added = $this->addToGroup($badIds, 'repeatGroup');
            $output->writeln('Добавили в группу повторников: ' . $added);
        }

        return 0;
    }

    private function findUsersByTraction(array $ids): array
    {
        $rows = $this->em->getRepository(Value::class)
            ->createQueryBuilder('value')
            ->select('user.id', 'SUM(value.value) AS sum')
            ->innerJoin('value.owner', 'user')
            ->andWhere("value.type = 1 and value.date between '2021-10-25' and '2021-11-05'")
            ->andWhere('user.id IN (:userIds)')
            ->setParameter('userIds', $ids)
            ->groupBy('user.id')
            ->having('SUM(value.value) >= 100000')
            ->getQuery()
            ->getResult();

        return array_column($rows, 'id');
    }

    private function addToGroup(array $userIds, string $type): int
    {
        // ищем предыдущие группы
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(UserGroupUser::class)->createQueryBuilder('ugu');
        /** @var UserGroupUser[] $relations */
        $relations = $qb
            ->addSelect('u')
            ->innerJoin('ugu.user', 'u')
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ug.courseStream', 'cs')
            ->where("cs.name = '{$this->hundred}'")
            ->andWhere('ugu.deleted = 0')
            ->andWhere('u.id IN (:userIds)')
            ->andWhere("ug.name NOT LIKE '%сборная%'")
            ->setParameter('userIds', $userIds)
            ->getQuery()
            ->getResult();

        $cachedUsers = [];

        $added = 0;
        foreach ($relations as $relation) {
            // проверяем на повтор
            if (in_array($relation->getUser()->getId(), $cachedUsers)) {
                continue;
            }
            $cachedUsers[] = $relation->getUser()->getId();

            // определяем название след группы
            $name = $this->groups[$relation->getUserGroup()->getName()][$type];

            // ищем группу
            $userGroup = $this->getGroup($name);

            // добавляем в группу
            $targetRelation = $this->em->getRepository(UserGroupUser::class)->findOneBy(
                [
                    'userGroup' => $userGroup,
                    'user' => $relation->getUser(),
                ]
            );

            if ($targetRelation) {
                if ($targetRelation->isDeleted()) {
                    $added++;
                    $targetRelation->setDeleted(false);
                    $this->em->persist($targetRelation);
                }
                continue;
            }

            $added++;
            $targetRelation = (new UserGroupUser())
                ->setAddedBy($relation->getAddedBy())
                ->setAmoLeadId($relation->getAmoLeadId())
                ->setUser($relation->getUser())
                ->setUserGroup($userGroup);
            $this->em->persist($targetRelation);
        }

        $this->em->flush();

        return $added;
    }

    private function getGroup(string $name): UserGroup
    {
        // ищем в кеше
        static $groups = [];
        if (isset($groups[$name])) {
            return $groups[$name];
        }

        $userGroup = $this->em->getRepository(UserGroup::class)->findOneBy(['name' => $name]);
        $groups[$name] = $userGroup;

        return $userGroup;
    }

}

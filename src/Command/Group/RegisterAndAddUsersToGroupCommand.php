<?php

namespace App\Command\Group;

use App\Entity\User;
use App\Entity\UserGroup;
use App\Entity\UserGroupUser;
use App\Service\AuthServerHelper;
use App\Service\FormHelper;
use App\Service\UserHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RegisterAndAddUsersToGroupCommand extends Command
{

    protected static $defaultName = 'group:register-and-add-users-to-group';

    private $em;
    private $userHelper;
    private $formHelper;
    private $authServerHelper;

    public function __construct(
        EntityManagerInterface $em,
        UserHelper $userHelper,
        FormHelper $formHelper,
        AuthServerHelper $authServerHelper
    ) {
        parent::__construct();
        $this->em = $em;
        $this->userHelper = $userHelper;
        $this->formHelper = $formHelper;
        $this->authServerHelper = $authServerHelper;
    }

    protected function configure()
    {
        $this->setDescription('Регистрация пользователей в LMS и добавление их в группу')
            ->addArgument('groupId', InputArgument::REQUIRED)
            ->addArgument('filePath', InputArgument::REQUIRED, 'Ожидаем файл fullName;email;phone;budget;amoLeadId')
            ->addArgument(
                'courseName',
                InputArgument::OPTIONAL,
                'speed, hundred, likeFamily'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $courseName = $input->getArgument('courseName') ?? 'withoutCourse';
        $output->writeln(
            sprintf(
                'groupId: %s | courseName: %s',
                $input->getArgument('groupId'),
                $courseName
            )
        );
        sleep(3);

        // ищем группу
        /** @var UserGroup $userGroup */
        $userGroup = $this->em->getRepository(UserGroup::class)->find($input->getArgument('groupId'));

        $output->writeln('Группа ' . $userGroup->getName());

        // берем емейлы всех участников группы, чтобы проверять на повтор
        $groupUserIds = $userGroup->getUserRelations()->map(
            function (UserGroupUser $userRelation) {
                return $userRelation->getUser()->getAuthUserId();
            }
        )->toArray();
        $groupUserEmails = array_map(
            static function ($user) {
                return mb_strtolower($user['email']);
            },
            $this->userHelper->getUserInfoArrayByIds($groupUserIds)
        );

        $invalidEmails = [];
        $users = [];

        // подготавливаем входящие данные
        $fileRows = explode(PHP_EOL, file_get_contents($input->getArgument('filePath')));
        $output->writeln('Всего строк: ' . count($fileRows));
        foreach ($fileRows as $fileRow) {
            $fileRowArray = explode(';', $fileRow);
            if (!array_key_exists(1, $fileRowArray)) {
                continue;
            }

            // ищем имя-фамилию
            $fullNameArray = explode(' ', $fileRowArray[0]);
            $name = null;
            $lastName = null;
            if (!empty($fullNameArray)) {
                if (array_key_exists(1, $fullNameArray)) {
                    $name = array_key_exists(2, $fullNameArray) ? $fullNameArray[1] : $fullNameArray[0];
                    $lastName = array_key_exists(2, $fullNameArray) ? $fullNameArray[0] : $fullNameArray[1];
                } else {
                    $name = $fullNameArray[0];
                }
            }

            // отрабатываем несколько емейлов
            $emailArray = explode(',', $fileRowArray[1]);
            $email = trim(mb_strtolower($emailArray[0]));

            // валидируем емейл
            if (!$this->formHelper->toEmail($email)) {
                $invalidEmails[] = $email;
                continue;
            }

            // отрабатываем несколько телефонов
            $phone = null;
            if (array_key_exists(2, $fileRowArray)) {
                $phoneArray = explode(',', $fileRowArray[2]);
                $phone = $phoneArray[0];
            }

            $budget = null;
            if (isset($fileRowArray[3])) {
                $budget = (int) $fileRowArray[3];
            }

            $amoLeadId = null;
            if (isset($fileRowArray[4])) {
                $amoLeadId = (int) $fileRowArray[4];
            }

            // проверяем, есть ли такой пользователь в группе уже
            if (!in_array($email, $groupUserEmails)) {
                $users[] = [
                    'email' => $email,
                    'phone' => $phone,
                    'name' => $name,
                    'lastName' => $lastName,
                    'budget' => $budget,
                    'amoLeadId' => $amoLeadId
                ];
            }
        }

        $output->writeln('Пользователей для регистрации: ' . count($users));

        $count = 0;
        foreach ($users as $userKey => $user) {
            $email = $user['email'];

            $output->write($userKey . ') ' . $email . ' – ');

            // проверяем на повтор добавления
            if (in_array($email, $groupUserEmails)) {
                $output->writeLn('уже добавили в этой сессии');
                continue;
            }

            // отправляем запрос на регистрацию
            [$authUser, $errors] = $this->authServerHelper->registerAuthUser($user, true, $courseName);
            if (!empty($errors)) {
                if (!isset($authUser->lmsUserId)) {
                    $output->writeLn($errors[0]['message']);
                    continue;
                } else {
                    $output->write($errors[0]['message'] . ' – ');
                }
            }

            // ищем юзера
            /** @var User $lmsUser */
            $lmsUser = $this->em->getRepository(User::class)->find($authUser->lmsUserId);
            if (!$lmsUser) {
                $output->writeLn('lmsUser не найден');
                continue;
            }

            // добавляем в группу
            try {
                $userGroup->addUser($lmsUser, null, $user['budget'], $user['amoLeadId']);
                $this->em->persist($userGroup);
                $this->em->flush();
            } catch (\Exception $e) {
                $output->writeLn($e->getMessage());
                continue;
            }

            // добавляем в массив, чтобы не дублировать
            $groupUserEmails[] = $email;

            $count++;
            $output->writeLn($count);
        }

        $output->writeln('Добавили юзеров за сессию: ' . $count);

        if (!empty($invalidEmails)) {
            $output->writeln('Невалидные емейлы:');
            foreach ($invalidEmails as $invalidEmail) {
                $output->writeln($invalidEmail);
            }
        }

        return 1;
    }

}

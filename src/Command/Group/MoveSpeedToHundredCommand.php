<?php

namespace App\Command\Group;

use App\Entity\AbstractCourse;
use App\Entity\UserGroup;
use App\Entity\UserGroupUser;
use App\Service\SmartSenderHelper;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MoveSpeedToHundredCommand extends Command
{

    protected static $defaultName = 'group:move-speed-to-hundred';

    private $em;
    private $smartSenderHelper;

    public function __construct(EntityManagerInterface $em, SmartSenderHelper $smartSenderHelper)
    {
        parent::__construct();
        $this->em = $em;
        $this->smartSenderHelper = $smartSenderHelper;
    }

    protected function configure()
    {
        $this->setDescription('Жесткое удаление всех пользователей из группы');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(UserGroupUser::class)->createQueryBuilder('ugu');
        /** @var UserGroupUser[] $relations */
        $relations = $qb
            ->innerJoin('ugu.user', 'u')
            ->innerJoin('u.questionnaireUserAnswers', 'qua')
            ->innerJoin('qua.variant', 'qqav')
            ->innerJoin('qua.question', 'qq')
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ug.courseStream', 'cs')
            ->where("cs.name = 'Скорость. Клуб'")
            ->andWhere('ugu.deleted = 0')
            ->andWhere('qq.id = 1')
            ->andWhere("ug.name NOT LIKE '%повтор%'")
            ->andWhere("qqav.title IN('Найм', 'Нет работы')")
            ->getQuery()
            ->getResult();

        dump(count($relations));

        $count = 0;
        foreach ($relations as $relation) {
            dump('userId ' . $relation->getUser()->getId());

            $relation->setDeleted(true);
            $this->em->persist($relation);

            // определяем группу на Сотке
            $search = str_replace(
                'Скорость 19',
                'Сотка 4',
                $relation->getUserGroup()->getName()
            );

            /** @var null|UserGroup $userGroup */
            $userGroup = $this->em->getRepository(UserGroup::class)->findOneBy(['name' => $search]);
            if (!$userGroup) {
                dd(
                    $search . ' – 404'
                );
            }

            /** @var null|UserGroupUser $newRelation */
            $newRelation = $this->em->getRepository(UserGroupUser::class)->findOneBy(
                [
                    'user' => $relation->getUser(),
                    'userGroup' => $userGroup,
                ]
            );

            if ($newRelation && $newRelation->isDeleted()) {
                $newRelation->setDeleted(false);
                $newRelation->setFaculty(null);
                $this->em->persist($newRelation);
            }

            if (!$newRelation) {
                $newRelation = (new UserGroupUser())
                    ->setUserGroup($userGroup)
                    ->setUser($relation->getUser())
                    ->setAddedBy($relation->getAddedBy())
                    ->setAmoLeadId($relation->getAmoLeadId());
                $this->em->persist($newRelation);
            }

            foreach ($relation->getUser()->getTeams() as $team) {
                if ($team->getFolder()->getName() === 'Скорость. Клуб') {
                    $team->removeUser($relation->getUser());
                    $this->em->persist($team);
                    dump('удалили из группы');
                }
            }

            // отправляем запрос
            foreach ($relation->getUser()->getSmartSenderUsers() as $smartSenderUser) {
                if ($course = $smartSenderUser->getSmartSenderBot()->getAbstractCourse()) {
                    switch ($course->getSlug()) {
                        case AbstractCourse::SLUG_HUNDRED:
                            $result = $this->smartSenderHelper->triggerEventOnContact(
                                $smartSenderUser,
                                'toolbox_clear'
                            );
                            dump('toolbox_clear', $result);
                            break;

                        case AbstractCourse::SLUG_SPEED:
                        case AbstractCourse::SLUG_SPEED_CLUB:
                            $result = $this->smartSenderHelper->updateContact(
                                $smartSenderUser,
                                [
                                    'product' => '',
                                    'faculty' => '',
                                    'group' => '',
                                ]
                            );
                            dump('update', $result);
                            break;
                    }
                }
            }

            $this->em->flush();
            $output->writeln($count++);
        }

        return 0;
    }

}

<?php

namespace App\Command;

use App\Entity\CourseStream;
use App\Entity\MarathonInvestment\MarathonInvestment;
use App\Entity\Traction\Value;
use App\Service\AuthServerHelper;
use App\Service\Course\CertificateService;
use App\Service\FormHelper;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use ImagickException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SendCertificatesByCourseStreamsCommand extends Command
{

    protected static $defaultName = 'app:send-certificates-streams';

    private $em;
    private $formHelper;
    private $certificateService;
    private $authServerHelper;

    public function __construct(
        EntityManagerInterface $em,
        FormHelper $formHelper,
        CertificateService $certificateService,
        AuthServerHelper $authServerHelper
    ) {
        parent::__construct();
        $this->em = $em;
        $this->formHelper = $formHelper;
        $this->certificateService = $certificateService;
        $this->authServerHelper = $authServerHelper;
    }

    protected function configure(): void
    {
        $this->setDescription('Отправка сертификатов пользователям потоков')
            ->addArgument('stream', InputOption::VALUE_REQUIRED, 'Поток')
            ->addOption('userId', null, InputOption::VALUE_OPTIONAL, 'lmsId пользователя')
            ->addOption(
                'debug',
                'd',
                InputOption::VALUE_NONE,
                'Отправить тестовые письма?'
            );
    }

    /**
     * @throws ImagickException
     * @throws GuzzleException
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // Тестирование писем определенному пользователю
        if ($input->getOption('debug')) {
            $userId = (int)$input->getOption('userId');
            if ($userId) {
                $this->sendTestEmails($userId, $output);
            } else {
                $output->writeln('Для отправки тестовых писем введите userId');
            }

            return 1;
        }

        $stream = $this->em->getRepository(CourseStream::class)->find($input->getArgument('stream'));
        if ($stream) {
            // У потоков ищем пользователей которые заполнили все недели потока
            if ($stream->getAbstractCourse()->getSlug() === CertificateService::MI_SLUG) {
                $userIds = $this->getMiUsersIds($stream);
            } else {
                $userIds = $this->getSpeedOrHundredUserIds($stream);
            }

            $output->writeln('Создание и отправка сертификатов по потоку: ' . $stream->getName());

            $progressBar = new ProgressBar($output, count($userIds));
            $progressBar->setFormat('%current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s%');
            $progressBar->start();

            $this->sendEmails($userIds, $stream, $progressBar);
            $progressBar->finish();
        }

        return 1;
    }

    /**
     * Выбираем пользователей у которых заполнены все недели потока
     *
     * @param CourseStream $stream
     *
     * @return array
     * @throws Exception
     * @throws Exception
     */
    private function getSpeedOrHundredUserIds(CourseStream $stream): array
    {
        return $this->em->getRepository(Value::class)
            ->createQueryBuilder('value')
            ->select('user.id')
            ->join('value.owner', 'user')
            ->join('value.type', 'type')
            ->join('user.groupRelations', 'groupRelation')
            ->join('groupRelation.userGroup', 'userGroup')
            ->where('value.date between :startDate AND :endDate')
            ->setParameter('startDate', (clone $stream->getStartDate())->modify('-6 day'))
            ->setParameter('endDate', (clone $stream->getStartDate())->modify($stream->getPeriod() . 'week'))
            ->andWhere('type.id IN (:types)')
            ->setParameter('types', [1, 2])
            ->andWhere('groupRelation.deleted = 0')
            ->andWhere('userGroup.courseStream = :stream')
            ->setParameter('stream', $stream)
            ->groupBy('user.id')
            ->having('COUNT(value.value) >= :period')
            ->setParameter('period', $stream->getPeriod() * 2)
            ->getQuery()
            ->getResult();
    }

    /**
     * Выбираем пользователей у которых заполнены все недели марафона инвестиций (planned и passed и amount)
     *
     * @param CourseStream $stream
     *
     * @return array
     */
    private function getMiUsersIds(CourseStream $stream): array
    {
        return $this->em->getRepository(MarathonInvestment::class)
            ->createQueryBuilder('mi')
            ->select('u.id', 'COUNT(mi.id) as count')
            ->join('mi.user', 'u')
            ->where('mi.stream = :streamId')
            ->setParameter('streamId', $stream->getId())
            ->andWhere('mi.planned IS NOT NULL')
            ->andWhere('mi.passed IS NOT NULL')
            ->andWhere('mi.amount IS NOT NULL')
            ->having('COUNT(mi.id) >= :streamPeriod')
            ->setParameter('streamPeriod', $stream->getPeriod())
            ->groupBy('u.id')
            ->getQuery()
            ->getResult();
    }

    /**
     * Создаем по пользователям новые сертификаты и отправляем их, если сертификата еще не было
     *
     * @param array $userIds
     * @param CourseStream $stream
     * @param null|ProgressBar $progressBar
     * @param bool $isDebug
     *
     * @throws GuzzleException
     * @throws ImagickException
     * @throws Exception
     */
    private function sendEmails(
        array $userIds,
        CourseStream $stream,
        ?ProgressBar $progressBar = null,
        bool $isDebug = false
    ): void {
        foreach ($userIds as $id) {
            $certificateData = $this->certificateService->newCertificateByStream($id['id'], $stream);
            if (!$certificateData['isSend'] || $isDebug) {
                $certificateData = $this->certificateService->getCertificate(
                    $certificateData['serialNumber']
                );

                $image = $this->certificateService
                    ->createCertificateImg($certificateData);
                $image->scaleImage(1200, 0);
                $certificateData['image'] = json_encode(
                    base64_encode($image->getimageblob()),
                    JSON_THROW_ON_ERROR
                );
                [$authUser, $errors] = $this->authServerHelper
                    ->sendCertificate($certificateData, $stream->getAbstractCourse()->getSlug());

                if (!empty($errors)) {
                    if (!isset($authUser->lmsUserId)) {
                        $this->formHelper->addError('lmsUserId', 'notSendFromAuthServer');
                    } else {
                        $this->formHelper->addError($errors[0]['field'], $errors[0]['message']);
                    }
                }

                if ($progressBar) {
                    $progressBar->advance();
                }
            }
        }
    }

    /**
     * @param int $userId
     * @param OutputInterface $output
     *
     * @throws GuzzleException
     * @throws ImagickException
     */
    private function sendTestEmails(int $userId, OutputInterface $output): void
    {
        $streams = $this->em->getRepository(CourseStream::class)
            ->createQueryBuilder('cs')
            ->where('cs.name IN (:names)')
            ->setParameter('names', ['Сотка 4', 'Скорость Клуб', 'МИ 5'])
            ->getQuery()
            ->getResult();

        foreach ($streams as $stream) {
            $this->sendEmails([['id' => $userId]], $stream, null, true);
            $output->writeln('Отправлено ' . $stream->getName());
        }
    }

}

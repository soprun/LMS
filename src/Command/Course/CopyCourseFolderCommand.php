<?php

namespace App\Command\Course;

use App\Entity\Courses\CourseFolder;
use App\Service\DuplicateEntityService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CopyCourseFolderCommand extends Command
{
    protected static $defaultName = 'course:copy:folder';

    private $em;
    private $duplicateEntityService;

    public function __construct(
        EntityManagerInterface $em,
        DuplicateEntityService $duplicateEntityService
    ) {
        parent::__construct();
        $this->em = $em;
        $this->duplicateEntityService = $duplicateEntityService;
    }

    protected function configure(): void
    {
        $this->setDescription('Копирование папки курсов')
            ->addArgument('folder', InputOption::VALUE_REQUIRED, 'Папка');
    }

    /**
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $courseFolder = $this->em->getRepository(CourseFolder::class)
            ->find($input->getArgument('folder'));
        if ($courseFolder) {
            $courseFolder = $this->duplicateEntityService->duplicateFolder($courseFolder);
        }

        return 1;
    }

}

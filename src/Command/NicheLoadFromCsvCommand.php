<?php

namespace App\Command;

use App\Entity\BusinessArea;
use App\Entity\BusinessNiche;
use App\Entity\User;
use App\Repository\BusinessAreaRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Throwable;

/**
 * Запрос для выгрузки CSV файла с auth можно посмотреть в https://tracker.yandex.ru/LMS-579
 */
final class NicheLoadFromCsvCommand extends Command
{
    private const BUSINESS_TYPES = [
        0 => null,
        '1' => 'offline',
        '2' => 'online',
        '3' => 'combined',
    ];

    private const CATEGORIES = [
        1 => 'Нет бизнеса',
        2 => 'Оптовые продажи',
        3 => 'Продажа товаров',
        4 => 'Фастфуд и общепит',
        5 => 'Спорт и здоровье',
        6 => 'Красота',
        7 => 'Ремонт, строительство',
        8 => 'Образование',
        9 => 'Недвижимость; Аренда/Продажа',
        10 => 'Авто',
        11 => 'Туризм',
        12 => 'Производство',
        13 => 'Сезонный бизнес',
        14 => 'Доставка и логистика',
        15 => 'Стартаперы',
        16 => 'Консалтинг',
        17 => 'Маркетинг',
        18 => 'Продажа услуг',
    ];

    protected static $defaultName = 'niche:load-from-csv';
    protected static $defaultDescription = 'Загружает ниши из CSV файла';

    /** @var EntityManagerInterface */
    private $em;

    /** @var BusinessAreaRepository */
    private $areaRepository;

    public function __construct(
        BusinessAreaRepository $areaRepository,
        EntityManagerInterface $em
    ) {
        parent::__construct();
        $this->em = $em;
        $this->areaRepository = $areaRepository;
    }

    protected function configure(): void
    {
        $this->addArgument('file', InputArgument::REQUIRED, 'путь к txt/csv файлу с нишами');
    }

    /** @throws Throwable */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $progressBar = $io->createProgressBar();
        $progressBar->setRedrawFrequency(0);
        $progressBar->setFormat('%current% (%elapsed:6s% %memory:6s%) skipped %count_skipped%');
        $countSkipped = 0;
        $progressBar->setMessage(0, 'count_skipped');
        $progressBar->start();

        $categoriesMap = $this->getCategoriesToAreaMap();
        $userMap = $this->getUserMap();
        $csv = fopen($input->getArgument('file'), 'rb');
        $i = 0;
        while (($row = fgetcsv($csv)) !== false) {
            $i++;
            $progressBar->advance();
            [$authUserId, $name, $description, $businessTypeId, $categoryId, $franchises] = $row;
            $niche = new BusinessNiche();
            if (isset($userMap[$authUserId])) {
                /** @var User $user */
                $user = $this->em->getPartialReference(User::class, $userMap[$authUserId]);
            } else {
                $progressBar->setMessage(++$countSkipped, 'count_skipped');
                continue;
            }
            $niche->setUser($user);
            $niche->setName($name);
            $niche->setDescription($description ?: null);
            $niche->setFranchises($franchises ?: null);
            $niche->setType(self::BUSINESS_TYPES[(int)$businessTypeId]);
            /** @var BusinessArea $partArea */
            $partArea = $categoriesMap[$categoryId] ? $this->em->getPartialReference(
                BusinessArea::class,
                $categoriesMap[$categoryId]
            ) : null;
            $niche->setArea($partArea);
            $this->em->persist($niche);
            if ($i % 100 === 0) {
                $this->em->flush();
                $this->em->clear();
            }
        }

        $this->em->flush();

        $progressBar->finish();

        return 0;
    }

    /** @return array<int, int> */
    private function getCategoriesToAreaMap(): array
    {
        $categories = [];
        foreach (self::CATEGORIES as $id => $text) {
            $area = $this->areaRepository->findOneBy(['name' => $text]) ?? new BusinessArea();
            $area->setName($text);
            $this->em->persist($area);
            $categories[$id] = $area;
        }
        $this->em->flush();

        return array_combine(
            array_keys($categories),
            array_map(static function (BusinessArea $area) {
                return $area->getId();
            }, $categories)
        );
    }

    /** @throws Throwable */
    private function getUserMap(): array
    {
        $map = [];
        $sql = 'SELECT id, auth_user_id FROM user WHERE auth_user_id IS NOT NULL ';
        $result = $this->em->getConnection()->executeQuery($sql);
        while (($row = $result->fetchAssociative()) !== false) {
            $map[$row['auth_user_id']] = $row['id'];
        }

        return $map;
    }

}

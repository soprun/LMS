<?php

namespace App\Command\Team;

use App\Entity\City;
use App\Entity\CourseStream;
use App\Entity\Faculty;
use App\Entity\Questionnaire\Question;
use App\Entity\Team;
use App\Entity\TeamFolder;
use App\Entity\User;
use App\Service\AuthServerService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MakeTeamsByStream extends Command
{
    protected static $defaultName = 'teams:make-test';

    /** @var AuthServerService */
    private $authServerService;
    /** @var EntityManagerInterface  */
    private $em;

    /** @var string[] */
    private $timezones;
    /** @var TeamFolder */
    private $teamFolder;
    /** @var CourseStream */
    private $courseStream;
    /** @var int */
    private $teamNumber = 0;
    /** @var User|null */
    private $captain;

    public function __construct(
        EntityManagerInterface $em,
        AuthServerService $authServerService
    ) {
        parent::__construct();
        $this->em = $em;
        $this->authServerService = $authServerService;
    }

    protected function configure()
    {
        $this->setDescription('Сгруппировать нераспределенных пользователей ск/сотка по командам')
            ->addArgument('stream', InputArgument::REQUIRED) // "speed" - ск клуб 2, "hundred" - сотка 5
            ->addArgument('captain', InputArgument::OPTIONAL); // 72268 - Галина Воеводина, 10549 - Цурков Александр
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Достаём города и часовые пояса');
        $this->timezones = [];
        $cities = $this->em->getRepository(City::class)->findAll();
        foreach ($cities as $city) {
            $this->timezones[mb_strtolower(trim($city->getName()))] = $city->getTimezone();
        }

        if ($input->getArgument('captain') !== null) {
            $captainId = $input->getArgument('captain');
            if (! is_numeric($captainId)) {
                $output->writeln('ID Капитана должно быть числом!');
                exit();
            }

            $this->captain = $this->em->getRepository(User::class)->find($captainId);

            if ($captainId !== null && $this->captain === null) {
                $output->writeln('Капитан не найден в БД!');
                exit();
            }
        }
    }

    /**
     * @param  InputInterface  $input
     * @param  OutputInterface  $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $courseStreamId = $input->getArgument('stream');
        $this->courseStream = $this->em->getRepository(CourseStream::class)->find($courseStreamId);

        $output->writeln('Достаём папку команд');
        $this->teamFolder = $this->em->getRepository(TeamFolder::class)
            ->findOneBy(['courseStream' => $this->courseStream]);

//        $output->writeln('Достаём факультеты и нераспределённых пользователей');
//        $faculties = $this->getFaculties();
//        $this->fetchFaculties($faculties, $output, ' (второе распределение)');

        $output->writeln('Достаём факультеты и нераспределённых пользователей (бесплатники)');
        $faculties = $this->getFaculties();
        $this->fetchFaculties($faculties, $output, 'only', '');

        $output->writeln("\n\nДостаём факультеты и нераспределённых пользователей (платники)");
        $faculties = $this->getFaculties();
        $this->fetchFaculties($faculties, $output, 'exclude', 'п');

        return 0;
    }

    /**
     * @return Faculty[]
     */
    protected function getFaculties(): array
    {
        return $this->em->getRepository(Faculty::class)
            ->createQueryBuilder('faculty')
            ->innerJoin('faculty.userGroup', 'userGroup')
            ->innerJoin('userGroup.userRelations', 'userRelation')
            ->innerJoin('userRelation.user', 'user')
            ->where('faculty.courseStream = :courseStream')
            ->andWhere('userRelation.deleted = 0')
            ->setParameter('courseStream', $this->courseStream)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param  Faculty[]  $faculties
     * @param  OutputInterface  $output
     * @param  null|string  $type
     * @param  string  $teamSuffix
     */
    protected function fetchFaculties(
        array $faculties,
        OutputInterface $output,
        ?string $type = null,
        string $teamSuffix = ''
    ): void {
        $output->writeln('Начинаем перебирать факультеты');
        foreach ($faculties as $faculty) {
            $output->writeln("\nФакультет {$faculty->getId()}");
            $progressBar = new ProgressBar($output);
            $progressBar->start();
            $progressBar->setFormat('debug');

            $progressBar->setMessage('Перебираем пользователей');
            $users = $this->getUsers($faculty, $type);

            $progressBar->setMessage('Обогащаем данные пользователей');
            for ($i = 0; $i < ceil(count($users) / 50); $i++) {
                $this->authServerService->refillUsers(array_slice($users, $i * 50, 50));
            }

            $progressBar->setMessage('Распределяем пользователей по часовым поясам');
            $usersByTimezone = [];
            foreach ($users as $user) {
                $city = mb_strtolower(trim($user->getCity()));
                if ('москва' !== $city) {
                    $timezone = $this->timezones[$city] ?? 'Europe/Moscow';
                } else {
                    $timezone = 'Москва';
                }
                if (empty($timezone)) {
                    $usersByTimezone[$timezone] = [];
                }
                $usersByTimezone[$timezone][] = $user;
            }

            $progressBar->setMessage('Распределяем пользователей по командам');
            if (!isset($team) || !$team->getUsers()->isEmpty()) {
                $team = $this->createTeam((++$this->teamNumber) . $teamSuffix);
            }
            foreach ($usersByTimezone as $usersInTimezone) {
                foreach ($usersInTimezone as $user) {
                    if ($team->getUsers()->count() > 9) {
                        $this->saveTeam($team);
                        $team = $this->createTeam((++$this->teamNumber) . $teamSuffix);
                    }
                    $team->addUser($user);
                    $progressBar->advance();
                }
            }
            $this->saveTeam($team);
            $progressBar->finish();
        }
    }

    /**
     * @param  Faculty  $faculty
     * @param  null|string  $type
     * @return User[]
     */
    protected function getUsers(Faculty $faculty, ?string $type = null): array
    {
        $usersInTeams = $this->em->getRepository(User::class)
            ->createQueryBuilder('subUser')
            ->join('subUser.teams', 'subTeam')
            ->join('subTeam.folder', 'subFolder')
            ->where('subFolder.courseStream = :courseStream')
            ->getQuery();

        $freeUsersQuery = $this->em->getRepository(User::class)
            ->createQueryBuilder('subFreeUser')
            ->join('subFreeUser.groupRelations', 'subGroupRelation')
            ->join('subGroupRelation.userGroup', 'subUserGroup')
            ->join('subUserGroup.courseStream', 'subCourseStream')
            ->join('subCourseStream.abstractCourse', 'subAbstractCourse')
            ->where('subCourseStream.stream = :streamNumber and subAbstractCourse.slug = :courseSlug')
            ->andWhere('subGroupRelation.deleted = 0')
            ->getQuery();

        $qb = $this->em->getRepository(User::class)
            ->createQueryBuilder('user')
            ->innerJoin('user.groupRelations', 'groupRelation')
            ->innerJoin('groupRelation.userGroup', 'userGroup')
            ->innerJoin('userGroup.faculties', 'faculty')
            ->where('faculty = :faculty and groupRelation.deleted = 0')
            ->setParameter('faculty', $faculty);

        $qb->andWhere($qb->expr()->notIn('user', $usersInTeams->getDQL()))
            ->setParameter('courseStream', $this->courseStream);

        if ($type) {
            if ('only' === $type) {
                $qb->andWhere($qb->expr()->in('user', $freeUsersQuery->getDQL()));
            } elseif ('exclude' === $type) {
                $qb->andWhere($qb->expr()->notIn('user', $freeUsersQuery->getDQL()));
            }
            $qb->setParameter('streamNumber', $this->courseStream->getStream() - 1)
                ->setParameter('courseSlug', $this->courseStream->getAbstractCourse()->getSlug());
        }

        return $qb->getQuery()->getResult();
    }

    protected function saveTeam(Team $team): void
    {
        $wantLeader = $this->em->getRepository(User::class)
            ->createQueryBuilder('user')
            ->join('user.questionnaireUserAnswers', 'answer')
            ->join('answer.variant', 'variant')
            ->join('variant.question', 'question')
            ->where('question.slug = :slug and answer.courseStream = :courseStream')
            ->andWhere('user in (:users)')
            ->andWhere("variant.title = 'Да'")
            ->setParameter('slug', Question::WANT_LEADER_SLUG)
            ->setParameter('courseStream', $this->courseStream)
            ->setParameter('users', $team->getUsers())
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
        if ($wantLeader) {
            $team->setLeader($wantLeader);
        }

        $this->em->persist($team);
        $this->em->flush();
    }

    private function createTeam(string $name): Team
    {
        $captain = $this->captain ?? $this->getRandomUserFromGroup(413); //  413 - КАПИТАНСКИЙ ДОСТУП
        $seniorCaptain = $this->captain ?? $this->getRandomUserFromGroup(414); // 414 - СТАРШЕ-КАПИТАНСКИЙ ДОСТУП

        return (new Team())
            ->setName($name)
            ->setCaptain($captain)
            ->setSeniorCaptain($seniorCaptain)
            ->setFolder($this->teamFolder);
    }

    private function checkUser(int $userId): bool
    {
        foreach ($this->teams as $team) {
            if (in_array($userId, $team['users'])) {
                return true;
            }
        }
        return false;
    }

    private function loadExistedFaculties(string $groupName): void
    {
        $selectedUsers = [];

        $faculties = $this->em->getRepository(Faculty::class)->createQueryBuilder('f')
            ->innerJoin('f.teamFolder', 'tf')
            ->andWhere('tf.name = :name')
            ->setParameter('name', $groupName)
            ->getQuery()
            ->getResult();

        foreach ($faculties as $faculty) {
            /** @var User[] $users */
            $qb = $this->em->getRepository(User::class)->createQueryBuilder('u')
                ->select('u.id')
                ->innerJoin('u.groupRelations', 'ugu')
                ->innerJoin('ugu.userGroup', 'ug')
                ->andWhere('ugu.deleted = 0')
                ->andWhere('ugu.userGroup = :userGroup')
                ->setParameter('userGroup', $faculty->getUserGroup());

            if ($selectedUsers) {
                $qb->andWhere('u.id NOT IN(:selectedUsers)')
                    ->setParameter('selectedUsers', $selectedUsers);
            }
            $users = $qb->groupBy('u.id')->getQuery()->getResult();

            foreach ($users as $user) {
                $ids[] = $user['id'];
            }
            $this->faculties[$faculty->getId()] = $ids;

            foreach ($ids as $id) {
                $selectedUsers[] = $id;
            }
            $selectedUsers = array_unique($selectedUsers);
        }
    }

    private function loadExistedTeams(string $groupName): void
    {
        $teams = $this->em->getRepository(Team::class)->createQueryBuilder('f')
            ->innerJoin('f.folder', 'tf')
            ->andWhere('tf.name = :name')
            ->setParameter('name', $groupName)
            ->getQuery()
            ->getResult();

        foreach ($teams as $team) {
            $userIds = [];
            foreach ($team->getUsers() as $user) {
                $userIds[] = $user->getId();
            }

            foreach ($this->faculties as $key => $value) {
                if (in_array($userIds[0], $value)) {
                    $faculty = $key;
                    break;
                }
            }

            $this->teams[$team->getId()] = [
                'entity' => $team,
                'users' => $userIds,
                'leader' => $team->getLeader() ? $team->getLeader()->getId() : null,
                'handmade' => false,
                'faculty' => $faculty ?? null,
            ];
        }
    }

    private function getUsersInTeams()
    {
        $inTeamsUsers = [];
        foreach ($this->teams as $team) {
            foreach ($team['users'] as $user) {
                $inTeamsUsers[] = $user;
            }
        }
        return $inTeamsUsers;
    }

    private function addUserToTeam(Team $team, int $userId): void
    {
        $user = $this->em->getRepository(User::class)->find($userId);
        $team->addUser($user);
        $this->em->persist($team);
        $this->em->flush();

        $this->teams[$team->getId()]['users'][] = $userId;
    }

    private function findOrCreateTeam(int $userId, TeamFolder $folder): Team
    {
        foreach ($this->teams as $index => $team) {
            // пропускаем заполненные
            if (count($team['users']) > 9) {
                continue;
            }

            $teamUser = $team['users'][0];
            if ($team['faculty'] === null) {
                foreach ($this->faculties as $key => $value) {
                    if (array_search($teamUser, $value) !== false) {
                        $team['faculty'] = $key;
                        $this->teams[$index]['faculty'] = $key;
                        break;
                    }
                }
            }

            $faculty = $team['faculty'];
            if ($faculty && in_array($userId, $this->faculties[$faculty])) {
                return $team['entity'];
            }
        }
        return $this->createTeam($folder, count($this->teams) + 1);
    }

//    private function createTeam(TeamFolder $folder, int $name): Team
//    {
//        $captain = $this->getRandomUserFromGroup('Капитаны СК17');
//        $seniorCaptain = $this->getRandomUserFromGroup('Старшие капитаны СК17');
//
//        $team = (new Team())
//            ->setName("$name")
//            ->setCaptain($captain)
//            ->setSeniorCaptain($seniorCaptain)
//            ->setFolder($folder);
//
//        $this->em->persist($team);
//        $this->em->flush();
//
//        $this->teams[$team->getId()] = [
//            'entity' => $team,
//            'users' => [],
//            'leader' => null,
//            'handmade' => false,
//            'faculty' => null,
//        ];
//
//        return $team;
//    }

    private function getRandomUserFromGroup(int $groupId): User
    {
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(User::class)->createQueryBuilder('u');

        $users = $qb
            ->innerJoin('u.groupRelations', 'ugu')
            ->innerJoin('ugu.userGroup', 'ug')
            ->where('ug.id = :groupId')
            ->andWhere('ugu.deleted = 0')
            ->setParameter('groupId', $groupId)
            ->getQuery()
            ->getResult();

        shuffle($users);

        return $users[0];
    }
}

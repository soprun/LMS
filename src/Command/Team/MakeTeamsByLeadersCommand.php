<?php

namespace App\Command\Team;

use App\Entity\Team;
use App\Entity\User;
use App\Entity\UserGroup;
use App\Entity\UserGroupUser;
use App\Service\AuthServerHelper;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MakeTeamsByLeadersCommand extends Command
{

    protected static $defaultName = 'teams:make-by-leaders';

    private $em;
    private $authServerHelper;

    public function __construct(EntityManagerInterface $em, AuthServerHelper $authServerHelper)
    {
        parent::__construct();
        $this->em = $em;
        $this->authServerHelper = $authServerHelper;
    }

    // Давай логику авто распределения обкашляем
    // Берем все команды, где есть лидер, но менее 10 человек
    // Добавляем в незаполненные группы нераспределенных пока не будет 10 человек:
    // смотрим на город (чтобы был максимально из одного города)
    // смотрим на группу, чтобы нормал, повторники и предоплатники были друг с другом
    protected function configure()
    {
        $this
            ->setDescription('Сгруппировать нераспределенных пользователей по лидерам существующим командам')
            ->addArgument('random', InputArgument::OPTIONAL);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(Team::class)->createQueryBuilder('t');

        /** @var Team[] $teams */
        $teams = $qb
            ->innerJoin('t.folder', 'f')
            ->leftJoin('t.users', 'u')
            ->leftJoin('f.userGroups', 'ug')
            ->andWhere('t.leader IS NOT NULL')
            ->getQuery()
            ->getResult();

        $i = 0;
        foreach ($teams as $team) {
            $i++;

            $output->write(
                sprintf(
                    "%s) %s – %s -> ",
                    $i,
                    $team->getName(),
                    $team->getUsers()->count()
                )
            );
            if ($team->getUsers()->count() > 9) {
                $output->write('норм' . PHP_EOL);
                continue;
            }

            // группы, в которых будем искать
            $groupIds = $team->getFolder()->getUserGroups()->map(
                function (UserGroup $userGroup) {
                    return $userGroup->getId();
                }
            )->toArray();

            if (!$input->getArgument('random')) {
                // берем челов из группы, берем превалирующий город
                $authIds = $team->getUsers()->map(
                    function (User $user) {
                        return $user->getAuthUserId();
                    }
                )->toArray();

                $users = $this->authServerHelper->getUsers($authIds, ['id', 'city']);
                $cities = [];
                foreach ($users as $user) {
                    $city = $user['city'];
                    if (empty($city)) {
                        continue;
                    }

                    if (isset($cities[$city])) {
                        $cities[$city]++;
                    } else {
                        $cities[$city] = 1;
                    }
                }

                arsort($cities);
                $cities = array_flip($cities);
                $cityName = reset($cities);

                // ищем челов по самому популярному городу и группам
                $userIds = $this->findUsers($groupIds, "ui.city = '{$cityName}'");
            } else {
                // рандом
                $userIds = $this->findUsers($groupIds);
            }

            $added = 0;
            foreach ($userIds as $userId) {
                if ($team->getUsers()->count() > 9) {
                    break;
                }

                /** @var User $user */
                $user = $this->em->getRepository(User::class)->find($userId);
                $team->addUser($user);
                $this->em->persist($team);
                $added++;
            }
            $this->em->flush();
            $output->write('Добавили ' . $added . PHP_EOL);
        }

        return 0;
    }

    /**
     * Ищем нераспределнных пользователей для конкретных групп
     * @param  array  $groupIds
     * @param  string|null  $search
     * @return array
     */
    private function findUsers(array $groupIds, ?string $search = ''): array
    {
        // берем юзеров в командах
        $qb = $this->em->getRepository(Team::class)->createQueryBuilder('t');
        $rows = $qb
            ->select('u.id')
            ->innerJoin('t.users', 'u')
            ->groupBy('u.id')
            ->getQuery()
            ->getResult();
        $inTeams = array_map(
            static function ($row) {
                return $row['id'];
            },
            $rows
        );


        // оставляем тех, кто не в командах, но в группах
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(UserGroupUser::class)->createQueryBuilder('ugu');
        $qb = $qb
            ->select('u.authUserId AS authUserId')
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ugu.user', 'u')
            ->andWhere('ug.id IN(:userIds)')
            ->setParameter('userIds', $groupIds);

        if (!empty($inTeams)) {
            $qb
                ->andWhere('u.id NOT IN(:inTeams)')
                ->setParameter('inTeams', $inTeams);
        }

        $authIds = [];
        foreach ($qb->getQuery()->getResult() as $user) {
            $authIds[] = $user['authUserId'];
        }

        $users = $this->authServerHelper->getUsers($authIds, ['id'], $search);

        return array_map(
            static function ($user) {
                return $user['id'];
            },
            $users
        );
    }

}

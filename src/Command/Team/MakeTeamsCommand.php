<?php

namespace App\Command\Team;

use App\Entity\Questionnaire\Question;
use App\Entity\Team;
use App\Entity\TeamFolder;
use App\Entity\User;
use App\Service\AuthServerHelper;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MakeTeamsCommand extends Command
{

    protected static $defaultName = 'teams:make';

    private $em;
    private $authServerHelper;
    private $cityUsers = [];
    private $teams = [];

    public function __construct(EntityManagerInterface $em, AuthServerHelper $authServerHelper)
    {
        parent::__construct();
        $this->em = $em;
        $this->authServerHelper = $authServerHelper;
    }

    protected function configure()
    {
        $this->setDescription('Сгруппировать нераспределенных пользователей по командам');
    }


    /**
     * Что делает:
     * - Добавляет свободную команду с юзерами из города нераспределенного
     * - Устанавливает лидеров
     *
     * @param  InputInterface  $input
     * @param  OutputInterface  $output
     * @return int
     * @todo: учитывать что не все хотят быть лидерами (сейчас полагаемся на удачу)
     * @todo: можно уменьшить затраты памяти (сейчас доходить до 130 MiB)
     * @todo: резолв дублей. Кейс: добавили ранее вручную, а потом поменяли группы
     *
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        //        /** @var Team[] $teams */
        //        $teams = $this->em->getRepository(Team::class)->findBy(['name' => 'Команда']);
        //        foreach ($teams as $team) {
        //            foreach ($team->getUsers() as $user) {
        //                $team->removeUser($user);
        //            }
        //            $this->em->remove($team);
        //        }
        //        $this->em->flush();
        //        exit;

        $progressBar = new ProgressBar($output);
        $progressBar->start();
        $output->writeln('');
        $progressBar->setFormat('debug');

        // кешируем города всех юзеров
        $this->updateCities();
        $progressBar->advance();

        // формируем команды
        /** @var TeamFolder[] $folders */
        $folders = $this->em->getRepository(TeamFolder::class)->findAll();
        foreach ($folders as $folder) {
            $users = $this->getUnallocatedByFolder($folder);
            foreach ($users as $user) {
                $team = $this->findOrCreateTeam($user, $folder);
                $this->addUserToTeam($team, $user);
                $progressBar->advance();
            }
        }

        // устанавливаем лидеров
        foreach ($this->teams as $teams) {
            foreach ($teams as $teamId => $teamArray) {
                if ($teamArray['leader'] || $teamArray['handmade']) {
                    continue;
                }

                $wantLeaders = $this->em->getRepository(Question::class)
                    ->getWantBeLeaders($teamArray['users']);
                if ($leaderId = reset($wantLeaders)) {
                    $user = $this->em->getRepository(User::class)->find($leaderId);

                    /** @var Team $team */
                    $team = $teamArray['entity'];
                    $team->setLeader($user);
                    $this->em->persist($team);

                    $progressBar->advance();
                }
            }
        }
        $this->em->flush();

        $progressBar->finish();

        return 0;
    }

    /**
     * @param  TeamFolder  $folder
     * @return User[]
     */
    private function getUnallocatedByFolder(TeamFolder $folder): array
    {
        $inTeams = [];
        foreach ($folder->getTeams() as $team) {
            $this->teams[$folder->getId()][$team->getId()] = [
                'entity' => $team,
                'users' => [],
                'leader' => $team->getLeader() ? $team->getLeader()->getId() : null,
                'handmade' => !is_null($team->getCreatedBy()),
            ];

            foreach ($team->getUsers() as $user) {
                $this->teams[$folder->getId()][$team->getId()]['users'][] = $user->getId();
                $inTeams[] = $user->getId();
            }
        }

        // берем нераспределенных
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(User::class)->createQueryBuilder('u');
        /** @var User[] $userEntities */
        $userEntities = $qb
            ->select('u')
            ->innerJoin('u.groupRelations', 'ugu')
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ug.teamFolders', 'tf')
            ->innerJoin('tf.teams', 't')
            ->andWhere('ugu.deleted = 0')
            ->andWhere("ug.name NOT LIKE '%минимальный%'")
            ->andWhere('u.id NOT IN(:inTeams)')
            ->andWhere('tf = :folder')
            ->setParameters(
                [
                    'folder' => $folder,
                    'inTeams' => $inTeams,
                ]
            )
            ->groupBy('u')
            ->getQuery()
            ->getResult();

        // сортировка достойная 12-часового рабочего дня
        $users = $sortUsers = [];
        foreach ($userEntities as $user) {
            $userId = $user->getId();
            $sortUsers[$userId] = $this->cityUsers[$userId] ?? '';
            $users[$userId] = $user;
        }
        arsort($sortUsers);

        return array_map(
            static function ($userId) use ($users) {
                return $users[$userId];
            },
            array_keys($sortUsers)
        );
    }

    private function updateCities(): void
    {
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(User::class)->createQueryBuilder('u');
        $users = $qb
            ->select('u.authUserId')
            ->innerJoin('u.groupRelations', 'ugu')
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ug.teamFolders', 'tf')
            ->andWhere('ugu.deleted = 0')
            ->groupBy('u.authUserId')
            ->getQuery()
            ->getResult();
        $authIds = array_map(
            static function ($user) {
                return $user['authUserId'];
            },
            $users
        );

        $authUsers = $this->authServerHelper->getUsers($authIds, ['id', 'city']);
        foreach ($authUsers as $authUser) {
            $this->cityUsers[$authUser['id']] = mb_strtolower($authUser['city']);
        }
    }

    private function findOrCreateTeam(User $user, TeamFolder $folder): Team
    {
        $userCity = $this->cityUsers[$user->getId()] ?? '';
        $normalTeam = null;

        foreach ($this->teams[$folder->getId()] as $team) {
            // пропускаем заполненные и рукотворные
            if ($team['handmade'] || count($team['users']) > 9) {
                continue;
            }

            foreach ($team['users'] as $userId) {
                // используем существующую команду, если в ней есть чел из того же города
                if (isset($this->cityUsers[$userId]) && $userCity === $this->cityUsers[$userId]) {
                    return $team['entity'];
                }
            }

            // кешируем первую понравившуюся команду, если вдруг не найдем ничего получше
            if (!$normalTeam) {
                $normalTeam = $team['entity'];
            }
        }

        return $normalTeam ?? $this->createTeam($folder);
    }

    private function createTeam(TeamFolder $folder): Team
    {
        $name = "Команда";
        $captain = $this->getRandomUserFromGroup('Капитаны СК17');
        $seniorCaptain = $this->getRandomUserFromGroup('Старшие капитаны СК17');

        $team = (new Team())
            ->setName($name)
            ->setCaptain($captain)
            ->setSeniorCaptain($seniorCaptain)
            ->setFolder($folder);

        $this->em->persist($team);
        $this->em->flush();

        $this->teams[$folder->getId()][$team->getId()] = [
            'entity' => $team,
            'users' => [],
            'leader' => null,
            'handmade' => false,
        ];

        return $team;
    }

    private function addUserToTeam(Team $team, User $user): void
    {
        $team->addUser($user);
        $this->em->persist($team);
        $this->em->flush();

        $this->teams[$team->getFolder()->getId()][$team->getId()]['users'][] = $user->getId();
    }

    private function getRandomUserFromGroup(string $groupName): User
    {
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(User::class)->createQueryBuilder('u');

        $users = $qb
            ->innerJoin('u.groupRelations', 'ugu')
            ->innerJoin('ugu.userGroup', 'ug')
            ->where('ug.name = :groupName')
            ->andWhere('ugu.deleted = 0')
            ->setParameter('groupName', $groupName)
            ->getQuery()
            ->getResult();

        shuffle($users);

        return $users[0];
    }
}

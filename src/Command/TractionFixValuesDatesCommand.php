<?php

namespace App\Command;

use App\Doctrine\StringableDateTime;
use App\DTO\Traction\UserTractionForSettlementPeriod;
use App\Entity\Traction\Value;
use App\Entity\User;
use App\Repository\Traction\ValueRepository;
use App\Repository\UserRepository;
use App\Service\Traction\TractionService;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class TractionFixValuesDatesCommand extends Command
{
    protected static $defaultName = 'app:traction:fix-values-dates';
    protected static $defaultDescription = 'Привязывает даты значений трекшена к началам периодов';
    /** @var TractionService */
    private $tractionService;
    /** @var UserRepository */
    private $userRepository;
    /** @var ValueRepository */
    private $valueRepository;
    /** @var SymfonyStyle */
    private $io;
    /** @var EntityManagerInterface */
    private $em;
    /** @var bool */
    private $safely;


    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addOption('without-confirm', null, null)
            ->addOption('safely')
            ->addArgument('from', InputArgument::OPTIONAL, 'Дата начала Y-m', '2017-01-01')
            ->addArgument('to', InputArgument::OPTIONAL, 'Дата окончания Y-m', date('Y-12-31'));
    }

    public function __construct(
        TractionService $tractionService,
        UserRepository $userRepository,
        ValueRepository $valueRepository,
        EntityManagerInterface $em
    ) {
        parent::__construct();
        $this->tractionService = $tractionService;
        $this->userRepository = $userRepository;
        $this->valueRepository = $valueRepository;
        $this->em = $em;
    }

    /** @throws Exception */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $from = new DateTime($input->getArgument('from'));
        $to = new DateTime($input->getArgument('to'));

        $this->safely = $input->getOption('safely');

        $this->io = new SymfonyStyle($input, $output);

        if (!$input->getOption('without-confirm')) {
            $question = 'Будут обработаны значения пользователей в промежутке с '
                . $from->format('m.Y') . ' по ' . $to->format('m.Y')
                . '. Продолжить?';
            if (!$this->io->confirm($question, false)) {
                return 0;
            }
        }

        $userIds = $this->userRepository->getUserIdsWhichHasTractionValue($from, $to);
        $progressBar = $this->io->createProgressBar();
        $progressBar->setRedrawFrequency(0);
        $format = 'user #%user_id% %current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s%'
            . PHP_EOL;
        $progressBar->setFormat($format);
        foreach ($progressBar->iterate($userIds) as $id) {
            $progressBar->setMessage($id, 'user_id');
            /** @var User $user */
            $user = $this->userRepository->find($id);
            $traction = $this->tractionService->makeByUser($user, $from, $to);
            $this->fixValues($user, $traction);
        }

        return 0;
    }

    /** @param UserTractionForSettlementPeriod[] $traction */
    private function fixValues(User $user, array $traction): void
    {
        foreach ($traction as $i => $period) {
            switch ($this->getSettlementPeriod($period)) {
                case '1 week':
                    $notMoveFromNextMonthFirstDay = isset($traction[$i + 1])
                        && $this->getSettlementPeriod($traction[$i + 1]) === '1 month';
                    $this->fixForWeek($period, $user, $notMoveFromNextMonthFirstDay);
                    break;
                case '1 month':
                    $this->fixForMonth($period, $user);
                    break;
            }
        }
    }

    //Определяем приблизительный расчетный период между днем, неделей и месяцем
    private function getSettlementPeriod(UserTractionForSettlementPeriod $period): string
    {
        $days = $period->getFrom()->diff($period->getTo())->days;
        switch ($days) {
            case $days <= 1:
                return '1 day';
            case $days > 1 && $days <= 7:
                return '1 week';
            case $days >= 20 && $days <= 35:
                return '1 month';
            default:
                throw new RuntimeException('Не удалось определить период');
        }
    }

    private function fixForMonth(UserTractionForSettlementPeriod $period, User $user): void
    {
        $valuesInPeriod = $this->valueRepository->getUserValuesByDate(
            $user,
            $period->getKeyDate(),
            $period->getTo()
        );
        $this->restoreValuesToDate($valuesInPeriod, $period->getKeyDate());
    }

    private function fixForWeek(
        UserTractionForSettlementPeriod $period,
        User $user,
        bool $notMoveFromNextMonthFirstDay = false
    ): void {
        $valuesInPeriod = $this->valueRepository->getUserValuesByDate(
            $user,
            $period->getKeyDate(),
            $period->getTo()
        );
        $this->restoreValuesToDate($valuesInPeriod, $period->getKeyDate(), $notMoveFromNextMonthFirstDay);
    }

    private function restoreValuesToDate(
        array $valuesInPeriod,
        DateTimeInterface $keyDate,
        bool $notMoveFromNextMonthFirstDay = false
    ): void {
        $valuesByType = [];
        foreach ($valuesInPeriod as $value) {
            $valuesByType[$value->getType()->getId()][] = $value;
        }
        foreach ($valuesByType as $type => $values) {
            $correctValues = array_filter($values, static function (Value $v) use ($keyDate) {
                return $v->getDate() == $keyDate;
            });
            $values = array_udiff($values, $correctValues, static function (object $v1, object $v2) {
                return $v1 <=> $v2;
            });
            if ($notMoveFromNextMonthFirstDay) {
                $nextMonthFirstDay = (clone $keyDate)->modify('first day of next month');
                $values = array_udiff(
                    $values,
                    array_filter($values, static function (Value $v) use ($nextMonthFirstDay) {
                        return $v->getDate() != $nextMonthFirstDay;
                    }),
                    static function (object $v1, object $v2) {
                        return $v1 <=> $v2;
                    }
                );
            }
            if (!empty($values) && count($correctValues) === 0) {
                usort($values, static function (Value $a, Value $b) {
                    return $a->getUpdatedAt() <=> $b->getUpdatedAt();
                });
                /** @var Value $lastVal */
                $lastVal = array_pop($values);
                $this->io->writeln(
                    'Переносим значение (type: ' . $type . ', user ' . $lastVal->getOwner()->getId() . ') с '
                    . $lastVal->getDate()->format('Y-m-d') . ' на ' . $keyDate->format('Y-m-d'),
                    OutputInterface::VERBOSITY_VERBOSE
                );
                try {
                    $lastVal->setDate(new StringableDateTime($keyDate->format('Y-m-d')));
                    if (!$this->safely) {
                        $this->em->persist($lastVal);
                    }
                } catch (Exception $e) {
                    throw new RuntimeException('Ошибка при изменении даты значения');
                }
            }
            foreach ($values as $value) {
                $this->io->writeln(
                    'Удаляем значение (type: ' . $type . ', user ' . $value->getOwner()->getId() . ') '
                    . $value->getDate()->format('Y-m-d'),
                    OutputInterface::VERBOSITY_VERY_VERBOSE
                );
                if (!$this->safely) {
                    $this->em->remove($value);
                }
            }
        }
        if (!$this->safely) {
            $this->em->flush();
        }
        $this->em->clear();
    }

}

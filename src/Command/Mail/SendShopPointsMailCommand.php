<?php

namespace App\Command\Mail;

use App\Entity\Courses\Course;
use App\Entity\User;
use App\Entity\UserGroup;
use App\Service\AuthServerHelper;
use App\Service\MailerHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendShopPointsMailCommand extends Command
{

    protected static $defaultName = 'app:send-shop-points-mail';

    private $em;
    private $mailerHelper;
    private $authServerHelper;

    public function __construct(
        EntityManagerInterface $em,
        MailerHelper $mailerHelper,
        AuthServerHelper $authServerHelper
    ) {
        parent::__construct();
        $this->em = $em;
        $this->mailerHelper = $mailerHelper;
        $this->authServerHelper = $authServerHelper;
    }

    protected function configure()
    {
        $this->setDescription('Рассылка с предложением поменять баллы на продукты ЛЦ');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $course = $this->em->getRepository(Course::class)->find(77);

        $url = "https://lms.toolbox.bz/courses/course/{$course->getId()}/points_shop/";

        $authUserIds = [];
        $groups = $this->em->getRepository(UserGroup::class)->findBy(
            [
                'name' => [
                    'Скорость 12 (Стандарт)',
                    'Скорость 12 (Бизнес)',
                    'Скорость 12 (Вип)',
                ],
            ]
        );
        foreach ($groups as $group) {
            /** @var UserGroup $group */
            foreach ($group->getUserRelations() as $userRelation) {
                if ($authUserId = $userRelation->getUser()->getAuthUserId()) {
                    if (!in_array($authUserId, $authUserIds)) {
                        $authUserIds[] = $authUserId;
                    }
                }
            }
        }

        $output->writeln('Пользователей: ' . count($authUserIds));

        $users = $this->authServerHelper->getUsersInfo($authUserIds);
        $output->writeln('Пользователей с Auth: ' . count($users));

        foreach ($users as $userKey => $user) {
            $email = $user->email;
            $user = $this->em->getRepository(User::class)->find($user->lmsUserId);
            if (!$user) {
                $output->writeln(($userKey + 1) . ') ' . $email . ' – не найден');
            }

            $points = $this->em->getRepository(Course::class)
                ->getUserPointsSum($course, $user);

            $this->mailerHelper->sendPointsShop($email, $points, $url);
            $output->writeln(($userKey + 1) . ') ' . $email);
        }

        return 1;
    }

}

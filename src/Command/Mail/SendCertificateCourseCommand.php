<?php

namespace App\Command\Mail;

use App\Entity\Courses\Course;
use App\Entity\Task\AnswerTaskType1;
use App\Service\AuthServerHelper;
use App\Service\Course\CertificateService;
use App\Service\MailerHelper;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendCertificateCourseCommand extends Command
{

    protected static $defaultName = 'app:send-certificate-course-mail';

    private $em;
    private $mailerHelper;
    private $authServerHelper;
    private $certificateService;

    public function __construct(
        EntityManagerInterface $em,
        MailerHelper $mailerHelper,
        AuthServerHelper $authServerHelper,
        CertificateService $certificateService
    ) {
        parent::__construct();
        $this->em = $em;
        $this->mailerHelper = $mailerHelper;
        $this->authServerHelper = $authServerHelper;
        $this->certificateService = $certificateService;
    }

    protected function configure()
    {
        $this->setDescription('Рассылка сертификатов курса');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //Курс на котором получен сертификат
        /** @var Course $course */
        $course = $this->em->getRepository(Course::class)->find(184);//Скорость 13
        $output->writeln('Курс: ' . $course->getName());

        $authUserIds = [];
        $lmsUsers = new ArrayCollection();
        //Все ответы на последнее задание по этому курсу где получены баллы
        $taskId = 2658;//courses/lesson/2993 - последний стоп урок ск13
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(AnswerTaskType1::class)->createQueryBuilder('a');
        $qb->select('a', 'u')
            ->innerJoin('a.user', 'u')
            ->innerJoin('a.task', 'task')
            ->where('task = :taskId')
            ->setParameter('taskId', $taskId)
            ->andWhere('a.points > 0');

        $answers = $qb->getQuery()->getResult();

        /** @var AnswerTaskType1 $answer */
        foreach ($answers as $answer) {
            $lmsUsers->add($answer->getUser());
            $authUserIds[] = $answer->getUser()->getAuthUserId();
        }

        $output->writeln('Пользователей: ' . count($authUserIds));

        $users = $this->authServerHelper->getUsersInfo($authUserIds);
        $output->writeln('Пользователей с Auth: ' . count($users));

//        $users = [
//            (object) ['email' => 'demonzarov@yandex.by', 'lmsUserId' => 3934]
//        ];

        $sendingCount = 0;
        foreach ($users as $userKey => $user) {
            $email = $user->email;
            $lmsUserId = $user->lmsUserId;
            $lmsUser = $lmsUsers->filter(
                function ($e) use ($lmsUserId) {
                    if ($e->getId() == $lmsUserId) {
                        return $e;
                    }
                }
            );

            if (!$lmsUser) {
                $output->writeln(($userKey + 1) . ') ' . $email . ' – не найден');
                continue;
            } else {
                $lmsUser = $lmsUser->first();
            }

            $certificateData = $this->certificateService->newCertificate($lmsUser, $course);
            if (!$certificateData['isSend']) {
                $certificateData = $this->certificateService->getCertificate($certificateData['serialNumber']);
                $image = $this->certificateService->createCertificateImg($certificateData);
                $certificateData['image'] = json_encode(base64_encode($image->getimageblob()));
                [$authUser, $errors] = $this->authServerHelper->sendCertificate($certificateData);
                if (!empty($errors)) {
                    if (!isset($authUser->lmsUserId)) {
                        $output->writeln(($authUser->lmsUserId) . ') – notSendFromAuthServer');
                        break;
                    } else {
                        $output->writeln([$errors[0]['field'], $errors[0]['message']]);
                    }
                }
                $sendingCount++;
                $output->writeln('(' . ($userKey + 1) . ') ' . $email . '- SEND CERTIFICATE');
            }
        }


        $output->writeln('Всего было найдено пользователей: ' . count($users));
        $output->writeln('Всего отправили: ' . ($sendingCount));

        return 1;
    }

}

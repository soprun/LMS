<?php

namespace App\Command;

use App\Entity\BaseBlock\BaseBlockTaskWithCheck;
use App\Entity\Courses\Course;
use App\Entity\Courses\LessonPartBlock;
use App\Entity\GroupPermission;
use App\Entity\Notification;
use App\Entity\Task\AnswerTaskType1;
use App\Entity\Task\TaskSet;
use App\Entity\User;
use App\Service\Course\FirebasePushHelper;
use App\Service\UserHelper;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class NotificationDDLTasks extends Command
{

    protected static $defaultName = 'app:notification-ddl-tasks';

    private $em;
    private $firebasePushHelper;
    private $logger;
    private $userHelper;

    public function __construct(
        EntityManagerInterface $em,
        FirebasePushHelper $firebasePushHelper,
        LoggerInterface $logger,
        UserHelper $userHelper
    ) {
        parent::__construct();
        $this->em = $em;
        $this->firebasePushHelper = $firebasePushHelper;
        $this->logger = $logger;
        $this->userHelper = $userHelper;
    }

    protected function configure()
    {
        $this->setDescription('Уведомление о сроке сдачи задания');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $userIds = [];
        /** @var Course[] $courses */
        $courses = $this->em->getRepository(Course::class)
            ->createQueryBuilder('c')
            ->where('c.deleted = 0')
            ->orderBy('c.id', 'desc')
            ->getQuery()
            ->getResult();

        foreach ($courses as $course) {
            // Для каждого курса получает список блоков
            $lessonPartBlocks = $this->em->getRepository(LessonPartBlock::class)->findByCourseTaskBlocks($course);

            foreach ($lessonPartBlocks as $lessonPartBlock) {
                if (1 === $lessonPartBlock['variety']) {
                    /** @var BaseBlockTaskWithCheck $taskBlock */
                    $taskBlock = $this->em->getRepository(BaseBlockTaskWithCheck::class)
                        ->find($lessonPartBlock['blockId']);

                    $dateTime = $taskBlock->getDdl();
                    // Переводим текущее время в московское
                    $now = new \DateTime('now');
                    $now->setTimezone(new \DateTimeZone('Europe/Moscow'));

                    if (
                        $dateTime
                        && ($dateTime->getTimestamp() - 18000) <= $now->getTimestamp()
                        && $now->getTimestamp() < ($dateTime->getTimestamp() - 14400)
                    ) {
                        /** @var QueryBuilder $qb */
                        $qb = $this->em->getRepository(TaskSet::class)
                            ->createQueryBuilder('ts');
                        $sets = $qb->select('distinct ts.blockId')
                            ->where('ts.deleted = 0')
                            ->andWhere('ts.variety = 1 and ts.blockTaskWithCheck = :blockTaskWithCheck')
                            ->setParameter('blockTaskWithCheck', $taskBlock)
                            ->getQuery()
                            ->getResult();

                        foreach ($sets as $set) {
                            foreach ($course->getUserGroups() as $userGroup) {
                                foreach ($userGroup->getUserRelations() as $userRelation) {
                                    /** @var User $user */
                                    $user = $userRelation->getUser();
                                    $isUserAdmin = $this->em->getRepository(GroupPermission::class)
                                        ->findByUserIsAdmin($user);
                                    if ($isUserAdmin) {
                                        break;
                                    }

                                    $taskAnswer = $this->em->getRepository(AnswerTaskType1::class)
                                        ->findBy(['task' => $set['blockId'], 'user' => $user]);

                                    if (!$taskAnswer) {
                                        $userIds[] = $user->getId();
                                        $this->createNotification(
                                            'Задание ждёт вашего ответа',
                                            "Осталось меньше 5 часов",
                                            Notification::ICON_HOMEWORK,
                                            $user,
                                            Notification::LINK_TYPE_LESSON,
                                            $lessonPartBlock['lessonId'] ?? null
                                        );
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $this->logger->info(
            sprintf(
                "NotificationDDLTasks: Оповестили %s человек: %s",
                count($userIds),
                implode(',', $userIds)
            )
        );

        return 0;
    }

    private function createNotification(
        $title,
        $description,
        $iconId,
        User $user,
        $linkTypeId = Notification::LINK_TYPE_NO_LINK,
        $linkContent = null,
        $statusId = Notification::STATUS_NEW
    ) {
        if (!$linkContent) {
            $linkTypeId = Notification::LINK_TYPE_NO_LINK;
        }

        $notification = new Notification();
        $notification->setTitle($title);
        $notification->setDescription($description);
        $notification->setStatusId($statusId);
        $notification->setIconId($iconId);
        $notification->setLinkTypeId($linkTypeId);
        $notification->setLinkContent($linkContent);
        $notification->setUser($user);
        $this->em->persist($notification);
        $this->em->flush();

        // отправка уведомления в Firebase
        $this->firebasePushHelper->sendNotificationToFirebase($notification, $user);
    }
}

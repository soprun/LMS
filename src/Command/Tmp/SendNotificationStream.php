<?php

namespace App\Command\Tmp;

use App\Entity\Courses\Course;
use App\Entity\GroupPermission;
use App\Entity\Notification;
use App\Entity\User;
use App\Service\Course\FirebasePushHelper;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendNotificationStream extends Command
{

    protected static $defaultName = 'tmp:send-notification';

    private $em;
    private $firebasePushHelper;
    private $logger;

    public function __construct(
        EntityManagerInterface $em,
        FirebasePushHelper $firebasePushHelper,
        LoggerInterface $logger
    ) {
        parent::__construct();
        $this->em = $em;
        $this->firebasePushHelper = $firebasePushHelper;
        $this->logger = $logger;
    }

    protected function configure()
    {
        $this->setDescription('Отправка уведомлений потоку')
            ->addArgument('day', InputArgument::REQUIRED); // номер недели с 0 (смотреть таблицу)
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $day = $input->getArgument('day');

        $data = [
            [
                'title' => 'Напоминание о ДЗ от Аяза',
                'desc' => 'Твое домашнее задание уже в новом уроке. Делай его и получай баллы,
                на которые ты сможешь выбрать себе бонус с витрины!',
            ],
            [
                'title' => 'ДДЛ по домашке к занятию с Аязом',
                'desc' => 'В субботу закончится прием домашнего задания по Занятию 1 с Аязом. Задание ты найдешь под
                видео с занятия'
            ],
            [
                'title' => 'Напоминание о ДЗ от Тренера',
                'desc' => 'Твое домашнее задание от Тренера уже в разделе «встреча с Тренером». Делай его и получай
                баллы, на которые ты сможешь выбрать себе бонус с витрины!',
            ],
            [
                'title' => 'Напоминание о ДЗ от Тренера',
                'desc' => 'Пора сдавать домашнее задание, чтобы получить баллы. Переходи в раздел «Встреча с Тренером» и
                заполняй форму отчета по ДЗ',
            ],
            [
                'title' => 'Отчет по прибыли',
                'desc' => 'Поделись результатами на курсе. Нам нужны данные о том, сколько ты заработал(-а) за время
                курса на сегоднящний день. Это принесет тебе дополнительный баллы. Заполнение отчета по прибыли
                обязательно. Переходи в раздел «Контроль прогресса»'
            ],
            [
                'title' => 'Напоминание о ДЗ от Тренера',
                'desc' => 'Последняя возможность сдать домашнее задание и получить баллы. Переходи в папку «Встреча с
                тренером на первой неделе» Следующая неделя НЕ откроется, пока ты не отправишь свое домашнее задание'
            ],
        ];

        $links = [
            [6400, 7349, 7334, 7358, 7359, 7360, 7364, 7365],
            [6400, 7349, 7334, 7358, 7359, 7360, 7364, 7365],
            [6383, 6232, 6215, 6249, 6266, 7499, 7500, 7501],
            [6383, 6232, 6215, 6249, 6266, 7499, 7500, 7501],
            [
                'https://lms.toolbox.bz/rating/hundred/user/',
                'https://lms.toolbox.bz/rating/hundred/user/',
                'https://lms.toolbox.bz/rating/hundred/user/',
                'https://lms.toolbox.bz/rating/hundred/user/',
                'https://lms.toolbox.bz/rating/hundred/user/',
                'https://lms.toolbox.bz/rating/hundred/user/',
                'https://lms.toolbox.bz/rating/hundred/user/',
                'https://lms.toolbox.bz/rating/hundred/user/'
            ],
            [6383, 6232, 6215, 6249, 6266, 7499, 7500, 7501],
        ];

        /** @var Course[] $courses */
        $courses = $this->em->getRepository(Course::class)->createQueryBuilder('c')
            ->innerJoin('c.folder', 'f')
            ->andWhere('f.id = :folder')
            ->setParameter('folder', 108)
            ->andWhere('c.name LIKE :name')
            ->setParameter(':name', 'СБОРНАЯ %')
            ->getQuery()
            ->getResult();

        $userIds = [];

        foreach ($courses as $course) {
            switch ($course->getId()) {
                case 790:
                    $url = $links[$day][0];
                    break;

                case 792:
                    $url = $links[$day][1];
                    break;

                case 791:
                    $url = $links[$day][2];
                    break;

                case 793:
                    $url = $links[$day][3];
                    break;

                case 794:
                    $url = $links[$day][4];
                    break;

                case 931:
                    $url = $links[$day][5];
                    break;

                case 932:
                    $url = $links[$day][4];
                    break;

                case 933:
                    $url = $links[$day][5];
                    break;
            }

            foreach ($course->getUserGroups() as $userGroup) {
                foreach ($userGroup->getUserRelations() as $userRelation) {
                    if (!$userRelation->isDeleted()) {
                        $user = $userRelation->getUser();
                        $isUserAdmin = $this->em->getRepository(GroupPermission::class)->findByUserIsAdmin($user);

                        $finded = array_search($user->getId(), $userIds);

                        if (!$isUserAdmin && !$finded) {
                            $this->createNotification(
                                $data[$day]['title'],
                                $data[$day]['desc'],
                                Notification::ICON_DEFAULT,
                                $user,
                                ($day != 4) ? Notification::LINK_TYPE_LESSON : Notification::LINK_TYPE_SIMPLE_LINK,
                                $url
                            );
                            $userIds[] = $user->getId();
                        }
                    }
                }
            }
        }

        $this->logger->info(
            sprintf(
                "SendNotification: Оповестили %s человек",
                count($userIds),
            )
        );

        return 0;
    }

    private function createNotification(
        $title,
        $description,
        $iconId,
        User $user,
        $linkTypeId = Notification::LINK_TYPE_NO_LINK,
        $linkContent = null,
        $statusId = Notification::STATUS_NEW
    ) {
        if (!$linkContent) {
            $linkTypeId = Notification::LINK_TYPE_NO_LINK;
        }

        $notification = new Notification();
        $notification->setTitle($title);
        $notification->setDescription($description);
        $notification->setStatusId($statusId);
        $notification->setIconId($iconId);
        $notification->setLinkTypeId($linkTypeId);
        $notification->setLinkContent($linkContent);
        $notification->setUser($user);
        $this->em->persist($notification);
        $this->em->flush();

        // отправка уведомления в Firebase
        $this->firebasePushHelper->sendNotificationToFirebase($notification, $user);
    }
}

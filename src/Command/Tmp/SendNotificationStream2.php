<?php

namespace App\Command\Tmp;

use App\Entity\Courses\Course;
use App\Entity\GroupPermission;
use App\Entity\Notification;
use App\Entity\User;
use App\Service\Course\FirebasePushHelper;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendNotificationStream2 extends Command
{

    protected static $defaultName = 'tmp:send-notification2';

    private $em;
    private $firebasePushHelper;
    private $logger;

    public function __construct(
        EntityManagerInterface $em,
        FirebasePushHelper $firebasePushHelper,
        LoggerInterface $logger
    ) {
        parent::__construct();
        $this->em = $em;
        $this->firebasePushHelper = $firebasePushHelper;
        $this->logger = $logger;
    }

    protected function configure()
    {
        $this->setDescription('Отправка уведомлений потоку')
            ->addArgument('number', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $number = $input->getArgument('number');

        $data = [
            [
                //19.08.2021 в 12.00
                'courseId' => 1056,
                'title' => 'Мышление и энергия',
                'desc' => 'В уроке вы узнаете, что мешает вам стать богатым и как это исправить! А также задание, '
                    . 'которое поможет вам изменить свою жизнь.',
                'lessonId' => 8482
            ],
            [
                //19.08.2021 в 18.00
                'courseId' => 1056,
                'title' => 'Напоминание о занятии с главой факультета.',
                'desc' => 'Чтобы получить максимум от обучения на Скорость.Клуб, заходи на платформу в папку своего '
                    . 'факультета и смотри встречу с главой твоего факультета!',
                'lessonId' => null,
                'uri' => 'https://lms.toolbox.bz/courses/folder/130',
            ],
            [
                //20.08.2021 в 12.00
                'courseId' => 1056,
                'title' => 'Законы реальной жизни',
                'desc' => 'В уроке вы познакомитесь с 3 новыми законами реальной жизни и выполните задание на их '
                    . 'проработку!',
                'lessonId' => 8483,
                'uri' => null,
            ],
            [
                //20.08.2021 в 18.00
                'courseId' => 1056,
                'title' => 'Напоминание о сдачи ДЗ',
                'desc' => '"Последняя возможность сдать домашнее задание в этом модуле, чтобы получить обратную связь '
                    . 'от кураторов!',
                'lessonId' => null,
                'uri' => 'https://lms.toolbox.bz/courses/course/1056',
            ],
            [
                //21.08.2021 в 12.00
                'courseId' => 1057,
                'title' => 'Как увеличить трафик руками гениального таргетолога - подряд',
                'desc' => 'В этом уроке вы узнаете, что нужно для того, чтобы получать больше заявок.  Для '
                    . 'закрепления материала обязательно выполняйте домашнее задание',
                'lessonId' => 8484,
                'uri' => '',
            ],
            [
                //22.08.2021 в 12.00
                'courseId' => 1057,
                'title' => 'Таргетолог в найм',
                'desc' => 'В этом уроке вы узнаете, как нанять настоящего профессионала в вашу команду, как '
                    . 'провести стажировку, оценить работу и мотивировать таргетолога. И выполните задание '
                    . 'направленное на поиск таргетолога.',
                'lessonId' => 8485,
                'uri' => '',
            ],
            [
                //23.08.2021 в 12.00
                'courseId' => 1057,
                'title' => 'Гениальный менеджер по продажам',
                'desc' => 'В уроке вы узнаете, как нанять менеджера по продажам, который принесет вашей компании '
                    . 'кратный рост прибыли. Сразу после просмотра видеоурока выполняй домашнее задание!',
                'lessonId' => 8486,
                'uri' => '',
            ],
            [
                //24.08.2021 в 12.00
                'courseId' => 1057,
                'title' => '5 лайфхаков, как поднять продажи',
                'desc' => 'В уроке ва узнаете, как увеличить продажи и свою прибыль минимум в 2 раза.  Домашнее '
                    . 'задание находится под видеоуроками!',
                'lessonId' => 8487,
                'uri' => '',
            ],
            [
                //25.08.2021 в 12.00
                'courseId' => 1057,
                'title' => 'Мышление',
                'desc' => 'В этом уроке вас ждет одно из самых главных заданий, которое поможет вам сделать прорыв '
                    . 'в оставшиеся 2 недели интенсива! Обязательно выполняй его.',
                'lessonId' => 8488,
                'uri' => '',
            ],
            [
                //26.08.2021 в 12.00
                'courseId' => 1057,
                'title' => 'Медитация',
                'desc' => 'Прослушайте медитацию и настройтесь на дальнейшую работу!',
                'lessonId' => 8489,
                'uri' => '',
            ],
            [
                //26.08.2021 в 18.00
                'courseId' => 1057,
                'title' => 'Напоминание о занятии с главой факультета.',
                'desc' => 'Чтобы получить максимум от обучения на Скорость.Клуб, заходи на платформу в папку '
                    . 'своего факультета и смотри встречу с главой твоего факультета! ',
                'lessonId' => null,
                'uri' => 'https://lms.toolbox.bz/courses/folder/130 ',
            ],
            [
                //27.08.2021 в 12.00
                'courseId' => 1057,
                'title' => 'Законы реальной жизни ',
                'desc' => 'В уроке вы познакомитесь с 3 новыми законами реальной жизни и выполните задание на их '
                    . 'проработку!',
                'lessonId' => 8490,
                'uri' => '',
            ],
            [
                //27.08.2021 в 18.00
                'courseId' => 1057,
                'title' => 'Напоминание о сдачи ДЗ',
                'desc' => '"Последняя возможность сдать домашнее задание в этом модуле, чтобы получить обратную '
                    . 'связь от кураторов!',
                'lessonId' => null,
                'uri' => 'https://lms.toolbox.bz/courses/course/1057',
            ],
            [
                //13
                //28.08.2021 в 12.00
                'courseId' => 1058,
                'title' => 'Теория «узких горлышек» в бизнесе: карта бизнес-процессов',
                'desc' => 'В уроке вы узнаете, что такое "теория узких горлышек" в бизнесе и узнаете, как описать '
                    . 'один из важнейших бизнес-процессов в бизнесе!',
                'lessonId' => 8491,
                'uri' => '',
            ],
            [
                //29.08.2021 в 12.00
                'courseId' => 1058,
                'title' => 'Как найти «узкое горлышко» в бизнесе',
                'desc' => 'В уроке вы узнаете, как находить и расширять узкие места по карте бизнес-процессов в '
                    . 'вашем деле. Сразу после просмотра видеоуроков выполняй домашнее задание!',
                'lessonId' => 8492,
                'uri' => '',
            ],
            [
                //30.08.2021 в 12.00
                'courseId' => 1058,
                'title' => 'Найм проджект-менеджера и управленцев',
                'desc' => 'В уроке вы узнаете, как нанять проджект-менеджера в свою компанию: где искать, как '
                    . 'провести интервью и организовать стажеровку. Выполняйте задание направленное на поиск '
                    . 'проджект-менеджера!',
                'lessonId' => 8493,
                'uri' => '',
            ],
            [
                //31.08.2021 в 12.00
                'courseId' => 1058,
                'title' => 'Инструменты продаж: как увеличить продажи и делать это системно',
                'desc' => 'В уроке вы узнаете, как повысить продажи, сделать их системными и как мотивировать '
                    . 'менеджеров продавать еще лучше! Для закрепления материала обязательно выполняйте домашнее '
                    . 'задание',
                'lessonId' => 8495,
                'uri' => '',
            ],
            [
                //01.09.2021в 12.00
                'courseId' => 1058,
                'title' => 'Мышление. Техника «Быть самому себе другом» ',
                'desc' => 'В уроке вы узнаете, как стать другом самому себе и как создать собственную стратегию по '
                    . 'достижению любой цели!',
                'lessonId' => 8496,
                'uri' => '',
            ],
            [
                //02.09.2021 в 12.00
                'courseId' => 1058,
                'title' => 'Медитация',
                'desc' => 'Прослушайте медитацию и настройтесь на дальнейшую работу!',
                'lessonId' => 8497,
                'uri' => '',
            ],
            [
                //02.09.2021 в 18.00
                'courseId' => 1058,
                'title' => 'Напоминание о занятии с главой факультета.',
                'desc' => 'Чтобы получить максимум от обучения на Скорость.Клуб, заходи на платформу в папку '
                    . 'своего факультета и смотри встречу с главой твоего факультета!',
                'lessonId' => null,
                'uri' => 'https://lms.toolbox.bz/courses/folder/130',
            ],
            [
                //03.09.2021 в 12.00
                'courseId' => 1058,
                'title' => 'Законы реальной жизни',
                'desc' => 'В уроке вы познакомитесь с 3 новыми законами реальной жизни и выполните задание на '
                    . 'их проработку!',
                'lessonId' => 8498,
                'uri' => '',
            ],
            [
                //03.09.2021 в 18.00
                'courseId' => 1058,
                'title' => 'Напоминание о сдачи ДЗ',
                'desc' => '"Последняя возможность сдать домашнее задание в этом модуле, чтобы получить обратную '
                    . 'связь от кураторов!',
                'lessonId' => null,
                'uri' => 'https://lms.toolbox.bz/courses/course/1058',
            ],
            [
                //04.09.2021 в 12.00
                'courseId' => 1059,
                'title' => 'Продолжаем искать «узкие горлышки» и способы их расширения',
                'desc' => 'В уроке вы узнаете, как предусматреть появление "узкого горлышка" в вашем бизнесе. Для '
                    . 'закрепления материала обязательно выполняйте домашнее задание',
                'lessonId' => 8499,
                'uri' => '',
            ],
            [
                //05.09.2021 в 12.00
                'courseId' => 1059,
                'title' => 'Рост в 10 раз',
                'desc' => 'В этом уроке вас ждет задание, которое поможет вам понять, как заработать в 10 раз больше!',
                'lessonId' => 8500,
                'uri' => '',
            ],
            [
                //06.09.2021 в 12.00
                'courseId' => 1059,
                'title' => 'Система сбалансированных показателей',
                'desc' => 'В этом уроке вы узнаете, зачем вашему бизнесу система сбалансированных показателей и '
                    . 'выполните задание, чтобы вырасти в прибыли х10',
                'lessonId' => 8501,
                'uri' => '',
            ],
            [
                //07.09.2021 в 12.00
                'courseId' => 1059,
                'title' => 'Медитация',
                'desc' => 'Прослушайте медитацию и настройтесь на дальнейшую работу!',
                'lessonId' => 8502,
                'uri' => '',
            ],
            [
                //08.09.2021 в 12.00
                'courseId' => 1059,
                'title' => 'Мышление',
                'desc' => 'В уроке вы узнаете, что может мешать вам достигать больших целей и выполните задание '
                    . 'на преодоление этого!',
                'lessonId' => 8503,
                'uri' => '',
            ],
            [
                //09.09.2021 в 12.00
                'courseId' => 1059,
                'title' => 'Регулярный менеджмент',
                'desc' => 'В уроке вы узнаете, что позволяет бизнесу вырасти в 300 и более раз.  Домашнее задание '
                    . 'находится под видеоуроками!',
                'lessonId' => 8504,
                'uri' => '',
            ],
            [
                //09.09.2021 в 18.00
                'courseId' => 1059,
                'title' => 'Напоминание о занятии с главой факультета.',
                'desc' => 'Чтобы получить максимум от обучения на Скорость.Клуб, заходи на платформу в папку '
                    . 'своего факультета и смотри встречу с главой твоего факультета!',
                'lessonId' => null,
                'uri' => 'https://lms.toolbox.bz/courses/folder/130',
            ],
            [
                //10.09.2021 в 12.00
                'courseId' => 1059,
                'title' => 'Законы реальной жизни',
                'desc' => 'В уроке вы познакомитесь с 3 новыми законами реальной жизни и выполните задание на '
                    . 'их проработку!',
                'lessonId' => 8505,
                'uri' => '',
            ],
            [
                //10.09. 2021 в 18.00
                'courseId' => 1059,
                'title' => 'Напоминание о сдачи ДЗ',
                'desc' => '"Последняя возможность сдать домашнее задание в этом модуле, чтобы получить обратную '
                    . 'связь от кураторов!',
                'lessonId' => null,
                'uri' => 'https://lms.toolbox.bz/courses/course/1059',
            ],
        ];

        $push = $data[$number];
        /** @var Course $course */
        $course = $this->em->getRepository(Course::class)->find($push['courseId']);

        $userIds = [];

        foreach ($course->getUserGroups() as $userGroup) {
            foreach ($userGroup->getUserRelations() as $userRelation) {
                if (!$userRelation->isDeleted()) {
                    /** @var User $user */
                    $user = $userRelation->getUser();
                    $isUserAdmin = $this->em->getRepository(GroupPermission::class)->findByUserIsAdmin($user);

                    $finded = array_search($user->getId(), $userIds);

                    if (!$isUserAdmin && !$finded) {
                        $this->createNotification(
                            $push['title'],
                            $push['desc'],
                            Notification::ICON_DEFAULT,
                            $user,
                            $push['lessonId'] ? Notification::LINK_TYPE_LESSON : Notification::LINK_TYPE_SIMPLE_LINK,
                            $push['lessonId'] ?: $push['uri']
                        );
                        $userIds[] = $user->getId();
                    }
                }
            }
        }

        $this->logger->info(
            sprintf(
                "SendNotification: Оповестили %s человек",
                count($userIds),
            )
        );

        return 0;
    }

    private function createNotification(
        $title,
        $description,
        $iconId,
        User $user,
        $linkTypeId = Notification::LINK_TYPE_NO_LINK,
        $linkContent = null,
        $statusId = Notification::STATUS_NEW
    ) {
        if (!$linkContent) {
            $linkTypeId = Notification::LINK_TYPE_NO_LINK;
        }

        $notification = new Notification();
        $notification->setTitle($title);
        $notification->setDescription($description);
        $notification->setStatusId($statusId);
        $notification->setIconId($iconId);
        $notification->setLinkTypeId($linkTypeId);
        $notification->setLinkContent($linkContent);
        $notification->setUser($user);
        $this->em->persist($notification);
        $this->em->flush();

        try {
            // отправка уведомления в Firebase
            $this->firebasePushHelper->sendNotificationToFirebase($notification, $user);
        } catch (\Exception $e) {
            $this->logger->debug('Firebase fuck-up: ' . $e->getMessage());
        }
    }
}

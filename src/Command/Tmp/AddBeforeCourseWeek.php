<?php

namespace App\Command\Tmp;

use App\Entity\AbstractCourse;
use App\Entity\Analytics\ReportMSG;
use App\Entity\Analytics\ReportMSGCell;
use App\Entity\CourseStream;
use App\Entity\MarathonInvestment\Target;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AddBeforeCourseWeek extends Command
{

    protected static $defaultName = 'tmp:add-msa-before-course';

    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    protected function configure()
    {
        $this->setDescription('Добавить всем участникам МсА неделю до курса');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $reportsMsg = $this->em->getRepository(ReportMSG::class)->createQueryBuilder('r')
            ->innerJoin('r.cells', 'rcell')
            ->getQuery()
            ->getResult();

        foreach ($reportsMsg as $msg) {
            $cell = new ReportMSGCell();
            $cell->setWeekId(-1)
                ->setReportMSG($msg);

            $this->em->persist($cell);
        }
        $this->em->flush();
        $output->writeln('Неделя добавлена!');

        return 0;
    }
}

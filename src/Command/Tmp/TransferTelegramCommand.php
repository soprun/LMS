<?php

namespace App\Command\Tmp;

use App\Entity\SmartSender\SmartSenderUser;
use App\Service\AuthServerHelper;
use App\Service\FormHelper;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TransferTelegramCommand extends Command
{

    protected static $defaultName = 'tmp:transfer-telegram';

    private $em;
    private $formHelper;
    private $authServerHelper;
    private $logger;

    public function __construct(
        EntityManagerInterface $em,
        FormHelper $formHelper,
        AuthServerHelper $authServerHelper,
        LoggerInterface $logger
    ) {
        parent::__construct();
        $this->em = $em;
        $this->formHelper = $formHelper;
        $this->authServerHelper = $authServerHelper;
        $this->logger = $logger;
    }

    protected function configure()
    {
        $this->setDescription('Перенос телеграма на аутх');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var SmartSenderUser[] $smartSenderUsers */
        $smartSenderUsers = $this->em->getRepository(SmartSenderUser::class)->createQueryBuilder('smart')
            ->select('u.id, u.authUserId, smart.username as telegram')
            ->innerJoin('smart.user', 'u')
            ->getQuery()
            ->getResult();

        $temp = $users = [];
        foreach ($smartSenderUsers as $key => $value) {
            $finded = array_search($value['id'], $temp);

            if (!$finded) {
                $temp[] = $value['id'];
                $users[] = [
                    'authUserId' => $value['authUserId'],
                    'telegram' => $value['telegram'],
                    'courseName' => 'withoutEmail'
                ];
            } else {
                $users[$finded]['telegram'] .= "|" . $value['telegram'];
            }
        }

        $cnt = count($users);
        $it = 0;

        foreach ($users as $user) {
            [$authUser, $errors] = $this->authServerHelper->editUserByServer($user);

            if (!empty($errors)) {
                $this->logger->critical("Ошибка для authId: " . $user['authUserId'], $errors);
            } else {
                $it++;
                $output->writeln('Успешно перенесено: ' . "$it/$cnt");
            }
        }
        return 1;
    }
}

<?php

namespace App\Command\Tmp;

use App\Entity\Faculty;
use App\Entity\TeamFolder;
use App\Entity\UserGroup;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateFacultiesCommand extends Command
{

    protected static $defaultName = 'tmp:create-faculties';

    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    protected function configure()
    {
        $this->setDescription('Создание факультетов и сборных для Скорости 19');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // @todo: важно передавать courseStream в UserGroup, иначе удаление из группы факультета не сработает

        $faculties = [
///            'Название папки' => [
///                [
///                    'groupName' => 'Название группы',
///                    'name' => 'Название факультета',
///                    'redirectUrl' => null,
///                ],
///            ],
/// ([а-я \w,.()–-]+)\s+(С[А-яа-я, "\w().-]+)\s+(https://[\w./]+)
///                 [
//                    'groupName' => '$2',
//                    'name' => '$1',
//                    'redirectUrl' => '$3',
//                ],
            'Скорость Клуб 5' => [
                [
                    'groupName' => 'СК-5, Факультет "Детского развития"',
                    'name' => 'Детского развития',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2230',
                ],
                [
                    'groupName' => 'СК-5, Факультет "Косметологических услуг"',
                    'name' => 'Косметологических услуг',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2231',
                ],
                [
                    'groupName' => 'СК-5, Факультет "Построения команд"',
                    'name' => 'Построения команд',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2232',
                ],
                [
                    'groupName' => 'СК-5, Факультет "Бизнес-образования"',
                    'name' => 'Бизнес-образования',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2233',
                ],
                [
                    'groupName' => 'СК-5, Факультет "Продюсирования экспертов"',
                    'name' => 'Продюсирования экспертов',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2234',
                ],
                [
                    'groupName' => 'СК-5, Факультет "Предоставления сотрудников на аутсорсе"',
                    'name' => 'Предоставления сотрудников на аутсорсе',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2235',
                ],
                [
                    'groupName' => 'СК-5, Факультет "Оцифровки бизнеса"',
                    'name' => 'Оцифровки бизнеса',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2236',
                ],
                [
                    'groupName' => 'СК-5, Факультет "Розничной торговли "',
                    'name' => 'Розничной торговли ',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2237',
                ],
                [
                    'groupName' => 'СК-5, Факультет "2. Розничной торговли "',
                    'name' => '2. Розничной торговли ',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2330',
                ],
                [
                    'groupName' => 'СК-5, Факультет "Развития кофеен и ресторанов"',
                    'name' => 'Развития кофеен и ресторанов',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2238',
                ],
                [
                    'groupName' => 'СК-5, Факультет "Продвижения бизнеса"',
                    'name' => 'Продвижения бизнеса',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2239',
                ],
                [
                    'groupName' => 'СК-5, Факультет "Производства еды"',
                    'name' => 'Производства еды',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2240',
                ],
                [
                    'groupName' => 'СК-5, Факультет "Продаж"',
                    'name' => 'Продаж',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2241',
                ],
                [
                    'groupName' => 'СК-5, Факультет "Продажи товаров на Wildberries"',
                    'name' => 'Продажи товаров на Wildberries',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2242',
                ],
                [
                    'groupName' => 'СК-5, Факультет "Медицинских услуг"',
                    'name' => 'Медицинских услуг',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2243',
                ],
                [
                    'groupName' => 'СК-5, Факультет "Ремонта и отделки помещений"',
                    'name' => 'Ремонта и отделки помещений',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2244',
                ],
                [
                    'groupName' => 'СК-5, Факультет "Доставка продуктов и Street Food-а"',
                    'name' => 'Доставка продуктов и Street Food-а',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2245',
                ],
                [
                    'groupName' => 'СК-5, Факультет "Спорт и групповые тренировки "',
                    'name' => 'Спорт и групповые тренировки ',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2331',
                ],
                [
                    'groupName' => 'СК-5, Факультет "Спорта, зож и открытия студий"',
                    'name' => 'Спорта, зож и открытия студий',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2246',
                ],
                [
                    'groupName' => 'СК-5, Факультет "Автоуслуг"',
                    'name' => 'Автоуслуг',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2247',
                ],
                [
                    'groupName' => 'СК-5, Факультет "Доставка еды"',
                    'name' => 'Доставка еды',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2332',
                ],
                [
                    'groupName' => 'СК-5, Факультет "Парикмахерских"',
                    'name' => 'Парикмахерских',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2248',
                ],
                [
                    'groupName' => 'СК-5, Факультет "Производства мебели"',
                    'name' => 'Производства мебели',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2249',
                ],
                [
                    'groupName' => 'СК-5, Факультет "Онлайн Курсов мышления и саморазвития"',
                    'name' => 'Онлайн Курсов мышления и саморазвития',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2250',
                ],
                [
                    'groupName' => 'СК-5, Факультет "Маркетинг"',
                    'name' => 'Маркетинг',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2251'
                ],
                [
                    'groupName' => 'СК-5, Факультет "Офлайн образования"',
                    'name' => 'Офлайн образования',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2252'
                ],
                [
                    'groupName' => 'СК-5, Факультет "Открытия музыкальных школ"',
                    'name' => 'Открытия музыкальных школ',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2333'
                ],
                [
                    'groupName' => 'СК-5, Факультет "Зарубежных маркетплейсов"',
                    'name' => 'Зарубежных маркетплейсов',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2253'
                ],
                [
                    'groupName' => 'СК-5, Факультет "Продаж консультаций и услуг специалистов"',
                    'name' => 'Продаж консультаций и услуг специалистов',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2254'
                ],
                [
                    'groupName' => 'СК-5, Факультет "Развития офлайн-бизнеса"',
                    'name' => 'Развития офлайн-бизнеса',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2255'
                ],
                [
                    'groupName' => 'СК-5, Факультет "Бьюти"',
                    'name' => 'Бьюти ',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2334'
                ],
                [
                    'groupName' => 'СК-5, Факультет "Центров дополнительного образования"',
                    'name' => 'Центров дополнительного образования',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2335'
                ],
                [
                    'groupName' => 'СК-5, Факультет "Продажи одежды и аксессуаров в Instagram"',
                    'name' => 'Продажи одежды и аксессуаров в Instagram',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2256'
                ],
                [
                    'groupName' => 'СК-5, Факультет "Производства одежды"',
                    'name' => 'Производства одежды',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2336'
                ],
                [
                    'groupName' => 'СК-5, Факультет "Франшизы"',
                    'name' => 'Франшизы',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2337'
                ],
                [
                    'groupName' => 'СК-5, Факультет "Посуточной аренды квартир"',
                    'name' => 'Посуточной аренды квартир ',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2338'
                ],
                [
                    'groupName' => 'СК-5, Факультет "Сборная продаж на Ozon"',
                    'name' => 'Сборная продаж на Ozon ',
                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2339'
                ],
            ],
//            'Сотка 8' => [
//                [
//                    'groupName'   => 'С-8, Сборная "Продюсирования экспертов"',
//                    'name'        => 'Продюсирования экспертов',
//                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2309'
//                ],
//                [
//                    'groupName'   => 'С-8, Сборная "Онлайн-курсов"',
//                    'name'        => 'Онлайн-курсов',
//                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2310'
//                ],
//                [
//                    'groupName'   => 'С-8, Сборная "Розничной продажи товаров"',
//                    'name'        => 'Розничной продажи товаров',
//                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2311'
//                ],
//                [
//                    'groupName'   => 'С-8, Сборная "Сборная продаж на Ozon"',
//                    'name'        => 'Сборная продаж на Ozon ',
//                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2312'
//                ],
//                [
//                    'groupName'   => 'С-8, Сборная "С 0 до 1 000 000 на Wildberries"',
//                    'name'        => 'С 0 до 1 000 000 на Wildberries',
//                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2313'
//                ],
//                [
//                    'groupName'   => 'С-8, Сборная "Продаж консультаций"',
//                    'name'        => 'Продаж консультаций',
//                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2314'
//                ],
//                [
//                    'groupName'   => 'С-8, Сборная "Онлайн-коучинга"',
//                    'name'        => 'Онлайн-коучинга',
//                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2315'
//                ],
//                [
//                    'groupName'   => 'С-8, Сборная "Продаж услуг для бизнеса"',
//                    'name'        => 'Продаж услуг для бизнеса',
//                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2316'
//                ],
//                [
//                    'groupName'   => 'С-8, Сборная "Офлайн-бизнеса с минимальными вложениями"',
//                    'name'        => 'Офлайн-бизнеса с минимальными вложениями',
//                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2317'
//                ],
//                [
//                    'groupName'   => 'С-8, Сборная "Бьюти"',
//                    'name'        => 'Бьюти',
//                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2318'
//                ],
//                [
//                    'groupName'   => 'С-8, Сборная "Продаж услуг"',
//                    'name'        => 'Продаж услуг ',
//                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2319'
//                ],
//                [
//                    'groupName'   => 'С-8, Сборная "Создание клубов, сообществ"',
//                    'name'        => 'Создание клубов и сообществ',
//                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2320'
//                ],
//                [
//                    'groupName'   => 'С-8, Сборная "Выбора и продажи товаров на Wildberries"',
//                    'name'        => 'Выбора и продажи товаров на Wildberries',
//                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2322'
//                ],
//                [
//                    'groupName'   => 'С-8, Сборная "2-ая Продаж консультаций
//                (мини-группа)"',
//                    'name'        => '2-ая Продаж консультаций',
//                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2323'
//                ],
//                [
//                    'groupName'   => 'С-8, Сборная "3-я Продаж консультаций
//                (мини-группа)"',
//                    'name'        => '3-я Продаж консультаций',
//                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2324'
//                ],
//                [
//                    'groupName'   => 'С-8, Сборная "2-я Офлайн-бизнеса с минимальными вложениями',
//                    'name'        => '2-я Офлайн-бизнеса с минимальными вложениями',
//                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2321'
//                ],
//                [
//                    'groupName'   => 'С-8, Сборная "2-я Бьюти"',
//                    'name'        => '2-я Бьюти',
//                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2325'
//                ],
//                [
//                    'groupName'   => 'С-8, Сборная "Юр услуги"',
//                    'name'        => 'Юр услуги',
//                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2326'
//                ],
//                [
//                    'groupName'   => 'С-8, Сборная "Маркетинг"',
//                    'name'        => 'Маркетинг',
//                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2327'
//                ],
//                [
//                    'groupName'   => 'С-8, Сборная "Маркетплейсов (страны СНГ)"',
//                    'name'        => 'Маркетплейсов (страны СНГ)',
//                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2328'
//                ],
//                [
//                    'groupName'   => 'С-8, Сборная "Тик-ток продюсирования"',
//                    'name'        => 'Тик-ток продюсирования',
//                    'redirectUrl' => 'https://lms.toolbox.bz/courses/course/2329'
//                ],
//            ],
        ];

        foreach ($faculties as $folderName => $f) {
            /** @var TeamFolder $teamFolder */
            $teamFolder = $this->em->getRepository(TeamFolder::class)->findOneBy(['name' => $folderName]);

            foreach ($f as $faculty) {
                $facultyEntity = $this->em->getRepository(Faculty::class)->findOneBy(
                    [
                        'teamFolder' => $teamFolder,
                        'name' => $faculty['name'],
                    ]
                );

                if (!$facultyEntity) {
                    $facultyEntity = (new Faculty())
                        ->setName($faculty['name'])
                        ->setTeamFolder($teamFolder);
                }

                /** @var UserGroup $group */
                $group = $this->em->getRepository(UserGroup::class)->findOneBy(['name' => $faculty['groupName']]);
                if (!$group) {
                    dump($faculty['groupName']);
                }
                $facultyEntity
                    ->setRedirectUrl($faculty['redirectUrl'])
                    ->setUserGroup($group);
                $this->em->persist($facultyEntity);
            }
        }
        $this->em->flush();

        return 0;
    }

}

<?php

namespace App\Command\Tmp;

use App\Entity\User;
use App\Entity\UserGroup;
use App\Entity\UserGroupUser;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FindWrongUserGroupUsersCommand extends Command
{

    protected static $defaultName = 'tmp:find-wrong-user-group-users';

    private $em;

    public function __construct(
        EntityManagerInterface $em
    ) {
        parent::__construct();
        $this->em = $em;
    }

    protected function configure()
    {
        $this->setDescription('Поиск и удаление пересечений групп пользователей');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // d.smirnov@likebz.ru
        $addedBy = $this->em->getRepository(User::class)->find(User::SERVICE_USER);

        $connection = $this->em->getConnection();

        // находим юзеров с 1+ группами
        $sql = <<<'SQL'
            SELECT user_id
            FROM (
                SELECT ugu.user_id, COUNT(ug.id) AS count
                FROM user_group_user ugu
                INNER JOIN user_group ug ON ugu.user_group_id = ug.id
                WHERE ugu.deleted = 0
                AND (ug.name LIKE 'Скорость 17%' OR ug.name LIKE 'Сотка 2%')
                AND ug.name NOT LIKE '%предоплат%'
                AND ug.name NOT LIKE '%тест-драйв%'
                AND ug.name NOT LIKE '%минимальный%'
                GROUP BY ugu.user_id
            ) s
            WHERE count > 1
            SQL;

        $rows = $connection->executeQuery($sql)->fetchAllAssociative();
        $output->writeln('Всего: ' . count($rows));

        foreach ($rows as $row) {
            $userId = $row['user_id'];

            /** @var $qb QueryBuilder */
            $qb = $this->em->getRepository(UserGroupUser::class)->createQueryBuilder('ugu');
            /** @var UserGroupUser[] $userGroupUsers */
            $userGroupUsers = $qb
                ->innerJoin('ugu.user', 'u')
                ->innerJoin('ugu.userGroup', 'ug')
                ->andWhere('u.id = :userId')
                ->andWhere('ugu.deleted = 0')
                ->andWhere("ug.name LIKE 'С-16+Сотка%' OR ug.name LIKE 'Скорость 17%' OR ug.name LIKE 'Сотка 2%'")
                ->andWhere("ug.name NOT LIKE '%предоплат%'")
                ->andWhere("ug.name NOT LIKE '%тест-драйв%'")
                ->setParameters(
                    [
                        'userId' => $userId,
                    ]
                )
                ->getQuery()
                ->getResult();

            $output->writeln('userId ' . $userId);

            dump(
                array_map(
                    function (UserGroupUser $userGroupUser) {
                        return $userGroupUser->getUserGroup()->getName();
                    },
                    $userGroupUsers
                )
            );

            $user = $userGroupUsers[0]->getUser();

            // определяем единственную группу, где может быть повторник
            $necessaryGroup = null;
            foreach ($userGroupUsers as $userGroupUser) {
                switch ($userGroupUser->getUserGroup()->getName()) {
                    case 'С-16+Сотка с Аязом с бизнесом':
                        $necessaryGroup = 'Сотка 2 (Повторники)';
                        break 2;

                    case 'С-16+Сотка с Аязом с нуля':
                        $necessaryGroup = 'Скорость 17 (Повторники)';
                        break 2;
                }
            }

            // ситуация, когда не был на предыдущем потоке
            if (!$necessaryGroup) {
                foreach ($userGroupUsers as $userGroupUser) {
                    $groupName = $userGroupUser->getUserGroup()->getName();
                    if (!str_contains($groupName, 'Повторники')) {
                        dump('прикол');
                        $necessaryGroup = $groupName;
                        break;
                    }
                }
            }

            if (!$necessaryGroup) {
                dd('все ок. как?');
                continue;
            }

            // удаляем из всех групп
            foreach ($userGroupUsers as $userGroupUser) {
                $groupName = $userGroupUser->getUserGroup()->getName();
                if ($groupName === $necessaryGroup || str_starts_with($groupName, "С-16+Сотка с Аязом")) {
                    continue;
                }
                dump('удалили из ' . $groupName);
                $userGroupUser->setDeleted(true);
                $this->em->persist($userGroupUser);
            }

            // ищем группу и связь
            /** @var UserGroup $userGroup */
            $userGroup = $this->em->getRepository(UserGroup::class)->findOneBy(
                [
                    'name' => $necessaryGroup,
                ]
            );
            $groupName = $userGroup->getName();
            /** @var null|UserGroupUser $userGroupUser */
            $userGroupUser = $this->em->getRepository(UserGroupUser::class)->findOneBy(
                [
                    'user' => $user,
                    'userGroup' => $userGroup,
                ]
            );

            // добавляем в группу
            if ($userGroupUser && $userGroupUser->isDeleted()) {
                dump('вернули в группу ' . $groupName);
                $userGroupUser->setDeleted(false);
            } else {
                $userGroupUser = (new UserGroupUser())
                    ->setUser($user)
                    ->setUserGroup($userGroup)
                    ->setAddedBy($addedBy);
                dump('добавили в группу ' . $groupName);
            }
            $this->em->persist($userGroupUser);

            $output->writeln('');
            $this->em->flush();
        }

        return 0;
    }

}

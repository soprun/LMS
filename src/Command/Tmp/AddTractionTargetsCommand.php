<?php

namespace App\Command\Tmp;

use App\Entity\AbstractCourse;
use App\Entity\CourseStream;
use App\Entity\MarathonInvestment\Target;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AddTractionTargetsCommand extends Command
{

    protected static $defaultName = 'tmp:add-targets';

    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    protected function configure()
    {
        $this->setDescription('Добавить список целей');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $data = [
            [
                'name' => 'Составил список из 50 контактов',
                'url' => null,
            ],
            [
                'name' => 'Написал тизер (короткий / длинный)',
                'url' => null,
            ],
            [
                'name' => 'Посчитал фин. модель',
                'url' => null,
            ],
            [
                'name' => 'Сделал презентацию',
                'url' => null,
            ],
            [
                'name' => 'Привлек Х р. инвестиций',
                'url' => null,
            ],
            [
                'name' => 'Зашел на МсА 🎉',
                'url' => null,
            ],
        ];

        $streams = $this->em->getRepository(CourseStream::class)->createQueryBuilder('cs')
            ->innerJoin('cs.abstractCourse', 'ac')
            ->where('ac.slug = :slug')
            ->setParameter('slug', AbstractCourse::SLUG_MARATHON_INVESTMENT)
            ->orderBy('cs.name')
            ->getQuery()
            ->getResult();

        foreach ($streams as $stream) {
            foreach ($data as $item) {
                $target = new Target();
                $target->setName($item['name'])
                    ->setStream($stream);
                $this->em->persist($target);
            }
        }

        $this->em->flush();

        $output->writeln('Список целей добавлен');

        return 0;
    }
}

<?php

namespace App\Command\Tmp;

use App\Entity\Analytics\ReportMSGCell;
use App\Service\AuthServerHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MsaZeroValue extends Command
{

    protected static $defaultName = 'tmp:msa-remove-zero-values';

    private $em;

    public function __construct(EntityManagerInterface $em, AuthServerHelper $authServerHelper)
    {
        parent::__construct();
        $this->em = $em;
        $this->authServerHelper = $authServerHelper;
    }

    protected function configure()
    {
        $this->setDescription('Удаление нулевых значений (МсА)');
    }


    /**
     * @param  InputInterface  $input
     * @param  OutputInterface  $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $progressBar = new ProgressBar($output);
        $progressBar->start();
        $output->writeln('');
        $progressBar->setFormat('debug');

        $qb = $this->em->getRepository(ReportMSGCell::class)->createQueryBuilder('cell');
        $qb
            ->select('cell')
            ->innerJoin('cell.reportMSG', 'rm')
            ->andWhere('rm.year = 2021')
            ->andWhere('cell.weekId > 6');

        $reportsMsa = $qb->getQuery()->getResult();
        $cnt = 0;

        foreach ($reportsMsa as $cell) {
            if ($cell->getProfit() === 0) {
                $cell->setProfit(null);
                $this->em->persist($cell);
            }
            $cnt++;

            if ($cnt % 100 === 0) {
                $this->em->flush();
                $progressBar->advance(100);
            }
        }
        $this->em->flush();

        $progressBar->finish();
        return 0;
    }
}

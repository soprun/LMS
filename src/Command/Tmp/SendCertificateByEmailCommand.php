<?php

namespace App\Command\Tmp;

use App\Entity\Courses\Course;
use App\Entity\Courses\Lesson;
use App\Entity\User;
use App\Entity\UserGroup;
use App\Service\AuthServerHelper;
use App\Service\Course\CertificateService;
use App\Service\Course\LessonHelper;
use App\Service\FormHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendCertificateByEmailCommand extends Command
{

    protected static $defaultName = 'tmp:send-certificate-user';

    private $em;
    private $formHelper;
    private $authServerHelper;
    private $certificateService;
    private $lessonHelper;

    public function __construct(
        EntityManagerInterface $em,
        FormHelper $formHelper,
        AuthServerHelper $authServerHelper,
        CertificateService $certificateService,
        LessonHelper $lessonHelper
    ) {
        parent::__construct();
        $this->em = $em;
        $this->formHelper = $formHelper;
        $this->authServerHelper = $authServerHelper;
        $this->certificateService = $certificateService;
        $this->lessonHelper = $lessonHelper;
    }

    protected function configure()
    {
        $this->setDescription('Отправка сертификата пользователю');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $data = [
            [
                'userId' => 77713,
//                'period' => 'с 02.01.2021 г. по 13.02.2021 г. прошла обучение',
                'courseId' => 508
            ],
        ];
        $count = 0;

        foreach ($data as $item) {
            /** @var  User $user */
            $user = $this->em->getRepository(User::class)->find($item['userId']);

            /** @var  Course $course */
            $course = $this->em->getRepository(Course::class)->find($item['courseId']);

            $courseName = $course->getName();

            if (mb_stripos($courseName, 'СКОРОСТЬ') !== false) {
                $allStopLessonsComplete = true;
                /** @var Lesson $lesson */
                foreach ($course->getLessons() as $key => $lesson) {
                    if ($lesson->getIsStopLesson() && !$lesson->getLessonConfiguration()->getIsHidden()) {
                        $lessonArray = $this->lessonHelper->getLessonArray($lesson, null, $user);
                        if (!$lessonArray['task']['isAllChecked']) {
                            $allStopLessonsComplete = false;
                            break;
                        }
                    }
                }

                if ($allStopLessonsComplete) {
                    $count++;

                    $certificateData = $this->certificateService->newCertificate($user, $course);
                    $certificateData = $this->certificateService->getCertificate($certificateData['serialNumber']);

                    $certificateData['dateString'] = $item['period'];

                    $image = $this->certificateService->createCertificateImg($certificateData);
                    $certificateData['image'] = json_encode(base64_encode($image->getimageblob()));
                    [$authUser, $errors] = $this->authServerHelper->sendCertificate($certificateData);

                    if (empty($errors)) {
                        $output->writeln('Успешно отправлено: ' . $authUser->lmsUserId);
                    } else {
                        $output->writeln('Ошибка отправления: ' . $authUser->lmsUserId);
                    }
                } else {
                    $output->writeln('Есть невыполненые уроки у пользователя ' . $user->getId());
                }
            }
        }
        $output->writeln('Всего: ' . $count . '/' . count($data));
        return 1;
    }
}

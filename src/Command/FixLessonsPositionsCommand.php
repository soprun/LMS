<?php

namespace App\Command;

use App\Entity\Courses\Course;
use App\Entity\Courses\Lesson;
use App\Entity\UserGroup;
use App\Service\AuthServerHelper;
use App\Service\Course\CertificateService;
use App\Service\Course\LessonHelper;
use App\Service\FormHelper;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FixLessonsPositionsCommand extends Command
{

    protected static $defaultName = 'app:fix-lessons-positions';

    private $em;

    public function __construct(
        EntityManagerInterface $em
    ) {
        parent::__construct();
        $this->em = $em;
    }

    protected function configure()
    {
        $this->setDescription('Фикс дублирующихся позиции уроков');
    }

    /**
     * @throws \Doctrine\DBAL\Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->processCourses($this->getCourseIdsWithDuplicatedLessonsPosition());
        $this->processCourses($this->getCourseIdsWithDuplicatedLessonsPosition(true), true);

        $this->processCourses($this->getCourseIdsWithMinLessonsPositionNotZero());
        $this->processCourses($this->getCourseIdsWithMinLessonsPositionNotZero(true), true);


        return 1;
    }

    protected function getCourseIdsWithDuplicatedLessonsPosition(bool $deletedLessons = false): array
    {
        $courseIds = $this->em->getRepository(Lesson::class)
            ->createQueryBuilder('l')
            ->distinct()
            ->select('course.id as course_id')
            ->innerJoin('l.course', 'course')
            ->where('l.deleted = :deleted')
            ->setParameter('deleted', $deletedLessons)
            ->groupBy('course.id')
            ->addGroupBy('l.position')
            ->having("count(course.id) > 1")
            ->getQuery()
            ->getResult();

        return array_column($courseIds, 'course_id');
    }

    protected function getCourseIdsWithMinLessonsPositionNotZero(bool $deletedLessons = false): array
    {
        $courseIds = $this->em->getRepository(Lesson::class)
            ->createQueryBuilder('l')
            ->distinct()
            ->select('course.id as course_id, min(l.position)')
            ->innerJoin('l.course', 'course')
            ->where('l.deleted = :deleted')
            ->setParameter('deleted', $deletedLessons)
            ->groupBy('course.id')
            ->having("min(l.position) > 0")
            ->getQuery()
            ->getResult();

        return array_column($courseIds, 'course_id');
    }

    /**
     * @param array $courseIds
     * @param bool $deletedLessons
     *
     * @throws \Doctrine\DBAL\Exception
     */
    protected function processCourses(array $courseIds, bool $deletedLessons = false): void
    {
        foreach ($courseIds as $courseId) {
            $this->processCourse($courseId, $deletedLessons);
        }

        $this->em->flush();
    }

    /**
     * @param int $courseId
     * @param bool $deletedLessons
     *
     * @throws \Doctrine\DBAL\Exception
     */
    protected function processCourse(int $courseId, bool $deletedLessons = false): void
    {
        $lessons = $this->em->getRepository(Lesson::class)
            ->createQueryBuilder('l')
            ->andWhere('l.deleted = :deleted and l.course = :courseId')
            ->setParameter('deleted', $deletedLessons)
            ->setParameter('courseId', $courseId)
            ->orderBy('l.position', 'ASC')
            ->getQuery()
            ->getResult();
        foreach ($lessons as $key => $lesson) {
            $this->processLesson($lesson, $key);
        }
    }

    /**
     * @param Lesson $lesson
     * @param int $position
     *
     * @return int
     * @throws \Doctrine\DBAL\Exception
     */
    protected function processLesson(Lesson $lesson, int $position): int
    {
        return $this->em->getConnection()->update(
            'courses_lesson',
            ['position' => $position],
            ['id' => $lesson->getId()]
        );
    }

}

<?php

namespace App\Command;

use _PHPStan_c862bb974\Symfony\Component\Console\Input\Input;
use App\Entity\Task\AnswerTaskType1;
use App\Entity\Task\TaskType1;
use App\Entity\Traction\Value;
use App\Entity\User;
use App\Service\AuthServerService;
use App\Service\FormHelper;
use App\Service\UserHelper;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class MSATractionExportCommand extends Command
{

    protected static $defaultName = 'app:msa-traction-export';

    private $em;
    private $authServerService;
    private $firstDay;
    private $lastDay;

    public function __construct(
        EntityManagerInterface $em,
        LoggerInterface $logger,
        UserHelper $userHelper,
        FormHelper $formHelper,
        AuthServerService $authServerService
    ) {
        parent::__construct();
        $this->em = $em;
        $this->authServerService = $authServerService;
    }

    protected function configure()
    {
        $this->setDescription('Выгрузка трекшн-аналитики МсА')
            ->addArgument('month', InputArgument::REQUIRED, 'Месяц');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $month = $input->getArgument('month');
        $this->firstDay = new \DateTime(date('Y') . '-' . $month . '-01');
        $this->lastDay = (clone $this->firstDay)->modify('last day of this month');
        $this->makeCSV();

        return 1;
    }

    private function getTractionValues()
    {
        return $this->em->getRepository(Value::class)
            ->createQueryBuilder('value')
            ->select(
                'user.id as userId',
                'type.title as typeTitle',
                'sum(value.value) as val',
                'user.authUserId as authUserId'
            )
            ->join('value.owner', 'user')
            ->join('value.type', 'type')
            ->join('user.groupRelations', 'groupRelation')
            ->join('groupRelation.userGroup', 'userGroup')
            ->join('userGroup.courseStream', 'cs')
            ->join('cs.abstractCourse', 'ac')
            ->where('value.date between :startDate AND :endDate')
            ->setParameter('startDate', $this->firstDay)
            ->setParameter('endDate', $this->lastDay)
            ->andWhere('type.id IN (:types)')
            ->setParameter('types', [1, 2, 3])
            ->andWhere('groupRelation.deleted = 0')
            ->andWhere('ac.slug = :slug')
            ->setParameter('slug', 'msa')
            ->groupBy('user.id')
            ->addGroupBy('type.id')
            ->orderBy('user.id')
            ->getQuery()
            ->getResult();
    }

    private function getOtherMsaUsers(array $userIds)
    {
        return $this->em->getRepository(User::class)
            ->createQueryBuilder('user')
            ->select(
                'user.id as userId',
                'user.authUserId as authUserId'
            )
            ->distinct()
            ->join('user.groupRelations', 'groupRelation')
            ->join('groupRelation.userGroup', 'userGroup')
            ->join('userGroup.courseStream', 'cs')
            ->join('cs.abstractCourse', 'ac')
            ->andWhere('groupRelation.deleted = 0')
            ->andWhere('ac.slug = :slug')
            ->andWhere('user.id not in (:userIds)')
            ->setParameter('slug', 'msa')
            ->setParameter('userIds', $userIds)
            ->orderBy('user.id')
            ->getQuery()
            ->getResult();
    }

    private function makeCSV()
    {
        $fp = fopen("msaTractionReport.csv", "w");

        $tractionValues = $this->getTractionValues();

        $header = ['lms user_id', 'ФИО', 'Выручка', 'Прибыль', 'NPS', 'Ниша'];
        $headerFlipped = array_flip($header);

        fputcsv(
            $fp,
            $header,
        );

        $users = [];
        foreach ($tractionValues as $tractionValue) {
            $users[$tractionValue['userId']][] = $tractionValue;
        }

        $otherMsaUsers = $this->getOtherMsaUsers(array_keys($users));

        foreach ($users as $key => $userValues) {
            $line = [];
            $line[0] = $key;

            $userInfo = $this->authServerService->getInternalUsers(['ids' => [$userValues[0]['authUserId']]]);
            $userInfo = $userInfo['list'][$userValues[0]['authUserId']];

            $line[1] = $userInfo['name'] . ' ' . $userInfo['lastname'];

            foreach ($userValues as $userValue) {
                if (array_key_exists($userValue['typeTitle'], $headerFlipped)) {
                    $line[$headerFlipped[$userValue['typeTitle']]] = $userValue['val'];
                }
            }

            $line[5] = $userInfo['niche'];

            ksort($line);
            fputcsv(
                $fp,
                $line,
            );
        }

        foreach ($otherMsaUsers as $otherMsaUser) {
            $line = [];
            $line[0] = $otherMsaUser['userId'];
            $userInfo = $this->authServerService->getInternalUsers(['ids' => [$otherMsaUser['authUserId']]]);
            $userInfo = $userInfo['list'][$otherMsaUser['authUserId']];

            $line[1] = $userInfo['name'] . ' ' . $userInfo['lastname'];
            $line[2] = $line[3] = $line[4] = null;
            $line[5] = $userInfo['niche'];

            fputcsv(
                $fp,
                $line,
            );
        }

        fclose($fp);
    }

}

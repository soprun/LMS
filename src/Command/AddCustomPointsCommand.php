<?php

namespace App\Command;

use App\Entity\Task\AnswerTaskType1;
use App\Entity\Task\TaskType1;
use App\Entity\User;
use App\Service\AuthServerHelper;
use App\Service\FormHelper;
use App\Service\UserHelper;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AddCustomPointsCommand extends Command
{

    protected static $defaultName = 'app:add-custom-points';

    private $em;
    private $logger;
    private $userHelper;
    private $formHelper;
    private $authServerHelper;

    public function __construct(
        EntityManagerInterface $em,
        LoggerInterface $logger,
        UserHelper $userHelper,
        FormHelper $formHelper,
        AuthServerHelper $authServerHelper
    ) {
        parent::__construct();
        $this->em = $em;
        $this->logger = $logger;
        $this->userHelper = $userHelper;
        $this->formHelper = $formHelper;
        $this->authServerHelper = $authServerHelper;
    }

    protected function configure()
    {
        $this->setDescription('Добавление баллов пользователям')
            ->addArgument('taskId', InputArgument::REQUIRED)
            ->addArgument('filePath', InputArgument::REQUIRED); // csv емейл;баллы
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $task = $this->em->getRepository(TaskType1::class)->find($input->getArgument('taskId'));
        $output->writeln($task->getQuestion());

        // подготавливаем входящие данные
        $fileRows = explode(PHP_EOL, file_get_contents($input->getArgument('filePath')));
        $output->writeln('Всего строк: ' . count($fileRows));
        foreach ($fileRows as $fileRow) {
            $fileRowArray = explode(';', $fileRow);
            $email = trim($fileRowArray[0]);
            $points = trim($fileRowArray[1]);

            $output->write($email . ' – ');

            // ищем пользователя
            list($authUser, $errors) = $this->authServerHelper->getUserInfoByEmail($email);
            if (!empty($errors)) {
                $output->writeLn($errors[0]['message']);
                continue;
            }
            $authUserId = $authUser->id;

            // ищем юзера у нас
            $lmsUser = $this->em->getRepository(User::class)->findOneBy(['authUserId' => $authUserId]);
            if (!$lmsUser) {
                $output->writeLn('LmsUser не найден');
                continue;
            }

            // создаем задание
            $answer = new AnswerTaskType1();
            $answer->setTask($task);
            $answer->setUser($lmsUser);
            $answer->setIsChecked(true);
            $answer->setAnswer('Баллы за продажи КЦ на рефералке');
            $answer->setPoints($points);
            $this->em->persist($answer);
            $this->em->flush();

            $output->writeLn($authUserId);
        }

        return 1;
    }

}

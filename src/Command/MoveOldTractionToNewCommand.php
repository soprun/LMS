<?php

namespace App\Command;

use App\Entity\Analytics\ReportMSG;
use App\Entity\Traction\Value;
use App\Entity\UserGroup;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MoveOldTractionToNewCommand extends Command
{

    protected static $defaultName = 'app:move-old-traction';

    private const MSA_TYPES_TO_TRACTION_TYPES = [
        0 => 2,
        1 => 1,
        2 => 3,
        3 => 4,
    ];

    private $em;

    public function __construct(
        EntityManagerInterface $em
    ) {
        parent::__construct();
        $this->em = $em;
    }

    protected function configure()
    {
        $this->setDescription('Перенос МСА на общий трекшн');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $ex = $this->em->getExpressionBuilder();
        $userGroups = $this->em->getRepository(UserGroup::class)
            ->createQueryBuilder('ug')
            ->andWhere('ug.groupFolder = :groupFolderId and ug.id > :id')
            ->setParameter('groupFolderId', 43)
            ->setParameter('id', 501)
            ->getQuery()
            ->execute();

        foreach ($userGroups as $userGroup) {
            foreach ($userGroup->getUserRelations() as $userRelation) {
                $users[] = $userRelation->getUser();
            }
        }

        $usersChunks = array_chunk($users, 100);

        foreach ($usersChunks as $usersChunk) {
            $tractionValues = $this->em->getRepository(Value::class)->createQueryBuilder('v')
                ->andWhere('v.owner in (:users)')
                ->setParameter('users', $usersChunk)
                ->andWhere('v.date >= :date')
                ->setParameter('date', '2021-09-01')
                ->getQuery()
                ->execute();

            $tractionValues = collect($tractionValues)
                ->groupBy(function ($i) {
                    return $i->getOwner()->getId();
                })->toArray();

            $reports = $this->em->getRepository(ReportMSG::class)->createQueryBuilder('rm')
                ->select('rm, cells')
                ->innerJoin('rm.cells', 'cells')
                ->andWhere('rm.period = :period')
                ->setParameter(':period', 0)
                ->andWhere('rm.year = :year')
                ->setParameter('year', date('Y'))
                ->andWhere('rm.user in (:users)')
                ->setParameter('users', $usersChunk)
                ->andWhere('cells.weekId > :weekId')
                ->setParameter('weekId', 7)
                ->andWhere('cells.profit is not null')
                ->getQuery()
                ->execute();

            foreach ($reports as $report) {
                $userId = $report->getUser()->getId();
                $msaType = $report->getChannelId();

                foreach ($report->getCells() as $cell) {
                    $cellTimestamp = strtotime('2021-' . ($cell->getWeekId() + 1) . '-01');
                    if (array_key_exists($userId, $tractionValues)) {
                        $valueExists = false;
                        foreach ($tractionValues[$userId] as $tractionValue) {
                            if (
                                $tractionValue->getDate()->getTimestamp() === $cellTimestamp
                                && $tractionValue->getType()->getId() === self::MSA_TYPES_TO_TRACTION_TYPES[$msaType]
                            ) {
                                $valueExists = true;
                            }
                        }

                        if (!$valueExists) {
                            $this->em->getConnection()->createQueryBuilder()->insert('traction_value')
                                ->values([
                                    'owner_id' => $userId,
                                    'type_id' => self::MSA_TYPES_TO_TRACTION_TYPES[$msaType],
                                    'date' => $ex->literal(date('Y-m-d', $cellTimestamp)),
                                    'value' => $cell->getProfit(),
                                    'created_at' => $ex->literal(date('Y-m-d H:i:s')),
                                    'updated_at' => $ex->literal(date('Y-m-d H:i:s')),
                                ])->execute();
                        }
                    } else {
                        $this->em->getConnection()->createQueryBuilder()->insert('traction_value')
                            ->values([
                                'owner_id' => $userId,
                                'type_id' => self::MSA_TYPES_TO_TRACTION_TYPES[$msaType],
                                'date' => $ex->literal(date('Y-m-d', $cellTimestamp)),
                                'value' => $cell->getProfit(),
                                'created_at' => $ex->literal(date('Y-m-d H:i:s')),
                                'updated_at' => $ex->literal(date('Y-m-d H:i:s')),
                            ])->execute();
                    }
                }
            }
        }

        return 1;
    }

}

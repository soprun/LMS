<?php

namespace App\Command\Likecentre;

use App\Entity\UserGroupUser;
use App\Service\LikecentreDatabaseHelper;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateUserGroupUserBudgetCommand extends Command
{

    protected static $defaultName = 'likecentre:update-user-group-user-budget';

    private $em;
    private $likecentreDatabaseHelper;

    public function __construct(EntityManagerInterface $em, LikecentreDatabaseHelper $likecentreDatabaseHelper)
    {
        parent::__construct();

        $this->em = $em;
        $this->likecentreDatabaseHelper = $likecentreDatabaseHelper;
    }

    protected function configure()
    {
        $this->setDescription('Обновление бюджетов добавлений в группы. Забираем из amoCRM')
            ->addArgument(
                'courseStreamName',
                InputArgument::REQUIRED,
                "Имя потока, например 'Сотка 6,Скорость Клуб 3'"
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $courses = explode(',', $input->getArgument('courseStreamName'));

        $output->writeln('Ищем потоки:');
        foreach ($courses as $course) {
            $output->writeln($course);
        }

        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(UserGroupUser::class)->createQueryBuilder('ugu');
        /** @var UserGroupUser[] $relations */
        $relations = $qb
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ug.courseStream', 'cs')
            ->where('cs.name IN(:names)')
            ->andWhere('ugu.deleted = 0')
            ->andWhere('ugu.budget IS NULL')
            ->andWhere('ugu.amoLeadId IS NOT NULL')
            ->setParameter('names', $courses)
            ->getQuery()
            ->getResult();

        $output->writeln(
            sprintf(
                "Добавлений со сделкой, но без бюджета: %s",
                count($relations)
            )
        );

        $budgets = $this->getBudgets($relations);

        $nodFound = 0;
        foreach ($relations as $relation) {
            $amoId = $relation->getAmoLeadId();
            if (!isset($budgets[$amoId])) {
                $nodFound++;
                continue;
            }

            $relation->setBudget($budgets[$amoId]);
            $this->em->persist($relation);
        }
        $this->em->flush();

        /*
         * Причины того, что сделка не нашлась в ЛЦ:
         * 1. Сделка была удалена в амо (на стороне LMS нет такой хука). В ответе команда LMS
         * 2. Сделка была создана через refToolbox или appToolbox, но не долетела до ЛЦ. В ответе команда Коммерции
         */
        $output->writeln('Не нашли сделок в ЛЦ: ' . $nodFound);

        return 0;
    }

    private function getBudgets(array $relations): array
    {
        $amoIds = implode(
            ',',
            array_map(
                static function (UserGroupUser $relation) {
                    return $relation->getAmoLeadId();
                },
                $relations
            )
        );

        $sql = <<<SQL
            SELECT amo_lead_id, budget
            FROM leads
            WHERE amo_lead_id IN ({$amoIds})
              AND deleted_at IS NULL;
        SQL;

        $result = $this->likecentreDatabaseHelper->execute($sql);
        $rows = [];

        foreach ($result as $value) {
            $rows[$value['amo_lead_id']] = $value['budget'];
        }

        return $rows;
    }

}

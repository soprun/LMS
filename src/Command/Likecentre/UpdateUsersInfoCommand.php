<?php

namespace App\Command\Likecentre;

use App\Entity\AbstractCourse;
use App\Entity\User;
use App\Service\AuthServerHelper;
use App\Service\LikecentreDatabaseHelper;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @deprecated Так давно не юзалось, что скорее всего deprecated
 */
class UpdateUsersInfoCommand extends Command
{

    protected static $defaultName = 'likecentre:update-users-info';

    private $em;
    private $likecentreDatabaseHelper;
    private $authServerHelper;

    public function __construct(
        EntityManagerInterface $em,
        LikecentreDatabaseHelper $likecentreDatabaseHelper,
        AuthServerHelper $authServerHelper
    ) {
        parent::__construct();

        $this->em = $em;
        $this->likecentreDatabaseHelper = $likecentreDatabaseHelper;
        $this->authServerHelper = $authServerHelper;
    }

    protected function configure()
    {
        $this->setDescription('Обновить неверные продукты в боте');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // берем людей в группах
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(User::class)->createQueryBuilder('u');
        $rows = $qb
            ->select('u.authUserId', 'u.id AS lmsUserId')
            ->innerJoin('u.groupRelations', 'ugu')
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ug.courseStream', 'cs')
            ->innerJoin('cs.abstractCourse', 'ac')
            ->andWhere('(ac.slug = :slug1 AND cs.stream = :stream1) OR (ac.slug in (:slug2) AND cs.stream = :stream2)')
            ->andWhere('ugu.deleted = 0')
            ->setParameters(
                [
                    'slug1' => AbstractCourse::SLUG_HUNDRED,
                    'stream1' => 4,
                    'slug2' => [AbstractCourse::SLUG_SPEED, AbstractCourse::SLUG_SPEED_CLUB],
                    'stream2' => 19,
                ]
            )
            ->groupBy('u.authUserId', 'u.id')
            ->getQuery()
            ->getResult();

        $ids = [];
        foreach ($rows as $row) {
            $ids[$row['lmsUserId']] = $row['authUserId'];
        }
        unset($rows);

        // берем их емейлы
        $users = [];
        foreach ($this->authServerHelper->getUsers($ids, ['id', 'email', 'city']) as $user) {
            $users[mb_strtolower($user['email'])] = [
                'authUserId' => $ids[$user['id']],
                'city' => $user['city'],
            ];
        }

        $emails = implode(
            ',',
            array_map(
                static function ($e) {
                    return sprintf("'%s'", $e);
                },
                array_keys($users)
            )
        );

        // по емейлам ищем контакты на лц
        $sql = <<<SQL
            SELECT email, ANY_VALUE(city) AS city
            FROM (
                     SELECT c.email, IF(r.group_name = 'Франчайзи', r.name, l.city) AS city
                     FROM leads l
                              INNER JOIN contacts c ON l.contact_id = c.id
                              INNER JOIN amo_products p ON l.amo_product_id = p.amo_product_id
                              LEFT JOIN responsibles r ON c.responsible_id = r.amo_id
                     WHERE c.email IN ({$emails})
                       AND l.amo_status_id = 142
                       AND l.deleted_at IS NULL
                       AND r.deleted_at IS NULL
                       AND (p.amo_product_name LIKE 'Скорость%' OR p.amo_product_name LIKE 'Сотка%')
                     ORDER BY l.budget DESC
                 ) s
            WHERE city != 'Другой город'
            GROUP BY email;
        SQL;

        $rows = $this->likecentreDatabaseHelper->execute($sql);

        $count = 0;
        foreach ($rows as $row) {
            if (!isset($users[mb_strtolower($row['email'])]) || $row['city'] === 'Другой город') {
                continue;
            }

            $user = $users[mb_strtolower($row['email'])];

            if ($user['city'] !== $row['city']) {
                // обновляем на аутхе
                $output->writeln(
                    sprintf(
                        "%s) %s -> %s / %s",
                        ++$count,
                        $user['city'],
                        $row['city'],
                        $row['email']
                    )
                );

                $result = $this->authServerHelper->editUserByServer(
                    [
                        'authUserId' => $user['authUserId'],
                        'city' => $row['city'],
                    ]
                );
                dump($result);
            }
        }


        return 0;
    }

}

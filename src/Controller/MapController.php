<?php

namespace App\Controller;

use App\DTO\UserLocationDto;
use App\Entity\User;
use App\Exception\RequestValidationException;
use App\Form\UserLocationType;
use App\Service\MapService;
use CrEOF\Spatial\PHP\Types\Geography\Point;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use OpenApi\Annotations as OA;

/**
 * @Route("/api/map", name="map_")
 *
 * @OA\Tag(name="Карта")
 *
 * @IsGranted("ROLE_USER")
 */
class MapController extends BaseController
{
    private $mapService;

    public function __construct(
        EntityManagerInterface $em,
        NormalizerInterface $normalizer,
        LoggerInterface $logger = null,
        MapService $mapService
    ) {
        parent::__construct($em, $normalizer, $logger);
        $this->mapService = $mapService;
    }


    /**
     * @Rest\Patch("/user/location", name="user_location")
     *
     * @Rest\View(serializerGroups={"location"})
     *
     * @OA\RequestBody(@OA\JsonContent(
     *     @OA\Property(property="lon", type="number", example=70.123, description="Долгота"),
     *     @OA\Property(property="lat", type="number", example=49.123, description="Штрота")))
     *
     */
    public function setUserLocation(Request $request): array
    {
        $form = $this->createForm(UserLocationType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $requestBody = $form->getData();
            $point = new Point($requestBody['lon'], $requestBody['lat']);

            return ['user' => $this->mapService->setUserLocation($this->getUser(), $point)];
        }

        throw new RequestValidationException(
            $form->getErrors(true),
            'Данные не прошли валидацию'
        );
    }

    /**
     * @Rest\Get ("/user", name="user")
     *
     * @Rest\View(serializerGroups={"location"})
     *
     * @OA\Parameter(name="lon", in="query",
     *      @OA\Schema(type="number"), required=true,
     *      description="Долгота")
     *
     * @OA\Parameter(name="lat", in="query",
     *      @OA\Schema(type="number"), required=true,
     *      description="Широта")
     *
     * @OA\Parameter(name="lonDelta", in="query",
     *      @OA\Schema(type="number"), required=true,
     *      description="Долгота граница")
     *
     * @OA\Parameter(name="latDelta", in="query",
     *      @OA\Schema(type="number"), required=true,
     *      description="Широта граница")
     *
     * @OA\Parameter(name="pointSizeLon", in="query",
     *      @OA\Schema(type="number"), required=true,
     *      description="Размер точки долгота")
     *
     * @OA\Parameter(name="pointSizeLat", in="query",
     *      @OA\Schema(type="number"), required=true,
     *      description="Размер точки широта")
     *
     * @Rest\QueryParam(name="lon", strict=true,
     *     requirements="^((\-?|\+?)?\d+(\.\d+)?).\s*((\-?|\+?)?\d+(\.\d+)?)$",
     *     description="Долгота")
     *
     * @Rest\QueryParam(name="lat", strict=true,
     *     requirements="^((\-?|\+?)?\d+(\.\d+)?).\s*((\-?|\+?)?\d+(\.\d+)?)$",
     *     description="Широта")
     *
     * @Rest\QueryParam(name="lonDelta",  strict=true,
     *     requirements="^((\-?|\+?)?\d+(\.\d+)?).\s*((\-?|\+?)?\d+(\.\d+)?)$",
     *     description="Долгота граница")
     *
     * @Rest\QueryParam(name="latDelta",  strict=true,
     *     requirements="^((\-?|\+?)?\d+(\.\d+)?).\s*((\-?|\+?)?\d+(\.\d+)?)$",
     *     description="Широта граница")
     *
     * @Rest\QueryParam(name="pointSizeLon",  strict=true,
     *     requirements="^((\-?|\+?)?\d+(\.\d+)?).\s*((\-?|\+?)?\d+(\.\d+)?)$",
     *     description="Размер точки долгота")
     *
     * @Rest\QueryParam(name="pointSizeLat",  strict=true,
     *     requirements="^((\-?|\+?)?\d+(\.\d+)?).\s*((\-?|\+?)?\d+(\.\d+)?)$",
     *     description="Размер точки широта")
     */
    public function getUsersOnMap(ParamFetcherInterface $paramFetcher): array
    {
        $longitude = $paramFetcher->get('lon');
        $latitude = $paramFetcher->get('lat');
        $longitudeDelta = $paramFetcher->get('lonDelta');
        $latitudeDelta = $paramFetcher->get('latDelta');
        $pointSizeLon = $paramFetcher->get('pointSizeLon');
        $pointSizeLat = $paramFetcher->get('pointSizeLat');

        $userLocationDto = new UserLocationDto(
            $longitude,
            $latitude,
            $pointSizeLon,
            $pointSizeLat,
            $longitudeDelta,
            $latitudeDelta
        );

        $users = $this->mapService->getClosestUsersToThePoint($userLocationDto);

        return $users;
    }

    /**
     * @Rest\Get ("/user/point", name="user_point")
     *
     * @Rest\View(serializerGroups={"location"})
     *
     * @OA\Parameter(name="lon", in="query",
     *      @OA\Schema(type="number"), required=true,
     *      description="Долгота")
     *
     * @OA\Parameter(name="lat", in="query",
     *      @OA\Schema(type="number"), required=true,
     *      description="Широта")
     *
     * @OA\Parameter(name="pointSizeLon", in="query",
     *      @OA\Schema(type="number"), required=true,
     *      description="Размер точки долгота")
     *
     * @OA\Parameter(name="pointSizeLat", in="query",
     *      @OA\Schema(type="number"), required=true,
     *      description="Размер точки широта")
     *
     * @Rest\QueryParam(name="lon", strict=true,
     *     requirements="^((\-?|\+?)?\d+(\.\d+)?).\s*((\-?|\+?)?\d+(\.\d+)?)$",
     *     description="Долгота")
     *
     * @Rest\QueryParam(name="lat", strict=true,
     *     requirements="^((\-?|\+?)?\d+(\.\d+)?).\s*((\-?|\+?)?\d+(\.\d+)?)$",
     *     description="Широта")
     *
     * @Rest\QueryParam(name="pointSizeLon",  strict=true,
     *     requirements="^((\-?|\+?)?\d+(\.\d+)?).\s*((\-?|\+?)?\d+(\.\d+)?)$",
     *     description="Размер точки долгота")
     *
     * @Rest\QueryParam(name="pointSizeLat",  strict=true,
     *     requirements="^((\-?|\+?)?\d+(\.\d+)?).\s*((\-?|\+?)?\d+(\.\d+)?)$",
     *     description="Размер точки широта")
     */
    public function getUsersAtPoint(ParamFetcherInterface $paramFetcher): array
    {
        $longitude = $paramFetcher->get('lon');
        $latitude = $paramFetcher->get('lat');
        $pointSizeLon = $paramFetcher->get('pointSizeLon');
        $pointSizeLat = $paramFetcher->get('pointSizeLat');

        $mapDto = new UserLocationDto($longitude, $latitude, $pointSizeLon, $pointSizeLat);

        $users = $this->mapService->getUsersAtPoint($mapDto);

        return $users;
    }

}

<?php

namespace App\Controller;

use App\Service\LikeItHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;

class CityController extends BaseController
{
    /**
     * Список городов
     * @Route("/api/cities", methods={"GET"})
     * @IsGranted("ROLE_USER")
     */
    public function getCities(LikeItHelper $likeItHelper)
    {
        return $this->getResponse('getCities', $likeItHelper->getCities());
    }
}

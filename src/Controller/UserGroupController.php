<?php

namespace App\Controller;

use App\Entity\UserGroup;
use App\Entity\UserGroupFolder;
use App\Entity\UserGroupUser;
use App\Form\UserGroupType;
use App\Service\UserGroupHelper;
use App\Service\UserHelper;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Cache\TagAwareCacheInterface;

class UserGroupController extends BaseController
{
    /**
     * @Route("/users/group/list", name="app_users_group_list", methods={"GET"})
     */
    public function pageUserGroupList()
    {
        return $this->render('lms_vue.html.twig');
    }

    /**
     * @Route("/users/group/list/get", name="app_users_group_list_get", methods={"GET"})
     * @IsGranted("USER_GROUP_LIST")
     */
    public function getUserGroupList(UserGroupHelper $groupHelper)
    {
        return $groupHelper->getUserGroupList();
    }

    /**
     * @Route("/groups/{id}", methods={"GET"})
     * @param UserGroup $group
     * @IsGranted("USER_GROUP_LIST")
     */
    public function getGroupPermission(UserGroup $group, UserHelper $userHelper)
    {
        $permission = $userHelper->getPermissionsByGroup($group);

        return $this->getResponse('getGroupPermission', $permission);
    }

    /**
     * @Route("/users/group/new", name="app_users_group_new", methods={"POST"})
     * @IsGranted("USER_GROUP_CREATE")
     */
    public function newUserGroup(Request $request, EntityManagerInterface $em, TagAwareCacheInterface $cache)
    {
        $userGroup = new UserGroup();
        $form = $this->createForm(UserGroupType::class, $userGroup);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($userGroup);
            $em->flush();

            $cache->invalidateTags(['getUserGroupList']);

            return $this->getResponse('newUserGroup', ['id' => $userGroup->getId()]);
        }

        return $this->addFormErrors($form)->getResponse('newUserGroup');
    }

    /**
     * userGroup{
     *      name,
     *      folderId,
     *      permission{
     *          isAdmin: true,
     *          folderModuleAdmin: {
     *              create: "2",
     *              read: "2",
     *              update: "2",
     *              delete: "2"
     *          },
     *          ...
     *      },
     *      courses{
     *          id => 1,
     *          id => 2,
     *          ...
     *      }
     *      courseFolders{
     *          id => 1,
     *          id => 2,
     *          ...
     *      }
     * }
     *
     * @Route("/users/group/{userGroup}/edit", name="app_users_group_edit", methods={"POST"})
     * @ParamConverter("userGroup", options={"mapping": {"userGroup": "id"}})
     * @IsGranted("USER_GROUP_EDIT")
     */
    public function editUserGroup(
        Request $request,
        UserGroup $userGroup,
        UserGroupHelper $groupHelper,
        TagAwareCacheInterface $cache
    ) {
        $cache->invalidateTags(['getUserGroupList']);

        return $groupHelper->editUserGroup($request, $userGroup);
    }

    /**
     * @Route("/users/group/{userGroup}/delete", name="app_users_group_delete", methods={"DELETE"})
     * @ParamConverter("userGroup", options={"mapping": {"userGroup": "id"}})
     * @IsGranted("USER_GROUP_DELETE")
     */
    public function deleteUserGroup(
        UserGroup $userGroup,
        UserGroupHelper $groupHelper,
        TagAwareCacheInterface $cache
    ) {
        $cache->invalidateTags(['getUserGroupList']);

        return $groupHelper->deleteUserGroup($userGroup);
    }

    /**
     * Страница со всеми пользователями в группе
     * @Route("/users/group/{userGroup}/users", name="app_users_group_list_user", methods={"GET"})
     */
    public function userGroupUsers()
    {
        return $this->render('lms_vue.html.twig');
    }

    /**
     * Получение всех пользователей в группе
     * @Route("/users/group/{userGroup}/user/get", name="app_users_group_user_get", methods={"GET"})
     * @ParamConverter("userGroup", options={"mapping": {"userGroup": "id"}})
     * @IsGranted("USER_GROUP_LIST")
     *
     * @param UserGroup $userGroup
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param UserHelper $userHelper
     *
     * @return JsonResponse
     */
    public function getUsersFromGroup(
        UserGroup $userGroup,
        Request $request,
        EntityManagerInterface $em,
        UserHelper $userHelper
    ): JsonResponse {
        $page = (int)$request->query->get('page', 1);
        $search = $request->query->get('name');

        $userRelations = $em->getRepository(UserGroupUser::class)->createQueryBuilder('ugu')
            ->select('DISTINCT(user.authUserId) as id, ugu.createdAt')
            ->innerJoin('ugu.user', 'user')
            ->andWhere('ugu.userGroup = :userGroup')
            ->andWhere('ugu.deleted = 0')
            ->setParameter('userGroup', $userGroup)
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);

        $userData = [];
        foreach ($userRelations as $user) {
            $userData[$user['id']] = $user['createdAt']->format("Y-m-d");
        }

        $authUsers = $userHelper->getUserInfoArrayByIds(array_keys($userData), $search, $page);
        foreach ($authUsers as &$authUser) {
            $id = $authUser['authUserId'];
            $authUser['createdAt'] = $userData[$id];
        }

        return $this->getResponse('getUsersFromGroup', ['users' => $authUsers]);
    }

    /**
     * users{
     *      id = 1,
     *      id = 2,
     * }
     *
     * @Route("/users/group/{userGroup}/user/add", name="app_users_group_user_add", methods={"POST"})
     * @ParamConverter("userGroup", options={"mapping": {"userGroup": "id"}})
     * @IsGranted("USER_GROUP_EDIT")
     */
    public function addUsersToGroup(Request $request, UserGroup $userGroup, UserGroupHelper $groupHelper)
    {
        return $groupHelper->addUsersToGroup($request, $userGroup);
    }

    /**
     * Удаление пользователей из группы
     * @Route("/users/group/{userGroup}/user/remove", name="app_users_group_user_remove", methods={"POST"})
     * @ParamConverter("userGroup", options={"mapping": {"userGroup": "id"}})
     * @IsGranted("USER_GROUP_EDIT")
     *
     * @param Request $request
     * @param UserGroup $userGroup
     * @param UserGroupHelper $groupHelper
     *
     * @return JsonResponse
     */
    public function removeUsersFromGroup(
        Request $request,
        UserGroup $userGroup,
        UserGroupHelper $groupHelper
    ): JsonResponse {
        return $groupHelper->removeUsersFromGroup($request, $userGroup);
    }

    /**
     * groupFolder{
     *      name = Папка,
     *      parentId = 2,
     * }
     *
     * @Route("/users/groupFolder/add", name="app_group_folder_add", methods={"POST"})
     * @IsGranted("USER_GROUP_CREATE")
     */
    public function addGroupFolder(Request $request, UserGroupHelper $groupHelper)
    {
        return $groupHelper->addOrEditGroupFolder($request);
    }

    /**
     * groupFolder{
     *      name = Папка,
     *      parentId = 2,
     * }
     *
     * @Route("/users/groupFolder/{folderId}/edit", name="app_group_folder_edit", methods={"POST"})
     * @ParamConverter("userGroupFolder", options={"mapping": {"folderId": "id"}})
     * @IsGranted("USER_GROUP_EDIT")
     */
    public function editGroupFolder(Request $request, UserGroupFolder $userGroupFolder, UserGroupHelper $groupHelper)
    {
        return $groupHelper->addOrEditGroupFolder($request, $userGroupFolder);
    }

    /**
     * @Route("/users/groupFolder/{folderId}/delete", name="app_group_folder_delete", methods={"DELETE"})
     * @ParamConverter("userGroupFolder", options={"mapping": {"folderId": "id"}})
     * @IsGranted("USER_GROUP_DELETE")
     */
    public function deleteGroupFolder(UserGroupFolder $userGroupFolder, UserGroupHelper $groupHelper)
    {
        return $groupHelper->deleteGroupFolder($userGroupFolder);
    }

}

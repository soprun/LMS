<?php

namespace App\Controller;

use App\Entity\User;
use App\Exception\RequestValidationException;
use ArrayObject;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Class BaseController
 *
 * @package App\Controller
 *
 * @method User getUser()
 */
abstract class BaseController extends AbstractFOSRestController
{
    private $errors;
    private $code;

    /** @var EntityManagerInterface */
    protected $em;
    /** @var NormalizerInterface */
    private $normalizer;
    /** @var LoggerInterface */
    protected $logger;

    /**
     * @param EntityManagerInterface $em
     * @param NormalizerInterface $normalizer
     * @param null|LoggerInterface $logger
     */
    public function __construct(
        EntityManagerInterface $em,
        NormalizerInterface $normalizer,
        LoggerInterface $logger = null
    ) {
        $this->errors = [];
        $this->code = Response::HTTP_BAD_REQUEST;
        $this->em = $em;
        $this->normalizer = $normalizer;
        $this->logger = $logger;
    }

    /**
     * @param string $action
     * @param array $data
     *
     * @return JsonResponse
     * @deprecated use getResponseNew instead
     *
     */
    public function getResponse(string $action, array $data = [])
    {
        if ($this->errors === []) {
            $status = 'success';
            if ($this->code == 400) {
                $this->code = 200;
            }
        } else {
            $status = 'error';
        }

        return new JsonResponse(
            [
                'action' => $action,
                'status' => $status,
                'errors' => $this->errors,
                'data' => $data,
            ],
            $this->code
        );
    }

    /**
     * Костыльно, но пока так
     *
     * @param  $data
     * @param array $serializedGroups
     *
     * @return JsonResponse
     */
    public function getResponseNew($data = null, array $serializedGroups = []): JsonResponse
    {
        if ($this->errors === []) {
            $status = 'success';
            if ($this->code === Response::HTTP_BAD_REQUEST) {
                $this->code = Response::HTTP_OK;
            }
        } else {
            $status = 'error';
        }

        if (!empty($serializedGroups)) {
            try {
                $data = $this->normalize($data, $serializedGroups);
            } catch (ExceptionInterface $e) {
                throw new RuntimeException($e->getMessage(), $e->getCode(), $e);
            }
        }

        return new JsonResponse(
            [
                'status' => $status,
                'errors' => $this->errors,
                'data' => $data,
            ],
            $this->code
        );
    }

    /**
     * @template EntityClass
     *
     * @param string $formType
     * @param null|Request $request
     * @param EntityClass|array $entity
     *
     * @return EntityClass
     * @deprecated use Form::handleRequest instead
     */
    protected function fillByForm(string $formType, Request $request, $entity = null)
    {
        $form = $this->createForm($formType, $entity);
        $form->submit(array_merge($request->request->all(), $request->files->all(), $request->query->all()), false);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                return $form->getData();
            }
            throw new RequestValidationException($form->getErrors(true), 'Данные не прошли валидацию');
        }

        throw new RuntimeException('Форма не отправлена');
    }

    public function addError(string $field, string $message, ?int $code = Response::HTTP_BAD_REQUEST): self
    {
        $this->errors[] = [
            'field' => $field,
            'message' => $message,
        ];

        $this->code = $code;

        return $this;
    }

    protected function hasErrors(): bool
    {
        return !empty($this->errors);
    }

    protected function addFormErrors(FormInterface $form): self
    {
        if (count($form->getErrors(true))) {
            /** @var FormError $error */
            foreach ($form->getErrors(true) as $error) {
                $this->addError(
                    $error->getOrigin()->getName(),
                    $error->getMessage()
                );
            }
        }

        return $this;
    }

    /**
     * @param $object
     * @param array $groups
     *
     * @return null|array|ArrayObject|bool|float|int|string
     * @throws ExceptionInterface
     */
    public function normalize($object, array $groups)
    {
        return $this->normalizer->normalize($object, null, ['groups' => $groups]);
    }

}

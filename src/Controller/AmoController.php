<?php

declare(strict_types=1);

namespace App\Controller;

use AmoCRM\Client\AmoCRMApiClient as AmoClient;
use App\Service\AmoCRM\AmoCRMCallbackService;
use App\Service\AmoHelper;
use App\Service\FormHelper;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Throwable;

/*
 * Временный контроллер для добавления пользователей в ЛМС и группу после покупки курса.
 * Можно удалить, когда сделаем нормальную оплату
 */

/**
 * @Route("/amo", name="amo_")
 */
final class AmoController extends BaseController
{
    /**
     * @var AmoCRMCallbackService
     */
    private $amoCRMCallback;

    public function __construct(
        EntityManagerInterface $em,
        NormalizerInterface $normalizer,
        LoggerInterface $logger,
        AmoCRMCallbackService $amoCRMCallback
    ) {
        parent::__construct($em, $normalizer, $logger);

        $this->amoCRMCallback = $amoCRMCallback;
    }

    /**
     * @Route("/setup/{code}", name="app_setup")
     * @IsGranted("ROLE_USER")
     *
     * @param $code
     * @param AmoHelper $amoHelper
     * @param LoggerInterface $logger
     * @param FormHelper $formHelper
     * @param string $_AmocrmID
     * @param string $_AmocrmSecret
     * @param string $_AmocrmDomain
     * @param string $_AmocrmRedirectUri
     *
     * @return JsonResponse
     */
    public function setup(
        $code,
        AmoHelper $amoHelper,
        LoggerInterface $logger,
        FormHelper $formHelper,
        string $_AmocrmID,
        string $_AmocrmSecret,
        string $_AmocrmDomain,
        string $_AmocrmRedirectUri
    ) {
        $apiClient = new AmoClient($_AmocrmID, $_AmocrmSecret, $_AmocrmRedirectUri);
        $apiClient->setAccountBaseDomain($_AmocrmDomain);
        try {
            $accessToken = $apiClient->getOAuthClient()->getAccessTokenByCode($code);
            if (!$accessToken->hasExpired()) {
                $amoHelper->saveToken(
                    [
                        'accessToken' => $accessToken->getToken(),
                        'refreshToken' => $accessToken->getRefreshToken(),
                        'expires' => $accessToken->getExpires(),
                        'baseDomain' => $apiClient->getAccountBaseDomain(),
                    ]
                );

                return $formHelper->getResponse('Токен создан');
            }
        } catch (Exception $e) {
            $logger->critical('app_amo_setup: {message}', [
                'message' => $e->getMessage(),
            ]);
        }

        return $formHelper->getResponse('Что-то пошло не так');
    }

    /**
     * @Route("/callback", name="app_callback")
     *
     * @param Request $request
     * @param AmoHelper $amoHelper
     * @param LoggerInterface $logger
     * @param FormHelper $formHelper
     *
     * @return JsonResponse
     */
    public function callback(
        Request $request,
        LoggerInterface $logger,
        FormHelper $formHelper
    ): Response {
        $data = $request->request->all();

        $logger->info('{method}', [
            'method' => __METHOD__,
            'data' => $data,
        ]);

        if (isset($data['leads']) === false) {
            $formHelper->addError(
                'leads',
                'An error has occurred, data[leads] cannot be empty!'
            );

            return $formHelper->getResponse('callback');
        }

        if (array_key_exists('status', $data['leads']) || array_key_exists('add', $data['leads'])) {
            $callbackData = [];
            if (array_key_exists('status', $data['leads'])) {
                $callbackData = $data['leads']['status'];
            } elseif (array_key_exists('add', $data['leads'])) {
                $callbackData = $data['leads']['add'];
            }
            try {
                foreach ($callbackData as $callback) {
                    $this->amoCRMCallback->execute($callback);
                }
            } catch (Throwable $exception) {
                $this->logger->critical('{method}: {message}', [
                    'method' => __METHOD__,
                    'message' => $exception->getMessage(),
                    'trace' => $exception->getTrace(),
                ]);

                throw new BadRequestHttpException(
                    $exception->getMessage(),
                    $exception,
                    (int)$exception->getCode()
                );
            }
        }

        return $formHelper->getResponse('callback');
    }

    private function getLeadTags(int $leadId, AmoHelper $amoHelper, LoggerInterface $logger): array
    {
        // ищем лид
        $lead = $amoHelper->getLeadById($leadId);
        if (!$lead) {
            $logger->info(
                "AmoHelper::getLeadTags не нашли лид",
                [
                    'leadId' => $leadId,
                ]
            );

            return [];
        }

        return $this->getTags($lead);
    }

    private function getTags(array $lead): array
    {
        if (!isset($lead['tags'])) {
            return [];
        }

        return array_map(
            static function ($tag) {
                return mb_strtolower($tag['name']);
            },
            $lead['tags']
        );
    }

}

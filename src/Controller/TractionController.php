<?php

namespace App\Controller;

use App\Doctrine\StringableDateTime;
use App\DTO\Traction\UserTractionForSettlementPeriod;
use App\Entity\AbstractCourse;
use App\Entity\CourseStream;
use App\Entity\Traction\Value;
use App\Entity\User;
use App\Entity\UserGroup;
use App\Form\Traction\ValueType;
use App\Service\AuthServerService;
use App\Service\Traction\TractionService;
use DateTime;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Exception;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use InvalidArgumentException;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use OpenApi\Annotations as OA;

/**
 * @OA\Tag(name="Прогресс студента")
 *
 * @IsGranted("ROLE_USER")
 */
class TractionController extends BaseController
{
    /**
     * Список студентов
     *
     * @Rest\Get("/api/traction/users", name="traction_get_user")
     *
     * @Rest\QueryParam(name="order", requirements="revenue|profit", description="По какому значению сортировать")
     * @Rest\QueryParam(name="reversed", requirements="[01]", default=0, description="Инвертировать сортировку")
     *
     * @Rest\QueryParam(name="courses", requirements="[a-z_-]+", map=true, description="Только конкретные курсы")
     * @Rest\QueryParam(name="stream", requirements="\d+", description="ID потока курса")
     * @Rest\QueryParam(name="group", requirements="\d+", description="Группа пользователя")
     *
     * @Rest\QueryParam(name="q", description="Строка поиска")
     *
     * @Rest\QueryParam(name="startDate", requirements="\d{4}\-\d{1,2}\-\d{1,2}", description="С какой даты")
     * @Rest\QueryParam(name="endDate", requirements="\d{4}\-\d{1,2}\-\d{1,2}", description="По какую дату")
     *
     * @Rest\QueryParam(name="pageSize", requirements="\d+", default="10", description="Строк на страницу")
     * @Rest\QueryParam(name="page", requirements="\d+", default="1", description="Страница")
     *
     *
     * @OA\Parameter(
     *     name="order",
     *     in="query",
     *     description="По какому значению сортировать",
     *     @OA\Schema(type="array", @OA\Items(type="string", pattern="[a-z_-]+"))
     * )
     *
     * @OA\Parameter(
     *     name="courses[]",
     *     in="query",
     *     description="Только конкретные курсы",
     *     @OA\Schema(type="array", @OA\Items(type="string", pattern="[a-z_-]+"))
     * )
     * @OA\Parameter(name="group", in="query", description="Группа пользователя", @OA\Schema(type="integer"))
     * @OA\Parameter(name="stream", in="query", description="ID потока курса", @OA\Schema(type="integer"))
     *
     * @OA\Parameter(name="page", in="query", description="Страница", @OA\Schema(type="integer", default=1))
     * @OA\Parameter(
     *     name="pageSize",
     *     in="query",
     *     description="Строк на страницу",
     *     @OA\Schema(type="integer", default=10)
     * )
     *
     * @OA\Response(
     *     response=200,
     *     description="Список студентов",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *         @OA\Schema(
     *             type="object",
     *             @OA\Property(
     *                 property="status",
     *                 type="string"
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 type="array",
     *                 @OA\Items(
     *                     type="object",
     *                     @OA\Property(type="string", property="field"),
     *                     @OA\Property(type="string", property="message"),
     *                 ),
     *             ),
     *             @OA\Property(
     *                 property="data",
     *                 type="array",
     *                 @OA\Items(
     *                     type="object",
     *                     ref=@Model(
     *                         type=User::class,
     *                         groups={"traction"},
     *                     )
     *                 ),
     *             ),
     *         )
     *     )
     * )
     * @OA\Response(
     *      response=400,
     *      description="Ошибка авторизации",
     * )
     *
     * @IsGranted("USER_ANALYTICS_VIEW")
     */
    public function getListUsersAction(ParamFetcher $paramFetcher, AuthServerService $authServerService): JsonResponse
    {
        $page = (int)$paramFetcher->get('page');
        $pageSize = (int)$paramFetcher->get('pageSize');

        $qb = $this->em->getRepository(User::class)
            ->createQueryBuilder('user')
            ->select('distinct user.authUserId')
            ->join('user.groupRelations', 'groupRel')
            ->join('groupRel.userGroup', 'userGroup')
            ->join('userGroup.courseStream', 'courseStream')
            ->join('courseStream.abstractCourse', 'abstractCourse')
            ->where('groupRel.deleted = 0')
            // @todo: придумать лучшее решение
            ->andWhere('courseStream.id >= 23 or courseStream.id = 19');

        $this->addInFilter($paramFetcher, $qb, 'abstractCourse.slug', 'courses');
        $this->addEqFilter($paramFetcher, $qb, 'courseStream', 'stream');
        $this->addEqFilter($paramFetcher, $qb, 'userGroup', 'group');

        if ($order = $paramFetcher->get('order')) {
            $tractionValueTypeId = ($order === 'profit' ? 1 : 2);
            $reversed = $paramFetcher->get('reversed');

            $qb->select('distinct user.authUserId, tractionValue.date, tractionValue.value')
                ->leftJoin(
                    'user.tractionValues',
                    'tractionValue',
                    Join::WITH,
                    "tractionValue.type = " . $tractionValueTypeId . "
                     and tractionValue.date >=
                     DATE_ADD(courseStream.startDate, (-weekday(courseStream.startDate)),'DAY')
                        and tractionValue.date < DATE_ADD(courseStream.startDate, courseStream.period, 'WEEK')"
                );

            $this->addDateFilter($paramFetcher, $qb, 'tractionValue.date', '>=', 'startDate');
            $this->addDateFilter($paramFetcher, $qb, 'tractionValue.date', '<=', 'endDate');

            $users = [];
            $result = $qb->getQuery()->getScalarResult();
            foreach ($result as $item) {
                $users[$item['authUserId']] = ($users[$item['authUserId']] ?? 0) + $item['value'];
            }

            if ($reversed) {
                asort($users, SORT_NUMERIC);
            } else {
                arsort($users, SORT_NUMERIC);
            }
            $users = array_keys($users);
            if (!$paramFetcher->get('q')) {
                $count = count($users);
                $users = array_slice($users, ($page - 1) * $pageSize, $pageSize);
                $page = 1;
            }
        } elseif ($paramFetcher->get('startDate') || $paramFetcher->get('endDate')) {
            $qb->select('distinct user.authUserId, tractionValue.date, tractionValue.value')
                ->join('abstractCourse.tractionValueTypes', 'tractionValueType')
                ->leftJoin(
                    'user.tractionValues',
                    'tractionValue',
                    Join::WITH,
                    "tractionValue.type = tractionValueType
                     and tractionValue.date >=
                     DATE_ADD(courseStream.startDate, (-weekday(courseStream.startDate)),'DAY')
                     and tractionValue.date < DATE_ADD(courseStream.startDate, courseStream.period, 'WEEK')"
                );

            $this->addDateFilter($paramFetcher, $qb, 'tractionValue.date', '>=', 'startDate');
            $this->addDateFilter($paramFetcher, $qb, 'tractionValue.date', '<=', 'endDate');

            $users = [];
            $result = $qb->getQuery()->getScalarResult();
            foreach ($result as $item) {
                $users[$item['authUserId']] = ($users[$item['authUserId']] ?? 0) + $item['value'];
            }

            $users = array_keys($users);
            if (!$paramFetcher->get('q')) {
                $count = count($users);
                $users = array_slice($users, ($page - 1) * $pageSize, $pageSize);
                $page = 1;
            }
        } else {
            if (!$paramFetcher->get('q')) {
                $qb->setFirstResult(($page - 1) * $pageSize)
                    ->setMaxResults($pageSize);

                if ($page === 1) {
                    $count = (clone $qb)
                        ->select('count(distinct user.authUserId)')
                        ->resetDQLPart('orderBy')
                        ->resetDQLPart('groupBy')
                        ->getQuery()
                        ->getSingleScalarResult();
                }
                $page = 1;
            }
            $users = $qb->getQuery()->getScalarResult();
        }

        $authUsers = $authServerService->getInternalUsers([
            'ids' => $users,
            'q' => $paramFetcher->get('q'),
            'page' => $page,
            'pageSize' => $pageSize,
            'saveOrderByIds' => 1,
        ]);

        $qb = $this->em->getRepository(User::class)
            ->createQueryBuilder('user', 'user.authUserId')
            ->addSelect('tractionValue')
            ->join('user.groupRelations', 'groupRel')
            ->join('groupRel.userGroup', 'userGroup')
            ->join('userGroup.courseStream', 'courseStream')
            ->join('courseStream.abstractCourse', 'abstractCourse')
            ->where('groupRel.deleted = 0')
            // @todo: придумать лучшее решение
            ->andWhere('courseStream.id >= 23 or courseStream.id = 19')
            ->join('abstractCourse.tractionValueTypes', 'tractionValueType')
            ->leftJoin(
                'user.tractionValues',
                'tractionValue',
                Join::WITH,
                "tractionValue.type = tractionValueType
                 and tractionValue.date >= DATE_ADD(courseStream.startDate, (-weekday(courseStream.startDate)),'DAY')
                 and tractionValue.date < DATE_ADD(courseStream.startDate, courseStream.period, 'WEEK')"
            );

        $this->addInFilter($paramFetcher, $qb, 'abstractCourse.slug', 'courses');
        $this->addEqFilter($paramFetcher, $qb, 'courseStream', 'stream');
        $this->addEqFilter($paramFetcher, $qb, 'userGroup', 'group');
        $this->addDateFilter($paramFetcher, $qb, 'tractionValue.date', '>=', 'startDate');
        $this->addDateFilter($paramFetcher, $qb, 'tractionValue.date', '<=', 'endDate');

        $users = $authServerService->rebuildUsers($authUsers['list'], $qb, 'user', true);

        return $this->getResponseNew(
            [
                'list' => array_values($users),
                'count' => $count ?? $authUsers['count'],
                'authCount' => $authUsers['count'],
            ],
            ['traction']
        );
    }

    /**
     * @param ParamFetcher $paramFetcher
     * @param QueryBuilder $qb
     * @param string $queryKey
     * @param string $operator
     * @param string $dbAlias
     *
     * @return QueryBuilder
     * @throws Exception
     */
    protected function addDateFilter(
        ParamFetcher $paramFetcher,
        QueryBuilder $qb,
        string $dbAlias,
        string $operator,
        string $queryKey
    ): QueryBuilder {
        return $this->addFilter(
            $paramFetcher,
            $qb,
            $dbAlias,
            $operator,
            $queryKey,
            null,
            static function ($date) {
                return new DateTime($date);
            }
        );
    }

    /**
     * @param ParamFetcher $paramFetcher
     * @param QueryBuilder $qb
     * @param string $queryKey
     * @param string $dbAlias
     *
     * @return QueryBuilder
     */
    protected function addEqFilter(
        ParamFetcher $paramFetcher,
        QueryBuilder $qb,
        string $dbAlias,
        string $queryKey
    ): QueryBuilder {
        return $this->addFilter($paramFetcher, $qb, $dbAlias, '=', $queryKey);
    }

    /**
     * @param ParamFetcher $paramFetcher
     * @param QueryBuilder $qb
     * @param string $dbAlias
     * @param string $operator
     * @param string $queryKey
     * @param null|string $templateParameter
     * @param null|callable $callable
     *
     * @return QueryBuilder
     */
    protected function addFilter(
        ParamFetcher $paramFetcher,
        QueryBuilder $qb,
        string $dbAlias,
        string $operator,
        string $queryKey,
        ?string $templateParameter = null,
        ?callable $callable = null
    ): QueryBuilder {
        $allowedOperators = ['in', '=', '>', '<', '>=', '<='];
        if (!in_array($operator, $allowedOperators, true)) {
            throw new InvalidArgumentException('Invalid operator');
        }
        if ($value = $paramFetcher->get($queryKey)) {
            if ($callable) {
                $value = $callable($value);
            }
            $parameter = str_replace('.', '_', "sf_" . $dbAlias . "_" . $queryKey);
            $processedParameter = ":" . $parameter;
            if ($templateParameter) {
                $processedParameter = sprintf($templateParameter, $processedParameter);
            }
            $qb->andWhere($dbAlias . " " . $operator . " " . $processedParameter)
                ->setParameter($parameter, $value);
        }

        return $qb;
    }

    /**
     * @param ParamFetcher $paramFetcher
     * @param QueryBuilder $qb
     * @param string $queryKey
     * @param string $dbAlias
     *
     * @return QueryBuilder
     */
    protected function addInFilter(
        ParamFetcher $paramFetcher,
        QueryBuilder $qb,
        string $dbAlias,
        string $queryKey
    ): QueryBuilder {
        return $this->addFilter($paramFetcher, $qb, $dbAlias, 'in', $queryKey, '(%s)');
    }

    /**
     * Общая информация о студенте
     *
     * @Rest\Get("/api/traction/user/{id<\d+>}", name="traction_get_list_users")
     * @ParamConverter("user", options={"mapping": {"id": "id"}})
     *
     * @OA\Parameter(name="id", in="path", description="ID пользователя", @OA\Schema(type="integer"))
     *
     * @OA\Response(
     *     response=200,
     *     description="Общая информация о студенте",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *         @OA\Schema(
     *             type="object",
     *             @OA\Property(
     *                 property="status",
     *                 type="string"
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 type="array",
     *                 @OA\Items(
     *                     type="object",
     *                     @OA\Property(type="string", property="field"),
     *                     @OA\Property(type="string", property="message"),
     *                 ),
     *             ),
     *             @OA\Property(
     *                 property="data",
     *                 type="object",
     *                 ref=@Model(
     *                     type=User::class,
     *                     groups={"traction:user"},
     *                 )
     *             ),
     *         )
     *     )
     * )
     * @OA\Response(
     *      response=400,
     *      description="Ошибка авторизации",
     * )
     *
     * @IsGranted("USER_ANALYTICS_VIEW", subject="user")
     *
     * @throws Exception
     */
    public function getUserAction(AuthServerService $authServerService, User $user): JsonResponse
    {
        $users = $authServerService->refillUsers([$user]);

        return $this->getResponseNew($users[0], ['traction:user']);
    }

    /**
     * Доступные фильтры
     *
     * @Rest\Get("/api/traction/filters", name="traction_get_list_filters")
     *
     * @Rest\QueryParam(name="courses", requirements="[a-z_-]+", map=true, description="Только конкретные курсы")
     *
     * @OA\Parameter(
     *     name="courses[]",
     *     in="query",
     *     description="Только конкретные курсы",
     *     @OA\Schema(type="array", @OA\Items(type="string", pattern="[a-z_-]+"))
     * )
     *
     * @OA\Response(
     *     response=200,
     *     description="Доступные фильтры",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *         @OA\Schema(
     *             type="object",
     *             @OA\Property(
     *                 property="status",
     *                 type="string"
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 type="array",
     *                 @OA\Items(
     *                     type="object",
     *                     @OA\Property(type="string", property="field"),
     *                     @OA\Property(type="string", property="message"),
     *                 ),
     *             ),
     *             @OA\Property(
     *                 property="data",
     *                 type="object",
     *                 @OA\Property(
     *                      property="groups",
     *                      type="array",
     *                      @OA\Items(
     *                          ref=@Model(
     *                              type=UserGroup::class,
     *                              groups={"traction:filters"},
     *                          )
     *                      ),
     *                 ),
     *                 @OA\Property(
     *                      property="streams",
     *                      type="array",
     *                      @OA\Items(
     *                          ref=@Model(
     *                              type=CourseStream::class,
     *                              groups={"traction:filters"},
     *                          )
     *                      ),
     *                 ),
     *             ),
     *         )
     *     )
     * )
     * @OA\Response(
     *      response=400,
     *      description="Ошибка авторизации",
     * )
     *
     * @IsGranted("ROLE_USER")
     *
     * @throws Exception
     */
    public function getListFiltersAction(ParamFetcher $paramFetcher): JsonResponse
    {
        $courses = array_keys(AbstractCourse::TITLES);
        if ($paramFetcher->get('courses')) {
            $courses = $paramFetcher->get('courses');
        }
        $courseStreams = $this->em->getRepository(CourseStream::class)
            ->createQueryBuilder('courseStream')
            ->addSelect(['userGroup'])
            ->leftJoin('courseStream.userGroups', 'userGroup')
            ->innerJoin('courseStream.abstractCourse', 'abstractCourse')
            ->where('abstractCourse.slug in (:courseSlugs)')
            // @todo: придумать лучшее решение
            ->andWhere('courseStream.id >= 23 or courseStream.id = 19')
            ->setParameter('courseSlugs', $courses)
            ->getQuery()
            ->getResult();

        // @todo: удалить после правок на фронте
        $userGroups = $this->em->getRepository(UserGroup::class)
            ->createQueryBuilder('userGroup')
            ->addSelect(['courseStream'])
            ->innerJoin('userGroup.courseStream', 'courseStream')
            ->innerJoin('courseStream.abstractCourse', 'abstractCourse')
            ->where('abstractCourse.slug in (:courseSlugs)')
            // @todo: придумать лучшее решение
            ->andWhere('courseStream.id >= 23 or courseStream.id = 19')
            ->setParameter('courseSlugs', $courses)
            ->getQuery()
            ->getResult();

        return $this->getResponseNew(['streams' => $courseStreams, 'groups' => $userGroups], ['traction:filters']);
    }

    /**
     * Сохранение значения ключевой метрики в конкретную дату
     *
     * @Rest\Post("/api/traction/set/{typeId<\d+>}/{stringDate<\d{4}\-\d{1,2}\-\d{1,2}>}", name="traction_set_value")
     *
     * @OA\Parameter(name="typeId", in="path", description="ID ключевой метрики", @OA\Schema(type="integer"))
     * @OA\Parameter(
     *     name="stringDate",
     *     in="path",
     *     description="Дата",
     *     @OA\Schema(type="string", pattern="\d{4}\-\d{1,2}\-\d{1,2}")
     * )
     *
     * @OA\Response(
     *     response=200,
     *     description="Сохранение значения ключевой метрики в конкретную дату",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *         @OA\Schema(
     *             type="object",
     *             @OA\Property(
     *                 property="status",
     *                 type="string"
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 type="array",
     *                 @OA\Items(
     *                     type="object",
     *                     @OA\Property(type="string", property="field"),
     *                     @OA\Property(type="string", property="message"),
     *                 ),
     *             ),
     *             @OA\Property(
     *                 property="data",
     *                 type="string",
     *                 enum={"ko", "ok"}
     *             ),
     *         )
     *     )
     * )
     * @OA\Response(
     *      response=400,
     *      description="Ошибка авторизации",
     * )
     *
     * @IsGranted("ROLE_USER")
     *
     * @throws Exception
     */
    public function setValueAction(Request $request, int $typeId, string $stringDate): JsonResponse
    {
        /** @var \App\Entity\Traction\ValueType|null $tractionValueType */
        $tractionValueType = $this->em->getRepository(\App\Entity\Traction\ValueType::class)->find($typeId);
        if (!$tractionValueType) {
            throw new InvalidArgumentException('Несуществующий тип');
        }

        $user = $this->getUser();
        $this->isGranted('USER_ANALYTICS_VIEW', $user);

        $date = new StringableDateTime($stringDate);
        if ($date > new DateTime()) {
            $this->addError('date', 'Нельзя заполнять будущее');
        }
        // @todo: добавить передачу курса с фронта
//        if (
//            'Mon' !== $date->format('D')
//            && '1' !== $date->format('j')
//        ) {
//            $this->addError('date', 'Некорректная дата начала периода');
//        }

        $tractionValue = $this->em->getRepository(Value::class)
            ->findOneBy([
                'owner' => $user,
                'type' => $tractionValueType,
                'date' => $date,
            ]) ?: new Value();
        $form = $this->createForm(ValueType::class, $tractionValue);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && !$this->hasErrors()) {
            $tractionValue->setDate($date);
            $tractionValue->setType($tractionValueType);
            $tractionValue->setOwner($user);

            $this->em->persist($tractionValue);
            $this->em->flush();

            return $this->getResponseNew('ok');
        }

        return $this->addFormErrors($form)->getResponseNew('ko');
    }

    /**
     * Значения ключевых меток текущего или заданного пользователя в заданный период
     *
     * @Rest\Get("/api/traction/get", name="traction_get_values")
     * @Rest\Get("/api/traction/get/{id<\d+>}", name="traction_get_user_values")
     * @ParamConverter("user", options={"mapping": {"id": "id"}})
     *
     * @Rest\QueryParam(name="startDate", requirements="\d{4}\-\d{1,2}\-\d{1,2}", description="С какой даты")
     * @Rest\QueryParam(name="endDate", requirements="\d{4}\-\d{1,2}\-\d{1,2}", description="По какую дату")
     *
     * @OA\Parameter(name="id", in="path", description="ID студента", @OA\Schema(type="integer"))
     *
     * @OA\Response(
     *     response=200,
     *     description="Значения ключевых меток текущего или заданного пользователя в заданный период",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *         @OA\Schema(
     *             type="object",
     *             @OA\Property(
     *                 property="status",
     *                 type="string"
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 type="array",
     *                 @OA\Items(
     *                     type="object",
     *                     @OA\Property(type="string", property="field"),
     *                     @OA\Property(type="string", property="message"),
     *                 ),
     *             ),
     *             @OA\Property(
     *                 property="data",
     *                 type="array",
     *                 @OA\Items(
     *                     ref=@Model(
     *                         type=Value::class,
     *                         groups={"traction"},
     *                     )
     *                 ),
     *             ),
     *         )
     *     )
     * )
     * @OA\Response(
     *      response=400,
     *      description="Ошибка авторизации",
     * )
     *
     * @IsGranted("ROLE_USER")
     *
     * @throws Exception
     */
    public function getValuesAction(ParamFetcher $paramFetcher, ?User $user = null): JsonResponse
    {
        if (!$user) {
            $user = $this->getUser();
        }

        $qb = $this->em->getRepository(Value::class)->createQueryBuilder('value')
            ->where('value.owner = :owner')
            ->setParameter('owner', $user);

        if ($startDateString = $paramFetcher->get('startDate')) {
            $startDate = new DateTime($startDateString);
            $qb->andWhere('value.date >= :startDate')
                ->setParameter('startDate', $startDate);
        }

        if ($endDateString = $paramFetcher->get('endDate')) {
            $endDate = new DateTime($endDateString);
            $qb->andWhere('value.date <= :endDate')
                ->setParameter('endDate', $endDate);
        }

        $tractionValues = $qb->getQuery()->getResult();

        return $this->getResponseNew($tractionValues, ['traction']);
    }

    /**
     * Ключевые метрики заданных курсов
     *
     * @Rest\Get("/api/traction/type/list", name="traction_type_list")
     *
     * @Rest\QueryParam(name="courses", requirements="[a-z_-]+", map=true, description="Только конкретные курсы")
     *
     * @OA\Parameter(
     *     name="courses[]",
     *     in="query",
     *     description="Только конкретные курсы",
     *     @OA\Schema(type="array", @OA\Items(type="string", pattern="[a-z_-]+", default={"speed", "hundred"}))
     * )
     *
     * @OA\Response(
     *     response=200,
     *     description="Ключевые метрики заданных курсов",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *         @OA\Schema(
     *             type="object",
     *             @OA\Property(
     *                 property="status",
     *                 type="string"
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 type="array",
     *                 @OA\Items(
     *                     type="object",
     *                     @OA\Property(type="string", property="field"),
     *                     @OA\Property(type="string", property="message"),
     *                 ),
     *             ),
     *             @OA\Property(
     *                 property="data",
     *                 type="array",
     *                 @OA\Items(
     *                     ref=@Model(
     *                         type=\App\Entity\Traction\ValueType::class,
     *                         groups={"traction"},
     *                     )
     *                 ),
     *             ),
     *         )
     *     )
     * )
     * @OA\Response(
     *      response=400,
     *      description="Ошибка авторизации",
     * )
     *
     * @IsGranted("ROLE_USER")
     *
     * @throws Exception
     */
    public function listValueTypesAction(ParamFetcher $paramFetcher): JsonResponse
    {
        $courses = $paramFetcher->get('courses');
        if (empty($courses)) {
            $courses = ['hundred', 'speed', 'speed_club'];
        }

        $types = $this->em->getRepository(\App\Entity\Traction\ValueType::class)
            ->createQueryBuilder('type')
            ->join('type.abstractCourses', 'abstractCourse')
            ->where('abstractCourse.slug in (:courseSlugs)')
            ->setParameter('courseSlugs', $courses)
            ->orderBy('type.position')
            ->getQuery()
            ->getResult();

        return $this->getResponseNew($types, ['traction']);
    }

    /**
     * Список потоков курсов, на которые был зачислен текущий или заданный студент
     *
     * @Rest\Get("/api/traction/course-stream/list", name="traction_course_stream_list")
     * @Rest\Get("/api/traction/course-stream/list/{id<\d+>}", name="traction_user_course_stream_list")
     * @ParamConverter("user", options={"mapping": {"id": "id"}})
     *
     * @Rest\QueryParam(name="courses", requirements="[a-z_-]+", map=true, description="Только конкретные курсы")
     *
     * @OA\Parameter(name="id", in="path", description="ID студента", @OA\Schema(type="integer"))
     * @OA\Parameter(
     *     name="courses[]",
     *     in="query",
     *     description="Только конкретные курсы",
     *     @OA\Schema(type="array", @OA\Items(type="string", pattern="[a-z_-]+"))
     * )
     *
     * @OA\Response(
     *     response=200,
     *     description="Список потоков курсов, на которые был зачислен текущий или заданный студент",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *         @OA\Schema(
     *             type="object",
     *             @OA\Property(
     *                 property="status",
     *                 type="string"
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 type="array",
     *                 @OA\Items(
     *                     type="object",
     *                     @OA\Property(type="string", property="field"),
     *                     @OA\Property(type="string", property="message"),
     *                 ),
     *             ),
     *             @OA\Property(
     *                 property="data",
     *                 type="array",
     *                 @OA\Items(
     *                     ref=@Model(
     *                         type=CourseStream::class,
     *                         groups={"traction", "traction:user"},
     *                     )
     *                 ),
     *             ),
     *         )
     *     )
     * )
     * @OA\Response(
     *      response=400,
     *      description="Ошибка авторизации",
     * )
     *
     * @IsGranted("ROLE_USER")
     *
     */
    public function getAvailableStreams(ParamFetcher $paramFetcher, ?User $user = null): JsonResponse
    {
        if (!$user) {
            $user = $this->getUser();
        }
        $this->isGranted('USER_ANALYTICS_VIEW', $user);

        $qb = $this->em->getRepository(CourseStream::class)
            ->createQueryBuilder('courseStream')
            ->select(['courseStream', 'userGroup', 'userRel'])
            ->innerJoin('courseStream.userGroups', 'userGroup')
            ->innerJoin('userGroup.userRelations', 'userRel')
            ->where('userRel.user = :user and userRel.deleted = 0')
            ->setParameter('user', $user);

        if ($courses = $paramFetcher->get('courses')) {
            $qb->innerJoin('courseStream.abstractCourse', 'abstractCourse')
                ->andWhere('abstractCourse.slug in (:courseSlugs)')
                ->setParameter('courseSlugs', $courses);
        }

        $streams = $qb->getQuery()->getResult();

        return $this->getResponseNew($streams, ['traction', 'traction:user']);
    }

    /**
     * Получение сформированного трекшена для пользователя
     *
     * @Rest\Get("/api/user/{user<\d+>}/traction")
     * @Rest\View(serializerGroups={"user:traction"})
     *
     * @Rest\QueryParam(name="from", requirements="^\d{4}-\d{2}$", description="Месяц начала", strict=true)
     * @Rest\QueryParam(name="to", requirements="^\d{4}-\d{2}$", description="Месяц конца", strict=true)
     *
     * @OA\Parameter(name="user", in="path", @OA\Schema(type="integer", description="id пользователя"))
     *
     * @OA\Response(response=JsonResponse::HTTP_OK, description="Массива трекшена",
     *      @OA\MediaType(mediaType="applicaion/json",
     *          @OA\Schema(type="array",
     *              @OA\Items(ref=@Model(type=UserTractionForSettlementPeriod::class, groups={"user:traction"}))
     *          )
     *      )
     * )
     * @throws Exception
     */
    public function getUserTraction(User $user, TractionService $tractionService, ParamFetcher $params): array
    {
        return $tractionService->makeByUser(
            $user,
            new DateTime($params->get('from')),
            new DateTime($params->get('to'))
        );
    }

}

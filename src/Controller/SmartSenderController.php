<?php

namespace App\Controller;

use App\Annotation\JsonApiResponse;
use App\Entity\AbstractCourse;
use App\Entity\Faculty;
use App\Entity\Questionnaire\Question;
use App\Entity\Questionnaire\UserAnswer;
use App\Entity\SmartSender\SmartSenderBot;
use App\Entity\SmartSender\SmartSenderForm;
use App\Entity\SmartSender\SmartSenderUser;
use App\Entity\Timezone;
use App\Entity\UserGroup;
use App\Service\AuthServerHelper;
use App\Service\FormHelper;
use App\Service\SmartSenderHelper;
use App\Service\SmartSenderService;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use Exception;
use FOS\RestBundle\Controller\Annotations as Rest;
use GuzzleHttp\Exception\GuzzleException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use OpenApi\Annotations as OA;

/**
 * @OA\Tag(name="SmartSender bot")
 */
class SmartSenderController extends BaseController
{
    /**
     * Получение кода для связи с ботом в СмартСендере и состояния активности
     *
     * @Rest\Get("/api/v2/smartsender/code", name="smartsender_get_code")
     * @IsGranted("ROLE_USER")
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Информация для связи с ботом",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *         @OA\Schema(type="object",
     *              @OA\Property(property="code", type="number", example="12345678"),
     *              @OA\Property(property="active", type="boolean"),
     *         )
     *     )
     * )
     *
     * @throws Exception
     */
    public function getCode(SmartSenderService $smartSender): JsonResponse
    {
        $smartSenderUser = $smartSender->getConnectedUserWithCode($this->getUser());

        return $this->json([
            'code' => $smartSenderUser->getCode(),
            'active' => !is_null($smartSenderUser->getSmartSenderUserId()),
        ]);
    }

    /**
     * Получение кода с фронта для связи с ботом в СмартСендере
     *
     * @deprecated use getCode instead todo: remove after fix front
     *
     * @Rest\Get("/api/smartsender/code", name="code")
     * @IsGranted("ROLE_USER")
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function codeAction(): JsonResponse
    {
        $user = $this->getUser();

        $smartSenderUser = $this->em->getRepository(SmartSenderUser::class)
            ->findByUserAndActualStream($user);

        if (!$smartSenderUser) {
            // ищем актуального бота
            $smartSenderBot = $this->em->getRepository(SmartSenderBot::class)->findBot($user);
            if (!$smartSenderBot) {
                return $this
                    ->addError('botGroup', "Бот не найден. Пожалуйста, обратись в поддержку")
                    ->getResponseNew();
            }

            $smartSenderUser = new SmartSenderUser();
            $smartSenderUser->setUser($user);
            $smartSenderUser->setSmartSenderBot($smartSenderBot);
            $smartSenderUser->setCode(
                sprintf(
                    "%s%s",
                    random_int(10, 99),
                    $user->getId(),
                )
            );
            $this->em->persist($smartSenderUser);
            $this->em->flush();
        }

        return $this->getResponseNew(
            [
                'code' => $smartSenderUser->getCode(),
                'active' => !is_null($smartSenderUser->getSmartSenderUserId()),
                'username' => $user->getUsername(),
            ]
        );
    }


    /**
     * Обновление юзернейма
     *
     * @Rest\Patch("/api/v2/smartsender/username", name="smartsender_change_username")
     * @IsGranted("ROLE_USER")
     *
     * @OA\RequestBody(
     *     @OA\JsonContent(@OA\Property(property="username", type="string"))
     * )
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="empty"
     * )
     *
     * @throws GuzzleException
     */
    public function updateUsername(Request $request, SmartSenderService $smartSender): JsonResponse
    {
        $smartSender->updateUsername($this->getUser(), $request->get('username'));
        return $this->json(null);
    }

    /**
     * Обновление юзернейма с фронта
     *
     * @deprecated use updateUsername instead todo: remove after fix front
     *
     * @Rest\Post("/api/smartsender/username", name="username")
     * @IsGranted("ROLE_USER")
     *
     * @OA\RequestBody(
     *     @OA\JsonContent(@OA\Property(property="username", type="string"))
     * )
     *
     * @JsonApiResponse(
     *     response=Response::HTTP_OK,
     *     description="Результат",
     *     example="null"
     * )
     *
     * @param Request $request
     * @param AuthServerHelper $authServerHelper
     * @param FormHelper $formHelper
     *
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function usernameAction(
        Request $request,
        AuthServerHelper $authServerHelper,
        FormHelper $formHelper
    ): JsonResponse {
        $user = $this->getUser();

        $smartSenderUser = $this->em->getRepository(SmartSenderUser::class)
            ->findByUserAndActualStream($user);

        if (!$smartSenderUser) {
            return $this->addError('username', 'Как ты сюда попал? Пожалуйста, обратись в поддержку')
                ->getResponseNew();
        }

        $userArray = [
            'authUserId' => $user->getAuthUserId(),
            'telegram' => $formHelper->toRealStr($request->get('username')),
            'courseName' => 'withoutEmail',
        ];

        $authServerHelper->editUserByServer($userArray);

        return $this->getResponseNew();
    }

    /**
     * Подключение бота. СмартСендер отправляет этот запрос при первом подключении
     * @Rest\Route("/api/smartsender/connect", name="connect")
     *
     * @param Request $request
     * @param SmartSenderHelper $smartSenderHelper
     * @param AuthServerHelper $authServerHelper
     *
     * @return JsonResponse
     * @throws GuzzleException
     * @throws NonUniqueResultException
     */
    public function connectAction(
        Request $request,
        SmartSenderHelper $smartSenderHelper,
        AuthServerHelper $authServerHelper
    ): JsonResponse {
        /** @var SmartSenderUser $smartSenderUser */
        $smartSenderUser = $this->em->getRepository(SmartSenderUser::class)
            ->createQueryBuilder('ssu')
            ->innerJoin('ssu.smartSenderBot', 'ssb')
            ->where('ssu.code = :userCode')
            ->andWhere('ssb.token = :smartSenderBotToken')
            ->setParameters(
                [
                    'userCode' => $request->get('userCode'),
                    'smartSenderBotToken' => $request->get('smartSenderBotToken'),
                ]
            )
            ->getQuery()
            ->getOneOrNullResult();

        if (!$smartSenderUser) {
            throw $this->createNotFoundException('Пользователь не найден');
        }

        // если уже стоял другой смартсендер-юзер, то сбрасываем его
        if (
            $smartSenderUser->getSmartSenderUserId()
            && $smartSenderUser->getSmartSenderUserId() !== $request->get('smartSenderUserId')
        ) {
            // сбрасываем предыдущее подключение
            $smartSenderHelper->triggerEventOnContact($smartSenderUser, SmartSenderHelper::EVENT_DROP_USER);
        }

        // ставим нового юзера
        $smartSenderUser->setSmartSenderUserId(
            $request->get('smartSenderUserId')
        );
        $this->em->persist($smartSenderUser);
        $this->em->flush();

        return $this->getResponseNew($this->getUserInfo($smartSenderUser, $authServerHelper));
    }

    /**
     * Получение инфы о пользователе ЛМС. СмартСендер отправляет запрос когда ему нужно
     * @Rest\Route("/api/smartsender/user", name="user")
     *
     * @param Request $request
     * @param AuthServerHelper $authServerHelper
     *
     * @return JsonResponse
     * @throws GuzzleException
     * @throws NonUniqueResultException
     */
    public function userAction(Request $request, AuthServerHelper $authServerHelper): JsonResponse
    {
        /** @var SmartSenderUser $smartSenderUser */
        $smartSenderUser = $this->em->getRepository(SmartSenderUser::class)
            ->createQueryBuilder('ssu')
            ->innerJoin('ssu.smartSenderBot', 'ssb')
            ->where('ssu.smartSenderUserId = :smartSenderUserId')
            ->andWhere('ssb.token = :smartSenderBotToken')
            ->setParameters(
                [
                    'smartSenderUserId' => $request->get('smartSenderUserId'),
                    'smartSenderBotToken' => $request->get('smartSenderBotToken'),
                ]
            )
            ->getQuery()
            ->getOneOrNullResult();

        if (!$smartSenderUser) {
            throw new NotFoundHttpException('Пользователь не найден');
        }

        return $this->getResponseNew($this->getUserInfo($smartSenderUser, $authServerHelper));
    }

    /**
     * Прием вопросов-ответов от бота
     * @Rest\Post("/api/smartsender/form", name="form")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function formAction(Request $request): JsonResponse
    {
        /** @var null|SmartSenderUser $smartSenderUser */
        $smartSenderUser = $this->em->getRepository(SmartSenderUser::class)
            ->createQueryBuilder('ssu')
            ->innerJoin('ssu.smartSenderBot', 'ssb')
            ->where('ssu.code = :userCode')
            ->andWhere('ssb.token = :smartSenderBotToken')
            ->setParameters(
                [
                    'smartSenderUserId' => $request->get('smartSenderUserId'),
                    'smartSenderBotToken' => $request->get('smartSenderBotToken'),
                ]
            )
            ->getQuery()
            ->getOneOrNullResult();

        if (!$smartSenderUser) {
            throw $this->createNotFoundException('Пользователь не найден');
        }
        $formName = $request->get('formName');

        foreach ($request->get('form') as $item) {
            $form = new SmartSenderForm();
            $form->setFormName($formName);
            $form->setSmartSenderUser($smartSenderUser);
            $form->setQuestion($item['question']);
            $form->setAnswer($item['answer']);
            $this->em->persist($form);
        }
        $this->em->flush();

        return $this->getResponseNew();
    }

    /**
     * Редирект в чат-бота
     * @Rest\Get("/api/smartsender/redirect", name="redirect")
     *
     * @OA\Response(
     *     response=Response::HTTP_FOUND,
     *     headers={@OA\Header(header="Location", @OA\Schema(type="string"), description="URL для перехода")},
     *     description="redirect response"
     * )
     */
    public function redirectAction(): RedirectResponse
    {
        $user = $this->getUser();

        /** @var null|SmartSenderUser $smartSenderUser */
        $smartSenderUser = $this->em->getRepository(SmartSenderUser::class)
            ->findByUserAndActualStream($user);

        $url = 'https://t.me/likecentre_speed_bot?start=ZGw6MzcyMTQ';
        if (
            $smartSenderUser
            && $smartSenderUser->getSmartSenderBot()->getAbstractCourse()->getSlug() === AbstractCourse::SLUG_HUNDRED
        ) {
            $url = 'https://t.me/likecentre_sotka_bot?start=ZGw6MzczOTM';
        }

        return new RedirectResponse($url);
    }

    /**
     * @throws GuzzleException
     * @throws NonUniqueResultException
     */
    private function getUserInfo(SmartSenderUser $smartSenderUser, AuthServerHelper $authServerHelper): array
    {
        $botSlug = $smartSenderUser->getSmartSenderBot()->getAbstractCourse()->getSlug();
        $user = $smartSenderUser->getUser();
        $authUser = $authServerHelper->getUsersInfo([$user->getAuthUserId()])[0];

        /** @var Timezone $timezone */
        $timezone = $this->em->getRepository(Timezone::class)->find($authUser->timezoneId);

        /** @var Question[] $questions */
        $questions = $this->em->getRepository(Question::class)->findWithVariants();
        $answers = $this->em->getRepository(UserAnswer::class)->getArrayByUser($user);

        $data = [];
        foreach ($questions as $question) {
            $userAnswer = $answers[$question->getId()] ?? '';
            if (!empty($userAnswer) && !$question->getVariants()->isEmpty()) {
                foreach ($question->getVariants() as $variant) {
                    if ($variant->getId() === $userAnswer) {
                        $userAnswer = $variant->getTitle();
                        break;
                    }
                }
            }

            $data[$question->getSlug()] = $userAnswer;
        }

        $product = $package = '';
        $groups = $this->em->getRepository(UserGroup::class)->getUserGroupsInActiveCourseStream($user);
        foreach ($groups as $group) {
            $groupName = $group['name'];

            if ($group['activeStream'] && $group['courseSlug'] === $botSlug) {
                $words = explode('(', $groupName);
                $product = trim(reset($words));

                preg_match('#\((.*?)\)#', $groupName, $match);
                if (isset($match[1])) {
                    $package = $match[1];
                }
            }
        }

        // ищем факультет
        $facultyName = $groupName = '';

        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(Faculty::class)->createQueryBuilder('f');
        /** @var null|Faculty $faculty */
        $faculty = $qb
            ->innerJoin('f.userGroupUsers', 'ugu')
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ug.courseStream', 'cs')
            ->innerJoin('cs.abstractCourse', 'ac')
            ->andWhere('ugu.user = :user')
            ->andWhere('ugu.deleted = 0')
            ->andWhere('ac.slug = :slug')
            ->andWhere('cs.active = 1')
            ->setParameter('user', $user)
            ->setParameter('slug', $botSlug)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        if ($faculty) {
            if ($botSlug === AbstractCourse::SLUG_HUNDRED) {
                $groupName = $faculty->getName();
            } else {
                $facultyName = $faculty->getName();
            }
        }

        return array_merge(
            $data,
            [
                'name' => $authUser->name,
                'patronymic' => $authUser->patronymic,
                'lastname' => $authUser->lastname,
                'email' => $authUser->email,
                'phone' => $authUser->phone,
                'city' => $authUser->city,
                'niche' => $authUser->niche,
                'timezone' => $timezone->getName(),
                'product' => $product,
                'package' => $package,
                'faculty' => $facultyName,
                'group' => $groupName,
            ]
        );
    }

}

<?php

namespace App\Controller;

use App\Entity\BusinessNiche;
use App\Entity\BusinessNicheComment;
use App\Entity\User;
use App\Form\BusinessNicheCommentType;
use App\Form\BusinessNicheType;
use App\Repository\BusinessAreaRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\BusinessArea;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @OA\Tag(name="Направления бизнеса")
 * @IsGranted("ROLE_USER")
 */
final class BusinessNicheController extends BaseController
{
    /**
     * Список сфер бизнеса
     *
     * @Rest\Get("/api/v2/business/area")
     * @Rest\View()
     *
     * @OA\Response(response=Response::HTTP_OK, description="массив объектов сфер бизнеса",
     *     @OA\JsonContent(type="array", @OA\Items(ref=@Model(type=BusinessArea::class)))
     * )
     */
    public function getBusinessAreas(BusinessAreaRepository $businessAreaRepository): array
    {
        return $businessAreaRepository->findAll();
    }

    /**
     * Список возможных типов ведения бизнеса
     *
     * @Rest\Get("/api/v2/business/type")
     * @Rest\View()
     *
     * @OA\Response(response=Response::HTTP_OK, description="Список типов в формате ключ => текстовое название",
     *     @OA\JsonContent(type="object", example=BusinessNiche::TYPES)
     * )
     */
    public function getBusinessTypes(TranslatorInterface $translator): array
    {
        $translations = array_map(static function ($key) use ($translator) {
            return $translator->trans('business.type.' . $key);
        }, BusinessNiche::TYPES);

        return array_combine(BusinessNiche::TYPES, $translations);
    }

    /**
     * Создать пользователю новую нишу
     *
     * @Rest\Post("/api/v2/business/niche")
     * @Rest\View(serializerGroups={"business:niche"}, statusCode=Response::HTTP_CREATED)
     *
     * @OA\RequestBody(@Model(type=BusinessNicheType::class))
     * @OA\Response(response=Response::HTTP_CREATED, description="Возвращает созданную сущность",
     *     @Model(type=BusinessNiche::class, groups={"business:niche"})
     * )
     */
    public function addNiche(EntityManagerInterface $em, Request $request): BusinessNiche
    {
        $niche = (new BusinessNiche())->setUser($this->getUser());
        $this->fillByForm(BusinessNicheType::class, $request, $niche);
        $em->persist($niche);
        $em->flush();

        return $niche;
    }

    /**
     * Редактировать нишу
     *
     * @Rest\Patch("/api/v2/business/niche/{niche}")
     * @Rest\View(serializerGroups={"business:niche"})
     *
     * @OA\RequestBody(@Model(type=BusinessNicheType::class))
     * @OA\Response(response=Response::HTTP_OK, description="Возвращает измененную сущность",
     *     @Model(type=BusinessNiche::class, groups={"business:niche"})
     * )
     *
     * @Security("user === niche.getUser() or is_granted('USER_HAS_ADMIN_PERMISSION')")
     */
    public function editNiche(BusinessNiche $niche, Request $request, EntityManagerInterface $em): BusinessNiche
    {
        $this->fillByForm(BusinessNicheType::class, $request, $niche);
        $em->flush();

        return $niche;
    }

    /**
     * Удалить нишу
     *
     * @Rest\Delete("/api/v2/business/niche/{niche}")
     * @Rest\View()
     *
     * @OA\Response(response=Response::HTTP_OK, description="")
     *
     * @Security("user === niche.getUser()")
     */
    public function deleteNiche(BusinessNiche $niche, EntityManagerInterface $em): bool
    {
        $em->remove($niche);
        $em->flush();

        return true;
    }

    /**
     * Добавить комментарий к нише
     *
     * @Rest\Post("/api/v2/business/niche/{niche<\d+>}/comment")
     * @Rest\View(serializerGroups={"business:niche"}, statusCode=Response::HTTP_CREATED)
     *
     * @OA\RequestBody(@Model(type=BusinessNicheCommentType::class))
     * @OA\Response(response=Response::HTTP_CREATED, description="Возвращает созданную сущность",
     *     @Model(type=BusinessNicheComment::class, groups={"business:niche"})
     * )
     *
     * @IsGranted("USER_HAS_ADMIN_PERMISSION")
     */
    public function addNicheComment(
        BusinessNiche $niche,
        Request $request,
        EntityManagerInterface $em
    ): BusinessNicheComment {
        $comment = (new BusinessNicheComment())->setUser($this->getUser())->setNiche($niche);
        $this->fillByForm(BusinessNicheCommentType::class, $request, $comment);
        $em->persist($comment);
        $em->flush();

        return $comment;
    }

    /**
     * Удалить комментарий к нише
     *
     * @Rest\Delete("/api/v2/business/niche/comment/{comment}")
     * @Rest\View(serializerGroups={"business:niche"}, statusCode=Response::HTTP_OK)
     *
     * @OA\Response(response=Response::HTTP_OK, description="")
     *
     * @IsGranted("USER_HAS_ADMIN_PERMISSION")
     */
    public function deleteNicheComment(BusinessNicheComment $comment, EntityManagerInterface $em): bool
    {
        $em->remove($comment);
        $em->flush();

        return true;
    }

    /**
     * Получить нишу по id
     *
     * @Rest\Get("/api/v2/business/niche/{niche<\d+>}")
     * @Rest\View(serializerGroups={"business:niche"}, statusCode=Response::HTTP_OK)
     *
     * @OA\Response(response=Response::HTTP_OK, description="Возвращает сущность",
     *     @Model(type=BusinessNiche::class, groups={"business:niche"})
     * )
     */
    public function getNiche(BusinessNiche $niche): BusinessNiche
    {
        return $niche;
    }

    /**
     * Список ниш пользователя
     *
     * @Rest\Get("/api/v2/user/{user}/business/niche")
     * @Rest\View(serializerGroups={"business:niche"}, statusCode=Response::HTTP_OK)
     *
     * @OA\Response(response=Response::HTTP_OK, description="Возвращает список сущностей",
     *     @OA\JsonContent(type="array", @OA\Items(ref=@Model(type=BusinessNiche::class, groups={"business:niche"})))
     * )
     */
    public function getUserNiches(User $user)
    {
        return $user->getBusinessNiches();
    }

}

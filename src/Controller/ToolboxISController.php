<?php

namespace App\Controller;

use App\Entity\AbstractCourse;
use App\Entity\CourseStream;
use App\Service\AuthServerHelper;
use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use FOS\RestBundle\Controller\Annotations as Rest;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * @Rest\Route("/api/toolbox-is", name="toolbox_is_")
 */
class ToolboxISController extends BaseController
{
    /**
     * Получение дат признания продуктов по Скорости и Сотке
     * @Rest\Get("/recognition-dates/speed-hundred", name="recognition_dates_speed_hundred")
     */
    public function getRecognitionSpeedHundredProductDatesAction(Request $request): JsonResponse
    {
        $products = $request->request->all();

        // количество дней, после которого считаем услугу оказанной
        $days = [
            AbstractCourse::SLUG_SPEED => 6 * 7,
            AbstractCourse::SLUG_SPEED_CLUB => 6 * 7,
            AbstractCourse::SLUG_HUNDRED => 6 * 7,
        ];

        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(CourseStream::class)->createQueryBuilder('cs');
        $rows = $qb
            ->select('cs.name', 'ac.slug', 'cs.startDate')
            ->innerJoin('cs.abstractCourse', 'ac')
            ->where('cs.name IN(:products)')
            ->andWhere('cs.startDate IS NOT NULL')
            ->andWhere('ac.slug IN(:slugs)')
            ->setParameters(
                [
                    'products' => $products,
                    'slugs' => array_keys($days),
                ]
            )
            ->getQuery()
            ->getResult();

        $response = [];
        foreach ($rows as $row) {
            /** @var DateTime $date */
            $date = $row['startDate'];
            $response[$row['name']] = $this->addDays($date, $days[$row['slug']]);
        }

        $badProducts = [];
        foreach ($products as $product) {
            if (!isset($response[$product])) {
                // для потеряшек ставим 45 дней,
                // этого будет достаточно для ситуации, когда продали со стартом в следующем потоке
                $response[$product] = $this->addDays(new DateTime(), 45);
                $badProducts[] = $product;
            }
        }

        if (!empty($badProducts)) {
            $this->logger->error('Toolbox IS: не нашли продукты для признания', $badProducts);
        }

        return $this->getResponseNew($response);
    }

    /**
     * Получение дат признания сделок по МсА
     * @Rest\Get("/recognition-dates/msa", name="recognition_dates_msa")
     */
    public function getRecognitionMsaProductDatesAction(
        Request $request,
        AuthServerHelper $authServerHelper
    ): JsonResponse {
        $emails = array_map(
            static function (string $email) {
                return mb_strtolower(trim($email));
            },
            $request->request->all()
        );

        // ищем юзеров по емейлам
        $emailsString = $authServerHelper->implode($emails);
        $authUsers = $authServerHelper->getUsers([], ['id', 'email'], "u.email IN({$emailsString})");
        $users = [];
        foreach ($authUsers as $authUser) {
            $users[$authUser['id']] = $authUser['email'];
        }

        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(CourseStream::class)->createQueryBuilder('cs');
        $rows = $qb
            ->select('u.id', 'cs.startDate')
            ->innerJoin('cs.userGroups', 'ug')
            ->innerJoin('ug.userRelations', 'ugu')
            ->innerJoin('cs.abstractCourse', 'ac')
            ->innerJoin('ugu.user', 'u')
            ->where('ugu.deleted = 0')
            ->andWhere('cs.startDate IS NOT NULL')
            ->andWhere('ac.slug = :slug')
            ->andWhere('u.id IN(:ids)')
            ->setParameters(
                [
                    'ids' => array_keys($users),
                    'slug' => AbstractCourse::SLUG_MSA,
                ]
            )
            ->getQuery()
            ->getResult();

        $response = [];
        foreach ($rows as $row) {
            /** @var DateTime $date */
            $date = $row['startDate'];
            $email = $users[$row['id']];
            // МсА признаем через 45 дней
            $response[$email] = $this->addDays($date, 45);
        }

        // добавляем потеряшек
        $badEmails = [];
        // для потеряшек ставлю 90 дней,
        // этого будет достаточно для ситуации, когда продали со стартом в следующем потоке
        $recognitionDdl = $this->addDays(new DateTime(), 90);
        foreach ($emails as $email) {
            if (!isset($response[$email])) {
                $response[$email] = $recognitionDdl;
                $badEmails[] = $email;
            }
        }

        if (!empty($badEmails)) {
            $this->logger->error('Toolbox IS: не нашли даты признания МсА', $badEmails);
        }

        return $this->getResponseNew($response);
    }

    private function addDays(DateTime $date, int $days): string
    {
        return $date
            ->add(new DateInterval("P{$days}D"))
            ->format('Y-m-d');
    }

}

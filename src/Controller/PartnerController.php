<?php

namespace App\Controller;

use App\Service\UserHelper;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use App\Entity\Partner;
use App\Form\PartnerType;
use App\Repository\PartnerRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @OA\Tag(name="Партнеры")
 *
 * @IsGranted("ROLE_USER")
 */
class PartnerController extends BaseController
{
    /**
     * Создание нового партнера
     * @Rest\Post("/api/partner", name="partners_create")
     *
     * @OA\RequestBody(@Model(type=PartnerType::class))
     *
     * @OA\Response(response=Response::HTTP_CREATED, description="Добавить партнера",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *         @OA\Schema(type="object", ref=@Model(type=Partner::class,groups={"partner:info"}))
     *     )
     * )
     *
     * @param Request $request
     * @param UserHelper $userHelper
     *
     * @return JsonResponse
     */
    public function create(Request $request, UserHelper $userHelper): JsonResponse
    {
        if ($userHelper->isUserAdmin()) {
            $partner = $this->fillByForm(PartnerType::class, $request, new Partner());
            $this->em->persist($partner);
            $this->em->flush();

            return $this->json($partner, Response::HTTP_CREATED, [], ['groups' => ['partner:info']]);
        }
        throw new AccessDeniedHttpException();
    }

    /**
     * Получение списка партнеров
     * @Rest\Get("/api/partner", name="partners_list")
     *
     * @OA\Parameter(name="page", in="query", @OA\Schema(type="integer", default=1))
     * @OA\Parameter(name="pageSize", in="query", @OA\Schema(type="integer", default=10))
     *
     * @OA\Response(response=Response::HTTP_OK, description="Добавить партнера",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *         @OA\Schema(type="array",  @OA\Items(ref=@Model(type=Partner::class,groups={"partner:info"})))
     *     )
     * )
     *
     * @param Request $request
     * @param PartnerRepository $partnerRepository
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function list(Request $request, PartnerRepository $partnerRepository): JsonResponse
    {
        // @todo: переделать на рест-парам
        $page = (int)$request->query->get('page', 1);
        $pageSize = (int)$request->query->get('pageSize', 10);

        $queryBuilder = $partnerRepository->createQueryBuilder('p')
            ->where('p.deleted = 0')
            ->setFirstResult(($page - 1) * $pageSize)
            ->setMaxResults($pageSize);
        $data = (new Paginator($queryBuilder->getQuery()))->getIterator();

        return $this->json($data, Response::HTTP_OK, [], ['groups' => ['partner:info']]);
    }

    /**
     * Удаление партнера
     * @Rest\Delete("/api/partner/{partner<\d+>}", name="parnters_delete")
     *
     * @OA\Parameter(name="partner", in="path", @OA\Schema(type="integer"), description="id партнера")
     *
     * @OA\Response(response=Response::HTTP_OK, description="Добавить партнера",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *         @OA\Schema(type="object", ref=@Model(type=Partner::class,groups={"partner:info"}))
     *     )
     * )
     */
    public function delete(Partner $partner, UserHelper $userHelper): JsonResponse
    {
        if ($userHelper->isUserAdmin()) {
            $partner->setDeleted(true);
            $this->em->persist($partner);
            $this->em->flush();
            return $this->json($partner, Response::HTTP_OK, [], ['groups' => ['partner:info']]);
        }
        throw new AccessDeniedHttpException();
    }

    /**
     * Редактирование партнера
     * @Rest\Post("/api/partner/{partner<\d+>}", name="partners_edit")
     *
     * @OA\Parameter(name="partner", in="path", @OA\Schema(type="integer"), description="id партнера")
     *
     * @OA\RequestBody(@Model(type=PartnerType::class))
     *
     * @OA\Response(response=Response::HTTP_OK, description="Редактировать партнера",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *         @OA\Schema(type="object", ref=@Model(type=Partner::class,groups={"partner:info"}))
     *     )
     * )
     *
     * @param Partner $partner
     * @param Request $request
     * @param UserHelper $userHelper
     *
     * @return JsonResponse
     */
    public function edit(Partner $partner, Request $request, UserHelper $userHelper): JsonResponse
    {
        if ($userHelper->isUserAdmin()) {
            $partner = $this->fillByForm(PartnerType::class, $request, $partner);
            $this->em->flush();

            return $this->json($partner, Response::HTTP_OK, [], ['groups' => ['partner:info']]);
        }
        throw new AccessDeniedHttpException();
    }
}

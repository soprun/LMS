<?php

namespace App\Controller;

use App\Entity\BusinessPhoto;
use App\Entity\Timezone;
use App\Entity\User;
use App\Form\ImageType;
use App\Service\FileUploaderService;
use App\Service\UserHelper;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use League\Flysystem\FileExistsException;
use League\Flysystem\FileNotFoundException;
use League\Flysystem\FilesystemInterface;
use RuntimeException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

class UserController extends BaseController
{

    /**
     * Получение пользователя
     * @Route("/user/info/get", name="app_user_info_get", methods={"GET"})
     *
     * @param UserHelper $userHelper
     *
     * @return JsonResponse
     *
     * @IsGranted("ROLE_USER")
     */
    public function getUserInfo(UserHelper $userHelper)
    {
        return $userHelper->getUserInfo();
    }

    /**
     * Получение пользователя
     * @Route("/user/settings", name="app_user_settings", methods={"GET"})
     *
     */
    public function getUserSettings(UserHelper $userHelper)
    {
        return $this->render('lms_vue.html.twig');
    }

    /**
     * Смена пароля
     * @Route("/user/settings/resetpassword", name="reset_password", methods={"GET"})
     *
     */
    public function getUserResetPassword()
    {
        return $this->render('lms_vue.html.twig');
    }

    /**
     * Изменение статуса присутствия пользователя на курсе: онлайн или офлайн
     * @Route("/user/group/attendance/type/set", name="app_user_group_attendance_type_set", methods={"POST"})
     * @IsGranted("ROLE_USER")
     *
     * @param Request $request
     * @param UserHelper $userHelper
     *
     * @return JsonResponse
     */
    public function setUserGroupAttendanceType(Request $request, UserHelper $userHelper)
    {
        return $userHelper->setUserGroupAttendanceType($request);
    }

    /**
     * Получение таймзон
     * @Route("/user/timezones/get", name="app_user_timezones_get", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function getTimezones(EntityManagerInterface $em)
    {
        $timezones = $em->getRepository(Timezone::class)->findAll();
        $data['timezones'] = array_map(
            function (Timezone $timezone) {
                return [
                    'id' => $timezone->getId(),
                    'name' => $timezone->getName(),
                ];
            },
            $timezones
        );

        return $this->getResponse('getTimezones', $data);
    }

    /**
     * @param Request $request
     * @param FileUploaderService $fileUploader
     *
     * @return JsonResponse
     * @throws \ImagickException
     * @deprecated
     *
     * @Rest\Post("/api/business-photo/upload", name="v1_business_photo_upload")
     *
     */
    public function uploadBusinessPhotoActionV1(Request $request, FileUploaderService $fileUploader): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        if ($user->getBusinessPhotos()->count() > 3) {
            return $this->addError('image', 'Достигнуто максимальное количество фотографий')
                ->getResponseNew('ko');
        }

        $form = $this->createForm(ImageType::class, []);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            /** @var UploadedFile $uploadedPhoto */
            $uploadedPhoto = $data['image'];
            $im = new \Imagick($uploadedPhoto->getPathname());

            $photo = new BusinessPhoto();
            $photo->setOwner($user);
            $photo->setWidth($im->getImageWidth());
            $photo->setHeight($im->getImageHeight());

            // @todo: переделать на отложенную загрузку
            $filename = $fileUploader->uploadFile($uploadedPhoto, 'business-photo', true);
            $photo->setFilename($filename);

            $this->em->persist($photo);
            $this->em->flush();

            $fileUploader->prepareFile($photo, 'business-photo');

            return $this->getResponseNew($photo, ['business-photo']);
        }

        return $this->getResponseNew('ko');
    }

    /**
     * @param FileUploaderService $fileUploader
     * @param BusinessPhoto $photo
     *
     * @return JsonResponse
     * @throws FileNotFoundException
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     * @deprecated
     *
     * @Rest\Get("/api/business-photo/delete/{id<\d+>}", name="v1_business_photo_delete")
     * @ParamConverter("photo", options={"mapping": {"id": "id"}})
     *
     */
    public function deleteBusinessPhotoActionV1(FileUploaderService $fileUploader, BusinessPhoto $photo): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        if ($photo->getOwner() !== $user) {
            throw $this->createNotFoundException();
        }

        $fileUploader->deleteFile(['business-photo', $photo->getFilename()]);
        $this->em->remove($photo);
        $this->em->flush();

        return $this->getResponseNew('ok');
    }

    /**
     * @param FileUploaderService $fileUploader
     * @param null|User $user
     *
     * @return JsonResponse
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     * @deprecated
     *
     * @Rest\Get("/api/business-photo/list")
     * @Rest\Get("/api/business-photo/list/{id<\d+>}", name="v1_business_photo_list")
     * @ParamConverter("user", options={"mapping": {"id": "id"}})
     *
     */
    public function listBusinessPhotoActionV1(FileUploaderService $fileUploader, ?User $user = null): JsonResponse
    {
        if (!$user) {
            $user = $this->getUser();
        }

        foreach ($user->getBusinessPhotos() as $businessPhoto) {
            $fileUploader->prepareFile($businessPhoto, 'business-photo');
        }

        return $this->getResponseNew($user->getBusinessPhotos(), ['business-photo']);
    }

    /**
     * Загрузка фотографии
     *
     * @Rest\Post("/api/v2/business-photo", name="business_photo_upload")
     * @Rest\FileParam(
     *     name="image",
     *     requirements={
     *         "detectCorrupted": true,
     *         "mimeTypes": {"image/gif", "image/jpeg", "image/pjpeg", "image/png"}
     *     },
     *     image=true
     * )
     * @Rest\View(serializerGroups={"business-photo"})
     *
     * @OA\RequestBody(@OA\MediaType(mediaType="multipart/form-data",
     *     @OA\Schema(@OA\Property(property="image", type="binary", description="Фотография"))))
     *
     * @OA\Response(response=Response::HTTP_OK, description="Успешная загрузка",
     *     @OA\MediaType(mediaType="application/json",
     *         @OA\Schema(type="object", ref=@Model(type=BusinessPhoto::class,groups={"business-photo"}))
     *     )
     * )
     *
     * @param ParamFetcher $paramFetcher
     * @param FileUploaderService $fileUploader
     *
     * @return BusinessPhoto
     * @throws \ImagickException
     */
    public function uploadBusinessPhotoAction(
        ParamFetcher $paramFetcher,
        FileUploaderService $fileUploader
    ): BusinessPhoto {
        $user = $this->getUser();
        if ($user->getBusinessPhotos()->count() > 3) {
            throw new \InvalidArgumentException('Достигнуто максимальное количество фотографий');
        }

        /** @var UploadedFile $uploadedPhoto */
        $uploadedPhoto = $paramFetcher->get('image');
        $im = new \Imagick($uploadedPhoto->getPathname());

        $photo = new BusinessPhoto();
        $photo->setOwner($user);
        $photo->setWidth($im->getImageWidth());
        $photo->setHeight($im->getImageHeight());

        // @todo: переделать на отложенную загрузку
        $filename = $fileUploader->uploadFile($uploadedPhoto, 'business-photo', true);
        $photo->setFilename($filename);

        $this->em->persist($photo);
        $this->em->flush();

        $fileUploader->prepareFile($photo, 'business-photo');

        return $photo;
    }

    /**
     * Удаление фотографии
     *
     * @Rest\Delete("/api/v2/business-photo/{photoId<\d+>}", name="business_photo_delete")
     * @ParamConverter("photo", options={"mapping": {"photoId": "id"}})
     * @Security("user === photo.getOwner()", statusCode=Response::HTTP_NOT_FOUND)
     *
     * @OA\Parameter(name="photoId", in="path", @OA\Schema(type="number"))
     * @OA\Response(response=Response::HTTP_OK, description="Удаление фотографии")
     * @OA\Response(response=Response::HTTP_NOT_FOUND, description="Фотография не найдена")
     *
     * @param FileUploaderService $fileUploader
     * @param BusinessPhoto $photo
     */
    public function deleteBusinessPhotoAction(FileUploaderService $fileUploader, BusinessPhoto $photo): View
    {
        $fileUploader->deleteFile(['business-photo', $photo->getFilename()]);
        $this->em->remove($photo);
        $this->em->flush();

        return $this->view();
    }

    /**
     * Получение фотографий бизнеса
     *
     * @Rest\Get("/api/v2/business-photo")
     * @Rest\Get("/api/v2/business-photo/{userId<\d+>}", name="business_photo_list")
     * @ParamConverter("user", options={"mapping": {"userId": "authUserId"}})
     * @Rest\View(serializerGroups={"business-photo"})
     *
     * @OA\Parameter(name="userId", in="path", @OA\Schema(type="number"))
     * @OA\Response(response=Response::HTTP_OK, description="Получение фотографий бизнеса",
     *     @OA\MediaType(mediaType="application/json",
     *         @OA\Schema(type="array",
     *             @OA\Items(type="object", ref=@Model(type=BusinessPhoto::class,groups={"business-photo"})),
     *         )
     *     )
     * )
     *
     * @param FileUploaderService $fileUploader
     * @param null|User $user
     *
     * @return BusinessPhoto[]|Collection
     */
    public function listBusinessPhotoAction(FileUploaderService $fileUploader, ?User $user = null): Collection
    {
        if (!$user) {
            $user = $this->getUser();
        }

        foreach ($user->getBusinessPhotos() as $businessPhoto) {
            $fileUploader->prepareFile($businessPhoto, 'business-photo');
        }

        return $user->getBusinessPhotos();
    }

}

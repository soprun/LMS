<?php

namespace App\Controller;

use App\Entity\AbstractCourse;
use App\Entity\CourseStream;
use App\Entity\Faculty;
use App\Entity\User;
use App\Entity\UserGroupUser;
use App\Form\FacultyType;
use App\Repository\FacultyRepository;
use App\Service\SmartSenderHelper;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Exception;

/**
 * @Route("/api/faculty", name="faculty_")
 */
class FacultyController extends BaseController
{
    private $smartSenderHelper;
    private $paginator;

    public function __construct(
        EntityManagerInterface $em,
        NormalizerInterface $normalizer,
        SmartSenderHelper $smartSenderHelper,
        PaginatorInterface $paginator
    ) {
        parent::__construct($em, $normalizer);
        $this->paginator = $paginator;
        $this->smartSenderHelper = $smartSenderHelper;
    }

    /**
     * Установка факультета переходом по ссылке и админом.
     * Ссылка для перехода указывается в уроках через редактор.
     * @Route("/set/{facultyId}", name="set")
     * @ParamConverter("faculty", options={"mapping": {"facultyId": "id"}})
     *
     * @IsGranted("ROLE_USER")
     *
     * @param Faculty $faculty
     * @param Request $request
     *
     * @return RedirectResponse
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function setAction(Faculty $faculty, Request $request): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        // определяем юзера для администратора
        $isAdmin = false;
        if ($userId = $request->get('user')) {
            if (!$this->isGranted("TEAMS_ACCESS")) {
                throw new AccessDeniedHttpException();
            }

            $user = $this->em->getRepository(User::class)->find($userId);
            if (!$user) {
                throw new NotFoundHttpException();
            }

            $isAdmin = true;
        }

        // проверяем что участник состоит в группе с факультетами по актуальным курсам
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(UserGroupUser::class)->createQueryBuilder('ugu');
        /** @var null|UserGroupUser $userGroupUser */
        $userGroupUser = $qb
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ug.courseStream', 'cs')
            ->innerJoin('cs.abstractCourse', 'ac')
            ->andWhere('ugu.user = :user')
            ->andWhere('ugu.deleted = 0')
            ->andWhere('ac.slug IN(:slugs)')
            ->andWhere("ug.name NOT LIKE '%факультет%' OR ug.name NOT LIKE '%сборная%'")
            ->andWhere('cs.active = 1')
            ->setParameters(
                [
                    'slugs' => [
                        AbstractCourse::SLUG_HUNDRED,
                        AbstractCourse::SLUG_SPEED,
                        AbstractCourse::SLUG_SPEED_CLUB,
                    ],
                    'user' => $user,
                ]
            )
            ->setMaxResults(1)
            ->orderBy('cs.stream', 'DESC')
            ->getQuery()
            ->getOneOrNullResult();

        if (!$userGroupUser) {
            return $this->redirect(
                $this->generateUrl('app_courses_folder_list')
                . '?flashMessage=Нет прав для выбора факультета или сборной :('
            );
        }

        // не даем сменить факультет самостоятельно
        if (!$isAdmin && $userGroupUser->getFaculty()) {
            return $this->redirect(
                $this->generateUrl('app_courses_folder_list') . '?flashMessage=Ты уже выбрал факультет или сборную :('
            );
        }

        // устанавливаем факультет
        $userGroupUser->setFaculty($faculty);
        $this->em->persist($userGroupUser);
        $this->em->flush();

        // устанавливаем доступы в группы факультетов
        $this->updateFacultyGroups($user, $faculty);

        // определяем тип
        if ($faculty->getCourseStream()->getAbstractCourse()->getSlug() === AbstractCourse::SLUG_HUNDRED) {
            $courseType = Faculty::TYPE_HUNDRED;
            $facultyType = 'group';
        } else {
            $courseType = Faculty::TYPE_SPEED;
            $facultyType = 'faculty';
        }

        // обновляем юзера в боте
        $this->updateSmartSenderUser($user, $faculty, $courseType);

        if ($isAdmin) {
            return $this->getResponse('setAction');
        }

        // определяем редирект
        $url = $faculty->getRedirectUrl() ?? $this->generateUrl('app_courses_folder_list');
        $url .= '?set=' . $facultyType;

        return $this->redirect($url);
    }

    /**
     * Список доступных факультетов для пользователя
     * @Route("/list-for-user/{id}", name="list")
     * @ParamConverter("user", options={"mapping": {"id": "id"}})
     * @IsGranted("TEAMS_ACCESS")
     *
     * @param User $user
     *
     * @return JsonResponse
     */
    public function listForUserAction(User $user): JsonResponse
    {
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(Faculty::class)->createQueryBuilder('f');
        $faculties = $qb
            ->select('f.id', 'f.name AS name', 'cs.name AS courseName', 'ac.slug')
            ->innerJoin('f.courseStream', 'cs')
            ->innerJoin('cs.abstractCourse', 'ac')
            ->innerJoin('cs.userGroups', 'ug')
            ->innerJoin('ug.userRelations', 'ugu')
            ->where('cs.active = 1')
            ->andWhere('ugu.user = :user')
            ->andWhere('ugu.deleted = 0')
            ->andWhere('ac.slug IN(:slugs)')
            ->setParameters(
                [
                    'slugs' => [
                        AbstractCourse::SLUG_SPEED,
                        AbstractCourse::SLUG_SPEED_CLUB,
                        AbstractCourse::SLUG_HUNDRED,
                    ],
                    'user' => $user,
                ]
            )
            ->groupBy('f.id')
            ->getQuery()
            ->getResult();

        $type = 'faculty';
        if (isset($faculties[0]) && $faculties[0]['slug'] === 'hundred') {
            $type = 'group';
        }

        return $this->getResponse(
            'listForUserAction',
            [
                'faculties' => $faculties,
                'type' => $type,
            ]
        );
    }

    private function updateSmartSenderUser(User $user, Faculty $faculty, string $type): void
    {
        // @todo: мб стоит обновлять только того бота, тип которого передан (Скорость, Сотка)
        if ($type === Faculty::TYPE_HUNDRED) {
            $contact = [
                'faculty' => '',
                'group' => $faculty->getName(),
            ];
            $event = SmartSenderHelper::EVENT_GROUP_UPDATE;
        } else {
            $contact = [
                'faculty' => $faculty->getName(),
                'group' => '',
            ];
            $event = SmartSenderHelper::EVENT_FACULTY_UPDATE;
        }

        foreach ($user->getSmartSenderUsers() as $smartSenderUser) {
            if (
                $smartSenderUser->getSmartSenderUserId()
                // у ботов появились абстрактные курсы когда боты сами стали "1 курс = 1 бот"
                // проверка стремная, но пока пойдет
                && $smartSenderUser->getSmartSenderBot()->getAbstractCourse()
            ) {
                $this->smartSenderHelper->updateContact($smartSenderUser, $contact);
                $this->smartSenderHelper->triggerEventOnContact($smartSenderUser, $event);
            }
        }
    }

    private function updateFacultyGroups(User $user, Faculty $faculty): void
    {
        // удаляем из всех групп факультетов потока
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(UserGroupUser::class)->createQueryBuilder('ugu');
        /** @var UserGroupUser[] $userGroupUsers */
        $userGroupUsers = $qb
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ug.courseStream', 'cs')
            ->where('cs = :courseStream')
            ->andWhere('ugu.user = :user')
            ->andWhere('ugu.deleted = 0')
            ->andWhere("ug.name LIKE '%факультет%' OR ug.name LIKE '%сборная%'")
            ->setParameters(
                [
                    'courseStream' => $faculty->getCourseStream(),
                    'user' => $user,
                ]
            )
            ->getQuery()
            ->getResult();

        foreach ($userGroupUsers as $userGroupUser) {
            $userGroupUser->setDeleted(true);
            $this->em->persist($userGroupUser);
        }

        // добавляем в группу факультета
        /** @var null|UserGroupUser $userGroupUser */
        $userGroupUser = $this->em->getRepository(UserGroupUser::class)->findOneBy(
            [
                'user' => $user,
                'userGroup' => $faculty->getUserGroup(),
            ]
        );

        if ($userGroupUser && $userGroupUser->isDeleted()) {
            $userGroupUser->setDeleted(false);
            $this->em->persist($userGroupUser);
        }

        if (!$userGroupUser) {
            $userGroupUser = (new UserGroupUser())
                ->setUserGroup($faculty->getUserGroup())
                ->setUser($user);
            $this->em->persist($userGroupUser);
        }

        $this->em->flush();
    }

    /**
     * Список доступных факультетов для потока
     * @Route("/available-faculties/{id}", methods={"GET"})
     *
     * @param CourseStream $stream
     *
     * @return JsonResponse
     * @IsGranted("ROLE_USER")
     */
    public function listFacultiesForStream(CourseStream $stream): JsonResponse
    {
        $faculty = $this->em->getRepository(Faculty::class)->createQueryBuilder('f')
            ->select('f.id, f.name')
            ->innerJoin('f.teamFolder', 'tf')
            ->andWhere('tf.name = :fname')
            ->setParameter('fname', $stream->getName())
            ->getQuery()
            ->getResult();

        return $this->getResponse('listFacultiesForStream', $faculty);
    }


    /**
     * Получение списка факультетов
     * @Rest\Get("/", name="list")
     * @Rest\View(serializerGroups={"faculty:list"})
     *
     * @OA\Parameter(name="page", in="query", @OA\Schema(type="integer", default=1))
     * @OA\Parameter(name="per_page", in="query", @OA\Schema(type="integer", default=10))
     *
     * @OA\Response(response=Response::HTTP_OK, description="Список факультетов",
     *     @OA\JsonContent(
     *      @OA\Property(
     *         property="data",
     *         type="array",
     *         @OA\Items(ref=@Model(type=Faculty::class, groups={"faculty:list"}))
     *      ),
     *     @OA\Property(
     *         property="meta",
     *         type="array",
     *         @OA\Items(
     *            @OA\Property(property="current_page", type="integer", example=1),
     *            @OA\Property(property="last_page", type="integer", example=1),
     *            @OA\Property(property="per_page", type="integer", example=1),
     *            @OA\Property(property="total", type="integer", example=1)
     *         )
     *      ),
     *    )
     * )
     *
     * @param Request $request
     * @param FacultyRepository $facultyRepository
     *
     * @throws Exception
     */
    public function list(Request $request, FacultyRepository $facultyRepository)
    {
        $page = (int)$request->get('page', 1);
        $pagePage = (int)$request->get('per_page', 10);

        $queryBuilder = $facultyRepository
            ->createQueryBuilder('f');

        $pagination = $this->paginator->paginate($queryBuilder, $page, $pagePage);

        return [
            'items' => $pagination->getItems(),
            'meta' => [
                'current_page' => $pagination->getCurrentPageNumber(),
                'last_page' => ceil($pagination->getTotalItemCount() / $pagination->getItemNumberPerPage()),
                'per_page' => $pagination->getItemNumberPerPage(),
                'total' => $pagination->getTotalItemCount(),
            ],
        ];
    }


    /**
     * Создание нового факультета
     * @Rest\Post("/", name="create")
     * @Rest\View(serializerGroups={"faculty:list"})
     *
     * @OA\RequestBody(@Model(type=FacultyType::class))
     *
     * @OA\Response(response=Response::HTTP_CREATED, description="Добавить факультет",
     *     @OA\JsonContent(
     *        type="object",
     *        @OA\Items(ref=@Model(type=Faculty::class, groups={"faculty:list"}))
     *     )
     * )
     *
     * @param Request $request
     *
     * @return Faculty
     */
    public function create(Request $request): Faculty
    {
        $faculty = $this->fillByForm(FacultyType::class, $request, new Faculty());
        $this->em->persist($faculty);
        $this->em->flush();

        return $faculty;
    }

    /**
     * Редактирование потока
     * @Rest\Patch("/{faculty{<\d+>}", name="edit")
     * @Rest\View(serializerGroups={"faculty:list"})
     *
     * @OA\Parameter(name="faculty", in="path", @OA\Schema(type="integer"), description="id факультета")
     *
     * @OA\RequestBody(@Model(type=FacultyType::class))
     *
     * @OA\Response(response=Response::HTTP_OK, description="Редактировать факультет",
     *     @OA\JsonContent(
     *        type="object",
     *        @OA\Items(ref=@Model(type=Faculty::class, groups={"faculty:list"}))
     *     )
     * )
     *
     * @param Faculty $faculty
     * @param Request $request
     *
     * @return Faculty
     */
    public function edit(Faculty $faculty, Request $request): Faculty
    {
        $faculty = $this->fillByForm(FacultyType::class, $request, $faculty);
        $this->em->flush();

        return $faculty;
    }


    /**
     * Удаление потока
     * @Rest\Delete("/{faculty<\d+>}", name="delete")
     * @Rest\View()
     *
     * @OA\Parameter(name="faculty", in="path", @OA\Schema(type="integer"), description="id факультета")
     *
     * @OA\Response(response=Response::HTTP_NO_CONTENT)
     */
    public function delete(Faculty $faculty)
    {
        $this->em->remove($faculty);
        $this->em->flush();

        return [];
    }

}

<?php

namespace App\Controller;

use App\Entity\AbstractCourse;
use App\Entity\CourseStream;
use App\Entity\Questionnaire\Question;
use App\Entity\Questionnaire\QuestionAnswerVariant;
use App\Entity\Questionnaire\UserAnswer;
use App\Entity\SmartSender\SmartSenderUser;
use App\Entity\User;
use App\Entity\UserGroup;
use App\Entity\UserGroupUser;
use App\Form\Questionnaire\UserAnswerType;
use App\Service\QuestionAnswerService;
use App\Service\SmartSenderHelper;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use Throwable;

/**
 * @Rest\Route("/api/questionnaire", name="questionnaire_")
 *
 * @OA\Tag(name="Вопросы-ответы")
 */
class QuestionnaireController extends BaseController
{

    private $smartSenderHelper;

    public function __construct(
        EntityManagerInterface $em,
        NormalizerInterface $normalizer,
        SmartSenderHelper $smartSenderHelper
    ) {
        parent::__construct($em, $normalizer);
        $this->smartSenderHelper = $smartSenderHelper;
    }

    /**
     * Получение вопросов и ответов
     * @Rest\Get("/list", name="list")
     *
     * @IsGranted("ROLE_USER")
     *
     * @OA\Response(
     *     response=200,
     *     description="Список вопросов и варианты ответов",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *         @OA\Schema(
     *             type="object",
     *             @OA\Property(
     *                 property="status",
     *                 type="string"
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 type="array",
     *                 @OA\Items(
     *                     type="object",
     *                     @OA\Property(type="string", property="field"),
     *                     @OA\Property(type="string", property="message"),
     *                 ),
     *             ),
     *             @OA\Property(
     *                 property="data",
     *                 type="array",
     *                 @OA\Items(
     *                     ref=@Model(type=Question::class, groups={"questionnaire","questionnaire:user"})
     *                 ),
     *             ),
     *         )
     *     )
     * )
     * @OA\Response(
     *      response=Response::HTTP_NOT_FOUND,
     *      description="Нет подписки ни на один курс",
     * )
     * @OA\Response(
     *      response=Response::HTTP_BAD_REQUEST,
     *      description="Ошибка авторизации",
     * )
     * @return JsonResponse
     */
    public function listAction(): JsonResponse
    {
        $stream = $this->getCurrentCourseStream($this->getUser());
        $questions = $this->em->getRepository(Question::class)
            ->createQueryBuilder('question')
            ->select(['question', 'courseStream', 'variant', 'variantNextQuestion', 'questionNextQuestion', 'answer'])
            ->leftJoin('question.courseStreams', 'courseStream')
            ->leftJoin('question.variants', 'variant')
            ->leftJoin('variant.nextQuestion', 'variantNextQuestion')
            ->leftJoin('question.nextQuestion', 'questionNextQuestion')
            ->leftJoin(
                'question.answers',
                'answer',
                Join::WITH,
                'answer.user = :user and answer.courseStream = :courseStream'
            )
            ->where('courseStream = :courseStream')
            ->setParameter('user', $this->getUser())
            ->setParameter('courseStream', $stream)
            ->getQuery()
            ->getResult();

        return $this->getResponseNew($questions, ['questionnaire', 'questionnaire:user']);
    }

    /**
     * Сохранение ответа
     *
     * @Rest\Post("/answer/{question<\d+>}", name="answer")
     *
     * @IsGranted("ROLE_USER")
     *
     * @OA\Parameter(name="questionId", in="path", @OA\Schema(type="integer"), required=true, description="id вопроса")
     * @OA\RequestBody(
     *     @OA\JsonContent(
     *          @OA\Property(property="text", type="string"),
     *          @OA\Property(property="variant", type="number"),
     *     )
     * )
     * @OA\Response(
     *     response=200,
     *     description="Сохранение ответа",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *         @OA\Schema(
     *             type="object",
     *             @OA\Property(
     *                 property="status",
     *                 type="string"
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 type="array",
     *                 @OA\Items(
     *                     type="object",
     *                     @OA\Property(type="string", property="field"),
     *                     @OA\Property(type="string", property="message"),
     *                 ),
     *             ),
     *             @OA\Property(
     *                 enum={
     *                      {"forceHundred"="boolean"},
     *                      "ko",
     *                  },
     *                 property="data"
     *             ),
     *         )
     *     )
     * )
     * @OA\Response(
     *      response=Response::HTTP_NOT_FOUND,
     *      description="Вопрос не найден или нет подписки ни на один курс",
     * )
     * @OA\Response(
     *      response=Response::HTTP_BAD_REQUEST,
     *      description="Ошибка авторизации",
     * )
     *
     * @param Request $request
     * @param Question $question
     * @param QuestionAnswerService $questionService
     *
     * @return JsonResponse
     * @throws Throwable
     */
    public function answerAction(
        Request $request,
        Question $question,
        QuestionAnswerService $questionService
    ): JsonResponse {
        $user = $this->getUser();
        $stream = $this->getCurrentCourseStream($user);

        if (!$question->getCourseStreams()->contains($stream)) {
            throw $this->createNotFoundException();
        }

        // @todo: проверка на заполненность предыдущих вопросов!
        $answer = $this->em->getRepository(UserAnswer::class)
            ->findOneBy([
                'courseStream' => $stream,
                'question' => $question,
                'user' => $user,
            ]);
        if (!$answer) {
            $answer = new UserAnswer();
            $answer->setUser($user);
            $answer->setQuestion($question);
            $answer->setCourseStream($stream);
        }

        $form = $this->createForm(UserAnswerType::class, $answer, ['question' => $question]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $forceHundred = false;

            $variant = $answer->getVariant();
            if (
                $variant
                && $variant->getParameter('forceHundred', false)
                && $this->isAllowChangeSpeed($user, $stream)
            ) {
                $this->setCourse($user, 'hundred');
                $forceHundred = true;
            }
            $this->em->persist($answer);
            $this->em->flush();

            $questionService->saveDataInAuthUser($user, $answer);

            return $this->getResponseNew(['forceHundred' => $forceHundred]);
        }

        return $this->getResponseNew('ko');
    }

    protected function isAllowChangeSpeed(User $user, CourseStream $stream): bool
    {
        /** @var UserGroup $userGroup */
        $userGroup = $this->em->getRepository(UserGroup::class)
            ->createQueryBuilder('userGroup')
            ->innerJoin('userGroup.userRelations', 'userRelation')
            ->where('userRelation.user = :user')
            ->andWhere('userGroup.courseStream = :courseStream')
            ->setParameter('user', $user)
            ->setParameter('courseStream', $stream)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        return !$userGroup || !in_array($userGroup->getId(), [877, 876], true);
    }

    /**
     * Определение рекомендуемого курса исходя из ответов ученика
     *
     * @Rest\Get("/course", name="course")
     * @IsGranted("ROLE_USER")
     *
     * @OA\Response(
     *     response=200,
     *     description="Получение названия рекомендуемого курса",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *         @OA\Schema(
     *             type="object",
     *             @OA\Property(
     *                 property="status",
     *                 type="string"
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 type="array",
     *                 @OA\Items(
     *                     type="object",
     *                     @OA\Property(type="string", property="field"),
     *                     @OA\Property(type="string", property="message"),
     *                 ),
     *             ),
     *             @OA\Property(
     *                 type="object",
     *                 property="data",
     *                 @OA\Property(type="string", property="course")
     *             ),
     *         )
     *     )
     * )
     * @OA\Response(
     *      response=Response::HTTP_NOT_FOUND,
     *      description="Курс не найден",
     * )
     * @OA\Response(
     *      response=Response::HTTP_BAD_REQUEST,
     *      description="Ошибка авторизации",
     * )
     *
     * @return JsonResponse
     */
    public function courseAction(): JsonResponse
    {
        $stream = $this->getCurrentCourseStream($this->getUser());
        $type = $stream->getAbstractCourse()->getSlug();
        if (in_array($type, [AbstractCourse::SLUG_SPEED, AbstractCourse::SLUG_SPEED_CLUB], true)) {
            $type = AbstractCourse::SLUG_SPEED;
            /** @var UserAnswer[] $answers */
            $answers = $this->em->getRepository(UserAnswer::class)
                ->createQueryBuilder('answer')
                ->join('answer.variant', 'variant')
                ->where('answer.user = :user and answer.courseStream = :courseStream')
                ->setParameter('courseStream', $stream)
                ->setParameter('user', $this->getUser())
                ->getQuery()
                ->getResult();

            foreach ($answers as $answer) {
                $variant = $answer->getVariant();
                if (
                    $variant
                    && $variant->getParameter('forceHundred', false)
                    && $this->isAllowChangeSpeed($this->getUser(), $stream)
                ) {
                    $type = AbstractCourse::SLUG_HUNDRED;
                }
            }

            return $this->getResponseNew(['course' => AbstractCourse::TITLES[$type]]);
        }

        return $this->getResponseNew(['course' => AbstractCourse::TITLES[AbstractCourse::SLUG_HUNDRED]]);
    }

    /**
     * Смена курса Сотка/Скорость.
     * Используется:
     * 1) пользователями во время прохождения анкеты;
     * 2) админами в интерфейсе управления пользователями
     *
     * @Rest\Get("/toggle-speed-hundred/{courseName<(speed|hundred)>}", name="toggle_speed_hundred")
     * @Rest\Get("/toggle-speed-hundred/{courseName<(speed|hundred)>}/{userId<\d+>}", name="toggle_speed_hundred_for_user")
     *
     * @IsGranted("ROLE_USER")
     *
     * @OA\Parameter(
     *     name="courseName",
     *     in="path", required=true,
     *     @OA\Schema(enum={AbstractCourse::SLUG_HUNDRED, AbstractCourse::SLUG_SPEED, AbstractCourse::SLUG_SPEED_CLUB})
     * )
     * @OA\Parameter(name="userId", in="path", @OA\Schema(type="number"))
     * @OA\Response(
     *     response=200,
     *     description="Получение названия рекомендуемого курса",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *         @OA\Schema(
     *             type="object",
     *             @OA\Property(
     *                 property="status",
     *                 type="string"
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 type="array",
     *                 @OA\Items(
     *                     type="object",
     *                     @OA\Property(type="string", property="field"),
     *                     @OA\Property(type="string", property="message"),
     *                 ),
     *             ),
     *             @OA\Property(
     *                 type="string",
     *                 property="data"
     *             ),
     *         )
     *     )
     * )
     * @OA\Response(
     *      response=404,
     *      description="Курс не найден",
     * )
     * @OA\Response(
     *      response=400,
     *      description="Ошибка авторизации",
     * )
     *
     * @param string $courseName Ожидаем speed или hundred
     * @param null|int $userId
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function toggleSpeedHundredAction(string $courseName, ?int $userId = null): JsonResponse
    {
        $user = $this->getUser();
        $admin = null;

        // определение юзера для администратора
        if ($userId) {
            if (!$this->isGranted('TEAMS_ACCESS')) {
                throw new AccessDeniedHttpException();
            }

            $admin = $user;
            $user = $this->em->getRepository(User::class)->find($userId);
            if (!$user) {
                throw new NotFoundHttpException();
            }
        }

        return $this->setCourse($user, $courseName, $admin);
    }

    /**
     * @todo: need refactoring
     */
    private function setCourse(User $user, string $newCourseName, ?User $admin = null): JsonResponse
    {
        if (
            !in_array(
                $newCourseName,
                [AbstractCourse::SLUG_SPEED, AbstractCourse::SLUG_SPEED_CLUB, AbstractCourse::SLUG_HUNDRED],
                true
            )
        ) {
            throw new NotFoundHttpException();
        }

        $superAdmins = [
            'aloe.lal@mail.ru', // старший менеджер поддержки
            'julyfedykova@gmail.com', // руководитель трекинга обучения
            'nikita_eltsov@inbox.ru',
            'demonzarov@ya.ru',
            'unofficial.vd@gmail.com',
            'r.pilinskii@likebz.ru',
            'a.chekmarev@likebz.ru',
            's.ismagilova@likebz.ru',
            'ya.lozinskaya@likebz.ru',
            'd.ryazanzeva@likebz.ru',
            'a.basov@likebz.ru',
            'admin@dudev.ru',
            'a.ivanchikhina@likebz.ru',
            'a.chekmarev@likebz.ru',
            'ya.lozinskaya@likebz.ru',
            's.ismagilova@likebz.ru',
            'a.korobko.s@likebz.ru',
            'v.cheremnova@likebz.ru',
            'k.samoylova@likebz.ru',
            'a.astapovich@likebz.ru',
            'e.mikhailova@likebz.ru',
            'v.makienko@likebz.ru',
            'e.gerasimova@likebz.ru',
            'a.soboleva@likebz.ru',
            'n.pavlov@likebz.ru',
            'a.kuzovkova@likebz.ru',
            'l.cherniakova@likebz.ru',
            'p.kosova@likebz.ru',
            'd.romanyuk@likebz.ru',
            'm.mezencev@likebz.ru',
            'e.soloveva@likebz.ru',
            'd.kosova@likebz.ru',
            'akorobko002@gmail.com',
            'j.popuchieva@likebz.ru',
            'a.nemtin@likebz.ru',
            'l.volkova@likebz.ru',
            'yulkarudez04@gmail.com',
            'kristintka@gmail.com',
            'n.bushueva@likebz.ru',
            'a.basov@likebz.ru',
            'm.burykina@likebz.ru',
            'a.kulesha@likebz.ru',
            'a.kirianov@likebz.ru',
            'k.zhernovaya@likebz.ru',
            'a.novospasskaia@likebz.ru',
            'i.baibekova@likebz.ru',
            'n.martoshenko@likebz.ru',
            'm.shishmareva@likebz.ru',
            'a.chervonenko@likebz.ru',
            'm.burykina@likebz.ru',
            'd.ryazanzeva@likebz.ru',
            'a.chervonenko@likebz.ru',
            'a.kirianov@likebz.ru',
            'm.troinova@likebz.ru',
            'p.orekhova@likebz.ru',
            'e.solunina@likebz.ru ',
            'a.gubina@likebz.ru',
            'r.sharafiev@likebz.ru',
        ];

        $isSuperAdmin = ($admin && in_array($admin->getEmail(), $superAdmins, true));
        $speed = in_array($newCourseName, [AbstractCourse::SLUG_SPEED, AbstractCourse::SLUG_SPEED_CLUB], true);
        $softJet = ($user->getId() === 198759) && !$admin;
        if ($speed && !$isSuperAdmin && !$softJet) {
            if ($admin) {
                $this->addError('', 'Нельзя сменить курс на Скорость вручную. Обратись к Никите Ельцову');
            }

            // Упертый ученик может вернуться на шаг назад после выбора Сотки, чтобы выбрать Скорость
            // В таком случае не показываем ему никакую ошибку. Возможно это не лучший UX
            return $this->getResponseNew('ko');
        }

        $oldStream = $this->getCurrentCourseStream($user);

        // берем группы текущего потока
        /** @var UserGroupUser[] $relations */
        $relations = $this->em->getRepository(UserGroupUser::class)
            ->createQueryBuilder('ugu')
            ->addSelect('ug')
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ug.courseStream', 'cs')
            ->innerJoin('cs.abstractCourse', 'ac')
            ->where('ugu.user = :user and ac.slug in (:slug)')
            ->andWhere('ugu.deleted = 0 and cs.active = 1')
            ->setParameters([
                'user' => $user,
                'slug' => $speed ? [AbstractCourse::SLUG_HUNDRED] : [
                    AbstractCourse::SLUG_SPEED,
                    AbstractCourse::SLUG_SPEED_CLUB,
                ],
            ])
            ->getQuery()
            ->getResult();

        if (empty($relations)) {
            if ($admin) {
                $this->addError('', 'Переключить курс можно только на актуальном потоке');
            }

            return $this->getResponse('toggleSpeedHundredAction');
        }

        $alternativeGroup = null;

        // удаляем из актуального курса
        foreach ($relations as $relation) {
            $relation->setDeleted(true);
            $relation->setFaculty(null);
            $this->em->persist($relation);

            // удаляем из команд
            foreach ($user->getTeams() as $team) {
                // только по актуальной группе
                // @todo: оттестить
                if ($team->getFolder()->getUserGroups()->contains($relation->getUserGroup())) {
                    $team->removeUser($user);
                    $this->em->persist($team);
                }
            }

            if (!$alternativeGroup) {
                $alternativeGroup = $relation->getUserGroup()->getAlternativeGroup();
            }
        }

        if (!$alternativeGroup) {
            return $this->getResponse('toggleSpeedHundredAction');
        }

        // удаляем из групп факультетов
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(UserGroupUser::class)->createQueryBuilder('ugu');
        /** @var UserGroupUser[] $facultyRelations */
        $facultyRelations = $qb
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ug.courseStream', 'cs')
            ->innerJoin('cs.abstractCourse', 'ac')
            ->where('ugu.user = :user')
            ->andWhere("ug.name LIKE '%сборная%' OR ug.name LIKE '%факультет%'")
            ->andWhere('cs.active = 1')
            ->andWhere('ac.slug IN(:slugs)')
            ->setParameter('user', $user)
            ->setParameter(
                'slugs',
                [AbstractCourse::SLUG_HUNDRED, AbstractCourse::SLUG_SPEED, AbstractCourse::SLUG_SPEED_CLUB]
            )
            ->getQuery()
            ->getResult();
        foreach ($facultyRelations as $facultyRelation) {
            $facultyRelation->setDeleted(true);
            $this->em->persist($facultyRelation);
        }

        // добавляем в альтернативную группу
        // @todo: важно переносить amoLeadId и budget из предыдущей группы
        // @todo: подгрузить amoLeadId и budget всем потеряшкам как минимум по Сотке 5 и 6
        $userGroupUser = $this->em->getRepository(UserGroupUser::class)
            ->findOneBy([
                'userGroup' => $alternativeGroup,
                'user' => $user,
            ]);

        if ($userGroupUser && $userGroupUser->isDeleted()) {
            $userGroupUser->setDeleted(false);
            $this->em->persist($userGroupUser);
        } elseif (!$userGroupUser) {
            $userGroupUser = new UserGroupUser($user, $alternativeGroup);
            $this->em->persist($userGroupUser);
        }
        $this->em->flush();

        if ($oldStream->getAlternativeCourseStream()) {
            $answers = $this->em->getRepository(UserAnswer::class)
                ->findBy([
                    'courseStream' => $oldStream,
                    'user' => $user,
                ]);
            foreach ($answers as $answer) {
                $question = $this->em->getRepository(Question::class)
                    ->findBySlugAndStream($answer->getQuestion()->getSlug(), $oldStream->getAlternativeCourseStream());

                if (!$question) {
                    continue;
                }
                $existingAnswer = $this->em->getRepository(UserAnswer::class)
                    ->findOneBy([
                        'courseStream' => $oldStream->getAlternativeCourseStream(),
                        'question' => $question,
                        'user' => $user,
                    ]);

                if (!$existingAnswer) {
                    $newAnswer = new UserAnswer();
                    $newAnswer->setCourseStream($oldStream->getAlternativeCourseStream())
                        ->setQuestion($question)
                        ->setUser($user);

                    if (!$answer->getVariant() || $question->getVariants()->isEmpty()) {
                        $newAnswer->setText($answer->getText());
                        $this->em->persist($newAnswer);
                    } else {
                        /** @var QuestionAnswerVariant $variant */
                        foreach ($question->getVariants() as $variant) {
                            if ($variant->getTitle() === $answer->getVariant()->getTitle()) {
                                $newAnswer->setVariant($variant);
                                $this->em->persist($newAnswer);
                                break;
                            }
                        }
                    }
                }
            }
            $this->em->flush();
        }

        // ищем связи с ботами альтернативного курса актуального потока
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(SmartSenderUser::class)->createQueryBuilder('ssu');
        /** @var SmartSenderUser[] $smartSenderUsers */
        $smartSenderUsers = $qb
            ->innerJoin('ssu.smartSenderBot', 'ssb')
            ->innerJoin('ssb.smartSenderBotGroups', 'ssbg')
            ->innerJoin('ssbg.userGroup', 'ug')
            ->innerJoin('ug.courseStream', 'cs')
            ->innerJoin('cs.abstractCourse', 'ac')
            ->where('ac.slug in (:slug)')
            ->andWhere('cs.active = 1')
            ->andWhere('ssu.user = :user')
            ->andWhere('ssu.smartSenderUserId IS NOT NULL')
            ->setParameters(
                [
                    'user' => $user,
                    'slug' => $speed ? [AbstractCourse::SLUG_HUNDRED] : [
                        AbstractCourse::SLUG_SPEED,
                        AbstractCourse::SLUG_SPEED_CLUB,
                    ],
                ]
            )
            ->getQuery()
            ->getResult();

        foreach ($smartSenderUsers as $smartSenderUser) {
            $this->smartSenderHelper->updateContact(
                $smartSenderUser,
                [
                    // в боте Скорости не может быть продукта Сотки (и наоборот), поэтому обнуляем
                    'product' => '',
                    // факультетов и сборных тоже нет после смены курса
                    'faculty' => '',
                    'group' => '',
                ]
            );

            // отправляем триггер, чтобы бот написал "ты сменил курс, привязывай еще раз"
            $this->smartSenderHelper->triggerEventOnContact(
                $smartSenderUser,
                SmartSenderHelper::EVENT_CHANGE_COURSE
            );
        }

        return $this->getResponse('toggleSpeedHundredAction');
    }

    /**
     * @return CourseStream
     * @throws NotFoundHttpException
     */
    protected function getCurrentCourseStream(User $user): CourseStream
    {
        $stream = $this->em->getRepository(CourseStream::class)->getCurrentStream($user);
        if (!$stream) {
            throw $this->createNotFoundException();
        }

        return $stream;
    }

}

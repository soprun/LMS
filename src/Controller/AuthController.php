<?php

namespace App\Controller;

use App\Service\AuthServerHelper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class AuthController extends AbstractController
{
    /**
     * Регистрация LmsUser
     * @Route("/auth/registerPlatformUser", name="app_auth_register_lms_user")
     * @param  Request  $request
     * @param  AuthServerHelper  $authServerHelper
     * @return JsonResponse
     */
    public function registerLmsUser(Request $request, AuthServerHelper $authServerHelper)
    {
        return $authServerHelper->registerLmsUser($request);
    }
}

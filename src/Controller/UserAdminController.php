<?php

namespace App\Controller;

use App\Controller\Exports\TemporaryExportsController;
use App\Entity\User;
use App\Entity\UserGroupUser;
use App\Service\AuthServerHelper;
use App\Service\UserAdminHelper;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserAdminController extends AbstractController
{
    /**
     * Страница со всеми пользователями в LMS
     * @Route("/users/list", name="app_users_list", methods={"GET"})
     * @return Response
     */
    public function pageUsers(): Response
    {
        return $this->render('lms_vue.html.twig');
    }

    /**
     * Получение всех пользователей LMS
     * @Route("/user/list/get", name="app_users_list_get", methods={"GET"})
     * @IsGranted("USER_USERS_LIST")
     * @param  Request  $request
     * @param  UserAdminHelper  $userAdminHelper
     * @return JsonResponse
     */
    public function getUserList(Request $request, UserAdminHelper $userAdminHelper)
    {
        return $userAdminHelper->getUserList($request);
    }

    /**
     * Получение всех овтетов пользователя LMS
     * @Route("/user/answers/get", name="app_user_answers_get", methods={"GET"})
     * @IsGranted("USER_USERS_LIST")
     * @param  Request  $request
     * @param  UserAdminHelper  $userAdminHelper
     * @return JsonResponse
     */
    public function getUserAnswersList(Request $request, UserAdminHelper $userAdminHelper)
    {
        return $userAdminHelper->getUserAndAnswers($request);
    }

    /**
     * Страница пользователей и их баллов для выгрузки
     * @Route("/users/points", name="app_users_points_page", methods={"GET"})
     * @return Response
     */
    public function pageUserPoints(): Response
    {
        return $this->render('lms_vue.html.twig');
    }

    /**
     * Получение пользователей и их баллов для выгрузки
     * @Route("/users/points/get", name="app_users_points_get", methods={"GET"})
     * @IsGranted("USER_USERS_LIST")
     * @param  Request  $request
     * @param  UserAdminHelper  $userAdminHelper
     * @return JsonResponse
     */
    public function getUserPoints(Request $request, UserAdminHelper $userAdminHelper)
    {
        return $userAdminHelper->getUserPoints($request);
    }

    /**
     * Аналитика проверяющих
     *
     * @Route("/answers/analytics/validators", name="app_answers_analitics_validators", methods={"GET"})
     * @return Response
     */
    public function pageAnalyticsAnswersValidators()
    {
        return $this->render('lms_vue.html.twig');
    }

    /**
     * Отправить аналитику проверяющих
     *
     * @Route("/answers/analytics/validators/get", name="app_answers_analitics_validators_get", methods={"GET"})
     * @IsGranted("USER_ANALYTICS_VIEW")
     * @param  Request  $request
     * @param  UserAdminHelper  $userAdminHelper
     * @return JsonResponse
     */
    public function getAnalyticsAnswersValidators(Request $request, UserAdminHelper $userAdminHelper)
    {
        return $userAdminHelper->getAnalyticsAnswersValidators($request);
    }

    /**
     * Регистрация AuthUser
     * @Route("/admin/registerAuthUser", name="app_admin_register_auth_user", methods={"POST"})
     * @IsGranted("USER_USERS_CREATE")
     * @param  Request  $request
     * @param  UserAdminHelper  $userAdminHelper
     * @return JsonResponse
     */
    public function registerAuthUser(Request $request, UserAdminHelper $userAdminHelper)
    {
        return $userAdminHelper->registerAuthUser($request);
    }

    /**
     * Получение списка курсов, которые будут использоваться при добавлении пользователя
     * @Route("/admin/courses_for_email/get", name="app_admin_courses_for_email_get", methods={"GET"})
     * @IsGranted("USER_USERS_CREATE")
     * @param  UserAdminHelper  $userAdminHelper
     * @return JsonResponse
     */
    public function getCoursesForEmail(UserAdminHelper $userAdminHelper)
    {
        return $userAdminHelper->getCoursesForEmail();
    }

    /**
     * Получение персональной ссылки на сброс пароля админом для пользователя
     * @Route("/admin/getResetPasswordLink", name="app_admin_reset_password_link_get", methods={"POST"})
     * @param  Request  $request
     * @param  UserAdminHelper  $userAdminHelper
     * @return JsonResponse
     * @IsGranted("ROLE_USER")
     */
    public function getResetPasswordLink(Request $request, UserAdminHelper $userAdminHelper)
    {
        return $userAdminHelper->getResetPasswordLink($request);
    }

    /**
     * Edit user on authServer
     * @Route("/admin/user/server/edit", name="app_admin_user_server_edit", methods={"POST"})
     * @param  Request  $request
     * @param  UserAdminHelper  $userAdminHelper
     * @return JsonResponse
     * @IsGranted("USER_TEAM_EDIT")
     */
    public function editUserByServer(Request $request, UserAdminHelper $userAdminHelper)
    {
        return $userAdminHelper->editUserByServer($request);
    }

    /**
     * Edit user on authServer
     * @Route("/admin/course/tariffs/get", name="app_admin_course_tariffs_get", methods={"POST"})
     * @param  Request  $request
     * @param  UserAdminHelper  $userAdminHelper
     * @return JsonResponse
     * @IsGranted("USER_TEAM_EDIT")
     */
    public function getCourseTariffs(Request $request, UserAdminHelper $userAdminHelper)
    {
        return $userAdminHelper->getCourseTariffs($request);
    }

    /**
     * Edit user on authServer
     * @Route("/admin/user/course/tariff/get", name="app_admin_user_course_tariff_get", methods={"POST"})
     * @param  Request  $request
     * @param  UserAdminHelper  $userAdminHelper
     * @return JsonResponse
     * @IsGranted("USER_TEAM_EDIT")
     */
    public function getUserCourseTariff(Request $request, UserAdminHelper $userAdminHelper)
    {
        return $userAdminHelper->getUserCourseTariff($request);
    }

    /**
     * Edit user on authServer
     * @Route("/admin/user/course/tariff/edit", name="app_admin_user_course_tariff_edit", methods={"POST"})
     * @param  Request  $request
     * @param  UserAdminHelper  $userAdminHelper
     * @return JsonResponse
     * @IsGranted("USER_TEAM_EDIT")
     */
    public function addOrEditUserCourseTariff(Request $request, UserAdminHelper $userAdminHelper)
    {
        return $userAdminHelper->addOrEditUserCourseTariff($request);
    }

    /**
     * Получение инфы о группах пользователя
     *
     * @Route("/admin/user-groups/{email}", name="app_admin_user_groups")
     * @param  string  $email
     * @param  AuthServerHelper  $authServerHelper
     * @param  EntityManagerInterface  $em
     * @return Response
     *
     * @IsGranted("USER_TEAM_EDIT")
     */
    public function userGroupsAction(
        string $email,
        AuthServerHelper $authServerHelper,
        EntityManagerInterface $em
    ): Response {
        $authUser = $authServerHelper->getUsers([], ['id', 'name', 'lastname'], "u.email = '{$email}'")[0];
        /** @var User $user */
        $user = $em->getRepository(User::class)->find($authUser['id']);

        dump(
            [
                'lmsUserId' => $authUser['id'],
                'name' => $authUser['name'],
                'lastname' => $authUser['lastname'],
            ]
        );

        foreach ($user->getGroupRelations() as $relation) {
            $addedBy = 'система';
            if ($addedByUser = $relation->getAddedBy()) {
                $authUser = $authServerHelper->getUsers(
                    [$addedByUser->getAuthUserId()],
                    ['id', 'email', 'name', 'lastname']
                )[0];
                $addedBy = sprintf(
                    "%s | %s %s | lmsUserId %s",
                    $authUser['email'],
                    $authUser['name'],
                    $authUser['lastname'],
                    $authUser['id']
                );
            }

            dump(
                [
                    'Группа' => $relation->getUserGroup()->getName(),
                    'Ссылка на групу' => sprintf(
                        "https://lms.toolbox.wip/users/group/%s/users",
                        $relation->getUserGroup()->getId()
                    ),
                    'Дата добавления' => $relation->getCreatedAt()->format('Y-m-d H:i:s'),
                    'Дата изменения' => $relation->getCreatedAt()->format('Y-m-d H:i:s'),
                    'Статус' => $relation->isDeleted() ? 'удален ' : 'в группе',
                    'Кто добавил' => $addedBy,
                    'Амо' => $relation->getAmoLeadId()
                        ? 'https://likebusiness.amocrm.ru/leads/detail/' . $relation->getAmoLeadId()
                        : '',
                ]
            );
        }

        return new Response();
    }

    /**
     * Получение csv со списком юзеров на потоке. Нужно для сверки выданных доступов
     * @Route("/admin/users-on-stream/{stream}", name="admin_users_on_stream_csv")
     * @IsGranted("USER_ANALYTICS_VIEW")
     * @param  string  $stream
     * @param  AuthServerHelper  $authServerHelper
     * @param  EntityManagerInterface  $em
     * @return Response
     */
    public function usersOnStream(
        Request $request,
        string $stream,
        AuthServerHelper $authServerHelper,
        EntityManagerInterface $em
    ): Response {
        $page = (int)$request->query->get('page', 1);
        $pageSize = (int)$request->query->get('pageSize', 10);
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $em->getRepository(User::class)->createQueryBuilder('u');
        $rows = $queryBuilder
            ->select('u.id', 'u.authUserId', 'ug.name', 'ugu.createdAt')
            ->join('u.groupRelations', 'ugu')
            ->join('ugu.userGroup', 'ug')
            ->join('ug.courseStream', 'cs')
            ->where('ugu.deleted = 0 AND cs.name = :stream')
            ->setParameter('stream', $stream)
            ->setFirstResult(($page - 1) * $pageSize)
            ->setMaxResults($pageSize)
            ->groupBy('u.id', 'u.authUserId', 'ug.name', 'ugu.createdAt')
            ->getQuery()
            ->getResult();
        $users = [];
        foreach ($rows as $row) {
            $users[$row['id']] = [
                'id' => $row['id'],
                'group' => $row['name'],
                'authUserId' => $row['authUserId'],
                'createdAt' => $row['createdAt']->format("Y-m-d H:i:s"),
                'facultyName' => [],
            ];
        }
        // получение факультетов
        $queryBuilder = $em->getRepository(User::class)->createQueryBuilder('u');
        $rows = $queryBuilder
            ->select('u.id', 'f.name')
            ->join('u.groupRelations', 'ugu')
            ->join('ugu.userGroup', 'ug')
            ->join('ug.courseStream', 'cs')
            ->join('cs.faculties', 'f')
            ->where('ugu.deleted = 0 AND cs.name = :stream AND u.id IN (:users) AND f.userGroup = ug')
            ->setParameter('stream', $stream)
            ->setParameter('users', array_column($users, 'id'))
            ->setFirstResult(($page - 1) * $pageSize)
            ->setMaxResults($pageSize)
            ->getQuery()
            ->getResult();
        foreach ($rows as $row) {
            $users[$row['id']]['facultyName'][] = $row['name'];
        }
        $authUsers = $authServerHelper->getUsers(
            array_column($users, 'authUserId'),
            ['id', 'email', 'phone', 'telegram']
        );
        $response = implode(TemporaryExportsController::DEFAULT_SEPARATOR, [
            'lmsUserId', 'email', 'Телеграм', 'Телефон', 'Группа', 'Добавлен', 'Факультет',
        ]);
        foreach ($authUsers as $authUser) {
            $response .= "\n";
            $response .= implode(TemporaryExportsController::DEFAULT_SEPARATOR, [
                $authUser['id'],
                $authUser['email'],
                $authUser['telegram'],
                $authUser['phone'],
                $users[$authUser['id']]['group'],
                $users[$authUser['id']]['createdAt'],
                implode(',', $users[$authUser['id']]['facultyName']),
            ]);
        }

        return new Response($response, Response::HTTP_OK, ['content-type' => 'text/plain']);
    }

}

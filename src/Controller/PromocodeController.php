<?php

namespace App\Controller;

use App\Entity\AttemptToGetPromocode;
use App\Entity\Partner;
use App\Entity\Promocode;
use App\Form\PromocodeType;
use App\Repository\PromocodeRepository;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/api/promocodes", name="promocodes_")
 * @IsGranted("ROLE_USER")
 */
class PromocodeController extends BaseController
{
    /**
     * @Route("/create", name="create_promocode", methods={"POST"})
     *
     * @param  Request  $request
     *
     * @return JsonResponse
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function create(Request $request): JsonResponse
    {
        $promocode = new Promocode();
        $form = $this->createForm(PromocodeType::class, $promocode);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($promocode);
            $this->em->flush();

            return $this->getResponse(
                '',
                $this->normalize(
                    $promocode,
                    ['promocode:info', 'partner:info', 'promocode:user:info']
                )
            );
        }

        return $this->addFormErrors($form)
            ->getResponse('');
    }

    /**
     * @Route("/{partnerSlug}", name="take_promocode", methods={"GET"})
     * @ParamConverter("partner", options={"mapping": {"partnerSlug": "slug"}})
     *
     * @param Partner $partner
     *
     * @return JsonResponse
     */
    public function getCode(Partner $partner, PromocodeRepository $promocodeRepository): JsonResponse
    {
        $attemptToGetPromocode = new AttemptToGetPromocode();
        $attemptToGetPromocode->setPartner($partner);
        $attemptToGetPromocode->setUser($this->getUser());
        $this->em->persist($attemptToGetPromocode);
        $this->em->flush();

        $promocodeHasBeenTaken = $promocodeRepository->findBy([
            'user' => $this->getUser(),
            'partner' => $partner
        ]);

        if ($promocodeHasBeenTaken) {
            $promocode = $partner->getUserPromocode($this->getUser())->first();
        } else {
            $promocode = $partner->getFreePromocode()->first();
        }

        if (!$promocode) {
            // @TODO: надо добавит "тип ссылки" для партнера, чтобы опнимать когда надо промокод искать, а когда нет
            return $this->getResponse('');
        }

        if ($promocode->isUnique()) {
            $promocode->setUser($this->getUser());
            $this->em->persist($promocode);
            $this->em->flush();
        }

        return $this->getResponse(
            '',
            $this->normalize($promocode, [
                'promocode:info', 'partner:info', 'promocode:user:info'
            ])
        );
    }
}

<?php

namespace App\Controller;

use App\Entity\Notification;
use App\Entity\User;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/notification", name="notification_")
 * @IsGranted("ROLE_USER")
 *
 * Class NotificationController
 * @package App\Controller
 */
class NotificationController extends BaseController
{
    /**
     * @Route("/list", name="list")
     * @param  Request  $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function listAction(Request $request): JsonResponse
    {
        $qb = $this->createQueryBuilder();
        if (($isRead = $request->get('isRead', null)) !== null) {
            $qb->andWhere('c.statusId = :status')
                ->setParameter('status', $isRead ? Notification::STATUS_READ : Notification::STATUS_NEW);
        }

        $page = (int)$request->get('page', 1);
        $pageSize = (int)$request->get('pageSize', 10);
        if ($page && $pageSize) {
            $qb->setFirstResult(($page - 1) * $pageSize);
            $qb->setMaxResults($pageSize);
        }

        $paginator = new Paginator($qb->getQuery());

        /** @var Notification[]|Collection $notifications */
        $notifications = $paginator->getIterator()->getArrayCopy();

        foreach ($notifications as &$notification) {
            $notification = [
                'id' => $notification->getId(),
                'title' => $notification->getTitle(),
                'description' => $notification->getDescription(),
                'iconId' => $notification->getIconId(),
                'isRead' => $notification->isRead(),
                'link' => [
                    'typeId' => $notification->getLinkTypeId(),
                    'content' => $notification->getLinkContent(),
                ],
                'createdAt' => $notification->getCreatedAt()->format('Y-m-d H:i:s'),
            ];
        }

        $this->markAsRead();

        return $this->getResponse(
            'getNotificationsList',
            [
                'count' => $paginator->count(),
                'notifications' => $notifications,
            ]
        );
    }

    /**
     * @Route("/unread-count", name="unread_count")
     *
     * @return JsonResponse
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function unreadCountAction(): JsonResponse
    {
        $count = $this->createQueryBuilder()
            ->select('count(c)')
            ->andWhere('c.statusId != :status')
            ->setParameter('status', Notification::STATUS_READ)
            ->getQuery()
            ->getSingleScalarResult();

        return $this->getResponse('getUnreadNotificationsCount', ['count' => (int)$count]);
    }

    /**
     * @return QueryBuilder
     */
    private function createQueryBuilder(): QueryBuilder
    {
        /** @var QueryBuilder $qb */
        $qb = $this->getDoctrine()
            ->getRepository(Notification::class)
            ->createQueryBuilder('c');

        $qb->where('c.user = :user')
            ->setParameter('user', $this->getUser())
            ->orderBy('c.updatedAt', 'desc');

        return $qb;
    }

    private function markAsRead()
    {
        /** @var User $user */
        $user = $this->getUser();
        /** @var Connection $con */
        $con = $this->getDoctrine()->getConnection();
        $con->update(
            'notification',
            [
                'status_id' => Notification::STATUS_READ,
            ],
            [
                'status_id' => Notification::STATUS_NEW,
                'user_id' => $user->getId(),
            ]
        );
    }

}

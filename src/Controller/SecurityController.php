<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SecurityController extends AbstractController
{

    /**
     * Главная страница
     * @Route("/", name="app_main_page", methods={"GET"})
     */
    public function pageMain()
    {
        /** @var User $user */
        $user = $this->getUser();
        $routeName = $user->getEmail() ? 'app_courses_folder_list' : 'app_login_page';

        return $this->redirectToRoute($routeName);
    }

    /**
     * Страница регистрации
     * @Route("/registration", name="app_registration_page", methods={"GET"})
     */
    public function pageRegistration()
    {
        /** @var User $user */
        $user = $this->getUser();
        if ($user->getEmail()) {
            return $this->redirectToRoute('app_courses_folder_list');
        }

        return $this->render('lms_vue.html.twig');
    }

    /**
     * Страница логина
     * @Route("/login", name="app_login_page", methods={"GET"})
     */
    public function pageLogin()
    {
        /** @var User $user */
        $user = $this->getUser();
        if ($user->getEmail()) {
            return $this->redirectToRoute('app_courses_folder_list');
        }

        return $this->render('lms_vue.html.twig');
    }

    /**
     * Страница выхода
     * @Route("/logout", name="app_logout", methods={"GET"})
     */
    public function logout()
    {
        return $this->render('lms_vue.html.twig');
    }

    /**
     * Страница запроса ссылки на восстановление пароля
     * @Route("/resetpassword", name="app_reset_password", methods={"GET"})
     */
    public function resetPassword()
    {
        return $this->render('lms_vue.html.twig');
    }

    /**
     * Страница, на которую пользователь заходит по ссылке из письма, чтобы поставить новый пароль
     * @Route("/resetpassword/{resetPasswordHash}", name="app_reset_password_hash", methods={"GET"})
     */
    public function resetPasswordNew()
    {
        return $this->render('lms_vue.html.twig');
    }

}

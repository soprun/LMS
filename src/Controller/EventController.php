<?php

namespace App\Controller;

use App\Entity\Event;
use App\Entity\User;
use App\Form\EventType;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use OpenApi\Annotations as OA;

/**
 * @Rest\Route("/api/event", name="event_")
 *
 * @OA\Tag(name="События")
 *
 * @IsGranted("ROLE_USER")
 */
class EventController extends BaseController
{
    /**
     * Создать событие
     *
     * @Rest\Post("/create", name="create")
     *
     * @OA\RequestBody(
     *     @OA\MediaType(
     *         mediaType="application/json",
     *         @OA\Schema(
     *             ref=@Model(
     *                 type=EventType::class
     *             )
     *         )
     *     )
     * )
     *
     * @OA\Response(
     *     response=200,
     *     description="Создать событие",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *         @OA\Schema(
     *             type="object",
     *             @OA\Property(
     *                 property="status",
     *                 type="string"
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 type="array",
     *                 @OA\Items(
     *                     type="object",
     *                     @OA\Property(type="string", property="field"),
     *                     @OA\Property(type="string", property="message"),
     *                 ),
     *             ),
     *             @OA\Property(
     *                 property="data",
     *                 type="string",
     *                 enum={"ko", "ok"}
     *             ),
     *         )
     *     )
     * )
     * @OA\Response(
     *      response=400,
     *      description="Ошибка авторизации",
     * )
     *
     * @throws ExceptionInterface
     * @throws \Exception
     */
    public function createAction(Request $request): JsonResponse
    {
        /** @var User $currentUser */
        $currentUser = $this->getUser();

        $event = new Event($currentUser);
        $form = $this->createForm(EventType::class, $event);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($event);
            $this->em->flush();

            return $this->getResponseNew('ok');
        }

        return $this->addFormErrors($form)->getResponseNew('ko');
    }

}

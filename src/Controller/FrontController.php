<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class FrontController extends AbstractController
{

    /**
     * @Route("/questionnaire", name="questionnaire_bot")
     * @Route("/questionnaire/telegram", name="questionnaire_telegram")
     * @Route("/questionnaire/toggle-course", name="questionnaire_toggle_course")
     * @Route("/users/team", name="questionnaire_team")
     * @Route("/icons", name="questionnaire_icon")
     * @Route("/teams/allocated", name="teams_allocated")
     * @Route("/teams/unallocated", name="teams_unallocated")
     * @Route("/partners", name="partners")
     * @Route("/rating/teams", name="rating_teams")
     * @Route("/rating/investments", name="rating_investments")
     * @Route("/questionnaire/hundred", name="questionnaire_hundred")
     * @Route("/rating/investments/registration", name="rating_investments_registration")
     * @Route("/rating/investments/user", name="rating_investments_user")
     * @Route("/rating/investments/user/{id}", name="rating_investments_user_id")
     * @Route("/rating/unified", name="rating_unified")
     * @Route("/rating/unified/user", name="rating_unified_user")
     * @Route("/rating/unified/user/{id}", name="rating_unified_user_id")
     * @Route("/rating/fast-cash", name="rating_fast-cash")
     * @Route("/rating/fast-cash/user", name="rating_fast-cash_user")
     * @Route("/rating/fast-cash/user/{id}", name="rating_fast-cast_user_id")
     * @Route("/traction-analytics/speed-and-hundred", name="traction_analytics_speed_and_hundred")
     * @Route("/traction-analytics/reports", name="traction_analytics_reports")
     * @Route("/club", name="club")
     * @Route("/profile-club", name="club_user")
     * @Route("/profile-club/{id}", name="club_user_id")
     */
    public function any()
    {
        return $this->render('lms_vue.html.twig');
    }

}

<?php

namespace App\Controller\Courses;

use App\Controller\BaseController;
use App\Entity\Courses\Course;
use App\Entity\Courses\CourseFolder;
use App\Entity\Courses\Lesson;
use App\Entity\Task\AnswerTaskType1;
use App\Entity\UserLesson;
use App\Service\Course\CourseHelper;
use App\Service\Course\LessonHelper;
use App\Service\Course\MenuHelper;
use App\Service\Course\TaskHelper;
use App\Service\DuplicateEntityService;
use App\Service\FormHelper;
use App\Service\UserHelper;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CourseController extends BaseController
{

    /**
     * Страница папок с курсами
     *
     * @Route("/courses/folder/list", name="app_courses_folder_list", methods={"GET"})
     */
    public function pageCourses()
    {
        return $this->render('lms_vue.html.twig');
    }

    /**
     * Получить папки с курсами
     *
     * @Route("/courses/folder/list/get", name="app_courses_folder_list_get", methods={"GET"})
     * @IsGranted("ROLE_USER")
     */
    public function getListOfFolders(Request $request, CourseHelper $courseHelper)
    {
        return $courseHelper->getCourseFolders($request);
    }

    /**
     * Страница добавления папки курса
     *
     * @Route("/courses/folder/new", name="app_courses_folder_new", methods={"POST"})
     * @IsGranted("USER_FOLDER_CREATE")
     */
    public function newCourseFolder(Request $request, CourseHelper $courseHelper)
    {
        return $courseHelper->newOrEditCourseFolder($request);
    }

    /**
     * Страница редактирования папки курса
     *
     * @Route("/courses/folder/{folderId}/edit", name="app_courses_folder_edit", methods={"POST"})
     * @ParamConverter("courseFolder", options={"mapping": {"folderId": "id"}})
     * @IsGranted("USER_FOLDER_EDIT",  subject="courseFolder")
     */
    public function editCourseFolder(Request $request, CourseFolder $courseFolder, CourseHelper $courseHelper)
    {
        return $courseHelper->newOrEditCourseFolder($request, $courseFolder);
    }

    /**
     * Страница удаления папки курса
     *
     * @Route("/courses/folder/{folderId}/delete", name="app_courses_folder_delete", methods={"DELETE"})
     * @ParamConverter("courseFolder", options={"mapping": {"folderId": "id"}})
     * @IsGranted("USER_FOLDER_DELETE",  subject="courseFolder")
     */
    public function deleteCourseFolder(Request $request, CourseFolder $courseFolder, CourseHelper $courseHelper)
    {
        return $courseHelper->deleteCourseFolder($request, $courseFolder);
    }

    /**
     * Копирование папки с курсами
     *
     * @Route("/courses/folder/{folderId}/duplicate", name="app_courses_folder_duplicate", methods={"POST"})
     * @ParamConverter("courseFolder", options={"mapping": {"folderId": "id"}})
     * @IsGranted("USER_IN_FOLDER_DUPLICATE",  subject="courseFolder")
     */
    public function duplicateCourseFolder(
        CourseFolder $courseFolder,
        DuplicateEntityService $duplicateEntityService
    ): JsonResponse {
        ini_set('max_execution_time', 0);
        $courseFolder = $duplicateEntityService->duplicateFolder($courseFolder);

        return $this->getResponseNew($courseFolder, ['course_folder:info']);
    }

    /**
     * Страница папки с курсами
     *
     * @Route("/courses/folder/{folderId}", name="app_courses_folder", methods={"GET"})
     */
    public function pageCourseFolder()
    {
        return $this->render('lms_vue.html.twig');
    }

    /**
     * Получить курсы в папке
     *
     * @Route("/courses/folder/{folderId}/courses/get", name="app_courses_folder_get", methods={"GET"})
     * @ParamConverter("courseFolder", options={"mapping": {"folderId": "id"}})
     * @IsGranted("USER_IN_FOLDER", subject="courseFolder")
     */
    public function pageCourseFolderGet(CourseFolder $courseFolder, CourseHelper $courseHelper)
    {
        return $courseHelper->getCoursesInFolder($courseFolder);
    }

    /**
     * Страница добавления нового курса в папке
     *
     * @Route("/courses/folder/{folderId}/course/new", name="app_courses_folder_course_new", methods={"POST"})
     * @ParamConverter("courseFolder", options={"mapping": {"folderId": "id"}})
     * @IsGranted("USER_COURSE_IN_COURSE_FOLDER_NEW", subject="courseFolder")
     */
    public function newCourseInCourseFolder(Request $request, CourseFolder $courseFolder, CourseHelper $courseHelper)
    {
        return $courseHelper->newOrEditCourseInCoursesFolder($request, $courseFolder);
    }

    /**
     * Страница редактирования курса в папке
     *
     * @Route("/courses/course/{courseId}/edit", name="app_courses_folder_course_edit", methods={"POST"})
     * @ParamConverter("course", options={"mapping": {"courseId": "id"}})
     * @IsGranted("USER_COURSE_IN_COURSE_FOLDER_EDIT", subject="course")
     */
    public function editCourseInCourseFolder(Request $request, Course $course, CourseHelper $courseHelper)
    {
        return $courseHelper->newOrEditCourseInCoursesFolder($request, null, $course);
    }

    /**
     * Страница удаления курса в папке
     *
     * @Route("/courses/course/{courseId}/delete", name="app_courses_folder_course_delete", methods={"DELETE"})
     * @ParamConverter("course", options={"mapping": {"courseId": "id"}})
     * @IsGranted("USER_COURSE_IN_COURSE_FOLDER_DELETE", subject="course")
     */
    public function deleteCourseInCourseFolder(Course $course, CourseHelper $courseHelper)
    {
        return $courseHelper->deleteCourseInCourseFolder($course);
    }

    /**
     * Копирование курса
     *
     * @Route("/courses/course/{courseId}/duplicate", name="app_courses_course_duplicate", methods={"POST"})
     * @ParamConverter("course", options={"mapping": {"courseId": "id"}})
     * @IsGranted("USER_IN_COURSE_DUPLICATE", subject="course")
     */
    public function duplicateCourse(
        Course $course,
        DuplicateEntityService $duplicateEntityService
    ): JsonResponse {
        $course = $duplicateEntityService->duplicateCourse($course);

        return $this->getResponseNew($course, ['course:info']);
    }

    /**
     * Страница курса
     *
     * @Route("/courses/course/{courseId}", name="app_courses_course", methods={"GET"})
     */
    public function pageCourse()
    {
        return $this->render('lms_vue.html.twig');
    }

    /**
     * Получить список уроков для курса
     *
     * @Route("/courses/course/{courseId}/lessons/get", name="app_courses_folder_course_get", methods={"GET"})
     * @ParamConverter("course", options={"mapping": {"courseId": "id"}})
     * @IsGranted("USER_IN_COURSE", subject="course")
     */
    public function pageCourseGet(Course $course, LessonHelper $lessonHelper)
    {
        return $lessonHelper->getLessons($course);
    }

    /**
     * Получить ProgressBar
     *
     * @Route("/courses/course/{courseId}/lessons/progressbar/get", name="progressbar_get", methods={"GET"})
     * @ParamConverter("course", options={"mapping": {"courseId": "id"}})
     * @IsGranted("USER_IN_COURSE", subject="course")
     */
    public function getLessonProgressBar(Course $course, LessonHelper $lessonHelper)
    {
        return $lessonHelper->getLessonProgressBar($course);
    }


    /**
     * Страница курса
     *
     * @Route("/courses/select/get", name="app_courses_select_get", methods={"GET"})
     */
    public function getCourseSelect(CourseHelper $courseHelper)
    {
        return $courseHelper->getCourseSelect();
    }

    /**
     * Получение суммарного кол-ва баллов по пройденным урокам
     * @Route("/courses/{id}/stats", methods={"GET"})
     */
    public function getStatsOnLessons(
        Course $course,
        MenuHelper $menuHelper,
        EntityManagerInterface $em,
        FormHelper $formHelper,
        UserHelper $userHelper,
        TaskHelper $taskHelper
    ): JsonResponse {
        $user = $this->getUser();
        $opened = $passed = $checked = $total = 0;
        $points = $menuHelper->getUserPointsByCourse($user, $course);

        $courseLessons = $em->getRepository(Lesson::class)->createQueryBuilder('l')
            ->select('distinct l')
            ->innerJoin('l.lessonConfiguration', 'lc')
            ->leftJoin('l.userGroups', 'ug')
            ->leftJoin('ug.userRelations', 'ur')
            ->andWhere('l.course = :course')
            ->setParameter('course', $course)
            ->andWhere('ur.user = :user')
            ->setParameter('user', $user)
            ->andWhere('ur.deleted = 0')
            ->andWhere('l.deleted = 0')
            ->andWhere('lc.isHidden is null OR lc.isHidden = 0')
            ->getQuery()
            ->getResult();

        $statusForLessons = $em->getRepository(UserLesson::class)->getStatusForLessonsCourse($user, $courseLessons);
        $isAdmin = $userHelper->isUserAdmin();

        foreach ($courseLessons as $crsLesson) {
            $total++;

            if (isset($statusForLessons[$crsLesson->getId()])) {
                $opened++;
                $taskArray = [
                    'isTaskExist' => false,
                    'isAnswerExist' => false,
                    'isAllChecked' => true,
                    'ddl' => '',
                ];
                $tasks = $taskHelper->getTaskByLesson($crsLesson, null);

                foreach ($tasks as $task) {
                    $answers = $em->getRepository(AnswerTaskType1::class)->findBy([
                        'user' => $user,
                        'task' => $task['task'],
                    ]);
                    $allChecked = true;
                    if ($answers) {
                        /** @var AnswerTaskType1 $answer */
                        foreach ($answers as $answer) {
                            if ($answer->getIsChecked()) {
                                $passed++;
                                break;
                            } else {
                                $allChecked = false;
                                break;
                            }
                        }
                    } else {
                        $allChecked = false;
                    }
                    $taskArray = [
                        'isTaskExist' => true,
                        'isAnswerExist' => !$taskArray['isAnswerExist'] ? !empty($answers) : true,
                        'isAllChecked' => $allChecked,
                        'ddl' => $task && ($task['task'] || $isAdmin) && $task['ddl']
                        && !$answers ? $task['ddl']->format("Y-m-d H:i:s") : "",
                    ];
                }

                if (
                    !$taskArray['isTaskExist'] &&
                    $statusForLessons[$crsLesson->getId()] === UserLesson::STATE_VIEWED
                ) {
                    $checked++;
                    $passed++;
                }
                if (
                    $taskArray['isTaskExist'] &&
                    $statusForLessons[$crsLesson->getId()] !== UserLesson::STATE_VIEWED
                ) {
                    $checked++;
                }
            }
        }

        return $formHelper->getResponse(
            'getStatsOnLessons',
            [
                'points' => $points,
                'lessons' => [
                    'total' => $total,
                    'open' => $opened,
                    'passed' => $passed,
                    'checked' => $checked,
                ],
            ]
        );
    }

}

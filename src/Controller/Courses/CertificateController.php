<?php

namespace App\Controller\Courses;

use App\Controller\BaseController;
use App\Service\AuthServerHelper;
use App\Service\Course\CertificateService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

/**
 *
 * Class CertificateController
 * @package App\Controller
 */
class CertificateController extends BaseController
{
    /**
     * Сертификат
     *
     * @Route("/certificate/get", name="app_certificate_get", methods={"GET"})
     * @param  Request  $request
     * @param  CertificateService  $certificateService
     * @return Response
     */
    public function certificateGet(Request $request, CertificateService $certificateService): Response
    {
        $serialNumber = $request->get('serial', 0);

        if ($serialNumber) {
            $dataCert = $certificateService->getCertificate($serialNumber);
            if ($dataCert) {
                /* output the image */
                $image = $certificateService->createCertificateImg($dataCert);
                $filename = "certificate.png";

                $response = new Response();
                $disposition = $response->headers
                    ->makeDisposition(ResponseHeaderBag::DISPOSITION_INLINE, $filename);
                $response->headers->set('Content-Disposition', $disposition);
                $response->headers->set('Content-Type', 'image/png');
                $response->setContent($image);

                return $response;
            }
        }

        return $this->getResponse(
            'certificateGet',
            []
        );
    }

    /**
     * Сертификат send
     *
     * @Route("/certificate/send", name="app_certificate_send", methods={"GET"})
     * @param  Request  $request
     * @param  CertificateService  $certificateService
     * @param  AuthServerHelper  $authServerHelper
     * @return JsonResponse
     * @IsGranted("USER_USERS_LIST")
     */
    public function certificateSend(
        Request $request,
        CertificateService $certificateService,
        AuthServerHelper $authServerHelper
    ) {
        $serialNumber = $request->get('serial', 0);

        $data = [];

        if ($serialNumber) {
            $dataCert = $certificateService->getCertificate($serialNumber);
            if ($dataCert) {
                $image = $certificateService->createCertificateImg($dataCert);
                $dataCert['image'] = json_encode(base64_encode($image->getimageblob()));
                [$authUser, $errors] = $authServerHelper->sendCertificate($dataCert);
                if (!empty($errors)) {
                    $this->addError($errors[0]['field'], $errors[0]['message']);
                }
                if (!isset($authUser->lmsUserId)) {
                    $this->addError('lmsUserId', 'notSendFromAuthServer');
                } else {
                    $data = [
                        'sendCertificateUser' => $authUser->lmsUserId,
                        'sendCertificateEmail' => $dataCert['email'],
                        'sendCertificateSerial' => $dataCert['serialNumber'],
                    ];
                }
            }
        }

        return $this->getResponse(
            'certificateSend',
            $data
        );
    }

    /**
     * @Route("/certificate/generate", name="app_certificate_generate", methods={"GET"})
     * @param  CertificateService  $certificateService
     * @return JsonResponse
     * @IsGranted("USER_USERS_LIST")
     */
    public function generateSerial(CertificateService $certificateService)
    {
        return $this->getResponse(
            'certificateSerialNumber',
            [$certificateService->generateSerialNumber()]
        );
    }
}

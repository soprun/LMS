<?php

namespace App\Controller\Courses;

use App\Service\Course\FileHelper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class FileController extends AbstractController
{

    /**
     * Загрузка одного или нескольких файлов
     * @Route("/files/add", name="app_files_add", methods={"POST"})
     * @param  Request  $request
     * @param  FileHelper  $fileHelper
     * @return JsonResponse
     */
    public function addFiles(Request $request, FileHelper $fileHelper): JsonResponse
    {
        return $fileHelper->addFiles($request);
    }

    /**
     * Загрузка картинки с соответствующей валидацией
     * @Route("/files/img/add", name="app_files_img_add", methods={"POST"})
     * @param  Request  $request
     * @param  FileHelper  $fileHelper
     * @return JsonResponse
     */
    public function addImg(Request $request, FileHelper $fileHelper): JsonResponse
    {
        return $fileHelper->addImg($request);
    }

}

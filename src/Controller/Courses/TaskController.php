<?php

namespace App\Controller\Courses;

use App\Controller\BaseController;
use App\Entity\Task\AnswerComment;
use App\Service\Course\TaskHelper;
use Doctrine\DBAL\DBALException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TaskController extends BaseController
{
    /**
     * Новый ответ
     *
     * @Route("/courses/task/answer/new", name="app_courses_task_answer_new", methods={"POST"})
     * @param  Request  $request
     * @param  TaskHelper  $taskHelper
     * @return JsonResponse
     * @throws \Exception
     */
    public function newTaskAnswer(Request $request, TaskHelper $taskHelper)
    {
        return $taskHelper->newTaskAnswer($request);
    }

    /**
     * Получить все коменты для задания
     *
     * @Route("/courses/task/comments/get", name="app_courses_task_comments_get", methods={"POST"})
     * @param  Request  $request
     * @param  TaskHelper  $taskHelper
     * @return JsonResponse
     * @throws \Exception
     */
    public function getTaskAnswerAllComments(Request $request, TaskHelper $taskHelper)
    {
        return $taskHelper->getTaskAnswerAllComments($request);
    }

    /**
     * Новый коммент
     *
     * @Route("/courses/task/answer/comment/new", name="app_courses_task_answer_comment_new", methods={"POST"})
     * @param  Request  $request
     * @param  TaskHelper  $taskHelper
     * @return JsonResponse
     * @throws DBALException
     */
    public function newTaskAnswerComment(Request $request, TaskHelper $taskHelper)
    {
        return $taskHelper->newOrEditTaskAnswerComment($request);
    }

    /**
     * Редактирование коммента
     *
     * @Route("/courses/task/answer/comment/{commentId}/edit", name="task_answer_comment_edit", methods={"POST"})
     * @ParamConverter("answerComment", options={"mapping": {"commentId": "id"}})
     * @param  Request  $request
     * @param  TaskHelper  $taskHelper
     * @return JsonResponse
     * @throws DBALException
     */
    public function editTaskAnswerComment(Request $request, TaskHelper $taskHelper, AnswerComment $answerComment)
    {
        return $taskHelper->newOrEditTaskAnswerComment($request, $answerComment);
    }

    /**
     * Редактирование статуса коммента
     *
     * @Route("/courses/task/answer/comment/{commentId}/mark", name="task_answer_comment_mark", methods={"GET"})
     * @ParamConverter("answerComment", options={"mapping": {"commentId": "id"}})
     * @param  AnswerComment  $answerComment
     * @return JsonResponse
     */
    public function markTaskAnswerComment(AnswerComment $answerComment)
    {
        if ($answerComment->getState() == AnswerComment::STATE_NOT_VIEWED) {
            $con = $this->getDoctrine()->getConnection();
            $con->update(
                'answer_comment',
                [
                    'state' => AnswerComment::STATE_VIEWED,
                ],
                [
                    'state' => AnswerComment::STATE_NOT_VIEWED,
                    'id' => $answerComment->getId(),
                ]
            );
        } elseif ($answerComment->getState() == AnswerComment::STATE_VIEWED) {
            $con = $this->getDoctrine()->getConnection();
            $con->update(
                'answer_comment',
                [
                    'state' => AnswerComment::STATE_NOT_VIEWED,
                ],
                [
                    'state' => AnswerComment::STATE_VIEWED,
                    'id' => $answerComment->getId(),
                ]
            );
        }


        return $this->getResponse(
            'setNewStatusComment',
            [
                'id' => $answerComment->getId(),
                'state' => $answerComment->getState(),
            ]
        );
    }

    /**
     * Редактирование ответа
     *
     * @Route("/courses/task/answer/edit", name="app_courses_task_answer_edit", methods={"POST"})
     */
    public function editTaskAnswer(Request $request, TaskHelper $taskHelper)
    {
        return $taskHelper->editTaskAnswer($request);
    }

    /**
     * Получить инфу по блоку
     *
     * @Route("/courses/task/info/get", name="app_courses_task_info_get", methods={"GET"})
     */
    public function getQuestionsByTask(Request $request, TaskHelper $taskHelper)
    {
        return $taskHelper->getTaskInfo($request);
    }

    /**
     * Получить инфу по блоку
     *
     * @Route("/courses/task/info/full/get", name="app_courses_task_info_full_get", methods={"GET"})
     */
    public function getQuestionsFullByTask(Request $request, TaskHelper $taskHelper)
    {
        return $taskHelper->getTaskInfo($request, true);
    }

    /**
     * Ответ на тест
     * @Route("/courses/task/test/answer", name="app_courses_task_test_answer", methods={"POST"})
     */
    public function newTaskTestAnswer(Request $request, TaskHelper $taskHelper)
    {
        return $taskHelper->newTaskTestAnswer($request);
    }

    /**
     * Ответ на задание с вводом правильного ответа из списка
     * @Route("/courses/task/type5/answer", name="app_courses_task_type5_answer", methods={"POST"})
     * @param  Request  $request
     * @param  TaskHelper  $taskHelper
     * @return JsonResponse
     * @IsGranted("ROLE_USER")
     */
    public function newTaskType5Answer(Request $request, TaskHelper $taskHelper)
    {
        return $taskHelper->newTaskType5Answer($request);
    }

}

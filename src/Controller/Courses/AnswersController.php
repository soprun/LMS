<?php

namespace App\Controller\Courses;

use App\Entity\Courses\Course;
use App\Service\Course\AnswerCheckHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class AnswersController extends AbstractController
{

    /**
     * Лента ответов для пользователя
     * ToDo: так то сделать надо бы
     *
     * @Route("/answers/feed", name="app_answers_feed", methods={"GET"})
     */
    public function pageAnswersFeed()
    {
        return $this->render('lms_vue.html.twig');
    }

    /**
     * Лента ответов для пользователя
     * ToDo: так то сделать надо бы
     *
     * @Route("/answers/feed/course/{courseId}/get", name="app_answers_feed_get", methods={"GET"})
     * @ParamConverter("course", options={"mapping": {"courseId": "id"}})
     */
    public function getAnswersFeed(AnswerCheckHelper $answerCheckHelper, Course $course)
    {
        return $answerCheckHelper->getUsersAnswersByCourse($course);
    }


    /**
     * Страница проверки заданий
     * @Route("/answers/validating", name="app_answers_validating", methods={"GET"})
     */
    public function pageAnswersValidating()
    {
        return $this->render('lms_vue.html.twig');
    }

    /**
     * Получение списка папок-курсов для селекта курса
     * @Route("/answers/validating/courses/list", name="app_answers_validating_courses_list", methods={"GET"})
     * @IsGranted("USER_IS_VALIDATING")
     * @param  AnswerCheckHelper  $answerCheckHelper
     * @return JsonResponse
     */
    public function getCourseList(AnswerCheckHelper $answerCheckHelper)
    {
        return $answerCheckHelper->getCourseList();
    }

    /**
     * Получение списка заданий с новыми ответами
     * @Route("/answers/validating/course/{courseId}/list", name="app_answers_validating_course_tasks_list")
     * @ParamConverter("course", options={"mapping": {"courseId": "id"}})
     * @IsGranted("USER_IS_VALIDATING")
     * @param  AnswerCheckHelper  $answerCheckHelper
     * @param  Course  $course
     * @return JsonResponse
     */
    public function getTasksForValidatingByCourse(AnswerCheckHelper $answerCheckHelper, Course $course)
    {
        return $answerCheckHelper->getTasksByCourse($course, true);
    }

    /**
     * Получить один ответ на проверку по проверяющему
     *
     * @Route("/answers/validating/one/random/get", name="app_answer_random_by_task_get", methods={"GET"})
     * @IsGranted("USER_IS_VALIDATING")
     * @param  Request  $request
     * @param  AnswerCheckHelper  $answerCheckHelper
     * @return JsonResponse
     */
    public function getRandomAnswersByTaskFull(Request $request, AnswerCheckHelper $answerCheckHelper)
    {
        return $answerCheckHelper->getOneRandomAnswerByTask($request);
    }

    /**
     * Лента ответов для проверяющего
     *
     * @Route("/answers/validating/feed", name="app_answers_validating_feed", methods={"GET"})
     */
    public function pageAnswersValidatingFeed()
    {
        return $this->render('lms_vue.html.twig');
    }

    /**
     * Получение списка заданий
     *
     * @Route("/answers/validating/feed/course/{courseId}/list/get", name="app_answers_course_feed_list_get")
     * @ParamConverter("course", options={"mapping": {"courseId": "id"}})
     * @IsGranted("USER_IS_VALIDATING")
     * @param  AnswerCheckHelper  $answerCheckHelper
     * @param  Course  $course
     * @return JsonResponse
     */
    public function getCourseFeedList(AnswerCheckHelper $answerCheckHelper, Course $course)
    {
        return $answerCheckHelper->getTasksByCourse($course, false);
    }

    /**
     * Лента ответов для админов
     *
     * @Route("/answers/validating/feed/get", name="app_answers_validating_feed_get", methods={"GET"})
     * @IsGranted("USER_IS_VALIDATING")
     */
    public function getAnswersValidatingFeed(Request $request, AnswerCheckHelper $answerCheckHelper)
    {
        return $answerCheckHelper->getAnswersByTask($request);
    }

    /**
     * Отправить результат проверки
     *
     * @Route("/answers/validating/check", name="app_answers_validating_check", methods={"POST"})
     * @IsGranted("USER_IS_VALIDATING_CHECK")
     */
    public function checkAnswer(Request $request, AnswerCheckHelper $answerCheckHelper)
    {
        return $answerCheckHelper->checkAnswer($request);
    }


    /**
     * Лента коментов для проверяющего
     *
     * @Route("/answers/comments", name="app_answers_comments_feed", methods={"GET"})
     */
    public function pageAnswersCommentsFeed()
    {
        return $this->render('lms_vue.html.twig');
    }

    /**
     * Лента коментов для проверяющего
     *
     * @Route("/answers/comments/all/get", name="app_answers_comments_all_get", methods={"GET"})
     * @IsGranted("USER_IS_VALIDATING")
     */
    public function getAnswersCommentsAll(Request $request, AnswerCheckHelper $answerCheckHelper)
    {
        return $answerCheckHelper->getCommentsAll($request);
    }

    /**
     * Лента коментов для проверяющего
     *
     * @Route("/answers/comments/get", name="app_answers_comments_feed_get", methods={"GET"})
     * @IsGranted("USER_IS_VALIDATING")
     */
    public function getAnswersCommentsFeed(Request $request, AnswerCheckHelper $answerCheckHelper)
    {
        return $answerCheckHelper->getCommentsByTask($request);
    }

}

<?php

namespace App\Controller\Courses;

use App\Controller\BaseController;
use App\Entity\User;
use App\Entity\UserFirebaseToken;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PushTokenController extends BaseController
{
    /**
     * Получение FCM токена от клиента и сохранение его в БД
     *
     * @Route("/api/firebase-token/update", name="update_user_token", methods={"POST"})
     * @param  Request  $request
     * @return JsonResponse
     */

    public function setNewToken(Request $request): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$user->getId()) {
            return $this->getResponse('setToken');
        }

        $clientFCMToken = $request->request->get('clientFCMToken');

        $em = $this->getDoctrine()->getManager();
        $DBTokens = $em->getRepository(UserFirebaseToken::class)->findBy(['user' => $user]);
        foreach ($DBTokens as $DBToken) {
            if ($clientFCMToken == $DBToken->getToken()) {
                return $this->getResponse('setToken');
            }
        }
        $firebaseToken = new UserFirebaseToken();
        $firebaseToken->setUser($user);
        $firebaseToken->setToken($clientFCMToken);
        $em->persist($firebaseToken);

        $em->flush();

        return $this->getResponse('setToken');
    }

}

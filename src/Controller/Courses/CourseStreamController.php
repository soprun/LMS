<?php

namespace App\Controller\Courses;

use App\Controller\BaseController;
use App\Entity\CourseStream;
use App\Entity\TeamFolder;
use App\Form\CourseStreamType;
use App\Repository\CourseStreamRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Service\UserHelper;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Exception;

/**
 * @OA\Tag(name="Потоки")
 * @IsGranted("ROLE_ADMIN")
 */
class CourseStreamController extends BaseController
{

    /**
     * Получение списка потоков
     * @Rest\Get("/api/course_streams", name="course_streams_list")
     * @Rest\View(serializerGroups={"courseStream:list"})
     *
     * @OA\Response(response=Response::HTTP_OK, description="Добавить поток",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=CourseStream::class, groups={"courseStream:list"}))
     *     )
     * )
     *
     * @param Request $request
     * @param CourseStreamRepository $courseStreamRepository
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function list(Request $request, CourseStreamRepository $courseStreamRepository): JsonResponse
    {
        $data = $courseStreamRepository
            ->createQueryBuilder('cs')
            ->getQuery()
            ->getResult();

        return $data;
    }


    /**
     * Создание нового потока
     * @Rest\Post("/api/course_streams", name="course_streams_create")
     * @Rest\View(serializerGroups={"courseStream:list"})
     *
     * @OA\RequestBody(@Model(type=CourseStreamType::class))
     *
     * @OA\Response(response=Response::HTTP_CREATED, description="Добавить поток",
     *     @OA\JsonContent(
     *        type="object",
     *        @OA\Items(ref=@Model(type=CourseStream::class, groups={"courseStream:list"}))
     *     )
     * )
     *
     * @param Request $request
     *
     * @return CourseStream
     */
    public function create(Request $request): CourseStream
    {
        $courseStream = $this->fillByForm(CourseStreamType::class, $request, new CourseStream());
        $teamFolder = new TeamFolder();
        $teamFolder->setName($courseStream->getName());
        $teamFolder->setCourseStream($courseStream);

        $this->em->persist($courseStream);
        $this->em->persist($teamFolder);
        $this->em->flush();

        return $courseStream;
    }

    /**
     * Редактирование потока
     * @Rest\Patch("/api/course_streams/{courseStream<\d+>}", name="course_streams_edit")
     * @Rest\View(serializerGroups={"courseStream:list"})
     *
     * @OA\Parameter(name="course_stream", in="path", @OA\Schema(type="integer"), description="id потока")
     *
     * @OA\RequestBody(@Model(type=CourseStreamType::class))
     *
     * @OA\Response(response=Response::HTTP_OK, description="Редактировать поток",
     *     @OA\JsonContent(
     *        type="object",
     *        @OA\Items(ref=@Model(type=CourseStream::class, groups={"courseStream:list"}))
     *     )
     * )
     *
     * @param CourseStream $courseStream
     * @param Request $request
     *
     * @return CourseStream
     */
    public function edit(CourseStream $courseStream, Request $request): CourseStream
    {
        $courseStream = $this->fillByForm(CourseStreamType::class, $request, $courseStream);
        $this->em->flush();

        return $courseStream;
    }


    /**
     * Удаление потока
     * @Rest\Delete("/api/course_streams/{courseStream<\d+>}", name="course_streams_delete")
     * @Rest\View()
     *
     * @OA\Parameter(name="course_stream", in="path", @OA\Schema(type="integer"), description="id потока")
     *
     * @OA\Response(response=Response::HTTP_NO_CONTENT, description="удалить поток")
     */
    public function delete(CourseStream $courseStream)
    {
        $this->em->remove($courseStream);
        $this->em->flush();

        return [];
    }

}

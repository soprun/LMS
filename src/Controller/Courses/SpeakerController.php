<?php

namespace App\Controller\Courses;

use App\Service\Course\SpeakerHelper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SpeakerController extends AbstractController
{

    /**
     * Folder with courses inside
     *
     * @Route("/speaker/list/get", name="app_speaker_list_get", methods={"GET"})
     */
    public function pageCourseFolder(Request $request, SpeakerHelper $speakerHelper)
    {
        return $speakerHelper->getSpeakers($request);
    }

}

<?php

namespace App\Controller\Courses;

use App\Controller\BaseController;
use App\Entity\AbstractCourse;
use App\Entity\CourseStream;
use App\Form\AbstractCourseType;
use App\Repository\AbstractCourseRepository;
use App\Service\SmartSenderHelper;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Service\UserHelper;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Exception;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * @OA\Tag(name="Курсы")
 */
class AbstractCourseController extends BaseController
{
    public function __construct(
        EntityManagerInterface $em,
        NormalizerInterface $normalizer
    ) {
        parent::__construct($em, $normalizer);
    }

    /**
     * Получение списка курсов
     * @Rest\Get("/api/abstract_courses", name="abstract_courses_list")
     * @Rest\View(serializerGroups={"abstractCourse:info"})
     *
     *
     * @OA\Response(response=Response::HTTP_OK, description="Добавить партнера",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=AbstractCourse::class, groups={"abstractCourse:info"}))
     *     )
     * )
     *
     * @param Request $request
     * @param AbstractCourseRepository $abstractCourseRepository
     *
     * @throws Exception
     */
    public function list(Request $request, AbstractCourseRepository $abstractCourseRepository)
    {
        $data = $abstractCourseRepository
            ->createQueryBuilder('ac')
            ->getQuery()
            ->getResult();

        return $data;
    }


    /**
     * Создание нового курса
     * @Rest\Post("/api/abstract_courses", name="abstract_courses_create")
     * @Rest\View(serializerGroups={"abstractCourse:info"})
     *
     * @OA\RequestBody(@Model(type=AbstractCourseType::class))
     *
     * @OA\Response(response=Response::HTTP_CREATED, description="Добавить курс",
     *     @OA\JsonContent(
     *        type="object",
     *        @OA\Items(ref=@Model(type=AbstractCourse::class, groups={"abstractCourse:info"}))
     *     )
     * )
     *
     * @param Request $request
     *
     * @return AbstractCourse
     */
    public function create(Request $request): AbstractCourse
    {
        $abstractCourse = $this->fillByForm(AbstractCourseType::class, $request, new AbstractCourse());
        $this->em->persist($abstractCourse);
        $this->em->flush();

        return $abstractCourse;
    }


    /**
     * Редактирование курса
     * @Rest\Patch("/api/abstract_courses/{abstractCourse<\d+>}", name="abstract_courses_edit")
     * @Rest\View(serializerGroups={"abstractCourse:info"})
     *
     * @OA\Parameter(name="abstract_course", in="path", @OA\Schema(type="integer"), description="id курса")
     *
     * @OA\RequestBody(@Model(type=AbstractCourseType::class))
     *
     * @OA\Response(response=Response::HTTP_OK, description="Редактировать курс",
     *     @OA\JsonContent(
     *        type="object",
     *        @OA\Items(ref=@Model(type=AbstractCourse::class, groups={"abstractCourse:info"}))
     *     )
     * )
     *
     * @param AbstractCourse $abstractCourse
     * @param Request $request
     *
     * @return AbstractCourse
     */
    public function edit(AbstractCourse $abstractCourse, Request $request): AbstractCourse
    {
        $abstractCourse = $this->fillByForm(AbstractCourseType::class, $request, $abstractCourse);
        $this->em->flush();

        return $abstractCourse;
    }

    /**
     * Удаление курса
     * @Rest\Delete("/api/abstract_courses/{abstractCourse<\d+>}", name="abstract_courses_delete")
     * @Rest\View()
     *
     * @OA\Parameter(name="abstract_course", in="path", @OA\Schema(type="integer"), description="id курса")
     *
     * @OA\Response(response=Response::HTTP_NO_CONTENT, description="удалить курс")
     */
    public function delete(AbstractCourse $abstractCourse)
    {
        $this->em->remove($abstractCourse);
        $this->em->flush();

        return [];
    }

}

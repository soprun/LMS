<?php

namespace App\Controller\Courses;

use App\Controller\BaseController;
use App\Entity\Courses\Course;
use App\Entity\Courses\Lesson;
use App\Service\Course\BlockHelper;
use App\Service\Course\LessonHelper;
use App\Service\DuplicateEntityService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class LessonController extends BaseController
{

    /**
     * Страница урока
     * @Route("/courses/lesson/{lessonId}", name="app_courses_lesson", methods={"GET"})
     */
    public function pageLesson()
    {
        return $this->render('lms_vue.html.twig');
    }

    /**
     * Получить список блоков для урока
     *
     * @Route("/courses/lesson/{lessonId}/blocks/get", name="app_courses_lesson_get", methods={"GET"})
     *
     * @ParamConverter("lesson", options={"mapping": {"lessonId": "id"}})
     * @IsGranted("USER_IN_LESSON",subject="lesson")
     */
    public function pageLessonGet(Lesson $lesson, BlockHelper $blockHelper)
    {
        return $blockHelper->getLessonBlocks($lesson);
    }

    /**
     * New lesson
     *
     * @Route("/courses/course/{courseId}/lesson/new", name="app_courses_lesson_new", methods={"POST"})
     *
     * @ParamConverter("course", options={"mapping": {"courseId": "id"}})
     * @IsGranted("USER_LESSON_CREATE_BY_COURSE",subject="course")
     */
    public function lessonNew(Request $request, LessonHelper $lessonHelper, Course $course)
    {
        return $lessonHelper->newLesson($course, $request);
    }

    /**
     * @Route("/courses/lesson/{lessonId}/duplicate", name="app_courses_lesson_duplicate", methods={"POST"})
     *
     * @ParamConverter("lesson", options={"mapping": {"lessonId": "id"}})
     * @IsGranted("USER_LESSON_CREATE_BY_LESSON", subject="lesson")
     */
    public function lessonDuplicate(
        Lesson $lesson,
        DuplicateEntityService $duplicateEntityService
    ): JsonResponse {
        $lesson = $duplicateEntityService->duplicateLesson($lesson, $lesson->getCourse(), false);

        return $this->getResponseNew($lesson, ['lesson:info']);
    }

    /**
     * @Route("/courses/lesson/{lessonId}/edit", name="app_courses_lesson_edit", methods={"POST"})
     *
     * @ParamConverter("lesson", options={"mapping": {"lessonId": "id"}})
     * @IsGranted("USER_LESSON_EDIT",subject="lesson")
     */
    public function lessonEdit(Request $request, LessonHelper $lessonHelper, Lesson $lesson)
    {
        return $lessonHelper->editLesson($lesson, $request);
    }

    /**
     * Lesson
     *
     * @Route("/courses/lesson/{lessonId}/position/up", name="app_courses_lesson_position_up", methods={"GET"})
     *
     * @ParamConverter("lesson", options={"mapping": {"lessonId": "id"}})
     * @IsGranted("USER_LESSON_EDIT",subject="lesson")
     */
    public function lessonPositionUp(Lesson $lesson, LessonHelper $lessonHelper)
    {
        return $lessonHelper->lessonPositionUp($lesson);
    }

    /**
     * Lesson
     *
     * @Route("/courses/lesson/{lessonId}/position/down", name="app_courses_lesson_position_down", methods={"GET"})
     *
     * @ParamConverter("lesson", options={"mapping": {"lessonId": "id"}})
     * @IsGranted("USER_LESSON_EDIT",subject="lesson")
     */
    public function lessonPositionDown(Lesson $lesson, LessonHelper $lessonHelper)
    {
        return $lessonHelper->lessonPositionDown($lesson);
    }

    /**
     * @Route("/courses/lesson/{lesson}/delete", name="app_courses_lesson_delete", methods={"DELETE"})
     * @IsGranted("USER_LESSON_DELETE", subject="lesson")
     */
    public function lessonDelete(Lesson $lesson): JsonResponse
    {
        //убираем в конец, чтобы не повреждалась сортировка оставшихся
        $lesson->setPosition(-1);
        $this->em->persist($lesson);
        $this->em->flush();
        $lesson->setDeleted(true);
        $this->em->persist($lesson);
        $this->em->flush();

        return $this->getResponse(
            'deleteLesson',
            ['id' => $lesson->getId()]
        );
    }

}

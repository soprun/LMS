<?php

namespace App\Controller\Courses;

use App\Entity\Courses\LessonPart;
use App\Service\Course\BlockHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BlockController extends AbstractController
{

    /**
     * New block
     *
     * @Route("/courses/part/{lessonPartId}/block/new", name="app_courses_block_new", methods={"POST"})
     *
     * @ParamConverter("lessonPart", options={"mapping": {"lessonPartId": "id"}})
     */
    public function blockNew(Request $request, LessonPart $lessonPart, BlockHelper $blockHelper)
    {
        return $blockHelper->newLessonBlock($lessonPart, $request);
    }

    /**
     * Edit block
     *
     * @Route("/courses/block/edit", name="app_courses_block_edit", methods={"POST"})
     */
    public function blockEdit(Request $request, BlockHelper $blockHelper)
    {
        return $blockHelper->editLessonBlock($request);
    }

    /**
     * Duplicate block
     *
     * @Route("/courses/block/duplicate", name="app_courses_block_duplicate", methods={"POST"})
     */
    public function blockDuplicate(Request $request, BlockHelper $blockHelper)
    {
        return $blockHelper->duplicateLessonBlock($request);
    }

    /**
     * Delete block
     *
     * @Route("/courses/block/delete", name="app_courses_block_delete", methods={"DELETE"})
     */
    public function blockDelete(Request $request, BlockHelper $blockHelper)
    {
        return $blockHelper->deleteLessonBlock($request);
    }

    /**
     * Delete file from block
     *
     * @Route("/courses/block/files/delete", name="app_courses_block_file_delete", methods={"DELETE"})
     */
    public function blockFileDelete(Request $request, BlockHelper $blockHelper)
    {
        return $blockHelper->deleteFileFromBlock($request);
    }

    /**
     * Up block
     *
     * @Route("/courses/block/up", name="app_courses_block_up", methods={"POST"})
     */
    public function blockUp(Request $request, BlockHelper $blockHelper)
    {
        return $blockHelper->upBlock($request);
    }

    /**
     * Down block
     *
     * @Route("/courses/block/down", name="app_courses_block_down", methods={"POST"})
     */
    public function blockDown(Request $request, BlockHelper $blockHelper)
    {
        return $blockHelper->downBlock($request);
    }

    /**
     * Получение инфы по одному блоку
     * @Route("/courses/block/info/get", name="app_courses_block_info_get", methods={"GET"})
     */
    public function getBlockInfo(Request $request, BlockHelper $blockHelper)
    {
        return $blockHelper->getBlockInfo($request);
    }

}

<?php

namespace App\Controller\Courses;

use App\Controller\BaseController;
use App\Entity\Courses\Course;
use App\Entity\PointsShop\Order;
use App\Entity\PointsShop\Product;
use App\Entity\User;
use App\Service\Course\FileHelper;
use App\Service\LikeAdminHelper;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/courses/course/{courseId}/points_shop", name="app_courses_course_points_shop_")
 * @ParamConverter("course", options={"mapping": {"courseId": "id"}})
 */
class PointsShopController extends BaseController
{
    /**
     * @Route("", name="page")
     * @Route("/history", name="history_page")
     */
    public function pageAction()
    {
        return $this->render('lms_vue.html.twig');
    }

    /**
     * @Route("", name="shop")
     * @Route("/shop", name="shop")
     */
    public function shopAction()
    {
        return $this->render('lms_vue.html.twig');
    }

    /**
     * @Route("/get", name="products_get")
     * @param  Course  $course
     * @return JsonResponse
     * @IsGranted("ROLE_USER")
     */
    public function getProductsAction(Course $course, FileHelper $fileHelper): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();

        [$points, $balance] = $this->getUserShopPointsAndBalance($user, $course);

        $products = [];
        foreach ($this->getDoctrine()->getRepository(Product::class)->findNotDeleted() as $product) {
            /** @var Product $product */
            $products[] = [
                'id' => $product->getId(),
                'name' => $product->getName(),
                'title' => $product->getTitle(),
                'description' => $product->getDescription(),
                'fullDescription' => $product->getFullDescription() ?? '',
                'url' => $product->getUrl() ?? '',
                'price' => $product->getPrice(),
                'buttonText' => $product->getButtonText(),
                'img' => $product->getCoverImg() ? $fileHelper->getFileUrl($product->getCoverImg()) : '',
            ];
        }

        return $this->getResponse(
            'getProducts',
            [
                'courseName' => $course->getName(),
                'points' => $points,
                'balance' => $balance,
                'products' => $products,
            ]
        );
    }

    /**
     * @Route("/history/get", name="history_get")
     * @param  Course  $course
     * @return JsonResponse
     * @IsGranted("ROLE_USER")
     */
    public function getHistoryAction(Course $course): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();

        $orders = [];
        foreach ($user->getPointsShopOrders() as $order) {
            $orders[] = [
                'id' => $order->getId(),
                'price' => $order->getPrice(),
                'name' => $order->getProduct()->getName(),
                'promocode' => $order->getPromocode(),
                'createdAt' => $order->getCreatedAt()->format("Y-m-d H:i:s"),
            ];
        }

        return $this->getResponse(
            'getHistory',
            [
                'courseName' => $course->getName(),
                'orders' => $orders,
            ]
        );
    }

    /**
     * @Route("/order", name="order", methods={"POST"})
     * @param  Course  $course
     * @param  Request  $request
     * @param  EntityManagerInterface  $em
     * @param  LikeAdminHelper  $likeAdminHelper
     * @return JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @IsGranted("ROLE_USER")
     */
    public function orderAction(
        Course $course,
        Request $request,
        EntityManagerInterface $em,
        LikeAdminHelper $likeAdminHelper
    ): JsonResponse {
        /** @var User $user */
        $user = $this->getUser();

        /** @var Product|null $product */
        $product = $em->getRepository(Product::class)
            ->find($request->get('productId'));

        if (!$product) {
            $this->addError('productId', 'Продукт не найден');

            return $this->getResponse('order');
        }

        // проверяем баланс
        [$points, $balance] = $this->getUserShopPointsAndBalance($user, $course);
        if ($product->getPrice() > $balance) {
            $this->addError('balance', 'Недостаточно баллов для покупки');

            return $this->getResponse('order');
        }

        // отправляем запрос на лц
        [$response, $errors] = $likeAdminHelper->generatePromocode($product, $user);
        if (!empty($errors)) {
            $this->addError('unkown', 'Непредвиденная ошибка сервера. Попробуйте позже');

            return $this->getResponse('order');
        }

        $promocode = reset($response)->code;

        // сохраняем покупку
        $order = new Order();
        $order->setUser($user);
        $order->setProduct($product);
        $order->setPrice($product->getPrice());
        $order->setPromocode($promocode);
        $em->persist($order);
        $em->flush();

        return $this->getResponse(
            'order',
            [
                'promocode' => $promocode,
            ]
        );
    }

    private function getUserShopPointsAndBalance(User $user, Course $course)
    {
        $balance = $points = $this->getDoctrine()->getRepository(Course::class)
            ->getUserPointsSum($course, $user);

        foreach ($user->getPointsShopOrders() as $order) {
            $balance -= $order->getPrice();
        }

        return [$points, $balance];
    }


}

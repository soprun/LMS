<?php

namespace App\Controller\Exports;

use App\Controller\BaseController;
use App\Entity\AbstractCourse;
use App\Entity\CourseStream;
use App\Entity\MarathonInvestment\MarathonInvestment;
use App\Entity\MarathonInvestment\MarathonInvestmentConfiguration;
use App\Entity\Traction\Value;
use App\Entity\User;
use App\Entity\UserGroup;
use App\Exception\AnalyticsCommonServiceException;
use App\Repository\CourseStreamRepository;
use App\Repository\UserGroupRepository;
use App\Repository\UserRepository;
use App\Service\Analytics\AnalyticsCommonService;
use App\Service\AuthServerHelper;
use App\Service\AuthServerService;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * @Rest\Route("/api/exports/tmp")
 */
class TemporaryExportsController extends BaseController
{
    public const DEFAULT_SEPARATOR = ';';
    public const MAX_PAGE_SIZE = 500;

    /**
     * LMS-273
     *
     * @Rest\Get("/telegram", name="telegram")
     * @IsGranted("USER_ANALYTICS_VIEW")
     *
     * @param Request $request
     * @param UserRepository $userRepository
     * @param AuthServerHelper $authServerHelper
     *
     * @return Response
     */
    public function getTelegramNicknames(
        Request $request,
        UserRepository $userRepository,
        AuthServerHelper $authServerHelper
    ): Response {
        $groupName = $request->query->get('group');
        $fields = [
            'id',
            'name',
            'lastname',
            'patronymic',
            'email',
            'phone',
            'telegram',
            'city',
        ];
        $users = $userRepository->createQueryBuilder('u')
            ->select('u.authUserId')
            ->join('u.groupRelations', 'ugu')
            ->join('ugu.userGroup', 'ug')
            ->where('ug.name = :groupName AND ugu.deleted = 0')
            ->setParameter('groupName', $groupName)
            ->getQuery()
            ->getResult();
        $authIds = array_column($users, 'authUserId');
        $users = $authServerHelper->getUsers($authIds, $fields);
        $response = [
            implode(static::DEFAULT_SEPARATOR, $fields),
        ];
        foreach ($users as $user) {
            if (!empty($user['telegram'])) {
                $response[] = implode(static::DEFAULT_SEPARATOR, $user);
            }
        }

        return new Response(implode(PHP_EOL, $response), Response::HTTP_OK, [
            'Content-Type' => 'text/plain;charset=utf-8',
        ]);
    }

    /**
     * @Rest\Get("/fastcash/{stream<\d+>}", name="fastcash")
     * @IsGranted("USER_ANALYTICS_VIEW")
     *
     * @param CourseStream $stream
     * @param AuthServerService $authServerService
     *
     * @return Response
     * @throws \JsonException
     */
    public function fastcashTractionAction(
        CourseStream $stream,
        AuthServerService $authServerService
    ) {
        /** @var User[] $users */
        $users = $this->em->getRepository(User::class)
            ->createQueryBuilder('user')
            ->join('user.groupRelations', 'groupRelation')
            ->join('groupRelation.userGroup', 'userGroup')
            ->where('userGroup.courseStream = :courseStream')
            ->setParameter('courseStream', $stream)
            ->getQuery()
            ->getResult();

        $authServerService->refillUsers($users);

        /** @var Value[] $values */
        $values = $this->em->getRepository(Value::class)
            ->createQueryBuilder('value')
            ->addSelect('type')
            ->join('value.type', 'type')
            ->join('value.owner', 'user')
            ->join('user.groupRelations', 'groupRelation')
            ->join('groupRelation.userGroup', 'userGroup')
            ->where('userGroup.courseStream = :courseStream')
            ->andWhere('value.value > 0')
            ->andWhere('value.date between :startDate and :endDate')
            ->setParameter('courseStream', $stream)
            ->setParameter('startDate', $stream->getStartDate())
            ->setParameter('endDate', (clone $stream->getStartDate())->modify('+13 day'))
            ->getQuery()
            ->getResult();

        $csv = [
            [
                'lastname' => 'Фамилия',
                'name' => 'Имя',
                'patronymic' => 'Отчество',
                'email' => 'Email',
                'phone' => 'Телефон',
                'city' => 'Город',
            ]
        ];
        for ($i = 0; $i < 14; $i++) {
            $dateString = (clone $stream->getStartDate())->modify("+{$i} day")->format('Y-m-d');
            $csv[0]['Прибыль ' . $dateString] = 'Прибыль ' . $dateString;
        }
        for ($i = 0; $i < 14; $i++) {
            $dateString = (clone $stream->getStartDate())->modify("+{$i} day")->format('Y-m-d');
            $csv[0]['Выручка ' . $dateString] = 'Выручка ' . $dateString;
        }
        foreach ($users as $user) {
            $csv[$user->getId()] = [
                'lastname' => $user->getLastname(),
                'name' => $user->getName(),
                'patronymic' => $user->getPatronymic(),
                'email' => $user->getEmail(),
                'phone' => $user->getPhone(),
                'city' => $user->getCity(),
            ];
            for ($i = 0; $i < 14; $i++) {
                $dateString = (clone $stream->getStartDate())->modify("+{$i} day")->format('Y-m-d');
                $csv[$user->getId()]['Прибыль ' . $dateString] = '';
            }
            for ($i = 0; $i < 14; $i++) {
                $dateString = (clone $stream->getStartDate())->modify("+{$i} day")->format('Y-m-d');
                $csv[$user->getId()]['Выручка ' . $dateString] = '';
            }
        }

        foreach ($values as $value) {
            $dateString = $value->getDate()->format('Y-m-d');
            $typeTitle = $value->getType()->getTitle();
            $csv[$value->getOwnerId()]["{$typeTitle} {$dateString}"] = $value->getValue();
        }

        // финальная сборка
        $response = [];
        foreach ($csv as $row) {
            $response[] = implode(static::DEFAULT_SEPARATOR, $row);
        }

        return new Response(implode(PHP_EOL, $response), Response::HTTP_OK, [
            'Content-Type' => 'text/plain;charset=utf-8',
        ]);
    }

    /**
     * LMS-285
     *
     * @Rest\Get("/lf-msa", name="lf_and_msa")
     * @IsGranted("USER_ANALYTICS_VIEW")
     *
     * @param Request $request
     * @param CourseStreamRepository $courseStreamRepository
     * @param AuthServerHelper $authServerHelper
     * @param UserGroupRepository $userGroupRepository
     *
     * @return Response
     */
    public function getLfAndMsa(
        Request $request,
        CourseStreamRepository $courseStreamRepository,
        AuthServerHelper $authServerHelper,
        UserGroupRepository $userGroupRepository,
        UserRepository $userRepository,
        AnalyticsCommonService $analyticsCommonService
    ): Response {
        $courseStreamName = $request->query->get('stream');
        $courseStream = $courseStreamRepository->findOneBy([
            'name' => $courseStreamName,
        ]);
        $needleUserGroupsNames = [
            'новый мса',
            'like family',
        ];
        if (!$courseStream) {
            throw $this->createNotFoundException('Поток не найден');
        }
        $fields = ['id', 'name', 'lastname', 'patronymic', 'email', 'phone', 'telegram', 'city'];
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $userGroupRepository->createQueryBuilder('ug');
        $orX = $queryBuilder->expr()->orX();
        foreach ($needleUserGroupsNames as $needleUserGroupsName) {
            $orX->add(
                $queryBuilder->expr()->like('ug.name', "'%$needleUserGroupsName%'")
            );
        }
        $users = $queryBuilder
            ->select('u.id')
            ->join('ug.userRelations', 'ugu')
            ->join('ugu.user', 'u')
            ->where('ugu.deleted = 0')
            ->andWhere($orX)
            ->getQuery()
            ->getResult();
        $users = $userGroupRepository->createQueryBuilder('ug')
            ->select([
                'u.id',
                'u.authUserId',
                'ugu.amoLeadId',
                'IDENTITY(ugu.addedBy) as addedBy',
                'ugu.budget',
                'ug.name as groupName',
                'IDENTITY(ug.alternativeGroup) as alternativeGroupName',
                'f.name as facultyName',
                't.id as teamId',
                't.name as teamName',
                'IDENTITY(t.captain) as captain',
                'tr.name as tariffName',
            ])
            ->join('ug.courseStream', 'cs')
            ->join('ug.userRelations', 'ugu')
            ->join('ugu.user', 'u')
            ->join('ugu.faculty', 'f')
            ->join('u.teams', 't')
            ->join('u.userCourseTariffs', 'uct')
            ->join('uct.tariff', 'tr')
            ->where('ugu.deleted = 0 AND cs.name = :courseStreamName')
            ->andWhere('ugu.user IN(:users)')
            ->setParameter('courseStreamName', $courseStream->getName())
            ->setParameter('users', $users)
            ->getQuery()
            ->getResult();
        // сбор доп данных
        $rowData = [];
        $captains = [];
        foreach ($users as &$user) {
            $captains[$user['captain']] = $user['captain'];
            if (!empty($user['alternativeGroupName'])) {
                /** @var UserGroup $group */
                $group = $this->em->getRepository(UserGroup::class)->find($user['alternativeGroupName']);
                $user['alternativeGroupName'] = $group->getName();
            }
            $rowData[$user['id']] = $user;
        }
        if (!empty($captains)) {
            $captainsIds = $userRepository->createQueryBuilder('u')
                ->select('u.authUserId')
                ->where('u IN(:users)')
                ->setParameter('users', $captains)
                ->getQuery()
                ->getResult();
            $captainsIds = array_column($captainsIds, 'authUserId');
            $captainsIds = $authServerHelper->getUsers($captainsIds, ['id', 'name', 'lastname']);
            foreach ($captainsIds as $captainsId) {
                $captains[$captainsId['id']] = sprintf('%s %s', $captainsId['lastname'], $captainsId['name']);
            }
        }
        // сбор инфы по трекшену (выручка, прибыль)
        foreach ($users as $user) {
            $rowData[$user['id']]['week 1 profit'] = 0;
            $rowData[$user['id']]['week 1 revenue'] = 0;
            $rowData[$user['id']]['week 2 profit'] = 0;
            $rowData[$user['id']]['week 2 revenue'] = 0;
            $rowData[$user['id']]['week 3 profit'] = 0;
            $rowData[$user['id']]['week 3 revenue'] = 0;
            $rowData[$user['id']]['week 4 profit'] = 0;
            $rowData[$user['id']]['week 4 revenue'] = 0;
            $rowData[$user['id']]['week 5 profit'] = 0;
            $rowData[$user['id']]['week 5 revenue'] = 0;
            $rowData[$user['id']]['week 6 profit'] = 0;
            $rowData[$user['id']]['week 6 revenue'] = 0;
            $rowData[$user['id']]['week 7 profit'] = 0;
            $rowData[$user['id']]['week 7 revenue'] = 0;
            $rowData[$user['id']]['captainName'] = isset($captains[$user['captain']])
                ? $captains[$user['captain']] : '';

            $documents = $analyticsCommonService->getTractionByUserId($user['id'], $courseStream);
            foreach ($documents as $document) {
                if ($document->week) {
                    $rowData[$user['id']]["week {$document->week} revenue"] = $document->revenue;
                    $rowData[$user['id']]["week {$document->week} profit"] = $document->profit;
                }
            }
        }
        // сбор инфы о пользователе
        $authIds = array_column($users, 'authUserId');
        $usersInfos = $authServerHelper->getUsers($authIds, $fields);
        foreach ($usersInfos as $userInfo) {
            $rowData[$userInfo['id']]['name'] = $userInfo['name'];
            $rowData[$userInfo['id']]['lastname'] = $userInfo['lastname'];
            $rowData[$userInfo['id']]['patronymic'] = $userInfo['patronymic'];
            $rowData[$userInfo['id']]['email'] = $userInfo['email'];
            $rowData[$userInfo['id']]['phone'] = $userInfo['phone'];
            $rowData[$userInfo['id']]['telegram'] = $userInfo['telegram'];
            $rowData[$userInfo['id']]['city'] = $userInfo['city'];
        }
        // финальная сборка
        $response = [];
        if (!empty($rowData)) {
            $response = [implode(static::DEFAULT_SEPARATOR, array_keys($rowData[array_key_first($rowData)]))];
            foreach ($rowData as $row) {
                $response[] = implode(static::DEFAULT_SEPARATOR, $row);
            }
        }

        return new Response(implode(PHP_EOL, $response), Response::HTTP_OK, [
            'Content-Type' => 'text/plain;charset=utf-8',
        ]);
    }

    /**
     * LMS-293
     *
     * @Rest\Get("/mi-by-group", name="mi_by_group")
     * @Rest\QueryParam(name="groupId", requirements="\d+", description="ID группы")
     * @Rest\QueryParam(name="startDate", requirements="^[0-9.]+$", description="Дата добавления пользователя в группу")
     * @Rest\QueryParam(name="page", requirements="\d+", default="1", description="Номер страницы")
     * @Rest\QueryParam(name="pageSize", requirements="\d+", default="10", description="Количество записей на странице")
     * @IsGranted("USER_ANALYTICS_VIEW")
     *
     * @param ParamFetcher $paramFetcher
     * @param UserGroupRepository $userGroupRepository
     * @param UserRepository $userRepository
     * @param AuthServerHelper $authServerHelper
     *
     * @return Response
     * @throws \Exception
     */
    public function getMiByGroups(
        ParamFetcher $paramFetcher,
        UserGroupRepository $userGroupRepository,
        UserRepository $userRepository,
        AuthServerHelper $authServerHelper
    ): Response {
        $reportHeader = ['id', 'name', 'lastname', 'patronymic', 'email', 'niche'];
        $fields = ['id', 'name', 'lastname', 'patronymic', 'email', 'niche'];
        $reportHeader[] = 'profit';
        $reportHeader[] = 'pointA';
        $reportHeader[] = 'createdAt';
        $page = (int)$paramFetcher->get('page');
        $pageSize = (int)$paramFetcher->get('pageSize');
        if ($paramFetcher->get('groupId') && $paramFetcher->get('startDate')) {
            if ($pageSize > static::MAX_PAGE_SIZE) {
                $pageSize = static::MAX_PAGE_SIZE;
            }
            $startDate = new \DateTime($paramFetcher->get('startDate'));
            /** @var UserGroup $group */
            $group = $userGroupRepository->createQueryBuilder('ug')
                ->join('ug.courseStream', 'cs')
                ->join('cs.abstractCourse', 'ac')
                ->where('ug.id = :groupId AND ac.slug = :slug')
                ->setParameter('groupId', $paramFetcher->get('groupId'))
                ->setParameter('slug', AbstractCourse::SLUG_MARATHON_INVESTMENT)
                ->getQuery()
                ->getResult();
            if ($group) {
                $miStatistics = [];
                $authUsersIds = [];
                $users = $userRepository->createQueryBuilder('u')
                    ->select('u as user, ugu.createdAt')
                    ->join('u.groupRelations', 'ugu')
                    ->where('ugu.deleted = 0 AND ugu.createdAt >= :startDate AND ugu.userGroup = :group')
                    ->setParameter('startDate', $startDate)
                    ->setParameter('group', $group)
                    ->setFirstResult(($page - 1) * $pageSize)
                    ->setMaxResults($pageSize)
                    ->getQuery()
                    ->getResult();
                foreach ($users as $user) {
                    $createdAt = $user['createdAt']->format('Y-m-d H:i:s');
                    /** @var User $user */
                    $user = $user['user'];
                    if (
                        $user->getMarathonConfigurations()->count()
                        && $user->getMarathonInvestments()->count()
                    ) {
                        $authUsersIds[] = $user->getAuthUserId();
                        // сбор общих данных по МИ
                        foreach ($user->getMarathonConfigurations() as $investment) {
                            /** @var MarathonInvestmentConfiguration $investment */
                            $miStatistics[$user->getId()]['profit'] = $investment->getProfit();
                            $miStatistics[$user->getId()]['pointA'] = $investment->getPointA();
                            $miStatistics[$user->getId()]['createdAt'] = $createdAt;
                        }
                        // сбор данных по неделям
                        foreach ($user->getMarathonInvestments() as $investment) {
                            $reportHeader[] = 'amount ' . $investment->getWeek();
                            $reportHeader[] = 'passed ' . $investment->getWeek();
                            $reportHeader[] = 'week ' . $investment->getWeek();
                            $reportHeader[] = 'planned ' . $investment->getWeek();
                            /** @var MarathonInvestment $investment */
                            $miStatistics[$user->getId()]['weeks'][] = [
                                'amount' => $investment->getAmount(),
                                'passed' => $investment->getPassed(),
                                'week' => $investment->getWeek(),
                                'planned' => $investment->getPlanned(),
                            ];
                        }
                    }
                }
                $reportHeader = array_unique($reportHeader);
                $usersInfos = $authServerHelper->getUsers($authUsersIds, $fields);
                $response = [];
                $response[] = implode(static::DEFAULT_SEPARATOR, $reportHeader);
                // финальная сборка всех данных
                foreach ($usersInfos as $usersInfo) {
                    $rowData = [
                        'id' => $usersInfo['id'],
                        'name' => $usersInfo['name'],
                        'lastname' => $usersInfo['lastname'],
                        'patronymic' => $usersInfo['patronymic'],
                        'email' => $usersInfo['email'],
                        'niche' => $usersInfo['niche'],
                        'created at' => $miStatistics[$usersInfo['id']]['createdAt'],
                        'profit' => $miStatistics[$usersInfo['id']]['profit'],
                        'pointA' => $miStatistics[$usersInfo['id']]['pointA'],
                    ];
                    foreach ($miStatistics[$usersInfo['id']]['weeks'] as $weekData) {
                        $rowData['week ' . $weekData['week']] = $weekData['week'];
                        $rowData['passed ' . $weekData['week']] = $weekData['passed'];
                        $rowData['amount ' . $weekData['week']] = $weekData['amount'];
                    }
                    $row = [];
                    foreach ($reportHeader as $header) {
                        $row[$header] = isset($rowData[$header]) ? $rowData[$header] : '';
                    }
                    $response[] = implode(static::DEFAULT_SEPARATOR, $row);
                }

                return new Response(implode(PHP_EOL, $response), Response::HTTP_OK, [
                    'Content-Type' => 'text/plain;charset=utf-8',
                ]);
            }

            return new Response('Группа не найдена', Response::HTTP_OK, [
                'Content-Type' => 'text/plain;charset=utf-8',
            ]);
        }

        return new Response('', Response::HTTP_OK, [
            'Content-Type' => 'text/plain;charset=utf-8',
        ]);
    }

    /**
     * LMS-360
     *
     * @Rest\Get("/speed-club/{stream_name}", name="speed_club")
     * @Rest\QueryParam(name="page", requirements="\d+", default="1", description="Номер страницы")
     * @Rest\QueryParam(name="pageSize", requirements="\d+", default="10", description="Количество записей на странице")
     * @ParamConverter("courseStream", options={"mapping": {"stream_name": "name"}})
     * @IsGranted("USER_ANALYTICS_VIEW")
     *
     * @param ParamFetcher $paramFetcher
     * @param CourseStream $courseStream
     * @param AnalyticsCommonService $analyticsCommonService
     *
     * @return Response
     *
     * @throws \Exception
     */
    public function getAnalyticsByStream(
        ParamFetcher $paramFetcher,
        CourseStream $courseStream,
        AnalyticsCommonService $analyticsCommonService
    ): Response {
        try {
            $report = $analyticsCommonService->getFullReportByStream(
                $courseStream,
                $paramFetcher->get('page'),
                $paramFetcher->get('pageSize'),
            );

            return new Response(implode("\n", $report), Response::HTTP_OK, [
                'Content-Type' => 'text/plain;charset=utf-8',
            ]);
        } catch (AnalyticsCommonServiceException $e) {
            $this->logger->error('Найденный курс не относится к СК', [
                'course_id' => $courseStream->getId(),
                'message' => $e->getMessage(),
                'method' => __METHOD__,
                'trace' => $e->getTrace(),
            ]);

            return $this->addError('course_id', 'Найденный курс не относится к СК')->getResponseNew();
        }
    }
}

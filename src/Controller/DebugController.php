<?php

namespace App\Controller;

use App\Entity\CourseStream;
use App\Entity\Questionnaire\UserAnswer;
use App\Entity\User;
use App\Entity\UserFirebaseToken;
use Doctrine\ORM\EntityManagerInterface;
use Kreait\Firebase\Exception\FirebaseException;
use Kreait\Firebase\Exception\Messaging\InvalidMessage;
use Kreait\Firebase\Exception\MessagingException;
use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification as FirebaseNotification;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/debug", name="debug_")
 * @IsGranted("ROLE_USER")
 *
 * Class DebugController
 * @package App\Controller
 */
class DebugController extends BaseController
{
    /**
     * Отправка пуша юзеру
     * @Route("/test-push", name="test_push")
     * @param  KernelInterface  $kernel
     * @param  EntityManagerInterface  $em
     * @return JsonResponse
     * @throws FirebaseException
     * @throws MessagingException
     */
    public function testPushAction(KernelInterface $kernel, EntityManagerInterface $em): JsonResponse
    {
        $title = "Тестовый пуш";
        $description = sprintf("Отправлен в %s по серверу", date("H:i:s"));

        $user = $this->getUser();
        $data = [];

        $factory = (new Factory())->withServiceAccount(
            $kernel->getProjectDir() .
            '/config/toolbox-263317-firebase-adminsdk-bq4ha-27979d3623.json'
        );
        $messaging = $factory->createMessaging();

        // Достаём токены всех девайсов юзера
        /** @var UserFirebaseToken[] $userDeviceTokens */
        $userDeviceTokens = $em->getRepository(UserFirebaseToken::class)
            ->findBy(['user' => $user]);

        if (empty($userDeviceTokens)) {
            $data[] = 'Нет ни одного UserFirebaseToken';
        }

        foreach ($userDeviceTokens as $userDeviceToken) {
            $tokenId = $userDeviceToken->getId();
            $exception = null;

            // Пишем сообщение
            $message = CloudMessage::withTarget('token', $userDeviceToken->getToken())
                ->withNotification(FirebaseNotification::create($title, $description));

            // Проверяем сообщение
            try {
                $messaging->validate($message);
            } catch (InvalidMessage $e) {
                $errors = $e->errors();
                // Достаём код ошибки
                $errorCode = reset(reset($errors)['details'])['errorCode'];
                // Токен устарел
                if ($errorCode === 'UNREGISTERED') {
                    // ищем все такие токены в базе
                    $oldTokens = $em->getRepository(UserFirebaseToken::class)->findBy(
                        [
                            'token' => $userDeviceToken->getToken(),
                        ]
                    );
                    foreach ($oldTokens as $oldToken) {
                        $em->remove($oldToken);
                        $em->flush();
                    }
                    $exception = "Удалили просроченные токены";
                } else {
                    $exception = "InvalidMessage exception: " . $e->getMessage();
                }
            } catch (MessagingException $e) {
                $exception = "MessagingException exception: " . $e->getMessage();
            } catch (FirebaseException $e) {
                $exception = "FirebaseException exception: " . $e->getMessage();
            }

            if ($exception) {
                $data[$tokenId] = $exception;
                continue;
            }

            // Если всё ок, отправляем сообщение
            $result = $messaging->send($message);
            $data[$tokenId] = $result;
        }

        return $this->getResponse('getUnreadNotificationsCount', $data);
    }

    /**
     * Удаление всех ответов пользователя
     * @Route("/clean-questionnaire-answers", name="clean_questionnaire_answers")
     * @param  EntityManagerInterface  $em
     * @return JsonResponse
     */
    public function cleanQuestionnaireAnswersAction(EntityManagerInterface $em): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();

        $botsCount = $user->getSmartSenderUsers()->count();
        foreach ($user->getSmartSenderUsers() as $smartSenderUser) {
            $em->remove($smartSenderUser);
        }

        $answersCount = 0;
        $stream = $this->em->getRepository(CourseStream::class)->getCurrentStream($user);
        if ($stream) {
            $answers = $this->em->getRepository(UserAnswer::class)
                ->findBy([
                    'courseStream' => $stream,
                    'user' => $user,
                ]);
            $answersCount = count($answers);
            foreach ($answers as $answer) {
                $this->em->remove($answer);
            }
        }

        $em->flush();

        return $this->getResponse(
            'cleanQuestionnaireAnswersAction',
            [
                'Удалили ответов' => $answersCount,
                'Удалили связей с ботами' => $botsCount,
            ]
        );
    }

}

<?php

namespace App\Controller;

use App\Repository\Traction\CheckpointValueRepository;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Form\Traction\CheckpointValueType;
use App\Entity\Traction\CheckpointValue;
use App\Entity\Traction\Checkpoint;
use App\Entity\CourseStream;
use App\Entity\User;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\Response;

/**
 * @OA\Tag(name="Чекбоксы трекшена")
 * @IsGranted("ROLE_USER")
 */
class CheckpointController extends BaseController
{
    /**
     * Возвращает список чекбоксов с ответами для пользователя и его курса
     *
     * @Rest\Get("/api/v2/traction/checkpoints/{user<\d+>}/{stream<\d+>}")
     * @Rest\View(serializerGroups={"checkpoint:user_value"})
     *
     * @OA\Parameter(name="stream", in="path", @OA\Schema(type="integer"), description="id потока")
     * @OA\Parameter(name="user", in="path", @OA\Schema(type="integer"), description="id пользователя")
     *
     * @OA\Response(response=Response::HTTP_OK, description="",
     *     @OA\MediaType(mediaType="application/json",
     *         @OA\Schema(type="array", @OA\Items(ref=@Model(type=Checkpoint::class, groups={"checkpoint:user_value"})))
     *     )
     * )
     */
    public function getCheckpoints(CourseStream $stream, User $user): array
    {
        // @todo: проверка прав
        return $this->em->getRepository(Checkpoint::class)->getCheckPointByUserAndStream($stream, $user);
    }

    /**
     * Добавить значение для чекбокса
     *
     * @Rest\Post("/api/v2/traction/checkpoints/{checkpoint<\d+>}/value")
     * @Rest\View(serializerGroups={"checkpoint:user_value"}, statusCode=Response::HTTP_CREATED)
     *
     * @OA\Parameter(name="checkpoint", in="path", @OA\Schema(type="integer"), description="id чекбокса")
     * @OA\RequestBody(@Model(type=CheckpointValueType::class))
     * @OA\Response(response=Response::HTTP_CREATED, description="",
     *     @OA\MediaType(mediaType="application/json",
     *          @OA\Schema(ref=@Model(type=CheckpointValue::class, groups={"checkpoint:user_value"}))
     *     )
     * )
     */
    public function addCheckpointValue(
        Checkpoint $checkpoint,
        Request $request,
        CheckpointValueRepository $repository
    ): CheckpointValue {
        $value = $repository->findOneBy(['user' => $this->getUser(), 'checkpoint' => $checkpoint]);
        if ($value === null) {
            $value = new CheckpointValue($checkpoint, $this->getUser());
        }
        $this->fillByForm(CheckpointValueType::class, $request, $value);
        $this->em->persist($value);
        $this->em->flush();

        return $value;
    }

    /**
     * @deprecated use getCheckpoints instead
     * @Rest\Get("/api/traction/checkpoints/{stream<\d+>}/user/{user<\d+>}", name="api_traction_checkpoint_list")
     */
    public function listAction(CourseStream $stream, User $user): JsonResponse
    {
        // @todo: проверка прав
        $checkpoints = $this->em->getRepository(Checkpoint::class)
            ->getCheckPointByUserAndStream($stream, $user);

        return $this->getResponseNew($checkpoints, ['checkpoint:user_value']);
    }

    /**
     * @deprecated use addCheckpointValue instead
     * @Rest\Post("/api/traction/checkpoint/set/{checkpoint<\d+>}", name="api_traction_checkpoint_set")
     */
    public function saveCheckpointValue(Request $request, Checkpoint $checkpoint): JsonResponse
    {
        $user = $this->getUser();
        $value = $this->em->getRepository(CheckpointValue::class)
            ->findOneBy(['user' => $user, 'checkpoint' => $checkpoint]);
        if (!$value) {
            $value = new CheckpointValue($checkpoint, $user);
        }

        $form = $this->createForm(CheckpointValueType::class, $value);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($value);
            $this->em->flush();

            return $this->getResponseNew($value, ['checkpoint:user_value']);
        }

        return $this->addFormErrors($form)->getResponseNew();
    }

}

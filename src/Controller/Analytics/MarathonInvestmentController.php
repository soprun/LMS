<?php

namespace App\Controller\Analytics;

use App\Controller\BaseController;
use App\Entity\AbstractCourse;
use App\Entity\CourseStream;
use App\Entity\MarathonInvestment\MarathonInvestment;
use App\Entity\MarathonInvestment\MarathonInvestmentConfiguration;
use App\Entity\MarathonInvestment\Target;
use App\Entity\MarathonInvestment\UserNiche;
use App\Entity\MarathonInvestment\UserNicheComment;
use App\Entity\User;
use App\Entity\UserGroupUser;
use App\Form\MarathonInvestment\CollectionNichesType;
use App\Form\MarathonInvestment\MarathonConfigurationType;
use App\Form\MarathonInvestment\MarathonInvestmentType;
use App\Form\MarathonInvestment\MembersRequestFormType;
use App\Form\MarathonInvestment\NicheCommentType;
use App\Form\MarathonInvestment\UserNicheType;
use App\Repository\MarathonInvestment\UserNicheRepository;
use App\Repository\UserRepository;
use App\Service\AuthServerHelper;
use App\Service\AuthServerService;
use App\Service\LikeItHelper;
use App\Service\MarathonInvestment\MarathonInvestmentService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use Exception;
use FOS\RestBundle\Controller\Annotations as Rest;
use GuzzleHttp\Exception\GuzzleException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\Model;

class MarathonInvestmentController extends BaseController
{
    /**
     * Получение всех настроек для фильтров
     * @Rest\Get("/api/marathon-investment/settings")
     * @IsGranted("ROLE_USER")
     */
    public function settings(LikeItHelper $likeItHelper, EntityManagerInterface $em): JsonResponse
    {
        $cities = $likeItHelper->getCities();

        /** @var QueryBuilder $qb */
        $streams = $em->getRepository(CourseStream::class)->createQueryBuilder('cs')
            ->select('cs.id, cs.name')
            ->innerJoin('cs.abstractCourse', 'ac')
            ->where('ac.slug = :slug')
            ->setParameter('slug', AbstractCourse::SLUG_MARATHON_INVESTMENT)
            ->orderBy('cs.name')
            ->getQuery()
            ->getResult();

        return $this->getResponse('settings', [
            'cities' => $cities,
            'streams' => $streams,
        ]);
    }

    /**
     * Получить актуальный поток
     * @Rest\Get("/api/marathon-investment/current-stream")
     * @IsGranted("ROLE_USER")
     */
    public function currentStream(EntityManagerInterface $em): JsonResponse
    {
        $user = $this->getUser();

        $actualStream = $em->getRepository(UserGroupUser::class)->createQueryBuilder('ugu')
            ->select('cs.id, cs.name')
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ug.courseStream', 'cs')
            ->innerJoin('cs.abstractCourse', 'ac')
            ->andWhere('ugu.user = :user')
            ->setParameter('user', $user)
            ->andWhere('ac.slug = :slug')
            ->setParameter('slug', AbstractCourse::SLUG_MARATHON_INVESTMENT)
            ->orderBy('cs.stream', 'DESC')
            ->getQuery()
            ->getResult();

        return $this->getResponse('currentStream', $actualStream);
    }

    /**
     * Добавление конфигурации(точка А, чист.прибыль)
     * @Rest\Post("/api/marathon-investment/configuration")
     * @IsGranted("ROLE_USER")
     *
     * @throws ExceptionInterface
     */
    public function addConfiguration(Request $request, EntityManagerInterface $em): JsonResponse
    {
        $user = $this->getUser();

        $form = $this->createForm(MarathonConfigurationType::class);
        $form->submit($request->request->all(), false);

        if ($form->isSubmitted() && $form->isValid()) {
            $configuration = $form->getData();
            $existedConfiguration = $em->getRepository(MarathonInvestmentConfiguration::class)->findOneBy([
                'user' => $user,
                'stream' => $configuration->getStream(),
            ]);

            if (!$existedConfiguration) {
                $configuration->setUser($user);
                $em->persist($configuration);
                $em->flush();

                $records = $em->getRepository(MarathonInvestment::class)->createQueryBuilder('mi')
                    ->innerJoin('mi.user', 'u')
                    ->where('mi.user = :user')
                    ->setParameter('user', $user)
                    ->andWhere('mi.stream = :stream')
                    ->setParameter('stream', $configuration->getStream())
                    ->getQuery()
                    ->getResult();

                if (!$records) {
                    $this->generateTractionRecords($user, $configuration->getStream(), $em);
                }

                return $this->getResponse(
                    'addConfiguration',
                    $this->normalize($configuration, ['marathon-investment:niches:create'])
                );
            }

            return $this->getResponse(
                'addConfiguration',
                $this->normalize($existedConfiguration, ['marathon-investment:niches:create'])
            );
        }

        return $this->addFormErrors($form)->getResponse('addConfiguration');
    }

    /**
     * Установить ниши для участника
     * @Rest\Post("/api/marathon-investment/niches")
     * @IsGranted("ROLE_USER")
     *
     * @OA\Tag(name="Ниши пользователя")
     *
     * @throws ExceptionInterface
     */
    public function setNiches(Request $request, EntityManagerInterface $em): JsonResponse
    {
        $user = $this->getUser();

        $form = $this->createForm(CollectionNichesType::class);
        $form->submit($request->request->all(), false);

        if ($form->isSubmitted() && $form->isValid()) {
            $niches = $form->getData();

            foreach ($niches['niches'] as $niche) {
                $user->addUserNiche($niche);
                $em->persist($user);
            }
            $em->flush();

            return $this->getResponse(
                'setNiches',
                $this->normalize($user, ['marathon-investment:niches:create'])
            );
        }

        return $this->addFormErrors($form)->getResponse('setNiches');
    }

    /**
     * Установить ниши для участника
     * @Rest\Post("/api/marathon-investment/niches/add")
     * @IsGranted("ROLE_USER")
     * @OA\Tag(name="Ниши пользователя")
     *
     * @throws Exception
     */
    public function addNiche(Request $request, EntityManagerInterface $em): JsonResponse
    {
        $data = $request->request->all();
        $user = $this->getUser();

        $form = $this->createForm(UserNicheType::class);
        $form->submit($data, false);

        if ($form->isSubmitted() && $form->isValid()) {
            $niche = $form->getData();

            $userNiches = $em->getRepository(UserNiche::class)->createQueryBuilder('un')
                ->select('count(un.id)')
                ->where('un.user = :user')
                ->setParameter('user', $user)
                ->andWhere('un.stream = :stream')
                ->setParameter('stream', $niche->getStream())
                ->getQuery()
                ->getSingleScalarResult();

            if ((int)$userNiches < 4) {
                $user->addUserNiche($niche);

                $em->persist($user);
                $em->flush();
            }

            return $this->getResponse('addNiche', ['id' => $niche->getId()]);
        }

        return $this->addFormErrors($form)->getResponse('setNiches');
    }

    /**
     * Удаление ниши участника
     * @Rest\Delete("/api/marathon-investment/niches/{id}")
     *
     * @param UserNiche $niche
     * @param EntityManagerInterface $em
     *
     * @OA\Tag(name="Ниши пользователя")
     * @return JsonResponse
     * @IsGranted("ROLE_USER")
     * @Security("user === niche.getUser()")
     */
    public function removeNiche(UserNiche $niche, EntityManagerInterface $em): JsonResponse
    {
        $user = $this->getUser();
        $stream = $niche->getStream();
        $id = $niche->getId();

        $em->remove($niche);
        $em->flush();

        $niches = $em->getRepository(UserNiche::class)->findBy(['stream' => $stream, 'user' => $user]);

        if ($niches) {
            $userNiche = $niches[0];
            $userNiche->setMain(true);

            $em->persist($userNiche);
            $em->flush();
        }

        return $this->getResponse('removeNiche', ['id' => $id]);
    }

    /**
     * Отметить нишу как основную
     * @Rest\Post("/api/marathon-investment/users/{id}/niches/{niche_id}")
     *
     * @param User $targetUser
     * @param UserNiche $niche
     * @param EntityManagerInterface $em
     *
     * @return JsonResponse
     * @OA\Tag(name="Ниши пользователя")
     * @ParamConverter("niche", options={"mapping": {"niche_id": "id"}})
     * @Security("user === targetUser && user === niche.getUser()")
     */
    public function setNicheAsMain(User $targetUser, UserNiche $niche, EntityManagerInterface $em): JsonResponse
    {
        if ($niche->isMain()) {
            return $this->getResponse('setNicheAsMain', ['niche' => $niche->getId()]);
        }

        $niche->setMain(true);
        $em->persist($niche);

        $niches = $em->getRepository(UserNiche::class)->findBy([
            'stream' => $niche->getStream(),
            'user' => $targetUser,
        ]);

        foreach ($niches as $userNiche) {
            if ($userNiche->isMain() && ($userNiche !== $niche)) {
                $userNiche->setMain(false);
                $em->persist($userNiche);
            }
        }
        $em->flush();

        return $this->getResponse('setNicheAsMain', ['niche' => $niche->getId()]);
    }

    /**
     * Отметить цель
     * @Rest\Post("/api/marathon-investment/users/{id}/targets/{target_id}")
     *
     * @param User $user
     * @param Target $target
     * @param EntityManagerInterface $em
     *
     * @return JsonResponse
     * @IsGranted("ROLE_USER")
     * @ParamConverter("target", options={"mapping": {"target_id": "id"}})
     */
    public function markTarget(User $user, Target $target, EntityManagerInterface $em): JsonResponse
    {
        if ($user->getTractionTargets()->contains($target)) {
            $user->removeTractionTarget($target);
        } else {
            $user->addTractionTarget($target);
        }

        $em->persist($user);
        $em->flush();

        return $this->getResponse('markTarget', ['id' => $target->getId()]);
    }

    /**
     * Редактировать нишу
     * @Rest\Post("/api/marathon-investment/niches/{id}/edit")
     *
     * @param Request $request
     * @param UserNiche $niche
     * @param EntityManagerInterface $em
     *
     * @return JsonResponse
     * @OA\Tag(name="Ниши пользователя")
     * @Security("user === niche.getUser()")
     */
    public function editNiche(Request $request, UserNiche $niche, EntityManagerInterface $em): JsonResponse
    {
        $form = $this->createForm(UserNicheType::class, $niche);
        $form->submit($request->request->all(), false);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($niche);
            $em->flush();

            return $this->getResponse('editNiche', ['id' => $niche->getId()]);
        }

        return $this->addFormErrors($form)->getResponse('editNiche');
    }

    /**
     * Редактировать конфигурацию (точка А, чист.прибыль)
     * @Rest\Post("/api/marathon-investment/configurations/{id}/edit")
     *
     * @param Request $request
     * @param MarathonInvestmentConfiguration $configuration
     * @param EntityManagerInterface $em
     *
     * @return JsonResponse
     * @IsGranted("USER_ANALYTICS_EDIT", subject="configuration")
     */
    public function editConfiguration(
        Request $request,
        MarathonInvestmentConfiguration $configuration,
        EntityManagerInterface $em
    ): JsonResponse {
        $form = $this->createForm(MarathonConfigurationType::class, $configuration);
        $form->submit($request->request->all(), false);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($configuration);
            $em->flush();

            return $this->getResponse('editConfiguration', ['id' => $configuration->getId()]);
        }

        return $this->addFormErrors($form)->getResponse('editNiche');
    }

    /**
     * Редактировать запись по трекшену
     * @Rest\Post("/api/marathon-investment/records/{id}")
     *
     * @param Request $request
     * @param MarathonInvestment $record
     * @param EntityManagerInterface $em
     *
     * @return JsonResponse
     * @Security("user === record.getUser()")
     */
    public function editTractionRecord(
        Request $request,
        MarathonInvestment $record,
        EntityManagerInterface $em
    ): JsonResponse {
        $form = $this->createForm(MarathonInvestmentType::class, $record);
        $form->submit($request->request->all(), false);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($record);
            $em->flush();

            return $this->getResponse('editTractionRecord', ['id' => $record->getId()]);
        }

        return $this->addFormErrors($form)->getResponse('editTractionRecord');
    }

    /**
     * Получить аналитику по трекшену
     * @Rest\Get("/api/marathon-investment/users/{id}/records")
     * @IsGranted("USER_ANALYTICS_VIEW", subject="user")
     *
     * @throws NonUniqueResultException
     */
    public function getTractionRecords(Request $request, User $user, EntityManagerInterface $em): JsonResponse
    {
        $streamId = $request->get('stream', null);

        if ($streamId) {
            $qb = $em->getRepository(MarathonInvestment::class)->createQueryBuilder('mi')
                ->select('mi.id, mi.week, mi.planned, mi.passed, mi.amount')
                ->innerJoin('mi.user', 'u')
                ->where('mi.user = :user')
                ->setParameter('user', $user)
                ->andWhere('mi.stream = :stream')
                ->setParameter('stream', (int)$streamId);

            $records = $qb->getQuery()->getResult();

            if (!$records) {
                $stream = $em->getRepository(CourseStream::class)->find($streamId);
                $this->generateTractionRecords($user, $stream, $em);
                $records = $qb->getQuery()->getResult();
            }

            $stats = $this->calculateStatsByRecords($records);
            $stream = $em->getRepository(CourseStream::class)->find($streamId);
            $activeWeek = $stream->getActivePeriod(AbstractCourse::SLUG_MARATHON_INVESTMENT);

            if ($stats['totalAmount'] >= 1000000) {
                $this->checkAmount($em, $user, $streamId, $stats['totalAmount']);
            }

            return $this->getResponse('getTractionRecords', [
                'records' => $records,
                'stats' => $stats,
                'activeWeek' => $activeWeek,
            ]);
        }

        return $this->getResponse('getTractionRecords', []);
    }

    /**
     * Получить данные участника
     * @Rest\Get("/api/marathon-investment/users/{id}/profile")
     *
     * @param Request $request
     * @param User $user
     * @param EntityManagerInterface $em
     * @param AuthServerHelper $authHelper
     *
     * @return JsonResponse
     * @throws NonUniqueResultException
     * @IsGranted("USER_ANALYTICS_VIEW", subject="user")
     */
    public function getProfile(
        Request $request,
        User $user,
        EntityManagerInterface $em,
        AuthServerHelper $authHelper
    ): JsonResponse {
        $streamId = $request->get('stream');
        $data = [];

        $query = $em->getRepository(UserGroupUser::class)->createQueryBuilder('ugu')
            ->select('ugu.budget, cs.id')
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ug.courseStream', 'cs')
            ->andWhere('ugu.deleted = 0')
            ->andWhere('ugu.user = :user');

        if ($streamId) {
            $query->setParameter('user', $user)
                ->andWhere('cs.id = :streamId')
                ->setParameter('streamId', $streamId);
        } else {
            $query->innerJoin('cs.abstractCourse', 'ac')
                ->andWhere('ac.slug = :slug')
                ->setParameters(
                    [
                        'slug' => AbstractCourse::SLUG_MARATHON_INVESTMENT,
                        'user' => $user,
                    ]
                )
                ->orderBy('cs.stream', 'DESC');
        }
        $userGroupUser = $query->setMaxResults(1)->getQuery()->getOneOrNullResult();

        if ($userGroupUser) {
            $stream = $userGroupUser['id'];
            $targets = $em->getRepository(Target::class)->findBy(['stream' => $stream]);

            $userTargets = $user->getTractionTargets();

            foreach ($targets as $target) {
                $done = false;

                if ($userTargets->contains($target)) {
                    $done = true;
                }

                $name = $target->getName();

                if ($name === 'Привлек Х р. инвестиций') {
                    $total = 1500000 - $userGroupUser['budget'] ?? 0;
                    $name = str_replace('Х', number_format($total), $name);
                }

                $data['objectives'][] = [
                    'id' => $target->getId(),
                    'name' => $name,
                    'url' => $target->getUrl(),
                    'done' => $done,
                ];
            }

            $niches = $em->getRepository(UserNiche::class)->createQueryBuilder('un')
                ->andWhere('un.user = :user')
                ->andWhere('un.stream = :stream')
                ->setParameters([
                    'stream' => $stream,
                    'user' => $user,
                ])
                ->getQuery()
                ->getResult();
            $data['niches'] = [];

            foreach ($niches as $niche) {
                $data['niches'][] = [
                    'id' => $niche->getId(),
                    'title' => $niche->getTitle(),
                    'description' => $niche->getDescription(),
                    'rival' => $niche->isRival(),
                    'marked' => $niche->isMarked(),
                    'main' => $niche->isMain(),
                    'message' => $niche->getCount(),
                ];
            }

            $configuration = $em->getRepository(MarathonInvestmentConfiguration::class)->createQueryBuilder('mic')
                ->select('mic.id, mic.pointA, mic.profit')
                ->innerJoin('mic.user', 'u')
                ->where('mic.user = :user')
                ->setParameter('user', $user)
                ->andWhere('mic.stream = :stream')
                ->setParameter('stream', $stream)
                ->getQuery()
                ->getResult();

            $data['configuration'] = $configuration;

            $authUser = $authHelper->getUsers(
                [$user->getAuthUserId()],
                ['name', 'lastname', 'avatar', 'email', 'city', 'phone']
            );

            $data['user'] = $authUser;
            $data['stream'] = $stream;

            return $this->getResponse('getProfile', $data);
        }

        return $this->getResponse('getProfile');
    }

    /**
     * Возвращает список ниш по пользователю и потоку
     *
     * @Rest\Get("/api/v2/niches/user/{user<\d+>}/stream/{stream<\d+>}")
     * @Rest\View(serializerGroups={"marathon-investment:list"})
     *
     * @OA\Tag(name="Ниши пользователя")
     * @OA\Parameter(name="stream", in="path", @OA\Schema(type="integer"), description="id потока")
     * @OA\Parameter(name="user", in="path", @OA\Schema(type="integer"), description="id пользователя")
     * @OA\Response(response=Response::HTTP_OK, description="",
     *     @OA\MediaType(mediaType="application/json", @OA\Schema(type="array",
     *         @OA\Items(ref=@Model(type=UserNiche::class, groups={"marathon-investment:list"}))
     *     ))
     * )
     */
    public function getNiches(CourseStream $stream, User $user, UserNicheRepository $nicheRepository): array
    {
        return $nicheRepository->findBy([
            'stream' => $stream,
            'user' => $user,
        ]);
    }

    /**
     * Подтвердить нишу
     * @Rest\Post("/api/marathon-investment/niches/{id}/confirm")
     *
     * @param UserNiche $niche
     * @param EntityManagerInterface $em
     *
     * @return JsonResponse
     * @OA\Tag(name="Ниши пользователя")
     * @IsGranted("USER_ANALYTICS_EDIT")
     */
    public function confirmNiche(UserNiche $niche, EntityManagerInterface $em): JsonResponse
    {
        if ($niche->isMarked()) {
            $niche->setMarked(false);
        } else {
            $niche->setMarked(true);
        }

        $em->persist($niche);
        $em->flush();

        return $this->getResponse('confirmNiche', [
            'niche' => [
                'id' => $niche->getId(),
                'marked' => $niche->isMarked(),
            ],
        ]);
    }

    /**
     * Отправить комментарий
     * @Rest\Post("/api/marathon-investment/niches/{id}/comments/send")
     *
     * @param Request $request
     * @param UserNiche $niche
     * @param EntityManagerInterface $em
     * @OA\Tag(name="Ниши пользователя")
     *
     * @return JsonResponse
     * @IsGranted("USER_ANALYTICS_CREATE", subject="niche")
     */
    public function sendComment(Request $request, UserNiche $niche, EntityManagerInterface $em): JsonResponse
    {
        $user = $this->getUser();

        $data = $request->request->all();
        $data['user'] = $user->getId();

        $form = $this->createForm(NicheCommentType::class);
        $form->submit($data, false);

        if ($form->isSubmitted() && $form->isValid()) {
            $comment = $form->getData();
            $niche->addComment($comment);

            $em->persist($niche);
            $em->flush();

            return $this->getResponse('confirmNiche', [
                'comment' => [
                    'id' => $comment->getId(),
                    'text' => $comment->getText(),
                ],
            ]);
        }

        return $this->addFormErrors($form)->getResponse('sendComment');
    }

    /**
     * Получить все комментарии по нише
     * @Rest\Get("/api/marathon-investment/niches/{id}/comments")
     *
     * @param UserNiche $niche
     * @param AuthServerHelper $authHelper
     * @param EntityManagerInterface $em
     *
     * @return JsonResponse
     * @throws GuzzleException
     * @OA\Tag(name="Ниши пользователя")
     * @IsGranted("USER_ANALYTICS_VIEW", subject="niche")
     */
    public function getCommentsByNiche(
        UserNiche $niche,
        AuthServerHelper $authHelper,
        EntityManagerInterface $em
    ): JsonResponse {
        $data = [];

        /** @var QueryBuilder $qb */
        $comments = $em->getRepository(UserNicheComment::class)->createQueryBuilder('comments')
            ->select('u.authUserId, comments.text, comments.createdAt')
            ->innerJoin('comments.niche', 'cn')
            ->leftJoin('comments.user', 'u')
            ->andWhere('comments.niche = :niche')
            ->setParameter('niche', $niche)
            ->orderBy('comments.createdAt', 'DESC')
            ->getQuery()
            ->getResult();

        $authIds = [];

        foreach ($comments as $comment) {
            if (!in_array($comment['authUserId'], $authIds, true)) {
                $authIds[] = $comment['authUserId'];
            }
        }

        if ($authIds) {
            sort($authIds);
            $authUsers = $authHelper->getUsersInfo($authIds);

            foreach ($comments as $comment) {
                $key = array_search($comment['authUserId'], $authIds, true);

                $data[] = [
                    'name' => $authUsers[$key]->name,
                    'lastname' => $authUsers[$key]->lastname,
                    'avatar' => $authUsers[$key]->avatar,
                    'text' => $comment['text'],
                    'createdAt' => $comment['createdAt'],
                ];
            }
        }

        return $this->getResponse('getCommentsByNiche', ['comments' => $data]);
    }

    /**
     * @deprecated use getMembers instead
     *
     * Таблица участников + фильтры
     * @Rest\Get("/api/marathon-investment/users")
     * @IsGranted("USER_ANALYTICS_VIEW")
     */
    public function getUsers(
        Request $request,
        UserRepository $userRepository,
        AuthServerService $authServer
    ): JsonResponse {
        $sorted = $request->get('sort', []);
        $page = (int)$request->get('page', 1);
        $pageSize = (int)$request->get('pageSize', 10);

        $users = $userRepository->getUsersInMarafonInvesment(
            (int)$request->get('stream') ?: null,
            $request->get('isMarked', true) === '0',
            $sorted,
            $page,
            $pageSize,
        );

        $filledUsers = new ArrayCollection(
            $authServer->refillUsers(
                $users,
                $this->prepareSearch($request->get('city'), $request->get('name')) ?: null,
                true
            )
        );

        $orderBy = [];
        foreach ($sorted as $field => $sort) {
            if (in_array($field, ['amount', 'passed']) && in_array($sort, ['DESC', 'ASC'])) {
                $orderBy['total' . ucfirst(key($sorted))] = $sort;
            }
        }

        return $this->getResponseNew(
            array_values($filledUsers->matching((Criteria::create())->orderBy($orderBy))->toArray()),
            ['marathon-investment:list']
        );
    }

    /**
     * Список участников марафона инвестиций
     *
     * @Rest\Get("/api/v2/marathon-investment/user")
     * @Rest\View(serializerGroups={"marathon-investment:member"})
     *
     * @OA\Tag(name="Марафон инвестиций")
     * @OA\Parameter(name="filters", in="query", @Model(type=MembersRequestFormType::class))
     * @OA\Response(response=Response::HTTP_OK, description="", @OA\JsonContent(type="array",
     *     @OA\Items(ref=@Model(type=User::class, groups={"marathon-investment:member"}))
     * ))
     *
     * @IsGranted("USER_ANALYTICS_VIEW")
     */
    public function getMembers(Request $request, MarathonInvestmentService $service): ArrayCollection
    {
        $params = $this->fillByForm(MembersRequestFormType::class, $request);

        return $service->getMembers($params);
    }

    /**
     * Количество участников марафона инвестиций
     *
     * @Rest\Get("/api/v2/marathon-investment/user/count")
     * @Rest\View()
     *
     * @OA\Tag(name="Марафон инвестиций")
     * @OA\Parameter(name="filters", in="query", @Model(type=MembersRequestFormType::class))
     * @OA\Response(response=Response::HTTP_OK, description="", @OA\Schema(type="number"))
     *
     * @IsGranted("USER_ANALYTICS_VIEW")
     */
    public function getMembersCount(Request $request, MarathonInvestmentService $service): int
    {
        $params = $this->fillByForm(MembersRequestFormType::class, $request);

        return $service->getMembersCount($params);
    }

    /**
     * @deprecated use getMembersCount instead
     *
     * Количество участников с учетом фильтров
     * @Rest\Get("/api/marathon-investment/users/count")
     * @IsGranted("USER_ANALYTICS_VIEW")
     */
    public function getUsersCount(
        Request $request,
        UserRepository $userRepository,
        AuthServerService $authServer
    ): JsonResponse {
        $ids = $userRepository->getUsersInMarafonInvesmentAuthIds(
            (int)$request->get('stream') ?: null,
            $request->get('isMarked', true) === '0'
        );
        $authUsers = $authServer->getInternalUsers([
            'ids' => $ids,
            'pageSize' => 0,
            'search' => $this->prepareSearch($request->get('city'), $request->get('name')) ?: null,
        ]);

        return $this->getResponseNew($authUsers['count']);
    }

    private function generateTractionRecords(User $user, CourseStream $stream, EntityManagerInterface $em): void
    {
        for ($i = 1; $i <= 6; $i++) {
            $marathonInvestment = new MarathonInvestment();
            $marathonInvestment->setUser($user)
                ->setStream($stream)
                ->setWeek($i);

            $em->persist($marathonInvestment);
        }
        $em->flush();
    }

    private function calculateStatsByRecords(array $records): array
    {
        $totalPassed = $totalPlanned = $totalAmount = 0;

        foreach ($records as $record) {
            $totalPassed += $record['passed'];
            $totalPlanned += $record['planned'];
            $totalAmount += $record['amount'];
        }

        $data['totalPassed'] = [$totalPassed, 10];
        $data['totalPlanned'] = [$totalPlanned, 20];
        $data['totalAmount'] = $totalAmount;

        if ($totalPassed > 10) {
            $data['totalPassed'] = [$totalPassed, 30];
        }

        if ($totalPlanned > 20) {
            $data['totalPlanned'] = [$totalPlanned, 50];
        }

        return $data;
    }

    private function prepareSearch(?string $city = null, ?string $string = null): string
    {
        $search = '';

        if ($city) {
            $search = sprintf("ui.city = '%s'", $city);
        }

        if ($string) {
            if (!empty($search)) {
                $search .= " AND ";
            }

            $search .= sprintf(
                "(ui.name LIKE '%%%s%%' OR ui.lastname LIKE '%%%s%%' OR u.email LIKE '%%%s%%')",
                $string,
                $string,
                $string
            );
        }

        return $search;
    }

    /**
     * @throws NonUniqueResultException
     */
    private function checkAmount(EntityManagerInterface $em, User $user, int $streamId, int $amount): void
    {
        $userGroupUser = $em->getRepository(UserGroupUser::class)->createQueryBuilder('ugu')
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ug.courseStream', 'cs')
            ->andWhere('ugu.deleted = 0')
            ->andWhere('cs.id = :streamId')
            ->andWhere('ugu.user = :user')
            ->setParameter('user', $user)
            ->setParameter('streamId', $streamId)
            ->getQuery()
            ->getOneOrNullResult();

        $targets = $em->getRepository(Target::class)->findBy(['stream' => $streamId]);

        foreach ($targets as $target) {
            if ($target->getName() === 'Привлек Х р. инвестиций' && $userGroupUser) {
                $limit = 1500000 - $userGroupUser->getBudget() ?? 0;

                if ($amount >= $limit) {
                    $user->addTractionTarget($target);
                    $em->persist($user);
                    $em->flush();
                }
            }
        }
    }

}

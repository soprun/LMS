<?php

namespace App\Controller\Analytics;

use App\Controller\BaseController;
use App\Entity\Analytics\ReportMSG;
use App\Entity\User;
use App\Service\Analytics\AnalyticsMSGHelper;
use App\Service\UserHelper;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AnalyticsMSGController
 * @package App\Controller\Analytics
 */
class AnalyticsMSGController extends BaseController
{
    /**
     * @Route("/analytics/msg", name="analytics_report_msg")
     */
    public function index()
    {
        return $this->render('lms_vue.html.twig');
    }

    /**
     * @Route("/analytics/msg/get", name="analytics_report_msg_get"),  methods={"GET"})
     * @param  Request  $request
     * @param  AnalyticsMSGHelper  $analyticsMSGHelper
     * @return JsonResponse
     * @IsGranted("ROLE_USER")
     */
    public function getReportMsg(Request $request, AnalyticsMSGHelper $analyticsMSGHelper)
    {
        return $analyticsMSGHelper->getReportMSG($request);
    }

    /**
     * @Route("/rating/msa", name="analytics_rating_msa")
     * @param  UserHelper  $userHelper
     * @return RedirectResponse|Response
     */
    public function ratingMsa(UserHelper $userHelper)
    {
        return $this->render('lms_vue.html.twig');
    }

    /**
     * @Route("/analytics/msa/get", name="analytics_report_msa_get"),  methods={"GET"})
     * @param  Request  $request
     * @param  AnalyticsMSGHelper  $analyticsMSGHelper
     * @return JsonResponse
     * @IsGranted("ROLE_USER")
     */
    public function getReportMsa(Request $request, AnalyticsMSGHelper $analyticsMSGHelper)
    {
        return $analyticsMSGHelper->getReportMSA($request);
    }

    /**
     * @Route("/analytics/msa/streams/get", name="analytics_report_msa_streams_get"),  methods={"GET"})
     * @param  AnalyticsMSGHelper  $analyticsMSGHelper
     * @return JsonResponse
     * @IsGranted("ROLE_USER")
     */
    public function getReportMsaStreams(AnalyticsMSGHelper $analyticsMSGHelper)
    {
        return $analyticsMSGHelper->getStreamsMSA();
    }

    /**
     * @Route("/analytics/msa/csv/get", name="analytics_report_msa_csv_get", methods={"GET"})
     * @param  Request  $request
     * @param  AnalyticsMSGHelper  $analyticsMSGHelper
     * @return JsonResponse
     * @IsGranted("USER_ANALYTICS_EDIT")
     */
    public function getReportMsaCsv(Request $request, AnalyticsMSGHelper $analyticsMSGHelper)
    {
        return $analyticsMSGHelper->getReportMsaCsv($request);
    }

    /**
     * @Route("/rating/msa/user", name="analytics_rating_msa_my_user")
     */
    public function ratingMsaMyUser()
    {
        /** @var User $user */
        $user = $this->getUser();
        if ($user->getId() != null) {
            $url = $this->generateUrl('analytics_rating_msa_user', ['userId' => $user->getId()]);

            return new RedirectResponse($url);
        }

        return $this->render('lms_vue.html.twig');
    }

    /**
     * @Route("/rating/msa/user/{userId}", name="analytics_rating_msa_user")
     */
    public function ratingMsaUser()
    {
        return $this->render('lms_vue.html.twig');
    }

    /**
     * @Route("/analytics/msa/{userId}/get", name="analytics_report_msa_user_get"),  methods={"GET"})
     * @ParamConverter("user", options={"mapping": {"userId": "id"}})
     * @param  Request  $request
     * @param  AnalyticsMSGHelper  $analyticsMSGHelper
     * @param  User  $user
     * @return JsonResponse
     * @IsGranted("USER_ANALYTICS_VIEW", subject="user")
     */
    public function getReportMsaUser(Request $request, AnalyticsMSGHelper $analyticsMSGHelper, User $user)
    {
        $needAuth = true;

        if ($user === $this->getUser()) {
            $needAuth = false;
        }
        return $analyticsMSGHelper->getReportMSAUser($request, $user, $needAuth);
    }

    /**
     * @Route("/analytics/msa/user/cell/edit", name="analytics_report_msa_user_cell_edit", methods={"POST"})
     * @param  Request  $request
     * @param  AnalyticsMSGHelper  $analyticsMSGHelper
     * @return JsonResponse
     * @IsGranted("ROLE_USER")
     */
    public function editCellHundredUser(Request $request, AnalyticsMSGHelper $analyticsMSGHelper)
    {
        return $analyticsMSGHelper->editCellMSAUser($request);
    }

    /**
     * @Route("/analytics/msa/{userId}/years/get", name="analytics_report_msa_user_years_get"),  methods={"GET"})
     * @ParamConverter("user", options={"mapping": {"userId": "id"}})
     * @param  User  $user
     * @return JsonResponse
     * @IsGranted("ROLE_USER")
     */
    public function getReportMsaUserYears(User $user)
    {
        $data = [];
        $year = date('Y');
        /** @var QueryBuilder $qb */
        $qb = $this->getDoctrine()->getRepository(ReportMSG::class)->createQueryBuilder('c');

        $qb->select('min(c.year)')
            ->where('c.user = :user')
            ->setParameter('user', $user);

        try {
            $minYear = $qb->getQuery()->getSingleScalarResult();
        } catch (NoResultException $e) {
            $minYear = null;
        } catch (NonUniqueResultException $e) {
            $minYear = null;
        }

        if (empty($minYear)) {
            $minYear = $year;
        }

        $data = [
            'userId' => $user->getId(),
            'userAuthId' => $user->getAuthUserId(),
            'years' => range($minYear, $year),
        ];

        return $this->getResponse('getReportMsaUserYears', $data);
    }

    /**
     * @Route("/analytics/msg/groups/get", name="analytics_report_msg_groups_get"),  methods={"GET"})
     * @param  Request  $request
     * @param  AnalyticsMSGHelper  $analyticsMSGHelper
     * @return JsonResponse
     * @IsGranted("ROLE_USER")
     */
    public function getReportMsgGroups(Request $request, AnalyticsMSGHelper $analyticsMSGHelper)
    {
        return $analyticsMSGHelper->getReportMsgGroups($request);
    }

    /**
     * @Route("/analytics/msa/user/edit", name="analytics_report_msa_user_edit", methods={"POST"})
     * @param  Request  $request
     * @param  AnalyticsMSGHelper  $analyticsMSGHelper
     * @return JsonResponse
     * @IsGranted("ROLE_USER")
     */
    public function editReportMSAUser(Request $request, AnalyticsMSGHelper $analyticsMSGHelper)
    {
        return $analyticsMSGHelper->editReportMSAUser($request);
    }

    /**
     * @Route("/analytics/msg/user/edit", name="analytics_report_msg_user_edit", methods={"POST"})
     * @param  Request  $request
     * @param  AnalyticsMSGHelper  $analyticsMSGHelper
     * @return JsonResponse
     * @IsGranted("USER_ANALYTICS_EDIT")
     */
    public function editReportUser(Request $request, AnalyticsMSGHelper $analyticsMSGHelper)
    {
        return $analyticsMSGHelper->editReportUser($request);
    }

    /**
     * @Route("/analytics/msg/csv/get", name="analytics_report_msg_csv_get", methods={"GET"})
     * @param  Request  $request
     * @param  AnalyticsMSGHelper  $analyticsMSGHelper
     * @return JsonResponse
     * @IsGranted("USER_ANALYTICS_EDIT")
     */
    public function getReportMsgCsv(Request $request, AnalyticsMSGHelper $analyticsMSGHelper)
    {
        return $analyticsMSGHelper->getReportMsgCsv($request);
    }
}

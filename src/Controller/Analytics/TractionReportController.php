<?php

namespace App\Controller\Analytics;

use App\Controller\BaseController;
use App\Entity\CourseStream;
use App\Service\Analytics\TractionReportService;
use App\Service\LikecentreDatabaseHelper;
use Carbon\Carbon;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use OpenApi\Annotations as OA;

/**
 * @Rest\Route("/api/traction-report", name="traction_report_")
 *
 * @OA\Tag(name="Общая аналитика - отчеты")
 *
 * @IsGranted("ROLE_USER")
 */
class TractionReportController extends BaseController
{
    /**
     * Получение потоков и амо-продуктов для фронта
     *
     * @Rest\Get("/dictionary", name="get_report_dictionary")
     *
     * @OA\Response(
     *     response=200,
     *     description="Список курсов и продуктов",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *     )
     * )
     *
     * @OA\Response(
     *      response=400,
     *      description="Ошибка авторизации",
     * )
     *
     * @IsGranted("USER_ANALYTICS_VIEW")
     *
     * @param LikecentreDatabaseHelper $likecentreDatabaseHelper
     *
     * @return Response
     * @throws ExceptionInterface
     */
    public function dictionary(LikecentreDatabaseHelper $likecentreDatabaseHelper): Response
    {
        $sql = <<<SQL
            SELECT amo_product_name AS title, amo_product_id as value
            FROM amo_products
            WHERE (amo_product_name LIKE 'скорость клуб%' OR amo_product_name LIKE 'Сотка%' OR
                   amo_product_name LIKE 'Скорость. Клуб%')
              AND amo_product_name NOT LIKE '%автосделки%'
              AND amo_product_name NOT LIKE '%экватор%'
            ORDER BY amo_product_name;
        SQL;
        $amoProducts = $likecentreDatabaseHelper->execute($sql);

        $courseStreams = $this->em->getRepository(CourseStream::class)->createQueryBuilder('cs')
            ->select('cs.id as value', 'cs.name as title')
            ->innerJoin('cs.abstractCourse', 'ac')
            ->where("ac.slug = 'speed' AND cs.stream > 18")
            ->orWhere("ac.slug = 'speed_club'")
            ->orWhere("ac.slug = 'hundred' AND cs.stream > 3")
            ->getQuery()
            ->getResult();

        return $this->getResponseNew([
            'amoProducts' => $amoProducts,
            'courseStreams' => $this->normalize($courseStreams, ['traction']),
        ]);
    }

    /**
     * Выгрузка csv участников
     *
     * @Rest\Get("/speed-and-hundred/report", name="get_report")
     *
     * @Rest\QueryParam(name="productId", requirements="\d+", allowBlank=false, description="Id продукта")
     * @Rest\QueryParam(name="courseId", requirements="\d+", allowBlank=false, description="Id курса")
     *
     * @OA\Parameter(
     *     name="productId",
     *     in="query",
     *     required="true",
     *     description="Id продукта",
     *     @OA\Schema(type="integer")
     * )
     * @OA\Parameter(
     *     name="courseId",
     *     in="query",
     *     required="true",
     *     description="Id курса",
     *     @OA\Schema(type="integer")
     * )
     *
     * @OA\Response(
     *     response=200,
     *     description="CSV участников",
     *     @OA\MediaType(
     *         mediaType="text/csv",
     *     )
     * )
     *
     * @OA\Response(
     *      response=400,
     *      description="Ошибка авторизации",
     * )
     *
     * @IsGranted("USER_ANALYTICS_VIEW")
     *
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function report(
        Request $request,
        ParamFetcher $paramFetcher,
        TractionReportService $service
    ): Response {
        if (($productId = $paramFetcher->get('productId')) && ($courseId = $paramFetcher->get('courseId'))) {
            $lcUsers = $service->getLCUsers((int) $productId);
            $users = $service->getUsersAndCount($lcUsers, $courseId, $request)['users'];

            $csv = $this->getCsvHeader();

            foreach ($users as $user) {
                $string = '';
                foreach ($user as $value) {
                    $string .= $value . ';';
                }

                $csv .= "\n" . mb_substr($string, 0, -1);
            }

            $filename = Carbon::now()->format('d-m-Y_h_i_s') . '-report.csv';
            return new Response($csv, Response::HTTP_OK, ['Content-Disposition' => "attachment;filename={$filename}"]);
        }

        return $this->getResponseNew();
    }

    /**
     * Количество пользователей по категориям
     *
     * @Rest\Get("/speed-and-hundred/user-count", name="get_report_user_count")
     *
     * @Rest\QueryParam(name="productId", requirements="\d+", allowBlank=false, description="Id продукта")
     * @Rest\QueryParam(name="courseId", requirements="\d+", allowBlank=false, description="Id курса")
     *
     * @OA\Parameter(
     *     name="productId",
     *     in="query",
     *     required="true",
     *     description="Id продукта",
     *     @OA\Schema(type="integer")
     * )
     * @OA\Parameter(
     *     name="courseId",
     *     in="query",
     *     required="true",
     *     description="Id курса",
     *     @OA\Schema(type="integer")
     * )
     *
     * @OA\Response(
     *     response=200,
     *     description="Количество пользователей по категориям",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *     )
     * )
     *
     * @OA\Response(
     *      response=400,
     *      description="Ошибка авторизации",
     * )
     *
     * @IsGranted("USER_ANALYTICS_VIEW")
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \App\Exceptions\AuthServerServiceException
     */
    public function getUserCount(
        Request $request,
        ParamFetcher $paramFetcher,
        TractionReportService $service
    ): Response {
        if (($productId = $paramFetcher->get('productId')) && ($courseId = $paramFetcher->get('courseId'))) {
            $lcUsers = $service->getLCUsers((int) $productId);
            $count = $service->getUsersAndCount($lcUsers, $courseId, $request)['count'];

            return $this->getResponseNew($count);
        }

        return $this->getResponseNew();
    }

    private function getCsvHeader(): string
    {
        $csv = '';
        foreach (TractionReportService::CSV_HEADERS as $header) {
            $csv .= $header . ';';
        }

        return mb_substr($csv, 0, -1);
    }
}

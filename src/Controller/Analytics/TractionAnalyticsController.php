<?php

namespace App\Controller\Analytics;

use App\Controller\BaseController;
use App\DTO\Document\TractionAnalytics\TractionAnalyticsUserDocument;
use App\Entity\CourseStream;
use App\Entity\Faculty;
use App\Entity\Questionnaire\Question;
use App\Entity\Questionnaire\UserAnswer;
use App\Entity\Team;
use App\Entity\Traction\Value;
use App\Entity\Traction\ValueType;
use App\Entity\User;
use App\Entity\UserGroup;
use App\Entity\UserGroupUser;
use App\Service\AuthServerHelper;
use App\Service\AuthServerService;
use App\Service\LikecentreDatabaseHelper;
use App\Service\Traction\AnalyticsService;
use App\Service\Traction\TractionService;
use App\Service\UserHelper;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use OpenApi\Annotations as OA;

/**
 * @Route("/api/traction-analytics", name="traction_analytics_")
 */
class TractionAnalyticsController extends BaseController
{

    /** @var AuthServerHelper */
    private $authServerHelper;
    /** @var AuthServerService */
    private $authServerService;
    /** @var LikecentreDatabaseHelper */
    private $likecentreDatabaseHelper;
    /** @var TractionService */
    private $tractionService;
    /** @var UserHelper */
    private $userHelper;

    private const MAJOR_CAPTAINS_SPEED_GROUP = 414;
    private const MAJOR_CAPTAINS_HUNDRED_GROUP = 736;
    private const CAPTAINS_SPEED_GROUP = 516;
    private const CAPTAINS_HUNDRED_GROUP = 503;
    private const PARTNERS_GROUP = 8;

    public function __construct(
        EntityManagerInterface $em,
        NormalizerInterface $normalizer,
        AuthServerHelper $authServerHelper,
        AuthServerService $authServerService,
        LikecentreDatabaseHelper $likecentreDatabaseHelper,
        UserHelper $userHelper,
        TractionService $tractionService
    ) {
        parent::__construct($em, $normalizer);

        $this->authServerHelper = $authServerHelper;
        $this->authServerService = $authServerService;
        $this->likecentreDatabaseHelper = $likecentreDatabaseHelper;
        $this->userHelper = $userHelper;
        $this->tractionService = $tractionService;
    }

    /**
     * Пример выдачи csv
     *
     * @param Request $request
     *
     * @return Response
     * @todo: удалить
     * @Route("/speed-and-hundred/get", name="speed_and_hundred_get")
     * @IsGranted("USER_ANALYTICS_VIEW")
     */
    public function speedAndHundredAction(Request $request): Response
    {
        $csv = $this->getCsvHeader();

        $users = $this->getUsers($request);
        foreach ($users as $user) {
            $csv .= "\n" . $user;
        }

        // отдаю специально не скачиванием, чтобы было удобно вставлять в гугл док
        return new Response($csv, Response::HTTP_OK, ['content-type' => 'text/plain']);
    }

    /**
     * Пример выдачи csv
     * @Route("/club/get", name="club_get")
     * @Rest\QueryParam(name="stream", strict=true, map=true, requirements="^\d+$", description="Потоки")
     * @Rest\QueryParam(name="group", requirements="^\d+$", description="Группа")
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return Response
     */
    public function clubAction(Request $request, ParamFetcher $paramFetcher): Response
    {
        ini_set('memory_limit', '4G');
        $this->authServerService->setUserAccessToken(
            $request->headers->get('Authorization')
                ?: 'Bearer ' . $request->cookies->get('accessToken')
        );

        $streamIds = $paramFetcher->get('stream') ?: [19, 30, 32, 48];
        $groupId = $paramFetcher->get('group'); //680 - клуб ???

        $csv = $this->getCsvHeader(true);

        $streams = $this->em->getRepository(CourseStream::class)->findBy(['id' => $streamIds]);
        $usersChunks = [];
        foreach ($streams as $stream) {
            $users = $this->em->getRepository(User::class)
                ->createQueryBuilder('user')
                ->join('user.groupRelations', 'groupRelation')
                ->join('groupRelation.userGroup', 'userGroup')
                ->where('groupRelation.deleted = 0 and userGroup.courseStream = :courseStream')
                ->setParameter('courseStream', $stream)
                ->getQuery()
                ->getResult();
            $userIds = array_map(static function (User $user) {
                return $user->getId();
            }, $users);
            if ($groupId) {
                $users = $this->em->getRepository(User::class)
                    ->createQueryBuilder('user')
                    ->join('user.groupRelations', 'groupRelation')
                    ->join('groupRelation.userGroup', 'userGroup')
                    ->where('groupRelation.deleted = 0 and userGroup = :group and user in (:userIds)')
                    ->setParameter('userIds', $userIds)
                    ->setParameter('group', $groupId)
                    ->getQuery()
                    ->getResult();
                $userIds = array_map(static function (User $user) {
                    return $user->getId();
                }, $users);
            }
            $usersChunks[] = $this->getUsersData([], $userIds, $stream->getName(), true);
        }
        foreach ($usersChunks as $users) {
            foreach ($users as $user) {
                $csv .= "\n" . $user;
            }
        }

        // отдаю специально не скачиванием, чтобы было удобно вставлять в гугл док
        return new Response($csv, Response::HTTP_OK, ['content-type' => 'text/plain']);
    }

    /**
     * @Rest\Get("/fast-cash/{id<\d+>}", name="fast_cash_get")
     *
     * @OA\Parameter(name="id", in="path", required=true, @OA\Schema(type="int"), description="ID потока")
     */
    public function fastCashAction(CourseStream $courseStream, Request $request, AnalyticsService $service): Response
    {
        ini_set('memory_limit', '4G');
        $csv = $service->getFastCashReport($courseStream);

        return new Response(
            $csv,
            Response::HTTP_OK,
            ['content-type' => 'text/plain']
        );
    }

    /**
     * @Route("/speed-and-hundred/base", name="speed_and_hundred_base")
     * @IsGranted("USER_ANALYTICS_VIEW")
     * @param Request $request
     *
     * @return Response
     */
    public function speedAndHundredBase(Request $request): Response
    {
        $users = $this->getUsers($request);

        $tempInfo = [];
        if ($users !== []) {
            $tempInfo = $this->getTempInfo($users);
        }

        return $this->getResponseNew($this->parseBase($tempInfo));
    }

    /**
     * @Route("/speed-and-hundred/values", name="speed_and_hundred_values")
     * @IsGranted("USER_ANALYTICS_VIEW")
     * @param Request $request
     *
     * @return Response
     */
    public function getUserValues(Request $request): Response
    {
        $userIds = $request->get('users');
        $streamName = $request->get('stream');

        $users = $this->em->getRepository(User::class)->findById($userIds);
        $stream = $this->em->getRepository(CourseStream::class)->findOneBy(['name' => $streamName]);

        if ($users && $stream) {
            return $this->getResponseNew(
                $this->tractionService->makeByStream(
                    $stream,
                    $users
                ),
                ['user:traction']
            );
        }

        return $this->getResponseNew();
    }

    /**
     * @Route("/speed-and-hundred/filter", name="speed_and_hundred_filter")
     * @IsGranted("USER_ANALYTICS_VIEW")
     * @param Request $request
     *
     * @return Response
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function speedAndHundredFilter(Request $request): Response
    {
        $list = [];
        $page = $request->get('page', '1');
        $len = $request->get('len', '20');

        $users = $this->getUsers($request);

        if ($users !== []) {
            $tempInfo = $this->getTempInfo($users);

            switch ($request->get('report', 'team')) {
                case 'teams':
                    $list = $this->getReportGrouped($tempInfo, 'teamDict');
                    break;
                case 'capitan':
                    $list = $this->getReportGrouped($tempInfo, 'captainDict');
                    break;
                case 'faculty':
                    $list = $this->getReportGrouped($tempInfo, 'facultyDict');
                    break;
                case 'responsible':
                    $list = $this->getReportGrouped($tempInfo, 'responsiblePartnerDict');
                    break;
            }
        }

        $sortBy = $request->get('sortBy', 'fillable');
        $ascending = $request->get('ascending', 'false');
        $attribute = [];

        foreach ($list as $key => $row) {
            $attribute[$key] = $row['id'];
        }
        array_multisort($attribute, $ascending === 'true' ? SORT_ASC : SORT_DESC, $list);

        foreach ($list as $key => $row) {
            $attribute[$key] = $row[$sortBy];
        }
        array_multisort($attribute, $ascending === 'true' ? SORT_ASC : SORT_DESC, $list);

        return $this->getResponseNew(
            array_slice($list, ($page - 1) * $len, $len)
        );
    }

    private function getReportGrouped(array $tempInfo, $dict): array
    {
        $data = [];

        foreach ($tempInfo[$dict] as $id => $row) {
            $group = [
                'id' => $id,
                'name' => $row['name'],
            ];

            // Рассчет заполняемости
            $notNullTrack = 0;
            foreach ($row['users'] as $uid) {
                /** @var TractionAnalyticsUserDocument $user */
                $user = $tempInfo['userDict'][$uid];

                $group['users'][] = $this->parseUser($user);

                for ($i = 1; $i <= $tempInfo['lastWeek']; $i++) {
                    if ($user->revenue[$i] !== null) {
                        $notNullTrack++;
                    }
                }
            }

            // Прирост по выручке
            $usersToAccumulativeRevenue = array_intersect(
                $tempInfo['revenueUsersAccumulative'][$tempInfo['lastWeek']],
                $row['users']
            );

            $beforeRevenue = 0;
            $afterRevenue = 0;
            foreach ($usersToAccumulativeRevenue as $uid) {
                /** @var TractionAnalyticsUserDocument $user */
                $user = $tempInfo['userDict'][$uid];
                $beforeRevenue += $user->revenue_before;
                $afterRevenue += $user->revenue_2_weeks / ($tempInfo['lastWeek'] < 2 ? $tempInfo['lastWeek'] : 2);
            }

            // Прирост по прибыле
            $usersToAccumulativeProfit = array_intersect(
                $tempInfo['profitUsersAccumulative'][$tempInfo['lastWeek']],
                $row['users']
            );

            $beforeProfit = 0;
            $afterProfit = 0;
            foreach ($usersToAccumulativeProfit as $uid) {
                /** @var TractionAnalyticsUserDocument $user */
                $user = $tempInfo['userDict'][$uid];
                $beforeProfit += $user->profit_before;
                $afterProfit += $user->profit_2_weeks / ($tempInfo['lastWeek'] < 2 ? $tempInfo['lastWeek'] : 2);
            }

            $fillable = (count($row['users']) * $tempInfo['lastWeek'])
                ? ($notNullTrack / (count($row['users']) * $tempInfo['lastWeek'])) * 100
                : 0;
            $revenueGrowth = $beforeRevenue ? ($afterRevenue - $beforeRevenue) / $beforeRevenue * 100 : 999;
            $profitGrowth = $beforeProfit ? ($afterProfit - $beforeProfit) / $beforeProfit * 100 : 999;
            $revenueToOne = $usersToAccumulativeRevenue ? $afterRevenue / count($usersToAccumulativeRevenue) : 0;
            $profitToOne = $usersToAccumulativeProfit ? $afterProfit / count($usersToAccumulativeProfit) : 0;

            $group['fillable'] = round($fillable, 1);
            $group['revenueGrowth'] = round($revenueGrowth, 1);
            $group['profitGrowth'] = round($profitGrowth, 1);
            $group['revenueToOne'] = round($revenueToOne);
            $group['profitToOne'] = round($profitToOne);

            $data[] = $group;
        }

        return $data;
    }

    /**
     * @param TractionAnalyticsUserDocument $user
     *
     * @return array
     */
    private function parseUser(TractionAnalyticsUserDocument $user): array
    {
        return [
            'lmsUserId' => $user->lmsUserId,
            'revenue_average' => $user->revenue_average,
            'revenue_growth' => $user->revenue_growth,
            'profit_average' => $user->profit_average,
            'profit_growth' => $user->profit_growth,
            'name' => $user->name,
            'email' => $user->email,
            'phone' => $user->phone,
            'city' => $user->city,
            'facultyId' => $user->facultyId,
            'facultyName' => $user->facultyName,
            'addedBy' => $user->addedBy,
            'captainId' => $user->captainId,
            'captainName' => $user->captainName,
            'teamName' => $user->teamName,
            'teamId' => $user->teamId,
            'amoLeadId' => $user->amoLeadId,
            'responsiblePartnerId' => $user->responsiblePartnerId,
            'responsiblePartnerName' => $user->responsiblePartnerName,
            'lessonViews' => $user->lessonViews,
            'revenue_before' => $user->revenue_before,
            'revenue' => $user->revenue,
            'profit_before' => $user->profit_before,
            'profit' => $user->profit,
            'values' => $user->values,
        ];
    }


    private function getTempInfo(array $users): array
    {
        // Словарь полоьзователей param => Entity
        $userDict = [];
        $teamDict = [];
        $captainDict = [];
        $responsiblePartnerDict = [];
        $facultyDict = [];

        $tempData = [
            'totalUser' => count($users),
            'revenueUsers' => [],
            'profitUsers' => [],
            'revenueUsersAccumulative' => [],
            'profitUsersAccumulative' => [],
        ];

        /** @var TractionAnalyticsUserDocument $user */
        foreach ($users as $user) {
            $uid = $user->lmsUserId;

            // Словарь пользователей
            $userDict[$uid] = $user;

            // Словарь команд
            $teamDict[$user->teamId ?? -1]['name'] = $user->teamName ?? 'Не распределен';
            $teamDict[$user->teamId ?? -1]['users'][] = $uid;

            // Словарь капитанов
            $captainDict[$user->captainId ?? -1]['name'] = $user->captainName ?? 'Нет капитана';
            $captainDict[$user->captainId ?? -1]['users'][] = $uid;

            // Словарь партнеров
            $responsiblePartnerDict[$user->responsiblePartnerId ?? -1]['name'] =
                $user->responsiblePartnerName ?? 'Нет партнера';
            $responsiblePartnerDict[$user->responsiblePartnerId ?? -1]['users'][] = $uid;

            // Словарь факультетов
            $facultyDict[$user->facultyId ?? -1]['name'] = $user->facultyName ?? 'Не распределен';
            $facultyDict[$user->facultyId ?? -1]['users'][] = $uid;

            // Собираем пользователей по трекшену
            if ($user->revenue_before !== null) {
                $tempData['revenueUsers'][0][] = $uid;
            }

            if ($user->profit_before !== null) {
                $tempData['profitUsers'][0][] = $uid;
            }

            foreach ($user->revenue as $key => $value) {
                if (!isset($tempData['revenueUsers'][$key])) {
                    $tempData['revenueUsers'][$key] = [];
                }
                if ($value !== null) {
                    $tempData['revenueUsers'][$key][] = $uid;
                }
            }

            foreach ($user->profit as $key => $value) {
                if (!isset($tempData['profitUsers'][$key])) {
                    $tempData['profitUsers'][$key] = [];
                }
                if ($value !== null) {
                    $tempData['profitUsers'][$key][] = $uid;
                }
            }

            // avRevenue and growth
            $rev = $user->revenue;
            $count = count($rev);
            $avRevenue = 0;

            if ($rev[1] !== null && $rev[2] === null) {
                $avRevenue = $rev[1];
            } elseif ($rev[1] !== null && $rev[2] !== null) {
                for ($i = 2; $i < $count; $i++) {
                    if ($rev[$i] !== null && $rev[$i - 1] !== null) {
                        $avRevenue = round(($rev[$i] + $rev[$i - 1]) / 2);
                    } else {
                        break;
                    }
                }
            }

            $revenueGrowth = $user->revenue_before ?
                ($avRevenue - $user->revenue_before) / $user->revenue_before * 100 : 999;

            // avProfit and growth
            $prof = $user->profit;
            $count = count($prof);
            $avProfit = 0;

            if ($prof[1] !== null && $prof[2] === null) {
                $avProfit = $prof[1];
            } elseif ($prof[1] !== null && $prof[2] !== null) {
                for ($i = 2; $i < $count; $i++) {
                    if ($prof[$i] !== null && $prof[$i - 1] !== null) {
                        $avProfit = round(($prof[$i] + $prof[$i - 1]) / 2);
                    } else {
                        break;
                    }
                }
            }

            $profitGrowth = $user->profit_before ?
                ($avProfit - $user->profit_before) / $user->profit_before * 100 : 999;

            $user->revenue_average = round($avRevenue);
            $user->revenue_growth = round($revenueGrowth, 1);
            $user->profit_average = round($avProfit);
            $user->profit_growth = round($profitGrowth, 1);
        }

        $tempData['revenueUsersAccumulative'][0] = $tempData['revenueUsers'][0] ?? [];
        $k = count($tempData['revenueUsers']);
        for ($i = 1; $i < $k; $i++) {
            $tempData['revenueUsersAccumulative'][$i] = array_intersect(
                $tempData['revenueUsersAccumulative'][$i - 1],
                $tempData['revenueUsers'][$i]
            );
        }

        $tempData['profitUsersAccumulative'][0] = $tempData['profitUsers'][0] ?? [];
        $k = count($tempData['profitUsers']);
        for ($i = 1; $i < $k; $i++) {
            $tempData['profitUsersAccumulative'][$i] = array_intersect(
                $tempData['profitUsersAccumulative'][$i - 1],
                $tempData['profitUsers'][$i]
            );
        }

        $revenue = $tempData['revenueUsersAccumulative'];

        $lastWeek = 0;
        $countOfDataWeek = count($revenue[$lastWeek]);
        $countOfDataNextWeek = array_key_exists($lastWeek + 1, $revenue) ? count($revenue[$lastWeek + 1]) : $lastWeek;
        while (
            $lastWeek < count($revenue)
            && $lastWeek < 6
            && ($countOfDataWeek > 10 && $countOfDataNextWeek / $countOfDataWeek > 0.7)
        ) {
            $lastWeek++;
            $countOfDataWeek = count($revenue[$lastWeek]);
            $countOfDataNextWeek = count($revenue[$lastWeek + 1] ?? []);
        }

        if ($lastWeek === 0 && array_key_exists(1, $tempData['revenueUsersAccumulative'])) {
            $lastWeek = 1;
        }

        foreach ($tempData['revenueUsersAccumulative'][$lastWeek] as $uid) {
            /** @var TractionAnalyticsUserDocument $user */
            $user = $userDict[$uid];
            $user->revenue_count_max = $lastWeek;
            $k = 0;

            for ($i = $lastWeek; $i > 0; $i--) {
                $user->revenue_course += $user->revenue[$i];

                if ($k < 2) {
                    $user->revenue_2_weeks += $user->revenue[$i];
                }

                if ($k < 4) {
                    $user->revenue_4_weeks += $user->revenue[$i];
                }
                $k++;
            }

            $userDict[$uid] = $user;
        }

        foreach ($tempData['profitUsersAccumulative'][$lastWeek] as $uid) {
            /** @var TractionAnalyticsUserDocument $user */
            $user = $userDict[$uid];
            $user->profit_count_max = $lastWeek;
            $k = 0;

            for ($i = $lastWeek; $i > 0; $i--) {
                $user->profit_course += $user->profit[$i];

                if ($k < 2) {
                    $user->profit_2_weeks += $user->profit[$i];
                }

                if ($k < 4) {
                    $user->profit_4_weeks += $user->profit[$i];
                }
                $k++;
            }

            $userDict[$uid] = $user;
        }

        $tempData['userDict'] = $userDict;
        $tempData['teamDict'] = $teamDict;
        $tempData['captainDict'] = $captainDict;
        $tempData['responsiblePartnerDict'] = $responsiblePartnerDict;
        $tempData['facultyDict'] = $facultyDict;
        $tempData['lastWeek'] = $lastWeek;

        return $tempData;
    }

    private function parseBase(array $temp): array
    {
        if ($temp == []) {
            return [];
        }

        $base = [
            'total_count' => $temp['totalUser'],
            'current_week' => $temp['lastWeek'],
            'revenueFilled' => [
                'totalUser' => $temp['totalUser'],
                'weeks' => [],
            ],
            'revenueAccumulativeFilled' => [
                'totalUser' => $temp['totalUser'],
                'weeks' => [],
            ],
            'profitFilled' => [
                'totalUser' => $temp['totalUser'],
                'weeks' => [],
            ],
            'profitAccumulativeFilled' => [
                'totalUser' => $temp['totalUser'],
                'weeks' => [],
            ],
        ];

        foreach ($temp['revenueUsers'] as $weekNum => $uids) {
            $sum = 0;

            foreach ($uids as $uid) {
                $user = $temp['userDict'][$uid];

                $sum += $weekNum === 0 ?
                    $user->revenue_before :
                    $user->revenue[$weekNum];
            }
            $base['revenueFilled']['weeks'][$weekNum] = [
                'fill' => count($uids),
                'fillPercent' => round(count($uids) / $temp['totalUser'] * 100),
                'sum' => $sum,
                'avSum' => count($uids) ? round($sum / count($uids)) : 0,
            ];
        }

        foreach ($temp['profitUsers'] as $weekNum => $uids) {
            $sum = 0;

            foreach ($uids as $uid) {
                $user = $temp['userDict'][$uid];

                $sum += $weekNum === 0 ?
                    $user->profit_before :
                    $user->profit[$weekNum];
            }
            $base['profitFilled']['weeks'][$weekNum] = [
                'fill' => count($uids),
                'fillPercent' => round(count($uids) / $temp['totalUser'] * 100),
                'sum' => $sum,
                'avSum' => count($uids) ? round($sum / count($uids)) : 0,
            ];
        }

        foreach ($temp['revenueUsersAccumulative'] as $weekNum => $uids) {
            $sum = 0;

            foreach ($uids as $uid) {
                $user = $temp['userDict'][$uid];

                $sum += $weekNum === 0 ?
                    $user->revenue_before :
                    $user->revenue[$weekNum];
            }
            $base['revenueAccumulativeFilled']['weeks'][$weekNum] = [
                'fill' => count($uids),
                'fillPercent' => round(count($uids) / $temp['totalUser'] * 100),
                'sum' => $sum,
                'avSum' => count($uids) ? round($sum / count($uids)) : 0,
            ];
        }

        foreach ($temp['profitUsersAccumulative'] as $weekNum => $uids) {
            $sum = 0;

            foreach ($uids as $uid) {
                $user = $temp['userDict'][$uid];

                $sum += $weekNum === 0 ?
                    $user->profit_before :
                    $user->profit[$weekNum];
            }

            $base['profitAccumulativeFilled']['weeks'][$weekNum] = [
                'fill' => count($uids),
                'fillPercent' => round(count($uids) / $temp['totalUser'] * 100),
                'sum' => $sum,
                'avSum' => count($uids) ? round($sum / count($uids)) : 0,
            ];
        }

        $totalRevenueBefore = 0;
        $totalRevenueAfter = 0;
        $peopleWith0CountRevenue = 0;
        $firstMoneyCountRevenue = 0;
        $first10kCountRevenue = 0;
        $first100kCountRevenue = 0;
        $growth10CountRevenue = 0;
        $growth20CountRevenue = 0;
        $growth50CountRevenue = 0;
        $growth100CountRevenue = 0;

        $totalProfitBefore = 0;
        $totalProfitAfter = 0;
        $peopleWith0CountProfit = 0;
        $firstMoneyCountProfit = 0;
        $first10kCountProfit = 0;
        $first100kCountProfit = 0;
        $growth10CountProfit = 0;
        $growth20CountProfit = 0;
        $growth50CountProfit = 0;
        $growth100CountProfit = 0;

        $lastWeek = $temp['lastWeek'];
        $minCount = $lastWeek < 2 ? $lastWeek : 2;

        if ($minCount) {
            foreach ($temp['revenueUsersAccumulative'][$lastWeek] as $uid) {
                /** @var TractionAnalyticsUserDocument $user */
                $user = $temp['userDict'][$uid];

                $totalRevenueBefore += $user->revenue_before;
                $totalRevenueAfter += $user->revenue_2_weeks / $minCount;
                $totalProfitBefore += $user->profit_before;
                $totalProfitAfter += $user->profit_2_weeks / $minCount;

                if ($user->revenue_before == 0) {
                    $peopleWith0CountRevenue++;
                    if ($user->revenue_course > 0) {
                        $firstMoneyCountRevenue++;
                    }
                    if ($user->revenue_course > 10000) {
                        $first10kCountRevenue++;
                    }
                    if ($user->revenue_course > 100000) {
                        $first100kCountRevenue++;
                    }
                } else {
                    if (($user->revenue_2_weeks / $minCount - $user->revenue_before) / $user->revenue_before > 0.1) {
                        $growth10CountRevenue++;
                    }
                    if (($user->revenue_2_weeks / $minCount - $user->revenue_before) / $user->revenue_before > 0.2) {
                        $growth20CountRevenue++;
                    }
                    if (($user->revenue_2_weeks / $minCount - $user->revenue_before) / $user->revenue_before > 0.5) {
                        $growth50CountRevenue++;
                    }
                    if (($user->revenue_2_weeks / $minCount - $user->revenue_before) / $user->revenue_before > 1) {
                        $growth100CountRevenue++;
                    }
                }

                if ($user->profit_before == 0) {
                    $peopleWith0CountProfit++;
                    if ($user->profit_course > 0) {
                        $firstMoneyCountProfit++;
                    }
                    if ($user->profit_course > 10000) {
                        $first10kCountProfit++;
                    }
                    if ($user->profit_course > 100000) {
                        $first100kCountProfit++;
                    }
                } else {
                    if (($user->profit_2_weeks / $minCount - $user->profit_before) / $user->profit_before > 0.1) {
                        $growth10CountProfit++;
                    }
                    if (($user->profit_2_weeks / $minCount - $user->profit_before) / $user->profit_before > 0.2) {
                        $growth20CountProfit++;
                    }
                    if (($user->profit_2_weeks / $minCount - $user->profit_before) / $user->profit_before > 0.5) {
                        $growth50CountProfit++;
                    }
                    if (($user->profit_2_weeks / $minCount - $user->profit_before) / $user->profit_before > 1) {
                        $growth100CountProfit++;
                    }
                }
                $userDict[$uid] = $user;

                $totalRevenueBefore += $user->revenue_before;
                $totalRevenueAfter += $minCount ? $user->revenue_2_weeks / $minCount : 0;
                $totalProfitBefore += $user->profit_before;
                $totalProfitAfter += $minCount ? $user->profit_2_weeks / $minCount : 0;
            }
        }


        $count = count($temp['revenueUsersAccumulative'][$lastWeek]);

        $base['revenueInc'] = [
            'peopleWith0Count' => $peopleWith0CountRevenue,
            'peopleNot0Count' => $count - $peopleWith0CountRevenue,
            'rub' => $count ? round(($totalRevenueAfter - $totalRevenueBefore) / $count) : 0,
            'percent' => $totalRevenueBefore
                ? round(($totalRevenueAfter - $totalRevenueBefore) / $totalRevenueBefore * 100)
                : 0,
            'more' => [
                'firstMoney' => [
                    'people' => $firstMoneyCountRevenue,
                    'percent' => $peopleWith0CountRevenue ? round(
                        $firstMoneyCountRevenue / $peopleWith0CountRevenue * 100,
                        1
                    ) : 0,
                ],
                'first10k' => [
                    'people' => $first10kCountRevenue,
                    'percent' => $peopleWith0CountRevenue ? round(
                        $first10kCountRevenue / $peopleWith0CountRevenue * 100,
                        1
                    ) : 0,
                ],
                'first100k' => [
                    'people' => $first100kCountRevenue,
                    'percent' => $peopleWith0CountRevenue ? round(
                        $first100kCountRevenue / $peopleWith0CountRevenue * 100,
                        1
                    ) : 0,
                ],
                'growth10' => [
                    'people' => $growth10CountRevenue,
                    'percent' => ($count - $peopleWith0CountRevenue) ? round(
                        $growth10CountRevenue / ($count - $peopleWith0CountRevenue) * 100,
                        1
                    ) : 0,
                ],
                'growth20' => [
                    'people' => $growth20CountRevenue,
                    'percent' => ($count - $peopleWith0CountRevenue) ? round(
                        $growth20CountRevenue / ($count - $peopleWith0CountRevenue) * 100,
                        1
                    ) : 0,
                ],
                'growth50' => [
                    'people' => $growth50CountRevenue,
                    'percent' => ($count - $peopleWith0CountRevenue) ? round(
                        $growth50CountRevenue / ($count - $peopleWith0CountRevenue) * 100,
                        1
                    ) : 0,
                ],
                'growth100' => [
                    'people' => $growth100CountRevenue,
                    'percent' => ($count - $peopleWith0CountRevenue) ? round(
                        $growth100CountRevenue / ($count - $peopleWith0CountRevenue) * 100,
                        1
                    ) : 0,
                ],
            ],
        ];

        $base['profitInc'] = [
            'peopleWith0Count' => $peopleWith0CountProfit,
            'peopleNot0Count' => $count - $peopleWith0CountProfit,
            'rub' => $count ? round(($totalProfitAfter - $totalProfitBefore) / $count) : 0,
            'percent' => $totalProfitBefore ? round(
                ($totalProfitAfter - $totalProfitBefore) / $totalProfitBefore * 100
            ) : 0,
            'more' => [
                'firstMoney' => [
                    'people' => $firstMoneyCountProfit,
                    'percent' => $peopleWith0CountProfit ? round(
                        $firstMoneyCountProfit / $peopleWith0CountProfit * 100,
                        1
                    ) : 0,
                ],
                'first10k' => [
                    'people' => $first10kCountProfit,
                    'percent' => $peopleWith0CountProfit ? round(
                        $first10kCountProfit / $peopleWith0CountProfit * 100,
                        1
                    ) : 0,
                ],
                'first100k' => [
                    'people' => $first100kCountProfit,
                    'percent' => $peopleWith0CountProfit ? round(
                        $first100kCountProfit / $peopleWith0CountProfit * 100,
                        1
                    ) : 0,
                ],
                'growth10' => [
                    'people' => $growth10CountProfit,
                    'percent' => ($count - $peopleWith0CountProfit) ? round(
                        $growth10CountProfit / ($count - $peopleWith0CountProfit) * 100,
                        1
                    ) : 0,
                ],
                'growth20' => [
                    'people' => $growth20CountProfit,
                    'percent' => ($count - $peopleWith0CountProfit) ? round(
                        $growth20CountProfit / ($count - $peopleWith0CountProfit) * 100,
                        1
                    ) : 0,
                ],
                'growth50' => [
                    'people' => $growth50CountProfit,
                    'percent' => ($count - $peopleWith0CountProfit) ? round(
                        $growth50CountProfit / ($count - $peopleWith0CountProfit) * 100,
                        1
                    ) : 0,
                ],
                'growth100' => [
                    'people' => $growth100CountProfit,
                    'percent' => ($count - $peopleWith0CountProfit) ? round(
                        $growth100CountProfit / ($count - $peopleWith0CountProfit) * 100,
                        1
                    ) : 0,
                ],
            ],
        ];

        return $base;
    }

    /**
     * Получение списка потоков и амо-продуктов для фронта
     * @Route("/dictionary", name="dictionary")
     * @IsGranted("USER_ANALYTICS_VIEW")
     *
     * @OA\Parameter(
     *     name="csId",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="integer"),
     *     description="CourseStream ID"
     * )
     *
     * @param Request $request
     *
     * @return Response
     */
    public function dictionaryAction(Request $request): Response
    {
        $sql = <<<SQL
            SELECT amo_product_name AS title, amo_product_name as id
            FROM amo_products
            WHERE (amo_product_name LIKE 'скорость клуб%' OR amo_product_name LIKE 'Сотка%' OR
                   amo_product_name LIKE 'Скорость. Клуб%' OR amo_product_name LIKE '%МСА-LITE%')
              AND amo_product_name NOT LIKE '%автосделки%'
              AND amo_product_name NOT LIKE '%экватор%'
            ORDER BY amo_product_name;
        SQL;
        $amoProducts = $this->likecentreDatabaseHelper->execute($sql);

        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(CourseStream::class)->createQueryBuilder('cs');
        $courseStreams = $qb
            ->innerJoin('cs.abstractCourse', 'ac')
            ->where("ac.slug = 'speed' AND cs.stream > 18")
            ->orWhere("ac.slug = 'speed_club'")
            ->orWhere("ac.slug = 'msa_lite'")
            ->orWhere("ac.slug = 'hundred' AND cs.stream > 3")
            ->getQuery()
            ->getResult();

        $faculties = $this->em->getRepository(Faculty::class)->createQueryBuilder('f')
            ->innerJoin('f.userGroupUsers', 'ugu')
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ug.courseStream', 'cs')
            ->innerJoin('ugu.user', 'u')
            ->innerJoin('f.courseStream', 'courseStream')
            ->andWhere('ugu.deleted = 0')
            ->andWhere('courseStream.id = :csId')
            ->setParameter('csId', $request->get('csId'))
            ->orderBy('f.name')
            ->groupBy('f.id')
            ->getQuery()
            ->getResult();

        return $this->getResponseNew([
            'amoProducts' => $amoProducts,
            'courseStreams' => $this->normalize($courseStreams, ['traction']),
            'faculties' => $this->normalize($faculties, ['traction']),
        ]);
    }

    /**
     * @param Request $request
     *
     * @return TractionAnalyticsUserDocument[]
     */
    private function getUsers(Request $request): array
    {
        $this->authServerService->setUserAccessToken(
            $request->headers->get('Authorization')
                ?: 'Bearer ' . $request->cookies->get('accessToken')
        );
        $requestUser = $this->getUser();
        $permissions = $this->userHelper->getUserPermissionsByField($this->getUser(), 'analyticsModelAdmin');
        if ($permissions['isAdmin']) {
            $requestUser = null;
        }

        $amoProducts = $request->get('amoProducts', '["Скорость Клуб 2"]');
        $courseName = $request->get('courseName', 'Скорость Клуб 2');
        $budgetFrom = $request->get('budgetMin');
        $budgetTo = $request->get('budgetMax');
        $folderId = $request->get('folderId', 130);

        $amoProducts = implode('\', \'', json_decode($amoProducts));
        // берем из амо емейлы клиентов
        $sql = <<<SQL
            SELECT c.email, l.budget, l.amo_lead_id
            FROM leads l
                     INNER JOIN amo_products ap ON l.amo_product_id = ap.amo_product_id
                     INNER JOIN contacts c ON l.contact_id = c.id
            WHERE l.amo_status_id in(142,40737843)
              AND ap.amo_product_name IN ('{$amoProducts}')
              AND l.deleted_at IS NULL
        SQL;
        if ($budgetFrom) {
            $sql .= "\n      AND l.budget >= {$budgetFrom}";
        }
        if ($budgetTo) {
            $sql .= "\n      AND l.budget <= {$budgetTo}";
        }
        $sql .= "\n    GROUP BY c.email, l.budget, l.amo_lead_id;";
        $amoRows = $this->likecentreDatabaseHelper->execute($sql);

        // готовим емейлы, чтобы забрать с Аутха
        $emails = '';
        $budgets = $leadIds = [];
        foreach ($amoRows as $amoRow) {
            $emails .= sprintf("'%s',", $amoRow['email']);
            $budgets[$amoRow['amo_lead_id']] = $amoRow['budget'];
            $leadIds[mb_strtolower($amoRow['email'])] = $amoRow['amo_lead_id'];
        }
        $emails = mb_substr($emails, 0, -1);

        // ищем ответственных партнеров
        // сделал отдельным запросом, потому что на INNER JOIN responsible не хватает индексов
        $leadsIdsString = implode(',', $leadIds);

        $responsibleRows = [];
        if ($leadsIdsString) {
            $sql = <<<SQL
            SELECT l.amo_lead_id, r.name, r.id
            FROM leads l
                     INNER JOIN responsibles r ON l.responsible_id = r.amo_id
            WHERE r.deleted_at IS NULL
              AND l.amo_lead_id IN({$leadsIdsString})
              AND r.group_name = 'Франчайзи'
        SQL;
            $responsibleRows = $this->likecentreDatabaseHelper->execute($sql);
        }

        $responsible = [];
        foreach ($responsibleRows as $responsibleRow) {
            $responsible[$responsibleRow['amo_lead_id']] = [
                'id' => $responsibleRow['id'],
                'name' => $responsibleRow['name'],
            ];
        }

        // забираем юзеров с Аутха
        $authUsers = $this->authServerHelper->getUsers([], ['id'], "u.email IN({$emails}) AND u.email IS NOT NULL");
        $userIds = array_column($authUsers, 'id');

        return $this->getUsersData(
            [
                'folderId' => $folderId,
                'leadIds' => $leadIds,
                'budgets' => $budgets,
                'revenueMin' => $request->get('revenueMin'),
                'revenueMax' => $request->get('revenueMax'),
                'responsible' => $responsible,
                'captainName' => $request->get('captainName'),
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'city' => $request->get('city'),
                'facultyId' => (int)$request->get('facultyId'),
            ],
            $userIds,
            $courseName,
            false,
            $requestUser
        );
    }

    protected function getUsersData(
        array $params,
        array $userIds,
        string $courseName,
        bool $unlim = false,
        ?User $requestUser = null
    ) {
        $folderId = null;
        $leadIds = [];
        $budgets = [];
        $responsible = [];
        $revenueMin = null;
        $revenueMax = null;
        $captainName = null;
        $name = null;
        $email = null;
        $city = null;
        $facultyId = null;
        if (!empty($params)) {
            $folderId = $params['folderId'];
            $leadIds = $params['leadIds'];
            $budgets = $params['budgets'];
            $responsible = $params['responsible'];
            $revenueMin = $params['revenueMin'];
            $revenueMax = $params['revenueMax'];
            $captainName = $params['captainName'];
            $name = $params['name'];
            $email = $params['email'];
            $city = $params['city'];
            $facultyId = $params['facultyId'];
        }
        $requestGroup = null;
        if ($requestUser) {
            $requestGroup = $this->getRequestUserGroup($requestUser);
        }

        // берем всех учеников и их группы на потоке
        $rows = $this->em->getRepository(UserGroupUser::class)
            ->createQueryBuilder('ugu')
            ->select(
                'u.authUserId',
                'u.id',
                'ug.name',
                'ug.id AS groupId',
                'ugu.amoLeadId',
                'added.authUserId AS addedAuthId',
                'added.id AS addedId'
            )
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ug.courseStream', 'cs')
            ->innerJoin('ugu.user', 'u')
            ->leftJoin('ugu.addedBy', 'added')
            ->andWhere('cs.name = :course')
            ->setParameter('course', $courseName)
            ->andWhere('ugu.deleted = 0')
            ->andWhere('u.id IN(:userIds)')
            ->setParameter('userIds', $userIds)
            ->andWhere("ug.name NOT LIKE '%факультет%'")
            ->andWhere("ug.name NOT LIKE '%сборная%'")
            ->groupBy(
                'u.authUserId',
                'u.id',
                'ug.name',
                'ug.id',
                'ugu.amoLeadId',
                'added.authUserId',
                'added.id'
            )
            ->orderBy('ug.id', 'ASC')
            ->getQuery()
            ->getResult();

        $groups = $addedByIds = [];
        foreach ($rows as $row) {
            $userId = $row['id'];
            $groups[$userId] = $row['name'];

            if ($row['addedId']) {
                $addedByIds[$userId] = [
                    'id' => $row['addedId'],
                    'authId' => $row['addedAuthId'],
                ];
            }
        }

        // забираем аутх пользователей
        $authUserIds = array_values(array_unique(array_column($rows, 'authUserId')));
        unset($rows);

        $authRows = $this->authServerService->filterUsers(
            [
                'ids' => $authUserIds,
                'name' => $name,
                'email' => $email,
                'city' => $city,
            ],
            ['lms', 'user:preview', 'user:business'],
            1,
            count($authUserIds)
        )['data']['users'];

        $ids = array_column($authRows, 'id');

        $results = $this->getUserResults($ids, $courseName, $unlim);
        $faculties = $this->getUserFaculties($ids, $courseName);
        $badGroups = $this->getBadGroups($ids);
        [$deletedAlternativeGroups, $alternativeGroups] = $this->getAlternativeGroups($ids, $courseName);

        $teams = $this->getUserTeams($ids, $courseName);

        // забираем капитанов
        $captainAuthIds = array_filter(
            array_unique(array_column($teams, 'captainAuthId')),
            static function ($id) {
                return ($id);
            }
        );
        $captains = [];
        if (!empty($captainAuthIds)) {
            try {
                $authCaptains = $this->authServerService->filterUsers(
                    [
                        'ids' => $captainAuthIds,
                        'name' => $captainName,
                    ],
                    ['lms', 'user:preview'],
                    1,
                    count($captainAuthIds)
                )['data']['users'];
                foreach ($authCaptains as $authCaptain) {
                    $captains[$authCaptain['id']] = sprintf(
                        "%s %s",
                        $authCaptain['info']['name'],
                        $authCaptain['info']['lastname']
                    );
                }
            } catch (\Exception $e) {
                // ...
            }
        }

        // забираем тех, кто добавляет в группы
        $addedByNames = [];
        $addedByAuthIds = array_unique(array_column($addedByIds, 'authId'));
        if (!empty($addedByAuthIds)) {
            $authAddedByRows = $this->authServerHelper->getUsers($addedByAuthIds, ['id', 'name', 'lastname', 'email']);
            foreach ($authAddedByRows as $authAddedByRow) {
                $addedByNames[$authAddedByRow['id']] = sprintf(
                    "%s %s (%s)",
                    $authAddedByRow['name'],
                    $authAddedByRow['lastname'],
                    $authAddedByRow['email'],
                );
            }
        }

        // ищем просмотры уроков
        $lessonViews = $this->countLessonViews($userIds, $folderId);

        $users = [];
        foreach ($authRows as $authRow) {
            $user = new TractionAnalyticsUserDocument();
            $user->lmsUserId = $authRow['id'];

            // Если это пользователь (капитан или партнер) запрашивает фильтровать для него
            if ($requestGroup) {
                if (
                    empty($authRows['city'])
                    || (
                        $requestGroup === self::PARTNERS_GROUP
                        && mb_strtolower($authRows['city']) !== mb_strtolower($requestUser->getCity())
                    )
                ) {
                    continue;
                }
                if (
                    (
                        $requestGroup === self::MAJOR_CAPTAINS_SPEED_GROUP
                        || $requestGroup === self::CAPTAINS_SPEED_GROUP
                        || $requestGroup === self::MAJOR_CAPTAINS_HUNDRED_GROUP
                        || $requestGroup === self::CAPTAINS_HUNDRED_GROUP
                    )
                    && $captains[$teams[$user->lmsUserId]['captainId']] !== $requestUser->getId()
                ) {
                    continue;
                }
            }

            // Если в фильтрах есть поиск по выручке, отсортировывать нужно выручку до
            if ($revenueMin || $revenueMax) {
                if (!isset($results[$authRow['id']]['revenue_before'])) {
                    continue;
                }

                if (
                    ($revenueMin && $results[$authRow['id']]['revenue_before'] < $revenueMin) ||
                    ($revenueMax && $results[$authRow['id']]['revenue_before'] > $revenueMax)
                ) {
                    continue;
                }
            }

            // Фильтруем по найденным капитанам по имени
            if (
                $captainName &&
                (!isset($teams[$user->lmsUserId]) ||
                    !isset($captains[$teams[$user->lmsUserId]['captainId']]) ||
                    $captains[$teams[$user->lmsUserId]['captainId']] === null)
            ) {
                continue;
            }

            // Фильтруем по найденным капитанам по имени
            if (
                $facultyId &&
                (!isset($faculties[$user->lmsUserId]) ||
                    $faculties[$user->lmsUserId]['id'] !== $facultyId)
            ) {
                continue;
            }

            $user->email = $authRow['email'];
            $user->phone = $authRow['info']['phone'];
            $user->name = $authRow['info']['name'];
            $user->lastname = $authRow['info']['lastname'];
            $user->city = $authRow['info']['city'];
            $user->niche = $authRow['info']['niche'];
            $user->amoLeadId = $leadIds[$user->email] ?? null;
            $user->niche = str_replace(';', ',', $authRow['info']['niche']);
            $user->lessonViews = $lessonViews[$user->lmsUserId] ?? 0;

            $user->teamName = $teams[$user->lmsUserId]['name'] ?? null;
            $user->teamId = $teams[$user->lmsUserId]['teamId'] ?? null;

            if (isset($teams[$user->lmsUserId], $captains[$teams[$user->lmsUserId]['captainId']])) {
                $user->captainName = $captains[$teams[$user->lmsUserId]['captainId']];
                $user->captainId = $teams[$user->lmsUserId]['captainId'];
            }

            if (isset($addedByIds[$user->lmsUserId], $addedByNames[$addedByIds[$user->lmsUserId]['id']])) {
                $user->addedBy = $addedByNames[$addedByIds[$user->lmsUserId]['id']];
            }

            $user->groupName = $groups[$user->lmsUserId] ?? null;

            $user->facultyName = $faculties[$user->lmsUserId]['name'] ?? null;
            $user->facultyId = $faculties[$user->lmsUserId]['id'] ?? null;
            $user->alternativeGroupName = $alternativeGroups[$user->lmsUserId] ?? null;
            $user->deletedAlternativeGroupName = $deletedAlternativeGroups[$user->lmsUserId] ?? null;
            $user->badGroup = $badGroups[$user->lmsUserId] ?? null;
            $user->budget = $budgets[$user->amoLeadId] ?? null;
            $user->responsiblePartnerId = $responsible[$user->amoLeadId]['id'] ?? null;
            $user->responsiblePartnerName = $responsible[$user->amoLeadId]['name'] ?? null;

            preg_match('#\((.*?)\)#', $user->groupName, $match);
            $user->tariff = $match[1] ?? null;

            foreach ($results[$user->lmsUserId] ?? [] as $resultProperty => $userResults) {
                $user->$resultProperty = $userResults;
            }

            $users[] = $user;
        }

        return $users;
    }

    private function getUserFaculties(array $ids, string $courseName): array
    {
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(Faculty::class)->createQueryBuilder('f');

        $rows = $qb
            ->select('u.id', 'f.name', 'f.id as facultyId')
            ->innerJoin('f.userGroupUsers', 'ugu')
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ug.courseStream', 'cs')
            ->innerJoin('ugu.user', 'u')
            ->andWhere('ugu.deleted = 0')
            ->andWhere('cs.name = :course')
            ->andWhere('u.id IN(:ids)')
            ->setParameters(
                [
                    'ids' => $ids,
                    'course' => $courseName,
                ]
            )
            ->groupBy('u.id', 'f.name', 'facultyId')
            ->getQuery()
            ->getResult();

        $users = [];
        foreach ($rows as $row) {
            $users[$row['id']] = $row['name'];
            $users[$row['id']] = [
                'name' => $row['name'],
                'id' => $row['facultyId'],
            ];
        }

        return $users;
    }

    private function getUserTeams(array $ids, string $courseName): array
    {
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(Team::class)->createQueryBuilder('t');

        $rows = $qb
            ->select('u.id', 't.id AS teamId', 't.name', 'c.authUserId AS captainAuthId', 'c.id AS captainId')
            ->innerJoin('t.folder', 'f')
            ->innerJoin('t.users', 'u')
            ->leftJoin('t.captain', 'c')
            ->andWhere('f.name = :course')
            ->andWhere('u.id IN(:ids)')
            ->setParameters(
                [
                    'ids' => $ids,
                    'course' => $courseName,
                ]
            )
            ->groupBy('u.id', 't.id', 't.name', 'c.authUserId', 'c.id')
            ->getQuery()
            ->getResult();

        $users = [];
        foreach ($rows as $row) {
            $users[$row['id']] = [
                'name' => $row['name'],
                'captainAuthId' => $row['captainAuthId'],
                'captainId' => $row['captainId'],
                'teamId' => $row['teamId'],
            ];
        }

        return $users;
    }

    private function getUserResults(array $userIds, string $courseName, bool $unlim = false): array
    {
        $results = [];
        foreach ($userIds as $userId) {
            foreach (ValueType::PROFIT_AND_REVENUE_CODE_NAMES as $channel) {
                foreach (range(1, $unlim ? 30 : 6) as $week) {
                    $results[$userId][$channel][$week] = null;
                }
            }
        }

        /** @var CourseStream $courseStream */
        $courseStream = $this->em->getRepository(CourseStream::class)->findOneBy(['name' => $courseName]);
        /** @var Value[] $values */
        $qb = $this->em->getRepository(Value::class)
            ->createQueryBuilder('value')
            ->join('value.type', 'valueType')
            ->where('valueType.title in (:valueTypeTitles)')
            ->andWhere('value.owner in (:userIds) and value.date >= :startDate')
            ->setParameter('userIds', $userIds)
            ->setParameter('valueTypeTitles', array_keys(ValueType::PROFIT_AND_REVENUE_CODE_NAMES))
            ->setParameter('startDate', (clone $courseStream->getStartDate())->modify('-13 day'));

        if (!$unlim) {
            $qb
                ->andWhere("value.date < DATE_ADD(:startDate, :period, 'WEEK')")
                ->setParameter('period', $courseStream->getPeriod() + 1);
        }
        $values = $qb->getQuery()->getResult();

        $firstWeek = (int)$courseStream->getStartDate()->format('W');
        foreach ($values as $value) {
            $week = (int)$value->getDate()->format('W') - $firstWeek + 1;
            $type = ValueType::PROFIT_AND_REVENUE_CODE_NAMES[$value->getType()->getTitle()];
            if ($week === 0) {
                $results[$value->getOwnerId()][$type . '_before'] = $value->getValue();
                continue;
            }
            if ($week > 0) {
                $results[$value->getOwnerId()][$type][$week] = $value->getValue();
            }
        }

        /** @var UserAnswer[] $beforeCourseValues */
        $beforeCourseValues = $this->em->getRepository(UserAnswer::class)
            ->createQueryBuilder('answer')
            ->join('answer.question', 'question')
            ->where('answer.courseStream = :courseStream and question.slug in (:slugs)')
            ->setParameter('courseStream', $courseStream)
            ->setParameter('slugs', [Question::MONTH_PROFIT, Question::MONTH_REVENUE])
            ->getQuery()
            ->getResult();

        foreach ($beforeCourseValues as $value) {
            $type = Question::MONTH_PROFIT === $value->getQuestion()->getSlug() ? 'profit' : 'revenue';
            if ($value->getText()) {
                $results[$value->getUser()->getId()][$type . '_before'] = $value->getText() / 4;
            }
        }

        return $results;
    }

    private function getBadGroups(array $ids): array
    {
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(UserGroupUser::class)->createQueryBuilder('ugu');
        $rows = $qb
            ->select('u.id', 'ug.name')
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ugu.user', 'u')
            ->where("ug.name IN(:names) OR ug.name like '%Новый МСА%'")
            ->andWhere('ugu.deleted = 0')
            ->andWhere('u.id IN(:userIds)')
            ->setParameter(
                'names',
                [
                    'Like Family',
                    'Обучение Капитанов Скорость (С16)',
                    'Капитаны СК17',
                    'Like Family (карта не действует)',
                    'Капитаны СК19',
                    'Марафон Ушаков',
                    'Капитаны С16 + факультеты',
                    'Старшие капитаны СК17',
                    'Франчайзи',
                    'Франчайзи Скорость / Сотка',
                ]
            )
            ->setParameter('userIds', $ids)
            ->groupBy('u.id', 'ug.name')
            ->getQuery()
            ->getResult();

        $users = [];
        foreach ($rows as $row) {
            $users[$row['id']] = $row['name'];
        }

        return $users;
    }

    private function getAlternativeGroups(array $userIds, string $courseName): array
    {
        // определяем альтернативные группы
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(UserGroup::class)->createQueryBuilder('ug');
        $rows = $qb
            ->select('aug.id')
            ->innerJoin('ug.courseStream', 'cs')
            ->innerJoin('ug.alternativeGroup', 'aug')
            ->andWhere('cs.name = :course')
            ->setParameter('course', $courseName)
            ->getQuery()
            ->getResult();
        $alternativeIds = array_unique(array_column($rows, 'id'));

        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(UserGroupUser::class)->createQueryBuilder('ugu');
        $rows = $qb
            ->select('u.id', 'ug.name', 'ug.id AS groupId', 'ugu.deleted AS isDeleted')
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ugu.user', 'u')
            ->andWhere('u.id IN(:userIds)')
            ->andWhere('ug.id IN(:groupIds)')
            ->setParameters(
                [
                    'userIds' => $userIds,
                    'groupIds' => $alternativeIds,
                ]
            )
            ->groupBy('u.id', 'ug.name', 'ug.id')
            ->orderBy('ug.id', 'ASC')
            ->getQuery()
            ->getResult();

        $deletedAlternativeGroups = $alternativeGroups = [];
        foreach ($rows as $row) {
            $name = $row['name'];
            $userId = $row['id'];

            if ($row['isDeleted'] === true) {
                $deletedAlternativeGroups[$userId] = $name;
            } else {
                $alternativeGroups[$userId] = $name;
            }
        }

        return [$deletedAlternativeGroups, $alternativeGroups];
    }

    private function getCsvHeader(bool $unlim = false): string
    {
        $csv = '';
        foreach (array_keys(get_object_vars(new TractionAnalyticsUserDocument())) as $prop) {
            if ($prop === 'revenue' || $prop === 'profit') {
                foreach (range(1, $unlim ? 30 : 6) as $week) {
                    $csv .= "{$prop}_{$week};";
                }
                continue;
            }
            $csv .= $prop . ';';
        }

        return mb_substr($csv, 0, -1);
    }

    private function countLessonViews(array $userIds, $folderId): array
    {
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(User::class)->createQueryBuilder('u');

        $rows = $qb
            ->select('u.id', 'COUNT(l.id) AS count')
            ->innerJoin('u.userLessons', 'ul')
            ->innerJoin('ul.lesson', 'l')
            ->innerJoin('l.course', 'c')
            ->innerJoin('c.folder', 'f')
            ->andWhere('u.id IN(:userIds)')
            ->andWhere('f.id = :folderId')
            ->setParameters(
                [
                    'userIds' => $userIds,
                    'folderId' => $folderId,
                ]
            )
            ->groupBy('u.id')
            ->getQuery()
            ->getResult();

        $users = [];
        foreach ($rows as $row) {
            $users[$row['id']] = $row['count'];
        }

        return $users;
    }

    /**
     * @param User $requestUser
     *
     * @return null|int
     */
    private function getRequestUserGroup(User $requestUser): ?int
    {
        foreach ($requestUser->getGroupRelations() as $group) {
            switch ($group->getUserGroup()->getId()) {
                case self::MAJOR_CAPTAINS_HUNDRED_GROUP:
                    return self::MAJOR_CAPTAINS_HUNDRED_GROUP;
                case self::MAJOR_CAPTAINS_SPEED_GROUP:
                    return self::MAJOR_CAPTAINS_SPEED_GROUP;
                case self::CAPTAINS_HUNDRED_GROUP:
                    return self::CAPTAINS_HUNDRED_GROUP;
                case self::CAPTAINS_SPEED_GROUP:
                    return self::CAPTAINS_SPEED_GROUP;
                case self::PARTNERS_GROUP:
                    return self::PARTNERS_GROUP;
            }
        }

        return null;
    }

}

<?php

namespace App\Controller;

use App\Entity\Event;
use App\Entity\User;
use App\Entity\UserGroupUser;
use App\Exceptions\AuthServerServiceException;
use App\Service\AuthServerHelper;
use App\Service\AuthServerService;
use Carbon\Carbon;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Google\Cloud\Core\Exception\BadRequestException;
use GuzzleHttp\Exception\GuzzleException;
use JsonException;
use Knp\Component\Pager\PaginatorInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Throwable;

/**
 * @Route("/api/community", name="community_")
 * @IsGranted("ROLE_USER")
 */
class CommunityController extends BaseController
{

    public function __construct(
        EntityManagerInterface $em,
        NormalizerInterface $normalizer,
        LoggerInterface $logger,
        AuthServerHelper $authServerHelper,
        AuthServerService $authServerService,
        PaginatorInterface $paginator
    ) {
        parent::__construct($em, $normalizer, $logger);

        $this->authServerHelper = $authServerHelper;
        $this->authServerService = $authServerService;
        $this->paginator = $paginator;
    }

    /**
     * Список доступных факультетов для пользователя
     * @Route("/list", name="list")
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws AuthServerServiceException
     * @throws BadRequestException
     * @throws JsonException
     */
    public function listForUserAction(Request $request): JsonResponse
    {
        $users = $this->getUsers($request);

        return $this->getResponseNew($users, ['club']);
    }

    /**
     * @todo метод отваливается по таймауту, нужно перенести на сторону auth
     *
     * Список доступных факультетов для пользователя
     * @Route("/list/filters", name="filters")
     *
     * @return JsonResponse
     */
    public function getFilters(): JsonResponse
    {
        $users = $this->getUsersForFilter();
        $filters = $this->getFiltersFromUsersArray($users);

        return $this->getResponseNew($filters);
    }

    /**
     * Список доступных факультетов для пользователя
     * @Route("/profile/{id}", name="user_profile")
     * @ParamConverter("user", options={"mapping": {"id": "id"}})
     *
     * @param  User  $user
     *
     * @return JsonResponse
     * @throws Throwable
     */
    public function userProfile(User $user): JsonResponse
    {
        $profile = $this->getUserProfile($user);
        $this->createEvent(Event::TYPE_VIEW_CLUB_PROFILE, [], $user);

        return $this->getResponseNew($profile);
    }

    private function getFiltersFromUsersArray(array $users): array
    {
        $cities = [];
        $business = [];

        foreach ($users as $user) {
            if (!empty($user['city'])) {
                $cities[] = [
                    'title' => $user['city'],
                    'value' => $user['city'],
                ];
            }
            if (!empty($user['businessCategoryId'])) {
                $business[] = [
                    'title' => $user['businessCategoryName'],
                    'value' => $user['businessCategoryId'],
                ];
            }
        }

        $cities = array_unique($cities, SORT_REGULAR);
        $business = array_unique($business, SORT_REGULAR);
        sort($cities);
        sort($business);

        return [
            'cities' => $cities,
            'business' => $business
        ];
    }

    private function getUsersForFilter(): array
    {
        // Тут забираем только группу Клуба
        $groupIds = [680];
        $ids = [];

        // берем всех учеников и их группы на потоке
        $rows = $this->em->getRepository(UserGroupUser::class)->createQueryBuilder('ugu')
            ->select('u.authUserId')
            ->innerJoin('ugu.user', 'u')
            ->andWhere('ugu.deleted = 0')
            ->andWhere('ugu.userGroup IN(:groupIds)')
            ->groupBy('u.authUserId')
            ->setParameter('groupIds', $groupIds)
            ->getQuery()
            ->getResult();

        foreach ($rows as $row) {
            $ids[] = $row['authUserId'];
        }

        return $this->authServerHelper->getUsers(
            $ids,
            [
                'id',
                'avatar',
                'name',
                'lastname',
                'city',
                'businessCategory',
                'niche',
                'dateOfBirth'
            ],
            '',
            null,
            null,
            [
                'avatar' => 'profile'
            ]
        );
    }

    /**
     * @throws GuzzleException
     * @throws Exception
     */
    private function getUserProfile(User $user): array
    {

        $user = (array) $this->authServerHelper->getUsersInfo([$user->getAuthUserId()])[0];
        $user['age'] = $user['dateOfBirth'] ?
            (new DateTime($user['dateOfBirth']))->diff(new DateTime())->format('%y') : '';

        return $user;
    }

    /**
     * @param Request $request
     *
     * @return array
     * @throws JsonException
     * @throws AuthServerServiceException
     * @throws BadRequestException
     */
    private function getUsers(Request $request): array
    {
        $this->authServerService->setUserAccessToken($request->headers->get('Authorization'));
        $filter = [];
        $page = (int)$request->get('page');
        $pageSize = (int)$request->get('len');
        $params = json_decode($request->get('params', '{}'), true, 512, JSON_THROW_ON_ERROR);

        if (!empty($params['city'])) {
            $filter['city'] = $params['city'];
        }
        if ($q = $request->get('name')) {
            $q = mb_strtolower($q);
            $filter['name'] = $q;
            $filter['lastname'] = $q;
            $filter['username'] = $q;
        }
        // Тут забираем только группу Клуба
        $groupIds = [680];

        $this->createEvent(Event::TYPE_CLUB_SEARCH, $filter);

        // берем всех учеников и их группы на потоке
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(UserGroupUser::class)->createQueryBuilder('ugu')
            ->select('u.authUserId')
            ->innerJoin('ugu.user', 'u')
            ->andWhere('ugu.deleted = 0')
            ->andWhere('ugu.userGroup IN(:groupIds)')
            ->setParameter('groupIds', $groupIds)
            ->groupBy('u.authUserId');

        if (!empty($params['profitFrom'])) {
            // Забираем только пользователей у которых в среднем по году сумма по прибыли в месяц выше указанной
            $qb->andHaving('(SUM(COALESCE(tsc.profit, 0) + COALESCE(thc.profit, 0))/12) >= :profitFrom')
                ->setParameter('profitFrom', $params['profitFrom']);
        }

        if (!empty($params['profitTo'])) {
            // Забираем только пользователей у которых в среднем по году сумма по прибыли в месяц ниже указанной
            $qb->andHaving('(SUM(COALESCE(tsc.profit, 0) + COALESCE(thc.profit, 0))/12) <= :profitTo')
                ->setParameter('profitTo', $params['profitTo']);
        }

        $businessCategories = $this->authServerHelper->getProfileProperties()['businessCategories'];

        // Фильтрация по сфере бизнеса
        if (!empty($params['business'])) {
            $filter['businessCategory'] = $params['business'];
        }

        $qb->setFirstResult(($page - 1) * $pageSize)->setMaxResults($pageSize);

        $users = $qb->getQuery()->getResult();

        if (!empty($params['ageFrom'])) {
            $date = Carbon::now();
            $ageFrom = $date->addYears(-$params['ageFrom'])->format('Y-m-d');
        }
        if (!empty($params['ageTo'])) {
            $date = Carbon::now();
            $ageTo = $date->addYears(-$params['ageTo'])->format('Y-m-d');
        }
        if (isset($ageFrom, $ageTo)) {
            if ($ageTo < $ageFrom) {
                $filter['birthdayFrom'] = $ageTo;
                $filter['birthdayTo'] = $ageFrom;
            } else {
                $filter['birthdayFrom'] = $ageFrom;
                $filter['birthdayTo'] = $ageTo;
            }
        } elseif (isset($ageFrom)) {
            $filter['birthdayFrom'] = $ageFrom;
        } elseif (isset($ageTo)) {
            $filter['birthdayTo'] = $ageTo;
        }

        if (empty($users)) {
            return [
                'users' => [],
                'businessCategories' => $businessCategories
            ];
        }

        $filter['ids'] = array_column($users, 'authUserId');

        $authUsers = $this->authServerService->filterUsers(
            $filter,
            ['lms', 'user:preview', 'user:business', 'user:profit', 'user:social'],
            1,
            $pageSize
        );

        return [
            'users' => $authUsers['data']['users'],
            'businessCategories' => $businessCategories
        ];
    }

    protected function createEvent(int $type, $data = [], ?User $object = null): void
    {
        $currentUser = $this->getUser();
        $event = new Event($currentUser, $type, $object, $data);
        $this->em->persist($event);
        $this->em->flush();
    }
}

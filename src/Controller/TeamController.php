<?php

namespace App\Controller;

use App\Entity\AbstractCourse;
use App\Entity\Courses\Lesson;
use App\Entity\CourseStream;
use App\Entity\Team;
use App\Entity\TeamFolder;
use App\Entity\Traction\Value;
use App\Entity\Traction\ValueType;
use App\Entity\User;
use App\Entity\UserGroup;
use App\Entity\UserGroupUser;
use App\Entity\UserLesson;
use App\Repository\FacultyRepository;
use App\Repository\TeamFolderRepository;
use App\Repository\UserGroupUserRepository;
use App\Repository\Questionnaire\QuestionRepository;
use App\Repository\TeamRepository;
use App\Service\AuthServerHelper;
use App\Service\AuthServerService;
use App\Service\FormHelper;
use App\Service\LikeItHelper;
use App\Service\UserHelper;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use OpenApi\Annotations as OA;

/**
 *
 * @Route("/api")
 *
 * @IsGranted("ROLE_USER")
 */
class TeamController extends BaseController
{
    private $likeItHelper;
    private $authServerHelper;
    private $formHelper;

    public function __construct(
        EntityManagerInterface $em,
        NormalizerInterface $normalizer,
        LikeItHelper $likeItHelper,
        AuthServerHelper $authServerHelper,
        FormHelper $formHelper
    ) {
        parent::__construct($em, $normalizer);
        $this->likeItHelper = $likeItHelper;
        $this->authServerHelper = $authServerHelper;
        $this->formHelper = $formHelper;
    }

    /**
     * Получение настроек
     *
     * @Rest\Get ("/v2/teams/settings", name="team_v2_settings")
     *
     * @OA\Tag(name="Команды")
     * @OA\Parameter(name="streamId", in="query",
     *     @OA\Schema(type="integer"), required=false,
     *     description="ID потока")
     *
     * @Rest\QueryParam(name="streamId", requirements="\d+", description="ID потока")
     *
     * @param Request $request
     * @param UserGroupUserRepository $userGroupUserRepository
     * @param FacultyRepository $facultyRepository
     * @param TeamFolderRepository $teamFolderRepository
     *
     * @return JsonResponse
     */
    public function settingsV2Action(
        ParamFetcher $paramFetcher,
        UserGroupUserRepository $userGroupUserRepository,
        FacultyRepository $facultyRepository,
        TeamFolderRepository $teamFolderRepository
    ): JsonResponse {
        $cities = [];
        $captains = [];
        $seniorCaptains = [];
        $groupFolders = [];
        $streams = [];
        $faculties = [];

        if ($this->isGranted('USER_TEAM_EDIT')) {
            $cities = $this->likeItHelper->getCities();
        }

        if ($this->isGranted("TEAMS_ACCESS")) {
            $captains = $this->authServerHelper->getUsers(
                $userGroupUserRepository->getAuthUserIdsByGroupId(UserGroup::CAPTAINTS_GROUP_ID),
                ['id', 'name', 'lastname', 'avatar']
            );
            $seniorCaptains = $this->authServerHelper->getUsers(
                $userGroupUserRepository->getAuthUserIdsByGroupId(UserGroup::SENIOR_CAPTAINTS_GROUP_ID),
                ['id', 'name', 'lastname', 'avatar']
            );

            $streamId = $paramFetcher->get('streamId');
            if ($streamId !== '') {
                $faculties = $facultyRepository->getFacultyNamesOfCourseStream((int)$streamId);
            }

            foreach ($teamFolderRepository->findAll() as $folder) {
                foreach ($folder->getUserGroups() as $userGroup) {
                    $groupFolders[$userGroup->getId()] = $folder->getName();
                }
            }

            $courseStreams = $this->em->getRepository(CourseStream::class)
                ->getSpeedAndHundredStreams();

            foreach ($courseStreams as $courseStream) {
                $streams[] = [
                    'id' => $courseStream->getId(),
                    'name' => $courseStream->getName(),
                ];
            }
        }

        return $this->getResponse(
            'settingsAction',
            [
                'canEdit' => $this->isGranted('TEAMS_ACCESS'),
                'captains' => $captains,
                'seniorCaptains' => $seniorCaptains,
                'cities' => $cities,
                'groupFolders' => $groupFolders,
                'streams' => $streams,
                'faculties' => $faculties ?? [],
            ]
        );
    }

    /**
     * Получение списка команд
     *
     * @Route("/v2/teams/list", name="team_v2_list", methods={"GET"})
     *
     * @OA\Tag(name="Команды")
     *
     * @OA\Parameter(name="search", in="query",
     *      @OA\Schema(type="string"), required=false,
     *      description="Поиск участника")
     *
     * @OA\Parameter(name="faculty", in="query",
     *     @OA\Schema(type="string"), required=false,
     *     description="Название Факультета/Сборной")
     *
     * @OA\Parameter(name="captain", in="query",
     *     @OA\Schema(type="integer"), required=false,
     *     description="ID капитана команды")
     *
     * @OA\Parameter(name="courseStreamId", in="query",
     *     @OA\Schema(type="integer"), required=false,
     *     description="ID потока")
     *
     * @OA\Parameter(name="city", in="query",
     *     @OA\Schema(type="string"), required=false,
     *     description="Город участников команды")
     *
     * @OA\Parameter(name="week", in="query",
     *     @OA\Schema(type="integer"), required=false,
     *     description="ID недели")
     *
     * @OA\Parameter(name="pageSize", in="query",
     *     @OA\Schema(type="integer"), required=false,
     *     description="Количество записей на странице")
     *
     * @OA\Parameter(name="page", in="query",
     *     @OA\Schema(type="integer"), required=false,
     *     description="Номер страницы")
     *
     * @OA\Response(
     *     response=200,
     *     description="Члены команд",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *         @OA\Schema(
     *             type="object",
     *             @OA\Property(
     *                 property="status",
     *                 type="string"
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 type="array",
     *                 @OA\Items(
     *                     type="object",
     *                     @OA\Property(type="string", property="field"),
     *                     @OA\Property(type="string", property="message"),
     *                 ),
     *             ),
     *             @OA\Property(
     *                 property="data",
     *                 type="array",
     *                 @OA\Items(
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(type="integer", property="folderId"),
     *                         @OA\Property(type="string", property="folderName"),
     *                         @OA\Property(type="integer", property="id"),
     *                         @OA\Property(type="string", property="name"),
     *                         @OA\Property(type="integer", property="captain"),
     *                         @OA\Property(type="string", property="count"),
     *                     )
     *                 ),
     *             ),
     *         ),
     *     ),
     * )
     *
     * @OA\Response(
     *      response=400,
     *      description="Ошибка авторизации",
     * )
     *
     * @OA\Response(
     *      response=403,
     *      description="Недостаточно прав",
     * )
     * @Rest\QueryParam(name="search", description="Поиск участника")
     * @Rest\QueryParam(name="faculty", description="Название Факультета/Сборной")
     * @Rest\QueryParam(name="captain", description="ID капитана команды")
     * @Rest\QueryParam(name="courseStreamId", description="ID потока")
     * @Rest\QueryParam(name="city", description="Город участников команды")
     * @Rest\QueryParam(name="week", description="ID недели")
     * @Rest\QueryParam(name="pageSize", requirements="\d+", default=10, description="Количество записей на странице")
     * @Rest\QueryParam(name="page", requirements="\d+", default=1, description="Номер страницы")
     *
     * @param ParamFetcher $paramFetcher
     * @param TeamRepository $teamRepository
     * @param UserHelper $userHelper
     * @param AuthServerService $authServerService
     *
     * @return JsonResponse
     */
    public function getTeamsListV2(
        Request $request,
        ParamFetcher $paramFetcher,
        TeamRepository $teamRepository,
        UserHelper $userHelper,
        AuthServerService $authServerService
    ): JsonResponse {
        /** @var User $user */
        $user = $this->getUser();

        $authServerService->setUserAccessToken(
            $request->headers->get('Authorization')
                ?: 'Bearer ' . $request->cookies->get('accessToken')
        );
        // Флаг для поиска по текущему пользователю
        // используется если параметры поиска не заданны
        $findByUser = $this->needFindByUser($user) && !$userHelper->isUserAdmin();
        $cityName = $paramFetcher->get('city');
        $cities = [];
        if ($this->isGranted('USER_TEAM_EDIT') && $cityName) {
            $cities = [$cityName];
        } elseif ($findByUser) {
            $cities = $this->likeItHelper->getFranchiseeCities($user);
        }
        $queryBuilder = $teamRepository->createQueryBuilder('t');
        $queryBuilder
            ->select(
                'f.id as folderId',
                'f.name as folderName',
                't.id AS id',
                't.name AS name',
                'c.id AS captain',
                'COUNT(DISTINCT u_count.id) as count',
            )
            ->leftJoin('t.captain', 'c')
            ->join('t.folder', 'f')
            ->join('t.users', 'u_count')
            ->join('t.users', 'u_search')
            ->groupBy('t.id');
        $cities_usersIds = [];
        $search_usersIds = [];
        if (!empty($cities)) {
            foreach ($cities as $city) {
                $users = $authServerService->filterUsers(
                    [
                        '_resultLimit' => 0,
                        'city' => $city,
                    ],
                    ['lms'],
                    1,
                    -1,
                );
                if ($users['data']['count']) {
                    $cities_usersIds[] = array_column($users['data']['users'], 'id');
                }
            }
            $flattenIds = [];

            array_walk_recursive($cities_usersIds, function ($a) use (&$flattenIds) {
                $flattenIds[] = $a;
            });

            $cities_usersIds = $flattenIds;
        }
        $search = $paramFetcher->get('search');
        if (!empty($search)) {
            $users = $authServerService->filterUsers(
                [
                    '_resultLimit' => 0,
                    'name' => $search,
                    'lastname' => $search,
                    'username' => $search,
                    'email' => $search,
                ],
                ['lms'],
                1,
                -1,
            );
            if ($users['data']['count']) {
                $search_usersIds = array_column($users['data']['users'], 'id');
            }
        }
        if (!empty($cities_usersIds) || !empty($search_usersIds)) {
            if (!empty($cities_usersIds) && empty($search_usersIds)) {
                $usersIds = $cities_usersIds;
            } elseif (empty($cities_usersIds) && !empty($search_usersIds)) {
                $usersIds = $search_usersIds;
            } elseif (!empty($cities_usersIds) && !empty($search_usersIds)) {
                $usersIds = array_intersect($cities_usersIds, $search_usersIds);
            }
            $findByUser = false;
            $queryBuilder->andWhere('u_search.id IN (:usersIds)')
                ->setParameter('usersIds', $usersIds);
        }
        $courseStreamId = $paramFetcher->get('courseStreamId');
        if ($courseStreamId) {
            $findByUser = false;
            $queryBuilder
                ->join('f.courseStream', 'cs')
                ->andWhere('cs.id = :courseStreamId')
                ->setParameter('courseStreamId', $courseStreamId);
        }
        $captainId = $paramFetcher->get('captain');
        if ($captainId) {
            $findByUser = false;
            $queryBuilder->andWhere('c.id = :captainId')
                ->setParameter('captainId', $captainId);
        }
        $facultyName = $paramFetcher->get('faculty');
        if ($facultyName) {
            $findByUser = false;
            $queryBuilder
                ->join('u_search.groupRelations', 'ugu')
                ->join('ugu.userGroup', 'ug')
                ->join('ug.faculties', 'fac')
                ->andWhere('fac.name = :facultyName AND ugu.deleted = 0')
                ->setParameter('facultyName', $facultyName);
        }
        if ($findByUser) {
            $queryBuilder->andWhere('u_search = :user OR t.captain = :user OR t.seniorCaptain = :user')
                ->setParameter('user', $user);
        }
        $pageSize = (int)$paramFetcher->get('pageSize');
        $page = (int)$paramFetcher->get('page');
        $teams = $queryBuilder
            ->setMaxResults($pageSize)
            ->setFirstResult($pageSize * ($page - 1))
            ->getQuery()
            ->getResult();

        return $this->getResponseNew($teams);
    }

    /**
     * @Rest\Post ("/teams/user/{id<\d+>}/verification", name="team_verification")
     *
     * @Rest\View(serializerGroups={"verification"})
     *
     * @OA\Tag(name="Верификация пользователя")
     * @OA\Parameter(name="id", in="path", required=true, @OA\Schema(type="int"), description="ID пользователя")
     *
     * @OA\Response(
     *     response=200,
     *     description="Статус верификации пользователя",
     *     @OA\JsonContent(@OA\Property(property="verified", type="boolean"))
     * )
     *
     * @IsGranted("USER_TEAM_EDIT")
     */
    public function toggleUserVerification(User $user): array
    {
        $user->setVerified(!$user->isVerified());
        $this->em->flush();

        return ['verified' => $user->isVerified()];
    }

    /**
     * Получение настроек
     *
     * @Route("/teams/settings", name="team_settings")
     *
     * @param UserGroupUserRepository $userGroupUserRepository
     * @param FacultyRepository $facultyRepository
     * @param TeamFolderRepository $teamFolderRepository
     *
     * @return JsonResponse
     */
    public function settingsAction(
        UserGroupUserRepository $userGroupUserRepository,
        FacultyRepository $facultyRepository,
        TeamFolderRepository $teamFolderRepository
    ): JsonResponse {
        $cities = [];
        $captains = [];
        $seniorCaptains = [];
        $groupFolders = [];
        $folders = [];

        if ($this->isGranted('USER_TEAM_EDIT')) {
            $cities = $this->likeItHelper->getCities();
        }

        if ($this->isGranted("TEAMS_ACCESS")) {
            $captains = $this->authServerHelper->getUsers(
                $userGroupUserRepository->getAuthUserIdsByGroupId(UserGroup::CAPTAINTS_GROUP_ID),
                ['id', 'name', 'lastname', 'avatar']
            );
            $seniorCaptains = $this->authServerHelper->getUsers(
                $userGroupUserRepository->getAuthUserIdsByGroupId(UserGroup::SENIOR_CAPTAINTS_GROUP_ID),
                ['id', 'name', 'lastname', 'avatar']
            );
            $faculties = $facultyRepository->getUniqueFacultyNames();

            foreach ($teamFolderRepository->findAll() as $folder) {
                foreach ($folder->getUserGroups() as $userGroup) {
                    $groupFolders[$userGroup->getId()] = $folder->getName();
                }
                $folders[] = [
                    'id' => $folder->getId(),
                    'name' => $folder->getName(),
                ];
            }
        }

        return $this->getResponse(
            'settingsAction',
            [
                'canEdit' => $this->isGranted('TEAMS_ACCESS'),
                'captains' => $captains,
                'seniorCaptains' => $seniorCaptains,
                'cities' => $cities,
                'groupFolders' => $groupFolders,
                'folders' => $folders,
                'faculties' => $faculties ?? [],
            ]
        );
    }

    /**
     * Получение списка команд
     *
     * @Route("/teams/list", name="team_list", methods={"GET"})
     *
     * @param Request $request
     * @param TeamRepository $teamRepository
     * @param UserHelper $userHelper
     *
     * @return JsonResponse
     */
    public function getTeamsList(Request $request, TeamRepository $teamRepository, UserHelper $userHelper): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();

        if ($this->isGranted('USER_TEAM_EDIT') && $city = $request->get('city')) {
            $filterCities = [$city];
        } else {
            $filterCities = $this->likeItHelper->getFranchiseeCities($user) ?: null;
        }

        $search = $request->get('search') ?: null;
        $faculty = $request->get('faculty') ?: null;
        $captainId = $request->get('captain') ?: null;
        $folderIds = $request->get('folders') ?: null;
        $pageSize = (int)$request->get('pageSize') ?: null;
        $page = (int)$request->get('page') ?: null;
        $findByUser = $this->needFindByUser($user) && !$userHelper->isUserAdmin();

        $groupsData = $teamRepository->getGroupsData(
            $this->authServerHelper,
            $faculty,
            $captainId,
            $folderIds,
            $filterCities,
            $search,
            $user,
            $findByUser,
            $pageSize,
            $page,
        );

        // собираем ответ
        $tractionTeams = [];
        $data = [];
        foreach ($groupsData as $group) {
            $tractionClass = (str_starts_with($group['folderName'], 'Скорость'))
                ? TractionSpeed::class
                : TractionHundred::class;
            $tractionTeams[$tractionClass][] = $group['teamId'];
            $folderId = $group['folderId'];

            $team = [
                'id' => $group['teamId'],
                'name' => $group['teamName'],
                'count' => $group['count'],
                'captain' => $group['captainId'],
                'revenue' => 0,
                'profit' => 0,
            ];

            $dataKey = array_search($folderId, array_column($data, 'id'), true);

            if (is_numeric($dataKey)) {
                $data[$dataKey]['teams'][] = $team;
            } else {
                $data[] = [
                    'id' => $folderId,
                    'name' => $group['folderName'],
                    'teams' => [$team],
                ];
            }
        }

        // считаем показатели для трекшена
        [$tractionSummary, $teamsSummary] = $this->getTractionByTeams($tractionTeams, $request->get('week'));
        if ($teamsSummary) {
            foreach ($data as &$folder) {
                foreach ($folder['teams'] as &$team) {
                    $teamId = $team['id'];
                    if (isset($teamsSummary['revenue'][$teamId])) {
                        $team['revenue'] = $teamsSummary['revenue'][$teamId];
                    }
                    if (isset($teamsSummary['profit'][$team['id']])) {
                        $team['profit'] = $teamsSummary['profit'][$teamId];
                    }
                }
                unset($team);

                // сортируем по выручке
                $profit = array_column($folder['teams'], 'profit');
                array_multisort($profit, SORT_DESC, $folder['teams']);
            }
            unset($folder);
        }

        return $this->getResponseNew(
            [
                'folders' => $data,
                'tractionSummary' => $tractionSummary,
            ]
        );
    }

    /**
     * Получение списка команд по группе пользователя
     *
     * @Route("/teams/find-by-group/{id}", name="team_find_by_group")
     * @ParamConverter("userGroup", options={"mapping": {"id": "id"}})
     *
     * @param UserGroup $userGroup
     * @param TeamRepository $teamRepository
     *
     * @return JsonResponse
     * @IsGranted("TEAMS_ACCESS")
     *
     */
    public function teamsByGroupAction(UserGroup $userGroup, TeamRepository $teamRepository): JsonResponse
    {
        $user = $this->getUser();

        // фильтруем по пользователям
        $userIds = [];
        $franchiseeCities = $this->likeItHelper->getFranchiseeCities($user);
        if (!empty($franchiseeCities)) {
            $userIds = $teamRepository->getFilteredUserIds($this->authServerHelper, $franchiseeCities);
        }

        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(Team::class)->createQueryBuilder('t');
        $qb
            ->select('t.id AS id', 't.name AS team', 'COUNT(DISTINCT tu.id) AS count', 'c.id AS captain')
            ->innerJoin('t.folder', 'f')
            ->innerJoin('f.userGroups', 'ug')
            ->innerJoin('ug.courseStream', 'cs')
            ->innerJoin('cs.abstractCourse', 'ac')
            ->leftJoin('t.users', 'tu')
            ->leftJoin('t.captain', 'c')
            ->andWhere('ug = :userGroup')
            ->andWhere('cs.active = 1')
            ->andWhere('ac.slug IN(:slugs)')
            ->setParameters(
                [
                    'userGroup' => $userGroup,
                    'slugs' => [
                        AbstractCourse::SLUG_HUNDRED,
                        AbstractCourse::SLUG_SPEED,
                        AbstractCourse::SLUG_SPEED_CLUB,
                    ],
                ]
            )
            ->groupBy('t.id');

        if (!empty($userIds)) {
            // команды с юзерами франча
            $qb
                ->andWhere('tu.id IN(:userIds) OR t.createdBy = :user')
                ->setParameter('userIds', $userIds)
                ->setParameter('user', $user);
        } elseif ($this->needFindByUser($user)) {
            // сотрудники лц
            // @todo: сделать нормально
            $qb
                ->andWhere('t.captain = :user OR t.seniorCaptain = :user OR t.createdBy = :user')
                ->setParameter('user', $user);
        }

        return $this->getResponse(
            'teamsByGroup',
            $qb->getQuery()->getResult()
        );
    }

    /**
     * Получение списка команд по папке команд
     *
     * @Route("/teams/find-by-folder/{id}", name="team_find_by_folder")
     * @ParamConverter("teamFolder", options={"mapping": {"id": "id"}})
     *
     * @param TeamFolder $teamFolder
     * @param TeamRepository $teamRepository
     *
     * @return JsonResponse
     * @IsGranted("TEAMS_ACCESS")
     *
     */
    public function teamsByFolderAction(TeamFolder $teamFolder, TeamRepository $teamRepository): JsonResponse
    {
        $user = $this->getUser();

        // фильтруем по пользователям
        $userIds = [];
        $franchiseeCities = $this->likeItHelper->getFranchiseeCities($user);
        if (!empty($franchiseeCities)) {
            $userIds = $teamRepository->getFilteredUserIds($this->authServerHelper, $franchiseeCities);
        }

        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(TeamFolder::class)->createQueryBuilder('f');
        $qb
            ->select('t.id AS id', 't.name AS team', 'COUNT(DISTINCT tu.id) AS count', 'c.id AS captain')
            ->innerJoin('f.teams', 't')
            ->innerJoin('f.userGroups', 'ug')
            ->leftJoin('t.users', 'tu')
            ->leftJoin('t.captain', 'c')
            ->andWhere('f = :teamFolder')
            ->setParameter('teamFolder', $teamFolder)
            ->groupBy('t.id');

        if (!empty($userIds)) {
            // команды с юзерами франча
            $qb
                ->andWhere('tu.id IN(:userIds) OR t.createdBy = :user')
                ->setParameter('userIds', $userIds)
                ->setParameter('user', $user);
        } elseif ($this->needFindByUser($user)) {
            // сотрудники лц
            // @todo: сделать нормально
            $qb
                ->andWhere('t.captain = :user OR t.seniorCaptain = :user OR t.createdBy = :user')
                ->setParameter('user', $user);
        }

        return $this->getResponse(
            'teamsByFolderAction',
            $qb->getQuery()->getResult()
        );
    }

    /**
     * Получение списка доступных групп для создания в них команд
     *
     * @Route("/teams/available-folders", name="team_available_groups")
     * @ParamConverter("userGroup", options={"mapping": {"id": "id"}})
     * @IsGranted("TEAMS_ACCESS")
     *
     * @return JsonResponse
     */
    public function availableGroupsAction(): JsonResponse
    {
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(TeamFolder::class)->createQueryBuilder('f');
        $data = $qb
            ->select('f.id AS id', 'f.name AS name')
            ->getQuery()
            ->getResult();

        return $this->getResponse('availableGroupsAction', $data);
    }

    /**
     * Получение списка нераспределенных пользователей
     * @Route("/teams/unallocated", name="team_unallocated")
     *
     * @param Request $request
     * @param TeamRepository $teamRepository
     * @param UserGroupUserRepository $userGroupUserRepository
     * @param QuestionRepository $questionRepository
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     * @IsGranted("TEAMS_ACCESS")
     */
    public function getUnallocated(
        Request $request,
        TeamRepository $teamRepository,
        UserGroupUserRepository $userGroupUserRepository,
        QuestionRepository $questionRepository
    ): JsonResponse {
        $user = $this->getUser();

        $franchiseeCities = $this->likeItHelper->getFranchiseeCities($user);

        $search = $request->get('search');
        $filterCities = null;
        if (!empty($franchiseeCities)) {
            // берем города франчайзи, по которым будем фильтроваться
            $filterCities = $franchiseeCities;
        }

        if ($city = $request->get('city')) {
            $filterCities = [$city];
        }

        // берем юзеров в командах актуальных курсов
        $userInTeamIds = $teamRepository->getUserInActualCoursesIds();
        // оставляем тех, кто не в командах, но в группах
        $userInGroupButNotTeamInfo = $userGroupUserRepository->getUserInGroupButNotTeamInfo(
            $userInTeamIds,
            $request->get('courseStreamId'),
            $request->get('faculty')
        );
        $authIds = [];
        $userGroups = [];
        $userFaculties = [];
        $isVerifiedFields = [];

        foreach ($userInGroupButNotTeamInfo as $user) {
            /**
             * Пользователь может быть в платных и условно бесплатных группах
             * Поэтому не заменяем платные группы
             */
            $groupName = $user['groupName'];
            if (isset($userGroups[$user['userId']]) && str_contains($groupName, '(Повторники')) {
                continue;
            }

            $userGroups[$user['userId']] = $user['groupId'];
            $userFaculties[$user['userId']] = $user['faculty'];
            $authIds[] = $user['authUserId'];
            $isVerifiedFields[$user['userId']] = $user['verified'];
        }

        $users = $this->authServerHelper->getUsers(
            array_unique($authIds),
            ['id', 'name', 'lastname', 'city', 'avatar', 'email', 'telegram'],
            $teamRepository->prepareSearch($filterCities, $search),
            $request->get('page') ?: null,
            $request->get('pageSize') ?: null
        );

        array_walk(
            $users,
            static function (&$user) use ($userGroups, $userFaculties, $isVerifiedFields) {
                if (empty($user['id']) || !isset($userGroups[$user['id']])) {
                    return;
                }

                $telegramUsernames = explode("|", $user['telegram']);
                unset($user['telegram']);
                $user['username'] = end($telegramUsernames);
                $user['group'] = $userGroups[$user['id']];
                $user['faculty'] = $userFaculties[$user['id']];
                $user['verified'] = $isVerifiedFields[$user['id']];
            }
        );

        return $this->getResponseNew($users);
    }

    /**
     * Получение списка учеников в моем городе
     *
     * @Route("/teams/people-nearby", name="team_people_nearby")
     *
     * @return JsonResponse
     * @throws NonUniqueResultException
     */
    public function peopleNearbyAction(): JsonResponse
    {
        $user = $this->getUser();

        if (empty($user->getCity())) {
            return $this
                ->addError('city', 'Пустой город в профиле')
                ->getResponse('peopleNearbyAction');
        }

        // определяем актуальный поток пользователя
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(CourseStream::class)->createQueryBuilder('cs');
        /** @var null|CourseStream $courseStream */
        $courseStream = $qb
            ->innerJoin('cs.userGroups', 'ug')
            ->innerJoin('ug.userRelations', 'ugu')
            ->innerJoin('cs.abstractCourse', 'ac')
            ->where('ugu.user = :user')
            ->andWhere('ugu.deleted = 0')
            ->andWhere('cs.active = 1')
            ->andWhere('ac.slug IN(:slugs)')
            ->setParameters(
                [
                    'user' => $user,
                    'slugs' => [
                        AbstractCourse::SLUG_HUNDRED,
                        AbstractCourse::SLUG_SPEED,
                        AbstractCourse::SLUG_SPEED_CLUB,
                    ],
                ]
            )
            ->orderBy('cs.stream', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        // берем всех учеников актуальных потоков
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(UserGroupUser::class)->createQueryBuilder('ugu');
        $ids = $qb
            ->select('u.authUserId')
            ->innerJoin('ugu.user', 'u')
            ->innerJoin('ugu.userGroup', 'ug')
            ->innerJoin('ug.courseStream', 'cs')
            ->andWhere('ugu.deleted = 0')
            ->andWhere('cs = :stream')
            ->setParameter('stream', $courseStream)
            ->groupBy('u.authUserId')
            ->getQuery()
            ->getResult();

        if (empty($ids)) {
            return $this->getResponse('peopleNearbyAction');
        }

        $authIds = array_map(
            static function ($e) {
                return $e['authUserId'];
            },
            $ids
        );

        // забираем людей из моего города с аутха
        $users = $this->authServerHelper->getUsers(
            $authIds,
            ['id', 'name', 'lastname', 'avatar', 'city', 'telegram'],
            "ui.city = '{$user->getCity()}'"
        );

        array_walk(
            $users,
            static function (&$user) {
                $telegramUsernames = explode("|", $user['telegram']);
                unset($user['telegram']);
                $user['username'] = end($telegramUsernames);
            }
        );

        return $this->getResponse('peopleNearbyAction', $users);
    }

    /**
     * Установка пользователя в другую команду
     *
     * @Route("/teams/change", name="team_change", methods={"POST"})
     * @IsGranted("TEAMS_ACCESS")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function changeAction(Request $request): JsonResponse
    {
        /** @var User $user */
        $user = $this->em->getRepository(User::class)->find($request->get('user'));
        /** @var Team $team */
        $team = $this->em->getRepository(Team::class)->find($request->get('team'));

        foreach ($user->getTeams() as $oldTeam) {
            // удаляем из старых команд, если они в той же самой папке (нельзя быть в двух командах)
            if ($oldTeam->getFolder() === $team->getFolder()) {
                $oldTeam->removeUser($user);
                if ($oldTeam->getLeader() === $user) {
                    $oldTeam->setLeader(null);
                }
                $this->em->persist($oldTeam);
            }
        }

        $team->addUser($user);
        $this->em->persist($team);
        $this->em->flush();

        return $this->getResponse(
            'changeAction',
            $this->getTeam($team)
        );
    }

    /**
     * Удаление пользователя из команды
     *
     * @Route("/teams/remove", name="team_remove", methods={"POST"})
     * @IsGranted("TEAMS_ACCESS")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function removeAction(Request $request): JsonResponse
    {
        /** @var User $user */
        $user = $this->em->getRepository(User::class)->find($request->get('user'));
        /** @var Team $team */
        $team = $this->em->getRepository(Team::class)->find($request->get('team'));

        if ($team->getLeader() === $user) {
            $team->setLeader(null);
        }

        $team->removeUser($user);
        $this->em->persist($team);
        $this->em->flush();

        return $this->getResponse(
            'removeAction',
            $this->getTeam($team)
        );
    }

    /**
     * Создание команды
     *
     * @Route("/teams/create", name="team_create", methods={"POST"})
     * @IsGranted("TEAMS_ACCESS")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function createAction(Request $request): JsonResponse
    {
        $user = $this->getUser();

        $team = new Team();
        $team->setCreatedBy($user);
        $team = $this->processTeam($request, $team);
        $this->em->flush();

        return $this->getResponse(
            'createAction',
            $this->getTeam($team)
        );
    }

    /**
     * Получение трекшена по команде
     *
     * todo: написать заново, если работает криво - даже не пытаться чинить, проще собрать требования и переделать
     *
     * @Route("/teams/traction/{id}", name="team_traction", methods={"GET"})
     * @ParamConverter("team", options={"mapping": {"id": "id"}})
     *
     * @param Team $team
     * @param EntityManagerInterface $em
     *
     * @return JsonResponse
     */
    public function tractionAction(Team $team, EntityManagerInterface $em): JsonResponse
    {
        $data = [
            'id' => $team->getId(),
            'leader' => $team->getLeader() ? $team->getLeader()->getId() : null,
            'users' => [],
            'tractionType' => 'hundred',
        ];

        $usersData = $em->getRepository(Team::class)->createQueryBuilder('t')
            ->select('u.id, u.authUserId, f.name')
            ->innerJoin('t.users', 'u')
            ->innerJoin('u.groupRelations', 'ugu')
            ->innerJoin('ugu.faculty', 'f')
            ->andWhere('ugu.deleted = 0')
            ->andWhere('t.id = :team')
            ->setParameter('team', $team)
            ->getQuery()
            ->getResult();

        // собираем айди юзеров
        $authIds = $userIds = $faculties = [];
        foreach ($usersData as $userData) {
            $faculties[$userData['id']] = $userData['name'];

            $authIds[] = $userData['authUserId'];
            $userIds[] = $userData['id'];
        }

        if ($authIds) {
            $users = $this->authServerHelper->getUsers($authIds, ['id', 'name', 'lastname', 'avatar', 'niche', 'city']);

            // определяем тип трекшена: Скорость или Сотка
            if (str_starts_with($team->getFolder()->getName(), 'Скорость')) {
                $data['tractionType'] = 'speed';
            }

            // получаем стату
            $traction = $this->getTractionByUsers($userIds);

            // собираем юзеров
            foreach ($users as &$user) {
                $userId = $user['id'];

                $user['revenue'] = $traction[$userId]['revenue'] ?? null;
                $user['profit'] = $traction[$userId]['profit'] ?? null;
                $user['faculty'] = $faculties[$userId];
            }
            $data['users'] = $users;
        }

        return $this->getResponseNew($data);
    }

    /**
     * Получение команды
     * @Route("/teams/{id}", name="team_team", methods={"GET"})
     * @ParamConverter("team", options={"mapping": {"id": "id"}})
     *
     * @param Team $team
     *
     * @return JsonResponse
     */
    public function teamAction(Team $team): JsonResponse
    {
        return $this->getResponse(
            'teamAction',
            $this->getTeam(
                $team,
                !$this->isGranted('TEAMS_ACCESS')
            )
        );
    }

    /**
     * Изменение команды
     * @Route("/teams/{id}/edit", name="team_edit", methods={"POST"})
     * @ParamConverter("team", options={"mapping": {"id": "id"}})
     * @IsGranted("TEAMS_ACCESS")
     *
     * @param Team $team
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function editAction(Team $team, Request $request): JsonResponse
    {
        $team = $this->processTeam($request, $team);
        $this->em->flush();

        return $this->getResponse(
            'editAction',
            $this->getTeam($team)
        );
    }

    /**
     * Удаление команды
     * @Route("/teams/{id}/delete", name="team_delete", methods={"POST"})
     * @ParamConverter("team", options={"mapping": {"id": "id"}})
     * @IsGranted("TEAMS_ACCESS")
     *
     * @param Team $team
     *
     * @return JsonResponse
     */
    public function deleteAction(Team $team): JsonResponse
    {
        if ($team->getUsers()->isEmpty()) {
            $this->em->remove($team);
            $this->em->flush();
        } else {
            $this->addError(
                'team',
                'Нельзя удалить команду, в которой есть участники. Перемести участников и попробуй снова'
            );
        }

        return $this->getResponse('deleteAction');
    }

    private function getTeam(Team $team, bool $disableStat = false): array
    {
        $data = [
            'id' => $team->getId(),
            'folder' => $team->getFolder()->getId(),
            'leader' => $team->getLeader() ? $team->getLeader()->getId() : null,
            'seniorCaptain' => $team->getSeniorCaptain() ? $team->getSeniorCaptain()->getId() : null,
            'captain' => null,
            'users' => [],
        ];

        // собираем айди юзеров
        $authIds = $userIds = [];
        foreach ($team->getUsers() as $user) {
            $authIds[] = $user->getAuthUserId();
            $userIds[] = $user->getId();
        }
        if ($captain = $team->getCaptain()) {
            $authIds[] = $captain->getAuthUserId();
        }
        $authIds = array_unique($authIds);
        $users = $this->authServerHelper->getUsers(
            $authIds,
            ['id', 'name', 'lastname', 'city', 'avatar', 'email', 'telegram']
        );

        $lessonIds = $lessonsCompleted = $lessonsPassed = $faculties = [];

        if (!$disableStat) {
            // доступы на группе, а все тиммейты и так в одной группе
            // @todo: переделать, потому что тут уже не одна группа
            /** @var UserGroup $userGroup */
            $userGroup = $team->getFolder()->getUserGroups()->first();
            $lessonIds = $userGroup->getLessons()->map(
                function (Lesson $lesson) {
                    return $lesson->getId();
                }
            );

            // считаем просмотренные и пройденные уроки
            /** @var QueryBuilder $qb */
            $qb = $this->em->getRepository(UserLesson::class)->createQueryBuilder('ul');
            $rows = $qb
                ->select('u.id AS userId', 'ul.state AS state', 'COUNT(DISTINCT l.id) AS count')
                ->innerJoin('ul.user', 'u')
                ->innerJoin('ul.lesson', 'l')
                ->andWhere('l.deleted = 0')
                ->andWhere('u.id IN(:userIds)')
                ->andWhere('l.id IN(:lessonIds)')
                ->setParameters(
                    [
                        'userIds' => $userIds,
                        'lessonIds' => $lessonIds,
                    ]
                )
                ->groupBy('u.id', 'ul.state')
                ->getQuery()
                ->getResult();

            foreach ($rows as $row) {
                $userId = $row['userId'];
                $count = (int)$row['count'];

                switch ($row['state']) {
                    case UserLesson::STATE_VIEWED:
                    case UserLesson::STATE_ANSWER_SENT:
                        if (isset($lessonsCompleted[$userId])) {
                            $lessonsCompleted[$userId] += $count;
                        } else {
                            $lessonsCompleted[$userId] = $count;
                        }
                        break;

                    case UserLesson::STATE_ANSWER_CHECKED:
                        if (isset($lessonsPassed[$userId])) {
                            $lessonsPassed[$userId] += $count;
                        } else {
                            $lessonsPassed[$userId] = $count;
                        }
                        break;
                }
            }

            // собираем названия факультетов
            /** @var QueryBuilder $qb */
            $qb = $this->em->getRepository(UserGroupUser::class)->createQueryBuilder('ugu');
            $rows = $qb
                ->select('u.id AS userId', 'f.name AS name')
                ->innerJoin('ugu.user', 'u')
                ->innerJoin('ugu.userGroup', 'ug')
                ->innerJoin('ug.teamFolders', 'tf')
                ->innerJoin('ugu.faculty', 'f')
                ->andWhere('u.id IN(:userIds)')
                ->andWhere('ugu.deleted = 0')
                ->andWhere('tf = :teamFolder')
                ->setParameters(
                    [
                        'userIds' => $userIds,
                        'teamFolder' => $team->getFolder(),
                    ]
                )
                ->getQuery()
                ->getResult();
            foreach ($rows as $row) {
                $faculties[$row['userId']] = $row['name'];
            }
        }

        // собираем юзеров
        foreach ($users as $user) {
            $userId = $user['id'];

            if ($team->getCaptain() && $team->getCaptain()->getId() === $userId) {
                $data['captain'] = $user;

                // пропускаем, если капитан не в команде
                if (!in_array($userId, $userIds)) {
                    continue;
                }
            }

            if ($disableStat) {
                $user['email'] = '';
            }

            $telegramUsernames = explode("|", $user['telegram']);
            unset($user['telegram']);

            $user['username'] = end($telegramUsernames);
            $user['lessonsAvailable'] = count($lessonIds);
            $user['lessonsCompleted'] = $lessonsCompleted[$userId] ?? 0;
            $user['lessonsPassed'] = $lessonsPassed[$userId] ?? 0;
            $user['faculty'] = $faculties[$userId] ?? '';

            $data['users'][] = $user;
        }

        return $data;
    }

    private function processTeam(Request $request, Team $team): Team
    {
        foreach ($request->request->all() as $field => $value) {
            switch ($field) {
                case 'name':
                    $team->setName(
                        $this->formHelper->toRealStr($value)
                    );
                    break;

                case 'folder':
                    $team->setFolder(
                        $this->em->getRepository(TeamFolder::class)->find($value)
                    );
                    break;


                case 'captain':
                    $captain = $value ? $this->em->getRepository(User::class)->find($value) : null;
                    $team->setCaptain($captain);
                    break;

                case 'seniorCaptain':
                    $seniorCaptain = $value ? $this->em->getRepository(User::class)->find($value) : null;
                    $team->setSeniorCaptain($seniorCaptain);
                    break;

                case 'leader':
                    $team->setLeader(
                        $this->em->getRepository(User::class)->find($value)
                    );
                    break;
            }
        }
        $this->em->persist($team);

        return $team;
    }

    /**
     * @param User $user
     *
     * @return bool
     * @todo Метод на время, пока не раскидаем сломанные команды или не сделаем нормальные права для левых челов
     */
    private function needFindByUser(User $user): bool
    {
        return !in_array(
            mb_strtolower($user->getEmail()),
            [
                // сотрудники
                'nikita_eltsov@inbox.ru',
                'demonzarov@ya.ru',
                'unofficial.vd@gmail.com',
                'nagaytsev@yandex.ru',
                'r.pilinskii@likebz.ru',
                'iu.fediakova@likebz.ru',
                'admin@dudev.ru',
                // капитаны
                'barista.0055@mail.ru',
                '4992121@mail.ru',
                'Svetlana3979@gmail.com',
                'surchik7777@gmail.com',
                'Usm-2@ya.ru',
                'julyfedykova@gmail.com',
                'etk_pmg@mail.ru',
                'anesanelis@mail.ru',
                'veryrichadvisor@gmail.com',
                'gumerova-g@bk.ru',
                '9226451@gmail.com',
                '4160964@mail.ru',

                // сотрудники
                'a.ivanchikhina@likebz.ru',
                'a.chekmarev@likebz.ru',
                'ya.lozinskaya@likebz.ru',
                's.ismagilova@likebz.ru',
                'a.korobko.s@likebz.ru',
                'v.cheremnova@likebz.ru',
                'k.samoylova@likebz.ru',
                'a.astapovich@likebz.ru',
                'e.mikhailova@likebz.ru',
                'v.makienko@likebz.ru',
                'e.gerasimova@likebz.ru',
                'a.soboleva@likebz.ru',
                'n.pavlov@likebz.ru',
                'a.kuzovkova@likebz.ru',
                'l.cherniakova@likebz.ru',
                'p.kosova@likebz.ru',
                'd.romanyuk@likebz.ru',
                'm.mezencev@likebz.ru',
                'e.soloveva@likebz.ru',
                'd.kosova@likebz.ru',
                'akorobko002@gmail.com',
                'j.popuchieva@likebz.ru',
                'a.nemtin@likebz.ru',
                'l.volkova@likebz.ru',
                'yulkarudez04@gmail.com',
                'kristintka@gmail.com',
                'n.bushueva@likebz.ru',
                'a.basov@likebz.ru',
                'm.burykina@likebz.ru',
                'a.kulesha@likebz.ru',
                'a.kirianov@likebz.ru',
                'k.zhernovaya@likebz.ru',
                'a.novospasskaia@likebz.ru',
                'i.baibekova@likebz.ru',
                'n.martoshenko@likebz.ru',
                'm.shishmareva@likebz.ru',
                'a.chervonenko@likebz.ru',
                'm.burykina@likebz.ru',
                'd.ryazanzeva@likebz.ru',
                'a.chervonenko@likebz.ru',
                'a.kirianov@likebz.ru',
                'm.troinova@likebz.ru',
                'p.orekhova@likebz.ru',
                'e.solunina@likebz.ru ',
                'a.gubina@likebz.ru',
                'r.sharafiev@likebz.ru',
            ],
            true
        );
    }

    private function getTractionByTeams(array $tractionTeams, ?string $weekId): array
    {
        $summary = [
            'revenue' => 0,
            'profit' => 0,
        ];

        if (null === $weekId) {
            return [$summary, null];
        }

        $teamsSummary = [
            'revenue' => [],
            'profit' => [],
        ];

        foreach ($tractionTeams as $teamIds) {
            $rows = $this->em->getRepository(Value::class)->getTractionValuesByTeams($teamIds);

            foreach ($rows as $row) {
                $teamId = $row['teamId'];
                $sum = (int)$row['sum'];
                $type = ValueType::PROFIT_AND_REVENUE_CODE_NAMES[$row['valueType']];

                $summary[$type] += $sum;

                if (isset($teamsSummary[$type][$teamId])) {
                    $teamsSummary[$type][$teamId] += $sum;
                } else {
                    $teamsSummary[$type][$teamId] = $sum;
                }
            }
        }

        return [$summary, $teamsSummary];
    }

    private function getTractionByUsers(array $userIds): array
    {
        /** @var QueryBuilder $qb */
        $rows = $this->em->getRepository(Value::class)->getTractionValuesByUsers($userIds);

        $userMoney = [];
        foreach ($rows as $row) {
            $userMoney[$row['userId']][ValueType::PROFIT_AND_REVENUE_CODE_NAMES[$row['valueType']]] = $row['sum'];
        }

        return $userMoney;
    }

}

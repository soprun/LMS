<?php

namespace App\DataFixtures;

use App\Entity\AmoCallbackGroupIdByAmoProductId;
use App\Service\AmoHelper;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

final class AmoCallbackGroupIdByAmoProductIdFixture extends Fixture
{
    private static $items = [
        [
            'product_id' => AmoHelper::PRODUCT_HUNDRED4,
            'ticket_level_ids' => [
                4187144 => 510,
                4187152 => 511,
            ],
            'group_id' => 512,
        ],
        [
            'product_id' => AmoHelper::PRODUCT_HUNDRED_5,
            'ticket_level_ids' => [
                4187144 => 589,
                4187152 => 588,
            ],
            'group_id' => 587,
        ],
        [
            'product_id' => AmoHelper::PRODUCT_HUNDRED_6,
            'ticket_level_ids' => [
                4187144 => 675,
                4187152 => 674,
            ],
            'group_id' => 673,
        ],
        [
            'product_id' => AmoHelper::PRODUCT_HUNDRED_7,
            'ticket_level_ids' => [
                4187144 => 753,
                4187152 => 752,
            ],
            'group_id' => 751,
        ],
        [
            'product_id' => AmoHelper::PRODUCT_HUNDRED_8,
            'ticket_level_ids' => [
                4187144 => 867,
                4187152 => 866,
            ],
            'group_id' => 865,
        ],
        [
            'product_id' => AmoHelper::PRODUCT_HUNDRED_9,
            'ticket_level_ids' => [
                4187144 => 1062,
                4187152 => 1061,
            ],
            'group_id' => 1060,
        ],
        [
            'product_id' => AmoHelper::PRODUCT_SPEED_MSA_LITE_BUSINESS,
            'ticket_level_ids' => [
                4188362 => 863,
                4187152 => 1044,
            ],
            'group_id' => null,
        ],
        [
            'product_id' => AmoHelper::PRODUCT_SPEED_CLUB_3,
            'ticket_level_ids' => [
                4188362 => 672,
                4187152 => 671,
            ],
            'group_id' => 670,
        ],
        [
            'product_id' => AmoHelper::PRODUCT_SPEED_CLUB_4,
            'ticket_level_ids' => [
                4188362 => 750,
                4187152 => 749,
            ],
            'group_id' => 748,
        ],
        [
            'product_id' => AmoHelper::PRODUCT_SPEED_CLUB_5,
            'ticket_level_ids' => [
                4188362 => 863,
                4187152 => 861,
            ],
            'group_id' => 864,
        ],
        [
            'product_id' => AmoHelper::PRODUCT_SPEED_CLUB_6,
            'ticket_level_ids' => [
                4188362 => 1043,
                4187152 => 1042,
            ],
            'group_id' => 1041,
        ],
        [
            'product_id' => 4188268,
            'ticket_level_ids' => [
                4188362 => 427,
                4187152 => 428,
            ],
            'group_id' => 429,
        ],
        [
            'product_id' => AmoHelper::PRODUCT_SPEED_CLUB_2,
            'ticket_level_ids' => [
                4188362 => 592,
                4187152 => 591,
            ],
            'group_id' => 590,
        ],
        [
            'product_id' => AmoHelper::PRODUCT_SPEED_INFINITY,
            'ticket_level_ids' => [
                4188362 => 785,
                4187152 => 784,
            ],
            'group_id' => null,
        ],
        [
            'product_id' => 4188376,
            'ticket_level_ids' => [],
            'group_id' => 428,
        ],
        [
            'product_id' => 4188454,
            'ticket_level_ids' => [],
            'group_id' => 591,
        ],
        [
            'product_id' => 4188452,
            'ticket_level_ids' => [],
            'group_id' => 587,
        ],
        [
            'product_id' => 4188270,
            'ticket_level_ids' => [],
            'group_id' => 430,
        ],
        [
            'product_id' => 4188436,
            'ticket_level_ids' => [],
            'group_id' => 604,
        ],
        [
            'product_id' => 4188432,
            'ticket_level_ids' => [
                4187142 => 605,
                4187144 => 606,
            ],
            'group_id' => 604,
        ],
        [
            'product_id' => 4180134,
            'ticket_level_ids' => [],
            'group_id' => 169,
        ],
        [
            'product_id' => 4174028,
            'ticket_level_ids' => [],
            'group_id' => 169,
        ],
        [
            'product_id' => 4179908,
            'ticket_level_ids' => [],
            'group_id' => 302,
        ],
        [
            'product_id' => 4187860,
            'ticket_level_ids' => [],
            'group_id' => 302,
        ],
        [
            'product_id' => 4187800,
            'ticket_level_ids' => [],
            'group_id' => 197,
        ],
        [
            'product_id' => 4175668,
            'ticket_level_ids' => [
                4187154 => 141,
            ],
            'group_id' => 204,
        ],
        [
            'product_id' => 4188350,
            'ticket_level_ids' => [],
            'group_id' => 593,
        ],
        [
            'product_id' => 4188408,
            'ticket_level_ids' => [],
            'group_id' => 677,
        ],
        [
            'product_id' => 4188352,
            'ticket_level_ids' => [],
            'group_id' => 594,
        ],
        [
            'product_id' => AmoHelper::PRODUCT_MARATHON_INVESTMENT_5,
            'ticket_level_ids' => [],
            'group_id' => 601,
        ],
    ];

    public function load(ObjectManager $manager): void
    {
        foreach (self::$items as $item) {
            $entity = new AmoCallbackGroupIdByAmoProductId();

            $entity->setProductId($item['product_id']);
            $entity->setDefaultGroupId($item['group_id']);
            $entity->setTicketLevelIds($item['ticket_level_ids']);

            $manager->persist($entity);
        }
        $manager->flush();
    }

}

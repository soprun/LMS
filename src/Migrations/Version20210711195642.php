<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210711195642 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE traction_target ADD stream_id INT NOT NULL AFTER id');
        $this->addSql('ALTER TABLE traction_target ADD CONSTRAINT FK_DBD993D5D0ED463E FOREIGN KEY (stream_id) REFERENCES course_stream (id)');
        $this->addSql('CREATE INDEX IDX_DBD993D5D0ED463E ON traction_target (stream_id)');
        $this->addSql('ALTER TABLE user_niche ADD stream_id INT NOT NULL AFTER user_id');
        $this->addSql('ALTER TABLE user_niche ADD CONSTRAINT FK_9EA17CE1D0ED463E FOREIGN KEY (stream_id) REFERENCES course_stream (id)');
        $this->addSql('CREATE INDEX IDX_9EA17CE1D0ED463E ON user_niche (stream_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE traction_target DROP FOREIGN KEY FK_DBD993D5D0ED463E');
        $this->addSql('DROP INDEX IDX_DBD993D5D0ED463E ON traction_target');
        $this->addSql('ALTER TABLE traction_target DROP stream_id');
        $this->addSql('ALTER TABLE user_niche DROP FOREIGN KEY FK_9EA17CE1D0ED463E');
        $this->addSql('DROP INDEX IDX_9EA17CE1D0ED463E ON user_niche');
        $this->addSql('ALTER TABLE user_niche DROP stream_id');
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200716212537 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE team DROP FOREIGN KEY FK_C4E0A61FE24F2E6A');
        $this->addSql('DROP INDEX IDX_C4E0A61FE24F2E6A ON team');
        $this->addSql('ALTER TABLE team ADD leader_id INT DEFAULT NULL, CHANGE validating_id captain_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE team ADD CONSTRAINT FK_C4E0A61F3346729B FOREIGN KEY (captain_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE team ADD CONSTRAINT FK_C4E0A61F73154ED4 FOREIGN KEY (leader_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_C4E0A61F3346729B ON team (captain_id)');
        $this->addSql('CREATE INDEX IDX_C4E0A61F73154ED4 ON team (leader_id)');
        $this->addSql('ALTER TABLE team_user DROP is_leader');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE team DROP FOREIGN KEY FK_C4E0A61F3346729B');
        $this->addSql('ALTER TABLE team DROP FOREIGN KEY FK_C4E0A61F73154ED4');
        $this->addSql('DROP INDEX IDX_C4E0A61F3346729B ON team');
        $this->addSql('DROP INDEX IDX_C4E0A61F73154ED4 ON team');
        $this->addSql('ALTER TABLE team ADD validating_id INT DEFAULT NULL, DROP captain_id, DROP leader_id');
        $this->addSql('ALTER TABLE team ADD CONSTRAINT FK_C4E0A61FE24F2E6A FOREIGN KEY (validating_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_C4E0A61FE24F2E6A ON team (validating_id)');
        $this->addSql('ALTER TABLE team_user ADD is_leader TINYINT(1) DEFAULT NULL');
    }
}

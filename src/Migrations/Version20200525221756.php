<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200525221756 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE courses_task_set (id INT AUTO_INCREMENT NOT NULL, block_task_with_check_id INT NOT NULL, variety SMALLINT NOT NULL, block_id SMALLINT NOT NULL, INDEX IDX_70963BC9487045FD (block_task_with_check_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE courses_task_type_1 (id INT AUTO_INCREMENT NOT NULL, question VARCHAR(255) NOT NULL, type_of_task SMALLINT NOT NULL, min_points SMALLINT DEFAULT NULL, max_points SMALLINT DEFAULT NULL, points_before_ddl SMALLINT DEFAULT NULL, points_after_ddl SMALLINT DEFAULT NULL, is_auto_check TINYINT(1) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE courses_task_set ADD CONSTRAINT FK_70963BC9487045FD FOREIGN KEY (block_task_with_check_id) REFERENCES base_block_task_with_check (id)');
        $this->addSql('ALTER TABLE base_block_task_with_check DROP question, DROP description, DROP date_of_start, DROP type_of_task, DROP min_points, DROP max_points, DROP points_before_ddl, DROP points_after_ddl, DROP is_auto_check');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE courses_task_set');
        $this->addSql('DROP TABLE courses_task_type_1');
        $this->addSql('ALTER TABLE base_block_task_with_check ADD question VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, ADD description VARCHAR(1000) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, ADD date_of_start DATETIME DEFAULT NULL, ADD type_of_task SMALLINT DEFAULT NULL, ADD min_points SMALLINT DEFAULT NULL, ADD max_points SMALLINT DEFAULT NULL, ADD points_before_ddl SMALLINT DEFAULT NULL, ADD points_after_ddl SMALLINT DEFAULT NULL, ADD is_auto_check TINYINT(1) DEFAULT NULL');
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211002082402 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE traction_checkpoint (id INT AUTO_INCREMENT NOT NULL, stream_id INT NOT NULL, name VARCHAR(255) NOT NULL, priority INT NOT NULL, INDEX IDX_2DFFC0E6D0ED463E (stream_id), UNIQUE INDEX UNIQ_2DFFC0E6D0ED463E62A6DC27 (stream_id, priority), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE traction_checkpoint_value (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, checkpoint_id INT NOT NULL, value TINYINT(1) NOT NULL, INDEX IDX_50D777AEA76ED395 (user_id), INDEX IDX_50D777AE2DD651CC (checkpoint_id), UNIQUE INDEX UNIQ_50D777AEA76ED3952DD651CC (user_id, checkpoint_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE traction_checkpoint ADD CONSTRAINT FK_2DFFC0E6D0ED463E FOREIGN KEY (stream_id) REFERENCES course_stream (id)');
        $this->addSql('ALTER TABLE traction_checkpoint_value ADD CONSTRAINT FK_50D777AEA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE traction_checkpoint_value ADD CONSTRAINT FK_50D777AE2DD651CC FOREIGN KEY (checkpoint_id) REFERENCES traction_checkpoint (id)');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE traction_checkpoint_value DROP FOREIGN KEY FK_50D777AE2DD651CC');
        $this->addSql('DROP TABLE traction_checkpoint');
        $this->addSql('DROP TABLE traction_checkpoint_value');
    }
}

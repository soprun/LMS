<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211220134440 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql(
        /** @lang SQL */
            'CREATE TABLE IF NOT EXISTS amo_callback (id INT AUTO_INCREMENT NOT NULL, status_id INT DEFAULT NULL, pipeline_id INT DEFAULT NULL, product_id INT DEFAULT NULL, course_name VARCHAR(255) DEFAULT NULL, group_id INT DEFAULT NULL, tags JSON NOT NULL, to_remove TINYINT(1) NOT NULL, ticket_level_id INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB'
        );
        $this->addSql(
        /** @lang SQL */
            'CREATE TABLE IF NOT EXISTS amo_callback_group_id_by_amo_product_id (id INT AUTO_INCREMENT NOT NULL, product_id INT NOT NULL, ticket_level_ids JSON NOT NULL, default_group_id INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB'
        );
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('DROP TABLE amo_callback');
        $this->addSql('DROP TABLE amo_callback_group_id_by_amo_product_id');
    }

}

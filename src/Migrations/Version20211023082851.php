<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211023082851 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE course_stream ADD INDEX IDX_5ED17F511429FD3 (alternative_course_stream_id)');
        $this->addSql('ALTER TABLE faculty ADD CONSTRAINT FK_179660431ED93D47 FOREIGN KEY (user_group_id) REFERENCES user_group (id)');
        $this->addSql('ALTER TABLE traction_checkpoint RENAME INDEX idx_2dffc0e6d0ed463e TO IDX_225FF6B9D0ED463E');
        $this->addSql('ALTER TABLE traction_checkpoint RENAME INDEX uniq_2dffc0e6d0ed463e62a6dc27 TO UNIQ_225FF6B9D0ED463E62A6DC27');
        $this->addSql('DROP INDEX filename ON traction_checkpoint_value');
        $this->addSql('ALTER TABLE traction_checkpoint_value RENAME INDEX idx_50d777aea76ed395 TO IDX_C559DE92A76ED395');
        $this->addSql('ALTER TABLE traction_checkpoint_value RENAME INDEX idx_50d777ae2dd651cc TO IDX_C559DE92F27C615F');
        $this->addSql('ALTER TABLE traction_checkpoint_value RENAME INDEX uniq_50d777aea76ed3952dd651cc TO UNIQ_C559DE92A76ED395F27C615F');
        $this->addSql('ALTER TABLE traction_value CHANGE date date DATE NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE course_stream DROP INDEX IDX_5ED17F511429FD3');
        $this->addSql('ALTER TABLE faculty DROP FOREIGN KEY FK_179660431ED93D47');
        $this->addSql('ALTER TABLE traction_checkpoint RENAME INDEX uniq_225ff6b9d0ed463e62a6dc27 TO UNIQ_2DFFC0E6D0ED463E62A6DC27');
        $this->addSql('ALTER TABLE traction_checkpoint RENAME INDEX idx_225ff6b9d0ed463e TO IDX_2DFFC0E6D0ED463E');
        $this->addSql('CREATE UNIQUE INDEX filename ON traction_checkpoint_value (filename)');
        $this->addSql('ALTER TABLE traction_checkpoint_value RENAME INDEX idx_c559de92a76ed395 TO IDX_50D777AEA76ED395');
        $this->addSql('ALTER TABLE traction_checkpoint_value RENAME INDEX uniq_c559de92a76ed395f27c615f TO UNIQ_50D777AEA76ED3952DD651CC');
        $this->addSql('ALTER TABLE traction_checkpoint_value RENAME INDEX idx_c559de92f27c615f TO IDX_50D777AE2DD651CC');
        $this->addSql('ALTER TABLE traction_value CHANGE date date DATE NOT NULL');
    }
}

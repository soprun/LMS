<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200725182539 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE courses_task_type_6 (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) DEFAULT NULL, description VARCHAR(2000) DEFAULT NULL, analytics INT DEFAULT NULL, sales INT DEFAULT NULL, marketing INT DEFAULT NULL, team INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE courses_answer_task_type_6 (id INT AUTO_INCREMENT NOT NULL, task_id INT NOT NULL, user_id INT NOT NULL, rated_user_id INT NOT NULL, stars SMALLINT NOT NULL, comment VARCHAR(1000) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_853ADAD08DB60186 (task_id), INDEX IDX_853ADAD0A76ED395 (user_id), INDEX IDX_853ADAD0A8957C46 (rated_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE courses_answer_task_type_6 ADD CONSTRAINT FK_853ADAD08DB60186 FOREIGN KEY (task_id) REFERENCES courses_task_type_6 (id)');
        $this->addSql('ALTER TABLE courses_answer_task_type_6 ADD CONSTRAINT FK_853ADAD0A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE courses_answer_task_type_6 ADD CONSTRAINT FK_853ADAD0A8957C46 FOREIGN KEY (rated_user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE courses_answer_task_type_6 DROP FOREIGN KEY FK_853ADAD08DB60186');
        $this->addSql('DROP TABLE courses_task_type_6');
        $this->addSql('DROP TABLE courses_answer_task_type_6');
    }
}

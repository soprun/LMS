<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211215135219 extends AbstractMigration
{
    private const COURSE_NAME = 'МСА-LITE';
    private const COURSE_SLUG = 'msa_lite';

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $expr = $this->connection->createQueryBuilder()->expr();

        $this->connection->beginTransaction();

        $this->connection->createQueryBuilder()->insert('abstract_course')
            ->values([
                'name' => $expr->literal(self::COURSE_NAME),
                'slug' => $expr->literal(self::COURSE_SLUG),
                'settlement_period' => $expr->literal('1 week'),
            ])->execute();

        $oldStream = $this->connection->createQueryBuilder()->select('*')
            ->from('course_stream')
            ->where($expr->in('id', 55))->orderBy('id')
            ->execute()->fetch();

        $this->connection->createQueryBuilder();
        $newAbstractCourseId = $this->getLastInsertIdForTransaction();

        if (!$oldStream) {
            $this->connection->rollBack();

            return;
        }

        $this->connection->createQueryBuilder()->insert('course_stream')->values([
            'abstract_course_id' => $expr->literal($newAbstractCourseId),
            'stream' => 1,
            'name' => $expr->literal('Скорость МСА Лайт (бизнес)'),
            'start_date' => $expr->literal($oldStream['start_date']),
            'period' => 6,
            'active' => 1,
        ])->execute();

        $this->connection->createQueryBuilder()->update('user_group')
            ->set('course_stream_id', $this->getLastInsertIdForTransaction())
            ->where('id = :id')
            ->setParameter('id', 1044)
            ->execute();

        $this->connection->commit();
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    private function getLastInsertIdForTransaction(): int
    {
        return (int)$this->connection->executeQuery('SELECT last_insert_id();')->fetchOne();
    }

}

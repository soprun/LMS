<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200716150937 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE courses_task_test_question_answer (id INT AUTO_INCREMENT NOT NULL, question_id INT NOT NULL, text VARCHAR(255) NOT NULL, is_correct TINYINT(1) NOT NULL, position INT NOT NULL, is_deleted TINYINT(1) NOT NULL, points INT NOT NULL, money INT DEFAULT NULL, deals INT DEFAULT NULL, romi INT DEFAULT NULL, nps INT DEFAULT NULL, analytics INT DEFAULT NULL, sales INT DEFAULT NULL, marketing INT DEFAULT NULL, team INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_7D1631BE1E27F6BF (question_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE courses_task_test_result (id INT AUTO_INCREMENT NOT NULL, test_id INT NOT NULL, img_file_id INT DEFAULT NULL, text VARCHAR(2000) NOT NULL, points_from INT NOT NULL, points_to INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_8A3779821E5D0459 (test_id), INDEX IDX_8A377982DEA43D84 (img_file_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE courses_task_test (id INT AUTO_INCREMENT NOT NULL, img_file_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, description VARCHAR(2000) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_B53418F9DEA43D84 (img_file_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE courses_task_test_question_user_answer (id INT AUTO_INCREMENT NOT NULL, question_id INT NOT NULL, answer_id INT NOT NULL, user_id INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_D9DB330B1E27F6BF (question_id), INDEX IDX_D9DB330BAA334807 (answer_id), INDEX IDX_D9DB330BA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE courses_task_test_question (id INT AUTO_INCREMENT NOT NULL, test_id INT NOT NULL, img_file_id INT DEFAULT NULL, text VARCHAR(2000) NOT NULL, is_multiple_answers TINYINT(1) NOT NULL, position INT NOT NULL, is_deleted TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_1B1750F61E5D0459 (test_id), INDEX IDX_1B1750F6DEA43D84 (img_file_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE courses_task_test_question_answer ADD CONSTRAINT FK_7D1631BE1E27F6BF FOREIGN KEY (question_id) REFERENCES courses_task_test_question (id)');
        $this->addSql('ALTER TABLE courses_task_test_result ADD CONSTRAINT FK_8A3779821E5D0459 FOREIGN KEY (test_id) REFERENCES courses_task_test (id)');
        $this->addSql('ALTER TABLE courses_task_test_result ADD CONSTRAINT FK_8A377982DEA43D84 FOREIGN KEY (img_file_id) REFERENCES base_files (id)');
        $this->addSql('ALTER TABLE courses_task_test ADD CONSTRAINT FK_B53418F9DEA43D84 FOREIGN KEY (img_file_id) REFERENCES base_files (id)');
        $this->addSql('ALTER TABLE courses_task_test_question_user_answer ADD CONSTRAINT FK_D9DB330B1E27F6BF FOREIGN KEY (question_id) REFERENCES courses_task_test_question (id)');
        $this->addSql('ALTER TABLE courses_task_test_question_user_answer ADD CONSTRAINT FK_D9DB330BAA334807 FOREIGN KEY (answer_id) REFERENCES courses_task_test_question_answer (id)');
        $this->addSql('ALTER TABLE courses_task_test_question_user_answer ADD CONSTRAINT FK_D9DB330BA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE courses_task_test_question ADD CONSTRAINT FK_1B1750F61E5D0459 FOREIGN KEY (test_id) REFERENCES courses_task_test (id)');
        $this->addSql('ALTER TABLE courses_task_test_question ADD CONSTRAINT FK_1B1750F6DEA43D84 FOREIGN KEY (img_file_id) REFERENCES base_files (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE courses_task_test_question_user_answer DROP FOREIGN KEY FK_D9DB330BAA334807');
        $this->addSql('ALTER TABLE courses_task_test_result DROP FOREIGN KEY FK_8A3779821E5D0459');
        $this->addSql('ALTER TABLE courses_task_test_question DROP FOREIGN KEY FK_1B1750F61E5D0459');
        $this->addSql('ALTER TABLE courses_task_test_question_answer DROP FOREIGN KEY FK_7D1631BE1E27F6BF');
        $this->addSql('ALTER TABLE courses_task_test_question_user_answer DROP FOREIGN KEY FK_D9DB330B1E27F6BF');
        $this->addSql('DROP TABLE courses_task_test_question_answer');
        $this->addSql('DROP TABLE courses_task_test_result');
        $this->addSql('DROP TABLE courses_task_test');
        $this->addSql('DROP TABLE courses_task_test_question_user_answer');
        $this->addSql('DROP TABLE courses_task_test_question');
    }
}

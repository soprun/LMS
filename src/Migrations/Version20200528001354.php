<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200528001354 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE courses_answer_task_type_1 ADD validating_id INT DEFAULT NULL, ADD validating_text VARCHAR(1000) DEFAULT NULL');
        $this->addSql('ALTER TABLE courses_answer_task_type_1 ADD CONSTRAINT FK_1B5E4F73E24F2E6A FOREIGN KEY (validating_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_1B5E4F73E24F2E6A ON courses_answer_task_type_1 (validating_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE courses_answer_task_type_1 DROP FOREIGN KEY FK_1B5E4F73E24F2E6A');
        $this->addSql('DROP INDEX IDX_1B5E4F73E24F2E6A ON courses_answer_task_type_1');
        $this->addSql('ALTER TABLE courses_answer_task_type_1 DROP validating_id, DROP validating_text');
    }
}

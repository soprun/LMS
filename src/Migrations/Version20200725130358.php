<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200725130358 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE courses_answer_task_type_5 (id INT AUTO_INCREMENT NOT NULL, task_id INT NOT NULL, user_id INT NOT NULL, points INT NOT NULL, validating_text VARCHAR(255) DEFAULT NULL, money INT DEFAULT NULL, deals INT DEFAULT NULL, romi INT DEFAULT NULL, nps INT DEFAULT NULL, analytics INT DEFAULT NULL, sales INT DEFAULT NULL, marketing INT DEFAULT NULL, team INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_1C338B6A8DB60186 (task_id), INDEX IDX_1C338B6AA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE courses_answer_task_type_5 ADD CONSTRAINT FK_1C338B6A8DB60186 FOREIGN KEY (task_id) REFERENCES courses_task_type_5 (id)');
        $this->addSql('ALTER TABLE courses_answer_task_type_5 ADD CONSTRAINT FK_1C338B6AA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE courses_answer_task_type_5');
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200531163027 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE courses_course_admin (course_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_1FE2C51B591CC992 (course_id), INDEX IDX_1FE2C51BA76ED395 (user_id), PRIMARY KEY(course_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE courses_course_folder_admin (course_folder_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_3FA33484948DF9EE (course_folder_id), INDEX IDX_3FA33484A76ED395 (user_id), PRIMARY KEY(course_folder_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE courses_course_admin ADD CONSTRAINT FK_1FE2C51B591CC992 FOREIGN KEY (course_id) REFERENCES courses_course (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE courses_course_admin ADD CONSTRAINT FK_1FE2C51BA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE courses_course_folder_admin ADD CONSTRAINT FK_3FA33484948DF9EE FOREIGN KEY (course_folder_id) REFERENCES courses_course_folder (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE courses_course_folder_admin ADD CONSTRAINT FK_3FA33484A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE courses_course_admin');
        $this->addSql('DROP TABLE courses_course_folder_admin');
    }
}

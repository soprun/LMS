<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200527182340 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE team ADD created_at DATETIME DEFAULT "2020-01-01", ADD updated_at DATETIME DEFAULT "2020-01-01"');
        $this->addSql('ALTER TABLE courses_lesson_configuration ADD created_at DATETIME DEFAULT "2020-01-01", ADD updated_at DATETIME DEFAULT "2020-01-01"');
        $this->addSql('ALTER TABLE team_user ADD created_at DATETIME DEFAULT "2020-01-01", ADD updated_at DATETIME DEFAULT "2020-01-01"');
        $this->addSql('ALTER TABLE courses_course_configutarion ADD created_at DATETIME DEFAULT "2020-01-01", ADD updated_at DATETIME DEFAULT "2020-01-01"');
        $this->addSql('ALTER TABLE courses_lesson ADD created_at DATETIME DEFAULT "2020-01-01", ADD updated_at DATETIME DEFAULT "2020-01-01"');
        $this->addSql('ALTER TABLE courses_lesson_part_block ADD created_at DATETIME DEFAULT "2020-01-01", ADD updated_at DATETIME DEFAULT "2020-01-01"');
        $this->addSql('ALTER TABLE courses_module ADD created_at DATETIME DEFAULT "2020-01-01", ADD updated_at DATETIME DEFAULT "2020-01-01"');
        $this->addSql('ALTER TABLE courses_course ADD created_at DATETIME DEFAULT "2020-01-01", ADD updated_at DATETIME DEFAULT "2020-01-01"');
        $this->addSql('ALTER TABLE courses_course_folder ADD created_at DATETIME DEFAULT "2020-01-01", ADD updated_at DATETIME DEFAULT "2020-01-01"');
        $this->addSql('ALTER TABLE courses_lesson_part ADD created_at DATETIME DEFAULT "2020-01-01", ADD updated_at DATETIME DEFAULT "2020-01-01"');
        $this->addSql('ALTER TABLE group_permission ADD created_at DATETIME DEFAULT "2020-01-01", ADD updated_at DATETIME DEFAULT "2020-01-01"');
        $this->addSql('ALTER TABLE user_group ADD created_at DATETIME DEFAULT "2020-01-01", ADD updated_at DATETIME DEFAULT "2020-01-01"');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE courses_course DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE courses_course_configutarion DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE courses_course_folder DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE courses_lesson DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE courses_lesson_configuration DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE courses_lesson_part DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE courses_lesson_part_block DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE courses_module DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE group_permission DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE team DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE team_user DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE user_group DROP created_at, DROP updated_at');
    }
}

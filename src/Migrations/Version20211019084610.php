<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Throwable;

final class Version20211019084610 extends AbstractMigration
{
    private const COURSE_SKILLS_NAME = 'Скорость.Навыки';
    private const SPEED_STREAMS = [19, 30, 32, 48];

    /**
     * @throws Throwable
     */
    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $createdAt = date('Y-m-d H:i:s');
        $expr = $this->connection->createQueryBuilder()->expr();

        $this->connection->beginTransaction();

        $this->connection->createQueryBuilder()->insert('abstract_course')
            ->values([
                'name' => $expr->literal(self::COURSE_SKILLS_NAME),
                'slug' => $expr->literal('skills'),
            ])->execute();

        $oldStreams = $this->connection->createQueryBuilder()->select('*')
            ->from('course_stream')
            ->where($expr->in('id', self::SPEED_STREAMS))->orderBy('id')
            ->execute()->fetchAllAssociative();

        $this->connection->createQueryBuilder();
        $newAbstractCourseId = $this->getLastInsertIdForTransaction();

        foreach ($oldStreams as $num => $stream) {
            $newStreamName = self::COURSE_SKILLS_NAME . ' ' . ($num + 1);

            $startDate = $stream['start_date'];
            $startDate = date('Y-m-d', strtotime("+6 weeks", strtotime($startDate)));

            $this->connection->createQueryBuilder()->insert('course_stream')->values([
                'abstract_course_id' => $expr->literal($newAbstractCourseId),
                'stream' => $stream['stream'],
                'name' => $expr->literal($newStreamName),
                'start_date' => $expr->literal($startDate),
                'period' => 104,
                'active' => $stream['active'],
            ])->execute();
            $newStreamId = $this->getLastInsertIdForTransaction();
            $userGroups = $this->connection->createQueryBuilder()->select('*')->from('user_group')
                ->where($expr->eq('course_stream_id', $stream['id']))->execute()->fetchAllAssociative();
            foreach ($userGroups as $group) {
                $this->connection->createQueryBuilder()->insert('user_group')->values([
                    'name' => $expr->literal($group['name'] . ' (' . $newStreamName . ')'),
                    'course_stream_id' => $newStreamId,
                    'created_at' => $expr->literal($createdAt),
                    'updated_at' => $expr->literal($createdAt),
                ])->execute();

                $newUserGroupId = $this->getLastInsertIdForTransaction();
                $userGroupUsers = $this->connection->createQueryBuilder()->select('*')
                    ->from('user_group_user')
                    ->where($expr->eq('user_group_id', $group['id']))
                    ->execute()->fetchAllAssociative();

                foreach ($userGroupUsers as $userGroupUser) {
                    $this->connection->createQueryBuilder()->insert('user_group_user')->values([
                        'user_group_id' => $newUserGroupId,
                        'user_id' => $userGroupUser['user_id'] ?? 'NULL',
                        'amo_lead_id' => $userGroupUser['amo_lead_id'] ?? 'NULL',
                        'faculty_id' => $userGroupUser['faculty_id'] ?? 'NULL',
                        'budget' => $userGroupUser['budget'] ?? 'NULL',
                        'created_at' => $expr->literal($createdAt),
                        'updated_at' => $expr->literal($createdAt),
                        'is_deleted' => $userGroupUser['is_deleted'] ?? 'NULL',
                        'deleted' => $userGroupUser['deleted'],
                    ])->execute();
                }
            }
        }

        $this->connection->commit();
    }

    /**
     * @throws Throwable
     */
    private function getLastInsertIdForTransaction(): int
    {
        return (int)$this->connection->executeQuery('SELECT last_insert_id();')->fetchOne();
    }

    public function down(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
    }

}

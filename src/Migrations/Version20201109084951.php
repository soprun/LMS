<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201109084951 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE answer_comment (id INT AUTO_INCREMENT NOT NULL, commentator_id INT NOT NULL, answer_id INT NOT NULL, variety INT NOT NULL, position INT NOT NULL, comment VARCHAR(2000) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_DC349D1D506AFCC0 (commentator_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE answer_comment_base_files (answer_comment_id INT NOT NULL, base_files_id INT NOT NULL, INDEX IDX_AB0D59EB97E4F52D (answer_comment_id), INDEX IDX_AB0D59EBE90DF8D7 (base_files_id), PRIMARY KEY(answer_comment_id, base_files_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE answer_comment ADD CONSTRAINT FK_DC349D1D506AFCC0 FOREIGN KEY (commentator_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE answer_comment_base_files ADD CONSTRAINT FK_AB0D59EB97E4F52D FOREIGN KEY (answer_comment_id) REFERENCES answer_comment (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE answer_comment_base_files ADD CONSTRAINT FK_AB0D59EBE90DF8D7 FOREIGN KEY (base_files_id) REFERENCES base_files (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE answer_comment_base_files DROP FOREIGN KEY FK_AB0D59EB97E4F52D');
        $this->addSql('DROP TABLE answer_comment');
        $this->addSql('DROP TABLE answer_comment_base_files');
    }
}

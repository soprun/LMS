<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210618210816 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE smart_sender_bot ADD abstract_course_id INT DEFAULT NULL AFTER token');
        $this->addSql('ALTER TABLE smart_sender_bot ADD CONSTRAINT FK_A45C80168C8C2552 FOREIGN KEY (abstract_course_id) REFERENCES abstract_course (id)');
        $this->addSql('CREATE INDEX IDX_A45C80168C8C2552 ON smart_sender_bot (abstract_course_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE smart_sender_bot DROP FOREIGN KEY FK_A45C80168C8C2552');
        $this->addSql('DROP INDEX IDX_A45C80168C8C2552 ON smart_sender_bot');
        $this->addSql('ALTER TABLE smart_sender_bot DROP abstract_course_id');
    }
}

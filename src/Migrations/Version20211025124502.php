<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211025124502 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE courses_course DROP is_delete');
        $this->addSql('ALTER TABLE courses_course_folder DROP is_delete');
        $this->addSql('ALTER TABLE courses_lesson DROP is_delete');
        $this->addSql('ALTER TABLE courses_lesson_part_block DROP is_deleted');
        $this->addSql('ALTER TABLE courses_task_set DROP is_deleted');
        $this->addSql('ALTER TABLE courses_task_test_question DROP is_deleted');
        $this->addSql('ALTER TABLE courses_task_test_question_answer DROP is_deleted');
        $this->addSql('ALTER TABLE courses_task_test_result DROP is_deleted');
        $this->addSql('ALTER TABLE partner DROP is_deleted');
        $this->addSql('ALTER TABLE points_shop_product DROP is_deleted');
        $this->addSql('ALTER TABLE user_group_user DROP is_deleted');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE courses_course ADD is_delete TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE courses_course_folder ADD is_delete TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE courses_lesson ADD is_delete TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE courses_lesson_part_block ADD is_deleted TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE courses_task_set ADD is_deleted TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE courses_task_test_question ADD is_deleted TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE courses_task_test_question_answer ADD is_deleted TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE courses_task_test_result ADD is_deleted TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE partner ADD is_deleted TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE points_shop_product ADD is_deleted TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE user_group_user ADD is_deleted TINYINT(1) DEFAULT NULL');
    }
}

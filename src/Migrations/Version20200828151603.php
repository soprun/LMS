<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200828151603 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE team_report_captain (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, channel VARCHAR(255) NOT NULL, channel_id INT NOT NULL, INDEX IDX_49D103ACA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE team_report_captain ADD CONSTRAINT FK_49D103ACA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE team_report_cell ADD team_report_captain_id INT DEFAULT NULL, CHANGE team_report_user_id team_report_user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE team_report_cell ADD CONSTRAINT FK_539B85546CDAD0FC FOREIGN KEY (team_report_captain_id) REFERENCES team_report_captain (id)');
        $this->addSql('CREATE INDEX IDX_539B85546CDAD0FC ON team_report_cell (team_report_captain_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE team_report_captain');
        $this->addSql('ALTER TABLE team_report_cell DROP FOREIGN KEY FK_539B85546CDAD0FC');
        $this->addSql('DROP INDEX IDX_539B85546CDAD0FC ON team_report_cell');
        $this->addSql('ALTER TABLE team_report_cell DROP team_report_captain_id, CHANGE team_report_user_id team_report_user_id INT NOT NULL');
    }
}

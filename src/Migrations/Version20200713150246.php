<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200713150246 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE courses_course ADD is_delete TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE courses_course_folder ADD is_delete TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE courses_task_type_1 ADD is_link_forbidden TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE base_block_3 ADD img_file_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE base_block_3 ADD CONSTRAINT FK_3EF548CADEA43D84 FOREIGN KEY (img_file_id) REFERENCES base_files (id)');
        $this->addSql('CREATE INDEX IDX_3EF548CADEA43D84 ON base_block_3 (img_file_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE base_block_3 DROP FOREIGN KEY FK_3EF548CADEA43D84');
        $this->addSql('DROP INDEX IDX_3EF548CADEA43D84 ON base_block_3');
        $this->addSql('ALTER TABLE base_block_3 DROP img_file_id');
        $this->addSql('ALTER TABLE courses_course DROP is_delete');
        $this->addSql('ALTER TABLE courses_course_folder DROP is_delete');
        $this->addSql('ALTER TABLE courses_task_type_1 DROP is_link_forbidden');
    }
}
<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200624181658 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_group_folder ADD parent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_group_folder ADD CONSTRAINT FK_BFE8AD1F727ACA70 FOREIGN KEY (parent_id) REFERENCES user_group_folder (id)');
        $this->addSql('CREATE INDEX IDX_BFE8AD1F727ACA70 ON user_group_folder (parent_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_group_folder DROP FOREIGN KEY FK_BFE8AD1F727ACA70');
        $this->addSql('DROP INDEX IDX_BFE8AD1F727ACA70 ON user_group_folder');
        $this->addSql('ALTER TABLE user_group_folder DROP parent_id');
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200527182902 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE base_nps_1 ADD created_at DATETIME DEFAULT "2020-01-01", ADD updated_at DATETIME DEFAULT "2020-01-01"');
        $this->addSql('ALTER TABLE base_block_6 ADD created_at DATETIME DEFAULT "2020-01-01", ADD updated_at DATETIME DEFAULT "2020-01-01"');
        $this->addSql('ALTER TABLE base_block_task_with_check ADD created_at DATETIME DEFAULT "2020-01-01", ADD updated_at DATETIME DEFAULT "2020-01-01"');
        $this->addSql('ALTER TABLE base_block_7 ADD created_at DATETIME DEFAULT "2020-01-01", ADD updated_at DATETIME DEFAULT "2020-01-01"');
        $this->addSql('ALTER TABLE base_block_5 ADD created_at DATETIME DEFAULT "2020-01-01", ADD updated_at DATETIME DEFAULT "2020-01-01"');
        $this->addSql('ALTER TABLE base_files ADD created_at DATETIME DEFAULT "2020-01-01", ADD updated_at DATETIME DEFAULT "2020-01-01"');
        $this->addSql('ALTER TABLE base_block_4 ADD created_at DATETIME DEFAULT "2020-01-01", ADD updated_at DATETIME DEFAULT "2020-01-01"');
        $this->addSql('ALTER TABLE base_block_1 ADD created_at DATETIME DEFAULT "2020-01-01", ADD updated_at DATETIME DEFAULT "2020-01-01"');
        $this->addSql('ALTER TABLE base_block_3 ADD created_at DATETIME DEFAULT "2020-01-01", ADD updated_at DATETIME DEFAULT "2020-01-01"');
        $this->addSql('ALTER TABLE base_block_2 ADD created_at DATETIME DEFAULT "2020-01-01", ADD updated_at DATETIME DEFAULT "2020-01-01"');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE base_block_1 DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE base_block_2 DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE base_block_3 DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE base_block_4 DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE base_block_5 DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE base_block_6 DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE base_block_7 DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE base_block_task_with_check DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE base_files DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE base_nps_1 DROP created_at, DROP updated_at');
    }
}

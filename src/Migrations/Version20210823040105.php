<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210823040105 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE traction_value (id INT AUTO_INCREMENT NOT NULL, owner_id INT NOT NULL, type_id INT NOT NULL, date DATE NOT NULL, INDEX IDX_79BCDEB17E3C61F9 (owner_id), INDEX IDX_79BCDEB1C54C8C93 (type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE traction_value_course_stream (value_id INT NOT NULL, course_stream_id INT NOT NULL, INDEX IDX_E3A6139AF920BBA2 (value_id), INDEX IDX_E3A6139A524C0692 (course_stream_id), PRIMARY KEY(value_id, course_stream_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE traction_value_type (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE traction_value_type_abstract_course (value_type_id INT NOT NULL, abstract_course_id INT NOT NULL, INDEX IDX_247BB7262EEEF9F3 (value_type_id), INDEX IDX_247BB7268C8C2552 (abstract_course_id), PRIMARY KEY(value_type_id, abstract_course_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE traction_value ADD CONSTRAINT FK_79BCDEB17E3C61F9 FOREIGN KEY (owner_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE traction_value ADD CONSTRAINT FK_79BCDEB1C54C8C93 FOREIGN KEY (type_id) REFERENCES traction_value_type (id)');
        $this->addSql('ALTER TABLE traction_value_course_stream ADD CONSTRAINT FK_E3A6139AF920BBA2 FOREIGN KEY (value_id) REFERENCES traction_value (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE traction_value_course_stream ADD CONSTRAINT FK_E3A6139A524C0692 FOREIGN KEY (course_stream_id) REFERENCES course_stream (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE traction_value_type_abstract_course ADD CONSTRAINT FK_247BB7262EEEF9F3 FOREIGN KEY (value_type_id) REFERENCES traction_value_type (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE traction_value_type_abstract_course ADD CONSTRAINT FK_247BB7268C8C2552 FOREIGN KEY (abstract_course_id) REFERENCES abstract_course (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE traction_value_course_stream DROP FOREIGN KEY FK_E3A6139AF920BBA2');
        $this->addSql('ALTER TABLE traction_value DROP FOREIGN KEY FK_79BCDEB1C54C8C93');
        $this->addSql('ALTER TABLE traction_value_type_abstract_course DROP FOREIGN KEY FK_247BB7262EEEF9F3');
        $this->addSql('DROP TABLE traction_value');
        $this->addSql('DROP TABLE traction_value_course_stream');
        $this->addSql('DROP TABLE traction_value_type');
        $this->addSql('DROP TABLE traction_value_type_abstract_course');
    }
}

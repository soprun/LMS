<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210326220853 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE courses_answer_task_type_6 DROP FOREIGN KEY FK_853ADAD08DB60186');
        $this->addSql('ALTER TABLE games_achievement_game_user DROP FOREIGN KEY FK_E836C065B3EC99FE');
        $this->addSql('ALTER TABLE games_chapter_part DROP FOREIGN KEY FK_72D01364579F4768');
        $this->addSql('ALTER TABLE games_game_user_chapter_progress DROP FOREIGN KEY FK_DA0434C0579F4768');
        $this->addSql('ALTER TABLE games_hint DROP FOREIGN KEY FK_2BBECF8A579F4768');
        $this->addSql('ALTER TABLE games_knowledge_base_chapter DROP FOREIGN KEY FK_6FA569A5579F4768');
        $this->addSql('ALTER TABLE games_task_with_correct_answer DROP FOREIGN KEY FK_10F6E47E65739FF9');
        $this->addSql('ALTER TABLE games_task_with_path_change_option DROP FOREIGN KEY FK_8F4DA45D65739FF9');
        $this->addSql('ALTER TABLE push DROP FOREIGN KEY FK_5F3A166411A9D36F');
        $this->addSql('ALTER TABLE games_chapter_part_block DROP FOREIGN KEY FK_CC27DE0736D24FA3');
        $this->addSql('ALTER TABLE games_game_user_chapter_part_finish DROP FOREIGN KEY FK_112BC80036D24FA3');
        $this->addSql('ALTER TABLE games_chapter_part_block_button DROP FOREIGN KEY FK_61DCAAC564C06B48');
        $this->addSql('ALTER TABLE games_chapter_part_block_text_paragraph DROP FOREIGN KEY FK_3DE450F064C06B48');
        $this->addSql('ALTER TABLE games_task_with_correct_answer DROP FOREIGN KEY FK_10F6E47E64C06B48');
        $this->addSql('ALTER TABLE games_task_with_path_change DROP FOREIGN KEY FK_747CFDDF64C06B48');
        $this->addSql('ALTER TABLE games_achievement DROP FOREIGN KEY FK_FDA0B6FE48FD905');
        $this->addSql('ALTER TABLE games_chapter DROP FOREIGN KEY FK_4248DAC2E48FD905');
        $this->addSql('ALTER TABLE games_game_product DROP FOREIGN KEY FK_51A66CB5E48FD905');
        $this->addSql('ALTER TABLE games_game_user DROP FOREIGN KEY FK_4940C185E48FD905');
        $this->addSql('ALTER TABLE games_one_time_bonus DROP FOREIGN KEY FK_8EE462A1E48FD905');
        $this->addSql('ALTER TABLE games_strategy DROP FOREIGN KEY FK_BD4122D1E48FD905');
        $this->addSql('ALTER TABLE games_achievement_game_user DROP FOREIGN KEY FK_E836C06532ACACBA');
        $this->addSql('ALTER TABLE games_game_user DROP FOREIGN KEY FK_4940C185AC68C458');
        $this->addSql('ALTER TABLE games_game_user_chapter_part_finish DROP FOREIGN KEY FK_112BC80032ACACBA');
        $this->addSql('ALTER TABLE games_game_user_chapter_progress DROP FOREIGN KEY FK_DA0434C032ACACBA');
        $this->addSql('ALTER TABLE games_game_user_counter DROP FOREIGN KEY FK_9E605AD032ACACBA');
        $this->addSql('ALTER TABLE games_game_user_strategy DROP FOREIGN KEY FK_CC142CF932ACACBA');
        $this->addSql('ALTER TABLE games_hint_game_user_requested DROP FOREIGN KEY FK_C9EC979332ACACBA');
        $this->addSql('ALTER TABLE games_task_with_correct_answer_answer DROP FOREIGN KEY FK_8501AD2632ACACBA');
        $this->addSql('ALTER TABLE push_query DROP FOREIGN KEY FK_2A36075132ACACBA');
        $this->addSql('ALTER TABLE games_hint_game_user_requested DROP FOREIGN KEY FK_C9EC9793519161AB');
        $this->addSql('ALTER TABLE games_task_with_correct_answer DROP FOREIGN KEY FK_10F6E47EA7C83961');
        $this->addSql('ALTER TABLE games_task_with_path_change_option DROP FOREIGN KEY FK_8F4DA45DA7C83961');
        $this->addSql('ALTER TABLE games_game_user_strategy DROP FOREIGN KEY FK_CC142CF9D5CAD932');
        $this->addSql('ALTER TABLE games_task_with_correct_answer DROP FOREIGN KEY FK_10F6E47ED5CAD932');
        $this->addSql('ALTER TABLE games_task_with_path_change_option DROP FOREIGN KEY FK_8F4DA45DD5CAD932');
        $this->addSql('ALTER TABLE games_task_with_correct_answer_answer DROP FOREIGN KEY FK_8501AD2633239AEA');
        $this->addSql('ALTER TABLE games_task_with_path_change_option DROP FOREIGN KEY FK_8F4DA45D667C07B6');
        $this->addSql('ALTER TABLE games_knowledge_base_chapter DROP FOREIGN KEY FK_6FA569A51620F1CE');
        $this->addSql('ALTER TABLE games_game_product DROP FOREIGN KEY FK_51A66CB54584665A');
        $this->addSql('ALTER TABLE games_task_with_correct_answer DROP FOREIGN KEY FK_10F6E47EF30BCAF6');
        $this->addSql('ALTER TABLE games_task_with_path_change_option DROP FOREIGN KEY FK_8F4DA45DF30BCAF6');
        $this->addSql('ALTER TABLE push_query DROP FOREIGN KEY FK_2A360751F30BCAF6');
        $this->addSql('ALTER TABLE team_report_user DROP FOREIGN KEY FK_158FD4FF296CD8AE');
        $this->addSql('ALTER TABLE team_user DROP FOREIGN KEY FK_5C722232296CD8AE');
        $this->addSql('ALTER TABLE team_report_cell DROP FOREIGN KEY FK_539B85546CDAD0FC');
        $this->addSql('ALTER TABLE team_report_cell DROP FOREIGN KEY FK_539B8554408A6F62');
        $this->addSql('DROP TABLE courses_answer_task_type_6');
        $this->addSql('DROP TABLE courses_task_type_6');
        $this->addSql('DROP TABLE games_achievement');
        $this->addSql('DROP TABLE games_achievement_game_user');
        $this->addSql('DROP TABLE games_chapter');
        $this->addSql('DROP TABLE games_chapter_part');
        $this->addSql('DROP TABLE games_chapter_part_block');
        $this->addSql('DROP TABLE games_chapter_part_block_button');
        $this->addSql('DROP TABLE games_chapter_part_block_text_paragraph');
        $this->addSql('DROP TABLE games_game');
        $this->addSql('DROP TABLE games_game_product');
        $this->addSql('DROP TABLE games_game_user');
        $this->addSql('DROP TABLE games_game_user_chapter_part_finish');
        $this->addSql('DROP TABLE games_game_user_chapter_progress');
        $this->addSql('DROP TABLE games_game_user_counter');
        $this->addSql('DROP TABLE games_game_user_strategy');
        $this->addSql('DROP TABLE games_hint');
        $this->addSql('DROP TABLE games_hint_game_user_requested');
        $this->addSql('DROP TABLE games_knowledge_base_chapter');
        $this->addSql('DROP TABLE games_one_time_bonus');
        $this->addSql('DROP TABLE games_strategy');
        $this->addSql('DROP TABLE games_task_with_correct_answer');
        $this->addSql('DROP TABLE games_task_with_correct_answer_answer');
        $this->addSql('DROP TABLE games_task_with_path_change');
        $this->addSql('DROP TABLE games_task_with_path_change_option');
        $this->addSql('DROP TABLE knowledge_base');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE push');
        $this->addSql('DROP TABLE push_query');
        $this->addSql('DROP TABLE team');
        $this->addSql('DROP TABLE team_report_captain');
        $this->addSql('DROP TABLE team_report_cell');
        $this->addSql('DROP TABLE team_report_user');
        $this->addSql('DROP TABLE team_user');
        $this->addSql('ALTER TABLE courses_task_test_question_answer DROP money, DROP deals, DROP romi, DROP nps, DROP analytics, DROP sales, DROP marketing, DROP team');
        $this->addSql('ALTER TABLE courses_task_type_1 DROP money, DROP deals, DROP romi, DROP nps, DROP analytics, DROP sales, DROP marketing, DROP team');
        $this->addSql('ALTER TABLE courses_task_type_5 DROP money, DROP deals, DROP romi, DROP nps, DROP analytics, DROP sales, DROP marketing, DROP team, DROP add_cor_ans_money, DROP add_cor_ans_deals, DROP add_cor_ans_romi, DROP add_cor_ans_nps, DROP add_cor_ans_analytics, DROP add_cor_ans_sales, DROP add_cor_ans_marketing, DROP add_cor_ans_team');
        $this->addSql('ALTER TABLE courses_answer_task_type_5 DROP money, DROP deals, DROP romi, DROP nps, DROP analytics, DROP sales, DROP marketing, DROP team');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE courses_answer_task_type_6 (id INT AUTO_INCREMENT NOT NULL, task_id INT NOT NULL, user_id INT NOT NULL, rated_user_id INT NOT NULL, stars SMALLINT NOT NULL, comment VARCHAR(1000) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_853ADAD08DB60186 (task_id), INDEX IDX_853ADAD0A8957C46 (rated_user_id), INDEX IDX_853ADAD0A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE courses_task_type_6 (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, description VARCHAR(2000) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, analytics INT DEFAULT NULL, sales INT DEFAULT NULL, marketing INT DEFAULT NULL, team INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE games_achievement (id INT AUTO_INCREMENT NOT NULL, game_id INT DEFAULT NULL, name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, description VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, svg VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, congratulation_text VARCHAR(1000) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, congratulation_img VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_FDA0B6FE48FD905 (game_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE games_achievement_game_user (id INT AUTO_INCREMENT NOT NULL, game_user_id INT NOT NULL, achievement_id INT NOT NULL, is_confirm TINYINT(1) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_E836C065B3EC99FE (achievement_id), INDEX IDX_E836C06532ACACBA (game_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE games_chapter (id INT AUTO_INCREMENT NOT NULL, game_id INT DEFAULT NULL, name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, complexity SMALLINT DEFAULT NULL, time_to_done SMALLINT DEFAULT NULL, uri VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, position_in_menu SMALLINT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_4248DAC2E48FD905 (game_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE games_chapter_part (id INT AUTO_INCREMENT NOT NULL, chapter_id INT DEFAULT NULL, position_in_chapter INT NOT NULL, has_task TINYINT(1) DEFAULT NULL, INDEX IDX_72D01364579F4768 (chapter_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE games_chapter_part_block (id INT AUTO_INCREMENT NOT NULL, chapter_part_id INT DEFAULT NULL, position_in_part SMALLINT NOT NULL, type VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, block_header VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, block_text VARCHAR(1000) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, img VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_CC27DE0736D24FA3 (chapter_part_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE games_chapter_part_block_button (id INT AUTO_INCREMENT NOT NULL, chapter_part_block_id INT DEFAULT NULL, text VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, path VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, type VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, INDEX IDX_61DCAAC564C06B48 (chapter_part_block_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE games_chapter_part_block_text_paragraph (id INT AUTO_INCREMENT NOT NULL, chapter_part_block_id INT NOT NULL, header VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, text VARCHAR(2000) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, type VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, position_in_block SMALLINT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_3DE450F064C06B48 (chapter_part_block_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE games_game (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, uri VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE games_game_product (id INT AUTO_INCREMENT NOT NULL, game_id INT NOT NULL, product_id INT NOT NULL, INDEX IDX_51A66CB54584665A (product_id), INDEX IDX_51A66CB5E48FD905 (game_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE games_game_user (id INT AUTO_INCREMENT NOT NULL, game_id INT DEFAULT NULL, referral_owner_id INT DEFAULT NULL, user_id INT DEFAULT NULL, game_user_hash VARCHAR(32) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, referral_code VARCHAR(10) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, date_of_getting_referral_owner DATETIME DEFAULT NULL, php_session VARCHAR(100) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, ip_address BIGINT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_4940C185AC68C458 (referral_owner_id), INDEX IDX_4940C185E48FD905 (game_id), INDEX IDX_4940C185A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE games_game_user_chapter_part_finish (id INT AUTO_INCREMENT NOT NULL, game_user_id INT NOT NULL, chapter_part_id INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_112BC80036D24FA3 (chapter_part_id), INDEX IDX_112BC80032ACACBA (game_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE games_game_user_chapter_progress (id INT AUTO_INCREMENT NOT NULL, chapter_id INT NOT NULL, game_user_id INT NOT NULL, is_done TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_DA0434C032ACACBA (game_user_id), INDEX IDX_DA0434C0579F4768 (chapter_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE games_game_user_counter (id INT AUTO_INCREMENT NOT NULL, game_user_id INT NOT NULL, hints_left SMALLINT NOT NULL, account_balance INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_9E605AD032ACACBA (game_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE games_game_user_strategy (id INT AUTO_INCREMENT NOT NULL, game_user_id INT NOT NULL, strategy_id INT NOT NULL, INDEX IDX_CC142CF9D5CAD932 (strategy_id), INDEX IDX_CC142CF932ACACBA (game_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE games_hint (id INT AUTO_INCREMENT NOT NULL, chapter_id INT DEFAULT NULL, text VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, position_in_chapter SMALLINT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_2BBECF8A579F4768 (chapter_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE games_hint_game_user_requested (id INT AUTO_INCREMENT NOT NULL, game_user_id INT NOT NULL, hint_id INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_C9EC9793519161AB (hint_id), INDEX IDX_C9EC979332ACACBA (game_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE games_knowledge_base_chapter (id INT AUTO_INCREMENT NOT NULL, knowledge_base_id INT NOT NULL, chapter_id INT NOT NULL, INDEX IDX_6FA569A5579F4768 (chapter_id), INDEX IDX_6FA569A51620F1CE (knowledge_base_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE games_one_time_bonus (id INT AUTO_INCREMENT NOT NULL, game_id INT NOT NULL, name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, description VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, coefficient_to_referral_program NUMERIC(10, 2) DEFAULT NULL, coefficient_to_task_reward NUMERIC(10, 2) DEFAULT NULL, fix_reward INT DEFAULT NULL, INDEX IDX_8EE462A1E48FD905 (game_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE games_strategy (id INT AUTO_INCREMENT NOT NULL, game_id INT NOT NULL, name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, description VARCHAR(1000) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, coefficient_to_referral_program NUMERIC(10, 2) DEFAULT NULL, coefficient_to_task_reward NUMERIC(10, 2) DEFAULT NULL, strategy_type VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, INDEX IDX_BD4122D1E48FD905 (game_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE games_task_with_correct_answer (id INT AUTO_INCREMENT NOT NULL, chapter_part_block_id INT NOT NULL, target_chapter_id INT DEFAULT NULL, one_time_bonus_id INT DEFAULT NULL, strategy_id INT DEFAULT NULL, push_id INT DEFAULT NULL, question VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, correct_answer VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, success_message VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, reward INT DEFAULT NULL, reward_if_hint_requested INT DEFAULT NULL, error_message VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_10F6E47E65739FF9 (target_chapter_id), INDEX IDX_10F6E47ED5CAD932 (strategy_id), INDEX IDX_10F6E47EF30BCAF6 (push_id), INDEX IDX_10F6E47E64C06B48 (chapter_part_block_id), INDEX IDX_10F6E47EA7C83961 (one_time_bonus_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE games_task_with_correct_answer_answer (id INT AUTO_INCREMENT NOT NULL, task_with_correct_answer_id INT NOT NULL, game_user_id INT NOT NULL, answer VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, is_correct TINYINT(1) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_8501AD2632ACACBA (game_user_id), INDEX IDX_8501AD2633239AEA (task_with_correct_answer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE games_task_with_path_change (id INT AUTO_INCREMENT NOT NULL, chapter_part_block_id INT NOT NULL, question VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_747CFDDF64C06B48 (chapter_part_block_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE games_task_with_path_change_option (id INT AUTO_INCREMENT NOT NULL, task_with_path_change_id INT DEFAULT NULL, target_chapter_id INT DEFAULT NULL, strategy_id INT DEFAULT NULL, one_time_bonus_id INT DEFAULT NULL, push_id INT DEFAULT NULL, text VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, reward INT DEFAULT NULL, reward_if_hint_requested INT DEFAULT NULL, push_trigger_id INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_8F4DA45D65739FF9 (target_chapter_id), INDEX IDX_8F4DA45DA7C83961 (one_time_bonus_id), INDEX IDX_8F4DA45DF30BCAF6 (push_id), INDEX IDX_8F4DA45D667C07B6 (task_with_path_change_id), INDEX IDX_8F4DA45DD5CAD932 (strategy_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE knowledge_base (id INT AUTO_INCREMENT NOT NULL, term VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, definition VARCHAR(1000) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, url_more_details VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, description LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, image_url VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, price INT NOT NULL, min_price INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE push (id INT AUTO_INCREMENT NOT NULL, chapter_trigger_id INT DEFAULT NULL, header VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, description VARCHAR(1000) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, img VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, action_to_button VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, trigger_id INT DEFAULT NULL, INDEX IDX_5F3A166411A9D36F (chapter_trigger_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE push_query (id INT AUTO_INCREMENT NOT NULL, push_id INT NOT NULL, game_user_id INT NOT NULL, is_confirm TINYINT(1) NOT NULL, INDEX IDX_2A36075132ACACBA (game_user_id), INDEX IDX_2A360751F30BCAF6 (push_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE team (id INT AUTO_INCREMENT NOT NULL, course_id INT DEFAULT NULL, user_group_id INT DEFAULT NULL, captain_id INT DEFAULT NULL, leader_id INT DEFAULT NULL, name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_C4E0A61F1ED93D47 (user_group_id), INDEX IDX_C4E0A61F3346729B (captain_id), INDEX IDX_C4E0A61F591CC992 (course_id), INDEX IDX_C4E0A61F73154ED4 (leader_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE team_report_captain (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, channel VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, channel_id INT NOT NULL, INDEX IDX_49D103ACA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE team_report_cell (id INT AUTO_INCREMENT NOT NULL, team_report_user_id INT DEFAULT NULL, team_report_captain_id INT DEFAULT NULL, week_id INT NOT NULL, profit VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, INDEX IDX_539B8554408A6F62 (team_report_user_id), INDEX IDX_539B85546CDAD0FC (team_report_captain_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE team_report_user (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, team_id INT NOT NULL, channel VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, channel_id INT NOT NULL, INDEX IDX_158FD4FFA76ED395 (user_id), INDEX IDX_158FD4FF296CD8AE (team_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE team_user (id INT AUTO_INCREMENT NOT NULL, team_id INT NOT NULL, user_id INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_5C722232296CD8AE (team_id), INDEX IDX_5C722232A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE courses_answer_task_type_6 ADD CONSTRAINT FK_853ADAD08DB60186 FOREIGN KEY (task_id) REFERENCES courses_task_type_6 (id)');
        $this->addSql('ALTER TABLE courses_answer_task_type_6 ADD CONSTRAINT FK_853ADAD0A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE courses_answer_task_type_6 ADD CONSTRAINT FK_853ADAD0A8957C46 FOREIGN KEY (rated_user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE games_achievement ADD CONSTRAINT FK_FDA0B6FE48FD905 FOREIGN KEY (game_id) REFERENCES games_game (id)');
        $this->addSql('ALTER TABLE games_achievement_game_user ADD CONSTRAINT FK_E836C06532ACACBA FOREIGN KEY (game_user_id) REFERENCES games_game_user (id)');
        $this->addSql('ALTER TABLE games_achievement_game_user ADD CONSTRAINT FK_E836C065B3EC99FE FOREIGN KEY (achievement_id) REFERENCES games_achievement (id)');
        $this->addSql('ALTER TABLE games_chapter ADD CONSTRAINT FK_4248DAC2E48FD905 FOREIGN KEY (game_id) REFERENCES games_game (id)');
        $this->addSql('ALTER TABLE games_chapter_part ADD CONSTRAINT FK_72D01364579F4768 FOREIGN KEY (chapter_id) REFERENCES games_chapter (id)');
        $this->addSql('ALTER TABLE games_chapter_part_block ADD CONSTRAINT FK_CC27DE0736D24FA3 FOREIGN KEY (chapter_part_id) REFERENCES games_chapter_part (id)');
        $this->addSql('ALTER TABLE games_chapter_part_block_button ADD CONSTRAINT FK_61DCAAC564C06B48 FOREIGN KEY (chapter_part_block_id) REFERENCES games_chapter_part_block (id)');
        $this->addSql('ALTER TABLE games_chapter_part_block_text_paragraph ADD CONSTRAINT FK_3DE450F064C06B48 FOREIGN KEY (chapter_part_block_id) REFERENCES games_chapter_part_block (id)');
        $this->addSql('ALTER TABLE games_game_product ADD CONSTRAINT FK_51A66CB54584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE games_game_product ADD CONSTRAINT FK_51A66CB5E48FD905 FOREIGN KEY (game_id) REFERENCES games_game (id)');
        $this->addSql('ALTER TABLE games_game_user ADD CONSTRAINT FK_4940C185A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE games_game_user ADD CONSTRAINT FK_4940C185AC68C458 FOREIGN KEY (referral_owner_id) REFERENCES games_game_user (id)');
        $this->addSql('ALTER TABLE games_game_user ADD CONSTRAINT FK_4940C185E48FD905 FOREIGN KEY (game_id) REFERENCES games_game (id)');
        $this->addSql('ALTER TABLE games_game_user_chapter_part_finish ADD CONSTRAINT FK_112BC80032ACACBA FOREIGN KEY (game_user_id) REFERENCES games_game_user (id)');
        $this->addSql('ALTER TABLE games_game_user_chapter_part_finish ADD CONSTRAINT FK_112BC80036D24FA3 FOREIGN KEY (chapter_part_id) REFERENCES games_chapter_part (id)');
        $this->addSql('ALTER TABLE games_game_user_chapter_progress ADD CONSTRAINT FK_DA0434C032ACACBA FOREIGN KEY (game_user_id) REFERENCES games_game_user (id)');
        $this->addSql('ALTER TABLE games_game_user_chapter_progress ADD CONSTRAINT FK_DA0434C0579F4768 FOREIGN KEY (chapter_id) REFERENCES games_chapter (id)');
        $this->addSql('ALTER TABLE games_game_user_counter ADD CONSTRAINT FK_9E605AD032ACACBA FOREIGN KEY (game_user_id) REFERENCES games_game_user (id)');
        $this->addSql('ALTER TABLE games_game_user_strategy ADD CONSTRAINT FK_CC142CF932ACACBA FOREIGN KEY (game_user_id) REFERENCES games_game_user (id)');
        $this->addSql('ALTER TABLE games_game_user_strategy ADD CONSTRAINT FK_CC142CF9D5CAD932 FOREIGN KEY (strategy_id) REFERENCES games_strategy (id)');
        $this->addSql('ALTER TABLE games_hint ADD CONSTRAINT FK_2BBECF8A579F4768 FOREIGN KEY (chapter_id) REFERENCES games_chapter (id)');
        $this->addSql('ALTER TABLE games_hint_game_user_requested ADD CONSTRAINT FK_C9EC979332ACACBA FOREIGN KEY (game_user_id) REFERENCES games_game_user (id)');
        $this->addSql('ALTER TABLE games_hint_game_user_requested ADD CONSTRAINT FK_C9EC9793519161AB FOREIGN KEY (hint_id) REFERENCES games_hint (id)');
        $this->addSql('ALTER TABLE games_knowledge_base_chapter ADD CONSTRAINT FK_6FA569A51620F1CE FOREIGN KEY (knowledge_base_id) REFERENCES knowledge_base (id)');
        $this->addSql('ALTER TABLE games_knowledge_base_chapter ADD CONSTRAINT FK_6FA569A5579F4768 FOREIGN KEY (chapter_id) REFERENCES games_chapter (id)');
        $this->addSql('ALTER TABLE games_one_time_bonus ADD CONSTRAINT FK_8EE462A1E48FD905 FOREIGN KEY (game_id) REFERENCES games_game (id)');
        $this->addSql('ALTER TABLE games_strategy ADD CONSTRAINT FK_BD4122D1E48FD905 FOREIGN KEY (game_id) REFERENCES games_game (id)');
        $this->addSql('ALTER TABLE games_task_with_correct_answer ADD CONSTRAINT FK_10F6E47E64C06B48 FOREIGN KEY (chapter_part_block_id) REFERENCES games_chapter_part_block (id)');
        $this->addSql('ALTER TABLE games_task_with_correct_answer ADD CONSTRAINT FK_10F6E47E65739FF9 FOREIGN KEY (target_chapter_id) REFERENCES games_chapter (id)');
        $this->addSql('ALTER TABLE games_task_with_correct_answer ADD CONSTRAINT FK_10F6E47EA7C83961 FOREIGN KEY (one_time_bonus_id) REFERENCES games_one_time_bonus (id)');
        $this->addSql('ALTER TABLE games_task_with_correct_answer ADD CONSTRAINT FK_10F6E47ED5CAD932 FOREIGN KEY (strategy_id) REFERENCES games_strategy (id)');
        $this->addSql('ALTER TABLE games_task_with_correct_answer ADD CONSTRAINT FK_10F6E47EF30BCAF6 FOREIGN KEY (push_id) REFERENCES push (id)');
        $this->addSql('ALTER TABLE games_task_with_correct_answer_answer ADD CONSTRAINT FK_8501AD2632ACACBA FOREIGN KEY (game_user_id) REFERENCES games_game_user (id)');
        $this->addSql('ALTER TABLE games_task_with_correct_answer_answer ADD CONSTRAINT FK_8501AD2633239AEA FOREIGN KEY (task_with_correct_answer_id) REFERENCES games_task_with_correct_answer (id)');
        $this->addSql('ALTER TABLE games_task_with_path_change ADD CONSTRAINT FK_747CFDDF64C06B48 FOREIGN KEY (chapter_part_block_id) REFERENCES games_chapter_part_block (id)');
        $this->addSql('ALTER TABLE games_task_with_path_change_option ADD CONSTRAINT FK_8F4DA45D65739FF9 FOREIGN KEY (target_chapter_id) REFERENCES games_chapter (id)');
        $this->addSql('ALTER TABLE games_task_with_path_change_option ADD CONSTRAINT FK_8F4DA45D667C07B6 FOREIGN KEY (task_with_path_change_id) REFERENCES games_task_with_path_change (id)');
        $this->addSql('ALTER TABLE games_task_with_path_change_option ADD CONSTRAINT FK_8F4DA45DA7C83961 FOREIGN KEY (one_time_bonus_id) REFERENCES games_one_time_bonus (id)');
        $this->addSql('ALTER TABLE games_task_with_path_change_option ADD CONSTRAINT FK_8F4DA45DD5CAD932 FOREIGN KEY (strategy_id) REFERENCES games_strategy (id)');
        $this->addSql('ALTER TABLE games_task_with_path_change_option ADD CONSTRAINT FK_8F4DA45DF30BCAF6 FOREIGN KEY (push_id) REFERENCES push (id)');
        $this->addSql('ALTER TABLE push ADD CONSTRAINT FK_5F3A166411A9D36F FOREIGN KEY (chapter_trigger_id) REFERENCES games_chapter (id)');
        $this->addSql('ALTER TABLE push_query ADD CONSTRAINT FK_2A36075132ACACBA FOREIGN KEY (game_user_id) REFERENCES games_game_user (id)');
        $this->addSql('ALTER TABLE push_query ADD CONSTRAINT FK_2A360751F30BCAF6 FOREIGN KEY (push_id) REFERENCES push (id)');
        $this->addSql('ALTER TABLE team ADD CONSTRAINT FK_C4E0A61F1ED93D47 FOREIGN KEY (user_group_id) REFERENCES user_group (id)');
        $this->addSql('ALTER TABLE team ADD CONSTRAINT FK_C4E0A61F3346729B FOREIGN KEY (captain_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE team ADD CONSTRAINT FK_C4E0A61F591CC992 FOREIGN KEY (course_id) REFERENCES courses_course (id)');
        $this->addSql('ALTER TABLE team ADD CONSTRAINT FK_C4E0A61F73154ED4 FOREIGN KEY (leader_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE team_report_captain ADD CONSTRAINT FK_49D103ACA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE team_report_cell ADD CONSTRAINT FK_539B8554408A6F62 FOREIGN KEY (team_report_user_id) REFERENCES team_report_user (id)');
        $this->addSql('ALTER TABLE team_report_cell ADD CONSTRAINT FK_539B85546CDAD0FC FOREIGN KEY (team_report_captain_id) REFERENCES team_report_captain (id)');
        $this->addSql('ALTER TABLE team_report_user ADD CONSTRAINT FK_158FD4FF296CD8AE FOREIGN KEY (team_id) REFERENCES team (id)');
        $this->addSql('ALTER TABLE team_report_user ADD CONSTRAINT FK_158FD4FFA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE team_user ADD CONSTRAINT FK_5C722232296CD8AE FOREIGN KEY (team_id) REFERENCES team (id)');
        $this->addSql('ALTER TABLE team_user ADD CONSTRAINT FK_5C722232A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE courses_answer_task_type_5 ADD money INT DEFAULT NULL, ADD deals INT DEFAULT NULL, ADD romi INT DEFAULT NULL, ADD nps INT DEFAULT NULL, ADD analytics INT DEFAULT NULL, ADD sales INT DEFAULT NULL, ADD marketing INT DEFAULT NULL, ADD team INT DEFAULT NULL');
        $this->addSql('ALTER TABLE courses_task_test_question_answer ADD money INT DEFAULT NULL, ADD deals INT DEFAULT NULL, ADD romi INT DEFAULT NULL, ADD nps INT DEFAULT NULL, ADD analytics INT DEFAULT NULL, ADD sales INT DEFAULT NULL, ADD marketing INT DEFAULT NULL, ADD team INT DEFAULT NULL');
        $this->addSql('ALTER TABLE courses_task_type_1 ADD money INT DEFAULT NULL, ADD deals INT DEFAULT NULL, ADD romi INT DEFAULT NULL, ADD nps INT DEFAULT NULL, ADD analytics INT DEFAULT NULL, ADD sales INT DEFAULT NULL, ADD marketing INT DEFAULT NULL, ADD team INT DEFAULT NULL');
        $this->addSql('ALTER TABLE courses_task_type_5 ADD money INT DEFAULT NULL, ADD deals INT DEFAULT NULL, ADD romi INT DEFAULT NULL, ADD nps INT DEFAULT NULL, ADD analytics INT DEFAULT NULL, ADD sales INT DEFAULT NULL, ADD marketing INT DEFAULT NULL, ADD team INT DEFAULT NULL, ADD add_cor_ans_money INT DEFAULT NULL, ADD add_cor_ans_deals INT DEFAULT NULL, ADD add_cor_ans_romi INT DEFAULT NULL, ADD add_cor_ans_nps INT DEFAULT NULL, ADD add_cor_ans_analytics INT DEFAULT NULL, ADD add_cor_ans_sales INT DEFAULT NULL, ADD add_cor_ans_marketing INT DEFAULT NULL, ADD add_cor_ans_team INT DEFAULT NULL');
    }
}

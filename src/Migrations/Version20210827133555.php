<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210827133555 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE traction_value_course_stream');
        $this->addSql('ALTER TABLE traction_value MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE traction_value DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE traction_value ADD value INT DEFAULT NULL, DROP id');
        $this->addSql('ALTER TABLE traction_value ADD PRIMARY KEY (date, type_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE traction_value_course_stream (value_id INT NOT NULL, course_stream_id INT NOT NULL, INDEX IDX_E3A6139AF920BBA2 (value_id), INDEX IDX_E3A6139A524C0692 (course_stream_id), PRIMARY KEY(value_id, course_stream_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE traction_value_course_stream ADD CONSTRAINT FK_E3A6139A524C0692 FOREIGN KEY (course_stream_id) REFERENCES course_stream (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE traction_value_course_stream ADD CONSTRAINT FK_E3A6139AF920BBA2 FOREIGN KEY (value_id) REFERENCES traction_value (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE traction_value ADD id INT AUTO_INCREMENT NOT NULL, DROP value, DROP PRIMARY KEY, ADD PRIMARY KEY (id)');
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211109073040 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->connection->beginTransaction();

        $this->connection->createQueryBuilder()
            ->delete('user_group_user')
            ->where('user_group_id >= :from')
            ->andWhere('user_group_id <= :to')
            ->setParameter('from', 885)
            ->setParameter('to', 921)
            ->execute();


        $this->connection->createQueryBuilder()
            ->delete('user_group_user')
            ->where('user_group_id >= :from')
            ->andWhere('user_group_id <= :to')
            ->setParameter('from', 925)
            ->setParameter('to', 961)
            ->execute();


        $this->connection->createQueryBuilder()
            ->delete('user_group')
            ->where('id >= :from')
            ->andWhere('id <= :to')
            ->setParameter('from', 885)
            ->setParameter('to', 921)
            ->execute();

        $this->connection->createQueryBuilder()
            ->delete('user_group')
            ->where('id >= :from')
            ->andWhere('id <= :to')
            ->setParameter('from', 925)
            ->setParameter('to', 961)
            ->execute();

        $this->connection->commit();
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
    }

}

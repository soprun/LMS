<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210616132845 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE abstract_course (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE course_stream (id INT AUTO_INCREMENT NOT NULL, abstract_course_id INT NOT NULL, stream INT NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_5ED17F518C8C2552 (abstract_course_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE course_stream ADD CONSTRAINT FK_5ED17F518C8C2552 FOREIGN KEY (abstract_course_id) REFERENCES abstract_course (id)');
        $this->addSql('ALTER TABLE traction_hundred ADD course_stream_id INT DEFAULT NULL AFTER week_id');
        $this->addSql('ALTER TABLE traction_hundred ADD CONSTRAINT FK_AB9340BB524C0692 FOREIGN KEY (course_stream_id) REFERENCES course_stream (id)');
        $this->addSql('CREATE INDEX IDX_AB9340BB524C0692 ON traction_hundred (course_stream_id)');
        $this->addSql('ALTER TABLE traction_speed ADD course_stream_id INT DEFAULT NULL AFTER week_id');
        $this->addSql('ALTER TABLE traction_speed ADD CONSTRAINT FK_6BED7873524C0692 FOREIGN KEY (course_stream_id) REFERENCES course_stream (id)');
        $this->addSql('CREATE INDEX IDX_6BED7873524C0692 ON traction_speed (course_stream_id)');
        $this->addSql('ALTER TABLE user_group ADD course_stream_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_group ADD CONSTRAINT FK_8F02BF9D524C0692 FOREIGN KEY (course_stream_id) REFERENCES course_stream (id)');
        $this->addSql('CREATE INDEX IDX_8F02BF9D524C0692 ON user_group (course_stream_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE course_stream DROP FOREIGN KEY FK_5ED17F518C8C2552');
        $this->addSql('ALTER TABLE traction_hundred DROP FOREIGN KEY FK_AB9340BB524C0692');
        $this->addSql('ALTER TABLE traction_speed DROP FOREIGN KEY FK_6BED7873524C0692');
        $this->addSql('ALTER TABLE user_group DROP FOREIGN KEY FK_8F02BF9D524C0692');
        $this->addSql('DROP TABLE abstract_course');
        $this->addSql('DROP TABLE course_stream');
        $this->addSql('DROP INDEX IDX_AB9340BB524C0692 ON traction_hundred');
        $this->addSql('ALTER TABLE traction_hundred DROP course_stream_id');
        $this->addSql('DROP INDEX IDX_6BED7873524C0692 ON traction_speed');
        $this->addSql('ALTER TABLE traction_speed DROP course_stream_id');
        $this->addSql('DROP INDEX IDX_8F02BF9D524C0692 ON user_group');
        $this->addSql('ALTER TABLE user_group DROP course_stream_id');
    }
}

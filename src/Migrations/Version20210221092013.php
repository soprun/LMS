<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210221092013 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE traction_hundred (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, channel VARCHAR(255) NOT NULL, channel_id INT NOT NULL, week_id INT NOT NULL, INDEX IDX_AB9340BBA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE traction_hundred_cell (id INT AUTO_INCREMENT NOT NULL, traction_hundred_id INT NOT NULL, day_id INT NOT NULL, profit INT DEFAULT NULL, INDEX IDX_D807F54A36355991 (traction_hundred_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE traction_speed (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, channel VARCHAR(255) NOT NULL, channel_id INT NOT NULL, week_id INT NOT NULL, INDEX IDX_6BED7873A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE traction_speed_cell (id INT AUTO_INCREMENT NOT NULL, traction_speed_id INT NOT NULL, day_id INT NOT NULL, profit INT DEFAULT NULL, INDEX IDX_A168140A9AB7E69 (traction_speed_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE traction_hundred ADD CONSTRAINT FK_AB9340BBA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE traction_hundred_cell ADD CONSTRAINT FK_D807F54A36355991 FOREIGN KEY (traction_hundred_id) REFERENCES traction_hundred (id)');
        $this->addSql('ALTER TABLE traction_speed ADD CONSTRAINT FK_6BED7873A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE traction_speed_cell ADD CONSTRAINT FK_A168140A9AB7E69 FOREIGN KEY (traction_speed_id) REFERENCES traction_speed (id)');
        $this->addSql('ALTER TABLE report_msg_cell RENAME INDEX idx_5bf30397add8213f TO IDX_B65D06CEADD8213F');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE traction_hundred_cell DROP FOREIGN KEY FK_D807F54A36355991');
        $this->addSql('ALTER TABLE traction_speed_cell DROP FOREIGN KEY FK_A168140A9AB7E69');
        $this->addSql('DROP TABLE traction_hundred');
        $this->addSql('DROP TABLE traction_hundred_cell');
        $this->addSql('DROP TABLE traction_speed');
        $this->addSql('DROP TABLE traction_speed_cell');
        $this->addSql('ALTER TABLE report_msg_cell RENAME INDEX idx_b65d06ceadd8213f TO IDX_5BF30397ADD8213F');
    }
}

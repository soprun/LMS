<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211210160849 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE business_niche ADD main tinyint(1) CHECK (main = true OR main IS NULL)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2826006A76ED395BF28CD64 ON business_niche (user_id, main)');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE business_niche DROP main');
        $this->addSql('DROP INDEX UNIQ_2826006A76ED395BF28CD64 ON business_niche');
    }
}

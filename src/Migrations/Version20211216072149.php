<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211216072149 extends AbstractMigration
{
    private const SPEED_SLUG = 'speed';
    private const SPEED_CLUB_SLUG = 'speed_club';
    private const HUNDRED_SLUG = 'hundred';
    private const MI_SLUG = 'mi';
    private const MSA_LITE_SLUG = 'msa_lite';
    private const MSA_SLUG = 'msa';

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $expr = $this->connection->createQueryBuilder()->expr();

        $this->connection->beginTransaction();

        $abstractCourses = $this->connection->createQueryBuilder()->select('id', 'slug')
            ->from('abstract_course')
            ->execute()->fetchAll();

        foreach ($abstractCourses as $abstractCourse) {
            $serial = null;
            if (self::SPEED_SLUG === $abstractCourse['slug']) {
                $serial = '№ ';
            } elseif (self::SPEED_CLUB_SLUG === $abstractCourse['slug']) {
                $serial = '№ СК-';
            } elseif (self::HUNDRED_SLUG === $abstractCourse['slug']) {
                $serial = '№ С-';
            } elseif (self::MI_SLUG === $abstractCourse['slug']) {
                $serial = '№ МИ-';
            } elseif (self::MSA_LITE_SLUG === $abstractCourse['slug']) {
                $serial = '№ Л-';
            } elseif (self::MSA_SLUG === $abstractCourse['slug']) {
                $serial = '№ МСА-';
            }
            $this->connection->createQueryBuilder()->update('abstract_course')
                ->set('cert_serial', $expr->literal($serial))
                ->where('id = :id')
                ->setParameter('id', $abstractCourse['id'])
                ->execute();
        }

        $this->connection->commit();
    }

    public function down(Schema $schema): void
    {
    }

}

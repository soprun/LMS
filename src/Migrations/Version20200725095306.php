<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200725095306 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE courses_task_type_5 (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) DEFAULT NULL, description VARCHAR(2000) DEFAULT NULL, ddl DATETIME DEFAULT NULL, correct_answers VARCHAR(1000) NOT NULL, additional_correct_answers VARCHAR(1000) DEFAULT NULL, points_before_ddl SMALLINT DEFAULT NULL, points_after_ddl SMALLINT DEFAULT NULL, add_cor_ans_points_before_ddl SMALLINT DEFAULT NULL, add_cor_ans_points_after_ddl SMALLINT DEFAULT NULL, money INT DEFAULT NULL, deals INT DEFAULT NULL, romi INT DEFAULT NULL, nps INT DEFAULT NULL, analytics INT DEFAULT NULL, sales INT DEFAULT NULL, marketing INT DEFAULT NULL, team INT DEFAULT NULL, add_cor_ans_money INT DEFAULT NULL, add_cor_ans_deals INT DEFAULT NULL, add_cor_ans_romi INT DEFAULT NULL, add_cor_ans_nps INT DEFAULT NULL, add_cor_ans_analytics INT DEFAULT NULL, add_cor_ans_sales INT DEFAULT NULL, add_cor_ans_marketing INT DEFAULT NULL, add_cor_ans_team INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE courses_task_type_5');
    }
}

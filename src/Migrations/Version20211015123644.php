<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211015123644 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');


        $this->addSql('ALTER TABLE courses_course ADD deleted TINYINT(1) NOT NULL DEFAULT 0');
        $this->addSql('UPDATE courses_course SET deleted = IFNULL(is_delete, 0) WHERE TRUE');

        $this->addSql('ALTER TABLE courses_course_folder ADD deleted TINYINT(1) NOT NULL DEFAULT 0');
        $this->addSql('UPDATE courses_course_folder SET deleted = IFNULL(is_delete, 0) WHERE TRUE');

        $this->addSql('ALTER TABLE courses_lesson ADD deleted TINYINT(1) NOT NULL DEFAULT 0');
        $this->addSql('UPDATE courses_lesson SET deleted = IFNULL(is_delete, 0) WHERE TRUE');

        $this->addSql('ALTER TABLE courses_lesson_part_block ADD deleted TINYINT(1) NOT NULL DEFAULT 0');
        $this->addSql('UPDATE courses_lesson_part_block SET deleted = IFNULL(is_deleted, 0) WHERE TRUE');

        $this->addSql('ALTER TABLE courses_task_set ADD deleted TINYINT(1) NOT NULL DEFAULT 0');
        $this->addSql('UPDATE courses_task_set SET deleted = IFNULL(is_deleted, 0) WHERE TRUE');

        $this->addSql('ALTER TABLE courses_task_test_question ADD deleted TINYINT(1) NOT NULL DEFAULT 0');
        $this->addSql('UPDATE courses_task_test_question SET deleted = IFNULL(is_deleted, 0) WHERE TRUE');

        $this->addSql('ALTER TABLE courses_task_test_question_answer ADD deleted TINYINT(1) NOT NULL DEFAULT 0');
        $this->addSql('UPDATE courses_task_test_question_answer SET deleted = IFNULL(is_deleted, 0) WHERE TRUE');

        $this->addSql('ALTER TABLE courses_task_test_result ADD deleted TINYINT(1) NOT NULL DEFAULT 0');
        $this->addSql('UPDATE courses_task_test_result SET deleted = IFNULL(is_deleted, 0) WHERE TRUE');

        $this->addSql('ALTER TABLE partner ADD deleted TINYINT(1) NOT NULL DEFAULT 0');
        $this->addSql('UPDATE partner SET deleted = IFNULL(is_deleted, 0) WHERE TRUE');

        $this->addSql('ALTER TABLE points_shop_product ADD deleted TINYINT(1) NOT NULL DEFAULT 0');
        $this->addSql('UPDATE points_shop_product SET deleted = IFNULL(is_deleted, 0) WHERE TRUE');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE courses_course DROP deleted');
        $this->addSql('ALTER TABLE courses_course_folder DROP deleted');
        $this->addSql('ALTER TABLE courses_lesson DROP deleted');
        $this->addSql('ALTER TABLE courses_lesson_part_block DROP deleted');
        $this->addSql('ALTER TABLE courses_task_set DROP deleted');
        $this->addSql('ALTER TABLE courses_task_test_question DROP deleted');
        $this->addSql('ALTER TABLE courses_task_test_question_answer DROP deleted');
        $this->addSql('ALTER TABLE courses_task_test_result DROP deleted');
        $this->addSql('ALTER TABLE partner DROP deleted');
        $this->addSql('ALTER TABLE points_shop_product DROP deleted');
    }
}

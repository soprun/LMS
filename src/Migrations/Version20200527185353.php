<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200527185353 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE courses_task_set ADD created_at DATETIME DEFAULT "2020-01-01", ADD updated_at DATETIME DEFAULT "2020-01-01"');
        $this->addSql('ALTER TABLE courses_task_type_1 ADD created_at DATETIME DEFAULT "2020-01-01", ADD updated_at DATETIME DEFAULT "2020-01-01"');
        $this->addSql('ALTER TABLE courses_answer_task_type_1 ADD created_at DATETIME DEFAULT "2020-01-01", ADD updated_at DATETIME DEFAULT "2020-01-01"');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE courses_answer_task_type_1 DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE courses_task_set DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE courses_task_type_1 DROP created_at, DROP updated_at');
    }
}

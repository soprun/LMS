<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210709230250 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE traction_marathon_investment (id INT AUTO_INCREMENT NOT NULL, stream_id INT NOT NULL, user_id INT NOT NULL, week INT NOT NULL, planned INT DEFAULT NULL, passed INT DEFAULT NULL, amount INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_FFB1C871D0ED463E (stream_id), INDEX IDX_FFB1C871A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE traction_marathon_investment_configuration (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, stream_id INT NOT NULL, point_a INT NOT NULL, profit INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_CF46563AA76ED395 (user_id), INDEX IDX_CF46563AD0ED463E (stream_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE traction_target (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, url VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_target (user_id INT NOT NULL, target_id INT NOT NULL, INDEX IDX_233D34C1A76ED395 (user_id), INDEX IDX_233D34C1158E0B66 (target_id), PRIMARY KEY(user_id, target_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_niche (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, title VARCHAR(100) NOT NULL, description VARCHAR(300) NOT NULL, rival TINYINT(1) NOT NULL, marked TINYINT(1) NOT NULL, main TINYINT(1) NOT NULL, INDEX IDX_9EA17CE1A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_niche_comment (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, niche_id INT NOT NULL, text VARCHAR(300) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_38F8778BA76ED395 (user_id), INDEX IDX_38F8778BCB73EBD (niche_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE traction_marathon_investment ADD CONSTRAINT FK_FFB1C871D0ED463E FOREIGN KEY (stream_id) REFERENCES course_stream (id)');
        $this->addSql('ALTER TABLE traction_marathon_investment ADD CONSTRAINT FK_FFB1C871A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE traction_marathon_investment_configuration ADD CONSTRAINT FK_CF46563AA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE traction_marathon_investment_configuration ADD CONSTRAINT FK_CF46563AD0ED463E FOREIGN KEY (stream_id) REFERENCES course_stream (id)');
        $this->addSql('ALTER TABLE user_target ADD CONSTRAINT FK_233D34C1A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_target ADD CONSTRAINT FK_233D34C1158E0B66 FOREIGN KEY (target_id) REFERENCES traction_target (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_niche ADD CONSTRAINT FK_9EA17CE1A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_niche_comment ADD CONSTRAINT FK_38F8778BA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_niche_comment ADD CONSTRAINT FK_38F8778BCB73EBD FOREIGN KEY (niche_id) REFERENCES user_niche (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_target DROP FOREIGN KEY FK_233D34C1158E0B66');
        $this->addSql('ALTER TABLE user_niche_comment DROP FOREIGN KEY FK_38F8778BCB73EBD');
        $this->addSql('DROP TABLE traction_marathon_investment');
        $this->addSql('DROP TABLE traction_marathon_investment_configuration');
        $this->addSql('DROP TABLE traction_target');
        $this->addSql('DROP TABLE user_target');
        $this->addSql('DROP TABLE user_niche');
        $this->addSql('DROP TABLE user_niche_comment');
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200528174418 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE answer_task_type1_base_files (answer_task_type1_id INT NOT NULL, base_files_id INT NOT NULL, INDEX IDX_D3C95D92B1C96C86 (answer_task_type1_id), INDEX IDX_D3C95D92E90DF8D7 (base_files_id), PRIMARY KEY(answer_task_type1_id, base_files_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE answer_task_type1_base_files ADD CONSTRAINT FK_D3C95D92B1C96C86 FOREIGN KEY (answer_task_type1_id) REFERENCES courses_answer_task_type_1 (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE answer_task_type1_base_files ADD CONSTRAINT FK_D3C95D92E90DF8D7 FOREIGN KEY (base_files_id) REFERENCES base_files (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE courses_answer_task_type_1 ADD is_file_allow TINYINT(1) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE answer_task_type1_base_files');
        $this->addSql('ALTER TABLE courses_answer_task_type_1 DROP is_file_allow');
    }
}

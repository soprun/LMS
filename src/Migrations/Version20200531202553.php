<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200531202553 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user_group_course_folder (user_group_id INT NOT NULL, course_folder_id INT NOT NULL, INDEX IDX_37B069B01ED93D47 (user_group_id), INDEX IDX_37B069B0948DF9EE (course_folder_id), PRIMARY KEY(user_group_id, course_folder_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_group_course_folder ADD CONSTRAINT FK_37B069B01ED93D47 FOREIGN KEY (user_group_id) REFERENCES user_group (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_group_course_folder ADD CONSTRAINT FK_37B069B0948DF9EE FOREIGN KEY (course_folder_id) REFERENCES courses_course_folder (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE courses_course_admin');
        $this->addSql('DROP TABLE courses_course_folder_admin');
        $this->addSql('ALTER TABLE group_permission DROP FOREIGN KEY FK_3784F3181ED93D47');
        $this->addSql('DROP INDEX IDX_3784F3181ED93D47 ON group_permission');
        $this->addSql('ALTER TABLE group_permission DROP user_group_id');
        $this->addSql('ALTER TABLE user_group ADD group_permissions_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_group ADD CONSTRAINT FK_8F02BF9D4B386F5 FOREIGN KEY (group_permissions_id) REFERENCES group_permission (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8F02BF9D4B386F5 ON user_group (group_permissions_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE courses_course_admin (course_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_1FE2C51B591CC992 (course_id), INDEX IDX_1FE2C51BA76ED395 (user_id), PRIMARY KEY(course_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE courses_course_folder_admin (course_folder_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_3FA33484948DF9EE (course_folder_id), INDEX IDX_3FA33484A76ED395 (user_id), PRIMARY KEY(course_folder_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE courses_course_admin ADD CONSTRAINT FK_1FE2C51B591CC992 FOREIGN KEY (course_id) REFERENCES courses_course (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE courses_course_admin ADD CONSTRAINT FK_1FE2C51BA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE courses_course_folder_admin ADD CONSTRAINT FK_3FA33484948DF9EE FOREIGN KEY (course_folder_id) REFERENCES courses_course_folder (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE courses_course_folder_admin ADD CONSTRAINT FK_3FA33484A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE user_group_course_folder');
        $this->addSql('ALTER TABLE group_permission ADD user_group_id INT NOT NULL');
        $this->addSql('ALTER TABLE group_permission ADD CONSTRAINT FK_3784F3181ED93D47 FOREIGN KEY (user_group_id) REFERENCES user_group (id)');
        $this->addSql('CREATE INDEX IDX_3784F3181ED93D47 ON group_permission (user_group_id)');
        $this->addSql('ALTER TABLE user_group DROP FOREIGN KEY FK_8F02BF9D4B386F5');
        $this->addSql('DROP INDEX UNIQ_8F02BF9D4B386F5 ON user_group');
        $this->addSql('ALTER TABLE user_group DROP group_permissions_id');
    }
}

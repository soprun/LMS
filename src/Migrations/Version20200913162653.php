<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200913162653 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE report_msg (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, channel VARCHAR(255) NOT NULL, channel_id INT NOT NULL, period INT NOT NULL, year INT NOT NULL, INDEX IDX_98712728A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE report_msg_cell (id INT AUTO_INCREMENT NOT NULL, report_msg_id INT NOT NULL, week_id INT NOT NULL, profit INT DEFAULT NULL, INDEX IDX_5BF30397ADD8213F (report_msg_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_course_tariff (id INT AUTO_INCREMENT NOT NULL, course_id INT NOT NULL, manager_id INT DEFAULT NULL, tracker_id INT DEFAULT NULL, user_id INT NOT NULL, tariff_id INT NOT NULL, INDEX IDX_D4D34C60591CC992 (course_id), INDEX IDX_D4D34C60783E3463 (manager_id), INDEX IDX_D4D34C60FB5230B (tracker_id), INDEX IDX_D4D34C60A76ED395 (user_id), INDEX IDX_D4D34C6092348FD2 (tariff_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_tariff (id INT AUTO_INCREMENT NOT NULL, course_id INT NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_F1373B40591CC992 (course_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE report_msg ADD CONSTRAINT FK_98712728A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE report_msg_cell ADD CONSTRAINT FK_5BF30397ADD8213F FOREIGN KEY (report_msg_id) REFERENCES report_msg (id)');
        $this->addSql('ALTER TABLE user_course_tariff ADD CONSTRAINT FK_D4D34C60591CC992 FOREIGN KEY (course_id) REFERENCES courses_course (id)');
        $this->addSql('ALTER TABLE user_course_tariff ADD CONSTRAINT FK_D4D34C60783E3463 FOREIGN KEY (manager_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_course_tariff ADD CONSTRAINT FK_D4D34C60FB5230B FOREIGN KEY (tracker_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_course_tariff ADD CONSTRAINT FK_D4D34C60A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_course_tariff ADD CONSTRAINT FK_D4D34C6092348FD2 FOREIGN KEY (tariff_id) REFERENCES user_tariff (id)');
        $this->addSql('ALTER TABLE user_tariff ADD CONSTRAINT FK_F1373B40591CC992 FOREIGN KEY (course_id) REFERENCES courses_course (id)');
        $this->addSql('ALTER TABLE team_report_cell CHANGE weak_id week_id INT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE report_msg_cell DROP FOREIGN KEY FK_5BF30397ADD8213F');
        $this->addSql('ALTER TABLE user_course_tariff DROP FOREIGN KEY FK_D4D34C6092348FD2');
        $this->addSql('DROP TABLE report_msg');
        $this->addSql('DROP TABLE report_msg_cell');
        $this->addSql('DROP TABLE user_course_tariff');
        $this->addSql('DROP TABLE user_tariff');
        $this->addSql('ALTER TABLE team_report_cell CHANGE week_id weak_id INT NOT NULL');
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211007163433 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_certificates ADD course_stream_id INT DEFAULT NULL, CHANGE course_id course_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_certificates ADD CONSTRAINT FK_DEA875D524C0692 FOREIGN KEY (course_stream_id) REFERENCES course_stream (id)');
        $this->addSql('CREATE INDEX IDX_DEA875D524C0692 ON user_certificates (course_stream_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_certificates DROP FOREIGN KEY FK_DEA875D524C0692');
        $this->addSql('DROP INDEX IDX_DEA875D524C0692 ON user_certificates');
        $this->addSql('ALTER TABLE user_certificates DROP course_stream_id, CHANGE course_id course_id INT NOT NULL');
    }
}

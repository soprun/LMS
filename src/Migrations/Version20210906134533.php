<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210906134533 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE question_course_stream (question_id INT NOT NULL, course_stream_id INT NOT NULL, INDEX IDX_C690B5161E27F6BF (question_id), INDEX IDX_C690B516524C0692 (course_stream_id), PRIMARY KEY(question_id, course_stream_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE question_course_stream ADD CONSTRAINT FK_C690B5161E27F6BF FOREIGN KEY (question_id) REFERENCES questionnaire_question (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE question_course_stream ADD CONSTRAINT FK_C690B516524C0692 FOREIGN KEY (course_stream_id) REFERENCES course_stream (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE questionnaire_user_answer ADD course_stream_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE questionnaire_user_answer ADD CONSTRAINT FK_36AB8B38524C0692 FOREIGN KEY (course_stream_id) REFERENCES course_stream (id)');
        $this->addSql('CREATE INDEX IDX_36AB8B38524C0692 ON questionnaire_user_answer (course_stream_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE question_course_stream');
        $this->addSql('ALTER TABLE questionnaire_user_answer DROP FOREIGN KEY FK_36AB8B38524C0692');
        $this->addSql('DROP INDEX IDX_36AB8B38524C0692 ON questionnaire_user_answer');
        $this->addSql('ALTER TABLE questionnaire_user_answer DROP course_stream_id');
    }
}

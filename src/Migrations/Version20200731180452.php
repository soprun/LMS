<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200731180452 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE courses_course ADD cover_img_file_id INT DEFAULT NULL AFTER img_file_id');
        $this->addSql('ALTER TABLE courses_course ADD CONSTRAINT FK_49B7FACF5387784A FOREIGN KEY (cover_img_file_id) REFERENCES base_files (id)');
        $this->addSql('CREATE INDEX IDX_49B7FACF5387784A ON courses_course (cover_img_file_id)');
        $this->addSql('ALTER TABLE courses_course_folder ADD cover_img_file_id INT DEFAULT NULL AFTER img_file_id');
        $this->addSql('ALTER TABLE courses_course_folder ADD CONSTRAINT FK_DF36F8E05387784A FOREIGN KEY (cover_img_file_id) REFERENCES base_files (id)');
        $this->addSql('CREATE INDEX IDX_DF36F8E05387784A ON courses_course_folder (cover_img_file_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE courses_course DROP FOREIGN KEY FK_49B7FACF5387784A');
        $this->addSql('DROP INDEX IDX_49B7FACF5387784A ON courses_course');
        $this->addSql('ALTER TABLE courses_course DROP cover_img_file_id');
        $this->addSql('ALTER TABLE courses_course_folder DROP FOREIGN KEY FK_DF36F8E05387784A');
        $this->addSql('DROP INDEX IDX_DF36F8E05387784A ON courses_course_folder');
        $this->addSql('ALTER TABLE courses_course_folder DROP cover_img_file_id');
    }
}

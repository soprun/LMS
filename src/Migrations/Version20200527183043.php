<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200527183043 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE base_nps_1 CHANGE created_at created_at DATETIME NOT NULL, CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE base_block_6 CHANGE created_at created_at DATETIME NOT NULL, CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE base_block_task_with_check CHANGE created_at created_at DATETIME NOT NULL, CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE base_block_7 CHANGE created_at created_at DATETIME NOT NULL, CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE base_block_5 CHANGE created_at created_at DATETIME NOT NULL, CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE base_files CHANGE created_at created_at DATETIME NOT NULL, CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE base_block_4 CHANGE created_at created_at DATETIME NOT NULL, CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE base_block_1 CHANGE created_at created_at DATETIME NOT NULL, CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE base_block_3 CHANGE created_at created_at DATETIME NOT NULL, CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE base_block_2 CHANGE created_at created_at DATETIME NOT NULL, CHANGE updated_at updated_at DATETIME NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE base_block_1 CHANGE created_at created_at DATETIME DEFAULT \'2020-01-01 00:00:00\', CHANGE updated_at updated_at DATETIME DEFAULT \'2020-01-01 00:00:00\'');
        $this->addSql('ALTER TABLE base_block_2 CHANGE created_at created_at DATETIME DEFAULT \'2020-01-01 00:00:00\', CHANGE updated_at updated_at DATETIME DEFAULT \'2020-01-01 00:00:00\'');
        $this->addSql('ALTER TABLE base_block_3 CHANGE created_at created_at DATETIME DEFAULT \'2020-01-01 00:00:00\', CHANGE updated_at updated_at DATETIME DEFAULT \'2020-01-01 00:00:00\'');
        $this->addSql('ALTER TABLE base_block_4 CHANGE created_at created_at DATETIME DEFAULT \'2020-01-01 00:00:00\', CHANGE updated_at updated_at DATETIME DEFAULT \'2020-01-01 00:00:00\'');
        $this->addSql('ALTER TABLE base_block_5 CHANGE created_at created_at DATETIME DEFAULT \'2020-01-01 00:00:00\', CHANGE updated_at updated_at DATETIME DEFAULT \'2020-01-01 00:00:00\'');
        $this->addSql('ALTER TABLE base_block_6 CHANGE created_at created_at DATETIME DEFAULT \'2020-01-01 00:00:00\', CHANGE updated_at updated_at DATETIME DEFAULT \'2020-01-01 00:00:00\'');
        $this->addSql('ALTER TABLE base_block_7 CHANGE created_at created_at DATETIME DEFAULT \'2020-01-01 00:00:00\', CHANGE updated_at updated_at DATETIME DEFAULT \'2020-01-01 00:00:00\'');
        $this->addSql('ALTER TABLE base_block_task_with_check CHANGE created_at created_at DATETIME DEFAULT \'2020-01-01 00:00:00\', CHANGE updated_at updated_at DATETIME DEFAULT \'2020-01-01 00:00:00\'');
        $this->addSql('ALTER TABLE base_files CHANGE created_at created_at DATETIME DEFAULT \'2020-01-01 00:00:00\', CHANGE updated_at updated_at DATETIME DEFAULT \'2020-01-01 00:00:00\'');
        $this->addSql('ALTER TABLE base_nps_1 CHANGE created_at created_at DATETIME DEFAULT \'2020-01-01 00:00:00\', CHANGE updated_at updated_at DATETIME DEFAULT \'2020-01-01 00:00:00\'');
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211118100446 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('ALTER TABLE traction_value_type ADD position  tinyint unsigned  not null AFTER title');
        $this->addSql('UPDATE traction_value_type set position=1 where id=2');
        $this->addSql('UPDATE traction_value_type set position=2 where id=1');
        $this->addSql('UPDATE traction_value_type set position=3 where id=3');
        $this->addSql('UPDATE traction_value_type set position=4 where id=4');
        $this->addSql('UPDATE traction_value_type set position=5 where id=5');
        $this->addSql('UPDATE traction_value_type set position=6 where id=6');
        $this->addSql('UPDATE traction_value_type set position=7 where id=7');
        $this->addSql('ALTER TABLE traction_value_type ADD unique (position )');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql('ALTER TABLE traction_value_type drop position');
    }

}

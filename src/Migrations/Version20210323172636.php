<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210323172636 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE questionnaire_question (id INT AUTO_INCREMENT NOT NULL, next_question_id INT DEFAULT NULL, slug VARCHAR(50) NOT NULL, title VARCHAR(255) NOT NULL, user_property VARCHAR(100) DEFAULT NULL, INDEX IDX_28CC40C31CF5F25E (next_question_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE questionnaire_question_answer_variant (id INT AUTO_INCREMENT NOT NULL, question_id INT NOT NULL, next_question_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, INDEX IDX_28A29B5C1E27F6BF (question_id), INDEX IDX_28A29B5C1CF5F25E (next_question_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE questionnaire_user_answer (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, question_id INT NOT NULL, variant_id INT DEFAULT NULL, text LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_36AB8B38A76ED395 (user_id), INDEX IDX_36AB8B381E27F6BF (question_id), INDEX IDX_36AB8B383B69A9AF (variant_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE smart_sender_user (id INT AUTO_INCREMENT NOT NULL, smart_sender_bot_id INT NOT NULL, user_id INT NOT NULL, smart_sender_user_id INT DEFAULT NULL, code VARCHAR(255) NOT NULL, username VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_1352006E27B49FC2 (smart_sender_bot_id), INDEX IDX_1352006EA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE smart_sender_bot (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, token VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE smart_sender_bot_group (id INT AUTO_INCREMENT NOT NULL, smart_sender_bot_id INT NOT NULL, user_group_id INT NOT NULL, INDEX IDX_DC3BBCEB27B49FC2 (smart_sender_bot_id), UNIQUE INDEX UNIQ_DC3BBCEB1ED93D47 (user_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE timezone (id INT AUTO_INCREMENT NOT NULL, time_zone VARCHAR(100) NOT NULL, name VARCHAR(100) NOT NULL, offset INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE questionnaire_question ADD CONSTRAINT FK_28CC40C31CF5F25E FOREIGN KEY (next_question_id) REFERENCES questionnaire_question (id)');
        $this->addSql('ALTER TABLE questionnaire_question_answer_variant ADD CONSTRAINT FK_28A29B5C1E27F6BF FOREIGN KEY (question_id) REFERENCES questionnaire_question (id)');
        $this->addSql('ALTER TABLE questionnaire_question_answer_variant ADD CONSTRAINT FK_28A29B5C1CF5F25E FOREIGN KEY (next_question_id) REFERENCES questionnaire_question (id)');
        $this->addSql('ALTER TABLE questionnaire_user_answer ADD CONSTRAINT FK_36AB8B38A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE questionnaire_user_answer ADD CONSTRAINT FK_36AB8B381E27F6BF FOREIGN KEY (question_id) REFERENCES questionnaire_question (id)');
        $this->addSql('ALTER TABLE questionnaire_user_answer ADD CONSTRAINT FK_36AB8B383B69A9AF FOREIGN KEY (variant_id) REFERENCES questionnaire_question_answer_variant (id)');
        $this->addSql('ALTER TABLE smart_sender_user ADD CONSTRAINT FK_1352006E27B49FC2 FOREIGN KEY (smart_sender_bot_id) REFERENCES smart_sender_bot (id)');
        $this->addSql('ALTER TABLE smart_sender_user ADD CONSTRAINT FK_1352006EA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE smart_sender_bot_group ADD CONSTRAINT FK_DC3BBCEB27B49FC2 FOREIGN KEY (smart_sender_bot_id) REFERENCES smart_sender_bot (id)');
        $this->addSql('ALTER TABLE smart_sender_bot_group ADD CONSTRAINT FK_DC3BBCEB1ED93D47 FOREIGN KEY (user_group_id) REFERENCES user_group (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE questionnaire_question DROP FOREIGN KEY FK_28CC40C31CF5F25E');
        $this->addSql('ALTER TABLE questionnaire_question_answer_variant DROP FOREIGN KEY FK_28A29B5C1E27F6BF');
        $this->addSql('ALTER TABLE questionnaire_question_answer_variant DROP FOREIGN KEY FK_28A29B5C1CF5F25E');
        $this->addSql('ALTER TABLE questionnaire_user_answer DROP FOREIGN KEY FK_36AB8B381E27F6BF');
        $this->addSql('ALTER TABLE questionnaire_user_answer DROP FOREIGN KEY FK_36AB8B383B69A9AF');
        $this->addSql('ALTER TABLE smart_sender_user DROP FOREIGN KEY FK_1352006E27B49FC2');
        $this->addSql('ALTER TABLE smart_sender_bot_group DROP FOREIGN KEY FK_DC3BBCEB27B49FC2');
        $this->addSql('DROP TABLE questionnaire_question');
        $this->addSql('DROP TABLE questionnaire_question_answer_variant');
        $this->addSql('DROP TABLE questionnaire_user_answer');
        $this->addSql('DROP TABLE smart_sender_user');
        $this->addSql('DROP TABLE smart_sender_bot');
        $this->addSql('DROP TABLE smart_sender_bot_group');
        $this->addSql('DROP TABLE timezone');
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211102133609 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE traction_hundred_cell');
        $this->addSql('DROP TABLE traction_hundred');
        $this->addSql('DROP TABLE traction_speed_cell');
        $this->addSql('DROP TABLE traction_speed');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE traction_hundred (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, course_stream_id INT DEFAULT NULL, channel VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, channel_id INT NOT NULL, week_id INT NOT NULL, marked TINYINT(1) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_AB9340BB524C0692 (course_stream_id), INDEX IDX_AB9340BBA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE traction_hundred_cell (id INT AUTO_INCREMENT NOT NULL, traction_hundred_id INT NOT NULL, day_id INT NOT NULL, profit INT DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_D807F54A36355991 (traction_hundred_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE traction_speed (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, course_stream_id INT DEFAULT NULL, channel VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, channel_id INT NOT NULL, week_id INT NOT NULL, marked TINYINT(1) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_6BED7873524C0692 (course_stream_id), INDEX IDX_6BED7873A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE traction_speed_cell (id INT AUTO_INCREMENT NOT NULL, traction_speed_id INT NOT NULL, day_id INT NOT NULL, profit INT DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_A168140A9AB7E69 (traction_speed_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE traction_hundred ADD CONSTRAINT FK_AB9340BB524C0692 FOREIGN KEY (course_stream_id) REFERENCES course_stream (id)');
        $this->addSql('ALTER TABLE traction_hundred ADD CONSTRAINT FK_AB9340BBA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE traction_hundred_cell ADD CONSTRAINT FK_D807F54A36355991 FOREIGN KEY (traction_hundred_id) REFERENCES traction_hundred (id)');
        $this->addSql('ALTER TABLE traction_speed ADD CONSTRAINT FK_6BED7873524C0692 FOREIGN KEY (course_stream_id) REFERENCES course_stream (id)');
        $this->addSql('ALTER TABLE traction_speed ADD CONSTRAINT FK_6BED7873A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE traction_speed_cell ADD CONSTRAINT FK_A168140A9AB7E69 FOREIGN KEY (traction_speed_id) REFERENCES traction_speed (id)');

    }
}

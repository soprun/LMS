<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Illuminate\Support\Carbon;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211117094853 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $groupToCourseStreamInfo = [
            253 => [
                'stream' => 18,
                'start_date' => Carbon::parse('26.10.2020'),
                'end_date' => Carbon::parse('09.12.2020'),
            ],
            284 => [
                'stream' => 19,
                'start_date' => Carbon::parse('20.11.2020'),
                'end_date' => Carbon::parse('12.01.2021'),
            ],
            315 => [
                'stream' => 20,
                'start_date' => Carbon::parse('08.12.2020'),
                'end_date' => Carbon::parse('21.01.2021'),
            ],
            340 => [
                'stream' => 21,
                'start_date' => Carbon::parse('24.12.2020'),
                'end_date' => Carbon::parse('08.02.2021'),
            ],
            348 => [
                'stream' => 22,
                'start_date' => Carbon::parse('17.01.2021'),
                'end_date' => Carbon::parse('03.03.2021'),
            ],
            368 => [
                'stream' => 23,
                'start_date' => Carbon::parse('09.02.2021'),
                'end_date' => Carbon::parse('26.03.2021'),
            ],
            380 => [
                'stream' => 24,
                'start_date' => Carbon::parse('26.02.2021'),
                'end_date' => Carbon::parse('12.04.2021'),
            ],
            399 => [
                'stream' => 25,
                'start_date' => Carbon::parse('16.03.2021'),
                'end_date' => Carbon::parse('30.04.2021'),
            ],
            410 => [
                'stream' => 26,
                'start_date' => Carbon::parse('31.03.2021'),
                'end_date' => Carbon::parse('15.05.2021'),
            ],
            437 => [
                'stream' => 27,
                'start_date' => Carbon::parse('16.04.2021'),
                'end_date' => Carbon::parse('31.05.2021'),
            ],
            448 => [
                'stream' => 28,
                'start_date' => Carbon::parse('04.05.2021'),
                'end_date' => Carbon::parse('18.06.2021'),
            ],
            501 => [
                'stream' => 29,
                'start_date' => Carbon::parse('18.05.2021'),
                'end_date' => Carbon::parse('02.07.2021'),
            ],
        ];

        $this->connection->beginTransaction();

        $expr = $this->connection->getExpressionBuilder();
        $msaGroupsWithoutStreams = $this->connection->createQueryBuilder()
            ->select('*')
            ->from('user_group')
            ->where('group_folder_id = 43 and course_stream_id is null')
            ->andWhere('name like "Новый МСА%"')
            ->andWhere('id < 515')
            ->execute()
            ->fetchAll();

        foreach ($msaGroupsWithoutStreams as $msaGroup) {
            $this->connection->createQueryBuilder()->insert('course_stream')->values([
                'abstract_course_id' => 4,
                'stream' => $groupToCourseStreamInfo[$msaGroup['id']]['stream'],
                'name' => $expr->literal('МсА-' . $groupToCourseStreamInfo[$msaGroup['id']]['stream']),
                'start_date' => $expr->literal($groupToCourseStreamInfo[$msaGroup['id']]['start_date']->toDateString()),
                'period' =>104,
                'active' => 0,
            ])->execute();


            $this->connection->createQueryBuilder()->update('user_group')
                ->set('course_stream_id', $this->getLastInsertIdForTransaction())
                ->where('id = :id')
                ->setParameter('id', $msaGroup['id'])
                ->execute();
        }

        $this->connection->createQueryBuilder()->update('user_group')
            ->set('course_stream_id', 43)
            ->where('id = 855')
            ->execute();

        $this->connection->createQueryBuilder()->update('user_group')
            ->set('course_stream_id', 44)
            ->where('id = 965')
            ->execute();

        $this->connection->commit();
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    private function getLastInsertIdForTransaction(): int
    {
        return (int)$this->connection->executeQuery('SELECT last_insert_id();')->fetchOne();
    }

}

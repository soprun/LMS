<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210401184250 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE team_folder (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE team_folder_user_group (team_folder_id INT NOT NULL, user_group_id INT NOT NULL, INDEX IDX_28921140A129A7A5 (team_folder_id), INDEX IDX_289211401ED93D47 (user_group_id), PRIMARY KEY(team_folder_id, user_group_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE team_folder_user_group ADD CONSTRAINT FK_28921140A129A7A5 FOREIGN KEY (team_folder_id) REFERENCES team_folder (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE team_folder_user_group ADD CONSTRAINT FK_289211401ED93D47 FOREIGN KEY (user_group_id) REFERENCES user_group (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE team DROP FOREIGN KEY FK_C4E0A61F1ED93D47');
        $this->addSql('DROP INDEX IDX_C4E0A61F1ED93D47 ON team');
        $this->addSql('ALTER TABLE team CHANGE user_group_id folder_id INT NOT NULL');
        $this->addSql('ALTER TABLE team ADD CONSTRAINT FK_C4E0A61F162CB942 FOREIGN KEY (folder_id) REFERENCES team_folder (id)');
        $this->addSql('CREATE INDEX IDX_C4E0A61F162CB942 ON team (folder_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE team DROP FOREIGN KEY FK_C4E0A61F162CB942');
        $this->addSql('ALTER TABLE team_folder_user_group DROP FOREIGN KEY FK_28921140A129A7A5');
        $this->addSql('DROP TABLE team_folder');
        $this->addSql('DROP TABLE team_folder_user_group');
        $this->addSql('DROP INDEX IDX_C4E0A61F162CB942 ON team');
        $this->addSql('ALTER TABLE team CHANGE folder_id user_group_id INT NOT NULL');
        $this->addSql('ALTER TABLE team ADD CONSTRAINT FK_C4E0A61F1ED93D47 FOREIGN KEY (user_group_id) REFERENCES user_group (id)');
        $this->addSql('CREATE INDEX IDX_C4E0A61F1ED93D47 ON team (user_group_id)');
    }
}

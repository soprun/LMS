<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200601184811 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE group_permission DROP FOREIGN KEY FK_3784F318D69FE5A3');
        $this->addSql('CREATE TABLE user_group_folder (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('DROP TABLE group_permission_folder');
        $this->addSql('DROP INDEX IDX_3784F318D69FE5A3 ON group_permission');
        $this->addSql('ALTER TABLE group_permission DROP group_folder_id');
        $this->addSql('ALTER TABLE user_group ADD group_folder_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_group ADD CONSTRAINT FK_8F02BF9DD69FE5A3 FOREIGN KEY (group_folder_id) REFERENCES user_group_folder (id)');
        $this->addSql('CREATE INDEX IDX_8F02BF9DD69FE5A3 ON user_group (group_folder_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_group DROP FOREIGN KEY FK_8F02BF9DD69FE5A3');
        $this->addSql('CREATE TABLE group_permission_folder (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('DROP TABLE user_group_folder');
        $this->addSql('ALTER TABLE group_permission ADD group_folder_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE group_permission ADD CONSTRAINT FK_3784F318D69FE5A3 FOREIGN KEY (group_folder_id) REFERENCES group_permission_folder (id)');
        $this->addSql('CREATE INDEX IDX_3784F318D69FE5A3 ON group_permission (group_folder_id)');
        $this->addSql('DROP INDEX IDX_8F02BF9DD69FE5A3 ON user_group');
        $this->addSql('ALTER TABLE user_group DROP group_folder_id');
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210517185308 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE hotels (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, address VARCHAR(255) NOT NULL, tariff INT NOT NULL, image VARCHAR(255) NOT NULL, maplink VARCHAR(450) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE hotels_rooms (id INT AUTO_INCREMENT NOT NULL, hotel_id INT NOT NULL, number VARCHAR(100) NOT NULL, amount INT NOT NULL, description VARCHAR(255) NOT NULL, category INT NOT NULL, image1 VARCHAR(255) DEFAULT NULL, image2 VARCHAR(255) DEFAULT NULL, image3 VARCHAR(255) DEFAULT NULL, INDEX IDX_626E77853243BB18 (hotel_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE passports (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, series VARCHAR(10) NOT NULL, number VARCHAR(20) NOT NULL, authority_name VARCHAR(255) NOT NULL, authority_code VARCHAR(10) NOT NULL, issue_date DATE NOT NULL, gender VARCHAR(100) NOT NULL, birth_place VARCHAR(255) NOT NULL, residence_place VARCHAR(255) NOT NULL, is_confirm TINYINT(1) NOT NULL, INDEX IDX_1560E557A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rooms_reservation (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, room_id INT NOT NULL, UNIQUE INDEX UNIQ_83C31136A76ED395 (user_id), INDEX IDX_83C3113654177093 (room_id), UNIQUE INDEX user_room_idx (user_id, room_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE hotels_rooms ADD CONSTRAINT FK_626E77853243BB18 FOREIGN KEY (hotel_id) REFERENCES hotels (id)');
        $this->addSql('ALTER TABLE passports ADD CONSTRAINT FK_1560E557A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE rooms_reservation ADD CONSTRAINT FK_83C31136A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE rooms_reservation ADD CONSTRAINT FK_83C3113654177093 FOREIGN KEY (room_id) REFERENCES hotels_rooms (id)');
        $this->addSql('ALTER TABLE user ADD is_sochi_user TINYINT(1) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE hotels_rooms DROP FOREIGN KEY FK_626E77853243BB18');
        $this->addSql('ALTER TABLE rooms_reservation DROP FOREIGN KEY FK_83C3113654177093');
        $this->addSql('DROP TABLE hotels');
        $this->addSql('DROP TABLE hotels_rooms');
        $this->addSql('DROP TABLE passports');
        $this->addSql('DROP TABLE rooms_reservation');
        $this->addSql('ALTER TABLE user DROP is_sochi_user');
    }
}

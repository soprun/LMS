<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200828130911 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE points_shop_product (id INT AUTO_INCREMENT NOT NULL, cover_img_id INT NOT NULL, name VARCHAR(100) NOT NULL, title VARCHAR(100) NOT NULL, description VARCHAR(255) NOT NULL, full_description VARCHAR(2000) DEFAULT NULL, price INT NOT NULL, button_text VARCHAR(50) NOT NULL, url VARCHAR(1000) DEFAULT NULL, INDEX IDX_279AE56340D61A18 (cover_img_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE points_shop_order (id INT AUTO_INCREMENT NOT NULL, product_id INT NOT NULL, user_id INT NOT NULL, price INT NOT NULL, promocode VARCHAR(100) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_7A90901E4584665A (product_id), INDEX IDX_7A90901EA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE points_shop_product ADD CONSTRAINT FK_279AE56340D61A18 FOREIGN KEY (cover_img_id) REFERENCES base_files (id)');
        $this->addSql('ALTER TABLE points_shop_order ADD CONSTRAINT FK_7A90901E4584665A FOREIGN KEY (product_id) REFERENCES points_shop_product (id)');
        $this->addSql('ALTER TABLE points_shop_order ADD CONSTRAINT FK_7A90901EA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE points_shop_order DROP FOREIGN KEY FK_7A90901E4584665A');
        $this->addSql('DROP TABLE points_shop_product');
        $this->addSql('DROP TABLE points_shop_order');
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210909074254 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE course_stream ADD alternative_course_stream_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE course_stream ADD CONSTRAINT FK_5ED17F511429FD3 FOREIGN KEY (alternative_course_stream_id) REFERENCES course_stream (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5ED17F511429FD3 ON course_stream (alternative_course_stream_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE course_stream DROP FOREIGN KEY FK_5ED17F511429FD3');
        $this->addSql('DROP INDEX UNIQ_5ED17F511429FD3 ON course_stream');
        $this->addSql('ALTER TABLE course_stream DROP alternative_course_stream_id');
    }
}

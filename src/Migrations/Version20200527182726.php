<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200527182726 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE team CHANGE created_at created_at DATETIME NOT NULL, CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE courses_lesson_configuration CHANGE created_at created_at DATETIME NOT NULL, CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE team_user CHANGE created_at created_at DATETIME NOT NULL, CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE courses_course_configutarion CHANGE created_at created_at DATETIME NOT NULL, CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE courses_lesson CHANGE created_at created_at DATETIME NOT NULL, CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE courses_lesson_part_block CHANGE created_at created_at DATETIME NOT NULL, CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE courses_module CHANGE created_at created_at DATETIME NOT NULL, CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE courses_course CHANGE created_at created_at DATETIME NOT NULL, CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE courses_course_folder CHANGE created_at created_at DATETIME NOT NULL, CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE courses_lesson_part CHANGE created_at created_at DATETIME NOT NULL, CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE group_permission CHANGE created_at created_at DATETIME NOT NULL, CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE user_group CHANGE created_at created_at DATETIME NOT NULL, CHANGE updated_at updated_at DATETIME NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE courses_course CHANGE created_at created_at DATETIME DEFAULT \'2020-01-01 00:00:00\', CHANGE updated_at updated_at DATETIME DEFAULT \'2020-01-01 00:00:00\'');
        $this->addSql('ALTER TABLE courses_course_configutarion CHANGE created_at created_at DATETIME DEFAULT \'2020-01-01 00:00:00\', CHANGE updated_at updated_at DATETIME DEFAULT \'2020-01-01 00:00:00\'');
        $this->addSql('ALTER TABLE courses_course_folder CHANGE created_at created_at DATETIME DEFAULT \'2020-01-01 00:00:00\', CHANGE updated_at updated_at DATETIME DEFAULT \'2020-01-01 00:00:00\'');
        $this->addSql('ALTER TABLE courses_lesson CHANGE created_at created_at DATETIME DEFAULT \'2020-01-01 00:00:00\', CHANGE updated_at updated_at DATETIME DEFAULT \'2020-01-01 00:00:00\'');
        $this->addSql('ALTER TABLE courses_lesson_configuration CHANGE created_at created_at DATETIME DEFAULT \'2020-01-01 00:00:00\', CHANGE updated_at updated_at DATETIME DEFAULT \'2020-01-01 00:00:00\'');
        $this->addSql('ALTER TABLE courses_lesson_part CHANGE created_at created_at DATETIME DEFAULT \'2020-01-01 00:00:00\', CHANGE updated_at updated_at DATETIME DEFAULT \'2020-01-01 00:00:00\'');
        $this->addSql('ALTER TABLE courses_lesson_part_block CHANGE created_at created_at DATETIME DEFAULT \'2020-01-01 00:00:00\', CHANGE updated_at updated_at DATETIME DEFAULT \'2020-01-01 00:00:00\'');
        $this->addSql('ALTER TABLE courses_module CHANGE created_at created_at DATETIME DEFAULT \'2020-01-01 00:00:00\', CHANGE updated_at updated_at DATETIME DEFAULT \'2020-01-01 00:00:00\'');
        $this->addSql('ALTER TABLE group_permission CHANGE created_at created_at DATETIME DEFAULT \'2020-01-01 00:00:00\', CHANGE updated_at updated_at DATETIME DEFAULT \'2020-01-01 00:00:00\'');
        $this->addSql('ALTER TABLE team CHANGE created_at created_at DATETIME DEFAULT \'2020-01-01 00:00:00\', CHANGE updated_at updated_at DATETIME DEFAULT \'2020-01-01 00:00:00\'');
        $this->addSql('ALTER TABLE team_user CHANGE created_at created_at DATETIME DEFAULT \'2020-01-01 00:00:00\', CHANGE updated_at updated_at DATETIME DEFAULT \'2020-01-01 00:00:00\'');
        $this->addSql('ALTER TABLE user_group CHANGE created_at created_at DATETIME DEFAULT \'2020-01-01 00:00:00\', CHANGE updated_at updated_at DATETIME DEFAULT \'2020-01-01 00:00:00\'');
    }
}

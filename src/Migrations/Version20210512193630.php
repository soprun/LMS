<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210512193630 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE faculty (id INT AUTO_INCREMENT NOT NULL, team_folder_id INT NOT NULL, name VARCHAR(255) NOT NULL, redirect_url VARCHAR(255) DEFAULT NULL, INDEX IDX_17966043A129A7A5 (team_folder_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE faculty ADD CONSTRAINT FK_17966043A129A7A5 FOREIGN KEY (team_folder_id) REFERENCES team_folder (id)');
        $this->addSql('ALTER TABLE user_group_user ADD faculty_id INT DEFAULT NULL AFTER amo_lead_id');
        $this->addSql('ALTER TABLE user_group_user ADD CONSTRAINT FK_3AE4BD5680CAB68 FOREIGN KEY (faculty_id) REFERENCES faculty (id)');
        $this->addSql('CREATE INDEX IDX_3AE4BD5680CAB68 ON user_group_user (faculty_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_group_user DROP FOREIGN KEY FK_3AE4BD5680CAB68');
        $this->addSql('DROP TABLE faculty');
        $this->addSql('DROP INDEX IDX_3AE4BD5680CAB68 ON user_group_user');
        $this->addSql('ALTER TABLE user_group_user DROP faculty_id');
    }
}

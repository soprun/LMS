<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210818225356 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE team_folder ADD course_stream_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE team_folder ADD CONSTRAINT FK_9AB94E14524C0692 FOREIGN KEY (course_stream_id) REFERENCES course_stream (id)');
        $this->addSql('CREATE INDEX IDX_9AB94E14524C0692 ON team_folder (course_stream_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE team_folder DROP FOREIGN KEY FK_9AB94E14524C0692');
        $this->addSql('DROP INDEX IDX_9AB94E14524C0692 ON team_folder');
        $this->addSql('ALTER TABLE team_folder DROP course_stream_id');
    }
}

<?php

namespace App\DTO\MarathonInvestment;

use App\DTO\PageParamsDTO;
use App\Entity\CourseStream;

class MarathonInvestmentMembersFiltersDTO extends PageParamsDTO
{
    /** @var bool|null */
    public $hasVerified;
    /** @var CourseStream|null */
    public $stream;
    /** @var string|null */
    public $city;
    /** @var string[] */
    public $sorts = [];

}

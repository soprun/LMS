<?php

namespace App\DTO\Document;

/**
 * Абстрактный класс, используемый в DocumentBuilder-ах
 */
abstract class BaseDocument
{
    public const DEFAULT_EXPORT_SEPARATOR = ';';

    /**
     * @return string
     */
    public function getTitles(): string
    {
        return implode(static::DEFAULT_EXPORT_SEPARATOR, array_keys((array) $this));
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return implode(static::DEFAULT_EXPORT_SEPARATOR, (array) $this);
    }
}

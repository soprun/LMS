<?php

namespace App\DTO\Document\TractionAnalytics;

/**
 * Моделька для удобной работы с аналитикой.
 * Все заветы ООП игнорируются специально
 */
class TractionAnalyticsUserDocument
{
    public const DEFAULT_SEPARATOR = ';';

    public $lmsUserId;

    public $email;

    public $phone;

    public $name;

    public $lastname;

    public $city;

    public $niche;

    public $facultyId;

    public $facultyName;

    public $addedBy;

    public $groupName;

    /**
     * Альтернативная группа, в которой ученик находится прямо сейчас
     * По этому полю можно найти учеников, которые находятся на двух курсах сразу
     * @var null|string
     */
    public $alternativeGroupName;

    /**
     * Альтернативная группа, в которой ученик когда-то находился
     * По этому полю можно понять, что ученик сменил курс после анкеты
     * @var null|string
     */
    public $deletedAlternativeGroupName;

    public $captainId;

    public $captainName;

    public $teamName;

    public $teamId;

    public $badGroup;

    public $amoLeadId;

    public $budget;

    public $tariff;

    public $responsiblePartnerId;

    public $responsiblePartnerName;

    public $lessonViews;

    public $revenue_before;

    public $revenue = [];

    public $profit_before;

    public $profit = [];

    public $values = [];

    public $revenue_count_max = 0;

    public $revenue_course = 0;

    public $revenue_4_weeks = 0;

    public $revenue_2_weeks = 0;

    public $profit_count_max = 0;

    public $profit_course = 0;

    public $profit_4_weeks = 0;

    public $profit_2_weeks = 0;

    public $revenue_average = 0;

    public $revenue_growth = 0;

    public $profit_average = 0;

    public $profit_growth = 0;

    public function __toString(): string
    {
        $string = '';
        foreach ($this as $prop => $value) {
            if ($prop === 'revenue' || $prop === 'profit') {
                foreach ($value as $profit) {
                    $string .= $profit . static::DEFAULT_SEPARATOR;
                }
                continue;
            }
            if ($prop === 'values') {
                continue;
            }

            $string .= $value . static::DEFAULT_SEPARATOR;
        }

        return mb_substr($string, 0, -1);
    }

}

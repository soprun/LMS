<?php

namespace App\DTO\Document\TractionAnalytics;

use App\DTO\Document\BaseDocument;
use App\Entity\User;

class WeekRevenueProfitDocument extends BaseDocument
{
    /**
     * @var int
     */
    public $user;

    /**
     * Выручка за текущую неделю
     *
     * @var int
     */
    public $revenue;

    /**
     * Прибыль за текущую неделю
     *
     * @var int
     */
    public $profit;

    /**
     * Номер недели, отсчитывается с даты начала курса
     *
     * @var int
     */
    public $week;

    /**
     * Дата начала недели
     *
     * @var int
     */
    public $weekStartDate;

    /**
     * @param User|int $user = null
     * @param int $revenue = 0
     * @param int $profit = 0
     * @param int $week = null
     * @param int $weekStartDate = null (Timestamp)
     */
    public function __construct(
        $user = null,
        int $revenue = 0,
        int $profit = 0,
        int $week = null,
        int $weekStartDate = null
    ) {
        $this->user = $user;
        $this->revenue = $revenue;
        $this->profit = $profit;
        $this->week = $week;
        $this->weekStartDate = $weekStartDate;
    }
}

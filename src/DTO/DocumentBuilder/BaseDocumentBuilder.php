<?php

namespace App\DTO\DocumentBuilder;

use App\DTO\Document\BaseDocument;
use App\Service\AuthServerService;
use Closure;

abstract class BaseDocumentBuilder
{
    /**
     * @var AuthServerService
     */
    protected $authServerService;

    /**
     * @var Closure
     */
    protected $beforeBuild;

    /**
     * @param AuthServerHelper $authServerHelper
     */
    public function __construct(AuthServerService $authServerService)
    {
        $this->authServerService = $authServerService;
    }

    /**
     * @param Closure $beforeBuild
     *
     * @return self
     */
    public function setBeforeBuild(Closure $beforeBuild): self
    {
        $this->beforeBuild = $beforeBuild;

        return $this;
    }

    /**
     * @param mixed $entity
     * @param BaseDocument $document
     *
     * @return BaseDocument
     */
    public function beforeBuild(&$entity, BaseDocument &$document): BaseDocument
    {
        $callable = $this->beforeBuild;
        $document = $callable($entity, $document);

        return $document;
    }
}

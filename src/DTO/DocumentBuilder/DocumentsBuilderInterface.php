<?php

namespace App\DTO\DocumentBuilder;

use App\DTO\Document\BaseDocument;
use Illuminate\Support\Collection;

interface DocumentsBuilderInterface
{
    /**
     * @param mixed $entity
     * @param array $groups = []
     * @param array $filter = []
     * @param int|null $page = 1
     * @param int|null $pageSize = 10
     *
     * @return BaseDocument
     */
    public function buildOne(
        $entity,
        array $groups = [],
        array $filter = [],
        int $page = 1,
        int $pageSize = 10
    ): BaseDocument;

    /**
     * @param mixed[]|Traversable $entities
     * @param array $groups = []
     * @param array $filter = []
     * @param int|null $page = 1
     * @param int|null $pageSize = 10
     *
     * @return Collection
     */
    public function build(
        $entities,
        array $groups = [],
        array $filter = [],
        int $page = 1,
        int $pageSize = 10
    ): Collection;
}

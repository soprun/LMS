<?php

namespace App\DTO\Certificate;

use Imagick;

class CertificateImageDto
{
    /** @var Imagick */
    private $image;

    /** @var string */
    private $serialString;

    /** @var string */
    private $studentNameString;

    /** @var string|null */
    private $dateString;

    public function getImage(): Imagick
    {
        return $this->image;
    }

    public function setImage(Imagick $image): CertificateImageDto
    {
        $this->image = $image;

        return $this;
    }

    public function getSerialString(): string
    {
        return $this->serialString;
    }

    public function setSerialString(string $serialString): CertificateImageDto
    {
        $this->serialString = $serialString;

        return $this;
    }

    public function getStudentNameString(): string
    {
        return $this->studentNameString;
    }

    public function setStudentNameString(string $studentNameString): CertificateImageDto
    {
        $this->studentNameString = $studentNameString;

        return $this;
    }

    public function getDateString(): ?string
    {
        return $this->dateString;
    }

    public function setDateString(string $dateString): CertificateImageDto
    {
        $this->dateString = $dateString;

        return $this;
    }

}

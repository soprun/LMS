<?php

namespace App\DTO\Traction;

use App\Entity\CourseStream;
use App\Entity\Traction\Value;
use DateTimeInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     @OA\Property(property="period", example="05.07-11.07", description="Текстовое представление периода"),
 *     @OA\Property(property="from", description="Дата начала периода"),
 *     @OA\Property(property="to", description="Дата окончания периода")
 * )
 */
class UserTractionForSettlementPeriod
{
    /**
     * @var string
     * @Groups("user:traction")
     */
    private $period;

    /**
     * @var DateTimeInterface
     * @Groups("user:traction")
     */
    private $from;

    /**
     * Ключевая дата в которую проводится сохранение значений
     *
     * @var DateTimeInterface
     * @Groups("user:traction")
     */
    private $keyDate;

    /**
     * @var DateTimeInterface
     * @Groups("user:traction")
     */
    private $to;

    /**
     * @var Value[]
     * @Groups("user:traction")
     */
    private $values;

    /**
     * @var CourseStream[]
     * @Groups("user:traction")
     */
    private $streams;

    public function getValues(): array
    {
        return $this->values;
    }

    public function getStreams(): array
    {
        return $this->streams;
    }

    public function getPeriod(): string
    {
        return $this->period;
    }

    public function getFrom(): DateTimeInterface
    {
        return $this->from;
    }

    public function getTo(): DateTimeInterface
    {
        return $this->to;
    }

    public function getKeyDate(): DateTimeInterface
    {
        return $this->keyDate;
    }

    public function __construct(
        DateTimeInterface $from,
        DateTimeInterface $to,
        DateTimeInterface $keyDate,
        array $streams,
        string $period,
        array $values
    ) {
        $this->from = $from;
        $this->to = $to;
        $this->streams = $streams;
        $this->period = $period;
        $this->values = $values;
        $this->keyDate = $keyDate;
    }

}

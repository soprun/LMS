<?php

namespace App\DTO;

class PageParamsDTO
{
    /** @var int */
    public $page;

    /** @var int */
    public $pageSize;

    public function getFirstResult(): int
    {
        return ($this->page - 1) * $this->pageSize;
    }

}

<?php

namespace App\DTO;

final class UserLocationDto
{
    /**
     * @var float
     */
    private $longitude;

    /**
     * @var float
     */
    private $latitude;

    /**
     * @var float
     */
    private $pointSizeLongitude;

    /**
     * @var float
     */
    private $pointSizeLatitude;

    /**
     * @var float
     */
    private $longitudeDelta;

    /**
     * @var float
     */
    private $latitudeDelta;


    public function __construct(
        float $longitude,
        float $latitude,
        string $pointSizeLongitude,
        string $pointSizeLatitude,
        float $longitudeDelta = null,
        float $latitudeDelta = null
    ) {
        $this->longitude = $longitude;
        $this->latitude = $latitude;
        $this->pointSizeLongitude = $pointSizeLongitude;
        $this->pointSizeLatitude = $pointSizeLatitude;
        $this->longitudeDelta = $longitudeDelta;
        $this->latitudeDelta = $latitudeDelta;
    }

    public function getLongitude(): float
    {
        return $this->longitude;
    }

    public function getLatitude(): float
    {
        return $this->latitude;
    }

    public function getLongitudeDelta(): float
    {
        return $this->longitudeDelta;
    }

    public function getLatitudeDelta(): float
    {
        return $this->latitudeDelta;
    }

    public function getPointSizeLongitude(): float
    {
        return $this->pointSizeLongitude;
    }

    public function getPointSizeLatitude(): float
    {
        return $this->pointSizeLatitude;
    }

}

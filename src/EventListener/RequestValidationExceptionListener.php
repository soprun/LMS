<?php

namespace App\EventListener;

use App\Exception\RequestValidationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class RequestValidationExceptionListener
{
    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();
        if (!$exception instanceof RequestValidationException) {
            return;
        }
        $responseData = [
            'message' => $exception->getMessage(),
            'errors' => $exception->getMessages(),
        ];
        $event->setResponse(new JsonResponse($responseData, Response::HTTP_BAD_REQUEST));
    }

}

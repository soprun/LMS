<?php

namespace App\Annotation;

use OpenApi\Annotations;

/**
 * @Annotation
 *
 * todo: удалить после избавления от Json Api формата ответов
 *
 * @property-write $type The value MUST be one of "string", "number", "integer", "boolean", "array" or "object".
 * @property-write $example Example value
 */
class JsonApiResponse extends Annotations\Response
{
    public function __construct(array $properties)
    {
        $dataProperty = $this->makeDataProperty($properties);
        foreach (self::MOVE_TO_DATA_PROPERTY_FIELDS as $field) {
            unset($properties[$field]);
        }
        unset($properties['type'], $properties['value'], $properties['example']);
        parent::__construct(
            array_merge(
                $properties,
                [
                    'value' => new Annotations\MediaType(
                        [
                            'mediaType' => 'application/json',
                            'value' => new Annotations\Schema(
                                [
                                    'type' => 'object',
                                    'value' => [
                                        new Annotations\Property(['property' => 'status', 'type' => 'string']),
                                        $this->makeErrorsProperty(),
                                        $dataProperty,
                                    ],
                                ]
                            ),
                        ]
                    ),
                ]
            )
        );
    }

    private const MOVE_TO_DATA_PROPERTY_FIELDS = [
        'type',
        'example',
        'value',
    ];

    private function makeDataProperty(array $parentProperies): Annotations\Property
    {
        $properties = ['property' => 'data'];
        foreach (self::MOVE_TO_DATA_PROPERTY_FIELDS as $movedField) {
            if (!empty($parentProperies[$movedField])) {
                $properties[$movedField] = $parentProperies[$movedField];
            }
        }

        return new Annotations\Property($properties);
    }

    private function makeErrorsProperty(): Annotations\Property
    {
        return new Annotations\Property(
            [
                'property' => 'errors',
                'type' => 'array',
                'value' => new Annotations\Items([
                    'type' => 'object',
                    'value' => [
                        new Annotations\Property(['type' => 'string', 'property' => 'field']),
                        new Annotations\Property(['type' => 'string', 'property' => 'message']),
                    ],
                ]),
            ]
        );
    }

}

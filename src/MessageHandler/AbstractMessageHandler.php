<?php

namespace App\MessageHandler;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Рекомендация:
 * При создании обработчиков наследуемся от этого класса,
 * а не имплементируем Symfony\Component\Messenger\Handler\MessageHandlerInterface
 * в своих обработчиках.
 */
abstract class AbstractMessageHandler implements MessageHandlerInterface
{
    // статусы сообщений
    public const OPENED_STATUS = 'opened';
    public const CLOSED_STATUS = 'closed';
    public const SUCCESS_STATUS = 'success';
    public const ERROR_STATUS = 'error';
    public const DENIED_STATUS = 'denied';

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var MessageBusInterface
     */
    protected $messageBus;

    /**
     * @var mixed
     */
    protected $report;

    /**
     * @param EntityManagerInterface $em
     * @param LoggerInterface $logger
     * @param MessageBusInterface $messageBus
     */
    public function __construct(EntityManagerInterface $em, LoggerInterface $logger, MessageBusInterface $messageBus)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->messageBus = $messageBus;
    }

    /**
     * @param mixed $report = null
     *
     * @return void
     */
    protected function dispatchReport($report = null, array $stamps = []): void
    {
        if ($report) {
            $this->messageBus->dispatch($report, $stamps);
        } elseif ($this->report) {
            $this->messageBus->dispatch($this->report, $stamps);
        } else {
            $this->logger->error('Сообщение-отчет не найдено', [
                'handler' => get_class($this),
                'method' => __METHOD__,
            ]);
            throw new \InvalidArgumentException('Сообщение-отчет не найдено');
        }
    }
}

<?php

declare(strict_types=1);

namespace App\MessageHandler\LMS\Duplicate;

use App\Entity\Courses\CourseFolder;
use App\Message\LMS\Duplicate\CourseFolderDuplicateMessage;

final class CourseFolderDuplicateMessageHandler extends EntityDuplicateMessageHandler
{

    public function __invoke(CourseFolderDuplicateMessage $courseFolderDuplicateMessage)
    {
        /** @var CourseFolder $courseFolder */
        $courseFolder = $this->em->getRepository(CourseFolder::class)->find(
            $courseFolderDuplicateMessage->getCourseFolderId()
        );
        $this->duplicateEntityService->duplicateFolder($courseFolder, $courseFolderDuplicateMessage->getWithCourses());
    }

}

<?php

declare(strict_types=1);

namespace App\MessageHandler\LMS\Duplicate;

use App\Entity\Courses\Course;
use App\Message\LMS\Duplicate\CourseDuplicateMessage;

final class CourseDuplicateMessageHandler extends EntityDuplicateMessageHandler
{

    public function __invoke(CourseDuplicateMessage $courseDuplicateMessage)
    {
        /** @var Course $course */
        $course = $this->em->getRepository(Course::class)->find(
            $courseDuplicateMessage->getCourseId()
        );

        $this->duplicateEntityService->duplicateCourse($course);
    }

}

<?php

declare(strict_types=1);

namespace App\MessageHandler\LMS\Duplicate;

use App\Service\DuplicateEntityService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

abstract class EntityDuplicateMessageHandler implements MessageHandlerInterface
{
    protected $em;
    protected $duplicateEntityService;

    public function __construct(
        EntityManagerInterface $em,
        DuplicateEntityService $duplicateEntityService
    ) {
        $this->em = $em;
        $this->duplicateEntityService = $duplicateEntityService;
    }

}

<?php

declare(strict_types=1);

namespace App\MessageHandler\LMS\Duplicate;

use App\Entity\Courses\Course;
use App\Entity\Courses\Lesson;
use App\Message\LMS\Duplicate\LessonDuplicateMessage;

final class LessonDuplicateMessageHandler extends EntityDuplicateMessageHandler
{

    public function __invoke(LessonDuplicateMessage $lessonDuplicateMessage)
    {
        /** @var Lesson $lesson */
        $lesson = $this->em->getRepository(Lesson::class)->find(
            $lessonDuplicateMessage->getLessonId()
        );

        /**@var Course $course */
        $course = $this->em->getRepository(Course::class)->find(
            $lessonDuplicateMessage->getCourseId()
        );

        $this->duplicateEntityService->duplicateLesson($lesson, $course);
    }

}

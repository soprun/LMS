<?php

namespace App\MessageHandler\LMS;

use App\Entity\GroupModules;
use App\Entity\User;
use App\Entity\UserGroup;
use App\Entity\UserGroupUser;
use App\Entity\UserTariff;
use App\Exceptions\GrantingAccessMessageException;
use App\Message\LMS\GrantingAccessMessage;
use App\Message\LMS\ReportMessage;
use App\MessageHandler\AbstractMessageHandler;
use App\Service\AuthServerService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Transport\AmqpExt\AmqpStamp;
use Throwable;

class GrantingAccessMessageHandler extends AbstractMessageHandler
{
    public const OPERATION_OPEN = 'open';
    public const OPERATION_CLOSE = 'close';
    public const OPERATION_CHECK = 'check';

    /** @var AuthServerService */
    protected $authServerService;

    public function __construct(
        EntityManagerInterface $em,
        LoggerInterface $logger,
        AuthServerService $authServerService,
        MessageBusInterface $messageBus,
        string $_authServerToken
    ) {
        parent::__construct($em, $logger, $messageBus);

        $authServerService->setUserAccessToken('ServerToken ' . $_authServerToken);
        $this->authServerService = $authServerService;
    }

    public function __invoke(GrantingAccessMessage $message): void
    {
        try {
            switch ($message->getOperation()) {
                case static::OPERATION_OPEN:
                    $this->addToGroup($message);
                    break;
                case static::OPERATION_CLOSE:
                    $this->removeFromGroup($message);
                    break;
                case static::OPERATION_CHECK:
                    $this->checkGroup($message);
                    break;
                default:
                    $this->logger->error('Не удалось выполнить операцию', [
                        'operation' => $message->getOperation(),
                        'message_name' => get_class($message),
                        'method' => __METHOD__,
                    ]);
                    break;
            }
        } catch (Throwable $e) {
            if ($e instanceof GrantingAccessMessageException) {
                $reportMessage = $e->getReportMessage();
                $logMessage = $e->getMessage();
                $logContext = $e->getContext();
            } else {
                $logMessage = 'Задача провалена.';
                $logContext = [
                    'message_name' => get_class($message),
                    'message' => $e->getMessage(),
                    'exception' => get_class($e),
                    'method' => __METHOD__,
                    'trace' => $e->getTrace(),
                ];
                $reportMessage = new ReportMessage($message->getDeal(), static::ERROR_STATUS);
            }
            $this->logger->error($logMessage, $logContext);
            $this->dispatchReport($reportMessage, [new AmqpStamp('crm')]);
        }
    }


    private function getUserByMessage(GrantingAccessMessage $message): User
    {
        $response = $this->authServerService->createUser([
            'name' => $message->getName(),
            'email' => $message->getEmail(),
            'phone' => $message->getPhone(),
        ]);
        if (isset($response['id'])) {
            $user = $this->em->getRepository(User::class)->findOneBy([
                'authUserId' => $response['id'],
            ]);
            if (!$user) {
                $user = new User();
                $user->setAuthUserId($response['id']);
                $this->em->persist($user);
                $this->em->flush();
            }

//            if (
//                $message->getTariff() === null
//                && in_array(
//                    $message->getCourse(),
//                    [AbstractCourse::SLUG_HUNDRED, AbstractCourse::SLUG_SPEED, AbstractCourse::SLUG_SPEED_CLUB],
//                    true
//                )
//                && !$this->isAccessAvailable($user, $message->getTariff())
//            ) {
//                throw new GrantingAccessMessageException(
//                    'Отказанно в доступе',
//                    new ReportMessage($message->getDeal(), static::DENIED_STATUS),
//                    [
//                        'user' => $user->getId(),
//                        'tariff' => $message->getTariff(),
//                        'deal' => $message->getDeal(),
//                        'message_name' => get_class($message),
//                        'method' => __METHOD__,
//                    ]
//                );
//            }

            return $user;
        }

        throw new GrantingAccessMessageException(
            'Не удалось создать или найти пользователя',
            new ReportMessage($message->getDeal(), static::ERROR_STATUS),
            [
                'response' => $response,
                'message_name' => get_class($message),
                'method' => __METHOD__,
            ]
        );
    }

    /**
     * @return UserGroup[]
     * @throws GrantingAccessMessageException
     */
    private function getUserGroupsByMessage(GrantingAccessMessage $message): array
    {
        if (
            $message->getCourse() === null
            && $message->getStream() === null
            && $message->getModule() === null
        ) {
            $this->logger->error('Невалидные параметры', [
                'message_name' => get_class($message),
                'method' => __METHOD__,
            ]);
            throw new GrantingAccessMessageException(
                'Невалидные параметры',
                new ReportMessage($message->getDeal(), static::ERROR_STATUS),
                [
                    'message_name' => get_class($message),
                    'method' => __METHOD__,
                ]
            );
        }
        $groupModules = $this->em->getRepository(GroupModules::class)->getGroupModulesByMessage($message);
        if (empty($groupModules)) {
            throw new GrantingAccessMessageException(
                'Не найдены конфигурации для входящих данных',
                new ReportMessage($message->getDeal(), static::ERROR_STATUS),
                [
                    'course' => $message->getCourse(),
                    'module' => $message->getModule(),
                    'tariff' => $message->getTariff(),
                    'stream' => $message->getStream(),
                    'method' => __METHOD__,
                ]
            );
        }

        /** @var UserGroup[] $userGroups */
        $userGroups = array_filter(
            array_map(static function (GroupModules $gm) {
                return $gm->getRelatedGroup();
            }, $groupModules)
        );

        if (empty($userGroups)) {
            throw new GrantingAccessMessageException(
                'Не удалось найти группы',
                new ReportMessage($message->getDeal(), static::ERROR_STATUS),
                [
                    'group_modules' => $groupModules,
                    'message_name' => get_class($message),
                    'method' => __METHOD__,
                ]
            );
        }

        return $userGroups;
    }

    public function addToGroup(GrantingAccessMessage $message): void
    {
        $userGroups = $this->getUserGroupsByMessage($message);
        $user = $this->getUserByMessage($message);

        foreach ($userGroups as $userGroup) {
            $userGroupUser = $this->em->getRepository(UserGroupUser::class)->findOneBy([
                'userGroup' => $userGroup,
                'user' => $user,
            ]);
            if ($userGroupUser === null) {
                $userGroupUser = new UserGroupUser();
                $userGroupUser->setUser($user);
                $userGroupUser->setUserGroup($userGroup);
            }
            $userGroupUser->setDeleted(false);
            $this->em->persist($userGroupUser);
            $this->em->flush();
            $courseName = $userGroup->getCourseStream()
                ? $userGroup->getCourseStream()->getName()
                : $userGroup->getName();
            $report = new ReportMessage(
                $message->getDeal(),
                static::OPENED_STATUS,
                $message->getCourse() ? $courseName : null,
                $message->getModule() ? $courseName : null
            );
            $this->dispatchReport($report, [
                new AmqpStamp('crm'),
            ]);
        }
    }

    public function removeFromGroup(GrantingAccessMessage $message): void
    {
        $userGroups = $this->getUserGroupsByMessage($message);
        $user = $this->getUserByMessage($message);
        foreach ($userGroups as $userGroup) {
            $userGroupUser = $this->em->getRepository(UserGroupUser::class)->findOneBy([
                'userGroup' => $userGroup,
                'user' => $user,
            ]);
            if ($userGroupUser) {
                $userGroupUser->setDeleted(true);
                $this->em->persist($userGroupUser);
                $this->em->flush();
                $courseName = $userGroup->getCourseStream()
                    ? $userGroup->getCourseStream()->getName()
                    : $userGroup->getName();
                $this->dispatchReport(
                    new ReportMessage(
                        $message->getDeal(),
                        static::CLOSED_STATUS,
                        $message->getCourse() ? $courseName : null,
                        $message->getModule() ? $courseName : null
                    ),
                    [new AmqpStamp('crm')]
                );
            }
        }
    }

    public function checkGroup(GrantingAccessMessage $message): void
    {
        $userGroups = $this->getUserGroupsByMessage($message);
        $user = $this->getUserByMessage($message);

        foreach ($userGroups as $userGroup) {
            $userGroupUser = $this->em->getRepository(UserGroupUser::class)->findOneBy([
                'userGroup' => $userGroup,
                'user' => $user,
            ]);
            if ($userGroupUser) {
                $status = $userGroupUser->isDeleted() ? static::CLOSED_STATUS : static::OPENED_STATUS;
                $courseName = $userGroup->getCourseStream()
                    ? $userGroup->getCourseStream()->getName()
                    : $userGroup->getName();
                $this->dispatchReport(
                    new ReportMessage(
                        $message->getDeal(),
                        $status,
                        $message->getCourse() ? $courseName : null,
                        $message->getModule() ? $courseName : null
                    ),
                    [new AmqpStamp('crm')]
                );
            }
        }
    }

    private function isAccessAvailable(User $user, ?string $tariff = null): bool
    {
        return $tariff === UserTariff::LF_TARIFF_SLUG && $this->userInLf($user);
    }

    private function userInLf(User $user): bool
    {
        $likeFamilyGroupId = 169;
        $group = $this->em->getRepository(UserGroup::class)->find($likeFamilyGroupId);
        if (!$group) {
            $this->logger->error('Не удалось найти группу Like Family', [
                'like_family_group_id' => $likeFamilyGroupId,
                'message_handler' => get_class($this),
                'method' => __METHOD__,
            ]);
        }
        $userGroupUser = $this->em->getRepository(UserGroupUser::class)->findOneBy([
            'userGroup' => $group,
            'user' => $user,
        ]);
        if ($userGroupUser) {
            return true;
        }

        return false;
    }

}

<?php

namespace App\Exceptions;

use App\Message\LMS\ReportMessage;
use RuntimeException;

class GrantingAccessMessageException extends RuntimeException
{
    /** @var ReportMessage */
    private $reportMessage;

    /** @var array<string, mixed> */
    private $context;

    public function __construct(string $message, ReportMessage $reportMessage, array $context = [])
    {
        parent::__construct($message);
        $this->reportMessage = $reportMessage;
        $this->context = $context;
    }

    public function getReportMessage(): ReportMessage
    {
        return $this->reportMessage;
    }

    public function getContext(): array
    {
        return $this->context;
    }

}

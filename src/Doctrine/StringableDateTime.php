<?php

namespace App\Doctrine;

class StringableDateTime extends \DateTime
{
    public function __toString()
    {
        return $this->format('U');
    }
}

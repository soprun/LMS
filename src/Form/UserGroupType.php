<?php

namespace App\Form;

use App\Entity\Courses\Course;
use App\Entity\Courses\CourseFolder;
use App\Entity\GroupPermission;
use App\Entity\UserGroup;
use App\Entity\UserGroupFolder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserGroupType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'constraints' => [
                    new NotBlank()
                ],
            ])
            ->add('courses', EntityType::class, [
                'class' => Course::class,
                'multiple' => true,
                'by_reference' => false,
            ])
            ->add('courseFolders', EntityType::class, [
                'class' => CourseFolder::class,
                'multiple' => true,
                'by_reference' => false
            ])
            ->add('groupFolder', EntityType::class, [
                'class' => UserGroupFolder::class
            ])
            ->add('groupPermissions', GroupPermissionType::class, [
                'data_class' => GroupPermission::class,
                'constraints' => [
                    new NotBlank()
                ]
            ]);

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $userGroup = $event->getData();

            if (array_key_exists('groupFolder', $userGroup) && $userGroup['groupFolder'] === 0) {
                $userGroup['groupFolder'] = null;
                $event->setData($userGroup);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserGroup::class,
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}

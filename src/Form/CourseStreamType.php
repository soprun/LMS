<?php

namespace App\Form;

use App\Entity\AbstractCourse;
use App\Entity\CourseStream;
use App\Entity\Partner;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsNull;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\Regex;

class CourseStreamType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Length(['max' => 255]),
                ],
            ])
            ->add(
                'stream',
                NumberType::class,
                [
                    'constraints' => [
                        new NotBlank(),
                        new Length(['min' => 0]),
                    ],
                ],
            )
            ->add('start_date', DateType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
                'widget' => 'single_text',

            ])
            ->add(
                'period',
                NumberType::class,
                [
                    'constraints' => [
                        new NotBlank(),
                        new Length(['min' => 0]),
                    ],
                ],
            )
            ->add(
                'active',
                CheckboxType::class,
                [
                    'false_values' => [
                        0,
                        false,
                        null,
                    ],
                ],
            )
            ->add('abstract_course', EntityType::class, [
                'class' => AbstractCourse::class,
                'constraints' => [
                    new NotBlank(),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CourseStream::class,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'course_stream';
    }

}

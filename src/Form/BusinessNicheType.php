<?php

namespace App\Form;

use App\Entity\BusinessArea;
use App\Entity\BusinessNiche;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BusinessNicheType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class)
            ->add('description', TextType::class)
            ->add('franchises', TextType::class)
            ->add('type', ChoiceType::class, ['choices' => BusinessNiche::TYPES])
            ->add('verified', CheckboxType::class)
            ->add('hasRivals', CheckboxType::class)
            ->add('main', CheckboxType::class)
            ->add(
                'area',
                EntityType::class,
                ['class' => BusinessArea::class, 'documentation' => ['type' => 'integer']]
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'data_class' => BusinessNiche::class,
        ]);
    }

}

<?php

namespace App\Form\Traction;

use App\Entity\Traction\CheckpointValue;
use App\Service\FileUploaderService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Event\PreSubmitEvent;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class CheckpointValueType extends AbstractType
{
    /**
     * @var FileUploaderService
     */
    private $fileUploader;

    public function __construct(FileUploaderService $fileUploader)
    {
        $this->fileUploader = $fileUploader;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('value', CheckboxType::class, ['constraints' => [new NotBlank()]])
            // @todo: проверка на тип файла
            ->add('file', FileType::class, ['mapped' => false])
            ->add('filename', TextType::class)
            ->addEventListener(FormEvents::PRE_SUBMIT, function (PreSubmitEvent $event) {
                $data = $event->getData();
                if (isset($data['file'])) {
                    $fileName = $this->fileUploader->uploadFile($data['file'], 'checkpoint', true);
                    $data['filename'] = 'checkpoint/' . $fileName;
                    $event->setData($data);
                }
            });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CheckpointValue::class,
            'csrf_protection' => false,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return '';
    }

}

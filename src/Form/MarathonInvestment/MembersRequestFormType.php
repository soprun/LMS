<?php

namespace App\Form\MarathonInvestment;

use App\DTO\MarathonInvestment\MarathonInvestmentMembersFiltersDTO;
use App\Entity\CourseStream;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

final class MembersRequestFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('page', NumberType::class, ['documentation' => ['type' => 'number']])
            ->add('pageSize', NumberType::class, ['documentation' => ['type' => 'number']])
            ->add('hasVerified', CheckboxType::class)
            ->add(
                'stream',
                EntityType::class,
                ['class' => CourseStream::class, 'documentation' => ['type' => 'number']]
            )
            ->add(
                'sorts',
                ChoiceType::class,
                [
                    'multiple' => true,
                    'choices' => [
                        'pointA',
                        'profit',
                        'totalAmount',
                        'totalPassed',
                        '-pointA',
                        '-profit',
                        '-totalAmount',
                        '-totalPassed',
                    ],
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class' => MarathonInvestmentMembersFiltersDTO::class,
                'csrf_protection' => false,
            ]
        );
    }

}

<?php

namespace App\Form\MarathonInvestment;

use App\Entity\CourseStream;
use App\Entity\MarathonInvestment\MarathonInvestment;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MarathonInvestmentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('week', IntegerType::class)
            ->add('planned', IntegerType::class)
            ->add('passed', IntegerType::class)
            ->add('amount', IntegerType::class)
            ->add('stream', EntityType::class, [
                'class' => CourseStream::class
            ])
            ->add('user', EntityType::class, [
                'class' => User::class
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MarathonInvestment::class,
            'csrf_protection' => false,
        ]);
    }
}

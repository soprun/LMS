<?php

namespace App\Form\MarathonInvestment;

use App\Entity\CourseStream;
use App\Entity\MarathonInvestment\UserNiche;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserNicheType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('stream', EntityType::class, [
                'class' => CourseStream::class
            ])
            ->add('title')
            ->add('description')
            ->add('rival')
            ->add('main')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserNiche::class,
            'csrf_protection' => false,
        ]);
    }
}

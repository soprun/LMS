<?php

namespace App\Form\MarathonInvestment;

use App\Entity\CourseStream;
use App\Entity\MarathonInvestment\MarathonInvestmentConfiguration;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MarathonConfigurationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pointA', IntegerType::class)
            ->add('profit', IntegerType::class)
            ->add('stream', EntityType::class, [
                'class' => CourseStream::class
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MarathonInvestmentConfiguration::class,
            'csrf_protection' => false,
        ]);
    }
}

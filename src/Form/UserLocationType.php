<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Range;

class UserLocationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'lon',
                NumberType::class,
                [
                    'constraints' => [
                        new NotBlank(),
                        new NotNull(),
                        new Range(['min' => -180, 'max' => 180]),
                    ],
                ]
            )
            ->add(
                'lat',
                NumberType::class,
                [
                    'constraints' => [
                        new NotBlank(),
                        new NotNull(),
                        new Range(['min' => -90, 'max' => 90]),
                    ],
                ]
            )
            ->setMethod('PATCH');
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return '';
    }

}

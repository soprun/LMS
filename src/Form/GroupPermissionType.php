<?php

namespace App\Form;

use App\Entity\GroupPermission;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GroupPermissionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('isAdmin')
            ->add('folderModuleAdmin')
            ->add('courseModuleAdmin')
            ->add('lessonModuleAdmin')
            ->add('taskAnswerCheckAdmin')
            ->add('userModuleAdmin')
            ->add('userGroupModuleAdmin')
            ->add('userTeamModuleAdmin')
            ->add('analyticsModelAdmin')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => GroupPermission::class,
        ]);
    }
}

<?php

namespace App\Form;

use App\Entity\Partner;
use App\Service\FileUploaderService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Event\PreSubmitEvent;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class PartnerType extends AbstractType
{
    /**
     * @var FileUploaderService
     */
    private $fileUploader;

    public function __construct(FileUploaderService $fileUploader)
    {
        $this->fileUploader = $fileUploader;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'slug',
                TextType::class,
                ['constraints' => [new NotBlank(), new Regex('/^[a-z0-9]+$/'), new Length(['max' => 255])]]
            )
            ->add(
                'file',
                FileType::class,
                [
                    'mapped' => false,
                    'constraints' => [new NotBlank(), new Image(['detectCorrupted' => true,])],
                    //@todo разобраться как описать тип поля файл
                    'documentation' => ['type' => 'object'],
                ]
            )
            ->add('logo', TextType::class, ['documentation' => false])
            ->addEventListener(FormEvents::PRE_SUBMIT, function (PreSubmitEvent $event) {
                $data = $event->getData();
                if (isset($data['file']) && $data['file'] instanceof UploadedFile) {
                    $fileName = $this->fileUploader->uploadFile($data['file'], 'partners/logo', true);
                    $data['logo'] = 'partners/logo/' . $fileName;
                    $event->setData($data);
                }
            })
            ->add('name', TextType::class, ['constraints' => [new NotBlank(), new Length(['max' => 25])]])
            ->add('description', TextType::class, ['constraints' => [new NotBlank(), new Length(['max' => 100])]])
            ->add('title', TextType::class, ['constraints' => [new NotBlank(), new Length(['max' => 50])]])
            ->add('subtitle', TextType::class, ['constraints' => [new Length(['max' => 50])]])
            ->add('url', UrlType::class, ['constraints' => [new NotBlank(), new Length(['max' => 255])]]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'data_class' => Partner::class,
            'allow_extra_fields' => true,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return '';
    }

}

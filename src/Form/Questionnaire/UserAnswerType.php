<?php

namespace App\Form\Questionnaire;

use App\Entity\Questionnaire\Question;
use App\Entity\Questionnaire\QuestionAnswerVariant;
use App\Entity\Questionnaire\UserAnswer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserAnswerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Question $question */
        $question = $options['question'];
        $variantOptions = [
            'class' => QuestionAnswerVariant::class,
            'choices' => $question->getVariants(),
        ];
        if (!$question->getVariants()->isEmpty()) {
            $variantOptions['constraints'] = [new NotBlank()];
        }

        $builder
            ->add('text')
            ->add('variant', EntityType::class, $variantOptions);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserAnswer::class,
            'question' => null,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'answer';
    }

}

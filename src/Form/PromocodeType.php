<?php

namespace App\Form;

use App\Entity\Partner;
use App\Entity\Promocode;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use DateTime;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class PromocodeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Length(['max' => 255]),
                ],
            ])
            ->add('is_unique', CheckboxType::class, [
                'constraints' => [
                    new NotBlank()
                ],
            ])
            ->add('expires_in', DateTimeType::class, [
                'widget' => 'single_text',
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('partner', EntityType::class, [
                'class' => Partner::class,
                'constraints' => [
                    new NotBlank()
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'data_class' => Promocode::class,
            'allow_extra_fields' => true
        ]);
    }

    public function getBlockPrefix()
    {
        return 'promocode';
    }
}

<?php

namespace App\Security;

use App\Entity\User;
use App\Service\FormHelper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface;

class AccessDeniedHandler implements AccessDeniedHandlerInterface
{

    private $formHelper;
    private $security;

    public function __construct(FormHelper $formHelper, Security $security)
    {
        $this->formHelper = $formHelper;
        $this->security = $security;
    }

    public function handle(Request $request, AccessDeniedException $accessDeniedException)
    {
        $actionName = 'unknownAction';
        $errorMessage = 'accessDenied';

        // определяем эндпоинт
        if ($controllerName = $request->attributes->get('_controller')) {
            $controllerNameArray = explode('::', $controllerName);
            if (array_key_exists(1, $controllerNameArray)) {
                $actionName = $controllerNameArray[1];
            }
        }

        // выводим ошибку, если есть
        /** @var User $user */
        $user = $this->security->getUser();
        if ($user->getAuthError()) {
            $errorMessage = $user->getAuthError();
        }

        $this->formHelper->addError('auth', $errorMessage);

        return $this->formHelper->getResponse($actionName);
    }

}

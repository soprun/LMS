<?php

namespace App\Security;

use App\Entity\User;
use App\Service\AuthServerHelper;
use App\Service\FormHelper;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class TokenAuthenticator extends AbstractGuardAuthenticator
{

    private $formHelper;
    private $authServerHelper;

    public function __construct(FormHelper $formHelper, AuthServerHelper $authServerHelper)
    {
        $this->formHelper = $formHelper;
        $this->authServerHelper = $authServerHelper;
    }

    public function supports(Request $request)
    {
        /*
         * Считаем каждый запрос валидным,
         * но проверяем токен, только если он есть
         */
        return true;
    }

    public function getCredentials(Request $request)
    {
        // пробуем взять токен из хедера
        $credentials = $request->headers->get('Authorization', '');

        // если в хедере нет, пробуем взять из кук
        if (
            (empty($credentials) || 'Basic' === substr($credentials, 5))
            && $request->cookies->has('accessToken')
        ) {
            return $request->cookies->get('accessToken', '');
        }

        if (0 === strpos($credentials, 'Bearer ')) {
            return substr($credentials, 7);
        }

        return $credentials;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        if (!$credentials) {
            return (new User())->setAuthError(AuthServerHelper::ERROR_INVALID_TOKEN);
        }

        return $this->authServerHelper->getUserByAccessToken($credentials);
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $this->formHelper->addError('accessToken', $this->authServerHelper::ERROR_INVALID_TOKEN);

        return $this->formHelper->getResponse('auth');
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        $this->formHelper->addError('accessToken', $this->authServerHelper::ERROR_INVALID_TOKEN);

        return $this->formHelper->getResponse('auth');
    }

    public function supportsRememberMe()
    {
        return false;
    }

}

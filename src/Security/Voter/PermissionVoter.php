<?php

namespace App\Security\Voter;

use App\Entity\BusinessNiche;
use App\Entity\Courses\Course;
use App\Entity\Courses\CourseFolder;
use App\Entity\Courses\Lesson;
use App\Entity\MarathonInvestment\MarathonInvestmentConfiguration;
use App\Entity\MarathonInvestment\UserNiche;
use App\Entity\User;
use App\Service\LikeItHelper;
use App\Service\UserHelper;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class PermissionVoter extends Voter
{
    private $security;
    private $userHelper;
    private $likeItHelper;
    private $em;

    public function __construct(
        Security $security,
        UserHelper $userHelper,
        LikeItHelper $likeItHelper,
        EntityManagerInterface $em
    ) {
        $this->security = $security;
        $this->userHelper = $userHelper;
        $this->likeItHelper = $likeItHelper;
        $this->em = $em;
    }

    protected function supports($attribute, $subject): bool
    {
        return in_array(
            $attribute,
            [
                'USER_IN_FOLDER',
                'USER_FOLDER_CREATE',
                'USER_IN_FOLDER_DUPLICATE',
                'USER_FOLDER_EDIT',
                'USER_FOLDER_DELETE',
                'USER_IN_COURSE',
                'USER_IN_COURSE_DUPLICATE',
                'USER_COURSE_IN_COURSE_FOLDER_NEW',
                'USER_COURSE_IN_COURSE_FOLDER_EDIT',
                'USER_COURSE_IN_COURSE_FOLDER_DELETE',
                'USER_IN_LESSON',
                'USER_LESSON_EDIT',
                'USER_LESSON_CREATE_BY_COURSE',
                'USER_LESSON_CREATE_BY_LESSON',
                'USER_LESSON_DELETE',
                'USER_IS_VALIDATING',
                'USER_IS_VALIDATING_CHECK',
                'USER_GROUP_LIST',
                'USER_GROUP_CREATE',
                'USER_GROUP_EDIT',
                'USER_GROUP_DELETE',

                'USER_IN_TEAM',
                'USER_TEAM_CREATE',
                'USER_TEAM_EDIT',
                'USER_TEAM_DELETE',
                'ROLE_FRANCHISEE',
                'TEAMS_ACCESS',

                'USER_ANALYTICS_VIEW',
                'USER_ANALYTICS_CREATE',
                'USER_ANALYTICS_EDIT',
                'USER_ANALYTICS_DELETE',
                'USER_USERS_LIST',
                'USER_USERS_CREATE',

                'USER_HAS_ADMIN_PERMISSION',
            ]
        );
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        /** @var User $user */
        $user = $token->getUser();

        if (!$user instanceof UserInterface) {
            return false;
        }

        // убираем не-юзеров
        if (!$user->getEmail()) {
            return false;
        }

        if ($this->security->isGranted("ROLE_BLOCKED")) {
            return false;
        }

        switch ($attribute) {
            case 'USER_HAS_ADMIN_PERMISSION':
                $permissions = $this->userHelper->getUserPermissionsByField($user, 'folderModuleAdmin');

                return $permissions['isAdmin'];
            case 'USER_FOLDER_CREATE':
                // Забираем права на папки для того, чтобы понять можно ли создавать папки
                $permissions = $this->userHelper->getUserPermissionsByField($user, 'folderModuleAdmin');

                return $permissions['isAdmin'] && (int)$permissions['folderModuleAdmin']['create'] === 2;
            case 'USER_IN_FOLDER_DUPLICATE':
                // Забираем права на папки для того, чтобы понять можно ли создавать папки
                $permissions = $this->userHelper->getUserPermissionsByField($user, 'folderModuleAdmin');

                if ($permissions['isAdmin'] && (int)$permissions['folderModuleAdmin']['create'] === 2) {
                    return true;
                }

                if ($permissions['isAdmin'] && (int)$permissions['folderModuleAdmin']['create'] === 1) {
                    // Забираем папки с курсами, к которым у пользователя есть доступ
                    $permissions = $this->userHelper->getCourseFolderPermissions($user, $subject);

                    return $this->checkPermission('folderModuleAdmin', 'create', $permissions);
                }

// Забираем папки с курсами, к которым у пользователя есть доступ
                $isExist = $this->em->getRepository(CourseFolder::class)
                    ->findByUserAndFolderIfExist($user, $subject);

                return (bool)$isExist;

            case 'USER_FOLDER_EDIT':
                // Забираем права на папки для того, чтобы понять можно ли создавать папки
                $permissions = $this->userHelper->getUserPermissionsByField($user, 'folderModuleAdmin');

                if ($permissions['isAdmin'] && (int)$permissions['folderModuleAdmin']['update'] === 2) {
                    return true;
                }

                if ($permissions['isAdmin'] && (int)$permissions['folderModuleAdmin']['update'] === 1) {
                    // Забираем папки с курсами, к которым у пользователя есть доступ
                    $permissions = $this->userHelper->getCourseFolderPermissions($user, $subject);

                    return $this->checkPermission('folderModuleAdmin', 'update', $permissions);
                }

// Забираем папки с курсами, к которым у пользователя есть доступ
                $isExist = $this->em->getRepository(CourseFolder::class)
                    ->findByUserAndFolderIfExist($user, $subject);

                return (bool)$isExist;

            case 'USER_FOLDER_DELETE':
                // Забираем права на папки для того, чтобы понять можно ли удалять папки
                $permissions = $this->userHelper->getUserPermissionsByField($user, 'folderModuleAdmin');

                if ($permissions['isAdmin'] && (int)$permissions['folderModuleAdmin']['delete'] === 2) {
                    return true;
                }

                if ($permissions['isAdmin'] && (int)$permissions['folderModuleAdmin']['delete'] === 1) {
                    // Забираем папки с курсами, к которым у пользователя есть доступ
                    $permissions = $this->userHelper->getCourseFolderPermissions($user, $subject);

                    return $this->checkPermission('folderModuleAdmin', 'delete', $permissions);
                }

// Забираем папки с курсами, к которым у пользователя есть доступ
                $isExist = $this->em->getRepository(CourseFolder::class)
                    ->findByUserAndFolderIfExist($user, $subject);

                return (bool)$isExist;

            case 'USER_IN_FOLDER':
                // Забираем права на папки для того, чтобы понять нужно ли показывать все папки
                $permissions = $this->userHelper->getUserPermissionsByField($user, 'folderModuleAdmin');

                if ($permissions['isAdmin'] && (int)$permissions['folderModuleAdmin']['read'] === 2) {
                    return true;
                }

// Забираем папки с курсами, к которым у пользователя есть доступ
                $isExist = $this->em->getRepository(CourseFolder::class)
                    ->findByUserAndFolderIfExist($user, $subject);

                return (bool)$isExist;

            case 'USER_COURSE_IN_COURSE_FOLDER_NEW':
                // Забираем права на курсы для того, чтобы понять нужно ли создавать курсы
                $permissions = $this->userHelper->getUserPermissionsByField($user, 'courseModuleAdmin');

                if ($permissions['isAdmin'] && (int)$permissions['courseModuleAdmin']['create'] === 2) {
                    return true;
                }

                if ($permissions['isAdmin'] && (int)$permissions['courseModuleAdmin']['create'] === 1) {
                    // Забираем папки с курсами, к которым у пользователя есть доступ
                    $permissionsFolder = $this->userHelper->getCourseFolderPermissions($user, $subject);

                    $folderAccess = $this->checkPermission('folderModuleAdmin', 'create', $permissionsFolder);
                    $coursesAccess = $this->checkPermission('courseModuleAdmin', 'create', $permissions);

                    return $folderAccess && $coursesAccess;
                }

                return false;
            case 'USER_IN_COURSE_DUPLICATE':
                // Забираем права на курсы для того, чтобы понять нужно ли создавать курсы
                $permissions = $this->userHelper->getUserPermissionsByField($user, 'courseModuleAdmin');

                if ($permissions['isAdmin'] && (int)$permissions['courseModuleAdmin']['create'] === 2) {
                    return true;
                }

                if ($permissions['isAdmin'] && (int)$permissions['courseModuleAdmin']['create'] === 1) {
                    // Забираем папки с курсами, к которым у пользователя есть доступ
                    $permissionsFolder = $this->userHelper->getCourseFolderPermissions($user, $subject->getFolder());

                    return $this->checkPermission('folderModuleAdmin', 'create', $permissionsFolder);
                }

                return false;

            case 'USER_COURSE_IN_COURSE_FOLDER_EDIT':
                // Забираем права на курсы для того, чтобы понять нужно ли изменять курсы
                $permissions = $this->userHelper->getUserPermissionsByField($user, 'courseModuleAdmin');

                if ($permissions['isAdmin'] && (int)$permissions['courseModuleAdmin']['update'] === 2) {
                    return true;
                }

                if ($permissions['isAdmin'] && (int)$permissions['courseModuleAdmin']['update'] === 1) {
                    // Забираем папки с курсами, к которым у пользователя есть доступ
                    $permissionsCourse = $this->userHelper->getCoursePermissions($user, $subject);

                    return $this->checkPermission('courseModuleAdmin', 'update', $permissionsCourse);
                }

                return false;

            case 'USER_COURSE_IN_COURSE_FOLDER_DELETE':
                // Забираем права на курсы для того, чтобы понять нужно ли удалять курсы
                $permissions = $this->userHelper->getUserPermissionsByField($user, 'courseModuleAdmin');

                if ($permissions['isAdmin'] && (int)$permissions['courseModuleAdmin']['delete'] === 2) {
                    return true;
                }

                if ($permissions['isAdmin'] && (int)$permissions['courseModuleAdmin']['delete'] === 1) {
                    // Забираем папки с курсами, к которым у пользователя есть доступ
                    $permissionsCourse = $this->userHelper->getCoursePermissions($user, $subject);

                    return $this->checkPermission('courseModuleAdmin', 'delete', $permissionsCourse);
                }

                return false;

            case 'USER_IN_COURSE':
                // Забираем права на папки для того, чтобы понять нужно ли показывать все папки
                $permissions = $this->userHelper->getUserPermissionsByField($user, 'courseModuleAdmin');

                if ($permissions['isAdmin'] && (int)$permissions['courseModuleAdmin']['read'] === 2) {
                    return true;
                }

// Забираем папки с курсами, к которым у пользователя есть доступ
                $isExist = $this->em->getRepository(Course::class)
                    ->findByUserAndCourseIfExist($user, $subject);

                return (bool)$isExist;
            case 'USER_IN_LESSON':
                /** @var Lesson $subject */
                // Забираем права на папки для того, чтобы понять нужно ли показывать все папки
                $permissions = $this->userHelper->getUserPermissionsByField($user, 'lessonModuleAdmin');

                if ($permissions['isAdmin'] && (int)$permissions['lessonModuleAdmin']['read'] === 2) {
                    return true;
                }

                if ($permissions['isAdmin'] && (int)$permissions['lessonModuleAdmin']['read'] === 1) {
                    // Забираем папки с курсами, к которым у пользователя есть доступ
                    $permissions = $this->userHelper->getLessonPermissionsByCourse($user, $subject->getCourse());

                    $checkPermission = $this->checkPermission('lessonModuleAdmin', 'read', $permissions);
                    if ($checkPermission) {
                        return true;
                    }

// Забираем папки с курсами, к которым у пользователя есть доступ
                    $isExist = $this->em->getRepository(Course::class)
                        ->findByUserAndCourseIfExist($user, $subject->getCourse());

                    return (bool)$isExist;
                }

// Забираем папки с курсами, к которым у пользователя есть доступ
                $isExist = $this->em->getRepository(Course::class)
                    ->findByUserAndCourseIfExist($user, $subject->getCourse());

                $opened = true;
                if ($subject->getLessonConfiguration() && $subject->getLessonConfiguration()->getDateOfStart()) {
                    $opened = $subject->getLessonConfiguration()->getDateOfStart() < (new DateTime());
                }

                return $isExist && $opened;
            case 'USER_LESSON_EDIT':
                // Забираем права на папки для того, чтобы понять нужно ли показывать все папки
                $permissions = $this->userHelper->getUserPermissionsByField($user, 'lessonModuleAdmin');

                if ($permissions['isAdmin'] && (int)$permissions['lessonModuleAdmin']['update'] === 2) {
                    return true;
                }

                $permissions = $this->userHelper->getLessonPermissionsByCourse($user, $subject->getCourse());

                return $this->checkPermission('lessonModuleAdmin', 'update', $permissions);
            case 'USER_LESSON_CREATE_BY_COURSE':
                // Забираем права на папки для того, чтобы понять нужно ли показывать все папки
                $permissions = $this->userHelper->getUserPermissionsByField($user, 'lessonModuleAdmin');

                if ($permissions['isAdmin'] && (int)$permissions['lessonModuleAdmin']['create'] === 2) {
                    return true;
                }

                $permissions = $this->userHelper->getLessonPermissionsByCourse($user, $subject);

                return $this->checkPermission('lessonModuleAdmin', 'create', $permissions);
            case 'USER_LESSON_CREATE_BY_LESSON':
                // Забираем права на папки для того, чтобы понять нужно ли показывать все папки
                $permissions = $this->userHelper->getUserPermissionsByField($user, 'lessonModuleAdmin');

                if ($permissions['isAdmin'] && (int)$permissions['lessonModuleAdmin']['create'] === 2) {
                    return true;
                }

                $permissions = $this->userHelper->getLessonPermissionsByCourse($user, $subject->getCourse());

                return $this->checkPermission('lessonModuleAdmin', 'create', $permissions);
            case 'USER_LESSON_DELETE':
                // Забираем права на папки для того, чтобы понять нужно ли показывать все папки
                $permissions = $this->userHelper->getUserPermissionsByField($user, 'lessonModuleAdmin');

                if ($permissions['isAdmin'] && (int)$permissions['lessonModuleAdmin']['delete'] === 2) {
                    return true;
                }

                $permissions = $this->userHelper->getLessonPermissionsByCourse($user, $subject->getCourse());

                return $this->checkPermission('lessonModuleAdmin', 'delete', $permissions);
            case 'USER_IS_VALIDATING':
                // Забираем права на папки для того, чтобы понять нужно ли показывать все папки
                $permissions = $this->userHelper->getUserPermissionsByField($user, 'taskAnswerCheckAdmin');

                if ($permissions['isAdmin'] || $permissions['taskAnswerCheckAdmin']['read'] > 0) {
                    return true;
                }

                break;
            case 'USER_IS_VALIDATING_CHECK':
                $permissions = $this->userHelper->getUserPermissionsByField($user, 'taskAnswerCheckAdmin');

                if (
                    $permissions['isAdmin'] ||
                    ($permissions['taskAnswerCheckAdmin']['create'] > 0 ||
                        $permissions['taskAnswerCheckAdmin']['update'] > 0)
                ) {
                    return true;
                }
                break;

            case 'USER_GROUP_LIST':
                $permissions = $this->userHelper->getUserPermissionsByField($user, 'userGroupModuleAdmin');

                if ($permissions['isAdmin'] && (int)$permissions['userGroupModuleAdmin']['read'] === 2) {
                    return true;
                }

                if ($permissions['isAdmin'] && (int)$permissions['userGroupModuleAdmin']['read'] === 1) {
                    return $this->checkPermission('userGroupModuleAdmin', 'read', $permissions);
                }

                break;

            case 'USER_GROUP_CREATE':
                $permissions = $this->userHelper->getUserPermissionsByField($user, 'userGroupModuleAdmin');

                if ($permissions['isAdmin'] && (int)$permissions['userGroupModuleAdmin']['create'] === 2) {
                    return true;
                }

                if ($permissions['isAdmin'] && (int)$permissions['userGroupModuleAdmin']['create'] === 1) {
                    return $this->checkPermission('userGroupModuleAdmin', 'create', $permissions);
                }

                break;

            case 'USER_GROUP_EDIT':
                $permissions = $this->userHelper->getUserPermissionsByField($user, 'userGroupModuleAdmin');

                if ($permissions['isAdmin'] && (int)$permissions['userGroupModuleAdmin']['update'] === 2) {
                    return true;
                }

                if ($permissions['isAdmin'] && (int)$permissions['userGroupModuleAdmin']['update'] === 1) {
                    return $this->checkPermission('userGroupModuleAdmin', 'update', $permissions);
                }

                break;

            case 'USER_GROUP_DELETE':
                $permissions = $this->userHelper->getUserPermissionsByField($user, 'userGroupModuleAdmin');

                if ($permissions['isAdmin'] && (int)$permissions['userGroupModuleAdmin']['delete'] === 2) {
                    return true;
                }

                if ($permissions['isAdmin'] && (int)$permissions['userGroupModuleAdmin']['delete'] === 1) {
                    return $this->checkPermission('userGroupModuleAdmin', 'delete', $permissions);
                }

                break;

            case 'USER_IN_TEAM':
                $permissions = $this->userHelper->getUserPermissionsByField($user, 'userTeamModuleAdmin');

                if ($permissions['isAdmin'] && (int)$permissions['userTeamModuleAdmin']['read'] === 2) {
                    return true;
                }

                if ($permissions['isAdmin'] && (int)$permissions['userTeamModuleAdmin']['read'] === 1) {
                    return $this->checkPermission('userTeamModuleAdmin', 'read', $permissions);
                }

                break;

            case 'USER_TEAM_CREATE':
                $permissions = $this->userHelper->getUserPermissionsByField($user, 'userTeamModuleAdmin');

                if ($permissions['isAdmin'] && (int)$permissions['userTeamModuleAdmin']['create'] === 2) {
                    return true;
                }

                if ($permissions['isAdmin'] && (int)$permissions['userTeamModuleAdmin']['create'] === 1) {
                    return $this->checkPermission('userTeamModuleAdmin', 'create', $permissions);
                }

                break;

            case 'USER_TEAM_EDIT':
                $permissions = $this->userHelper->getUserPermissionsByField($user, 'userTeamModuleAdmin');

                if ($permissions['isAdmin'] && (int)$permissions['userTeamModuleAdmin']['update'] === 2) {
                    return true;
                }

                if ($permissions['isAdmin'] && (int)$permissions['userTeamModuleAdmin']['update'] === 1) {
                    return $this->checkPermission('userTeamModuleAdmin', 'update', $permissions);
                }

                break;

            case 'USER_TEAM_DELETE':
                $permissions = $this->userHelper->getUserPermissionsByField($user, 'userTeamModuleAdmin');

                if ($permissions['isAdmin'] && (int)$permissions['userTeamModuleAdmin']['delete'] === 2) {
                    return true;
                }

                if ($permissions['isAdmin'] && (int)$permissions['userTeamModuleAdmin']['delete'] === 1) {
                    return $this->checkPermission('userTeamModuleAdmin', 'delete', $permissions);
                }

                break;

            case 'ROLE_FRANCHISEE':
                return !empty($this->likeItHelper->getFranchiseeCities($user));

            case 'TEAMS_ACCESS':
                if (!$this->voteOnAttribute('USER_TEAM_EDIT', $subject, $token)) {
                    return $this->voteOnAttribute('ROLE_FRANCHISEE', $subject, $token);
                }

                return true;

            case 'USER_ANALYTICS_VIEW':
                $permissions = $this->userHelper->getUserPermissionsByField($user, 'analyticsModelAdmin');

                if ($permissions['isAdmin'] || $permissions['analyticsModelAdmin']['read'] > 0) {
                    return true;
                }

                if (!$subject) {
                    return false;
                }

                if ($subject === $user) {
                    return true;
                }

                if (get_class($subject) === MarathonInvestmentConfiguration::class) {
                    return $subject->getUser() === $user;
                }

                if (get_class($subject) === UserNiche::class) {
                    return $subject->getUser() === $user;
                }

                break;

            case 'USER_ANALYTICS_CREATE':
                $permissions = $this->userHelper->getUserPermissionsByField($user, 'analyticsModelAdmin');

                if ($permissions['isAdmin'] && $permissions['analyticsModelAdmin']['create'] > 0) {
                    return true;
                }

                if (get_class($subject) === UserNiche::class) {
                    return $subject->getUser() === $user;
                }

                break;

            case 'USER_ANALYTICS_EDIT':
                $permissions = $this->userHelper->getUserPermissionsByField($user, 'analyticsModelAdmin');

                if ($permissions['isAdmin'] && $permissions['analyticsModelAdmin']['update'] > 0) {
                    return true;
                }

                if (get_class($subject) === MarathonInvestmentConfiguration::class) {
                    return $subject->getUser() === $user;
                }

                break;

            case 'USER_ANALYTICS_DELETE':
                $permissions = $this->userHelper->getUserPermissionsByField($user, 'analyticsModelAdmin');

                if ($permissions['isAdmin'] && $permissions['analyticsModelAdmin']['delete'] > 0) {
                    return true;
                }

                break;

            case 'USER_USERS_LIST':
                $permissions = $this->userHelper->getUserPermissionsByField($user, 'userModuleAdmin');

                if ($permissions['isAdmin'] && $permissions['userModuleAdmin']['read'] > 0) {
                    return true;
                }

                break;

            case 'USER_USERS_CREATE':
                $permissions = $this->userHelper->getUserPermissionsByField($user, 'userModuleAdmin');

                if ($permissions['isAdmin'] && $permissions['userModuleAdmin']['create'] > 0) {
                    return true;
                }

                break;
        }

        return false;
    }

    protected function checkPermission(string $module, string $action, ?array $permissions = []): bool
    {
        if (empty($permissions)) {
            return false;
        }

        return $permissions['isAdmin'] &&
            array_key_exists($module, $permissions) &&
            array_key_exists($action, $permissions[$module]) &&
            $permissions[$module][$action] > 0;
    }

}

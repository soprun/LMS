var Encore = require('@symfony/webpack-encore');
var path = require('path');
var dotenv = require('dotenv');

const globImporter = require('node-sass-glob-importer');

// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    // directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // public path used by the web server to access the output path
    .setPublicPath('/build')
    // only needed for CDN's or sub-directory deploy
    //.setManifestKeyPrefix('build/')

    .configureDefinePlugin(options => {
      const env = dotenv.config({
        path: __dirname + '/.env.local'
      });


      if (env.error)
        throw env.error;

      options['process.env'].AUTH_SERVER_URL = JSON.stringify(env.parsed.AUTH_SERVER_URL);
      options['process.env'].APP_DOMAIN = JSON.stringify(env.parsed.APP_DOMAIN);
      options['process.env'].ERP_SERVER_URL = JSON.stringify(env.parsed.ERP_SERVER_URL);
      options['process.env'].SENTRY_DSN = JSON.stringify(env.parsed.SENTRY_DSN);
    })

    /*
     * ENTRY CONFIG
     *
     * Add 1 entry for each "page" of your app
     * (including one that's included on every page - e.g. "app")
     *
     * Each entry will result in one JavaScript file (e.g. app.js)
     * and one CSS file (e.g. app.css) if your JavaScript imports CSS.
     */
    .addEntry('lms', './assets/vue/Lms/index.js')
    //.addEntry('page1', './assets/js/page1.js')
    //.addEntry('page2', './assets/js/page2.js')

    .addAliases({ // псевдонимы путей для более удобной загрузки
        // вне зависимости от уровня вложенности файл в приложении
        '@lms': path.resolve(__dirname, 'assets/vue/Lms'),
        '@sass': path.resolve(__dirname, 'public/lms/sass'),
        '@ui': path.resolve(__dirname, 'assets/vue/Lms/components/UiKit'),
        '@views': path.resolve(__dirname, 'assets/vue/Lms/views'),
        //для упрощения переноса комонентов с ERP
        toolbox: path.resolve(__dirname, 'assets/vue/Lms'),
      }
    )
    // When enabled, Webpack "splits" your files into smaller pieces for greater optimization.
    .splitEntryChunks()

    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    .enableSingleRuntimeChunk()

    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    // enables @babel/preset-env polyfills
    // .configureBabelPresetEnv((config) => {
    //     config.useBuiltIns = 'usage';
    //     config.corejs = 3;
    // })

    .configureBabel((babelConfig) => {
        babelConfig.plugins.push('@babel/plugin-transform-runtime');
    }, {
        useBuiltIns: 'usage',
        corejs: 3
    })

    // enables Sass/SCSS support
    .addLoader({
      test: /\.s[ac]ss$/i,
      use: [
        "style-loader",
        {
          loader: "css-loader",
          options: {
            sourceMap: true,
          },
        },
        { loader: 'postcss-loader' },
        {
          loader: "sass-loader",
          options: {
            sourceMap: true,
            importer: globImporter()
          },
        },
      ],
    })

    // .enableSassLoader()

    .enableVueLoader()

    .addLoader({test: /\.pug$/, loader: 'pug-plain-loader'})

    // uncomment if you use TypeScript
    //.enableTypeScriptLoader()

    // uncomment to get integrity="..." attributes on your script & link tags
    // requires WebpackEncoreBundle 1.4 or higher
    .enableIntegrityHashes(Encore.isProduction())

    // uncomment if you're having problems with a jQuery plugin
    //.autoProvidejQuery()

    // uncomment if you use API Platform Admin (composer req api-admin)
    //.enableReactPreset()
    //.addEntry('admin', './assets/js/admin.js')
;

module.exports = Encore.getWebpackConfig();

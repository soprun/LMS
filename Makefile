SHELL := /bin/bash
USER_ID := $(shell id -u)
USER_GROUP := $(shell id -g)
OS = $(shell uname -s)

ifeq ($(shell echo $(OS) | egrep -c 'Darwin|FreeBSD|OpenBSD|DragonFly'),1)
DATE := $(shell date -j -r $(shell git log -n 1 --format="%ct") +%Y%m%d%H%M)
CPUS := $(shell sysctl hw.ncpu | awk '{print $$2}')
else
DATE := $(shell date --utc --date="$(GIT_DATE)" +%Y%m%d%H%M)
CPUS := $(shell nproc)
endif

# Load environment variables from .env
-include .env
-include .env.local

ifndef APP_ENV
$(error The env:APP_ENV variable is missing.)
endif

ifeq ($(filter $(APP_ENV),test dev prod),)
$(error The env:APP_ENV variable is invalid.)
endif

PROJECT_DIR = $(shell cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)
PROJECT_NAME = $(APP_PROJECT_NAME)

ifndef PROJECT_NAME
PROJECT_NAME = $(shell basename $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST)))) | tr '[:upper:]' '[:lower:]')
endif

## exec: ln -sf ./.env.dist ./.env
# ifeq ($(shell ! test -f "$(PROJECT_DIR)/.env.local" && echo -n yes),yes)
# $(shell touch "$(PROJECT_DIR)/.env.local")
# $(info "sdadasd")
# $(error "found an error!")
# endif

# Allows to pass arguments into make, eg. make TASK some args. Credits: https://stackoverflow.com/a/6273809
ARGS = $(filter-out $@,$(MAKECMDGOALS))
# ARGS = $(shell ARGS="$(filter-out $@,$(MAKECMDGOALS))" && echo $${ARGS:-${1}})

COLOR_RESET := \033[0m
COLOR_RED := \033[0;31m
COLOR_YELLOW := \033[0;33m
COLOR_GREEN := \033[0;32m
COLOR_BLUE := \033[0;34m

# ref: https://stackoverflow.com/questions/6273608/how-to-pass-argument-to-makefile-from-command-line
%: # do not change - this is useful for passing args to make
	@: # do not change - this is useful for passing args to make

# Self-Documented Makefile see https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.DEFAULT_GOAL := help

.PHONY: help
help:
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[34m<target>\033[0m\n"} /^[a-zA-Z0-9_-]+:.*?##/ { printf "  \033[36m%-27s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

#######################################################################
##@ [Standard Targets for Users 🎯 ]
#######################################################################
## https://www.gnu.org/software/make/manual/html_node/Standard-Targets.html#Standard-Targets

.PHONY: all
all: ## Run all targets
	@printf "$(COLOR_YELLOW)[WARNING] $(COLOR_RED)%s\n$(COLOR_RESET)" "Run all targets"
	@exit 0

commands_required := php symfony composer node npm yarn brew docker

.PHONY: install
install: ## Run install targets
	@printf  "\n\e[4m\e[1m%s\n\n" "Checking the programs required for the build are installed..."

	@for command in $(commands_required) ; do \
  		if command -v "$$command" >/dev/null 2>&1; then \
        	printf "\e[1m$(COLOR_BLUE)[info]: \e[0mcommand: $(COLOR_GREEN)\"$$command\"$(COLOR_RESET) is installed.\n$(COLOR_RESET)"; \
        else \
        	printf "\e[1m$(COLOR_RED)[error]: \e[0mcommand: $(COLOR_YELLOW)\"$$command\"$(COLOR_RESET) is required! 🚨️\n\n$(COLOR_RESET)"; \
        	printf "\e[94mExecute command: $(COLOR_RESET)\e[4mbrew install $$command\n$(COLOR_RESET)"; \
        	printf "\e[94mLink to brew: \e[1mhttps://formulae.brew.sh/formula/$$command\n\n$(COLOR_RESET)"; \
        	exit 1; \
        fi \
	done

	@printf "\n\e[4m\e[1m%s\n\n" "Deployment on local..."

	@printf "\e[1m$(COLOR_BLUE)[info]:$(COLOR_RESET) %s\n" "Create a local Certificate Authority"
	@symfony server:ca:install >/dev/null 2>&1

	@printf "\e[1m$(COLOR_BLUE)[info]:$(COLOR_RESET) %s\n" "Attach a local domain for the proxy"
	@symfony proxy:domain:attach lms.toolbox >/dev/null 2>&1

	@printf "\e[1m$(COLOR_BLUE)[info]:$(COLOR_RESET) %s\n" "Start the local proxy server"
	@symfony proxy:start >/dev/null 2>&1

	@#if [ ! -f "$(PROJECT_DIR)/.env.local" ];  \
#		then touch "$(PROJECT_DIR)/.env.local"; \
#		printf "\e[1m$(COLOR_BLUE)[info]:$(COLOR_RESET) %s\n" "Create a new $(COLOR_YELLOW).env.local$(COLOR_RESET) file!"; \
#	fi

	@composer dump-env dev --empty

	@printf "\e[1m$(COLOR_BLUE)[info]:$(COLOR_RESET) %s\n" "Install composer dependencies!"
	@make --silent composer-install

	@printf "\e[1m$(COLOR_BLUE)[info]:$(COLOR_RESET) %s\n" "Install npm dependencies!"
	@make --silent npm-install

	@printf "\e[1m$(COLOR_BLUE)[info]:$(COLOR_RESET) %s\n" "Frontend build!"
	@npm run-script dev

	@printf "\n\n\e[1m$(COLOR_GREEN)[success]:$(COLOR_RESET) %s\n" "Install successfully ✅"

.PHONY: test
test: phpunit-unit phpunit-integration; ## Run test targets
	$(error "Run test targets")

.PHONY: clean
clean: ## Delete all files
	@rm -rf $(PROJECT_DIR)/var/cache/*
	@rm -rf $(PROJECT_DIR)/var/log/*
	@#docker system prune --volumes

#######################################################################
##@ [Composer & Node.js 🐘 ]
#######################################################################

.PHONY: composer-install
composer-install: ## Install composer dependencies
	composer install \
		--ansi \
		--no-suggest \
		--optimize-autoloader \
		--classmap-authoritative \
		--apcu-autoloader \
		--ignore-platform-reqs \
		--no-interaction \
		--prefer-dist \
		--profile \
		--no-progress

composer-outdated: ## Composer outdated
	composer outdated --direct --minor-only --outdated

.PHONY: npm-install
npm-install: ## Install node.js dependencies
	npm install

.PHONY: yarn-install
yarn-install: ## Install node.js dependencies
	yarn install

#######################################################################
## [Docker variables]
#######################################################################

docker_build_args := \
	--build-arg USER_ID=$(USER_ID) \
	--build-arg USER_GROUP=$(USER_GROUP) \
	--build-arg APP_PHP_VERSION=$(APP_PHP_VERSION) \
	--build-arg APP_COMPOSER_VERSION=$(APP_COMPOSER_VERSION) \
	--build-arg APP_NODE_VERSION=$(APP_NODE_VERSION) \
	--build-arg APP_NGINX_VERSION=$(APP_NGINX_VERSION) \
	--build-arg APP_VERSION=$(APP_VERSION) \
	--build-arg APP_PROJECT_NAME=$(APP_PROJECT_NAME)

docker_bin := $(shell command -v docker 2> /dev/null)
docker_compose := \
	$(docker_bin) compose \
	--project-name $(PROJECT_NAME) \
	--project-directory $(PROJECT_DIR) \
	--file $(PROJECT_DIR)/docker-compose.yml

ifeq ($(shell test -e $(PROJECT_DIR)/docker-compose.override.yml && echo -n yes),yes)
	docker_compose += --file $(PROJECT_DIR)/docker-compose.override.yml
endif

container_default := app
container_default_user := www-data
container_default_id := $(shell docker container list --filter name='^/$(PROJECT_NAME)_$(container_default)' --format "{{ .ID }}")
container_default_exec = $(shell ARGS="$(filter-out $@,$(MAKECMDGOALS))" && echo $${ARGS:-${container_default}})

# container_default_exec := $(docker_compose) exec --user $(container_default_user):$(USER_GROUP) $(container_default)
# container_default_exec_root := $(docker_compose) exec $(container_default)

#######################################################################
##@ [Docker compose 🐳 ] ...
#######################################################################

.PHONY: build
build: CMD=build $(docker_build_args) --force-rm --pull $(ARGS) ## Build or rebuild services

.PHONY: rebuild
rebuild: ## Rebuild and start all services
	@make build $(ARGS)
	@make up $(ARGS)

.PHONY: up
up: CMD=up --detach --no-recreate --remove-orphans $(ARGS) ## Create and start all services (in background) for development

.PHONY: stop
stop: CMD=stop $(ARGS) ## Stop services

.PHONY: down
down: CMD=down $(ARGS) ## Stop and remove containers, networks

.PHONY: logs
logs: CMD=logs --follow --tail 50 $(ARGS) ## View output from containers

.PHONY: ps
ps: CMD=ps --all $(ARGS) ## List containers

.PHONY: exec-user
exec-user: CMD=exec --user $(container_default_user):$(USER_GROUP) $(container_default_exec) bash ## Execute a command in a running default container.

.PHONY: exec-root
exec-root: CMD=exec $(container_default_exec) bash ## Execute a command in a running container.

.PHONY: exec
exec: CMD=exec $(container_default_exec) bash ## Execute a command in a running container.

.PHONY: pull
pull: CMD=pull $(ARGS) ## Pull service images

.PHONY: images
images: CMD=images $(ARGS) ## List images used by the created containers

.PHONY: services
services: CMD=convert --services ## Print the service names

.PHONY: volumes
volumes: CMD=convert --volumes ## Print the volume names

.PHONY: prune
prune: ## Remove all unused containers, networks, images, and optionally, volumes.
	@docker system prune --volumes

# Usage: `make docker CMD="ps --services"`
# Usage: `make docker CMD="config"`
# Usage: `make docker CMD="build --parallel --pull --force-rm --no-cache"`
.PHONY: docker
docker build up stop down logs ps pull images services volumes exec-user exec-root exec: env-local
	@$(docker_compose) $(CMD)

# see: https://medium.com/redbubble/running-a-docker-container-as-a-non-root-user-7d2e00f8ee15
container-uid:
	@$(CONTAINER_DEFAULT_EXEC) bash -c '\echo "I am `whoami`. My uid is `id -u`." && echo "Docker runs!"' && echo success

#######################################################################
##@ [PHP Code Quality Tools 🛠 ]
#######################################################################

.PHONY: code-quality lint phpcs phpcbf phpmd security-check

code-quality: lint phpcs phpmd ## Run all test quality control
	@echo "$(COLOR_RED)Tasks successfully fulfilled!$(COLOR_RESET)"

lint: ## Run lint
	@./bin/console lint:container --quiet >/dev/null 2>&1
	@./bin/console lint:yaml ./config/
	@./bin/console lint:twig ./templates/

# see: https://github.com/squizlabs/PHP_CodeSniffer
phpcs: ## PHP_CodeSniffer detects violations of a defined coding standard.
	@./vendor/bin/phpcs \
		--standard=$(PROJECT_DIR)/phpcs.xml.dist \
		--parallel=$(CPUS) \
		--report=full \
		--colors

phpcbf: ## PHP Code Beautifier and Fixer fixes violations of a defined coding standard.
	@./vendor/bin/phpcbf \
		--standard=$(PROJECT_DIR)/phpcs.xml.dist \
		--parallel=$(CPUS) \
		--colors

# use: make phpmd or make phpmd Controller
phpmd: ## PHP Mess Detector https://phpmd.org
	@./vendor/bin/phpmd \
		$(PROJECT_DIR)/src/$(filter-out $@,$(MAKECMDGOALS)) \
		ansi \
		cleancode,codesize,controversial,design

deptrac: ## Deptrac https://github.com/qossmic/deptrac
	@./vendor/bin/deptrac \
		analyse \
		$(PROJECT_DIR)/depfile.yaml

phplint: ## PHPLint https://github.com/overtrue/phplint
	@./vendor/bin/phplint \
		--jobs=$(CPUS) \
		--configuration=$(PROJECT_DIR)/.ci/.phplint.yml

hadolint: ## Haskell Dockerfile Linter https://github.com/hadolint/hadolint
	@for file in $(shell file $(shell find $(PROJECT_DIR)/docker -type f \( -name "Dockerfile" -or -name "*.dockerfile" \) -print) | grep 'text' | cut -d: -f1 | sort -u ) ; do \
		echo -e "\n\033[0;34m$$file\033[0m"; \
		docker run --rm --interactive hadolint/hadolint < $$file; \
	done

shellcheck: ## ShellCheck finds bugs in your shell scripts: https://www.shellcheck.net
	@for file in $(shell file $(shell find $(PROJECT_DIR)/docker -type f -print) | grep 'shell script' | cut -d: -f1 | sort -u ) ; do \
		echo -e "\n\033[0;34m$$file\033[0m"; \
		shellcheck --check-sourced --external-sources $$file; \
	done

security-check:	## Check security issues in project dependencies
	@symfony security:check

#######################################################################
##@ [PHP Static Analysis Tool 🛠 ]
#######################################################################

.PHONY: static-analysis, phpstan, psalm, psalm-alter, psalm-baseline, psalm-taint-analysis, phpmetrics

static-analysis: phpstan, psalm, phpmetrics ## Run all test scenarios
	@echo "$(COLOR_RED)Tasks successfully fulfilled!$(COLOR_RESET)"

phpstan: ## PHPStan https://github.com/phpstan/phpstan
	@./vendor/bin/phpstan analyse \
		--configuration $(PROJECT_DIR)/phpstan.neon \
		--memory-limit=4G \
		--xdebug \
		--ansi \
		--generate-baseline $(PROJECT_DIR)/public/report/phpstan-baseline.neon

psalm_baseline_file := $(PROJECT_DIR)/public/report/psalm-baseline.xml
psalm_command := \
	--config=$(PROJECT_DIR)/psalm.xml \
	--threads=$(CPUS)

psalm: ## Psalm https://psalm.dev/
	@export PSALM_ALLOW_XDEBUG=1; php -dxdebug.mode=develop,trace ./vendor/bin/psalm $(psalm_command)

psalm-alter: ## Psalm Psalter
	@export PSALM_ALLOW_XDEBUG=1; php -dxdebug.mode=develop,trace ./vendor/bin/psalm $(psalm_command) \
		--use-baseline=$(psalm_baseline_file) \
		--alter \
		--issues=all

psalm-baseline: ## Psalm baseline
	@export PSALM_ALLOW_XDEBUG=1; php -dxdebug.mode=develop,trace ./vendor/bin/psalm $(psalm_command) \
		--taint-analysis \
		--ignore-baseline \
		--set-baseline=$(psalm_baseline_file)

psalm-taint-analysis: ## Security Analysis in Psalm. https://psalm.dev/docs/security_analysis/
	 @export PSALM_ALLOW_XDEBUG=1; php -dxdebug.mode=develop,trace ./vendor/bin/psalm $(psalm_command) \
 		--use-baseline=$(psalm_baseline_file) \
 		--taint-analysis \
 		--dump-taint-graph=$(PROJECT_DIR)/public/report/psalm-taints.dot \
	@dot -Tsvg -o $(PROJECT_DIR)/public/report/psalm-taints.svg $(PROJECT_DIR)/public/report/psalm-taints.dot

phpmetrics: ## PhpMetrics https://phpmetrics.org
	@./vendor/bin/phpmetrics \
		--config=$(PROJECT_DIR)/phpmetrics.json \
		$(PROJECT_DIR)/src

#######################################################################
##@ [PHP Testiong Tool 🛠 ]
#######################################################################

.PHONY: phpunit
phpunit: ## PHPUnit https://phpunit.de
	@php -dxdebug.mode=coverage ./vendor/bin/phpunit \
		--configuration $(PROJECT_DIR)/phpunit.xml.dist \
		--testdox

.PHONY: phpunit-unit
phpunit-unit: ## PHPUnit https://phpunit.de
	@php -dxdebug.mode=coverage ./vendor/bin/phpunit \
		--configuration $(PROJECT_DIR)/phpunit.xml.dist \
		--testsuite "UnitTest" \
		--testdox

.PHONY: phpunit-integration
phpunit-integration: ## PHPUnit https://phpunit.de
	@php -dxdebug.mode=coverage ./vendor/bin/phpunit \
		--configuration $(PROJECT_DIR)/phpunit.xml.dist \
		--testsuite "IntegrationTest" \
		--testdox

#######################################################################
##@ [Symfony server] run a local web server
#######################################################################

.PHONY: server-start, server-stop, server-log, server-status, server-prod, server-restart

server-start: CMD=server:start --daemon ## Run a local web server
server-stop: CMD=server:stop ## Stop the local web server
server-log: CMD=server:log ## Display local web server logs
server-status: CMD=server:status ## Get the local web server status
server-prod: CMD=server:prod ## Switch a project to use Symfony's production environmen

server-restart: server-stop server-start ## Symfony webserver restart

server-start server-stop server-log server-status server-prod:;
	@symfony proxy:start &>/dev/null
	@symfony $(CMD)

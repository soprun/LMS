import Vuex from 'vuex'
import $store from '../store'

export default [
  {
    path: '/registration',
    name: 'registration',
    meta: {layout: 'auth', view: 'registration', noSecure: true},
    component: () => import('../views/Security/Auth/Register')
  },
  {
    path: '/registration/needEmail',
    name: 'registrationNeedEmail',
    meta: {layout: 'auth'},
    component: () => import('../views/Security/Registration/NeedEmail')
  },
  {
    path: '/login',
    name: 'login',
    meta: {layout: 'auth', view: 'login', noSecure: true},
    component: () => import('../views/Security/Auth/Login')
  },
  {
    path: '/logout',
    name: 'logout',
    meta: {layout: 'empty'},
    component: () => import('../views/Security/Logout/index')
  },
  {
    path: '/resetpassword',
    name: 'resetpassword',
    meta: {layout: 'auth', noSecure: true, noRefresh: true},
    component: () => import('../views/Security/Resetpassword/index')
  },
  {
    path: '/resetpassword/:hash',
    name: 'resetpasswordHash',
    meta: {layout: 'auth', noSecure: true, noRefresh: true},
    component: () => import('../views/Security/Resetpassword/NewPassword/index')
  },
  {
    path: '/icons',
    name: 'icons',
    meta: {layout: 'empty'},
    props: {printAll: true},
    component: () => import('../components/Icon')
  },
  {
    path: '/courses/folder/list',
    name: 'courses',
    meta: {layout: 'ui', docTitle: 'Папки курсов'},
    component: () => import('../views/Courses')
  },
  {
    path: '/courses/folder/:folderId',
    name: 'coursesFolder',
    meta: {layout: 'ui'},
    component: () => import('../views/Courses/Folder')
  },
  {
    path: '/courses/course/:courseId',
    name: 'coursesCourse',
    meta: {layout: 'ui', isCourse: true},
    component: () => import('../views/Courses/Course')
  },
  {
    path: '/courses/course/:courseId/points_shop',
    name: 'coursesCoursePointsShop',
    meta: {layout: 'ui'},
    component: () => import('../views/Courses/PointsShop')
  },
  {
    path: '/courses/course/:courseId/points_shop/shop',
    name: 'coursesCoursePointsShopShop',
    meta: {layout: 'ui'},
    component: () => import('../views/Courses/PointsShop/layouts/Shop')
  },
  {
    path: '/courses/course/:courseId/points_shop/history',
    name: 'coursesCoursePointsShopHistory',
    meta: {layout: 'ui'},
    component: () => import('../views/Courses/PointsShop/History')
  },
  {
    path: '/courses/lesson/:lessonId',
    name: 'coursesLesson',
    meta: {layout: 'ui', isCourse: true},
    component: () => import('../views/Courses/Lesson')
  },
  {
    path: '/answers/validating',
    name: 'answersValidating',
    meta: {layout: 'ui', docTitle: 'Проверка заданий'},
    component: () => import('../views/Answers/Validating/index')
  },
  {
    path: '/answers/validating/list',
    name: 'validatingList',
    meta: {layout: 'ui'},
    component: () => import('../views/Answers/Validating/List')
  },
  {
    path: '/answers/validating/feed',
    name: 'validatingFeed',
    meta: {layout: 'ui', docTitle: 'Лента ответов'},
    component: () => import('../views/Answers/Validating/Feed')
  },
  {
    path: '/answers/feed',
    name: 'answersFeed',
    meta: {layout: 'ui'},
    component: () => import('../views/Answers/Feed/index')
  },
  {
    path: '/answers/analytics/validators',
    name: 'answersAnalyticsValidators',
    meta: {layout: 'ui', docTitle: 'Аналитика проверяющих'},
    component: () => import('../views/Answers/Analytics/Validators')
  },
  {
    path: '/answers/comments',
    name: 'answersComments',
    meta: {layout: 'ui', docTitle: 'Комментарии к заданиям'},
    component: () => import('../views/Answers/Comments')
  },
  {
    path: '/users/list',
    name: 'users',
    meta: {layout: 'ui', docTitle: 'Пользователи'},
    component: () => import('../views/Users')
  },
  {
    path: '/teams/allocated',
    name: 'usersTeam',
    meta: {layout: 'ui', docTitle: 'Команды', mode: 'allocated'},
    component: () => import('../views/TeamGroup')
  },
  {
    path: '/teams/unallocated',
    name: 'usersTeamUnallocated',
    meta: {layout: 'ui', docTitle: 'Нераспределенные участники', mode: 'unallocated'},
    component: () => import('../views/TeamGroup')
  },
  {
    path: '/partners',
    name: 'partners',
    meta: {layout: 'ui', docTitle: 'Партнёры'},
    component: () => import('../views/Partners'),
    beforeEnter: async (to, from, next) => {
      await $store.dispatch('auth/getUserInfo');
      let user = $store.state['auth'].data.user;
      if(user.isSpeedCourse || user.isHundredCourse || user.isAdmin) {
        next()
      }
      else {
        next('/courses/folder/list')
      }
    }
  },
  {
    path: '/users/team/:teamId/users',
    name: 'usersTeamMembers',
    meta: {layout: 'ui', docTitle: 'Пользователи в команде', headerPrevLink: '/users/team'},
    component: () => import('../views/Users/Team/Users')
  },
  {
    path: '/users/group/list',
    name: 'usersGroups',
    meta: {layout: 'ui', docTitle: 'Группы'},
    component: () => import('../views/Users/Groups')
  },
  {
    path: '/user/settings',
    name: 'userSettings',
    meta: {layout: 'ui', docTitle: 'Профиль'},
    component: () => import('../views/Users/Settings')
  },
  {
    path: '/user/settings/resetpassword',
    name: 'resetPassword',
    meta: {layout: 'ui', docTitle: 'Смена пароля'},
    component: () => import('../views/Users/Settings/ResetPassword')
  },
  {
    path: '/users/group/:groupId/users',
    name: 'userGroupMembers',
    meta: {layout: 'ui', docTitle: 'Пользователи в группе', headerPrevLink: '/users/group/list'},
    component: () => import('../views/Users/Groups/Users')
  },
  {
    path: '/users/points',
    name: 'usersPoints',
    meta: {layout: 'ui', docTitle: 'Просмотр баллов по группам'},
    component: () => import('../views/Users/Points')
  },
  {
    path:'/analytics/msg',
    name:'reportMsg',
    meta:{layout:'ui', docTitle: 'Отчетность МСГ'},
    component:() => import('../views/TractionMsg')
  },
  {
    path:'/rating/hundred',
    name: 'ratingHundred',
    meta: { layout:'ui', docTitle: 'Рейтинг участников Сотки'},
    component: () => import('../views/Rating/Hundred'),
  },
  {
    path:'/rating/hundred/user/:id',
    name: 'ratingHundredUser',
    meta: { layout:'ui', docTitle: 'Профиль участника Сотки'},
    component: () => import('../views/Rating/Hundred/User'),
  },
  {
    path:'/rating/speed',
    name: 'ratingSpeed',
    meta: { layout:'ui', docTitle: 'Рейтинг участников Скорости'},
    component: () => import('../views/Rating/Speed'),
  },
  {
    path:'/rating/speed/user/:id',
    name: 'ratingSpeedUser',
    meta: { layout:'ui', docTitle: 'Профиль участника Скорости'},
    component: () => import('../views/Rating/Speed/User'),
  },
  {
    path: '/rating/unified',
    name: 'ratingUnifiedGlobal',
    meta: { layout:'ui', docTitle: 'Рейтинг участников'},
    component: () => import('../views/Rating/Unified/Global/UnifiedGlobal'),
    beforeEnter: async (to, from, next) => {
      if (!$store.state.auth.meta.getUserData) {
        await $store.dispatch('auth/getUserInfo');
      }
      const user = $store.state.auth.data.user
      if (user.isAdmin || user.permissions.analyticsModelAdmin.read > 0) {
        next()
      } else {
        next('/courses/folder/list')
      }
    }
  },
  {
    path: '/rating/unified/user/:userId',
    name: 'ratingUnifiedUser',
    meta: { layout:'ui', docTitle: 'Профиль участника'},
    component: () => import('../views/Rating/Unified/User/UnifiedUser'),
    props: (route) => ({ userId: Number.parseInt(route.params.userId) }),
    beforeEnter: async (to, from, next) => {
      if (!$store.state.auth.meta.getUserData) {
        await $store.dispatch('auth/getUserInfo');
      }
      const user = $store.state.auth.data.user
      if (user.id === Number.parseInt(to.params['userId']) || user.isAdmin || user.permissions.analyticsModelAdmin.read > 0) {
        next()
      } else {
        next('/courses/folder/list')
      }
    }
  },
  {
    path:'/rating/msa',
    name: 'ratingMsa',
    meta: { layout:'ui', docTitle: 'Рейтинг участников МсА'},
    component: () => import('../views/Rating/MSA'),
  },
  {
    path: '/rating/msa/user/:id',
    name: 'ratingMsaUser',
    meta: { layout: 'ui', docTitle: 'Профиль участника МсА', headerPrevLink: '/rating/msa'},
    component: () => import('../views/Rating/MSA/User'),
  },
  {
    path: '/rating/investments',
    name: 'ratingInvestments',
    meta: { layout: 'ui', docTitle: 'Аналитика Марафон инвестиций' },
    component: () => import('../views/Rating/Investments')
  },
  {
    path: '/rating/investments/registration',
    name: 'ratingInvestmentsRegistration',
    meta: { layout: 'empty', docTitle: 'Анкета участника МИ' },
    component: () => import('../views/Rating/Investments/Registration')
  },
  {
    path:'/rating/teams',
    name: 'ratingTeams',
    meta: { layout:'ui', docTitle: 'Рейтинг по командам'},
    component: () => import('../views/Rating/Teams'),
  },
  {
    path: '/questionnaire',
    name: 'questionnaire',
    meta: {layout: 'empty', docTitle: 'Продолжить регистрацию'},
    component: () => import('../views/Questionnaire')
  },
  {
    path: '/questionnaire/telegram',
    name: 'questionnaireTelegram',
    meta: {layout: 'empty', docTitle: 'Продолжить регистрацию'},
    component: () => import('../views/Questionnaire/Telegram')
  },
  {
    path: '/questionnaire/hundred',
    name: 'questionnaireHundred',
    meta: {layout: 'empty', docTitle: 'Сотка'},
    component: () => import('../views/Questionnaire/hundred')
  },
  {
    path: '/questionnaire/toggle-course',
    name: 'questionnaireTtoggleCourse',
    meta: {layout: 'empty', docTitle: 'Продолжить регистрацию'},
    component: () => import('../views/Questionnaire/ToggleCourse')
  },
  {
    path: '/traction-analytics/speed-and-hundred',
    name: 'tractionSpeedHundred',
    meta: {layout: 'ui', docTitle: 'Общая аналитика'},
    component: () => import('../views/Rating/SpeedAndHundred')
  },
  {
    path: '/traction-analytics/reports',
    name: 'tractionReports',
    meta: {layout: 'ui', docTitle: 'Общая аналитика - Отчеты'},
    component: () => import('../views/Rating/Reports/SpeedAndHundredReport')
  },
  {
    path: '/club',
    name: 'club',
    meta: {
      layout: 'ui',
      docTitle: 'Клуб',
      search: true,
      searchPlaceholder: 'Поиск по участникам',
      searchValueName: 'name',
      searchFilter: true,
      filterStoreName: 'club'
    },
    component: () => import('../views/Club')
  },
  {
    path: '/profile-club',
    name: 'clubUser',
    meta: {layout: 'ui', headerPrevLink: '/club'},
    component: () => import('../views/Club/User')
  },
  {
    path: '/profile-club/:userId',
    name: 'clubUserId',
    meta: {layout: 'ui', headerPrevLink: '/club'},
    component: () => import('../views/Club/User')
  },

  // Редиректы
  {
    path:'/rating/hundred/user',
    name: 'ratingHundredMyUser',
    beforeEnter: async (to, from, next) => {
      await $store.dispatch('auth/getUserInfo');
      let user = $store.state['auth'].data.user

      next(`/rating/hundred/user/${user.id}`)
    }
  },
  {
    path:'/rating/msa/user',
    name: 'ratingMsaMyUser',
    beforeEnter: async (to, from, next) => {
      await $store.dispatch('auth/getUserInfo');
      let user = $store.state['auth'].data.user

      next(`/rating/msa/user/${user.id}`)
    }
  },
  {
    path:'/rating/speed/user',
    name: 'ratingSpeedMyUser',
    beforeEnter: async (to, from, next) => {
      await $store.dispatch('auth/getUserInfo');
      let user = $store.state['auth'].data.user

      next(`/rating/speed/user/${user.id}`)
    }
  },
  {
    path:'/rating/investments/user',
    name: 'ratingInvestmentsMyUser',
    beforeEnter: async (to, from, next) => {
      if (!$store.state.auth.meta.getUserData) {
        await $store.dispatch('auth/getUserInfo');
      }
      const user = $store.state.auth.data.user

      next(`/rating/investments/user/${user.id}`)
    }
  },
  {
    path:'/rating/unified/user',
    name: 'ratingHundredSpeedMyUser',
    beforeEnter: async (to, from, next) => {
      if (!$store.state.auth.meta.getUserData) {
        await $store.dispatch('auth/getUserInfo');
      }
      const user = $store.state.auth.data.user

      next(`/rating/unified/user/${user.id}`)
    }
  },
]


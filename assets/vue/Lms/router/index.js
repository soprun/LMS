import Vue from "vue"
import VueRouter from "vue-router"
import store from "@lms/store"
import lmsRoutes from "@lms/router/lmsRoutes";

Vue.use(VueRouter);

import parseJWT from "@lms/functions/parseJWT"
import getFingerprint from "@lms/functions/getFingerprint"
import cookieMethod from "@lms/functions/cookieMethod"

const router = new VueRouter({
  mode: 'history',
  routes: [...lmsRoutes]
})

router.onError(error => {
  if (/ chunk \d* failed./i.test(error.message)) {
    window.location.reload()
  }
})

router.beforeEach(async (to, from, next) => {
  if(!store.state.auth.data.fingerprint) {
    let fingerprint = await getFingerprint()
    await store.dispatch('auth/setData', {path: 'fingerprint', value: fingerprint})
  }
  let tokens = await findTokens(to, from)

  if((from.path === '/' && to.meta.noSecure) || to.meta.noSecure) { // верно, если роут не проверят токен
    if(!tokens || to.meta.noRefresh)
      next() // Если токено
    else
      next('/courses/folder/list') // если есть токены, отправляем на нормальный роут
  } else { // верно, если роут проверяет токен
    if (!tokens) { // токенов нет - разлогинить
      next(false)
      await store.dispatch('auth/userNoAccess', {path: to.path})
    } else
      next()
  }
})

let findTokens = (to, from) => {
  return new Promise( async resolve => {

    let accessToken = await cookieMethod({name: 'accessToken', method: 'get'})
    let refreshToken = await cookieMethod({name: 'refreshToken', method: 'get'})

    if (!refreshToken || to.meta.noRefresh) {
      resolve(false)
      return
    }

    if (accessToken) {
      let token = parseJWT(accessToken)
      let expireDate = new Date(token.exp * 1000)
      let now = new Date()
      now.setSeconds(now.getSeconds() + 2)

      if (now.getTime() >= expireDate.getTime()) { // Токен просрочен
        await store.dispatch('auth/setMeta', {path: 'accessTokenExpired', value: true})
        await store.dispatch('auth/updateTokens')
      }
    } else {
      await store.dispatch('auth/setMeta', {path: 'accessTokenExpired', value: true})
      await store.dispatch('auth/updateTokens')
    }

    resolve(true)
  })
}

export default router
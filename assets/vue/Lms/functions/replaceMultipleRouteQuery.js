import router from "@lms/router"

export default function replaceMultipleRouteQuery(queries = []){
  let query = _.cloneDeep(router.currentRoute.query)

  queries.forEach(q => {
    let {name, value, method} = q

    if(method === 'replace') {
      query[name] = value
      if(value === '')
        delete query[name]
    }

    if(method === 'add') {
      if(query[name]) {
        let regex = new RegExp("("+value+")\,?")
        query[name] = query[name].replace(regex, query[name])
        query[name] += ','+value
      } else {
        query[name] = value
      }
    }

    if(method === 'toggle') {
      if (query[name]) {
        let regex = new RegExp("(" + value + ")\,?")
        if (query[name].match(regex)) {
          query[name] = query[name].replace(regex, '')
        } else {
          query[name] += ',' + value
        }
      } else {
        query[name] = value
      }
      query[name] = query[name].replace(/(,{2,})/, ',')
      query[name] = query[name].replace(/(,$)/, '')

      if (!query[name])
        delete query[name]
    }
  })

  if(!_.isEqual(router.currentRoute.query, query))
    router.replace({query})
}
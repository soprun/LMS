import TurndownService from "turndown"

let htmlToMarkdown = new TurndownService({})

htmlToMarkdown.addRule('strikethrough', {
  filter: ['del', 's', 'strike'],
  replacement: function (content) {
    return '~lt~ ' + content + ' ~lt~'
  }
})

htmlToMarkdown.addRule('h2', {
  filter: ['h2'],
  replacement: function (content) {
    return '=== ' + content + ' ==='
  }
})

htmlToMarkdown.addRule('h3', {
  filter: ['h3'],
  replacement: function (content) {
    return '### ' + content + ' ###'
  }
})

htmlToMarkdown.addRule('br', {
  filter: ['br'],
  replacement: function (content) {
    return '&br&'
  }
})

htmlToMarkdown.addRule('p', {
  filter: ['p'],
  replacement: function (content) {
    return '~p~' + content + '~p~'
  }
})

htmlToMarkdown.addRule('ol', {
  filter: ['ol'],
  replacement: function (content) {
    return '=ol= ' + content + ' =ol='
  }
})

htmlToMarkdown.addRule('ul', {
  filter: ['ul'],
  replacement: function (content) {
    return '=ul= ' + content + ' =ul='
  }
})

htmlToMarkdown.addRule('li', {
  filter: ['li'],
  replacement: function (content) {
    return '-li- ' + content + ' -li-'
  }
})

htmlToMarkdown.addRule('i', {
  filter: ['i'],
  replacement: function (content) {
    return '-i- ' + content + ' -i-'
  }
})

htmlToMarkdown.addRule('b', {
  filter: ['b'],
  replacement: function (content) {
    return '-b- ' + content + ' -b-'
  }
})

htmlToMarkdown.addRule('a', {
  filter: ['a'],
  replacement: function (content, node) {
    return '-a- ['+node.getAttribute('href')+']' + '['+content+']' + ' -a-'
  }
})

export default htmlToMarkdown
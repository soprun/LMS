function textToHtmlWithLinks(string) {
  if(!string || typeof string !== 'string')
    return

  let html = string.replace(/<[^>]+>/ig, '')
  html = html.replace(
    /((https|http):\/\/\S+)/ig,
    '<a class="hover__link" style="color: #6979F8; text-decoration: none;" href="$1" target="_blank">$1</a>'
  )

  return html
}

export default textToHtmlWithLinks
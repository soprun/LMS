import * as Fingerprint2 from 'fingerprintjs2'

export default function getFingerprint () {
  return new Promise((resolve, reject) => {
    let checkCoincidence = (field, list)=>{
      return list.find(item => field === item)
    }

    async function getHash () {

      try {
        const components = await Fingerprint2.getPromise()
        const values = components.map(component => {
          if (checkCoincidence(component.key, [
            'screenResolution',
            'availableScreenResolution',
            'timezoneOffset',
            'timezone',
            'plugins',
            'fonts',
            'audio',
          ])) return
          
          return component.value
        })

        return String(Fingerprint2.x64hash128(values.join(''), 31))
      } catch (e) {
        reject(e)
      }
    }

    if (window.requestIdleCallback) {
      requestIdleCallback(async () => resolve(await getHash()))
    } else {
      setTimeout(async () => resolve(await getHash()), 500)
    }
  })
}
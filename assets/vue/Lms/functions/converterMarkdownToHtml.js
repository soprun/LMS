import showdown from 'showdown'

let myext = function () {
  const brExt = {
    type: 'lang',
    regex: /&br&/gm,
    replace: '<br />'
  }
  const strikethrough = {
    type: 'lang',
    regex: /~lt~ (.+?) ~lt~/gm,
    replace: '<strike>$1</strike>'
  }
  const h3 = {
    type: 'lang',
    regex: /### (.+?) ###/gm,
    replace: '<h3>$1</h3>'
  }
  const h2 = {
    type: 'lang',
    regex: /=== (.+?) ===/gm,
    replace: '<h2>$1</h2>'
  }
  const p = {
    type: 'lang',
    regex: /~p~(.+?)~p~/gm,
    replace: '<p>$1</p>'
  }
  const ol = {
    type: 'lang',
    regex: /=ol= (.+?) =ol=/gm,
    replace: '<ol>$1</ol>'
  }
  const ul = {
    type: 'lang',
    regex: /=ul= (.+?) =ul=/gm,
    replace: '<ul>$1</ul>'
  }
  const li = {
    type: 'lang',
    regex: /-li- (.+?) -li-/gm,
    replace: '<li>$1</li>'
  }
  const i = {
    type: 'lang',
    regex: /-i- (.+?) -i-/gm,
    replace: '<i>$1</i>'
  }
  const b = {
    type: 'lang',
    regex: /-b- (.+?) -b-/gm,
    replace: '<b>$1</b>'
  }
  const a = {
    type: 'lang',
    regex: /-a- \[([^\]]+)]\[(.+?)] -a-/gm,
    replace: '<a href="$1" target="_blank">$2</a>'
  }
  const numDot = {
    type: 'lang',
    regex: /(\d)\\(\.)/gm,
    replace: '$1$2'
  }
  const less = {
    type: 'lang',
    regex: /\\>/gm,
    replace: '>'
  }
  const minus = {
    type: 'lang',
    regex: /\\-/gm,
    replace: '-'
  }
  const equal = {
    type: 'lang',
    regex: /\\=/gm,
    replace: '='
  }
  const star = {
    type: 'lang',
    regex: /\\(\*)/gm,
    replace: '*'
  }
  const dot = {
    type: 'lang',
    regex: /\\•/gm,
    replace: '•'
  }
  return [brExt, strikethrough, h3, h2, p, ol, ul, li, i, b, a, numDot, less, minus, dot, equal, star];
}

showdown.extension('myext', myext)

let markdownToHtml = new showdown.Converter({extensions: ['myext']})

markdownToHtml.setOption('openLinksInNewWindow', true)
markdownToHtml.setOption('noHeaderId', true)

export default markdownToHtml
import router from "@lms/router"

export default function getQueryObject() {
  let query = {};
    Object.entries(router.currentRoute.params).forEach( entry => {
      query[entry[0]] = entry[1]
    })
  return query
}
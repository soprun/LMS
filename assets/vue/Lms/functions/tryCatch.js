export default async (func) => {
  try {
    return await func()
  } catch (e) {
    console.log(e)
  }
}

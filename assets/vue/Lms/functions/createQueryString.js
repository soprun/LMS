import router from "@lms/router"

export default function createQueryString() {
  // let query = '';

  // Object.entries(router.currentRoute.query).forEach((entry) => {
  //   if(entry[1])
  //     query += entry[0]+'='+entry[1]+'&'
  // })

  // query ? query = '?'+query : query

  // query = query.replace(/&$/, '')

  let url = new URL(window.location.href)

  return url.search
}
export default function cookieMethod({name, value, expired = {minutes: 60}, method = 'get'}) {
  return new Promise(async resolve => {
    if (method === 'get') {
      if (!name) {
        resolve()
        return
      }

      let cookieString = document.cookie

      let regex = new RegExp(name + "=[^;]*")
      let matchArr = cookieString.match(regex)

      if (matchArr !== null) {
        let result = matchArr[0].split('=')
        resolve(result[1])
        return
      }
    }

    if (method === 'set') {
      if (!name) {
        resolve()
        return
      }

      if(value === undefined)
        value = ''

      let hostname = window.location.host.split('.')
      hostname = hostname.length > 2 ?
        hostname[hostname.length - 2] + '.' + hostname[hostname.length - 1] : hostname.join('.')

      document.cookie = `${name}=${value}; domain=${hostname}; max-age=0; path=/;` // Обнуление куки, если она была

      let expireSeconds = 0
      if (expired.minutes)
        expireSeconds = 60 * expired.minutes

      if (expired.hours)
        expireSeconds = 60 * 60 * expired.hours + expireSeconds

      if (!value)
        expireSeconds = 0

      document.cookie = `${name}=${value}; domain=${hostname}; max-age=${expireSeconds}; path=/; secure; samesite=lax;`
    }
    resolve()
  })
}
export default function notificationMessagesFilter(value){
  const error = '[Ошибка]:'
  const types = {
    'Request failed with status code 404': `${error} Неизвестная ссылка при api-запросе`,
  }

  return types[value] ? types[value] : value
}
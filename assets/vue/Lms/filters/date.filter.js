import moment from 'moment'

export default function dateFilter(value, format) {
  if(value && format) {
    switch (format) {
      case 'YYYY-MM-DD':
        return moment(value).format('YYYY-MM-DD')
      case 'YYYY-MM-DD HH:mm:ss':
        return moment(value).format('YYYY-MM-DD HH:mm:ss')
      case 'DD.MM.YYYY HH:mm:ss':
        return moment(value).format('DD.MM.YYYY HH:mm:ss')
      case 'YYYY-MM-DDTHH:mm:ss':
        return moment(value).format('YYYY-MM-DDTHH:mm:ss')
      case 'YYYY-MM-DDTHH:mm:ssZ':
        return moment(value).format('YYYY-MM-DDTHH:mm:ssZ')
      case 'DD.MM.YYYY':
        return moment(value).format('DD.MM.YYYY')
      case 'DD/MM/YYYY':
        return moment(value).format('DD/MM/YYYY')
      case 'MM.DD.YYYY':
        return moment(value).format('MM.DD.YYYY')
      case 'HH:MM:SS':
        moment.locale('ru')
        return moment(value).format('LTS')
      case 'DD-MM':
        return moment(value).format('DD-MM')
      case 'DD.MM':
        return moment(value).format('DD.MM')
      default:
        return moment(value).format('DD.MM.YYYY')
    }
  } else { // Если дата уже в каком-то формате типа 29.03.2019
    return value;
  }
}

export default function numberFormat(value, noDot = false) {
  const chunkRight = (arr, size) => {
    const rm = arr.length % size;
    return rm ?
      [arr.slice(0, rm), ..._.chunk(arr.slice(rm), size)] : _.chunk(arr, size);
  };


  value = (''+value).replace(/[^\d-.,]/g, '').replace(/,/g, '.')
  let splitNumber,
    dotString = ''

  if(value.includes('.')){
    dotString = '.'+value.split('.')[1]
    value = value.split('.')[0]
  }

  if(value.includes('-')){
    let valueWithoutMinus = value.replace('-', '')
    splitNumber = chunkRight(valueWithoutMinus, 3)
    splitNumber = splitNumber.map(nums => {
      return typeof nums === 'object' ?
        nums.reduce((sum, current) => sum + current + '')
        : nums
    })
    splitNumber = '-'+splitNumber.join(' ')
  } else {
    splitNumber = chunkRight(value, 3)
    splitNumber = splitNumber.map(nums => {
      return typeof nums === 'object' ?
        nums.reduce((sum, current) => sum + current + '')
        : nums
    })

    dotString = noDot ? '' : dotString
    return splitNumber.join(' ')+dotString
  }
  return splitNumber+dotString
}
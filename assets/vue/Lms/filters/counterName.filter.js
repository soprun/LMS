export default function counterNameFilter(value, namesArray){
  let storeLength = value.length || value;

	if (storeLength % 10 == 1 && storeLength != 11)
		return storeLength + " " + namesArray[0]
		
	else if (
			storeLength % 10 >= 2 &&
			storeLength % 10 <= 4 &&
			(storeLength < 11 || storeLength > 14))

		return storeLength + " " + namesArray[1]

	else
		return storeLength + " " + (namesArray[2] || namesArray[1])

}
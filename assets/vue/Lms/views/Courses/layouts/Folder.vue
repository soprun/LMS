<template lang="pug">
  .courses-card
    .courses-card__image(
      v-if="folder.img"
      :style="{backgroundImage: `url('`+folder.img.url+`')`}"
      @click.self="openFolder"
    )

    .courses-card__info(@click.self="openFolder")
      .courses-card__info-head
        .courses-card__info-head-top
          p.courses-card__name(@click="openFolder")
            | {{folder.name}}
          UiOptionsList(
            v-if="optionsFields.length"
            :optionsFields="optionsFields"
          )
        .courses-card__description(
          @click="openFolder"
          v-if="folder.description"
        )
          p {{folder.description}}

      .courses-card__numbers(@click="openFolder")
        .courses-card__numbers-item
          p.courses-card__numbers-item-name
            | Курсов внутри:&nbsp;
          p.courses-card__numbers-item-amount
            | {{folder.courseCount}}

</template>

<script>
  import {mapState, mapActions} from 'vuex';
  import MainAsideRight from '@lms/components/MainAsideRight/index';
  import Aside from './Aside'

  export default {
    props: ['folder'],
    components: {MainAsideRight, Aside},
    data() {
      return {
        options: [
          {
            text: 'Редактировать',
            func: this.editFolder,
            icon: 'edit',
          },
          {
            text: 'Дублировать',
            func: this.cloneFolders,
            icon: 'copy',
            confirm: true,
            confirmTitle: 'Вы уверены, что хотите дублировать эту папку?',
            confirmDescription: 'Папка будет дублирована со всеми курсами',
            confirmApplyText: 'Дублировать',
            confirmCancelText: 'Отменить',
          },
          {
            text: 'Удалить',
            func: this.deleteFolders,
            icon: 'delete',
            red: true,
            confirm: true,
            confirmTitle: 'Вы уверены, что хотите удалить эту папку?',
            confirmDescription: 'Папка будет удалена навсегда',
            confirmApplyText: 'Удалить',
            confirmCancelText: 'Отменить',
          },
        ]
      }
    },
    computed: {
      ...mapState('coursesFolder', {
        isOpen: s => s.aside.meta.isOpen,
        asideName: s => s.data.asideName,
      }),
      ...mapState('auth', {
        $permissions: s => s.data.user.permissions.folderModuleAdmin
      }),
      optionsFields() {
        let arr = []

        if (!this.folder.permissions || !this.folder.permissions.isAdmin || !this.folder.permissions.folderModuleAdmin)
          return arr

        if (this.$permissionsUpdate || +this.folder.permissions.folderModuleAdmin.update)
          arr.push(this.options[0])
        if (this.$permissionsCreate || +this.folder.permissions.folderModuleAdmin.create)
          arr.push(this.options[1])
        if (this.$permissionsDelete || +this.folder.permissions.folderModuleAdmin.delete)
          arr.push(this.options[2])

        return arr
      },
    },
    methods: {
      ...mapActions('coursesFolder', [
        'deleteFolder',
        'cloneFolder'
      ]),
      openFolder() {
        if (this.folder.courseCount === 1 && !this.$permissionsCreate) {
          this.$router.push({path: '/courses/course/' + this.folder.firstCourseId})
          return;
        }
        this.$store.dispatch('coursesCourse/setAsideData', {path: 'folderId', value: this.folder.id})
        this.$router.push({path: '/courses/folder/' + this.folder.id})
      },
      editFolder() {
        this.$store.dispatch('asides/addAside', {
          data: {
            name: this.asideName,
            title: 'Редактирование папки',
            subtitle: '',
            folder: _.cloneDeep(this.folder)
          },
          meta: {
            hasChanges: false
          }
        })
      },
      deleteFolders() {
        this.deleteFolder(this.folder.id)
      },
      cloneFolders() {
        this.cloneFolder({folder: this.folder})
      }
    },
  }
</script>
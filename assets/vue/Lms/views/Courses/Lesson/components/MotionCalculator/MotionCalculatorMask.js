export default (() => {
  let mask = [];
  for (let i = 1; i < 32; i++) {
    mask.push({
      date: i,
      body: {
        expenses: "",
        screenings: "",
        click: "",
        applications: "",
        impressionsConversion: "",
        applicationConversion: "",
        visitorPrice: "",
        applicationPrice: "",
      }
    });
  }
  return mask;
})()
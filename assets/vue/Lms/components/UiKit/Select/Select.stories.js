import Select from "./index"

export default {
  title: 'UI-kit/Select',
  component: Select,
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  data: ()=> {
    return {
      selectValue: 'option', 
    }
  },
  components: { Select },
  template: `
  <div>
    <Select v-bind="$props" v-model="selectValue">
    </Select>
    <h1>{{selectValue}}</h1>
  </div>
  `,
});

export const Default = Template.bind();
Default.args = {
  title: "Выберите статус задания",
  option: [
    {title: 'Свойство', value: 'option'},
    {title: 'Свойство1', value: 'option1'},
    {title: 'Свойство2', value: 'option2'},
    {title: 'Значение 0', value: '0'},
    {title: 'Без value'}
  ] 
};

export const AppendBody = Template.bind();
AppendBody.args = {
  title: "Выберите статус задания",
  option: [
    {title: 'Свойство', value: 'option'},
    {title: 'Свойство1', value: 'option1'},
    {title: 'Свойство2', value: 'option2'},
    {title: 'Значение 0', value: '0'},
    {title: 'Без value'}
  ],
  appendBody: true,
};

export const Placeholder = Template.bind();
Placeholder.args = {
  title: "Выберите статус задания",
  option: [
    {title: 'Свойство', value: 'option'},
    {title: 'Свойство1', value: 'option1'},
    {title: 'Свойство2', value: 'option2'},
    {title: 'Значение 0', value: '0'},
  ],
  placeholder: 'значение'
};

export const Popup = Template.bind();
Popup.args = {
  title: "Выберите статус задания",
  option: [
    {title: 'Свойство', value: 'option'},
    {title: 'Свойство1', value: 'option1'},
    {title: 'Свойство2', value: 'option2'},
    {title: 'Значение 0', value: '0'},
  ],
  popup: {
    msg: "ошибка",
    status: "error"
  }
};

export const ZIndex = Template.bind();
ZIndex.args = {
  title: "Выберите статус задания",
  option: [
    {title: 'Свойство', value: 'option'},
    {title: 'Свойство1', value: 'option1'},
    {title: 'Свойство2', value: 'option2'},
    {title: 'Значение 0', value: '0'},
  ],
  zIndex: 1001
};

import SelectFilter from "./index"

export default {
  title: 'UI-kit/SelectFilter',
  component: SelectFilter,
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  data: ()=> {
    return {
      selectValue: '', 
    }
  },
  components: { SelectFilter },
  methods: {
    alertItem({title , value}) {
      alert(`title: ${title}, value: ${value}`);
    }
  },
  template: `
  <div>
    <SelectFilter v-bind="$props" v-model="selectValue" @iconClick="alertItem">
    ${args.slotContent || ''}
    </SelectFilter>
    <h1>{{selectValue}}</h1>
  </div>
  `,
});

export const Default = Template.bind();
Default.args = {
  title: "Выберите статус задания",
  option: [
    {title: 'Свойство', value: 'option'},
    {title: 'Свойство1', value: 'option1'},
    {title: 'Свойство2', value: 'option2'},
    {title: 'Значение 0', value: '0'},
  ] 
};

export const appendBody = Template.bind();
appendBody.args = {
  title: "Выберите статус задания",
  option: [
    {title: 'Свойство', value: 'option'},
    {title: 'Свойство1', value: 'option1'},
    {title: 'Свойство2', value: 'option2'},
    {title: 'Значение 0', value: '0'},
  ],
  appendBody: true
};

export const OptionIcon = Template.bind();
OptionIcon.args = {
  title: "Выберите статус задания",
  optionIcon: 'ui-pencil',
  option: [
    {title: 'Свойство', value: 'option'},
    {title: 'Свойство1', value: 'option1'},
    {title: 'Свойство2', value: 'option2'},
    {title: 'Значение 0', value: '0'},
  ] 
};

export const Slot = Template.bind();
Slot.args = {
  title: "Выберите статус задания",
  optionIcon: 'ui-pencil',
  option: [
    {title: 'Свойство', value: 'option'},
    {title: 'Свойство1', value: 'option1'},
    {title: 'Свойство2', value: 'option2'},
    {title: 'Значение 0', value: '0'},
  ],
  slotContent: `
    <UiButton :wide="true" style="padding: 6px 24px">Создать новую роль</UiButton>
  `
};


export const Placeholder = Template.bind();
Placeholder.args = {
  title: "Выберите статус задания",
  option: [
    {title: 'Свойство', value: 'option'},
    {title: 'Свойство1', value: 'option1'},
    {title: 'Свойство2', value: 'option2'},
    {title: 'Значение 0', value: '0'},
  ],
  placeholder: 'Выберите значение'
};

export const Popup = Template.bind();
Popup.args = {
  title: "Выберите статус задания",
  option: [
    {title: 'Свойство', value: 'option'},
    {title: 'Свойство1', value: 'option1'},
    {title: 'Свойство2', value: 'option2'},
    {title: 'Значение 0', value: '0'},
  ],
  popup: {
    msg: "ошибка",
    status: "error"
  }
};

export const ZIndex = Template.bind();
ZIndex.args = {
  title: "Выберите статус задания",
  option: [
    {title: 'Свойство', value: 'option'},
    {title: 'Свойство1', value: 'option1'},
    {title: 'Свойство2', value: 'option2'},
    {title: 'Значение 0', value: '0'},
  ],
  zIndex: 1001
};

export const MaxHeight = Template.bind();
MaxHeight.args = {
  title: "Выберите статус задания",
  option: [
    {title: 'Свойство', value: 'option'},
    {title: 'Свойство1', value: 'option1'},
    {title: 'Свойство2', value: 'option2'},
    {title: 'Значение 0', value: '0'},
  ],
  maxHeight: 200,
};

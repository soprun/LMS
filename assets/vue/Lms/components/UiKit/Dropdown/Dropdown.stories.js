import Dropdown from "./index"

export default {
  title: 'UI-kit/Dropdown',
  component: Dropdown,
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  data: ()=> {
    return {
      selectValue: [], 
    }
  },
  components: { Dropdown },
  template: `
  <div>
    <Dropdown style="max-width: 400px" v-bind="$props" v-model="selectValue">
    </Dropdown>
    <h1>value(id): {{selectValue}}</h1>
  </div>
  `,
});

export const Default = Template.bind();
Default.args = {
  title: "Поиск",
  placeholder: "Какой группе вы хотите дать доступ?",
  option: [
    {id: "0", title: 'Свойство',},
    {id: "1", title: 'Свойство1',},
    {id: "2", title: 'Свойство2',},
    {id: "3", title: 'Значение 0',},
    {id: "4", title: 'Без value',}
  ] 
};

export const Error = Template.bind();
Error.args = {
  title: "Выберите статус задания",
  option: [
    {id: "0", title: 'Свойство',},
    {id: "1", title: 'Свойство1',},
    {id: "2", title: 'Свойство2',},
    {id: "3", title: 'Значение 0',},
    {id: "4", title: 'Без value',}
  ],
  placeholder: 'значение',
  error: true,
};

export const Popup = Template.bind();
Popup.args = {
  title: "Выберите статус задания",
  option: [
    {id: "0", title: 'Свойство',},
    {id: "1", title: 'Свойство1',},
    {id: "2", title: 'Свойство2',},
    {id: "3", title: 'Значение 0',},
    {id: "4", title: 'Без value',}
  ],
  placeholder: 'значение',
  error: true,
  popup: {msg: "Ошибка"},
};

import Input from './Input.vue'

export default {
  title: 'UI-kit/Input',
  component: Input,
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  data: ()=> {
    return {
      text: ''
    }
  },
  components: { Input },
  template: `<Input v-bind="$props" v-model="text"></Input>`,
});

export const Text = Template.bind({});
Text.args = {
  title: 'Курс',
  placeholder: 'Какое название?',
};

export const NoTitle = Template.bind({});
NoTitle.args = {
  placeholder: 'Какое название?',
}; 

export const BadInput = Template.bind({});
BadInput.args = {
  title: 'Курс',
  placeholder: 'Какое название?',
  status: 'error',
};

export const Popup = Template.bind({});
Popup.args = {
  title: 'Курс',
  placeholder: 'Какое название?',
  status: 'error',
  popup: {
    msg: 'Не забудьте заполнить это поле :)'
  },
};

export const LongPopup = Template.bind({});
LongPopup.args = {
  title: 'Курс',
  placeholder: 'Какое название?',
  error: true,
  popup: {
    msg: 'Не забудьте заполнить это поле :) Не забудьте заполнить это поле :)'
  },
};

export const IconInput = Template.bind({});
IconInput.args = {
  title: 'Поиск',
  placeholder: 'Какое название?',
  icon:'search'
};

export const PopupStatus = Template.bind({});
PopupStatus.args = {
  title: 'Поиск',
  placeholder: 'Какое название?',
  icon:'search',
  popup: {
    error: true,
    msg: "Ошибка из попапа"
  }
};
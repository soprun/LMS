import CustomDropdown from "./index"

export default {
  title: 'UI-kit/CustomDropdown',
  component: CustomDropdown,
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  data: ()=> {
    return {
      selectValue: 'option', 
    }
  },
  components: { CustomDropdown },
  template: `
  <div>
    <CustomDropdown v-bind="$props" v-model="selectValue">
     {{slotContent}}
    </CustomDropdown>
  </div>
  `,
});

export const Default = Template.bind();
Default.args = {
  title: "Выберите статус задания",
  placeholder: "test",
  slotContent: "content"
};

export const AppendBody = Template.bind();
AppendBody.args = {
  title: "Выберите статус задания",
  placeholder: "test",
  slotContent: "content",
  appendBody: true,
};


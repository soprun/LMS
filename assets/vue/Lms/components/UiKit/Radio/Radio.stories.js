import Radio from './index'

export default {
  title: 'UI-kit/Radio',
  component: Radio,
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  data: ()=> {
    return {
      radio: '',
    }
  },
  components: { Radio },
  template: `
  <form>
    <Radio v-bind="$props" v-model="radio" :val="'radio1'">${args.slotContent || "Значение 1"}</Radio>
    <Radio v-bind="$props" v-model="radio" :val="'radio2'">${args.slotContent || "Значение 2"}</Radio>
    <Radio v-bind="$props" v-model="radio" :val="'radio3'">${args.slotContent || "Значение 3"}</Radio>
    <div>
      <p>{{radio}}</p>
    </div>
  </form>`,
});

export const Default = Template.bind();
Default.args = {
};

export const Disabled = Template.bind();
Disabled.args = {
  disabled: true,
};

export const Reverse = Template.bind();
Reverse.args = {
  reverse: true,
};

export const Wide = Template.bind();
Wide.args = {
  wide: true,
};

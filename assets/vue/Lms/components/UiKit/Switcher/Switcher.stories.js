import Switcher from "./index"

export default {
  title: 'UI-kit/Switcher',
  component: Switcher,
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  data: ()=> {
    return {
      isChecked: false, 
    }
  },
  components: { Switcher },
  template: `
    <Switcher v-bind="$props" v-model="isChecked">${args.slotContent || "Открыть для всех"}</Switcher>
  `,
});

export const Default = Template.bind();
Default.args = {
};

export const Disabled = Template.bind();
Disabled.args = {
  disabled: true,
};


export const Reverse = Template.bind();
Reverse.args = {
  reverse: true,
};

export const Wide = Template.bind();
Wide.args = {
  wide: true,
};

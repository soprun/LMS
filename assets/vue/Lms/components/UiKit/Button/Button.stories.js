import Button from './index'

export default {
  title: 'UI-kit/Button',
  component: Button,
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  data: ()=> {
    return {
      text: ''
    }
  },
  components: { Button },
  template: `<Button v-bind="$props" v-model="text">${args.slotContent || "Кнопка"}</Button>`,
});

export const Primary = Template.bind();
Primary.args = {
  slotContent: 'Создать курс'
};

export const Warning = Template.bind();
Warning.args = {
  slotContent: 'Создать курс',
  color: 'warning'
};

export const Light = Template.bind();
Light.args = {
  slotContent: 'Создать курс',
  color: 'light'
};

export const Alert = Template.bind();
Alert.args = {
  slotContent: 'Создать курс',
  color: 'alert'
};

export const Disabled = Template.bind();
Disabled.args = {
  slotContentContent: 'Создать курс',
  disabled: true,
}

export const Link = Template.bind();
Link.args = {
  slotContentContent: 'Перейти по ссылке',
  href: '#',
}
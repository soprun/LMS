import Modal from "./index"

export default {
  title: 'UI-kit/Modal',
  component: Modal,
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  data: ()=> {
    return {
      isOpen: true
    }
  },
  methods: {
    alert(text) {
      alert(text)
    }
  },
  components: { Modal },
  template: `
  <div>
  <UiButton @click="()=> isOpen=true">Open modal</UiButton>
  <Modal v-bind="$props" v-model="isOpen" @accept="alert('accept')" @reject="alert('reject')">
    ${args.slotContent || "Текст"}
  </Modal>
  </div>

  `,
});

export const Default = Template.bind();
Default.args = {
  slotContent: 'Исправьте пожалуйста ошибку в поле поле',
};

export const LongText = Template.bind();
LongText.args = {
  slotContent: (()=> {
    let text="Lorem ipsum "
    for(let i=0; i<10; i++) {
      text+=text
    }
    return text
  })(),
};

export const Reverse = Template.bind();
Reverse.args = {
  slotContent: "Кнопки поменялись местами",
  reverse: true,
};

export const ButtonSetting = Template.bind();
ButtonSetting.args = {
  slotContent: "Пример изменения кнопок",
  reverse: true,
  acceptColor: 'primary',
  acceptText:'пропустить',
  rejectColor: 'warning',
  rejectText: "ВНИМАНИЕ",
  reverse: {
    default: false,
  }
};

export const OtherContent = Template.bind();
OtherContent.args = {
  slotContent: `
  <div>
  <UiRadio value='radio' val='radio'>Вывод других компонентов</UiRadio>
  <UiSwitcher :value='true' :disabled="true">Вывод других компонентов</UiSwitcher>
  </div>`,
};

export const CustomClass = Template.bind();
CustomClass.args = {
  slotContent: "Кастомные классы",
  acceptClass: 'storybook-button',
  acceptText:'Дополнительный класс - storybook-button',
  rejectClass: '-light-',
  rejectColor: '',
  rejectText: "Дополнительный класс -light-",
  reverse: {
    default: false,
  }
};

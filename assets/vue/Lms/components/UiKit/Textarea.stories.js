import Textarea from './Textarea'

export default {
  title: 'UI-kit/Textarea',
  component: Textarea,
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  data: ()=> {
    return {
      text: ''
    }
  },
  components: { Textarea },
  template: `<Textarea v-bind="$props" v-model="text"></Textarea>`,
});

export const Text = Template.bind({});
Text.args = {
  title: 'Курс',
  placeholder: 'Какое название?',
};

export const NoTitle = Template.bind({});
NoTitle.args = {
  placeholder: 'Какое название?',
}; 

export const BadInput = Template.bind({});
BadInput.args = {
  title: 'Курс',
  placeholder: 'Какое название?',
  status: 'error',
};

export const Popup = Template.bind({});
Popup.args = {
  title: 'Курс',
  placeholder: 'Какое название?',
  status: 'error',
  popup: {
    msg: 'Не забудьте заполнить это поле :)'
  },
};

export const IconInput = Template.bind({});
IconInput.args = {
  title: 'Поиск',
  placeholder: 'Какое название?',
  icon:'search'
};

export const CustomHeight = Template.bind({});
CustomHeight.args = {
  title: 'Курс',
  placeholder: 'Какое название?',
  height: 200
};
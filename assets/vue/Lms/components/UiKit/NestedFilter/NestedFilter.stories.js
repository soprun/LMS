import NestedFilter from "./index"

export default {
  title: 'UI-kit/NestedFilter',
  component: NestedFilter,
};

let option =
[
  {
    title: "Продажники",
    id: 0,
    list: [
      {
        title: "Тест",
        id: 999,
      }
    ]
  },
  {
    title: "Маркетинг",
    id: 1,
    list: [
      {
        title: "Санкт-Петербург",
        id: 2,
        list: [
          {
            title: "Санкт-Петербург 1 Михайлов",
            id: 3,
          },
          {
            title: "Санкт-Петербург 2 Рубинян",
            id: 4,
          },
        ]
      }
    ]
  },
  {
    title: "Чекбокс",
    id: 998,
  },
  {
    title: "Группа",
    id: 997,
    list: [
      {
        title: "Тест",
        id: 996,
      }
    ]
  },
]

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { NestedFilter },
  data: ()=> {
    return {
      selectValue: [3]
    }
  },
  template: `
  <div style="max-width: 400px">
    <NestedFilter v-model="selectValue" v-bind="$props">
    </NestedFilter>
    <h1>Select value: {{selectValue}}</h1>
  </div>
  `,
});

export const Default = Template.bind();
Default.args = {
  title: "Поиск",
  placeholder: "",
  option,  
};

export const Radio = Template.bind();
Radio.args = {
  title: "Поиск",
  placeholder: "",
  option,  
  radio: true,
};

export const NoParentCheck = Template.bind();
NoParentCheck.args = {
  title: "Поиск",
  placeholder: "",
  option,  
  radio: true,
  parentCheck: false,
};

export const OpenCategory = Template.bind();
OpenCategory.args = {
  title: "Поиск",
  placeholder: "",
  option,  
  radio: true,
  openCategory: true,
};

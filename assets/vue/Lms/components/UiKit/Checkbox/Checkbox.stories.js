import Checkbox from './index'

export default {
  title: 'UI-kit/Checkbox',
  component: Checkbox,
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  data: ()=> {
    return {
      isChecked: false, 
    }
  },
  components: { Checkbox },
  template: `
  <form>
    <Checkbox v-bind="$props" v-model="isChecked">${args.slotContent || "Чекбокс"}</Checkbox>
  </form>
  `,
});

export const Default = Template.bind();
Default.args = {
};

export const Disabled = Template.bind();
Disabled.args = {
  disabled: true,
};


export const Reverse = Template.bind();
Reverse.args = {
  reverse: true,
};

export const Wide = Template.bind();
Wide.args = {
  wide: true,
};

export const Color = Template.bind();
Color.args = {
  color: "gray"
};
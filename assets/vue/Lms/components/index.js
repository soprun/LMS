import Vue from 'vue'

import FixedTooltip from './FixedTooltip'
import Icon from './Icon'
import Avatar from './Avatar'
import OptionsList from './OptionsList'
import Confirm from './Confirm';
import Button from './Button/Button'
import ButtonCancel from './Button/ButtonCancel'
import ControlsArrow from './ControlsArrow/ControlsArrow'
import PageScroll from './PageScroll'
import SearchFilter from './SearchFilter'
import Notification from '@lms/components/Notification'
import TableEmpty from './TableEmpty'
import Phone from './Phone'
import Percents from './Percents'
import Popup from './Popup'
import Diagram from './Diagram'



Vue.component('FixedTooltip', FixedTooltip)
Vue.component('Icon', Icon)
Vue.component('Avatar', Avatar)
Vue.component('OptionsList', OptionsList)
Vue.component('Confirm', Confirm)
Vue.component('Button', Button)
Vue.component('ButtonCancel', ButtonCancel)
Vue.component('ControlsArrow', ControlsArrow)
Vue.component('PageScroll', PageScroll)
Vue.component('SearchFilter',SearchFilter)
Vue.component('Notification',Notification)
Vue.component('TableEmpty', TableEmpty)
Vue.component('Phone', Phone)
Vue.component('Percents', Percents)
Vue.component('Popup', Popup)
Vue.component('Diagram', Diagram)


import Input from './Form/Input'
import TextArea from './Form/TextArea'
import InputNumber from './Form/InputNumber'
import InputDate from './Form/InputDate'
import InputSwitch from './Form/InputSwitch'
import UploadFiles from './Form/UploadFiles'
import Select from './Form/Select'
import SelectRecursion from './Form/SelectRecursion/SelectRecursion'
import SelectGroup from './SelectGroup'
import SelectMultiple from './Form/SelectMultiple'
import SwitchBlock from './Form/SwitchBlock'
import Checkbox from './Form/Checkbox'
import RadioBlock from './Form/RadioBlock'
import Search from './Form/Search'
import InputSearch from './Form/InputSearch'

import DropAsideImage from './Form/DropAsideImage'

Vue.component('Search', Search)
Vue.component('SwitchBlock', SwitchBlock)
Vue.component('Checkbox', Checkbox)
Vue.component('RadioBlock', RadioBlock)
Vue.component('Input', Input)
Vue.component('TextArea', TextArea)
Vue.component('InputNumber', InputNumber)
Vue.component('InputDate', InputDate)
Vue.component('InputSwitch', InputSwitch)
Vue.component('UploadFiles',UploadFiles)
Vue.component('Select', Select)
Vue.component('SelectMultiple', SelectMultiple)
Vue.component('SelectRecursion',SelectRecursion)
Vue.component('SelectGroup',SelectGroup)
Vue.component('DropAsideImage', DropAsideImage)

//ui-kit
import Aside from './UiKit/Aside'
import UiOptionsList from './UiKit/OptionsList'

import UiInput from './UiKit/Input'
import UiButton from './UiKit/Button'
import UiPopup from './UiKit/Popup'
import UiCheckbox from './UiKit/Checkbox'
import UiFile from './UiKit/File'
import UiRadio from './UiKit/Radio'
import UiSwitcher from './UiKit/Switcher'
import UiSelect from './UiKit/Select'
import UiDropdown from './UiKit/Dropdown'
import UiModal from './UiKit/Modal'
import UiDropAsideImage from './UiKit/DropAsideImage'
import UiCustomDropdown from './UiKit/CustomDropdown'
import UiNestedFilter from './UiKit/NestedFilter'
import UiSelectFilter from './UiKit/SelectFilter'
import UiDateInput from './UiKit/DateInput'
import UiNumberInput from './UiKit/NumberInput'
import UiLoaderDots from "./UiKit/LoaderDots/index";
import UiTextarea from "./UiKit/Textarea";
import UiTabs from "./UiKit/Tabs";
import UiLineLoader from "./UiKit/LineLoader";

Vue.component('Aside', Aside)
Vue.component('UiOptionsList', UiOptionsList)

Vue.component('UiInput', UiInput)
Vue.component('UiButton', UiButton)
Vue.component('UiPopup', UiPopup)
Vue.component('UiCheckbox', UiCheckbox)
Vue.component('UiFile', UiFile)
Vue.component('UiRadio', UiRadio)
Vue.component('UiSwitcher', UiSwitcher)
Vue.component('UiSelect', UiSelect)
Vue.component('UiDropAsideImage', UiDropAsideImage)
Vue.component('UiDropdown', UiDropdown)
Vue.component('UiModal', UiModal)
Vue.component('UiCustomDropdown', UiCustomDropdown)
Vue.component('UiNestedFilter', UiNestedFilter)
Vue.component('UiSelectFilter', UiSelectFilter)
Vue.component('UiDateInput', UiDateInput)
Vue.component('UiNumberInput', UiNumberInput)
Vue.component('InputSearch', InputSearch)
Vue.component('UiLoaderDots', UiLoaderDots)
Vue.component('UiTextarea', UiTextarea)
Vue.component('UiTabs', UiTabs)
Vue.component('UiLineLoader', UiLineLoader)

export default function getPermissionName(key) {
  switch (key) {
    case 'courseModuleAdmin':
      return 'Модерирование курсов'
    case 'folderModuleAdmin':
      return 'Модерирование папок'
    case 'lessonModuleAdmin':
      return 'Модерирование уроков'
    case 'taskAnswerCheckAdmin':
      return 'Модерирование проверки заданий'
    case 'userGroupModuleAdmin':
      return 'Управление группами пользователей'
    case 'userModuleAdmin':
      return 'Управление пользователями'
    case 'userTeamModuleAdmin':
      return 'Управление командами'
    case 'analyticsModelAdmin':
      return 'Управление аналитикой'
    default:
      return ''
  }
}
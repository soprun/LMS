export default function getPermissionDescriptions(key) {
  switch (key) {
    case 'courseModuleAdmin':
      return ['Разрешено все', 'По списку курсов', 'Запрещено']
    case 'folderModuleAdmin':
      return ['Разрешено все', 'По списку папок', 'Запрещено']
    case 'lessonModuleAdmin':
      return ['Разрешено все', 'По списку курсов', 'Запрещено']
    case 'taskAnswerCheckAdmin':
      return ['Разрешено все', 'Там, где проверяющий', 'Запрещено']
    case 'userGroupModuleAdmin':
      return ['Разрешено все', 'В этой группе', 'Запрещено']
    case 'userModuleAdmin':
      return ['Разрешено все', 'В этой группе', 'Запрещено']
    default:
      return ['Разрешено все', 'Только свое', 'Запрещено']
  }
}
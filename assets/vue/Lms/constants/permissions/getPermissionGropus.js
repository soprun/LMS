export default function getPermissionGroups() {
  return {
    folderModuleAdmin: {
      create: "0",
      read: "0",
      update: "0",
      delete: "0"
    },
    courseModuleAdmin: {
      create: "0",
      read: "0",
      update: "0",
      delete: "0"
    },
    lessonModuleAdmin: {
      create: "0",
      read: "0",
      update: "0",
      delete: "0"
    },
    taskAnswerCheckAdmin: {
      create: "0",
      read: "0",
      update: "0",
      delete: "0"
    },
    userGroupModuleAdmin: {
      create: "0",
      read: "0",
      update: "0",
      delete: "0"
    },
    userModuleAdmin: {
      create: "0",
      read: "0",
      update: "0",
      delete: "0"
    },
    userTeamModuleAdmin: {
      create: "0",
      read: "0",
      update: "0",
      delete: "0"
    },
    analyticsModelAdmin: {
      create: "0",
      read: "0",
      update: "0",
      delete: "0"
    },
  }
}
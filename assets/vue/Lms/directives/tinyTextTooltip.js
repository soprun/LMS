export default {
  bind: function (el, {value}) {
    let {text, positions} = value
    if(!text)
      return
      
    
    let tooltip = document.createElement('div')
    tooltip.classList.add('tiny-text-tooltip')

    if(positions)
      positions.split(',').forEach(p => {
        tooltip.classList.add(p)
      })

    let p = document.createElement('p')
    p.innerHTML = text

    tooltip.append(p)
    el.prepend(tooltip)

    el.addEventListener('mouseenter', function () {
      el.querySelector('.tiny-text-tooltip').style.opacity = 1
    })
    el.addEventListener('mouseleave', function () {
      el.querySelector('.tiny-text-tooltip').style.opacity = 0
    })
  },
  unbind: function (el) {
    el.removeEventListener('mouseenter', function () {
      el.querySelector('.tiny-text-tooltip').style.opacity = 1
    })
    el.removeEventListener('mouseleave', function () {
      el.querySelector('.tiny-text-tooltip').style.opacity = 0
    })
  }
}
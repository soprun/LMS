import {mapState} from 'vuex'
export default {
  computed: {
    ...mapState('auth',{
      $permissions: s => s.data.user.permissions.taskAnswerCheckAdmin,
      $otherPermissions: s => s.data.user.permissions,
      isAdmin: s => s.data.user.permissions.isAdmin
    }),
    $permissionsRead() {
      if (this.$permissions && this.$permissions.read)
        return this.getPermissionTypeResult(this.$permissions.read)
    },
    $permissionsPointRead() {
      if (this.$permissions && this.$permissions.read)
        return this.$permissions.read
    },
    $permissionsCreate() {
      if (this.$permissions && this.$permissions.create)
        return this.getPermissionTypeResult(this.$permissions.create)
    },
    $permissionsPointCreate() {
      if (this.$permissions && this.$permissions.create)
        return this.$permissions.create
    },
    $permissionsUpdate() {
      if (this.$permissions && this.$permissions.update)
        return this.getPermissionTypeResult(this.$permissions.update)
    },
    $permissionsPointUpdate() {
      if (this.$permissions && this.$permissions.update)
        return this.$permissions.update;
    },
    $permissionsDelete() {
      if (this.$permissions && this.$permissions.delete)
        return this.getPermissionTypeResult(this.$permissions.delete)
    },
    $permissionsPointDelete() {
      if (this.$permissions && this.$permissions.delete)
        return this.$permissions.delete
    },
  },
  methods: {
    checkPermission({key, type}) {
      if (this.$otherPermissions && this.$otherPermissions[key]) {
        if (this.$otherPermissions[key] !== undefined && type)
          return this.getPermissionTypeResult(this.$otherPermissions[key][type])
      }
    },
    getPermissionTypeResult(value) {
      switch (parseInt(value)) {
        case 0:
          return false
        case 1:
          return 1
        case 2:
          return 2
        default:
          return false
      }
    },
  }
}
import { mapGetters } from 'vuex';

export default {
  computed: {
    ...mapGetters('asides', [
      'isAsideIsOpen'
    ])
  },
  methods: {
    asideIsOpen({name}) {
      return this.isAsideIsOpen({name})
    }
  }
}
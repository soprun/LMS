export default {
  methods: {
    validPopup(isErr, text) {
      if(!this.$v.$dirty)
        return
      if(isErr)
        return { msg: text }
    },
  }
}
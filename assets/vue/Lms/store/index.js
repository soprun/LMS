import Vue from "vue"
import Vuex from "vuex"

import mainLayout from "./Layouts/Main"

import ajax from "./ajax"
import lms from "./lms"
import auth from "./auth"
import security from "./security"
import courses from "./courses"
import coursesFolder from "./courses/Folder"
import coursesCourse from "./courses/Course"
import coursesLesson from "./courses/Lesson"
import answersValidating from "./answers/validating"
import answersValidatingFeed from "./answers/validating/feed"
import answersComments from "./answers/comments"
import search from "./components/search"
import notifications from "./components/notifications"
import users from "./users"
import usersGroups from "./users/Groups"
import usersGroupsUsers from "./users/Groups/Users"
import usersTeam from "./users/Team"
import usersTeamMembers from "./users/Team/Users"
import usersPoints from "./users/Points"
import settingStore from './setting'
import teamGroup from './teamGroup'
import answersAnalyticsValidators from './answers/analytics/validators'
import tractionMap from './tractionMap'
import coursePointsShop from './courses/PointsShop'
import coursePointsShopHistory from './courses/PointsShop/History'
import msg from './MSG'
import ratingMsa from './rating/msa'
import ratingMsaUser from './rating/msa/user'
import ratingHundred from './rating/hundred'
import ratingHundredUser from './rating/hundred/user'
import ratingSpeed from './rating/speed'
import ratingSpeedUser from './rating/speed/user'
import ratingInvestments from './rating/investments'
import ratingInvestmentsRegistration from './rating/investments/registration'
import ratingUnifiedGlobal from './rating/unified/global'
import ratingUnifiedUser from './rating/unified/user'
import ratingFastCashUser from './rating/fastCash/user'
import ratingFastCashGlobal from './rating/fastCash/global'
import ratingTeams from './rating/teams'
import ratingSpeedAndHundred from './rating/speedAndHundred'
import club from './club/'
import analSpeed from './analSpeed'
import asides from './asides'
import menu from './components/menu'
import questionnaire from './questionnaire'
import hypothesis from './hypothesis'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    mainLayout,
    ajax,
    lms,
    auth,
    security,
    courses,
    coursesFolder,
    coursesCourse,
    coursesLesson,
    search,
    notifications,
    answersValidating,
    answersValidatingFeed,
    answersComments,
    users,
    usersGroups,
    usersGroupsUsers,
    usersTeam,
    usersTeamMembers,
    usersPoints,
    settingStore,
    teamGroup,
    answersAnalyticsValidators,
    tractionMap,
    coursePointsShop,
    coursePointsShopHistory,
    msg,
    ratingMsa,
    ratingMsaUser,
    ratingHundred,
    ratingHundredUser,
    ratingSpeed,
    ratingSpeedUser,
    ratingUnifiedGlobal,
    ratingUnifiedUser,
    ratingFastCashUser,
    ratingFastCashGlobal,
    ratingInvestments,
    ratingInvestmentsRegistration,
    ratingTeams,
    ratingSpeedAndHundred,
    club,
    asides,
    analSpeed,
    menu,
    questionnaire,
    hypothesis
  }
});

export default {
  setMeta({commit}, {path, value}){
    commit('setMeta', {path, value})
  },
  setData({commit}, {path, value}){
    commit('setData', {path, value})
  },
  setMultipleData({commit}, multipleData){
    commit('setMultipleData', multipleData)
  },
  setMultipleMeta({commit}, multipleMeta){
    commit('setMultipleMeta', multipleMeta)
  },
  /* =======Aside======== */
  setAsideData({commit}, {path, value}){
    commit('setAsideData', {path, value})
  },
  setAsideMeta({commit}, {path, value}){
    commit('setAsideMeta', {path, value})
  },
  setAsideMultipleData({commit}, multipleData){
    commit('setAsideMultipleData', multipleData)
  },
  setAsideMultipleMeta({commit}, multipleMeta){
    commit('setAsideMultipleMeta', multipleMeta)
  },
  /* =======AsideInAside======== */
  setAsideAsideData({commit}, {path, value}){
    commit('setAsideAsideData', {path, value})
  },
  setAsideAsideMeta({commit}, {path, value}){
    commit('setAsideAsideMeta', {path, value})
  },
  setAsideAsideMultipleData({commit}, multipleData){
    commit('setAsideAsideMultipleData', multipleData)
  },
  setAsideAsideMultipleMeta({commit}, multipleMeta){
    commit('setAsideAsideMultipleMeta', multipleMeta)
  },
  /* ======= Menu ======== */
  setMenuData({commit}, {path, value}){
    commit('setMenuData', {path, value})
  },
  setMenuMeta({commit}, {path, value}){
    commit('setMenuMeta', {path, value})
  },
  setMenuMultipleData({commit}, multipleData){
    commit('setMenuMultipleData', multipleData)
  },
  setMenuMultipleMeta({commit}, multipleMeta){
    commit('setMenuMultipleMeta', multipleMeta)
  },
}
import initialActions from "@lms/store/initialActions";
import initialMutations from "@lms/store/initialMutations";
import tryCatch from "@lms/functions/tryCatch";

export default {
  namespaced: true,
  state: {
    data: {
      headerTitle: '',
      headerPrevLink: '',
    },
    menu: {
      data: {
        lessonArray: [],
        team: {},
        points: 0,
        user: {},
        course: {},
        numbers: {},
        activeDayId: undefined,
        courseId: undefined,
        days: {},
      },
      meta: {
        isOpen: true,
        isFranch: false,
        isLoadFranch: false
      }
    },
    meta: {
      showFranchTypePopup: false
    }
  },
  actions: {
    ...initialActions,
    async getMenu({commit, dispatch, state}) {
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/menu/course/${state.menu.data.course.id}/get`,
        }, {root: true})

        if (res.data.status === 'success') {
          Object.entries(res.data.data).forEach((entry) => {
            commit('setMenuData', {path: entry[0], value: entry[1]})
          })
        }
      })
    },
    async getFranch({commit, dispatch, state}){
      await tryCatch(async () => {
        commit('setMenuMeta', { path:'isFranch', value: true })
        commit('setMenuMeta', { path: 'isLoadFranch', value: true})
        let res = await dispatch('ajax/axiosSend',
          {
            type:'get',
            query:'/franch/menu/get'
          },
          { root: true })

          if(res && res.data.status === 'success')
            Object.entries(res.data.data).forEach(el => {
              commit('setMenuData', { path: el[0], value: el[1]})
            })
        commit('setMenuMeta', { path: 'isLoadFranch', value: false})
      })
    },
    toggleMenu({commit, state}, value){
      commit('setMenuMeta', {path: 'isOpen', value: value === undefined ? !state.menu.meta.isOpen : value})
    },
    async setFranchAttendance({commit, rootCommit, dispatch}, {typeId}) {
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend',
          {
            type: 'post',
            query: '/user/group/attendance/type/set',
            sendData: {typeId}
          },
          { root: true })
        if(res.data.status === 'success') {
          commit('auth/setFranchAttendance', {typeId}, {root: true})
          commit('setMeta', {path: 'showFranchTypePopup', value: false})
        }
      })
    }
  },
  mutations: {
    ...initialMutations,

  }
}
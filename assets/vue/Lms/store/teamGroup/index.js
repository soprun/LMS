import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import tryCatch from "@lms/functions/tryCatch"
import _ from 'lodash'

export default {
  namespaced: true,
  state:{
    data:{
      groups: [],
      teams: [],
      settings: {
        captains: [],
        cities: [],
        seniorCaptains: [],
        canEdit: false,
        streams: [],
        faculties: [],
        groupFolders: {},
      },
      unallocated: [],
      peopleNearby: [],
      filterList: {
        captain: undefined,
        city: undefined,
        search: undefined,
        faculty: undefined,
        courseStreamId: undefined,
      },
      allGroups: [],
    },
    meta:{
      isGroupLoading: false,
      isUnallocatedLoading: false,
      isPeopleNearbyLoading: false,
      isTeamLoading: false,
      isSettingLoading: false,
      isLoadingAllGroups: false,
      isLoadingMoreGroups: false,
      isLoadingMoreUnallocated: false,

      loadedGroup: false,
      loadedSetting: false,
      loadedUnallocated: false,
      loadedPeopleNearby: false,

      groupsPage: 1,
      unallocatedPage: 1,
      hasMoreGroups: true,
      hasMoreUnallocated: true,
      pageSize: 30
    }
  },
  actions:{
    ...initialActions,

    async getTeamGroupData({state, commit, dispatch}){
      await tryCatch( async () => {

        if (state.meta.groupsPage > 1) {
          commit('setMeta', { path: 'isLoadingMoreGroups', value: true })
        } else {
          commit('setMeta', { path: 'isGroupLoading', value: true })
        }

        const res = await dispatch('ajax/axiosSend',{
          type: 'get',
          query: `/api/v2/teams/list`,
          params: {
            ...state.data.filterList,
            page: state.meta.groupsPage,
            pageSize: state.meta.pageSize
          },
          cancel: {name: 'getTeamGroupData'},
        },{ root:true });

        if (res && res.data.status === 'success') {
          commit('setData', { path: 'groups', value: [...state.data.groups, ...res.data.data] })
          commit('setMeta', { path: 'hasMoreGroups', value: res.data.data.length === state.meta.pageSize })
          commit('setMeta', { path: 'loadedGroup', value: true })
          if (state.meta.hasMoreGroups) {
            commit('setMeta', { path: 'groupsPage', value: state.meta.groupsPage + 1 })
          }
        }
        commit('setMeta', { path: 'isLoadingMoreGroups', value: false })
        commit('setMeta', { path: 'isGroupLoading', value: false })
      })
    },

    async getTeamsData({commit, dispatch}, teamId){
      let state = {}
      await tryCatch( async () => {
        commit('setMeta', {path: 'isTeamLoading', value: true})

        let res = await dispatch('ajax/axiosSend',{
          type: 'get',
          query: `/api/teams/${teamId}`,
        },{ root:true });

        if (res && res.data.status === 'success'){
          state.status = 'success'
          res.data.data.id = teamId
          commit('setTeamData', { path: 'teams', value: res.data.data })
        }

        commit('setMeta', {path: 'isTeamLoading', value: false})
      })
      return state
    },

    async getTeamsSettings({commit, dispatch}, streamId){
      await tryCatch( async () => {
        commit('setMeta', {path: 'isSettingLoading', value: true})

        let res = await dispatch('ajax/axiosSend',{
          type: 'get',
          query: `/api/v2/teams/settings`,
          params: {
            streamId
          }
        },{ root:true });

        if (res && res.data.status === 'success'){
          commit('setData', { path: 'settings', value: res.data.data })
          commit('setMeta', {path: 'loadedSetting', value: true})
        }
        commit('setMeta', {path: 'isSettingLoading', value: false})
      })
    },

    // Можно отправлять по одному ключу.
    // Например, кликнули на звездочку чтобы назначить лидера,
    // отправили ключ leader.
    async editTeam({commit, dispatch}, team){
      let state = {}
      let sendData = _.cloneDeep(team)
      delete sendData.id

      await tryCatch( async () => {
        let res = await dispatch('ajax/axiosSend',{
          type: 'post',
          query: `/api/teams/${team.id}/edit`,
          sendData,
        },{ root:true });

        if (res && res.data.status === 'success') {
          state.status = 'success'
          // Обновляем команду в группах
          commit('teamUpdate', team)

          // Обновляем команду из массива команд
          res.data.data.id = team.id
          commit('setTeamData', { path: 'teams', value: res.data.data })
        }
      })
      return state
    },

    async createTeam({dispatch}, team) {
      let state = {}
      await tryCatch( async() => {
        let res = await dispatch('ajax/axiosSend',{
          type: 'post',
          query: `/api/teams/create`,
          sendData: team,
        },{ root:true });

        if (res && res.data.status === 'success') {
          state.status = res.data.status
        }
      })
      return state
    },

    async editVerification({commit, dispatch}, userId){
      let state
      try {
        const res = await dispatch('ajax/axiosSend',{
          type: 'post',
          query: `/api/teams/user/${userId}/verification`,
        },{ root:true });
        if (res) {
          state = res.data.verified
          commit('setUserVerificate', { userId: userId, verified: res.data.verified })
          dispatch('ajax/addMessage', { message: res.data.verified ? 'Верификация успешно подтверждена' : 'Верификация отклонена', type: 'success' }, { root: true })
        } else {
          dispatch('ajax/addMessage', { message: 'При изменении верификации возникла ошибка', type: 'error' }, { root: true })
        }
      } catch (e) {
        console.error(e)
      } 
      return state
    },

    // Перемещение пользователя
    async userTeamChange({commit, dispatch}, {user, team, oldTeam}) {
      let data = {}
      await tryCatch( async() => {
        let res = await dispatch('ajax/axiosSend',{
          type: 'post',
          query: `/api/teams/change`,
          sendData: {
            user,
            team,
          },
        },{ root:true });

        if (res && res.data.status === 'success') {
          res.data.data.id = team 
          data = res.data

          commit('removeUser', {teamId: oldTeam, userId: user})
          commit('setTeamData', { path: 'teams', value: res.data.data })
          commit('updateUserCounter', {teamId: oldTeam})
          commit('updateUserCounter', {teamId: team})
        }
      })
      return data
    },

    // Нераспределённые
    async getUnallocated({state, commit, dispatch}){
      await tryCatch( async () => {

        if (state.meta.unallocatedPage > 1) {
          commit('setMeta', { path: 'isLoadingMoreUnallocated', value: true })
        } else {
          commit('setMeta', { path: 'isUnallocatedLoading', value: true })
        }

        let res = await dispatch('ajax/axiosSend',{
          type: 'get',
          query: `/api/teams/unallocated`,
          params: {
            ...state.data.filterList,
            page: state.meta.unallocatedPage,
            pageSize: state.meta.pageSize
          },
          cancel: {name: 'getUnallocated'},
        },{ root:true });

        if (res && res.data.status === 'success'){
          commit('setData', { path: 'unallocated', value: [...state.data.unallocated, ...res.data.data] })
          commit('setMeta', { path: 'hasMoreUnallocated', value: res.data.data.length === state.meta.pageSize })
          commit('setMeta', { path: 'loadedUnallocated', value: true })
          if (state.meta.hasMoreUnallocated) {
            commit('setMeta', { path: 'unallocatedPage', value: state.meta.unallocatedPage + 1 })
          }
        }

        commit('setMeta', { path: 'isLoadingMoreUnallocated', value: false })
        commit('setMeta', { path: 'isUnallocatedLoading', value: false })
      })
    },

    // Участники по городу
    async getPeopleNearby({commit, dispatch}){
      await tryCatch( async () => {
        commit('setMeta', {path: 'isPeopleNearbyLoading', value: true})

        let res = await dispatch('ajax/axiosSend',{
          type: 'get',
          query: `/api/teams/people-nearby`,
        },{ root:true });

        if (res && res.data.status === 'success'){
          commit('setData', { path: 'peopleNearby', value: res.data.data })
          commit('setMeta', {path: 'loadedPeopleNearby', value: true})
        }

        commit('setMeta', {path: 'isPeopleNearbyLoading', value: false})
      })
    },

    // Фильтры
    async setFilter({commit, state, dispatch},{path, value}) {
      if (path === 'courseStreamId' && value !== state.data.filterList.courseStreamId) {
        commit('setFilter', { path: 'faculty', value: undefined });
        dispatch('getTeamsSettings', value);
      }

      commit('setFilter', {path, value})
    },

    clearGroup({commit}) {
      commit('setMeta', {path: 'loadedUnallocated', value: false})
      commit('setMeta', {path: 'loadedGroup', value: false})
      commit('clearGroup')
    },

    // Команды у конкретной группы
    async getTeamByGroup({commit, dispatch}, {id}) {
      let data = {}
      await tryCatch( async () => {
        let res = await dispatch('ajax/axiosSend',{
          type: 'get',
          query: `/api/teams/find-by-group/${id}`,
        },{ root:true });

        if (res && res.data.status === 'success'){
          data = res.data
        }
      })
      return data
    },

    // Команды у конкретной группы
    async getTeamByFolder({commit, dispatch}, {id}) {
      let data = {}
      await tryCatch( async () => {
        let res = await dispatch('ajax/axiosSend',{
          type: 'get',
          query: `/api/teams/find-by-folder/${id}`,
        },{ root:true });

        if (res && res.data.status === 'success'){
          data = res.data
        }
      })



      return data
    },

    // Все группы
    async getAllGroups({commit, dispatch}) {
      commit('setMeta', {path: 'isLoadingAllGroups', value: true})
      await tryCatch( async () => {
        let res = await dispatch('ajax/axiosSend',{
          type: 'get',
          query: `/api/teams/available-folders`,
        },{ root:true });

        if (res && res.data.status === 'success') {
          commit('setData', {path: 'allGroups', value: res.data.data})
        }
      })
      commit('setMeta', {path: 'isLoadingAllGroups', value: false})
    },

    async deleteTeam({commit, dispatch}, id) {
      await tryCatch( async () => {
        let res = await dispatch('ajax/axiosSend',{
          type: 'POST',
          query: `/api/teams/${id}/delete`,
        },{ root:true });

        if (res && res.data.status === 'success')
          commit('deleteTeam', {id})
      })
    },
    async removeUserFromTeam({commit, dispatch}, {user, team}) {
      let data = {}
      await tryCatch( async() => {
        let res = await dispatch('ajax/axiosSend',{
          type: 'post',
          query: `/api/teams/remove`,
          sendData: {
            user,
            team,
          },
        },{ root:true });

        if (res && res.data.status === 'success') {
          res.data.data.id = team
          data = res.data

          commit('addUserToUnallocated', {teamId: team, userId: user})
          commit('updateUserCounter', {teamId: team})
          // TODO
          // Бэк будет отдавать пользователя
          // Нужно будет записать его в unallocated
          // Пока просто запрашиваю unallocated заново
          await dispatch('getUnallocated')
        }
      })
      return data
    },
  },
  mutations:{
    ...initialMutations,

    setUserVerificate(state, {userId, verified}) {
      state.data.unallocated.find(user => user.id === userId).verified = verified
    },
    setTeamData(state, {value}) {
      // Если команда существует, обновляем её
      let idx = state.data['teams'].findIndex(team => team.id === value.id)
      if(idx === -1)
        state.data['teams'].push(value);
      else
        state.data['teams'].splice(idx, 1, value)
    },
    teamUpdate(state, team) {

      // ОБНОВЛЯЕМ GROUPS
      // Ищем группу
      state.data.groups.forEach(group => {
        // Ищем команду в группе
        group.teams.forEach(groupTeam => {
          // Проверяем та ли команда
          if(groupTeam.id === team.id) {
            // Если та, то перебираем возможные свойства
            Object.keys(groupTeam).forEach(key => {
              if(key in team) {
                groupTeam[key] = team[key]
              }
            })
          }
        })
      })

    },
    removeUser(state, {teamId, userId}) {
      // Если нет команды, то ищем в нераспределённых
      if(teamId === undefined) {
        let idx = state.data.unallocated.findIndex(({id}) => userId === id)
        if(idx === -1) return
        state.data.unallocated.splice(idx, 1)
      } else {
      // Если есть команда, ищем в ней
        let teamIdx = state.data.teams.findIndex(({id}) => teamId === id)
        if(teamIdx === -1) return

        let userIxd = state.data.teams[teamIdx].users.findIndex(({id}) => userId === id)
        if(userIxd === -1) return


        state.data.teams[teamIdx].users.splice(userIxd, 1)
      }
    },
    setFilter(state, {path, value}) {
      // Что бы лишний раз не триггерить watch на фильтрах
      if(state.data.filterList[path] === value) return
      state.data.filterList[path] = value
    },
    clearGroup(state) {
      state.data.groups = []
      state.data.teams = []
      state.data.unallocated = []
      state.meta.groupsPage = 1
      state.meta.unallocatedPage = 1
      state.meta.hasMoreGroups = true
      state.meta.hasMoreUnallocated = true
    },
    deleteTeam(state, {id}) {
      // Ищем в группе
      state.data.groups.find(group => {
        let idx = group.teams.findIndex(gropuTeam => gropuTeam.id === id)
        if(idx !== -1) {
          group.teams.splice(idx, 1)
          return true
        }
      })

      // Ищем в командах
      let teamIdx = state.data.teams.findIndex(team => team.id === id)
      if(teamIdx !== -1)
        state.data['teams'].splice(teamIdx, 1)
    },
    updateUserCounter(state, {teamId}) {
      let team = state.data.teams.find(({id})=> id === teamId)
      if(!team) return

      let userCount = team.users.length

      state.data.groups.find(group => {
        // Ищем команду в группе
        return group.teams.find(gropuTeam => {
          // Проверяем та ли команда
          if(gropuTeam.id === team.id) {
            gropuTeam.count = userCount
            return true
          }
        })
      })
    },
    addUserToUnallocated(state, {teamId, userId}) {
      let teamIdx = state.data.teams.findIndex(({id}) => teamId === id)
      if(teamIdx === -1) return

      let userIxd = state.data.teams[teamIdx].users.findIndex(({id}) => userId === id)
      if(userIxd === -1) return

      state.data.unallocated.push(state.data.teams[teamIdx].users[userIxd])
      state.data.teams[teamIdx].users.splice(userIxd, 1)
    }

    // Возможно позже
    // addTeamInGroup(state, {team, groupId}) {
    //   let idx = state.data.groups.findIndex(({id}) => groupId === id)
    //   if (idx !== -1)
    //     state.data.groups[idx].teams.push(team)
    // }
  }
}

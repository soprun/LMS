import tryCatch from "@lms/functions/tryCatch";

export default {
  namespaced: true,
  state: {},
  actions: {
    async searchByUrl({commit, dispatch}, {url, cancelTokenName, name}){
      let result;
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {type: 'get', query: url+'?name='+name, cancel: {name: cancelTokenName}}, {root: true})
        if(!res)
          return

        if (res.data.status === 'success') {
          if (res.data.data) {
            result = res.data.data
          }
        }
      })
      return result
    },
  },
  mutations: {
  },
}
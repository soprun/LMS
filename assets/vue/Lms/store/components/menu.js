import initialActions from "toolbox/store/initialActions";
import initialMutations from "toolbox/store/initialMutations";

export default {
  namespaced: true,
  state: {
    data: {
      modal: {
        isActive: false,
        menuFocus: false,
        focus: false,
        position: '',
        name: '',
        list: [],
        path: '',
      }
    },
    meta: {
      isOpen: false,
      isMinimize: false,
    },
  },
  actions: {
    ...initialActions,
    setModal({commit}, data) {
      commit('setModal', data)
    },
    setModalFocus({commit}) {
      commit('setModalData', {path: 'focus', value: true})
    },
    unsetModalFocus({commit}) {
      commit('setModalData', {path: 'focus', value: false})
    },
    setMenuFocus({commit}) {
      commit('setModalData', {path: 'menuFocus', value: true})
    },
    unsetMenuFocus({commit}) {
      commit('setModalData', {path: 'menuFocus', value: false})
    }
  },
  mutations: {
    ...initialMutations,
    setModal(state, {position, name, list, path, tag, isActive}) {
      state.data.modal.name = name;
      state.data.modal.position = position;
      state.data.modal.list = list || [];
      state.data.modal.path = path;
      state.data.modal.tag = tag;
      state.data.modal.isActive = isActive;
    },
    setModalData(state, {path, value}) {
      state.data.modal[path] = _.cloneDeep(value)
    }
  },
}
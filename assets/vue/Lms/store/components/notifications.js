import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import createQueryString from "@lms/functions/createQueryString"
import tryCatch from "@lms/functions/tryCatch";

export default {
  namespaced: true,
  state: {
    data: {
      count: 0,
      page: 1,
      notifications: []
    },
    meta: {
      isLoading: false,
      addingNotifications: false,
      lastPage: false,
    }
  },
  actions: {
    ...initialActions,
    async getNotifications({commit, dispatch}) {
      await tryCatch(async () => {
        commit('setMeta', {path: 'isLoading', value: true})
        let res = await dispatch('ajax/axiosSend', {
            type: 'get',
            query: '/notification/list',
            params: {
              page: 1
            },
            cancel: {name: 'cancelNotifications'}
          },
          {root: true})

        if (res && res.data.status === 'success') {
          if (res.data.data)
            commit('setData', {path: 'notifications', value: res.data.data.notifications})
        }
        commit('setMeta', {path: 'isLoading', value: false})
      })
    },
    async getNotificationsUnread({commit, dispatch, state}){
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'get', 
          query: '/notification/unread-count', 
          cancel: {name: 'cancelNotificationsUnread'}}, 
          {root: true})

        if (res && res.data.status === 'success' && state.data.count !== res.data.data.count) {
          commit('setData', { path: 'count', value: res.data.data.count})
        }
      })
    },
    async addMoreNotifications({commit, dispatch, state}) {
      await commit('setData', {path: 'page', value: state.data.page + 1})
      commit('setMeta', {path: 'addingNotifications', value: true})

      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
            type: 'get',
            query: '/notification/list',
            params: {
              page: state.data.page
            },
            cancel: {name: 'addingMoreNotifications'}
          }, {root: true})

        if (res && res.data.status === 'success') {
          if (res.data.data.notifications.length)
            commit('setData', {
              path: 'notifications',
              value: [...state.data.notifications, ...res.data.data.notifications]
            })
          else
            commit('setMeta', {path: 'lastPage', value: true})
        }
      })
      commit('setMeta', {path: 'addingNotifications', value: false})
    },
    clearData({commit}){
      commit('setData', { path: 'page', value: 1})
      commit('setData', { path: 'notifications', value: []})
      commit('setMeta', { path: 'lastPage', value: false})
    }
  },
  mutations: {
    ...initialMutations,
  },
}
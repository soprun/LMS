import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import createQueryString from "@lms/functions/createQueryString"
import tryCatch from '@lms/functions/tryCatch';
import _ from 'lodash';
import moment from 'moment';


export default {
  namespaced: true,
  state: {
    aside: {
      data: {
        hypothesis: null,
        // TODO получать из erp
        categoryToSelect: [
          { "text": "Анализ ниши", "value": "Анализ ниши" },
          { "text": "Анализ конкурентов", "value": "Анализ конкурентов" },
          { "text": "MVP", "value": "MVP" },
          { "text": "Продажи", "value": "Продажи" },
          { "text": "Трафик", "value": "Трафик" },
          { "text": "Инвестиции", "value": "Инвестиции" },
          { "text": "Команда", "value": "Команда" },
          { "text": "Продукт", "value": "Продукт" },
          { "text": "Другое", "value": "Другое" }
        ]
      },
      meta: {
        canSave: false,
        isOpen: false,
        forceClose: false
      }
    }
  },
  actions: {
    ...initialActions,
    async getCategoryToSelect({ commit, dispatch, state }) {
      await tryCatch( async () => {
        // TODO Сделать запрос на ERP когда будет эндпоинт
        // let res = await dispatch('ajax/axiosSend', {
        //   type: 'get',
        //   query: `${process.env.ERP_SERVER_URL}/hypothesis/get`,
        // }, {root: true});

        // if(!res && res.data.status !== 'success') {
        //   throw new Error;
        // }

        // commit('setAsideData', {path: 'categoryToSelect', value: res.data.data.categoryToSelect})
      })
    },
    async saveHypothesis({ commit, dispatch, state }) {
      await tryCatch(async () => {
        let data = {
          text: state.aside.data.hypothesis.name,
          ddl: state.aside.data.hypothesis.date,
          status: 'В ожидании',
          comment: '',
          category: state.aside.data.hypothesis.category,
          actions: state.aside.data.hypothesis.actions.filter(item => {
            return item.action !== ''
          }).map(item => {
            return {
              action: item.action,
              isComplete: item.isComplete
            }
          })
        }

        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `${process.env.ERP_SERVER_URL}/api/external/hypothesis`,
          sendData: data
        }, {root: true});

        if(!res && res.data.status !== 'success') {
          throw new Error;
        }

        commit('setAsideData', {path: 'hypothesis', value: null})
      })
    },
    clearData({ commit, dispatch, state }) {
      commit('setAsideMeta', {path: 'canSave', value: false})
      commit('setAsideMeta', {path: 'isOpen', value: false})
      commit('setAsideMeta', {path: 'forceClose', value: false})
      commit('setAsideData', {path: 'hypothesis', value: null})
      // TODO: после эндпоинта на erp
      // commit('setAsideData', {path: 'categoryToSelect', value: null})
    }
  },
  mutations: {...initialMutations}
}

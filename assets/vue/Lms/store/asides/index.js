const initialAside = {
  meta: {
    hasChanges: false
  }
}

export default {
  namespaced: true,
  state: {
    data: {
      asides: []
    },
  },
  actions: {
    addAside({state, commit}, {data, meta} = {}) {
      if(!data || (data && !data.name) || typeof data !== 'object') {
        console.warn('Неверные данные для создания эсайда', {data, meta})
        return
      }

      if(state.data.asides.length && state.data.asides.some(el => el.data.name === data.name)){
        console.warn('Такой эсайд уже активен', {data, meta})
        return
      }

      commit('addAside', {data, meta})
    },
    updateAsideData({state, commit}, {asideIndex, data, asideName}) {
      if (asideIndex === undefined && !asideName) {
        console.warn('Не указана индекс или имя эсайда', {asideIndex})
        return
      }

      if (!data) {
        console.warn('Не указана data', {data})
        return
      }

      if (!state.data.asides[asideIndex] && !asideName) {
        console.warn('Эсайд не найден', {asideIndex, data})
        return
      }

      commit('updateAsideData', {asideIndex, data, asideName})
    },
    updateAsideDataPath({state, commit}, {asideIndex, path, value}) {
      if(asideIndex === undefined) {
        console.warn('Не указана индекс эсайда', {asideIndex})
        return
      }

      if(!path) {
        console.warn('Не указана path', {path})
        return
      }

      if (!state.data.asides[asideIndex]) {
        console.warn('Эсайд не найден', {asideIndex, path, value})
        return
      }

      commit('updateAsideDataPath', {asideIndex, path, value})
    },
    updateAsideMeta({state, commit}, {asideIndex, meta}) {
      if(asideIndex === undefined) {
        console.warn('Не указана индекс эсайда', {asideIndex})
        return
      }

      if(!meta) {
        console.warn('Не указана meta', {meta})
        return
      }

      if(!state.data.asides[asideIndex]) {
        console.warn('Эсайд не найден', {asideIndex, meta})
        return
      }

      commit('updateAsideMeta', {asideIndex, meta})
    },
    updateAsideMetaPath({state, commit}, {asideIndex, path, value}) {
      if(asideIndex === undefined) {
        console.warn('Не указана индекс эсайда', {asideIndex})
        return
      }

      if(!path) {
        console.warn('Не указана path', {path})
        return
      }

      if (!state.data.asides[asideIndex]) {
        console.warn('Эсайд не найден', {asideIndex, path, value})
        return
      }

      commit('updateAsideMetaPath', {asideIndex, path, value})
    },
    closeAside({state, commit}, {name}) {
      if(!name) {
        console.warn('Не указано имя эсайда', {name})
        return
      }

      if(!state.data.asides.length){
        console.warn('Нет активных эсайдов', {name})
        return
      }

      if(!state.data.asides.some(el => el.data.name === name)){
        console.warn('Эсайд с таким именем не найден', {name})
        return
      }

      commit('closeAside', {name})
    },
    removeAside({state, commit}, {name}) {
      if(!name) {
        console.warn('Не указано имя эсайда', {name})
        return
      }

      if(!state.data.asides.length){
        console.warn('Нет активных эсайдов', {name})
        return
      }

      if(!state.data.asides.some(el => el.data.name === name)){
        console.warn('Эсайд с таким именем не найден', {name})
        return
      }

      commit('removeAside', {name})
    }
  },
  mutations: {
    addAside(state, {data, meta = {}}) {
      let newAside = {
        data: {...data},
        meta: {
          ...initialAside.meta,
          ...meta
        }
      }

      state.data.asides.push(newAside)
      state.data.asides = [...state.data.asides]
    },

    updateAsideData(state, {asideIndex, data, asideName}) {
      const index = asideIndex !== undefined ? asideIndex : state.data.asides.findIndex(el => el.data.name === asideName)
      if(!state.data.asides[index]) {
        console.log('Эсайд не найден', {index, data})
        return
      }

      state.data.asides[index].data = {...data}

      state.data.asides = [...state.data.asides]
    },

    updateAsideDataPath(state, {asideIndex, path, value}) {
      if(!state.data.asides[asideIndex]) {
        console.log('Эсайд не найден', {asideIndex, path, value})
        return
      }

      state.data.asides[asideIndex].data[path] = typeof value === 'object' ? _.cloneDeep(value) : value

      state.data.asides = [...state.data.asides]
    },

    updateAsideMeta(state, {asideIndex, meta}) {
      if(!state.data.asides[asideIndex]) {
        console.log('Эсайд не найден', {asideIndex, meta})
        return
      }

      state.data.asides[asideIndex].meta = {...meta}

      state.data.asides = [...state.data.asides]
    },

    updateAsideMetaPath(state, {asideIndex, path, value}) {
      if(!state.data.asides[asideIndex]) {
        console.log('Эсайд не найден', {asideIndex, path, value})
        return
      }

      state.data.asides[asideIndex].meta[path] = typeof value === 'object' ? _.cloneDeep(value) : value

      state.data.asides = [...state.data.asides]
    },

    closeAside(state, {name}) {
      let asideIdx = state.data.asides.findIndex(el => el.data.name === name)
      if(asideIdx !== -1) {
        state.data.asides[asideIdx].meta.close = true

        state.data.asides = [...state.data.asides]
      }
    },

    removeAside(state, {name}) {
      let asideIdx = state.data.asides.findIndex(el => el.data.name === name)
      if(asideIdx !== -1) {
        state.data.asides.splice(asideIdx, 1)

        state.data.asides = [...state.data.asides]
      }
    }
  },
  getters: {
    isAsideIsOpen: state => ({name}) => {
      if(!name) {
        console.warn('Имя эсайда не указано')
        return
      }

      if(!state.data.asides.length)
        return

      if(state.data.asides.some(el => el.data.name === name))
        return {result: 'yes'}
    },
    getAsideObject: state => ({name}) => {
      if(!name) {
        console.warn('Имя эсайда не указано', {name})
        return
      }

      if(!state.data.asides.length)
        return

      let asideIndex = state.data.asides.findIndex(el => el.data.name === name)
      if(asideIndex === -1) {
        console.warn('Эсайд не найден', {name})
        return
      }

      return {asideIndex, aside: state.data.asides[asideIndex]}
    },
    getAsideMetaPath: state => ({name, path}) => {
      if(!name) {
        console.warn('Имя эсайда не указано', {name})
        return
      }

      if(!state.data.asides.length)
        return

      let asideIndex = state.data.asides.findIndex(el => el.data.name === name)
      if(asideIndex === -1) {
        console.warn('Эсайд не найден', {name})
        return
      }

      if(!state.data.asides[asideIndex].meta) {
        console.warn('В эсайде нет меты', {name})
        return
      }

      return state.data.asides[asideIndex].meta[path]
    },
  }
}

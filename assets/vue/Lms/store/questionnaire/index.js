import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import tryCatch from '@lms/functions/tryCatch'
import router from '@lms/router/index.js'
import moment from "moment";
import _ from "lodash";

export default
{
  namespaced: true,
  state: {
    data: {
      activeQuestionId: 0,
      questionList: [],
      history: [],
      telegramName: '',
      hundredWasForced: false
    },
    meta: {
      isLoading: false,
      isSendQuestion: false,
    }
  },
  getters: {
    activeQuestion(state){
      let question = state.data.questionList.find(({id}) => (
        id === state.data.activeQuestionId
      ))
      return question
    }
  },
  actions: {
    ...initialActions,
    setStep({commit}, step) {
      commit('setData', {path: 'step', value: step})
    },
    nextStep({dispatch, state}) {
      dispatch('setStep', state.data.step + 1)
    },
    lastStep({dispatch, state}) {
      dispatch('setStep', state.data.step - 1)
    },

    async getQuestionnaireList({commit, dispatch}) {
      await tryCatch(async () => {
        commit('setMeta', {path: 'isLoading', value: true})
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/api/questionnaire/list`
        }, {root: true})

        if (res && res.data.status === 'success') {
          const revenueQuestion = res.data.data.find(q => q.slug === 'month_revenue')
          const profitQuestion = res.data.data.find(q => q.slug === 'month_profit')

          res.data.data = res.data.data.filter(q => {
            return q.slug !== 'month_revenue' && q.slug !== 'month_profit'
          })
          res.data.data.forEach(q => {
            q.variants = Object.values(q.variants)
          })
          res.data.data = [...res.data.data, {
            id: revenueQuestion.id,
            nextQuestion: profitQuestion.nextQuestion,
            helpPage: profitQuestion.id,
            slug: 'month_revenue_profit',
            title: 'Какая у тебя выручка в месяц?',
            additionalTitle: 'Какая у тебя чистая прибыль в месяц?',
            answer: null,
            userProperty: null,
            variants: []
          }, {
            id: profitQuestion.id,
            nextQuestion: null,
            slug: 'month_revenue_profit_help',
            title: 'Как посчитать выручку и прибыль',
            answer: null,
            userProperty: null,
            variants: []
          }]

          commit('setData', {path: 'questionList', value: res.data.data})

          commit('setMeta', {path: 'isLoading', value: false})
        }
      })
    },
    async hundredReplace({commit, dispatch}, redirectSlug) {
      await tryCatch(async () => {
        commit('setMeta', {path: 'isLoading', value: true})
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/api/questionnaire/list`
        }, {root: true})

        if (res && res.data.status === 'success') {
          const revenueQuestion = res.data.data.find(q => q.slug === 'month_revenue')
          const profitQuestion = res.data.data.find(q => q.slug === 'month_profit')
          res.data.data = res.data.data.filter(q => {
            return q.slug !== 'month_revenue' && q.slug !== 'month_profit'
          })
          res.data.data.forEach(q => {
            q.variants = Object.values(q.variants)
          })
          res.data.data = [...res.data.data, {
            id: revenueQuestion.id,
            nextQuestion: profitQuestion.nextQuestion,
            helpPage: profitQuestion.id,
            slug: 'month_revenue_profit',
            title: 'Какая у тебя выручка в месяц?',
            additionalTitle: 'Какая у тебя чистая прибыль в месяц?',
            answer: null,
            userProperty: null,
            variants: []
          }, {
            id: profitQuestion.id,
            nextQuestion: null,
            slug: 'month_revenue_profit_help',
            title: 'Как посчитать выручку и прибыль',
            answer: null,
            userProperty: null,
            variants: []
          }]
          const nextQuestionId = res.data.data.find(q => q.slug === redirectSlug).id

          commit('setData', {path: 'questionList', value: res.data.data})
          dispatch('setQuestion', nextQuestionId)

          commit('setMeta', {path: 'isLoading', value: false})
        }
      })
    },
    async sendQuestion({state, commit, dispatch}, {id, value, isVariant }) {
      let status = {}
      commit('setMeta', {path: 'isSendQuestion', value: true})
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/api/questionnaire/answer/${id}`,
          sendData: {
            answer: {
              variant: isVariant ? value : undefined,
              text: !isVariant ? value.toString() : undefined
            }
          }
        }, {root: true})

        let forceHundred = res.data.data.forceHundred;
        if (res && res.data.status === 'success') {
          status.status = 'success'
          status.forceHundred = forceHundred
          commit('setData', {path: 'hundredWasForced', value: state.data.hundredWasForced ? true : forceHundred })
        }
      })
      commit('setMeta', {path: 'isSendQuestion', value: false})
      return status
    },
    async sendPointA({state, commit, dispatch}, {revenue, profit}) {
      let status = {}
      commit('setMeta', {path: 'isSendQuestion', value: true})
      await tryCatch(async () => {
        const revenueId = state.data.questionList.find(q => q.slug === 'month_revenue_profit').id
        const profitId = state.data.questionList.find(q => q.slug === 'month_revenue_profit_help').id

        const resRevenue = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/api/questionnaire/answer/${revenueId}`,
          sendData: {
            answer: {
              text: revenue
            }
          }
        }, {root: true})

        const resProfit = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/api/questionnaire/answer/${profitId}`,
          sendData: {
            answer: {
              text: profit
            }
          }
        }, {root: true})

        let forceHundred = resProfit.data.data.forceHundred;
        if (resRevenue && resProfit && resRevenue.data.status === 'success' && resProfit.data.status === 'success') {
          status.status = 'success'
          status.forceHundred = forceHundred
          commit('setData', {path: 'hundredWasForced', value: state.data.hundredWasForced ? true : forceHundred })
        }
      })
      commit('setMeta', {path: 'isSendQuestion', value: false})
      return status
    },
    async setQuestion({commit}, id) {
      commit('updateHistory')
      commit('setData', {path: 'activeQuestionId', value: id})
    },
    comeBackHistory({commit}) {
      commit('comeBackHistory')

    },
    async getTelegramCode({dispatch}) {
      let response = {}
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/api/smartsender/code`,
        }, {root: true})

        if (res && res.data.status === 'success') {
          response = res.data
        }
      })
      return response
    },
    async sendTelegramUsername({dispatch}, {username}) {
      let response = {}

      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          query: '/api/smartsender/username',
          type: 'post',
          sendData: {
            username
          },
        }, {root: true})

        if (res && res.data.status === 'success') {
          response.status = res.data.status
        }
      })
      return response
    },
    async setCourse({dispatch}, {course, userId}) {
      let response = {}

      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: userId ? `/api/questionnaire/toggle-speed-hundred/${course}/${userId}` : `/api/questionnaire/toggle-speed-hundred/${course}`
        }, {root: true})

        if (res && res.data.status === 'success') {
          response.status = res.data.status
        }
      })
      return response
    },
    async getRecommendedCourse({dispatch}) {
      let response = {}

      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/api/questionnaire/course`,
        }, {root: true})

        if (res && res.data.status === 'success') {
          response = {
            status: 'success',
            data: res.data.data,
          }
        }
      })
      return response
    },
    async setFaculty({dispatch}, {faculty, userId}) {
      let response = {}

      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/api/faculty/set/${faculty}`,
          sendData: {
            user: userId
          }
        }, {root: true})

        if (res && res.data.status === 'success') {
          response.status = res.data.status
        }
      })
      return response
    },
    async getUserFaculty({dispatch}, {user}) {
      let response = {}
      await tryCatch( async () => {
        let res = await dispatch('ajax/axiosSend',{
          type: 'get',
          query: `/api/faculty/list-for-user/${user}`,
        },{ root:true });

        if (res && res.data.status === 'success') {
          response = {
            status: 'success',
            data: _.cloneDeep(res.data.data),
          }
        }
      })
      return response
    }
  },
  mutations: {
    ...initialMutations,
    updateHistory(state) {
      if(state.data.history.slice !== state.data.activeQuestionId)
        state.data.history.push(state.data.activeQuestionId)
    },
    comeBackHistory(state) {
      let questionId = state.data.history.pop()
      state.data.activeQuestionId = questionId
    }
  }
}

import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import createQueryString from "@lms/functions/createQueryString"
import tryCatch from '@lms/functions/tryCatch';

export default {
    namespaced: true,
    state:{
        data:{
            captains:[
            ],
            teams:{},
            updateBlocks:{},
            captainList:[],
            searchingCaptain: '',
            teamAllWeeks: {}
        },
        meta:{
            isUpdate: false,
            isLoadCaptainList: false,
            isLoadTeam: false,
            isLoadAddCaptain: false,
            isSetCaptainWeeks: false,
        },
        aside:{
            data:{
                addCaptains: []
            },
            meta:{
                canSave: false,
                isOpen: false
            }
        }
    },
    actions:{
        ...initialActions,
        async getTractionMapCaptianList({commit, dispatch}){
            await tryCatch ( async () => {
                commit('setMeta', {
                    path: 'isLoadCaptainList',
                    value: true
                });

                let res = await dispatch(
                    'ajax/axiosSend', 
                    {
                        type: 'get',
                        query: '/teams/report/captain/get' + createQueryString(),
                        // query: '/teams/report/captain/get',
                        cancel: {name: 'cancelCaptainList'},
                    },
                    { root: true }
                );
                
                if( res && res.data.status === 'success')
                    commit('setData', { path: 'captains', value: res.data.data.captains})

                commit('setMeta', {
                    path: 'isLoadCaptainList',
                    value: false
                });
            })
        },
        async getTractionMapTeam({commit, dispatch, state}, { id, captainId } ){
            await tryCatch ( async () => {
                commit('setMeta', {
                    path: 'isLoadTeam',
                    value: true
                });

                let res = await dispatch(
                    'ajax/axiosSend', 
                    {
                        type: 'get',
                        query: '/teams/report/team/get',
                        cancel: {name: 'cancelTeam'},
                        params:{
                            teamId : id
                        }
                    },
                    { root: true }
                );
            
                if( res && res.data.status === 'success')
                    commit('setTeams',{data: res.data.data.reports[0], captainId: captainId, id: id})
            
                commit('setMeta', {
                    path: 'isLoadTeam',
                    value: false
                });
            })
        },
        async addTractionMapCaptains({commit, dispatch, state}){
            await tryCatch ( async () => {
                commit('setMeta', {
                    path: 'isLoadAddCaptain',
                    value: true
                });

                let captainList = new Set(state.aside.data.addCaptains.map(el => el.id))

                let res = await dispatch(
                    'ajax/axiosSend', 
                    {
                        type: 'post',
                        query: '',
                        sendData:{
                            captains: [...captainList]
                        }
                    },
                    { root: true }
                );
                
                if( res && res.data.status === 'success'){

                }
                
                commit('setMeta', {
                    path: 'isLoadAddCaptain',
                    value: false
                });
            })
        },
        async setTractionMapCaptainWeeks({commit, dispatch}, { id, teamId, weeks }){
            await tryCatch ( async () => {
                let res = await dispatch(
                    'ajax/axiosSend', 
                    {
                        type: 'post',
                        query: '/teams/report/captain/edit',
                        sendData:{
                            analytics: {
                                id: id,
                                weeks: weeks
                            }
                        }
                    },
                    { root: true }
                );
                
                if( res && res.data.status === 'success'){

                }
            })
        },
        async setTractionMapTeamWeeks({commit, dispatch}, { id, teamId, weeks }){
            await tryCatch ( async () => {
                let res = await dispatch(
                    'ajax/axiosSend', 
                    {
                        type: 'post',
                        query: '/teams/report/user/edit',
                        sendData:{
                            analytics: {
                                id: id,
                                teamId: teamId,
                                weeks: weeks
                            }
                        }
                    },
                    { root: true }
                );
            })
        },
        async updateTractionMapTeamName({commit, dispatch}, { data, id, captain}){
            await tryCatch( async () => {
                let res = await dispatch(
                    'ajax/axiosSend',
                    {
                        type: 'post',
                        query: `/users/team/${id}/edit/name`,
                        sendData: {
                            "team": data
                        }
                    },
                    { root: true }
                );

                if( res && res.data.status === 'success' ){
                    commit('setNewTeamName', { value: data.name, id: id, captain: id})
                }
            })
        },
        async setAuthValue({commit, dispatch, rootState}, {id, captain, siteLink, profitPerYear, profitPerLastYear}){
            await tryCatch( async () => {
                let obj = {
                    id: id
                }
                if(siteLink)
                    obj.siteLink = siteLink
                if(profitPerYear)
                    obj.profitPerYear = profitPerYear
                if(profitPerLastYear)
                    obj.profitPerLastYear = profitPerLastYear
                
                let res = await dispatch(
                    'ajax/axiosSend',
                    {
                        type: 'post',
                        query: `/admin/user/server/edit`,
                        sendData: {
                            user: obj,
                        }
                    },
                    { root: true }
                );
                if( res && res.data.status === 'success'){
                    delete obj.id
                    commit('setNewData', {captain: captain, id: id, data: obj})
                }
            })
        },
        async deleteTractionMapCaptain({ commit, dispatch }, { id }){
            await tryCatch( async () => {
                let res = await dispatch(
                    'ajax/axiosSend',
                    {
                        type: 'post',
                        query: '/users/team/${teamId}/save',
                        sendData: {
                            anylics
                        }
                    },
                    { root: true }
                )
                if( res && res.data.status === 'success' )
                    commit('deleteTractionMapCaptain', { id: id})
            })
        },
        async deleteTractionMapTeam({ commit, dispatch, state}, { users, teamId , captainId}){
            await tryCatch( async () => {
                let res = await dispatch(
                    'ajax/axiosSend',
                    {
                        type: 'post',
                        query: `/users/team/${teamId}/save`,
                        sendData: {
                            users: users
                        }
                    },
                    { root: true }
                );
                if( res && res.data.status === 'success' ){
                    dispatch('getTractionMapCaptianList')
                }
            })
        },
        async searchTractionMapCaptain({commit, dispatch}, {value}){
            await tryCatch( async () => {
                let res = await dispatch(
                    'ajax/axiosSend',
                    {
                        type: 'get',
                        query: '',
                        cancel:{ name:'searchTractionMapCaptain'},
                        sendData: {
                            name: value
                        }
                    },
                    { root: true }
                );
                if( res && res.data.status === 'success' )
                    commit('setData', { path: 'captainsList', value: res.data.data})
            })
        },
        setUpdateBlocks({ commit }, { boolean, id}){
            boolean ? commit('addUpdateBlock', id) : commit('removeUpdateBlock', id)
        }
    },
    mutations:{
        ...initialMutations,
        addUpdateBlock( state, id)
        {
            let object = _.cloneDeep(state.data.updateBlocks)
            object[id] = true
            state.data.updateBlocks = {...object}
        },
        removeUpdateBlock( state, id)
        {
            let object = _.cloneDeep(state.data.updateBlocks)
            delete object[id]
            state.data.updateBlocks = {...object}
        },
        setNewTeamName(state, {id, value, captain}){
            state.data.captains = [...state.data.captains.map( el => {
                if( el.captain.user.id == captain)
                    el.teams = [...el.teams].map( elem => {
                        if(elem.id == id)
                            elem.name = data
                        return {...elem}
                    })
                return el
            })]
        },
        setTeams(state, { data, captainId,id }){
            if(state.data.teams[captainId])
                state.data.teams[captainId][id] = [...data.weeks]
            else 
            {
                state.data.teams[captainId] = {}
                state.data.teams[captainId][id] = [...data.weeks]
            }
        },
        setNewData(state, {id, captain, data}){
            let key = Object.keys(data)[0]
            if(captain){
                state.data.teams[captain][id] = {...state.data.teams[captain][id].map( el => {
                    if(el.weeks.user.id == id)
                        el.weeks.user[key] = data[key]
                    return {...el}
                })} 
            }else {
                state.data.captains = [...state.data.captains.map( el => {
                        if(el.captain.user.id == id)
                            el.captain.user[key] = data[key]
                        return {...el}
                    })]
            }
        }
    }
}
import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import createQueryString from "@lms/functions/createQueryString"
import tryCatch from "@lms/functions/tryCatch"

export default {
  namespaced: true,
  state: {
    data: {
      users: [],
      groups:[],
      userId:'',
      pages:{
        page:1
      },
      userCoruseList:[],
      courseList:{},
      selectedCourse: [],
      selectedData: [],
      mentors:[]
    },
    meta: {
      gettingUsers: false,
      loadedUsers: false,
      loaded:false,
      isAddingPage:false,
      loadedSoloUsers:false,
      noMorePage: false,
    },
    aside: {
      data: {
        stagedUser: {},
        user:{}
      },
      meta: {
        canSave: false,
        isOpen: false,
        forceClose: false,
      }
    }
  },
  actions: {
    ...initialActions,
    async getUsers({commit, dispatch, state}, data){
      const page =  data ||  state.data.pages
      commit('setMeta', { path:'loadedUsers', value:true });
      commit('setMeta', { path:'loaded', value:true });
      await tryCatch(async () => {
        commit('setMeta', {path: 'gettingUsers', value: true})
        let res = await dispatch('ajax/axiosSend', {
          type: 'get', 
          query: `/user/list/get`+createQueryString(),
          params: page, 
          cancel: {name: 'gettingUsers'}}, 
          {root: true})
        if (res.data.status === 'success') {
          if (res.data.data) {
            /* очищаем дата стор перед заполнением новыми данными */
            Object.entries(res.data.data).forEach((entry) => {
              commit('setData', {path: entry[0], value: entry[1]})
            })
          }
          commit('setMeta', { path:'loadedUsers', value:false })
          commit('setMeta', { path:'loaded', value:false })
        }
        commit('setMeta', { path:'gettingUsers', value: false })
      })
    },
    async addUser({commit, dispatch, state}) {
      let status;
      let arrId = state.data.selectedCourse.map( el => el.courseId)
      commit('setMeta', { path:'loadedUsers', value:true });

      await tryCatch(async () => {
        let user = _.cloneDeep(state.aside.data.user);

        let courseName = _.cloneDeep(user.select);
        delete user.select
        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/admin/registerAuthUser`,
          sendData: {user,courseName}
        }, {root: true})
        status = res.data.status
        if (res.data.status === 'success') {
          user.authUserId = res.data.data.authUserId
          user.lmsUserId = res.data.data.lmsUserId
          commit('setData',{path:'userId', value:user.lmsUserId})
          commit('addUser', {user})
          if(arrId.length > 0)
            dispatch('editUsersDataList', user.lmsUserId)
        }

        commit('setMeta', {path:'loadedUsers',value:false})
      })
      return status
    },
    async editUser({ commit, dispatch, state }){
      let status;
      let arrId = state.data.selectedCourse.map( el => el.courseId);
      commit('setMeta', { path:'loadedUsers', value:true });
      await tryCatch(async () => {
        let user = _.cloneDeep(state.aside.data.user);

        let courseName = _.cloneDeep(user.select);
        delete user.select
        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/admin/user/server/edit`,
          sendData: {user,courseName}
        }, {root: true})
        
        status = res.data.status
        if (res.data.status === 'success') {
          user.authUserId = res.data.data.user.authUserId
          user.lmsUserId = res.data.data.user.lmsUserId
          commit('setData',{path:'userId', value: user.lmsUserId})
          commit('editUser', {user})
          if(arrId.length > 0)
            dispatch('editUsersDataList', user.lmsUserId)
        }
        commit('setMeta', {path:'loadedUsers',value:false})
      })
      return status
    },
    async addPage({commit, dispatch},page){
      commit('setMeta',{path:'isAddingPage', value:true})
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'get', 
          query: `/user/list/get`+createQueryString(),
          params: page, 
          cancel: {name: `cancelAddUsers${page.page}`}
        }, {root: true})

        if (res.data.status === 'success') {
          if (res.data.data) {
            //Если приходит пустой массив юзеров, ставим флаг noMorePage
            if(res.data.data.users.length)
              commit('addPage', res.data.data.users);
            else
              commit('setMeta', {path:'noMorePage', value: true})

          }
          commit('setMeta',{path:'isAddingPage', value:false})
        }
      })
    },
    async removePage({commit, dispatch}){
      tryCatch( async () => {
        commit('removePage')
      })
    },
    async searchMethods({commit, dispatch,state}){    
      commit('setMeta', {path:'noMorePage', value:false})
      commit('setMeta', { path:'loadedUsers', value:true });
      await tryCatch( async () => {
        commit('setMeta', {path: 'gettingUsers', value: true})       

        let res = await  dispatch('ajax/axiosSend',{
          type:'get',
          query:'/user/list/get'+createQueryString(),
          params:state.data.pages,
          cancel:{name:'cancelSearch'}
        }, {root:true})
        if (res.data.status === 'success') {
          if (res.data.data) {
            Object.entries(res.data.data).forEach((entry) => {
              commit('setData', {path: entry[0], value: entry[1]})
            })
          }
          commit('setMeta', { path:'loadedUsers', value:false })
        }
        commit('setMeta', { path:'gettingUsers', value: false })
      })
    },
    async searchMethodsSolo({commit, dispatch,state},string){
      commit('setMeta', { path:'loadedSoloUsers', value:true });
      await tryCatch( async () => { 

        let res = await  dispatch('ajax/axiosSend',{
          type:'get',
          query:'/user/list/get',
          params:{
            name:string
          },
          cancel:{name:'cancelSoloSearch'}
        }, {root:true})
        if (res && res.data.status === 'success') {
          if (res.data.data) {
            Object.entries(res.data.data).forEach((entry) => {
              commit('setData', {path: entry[0], value: entry[1]})
            })
          }
          commit('setMeta', { path:'loadedSoloUsers', value:false })
        }
      })
    },
    async getUsersCourse({ commit, dispatch }, { id }){
      await tryCatch( async () => {
        let res = await dispatch('ajax/axiosSend', {
            type: 'post',
            query: '/admin/user/course/tariff/get',
            sendData:{
              userId: id
            }
          }, 
            { root: true }
        )
        if (res && res.data.status === 'success'){
          commit('setData', { path:'userCoruseList', value: res.data.data.userCourseTariffs || [] })
          res.data.data.userCourseTariffs.forEach( el => {
            dispatch('setSelectedCourse', {id: el.courseId})
          })
        }
      })
    },
    async getUsersDataList({commit, dispatch }, { id }){
      await tryCatch( async () => {
        let res = await dispatch('ajax/axiosSend', {
            type: 'post',
            query: '/admin/course/tariffs/get',
            sendData:{
              courseId: id
            }
          }, 
            { root: true }
        );
        if (res && res.data.status === 'success')
          dispatch('setDataCourseListUser', { data: res.data.data, course: id})
      })
    },
    async editUsersDataList( { dispatch, state }, id ){
      await tryCatch( async () => {
        let res = await dispatch( 'ajax/axiosSend', {
          type:'post',
          query:'/admin/user/course/tariff/edit',
          sendData:{
            userCourseTariffs: [...state.data.selectedCourse.map( el => {
                let obj = {
                  userId: id, 
                  tariffId: el.tariffId,
                  courseId: el.courseId,
                }
                if ( el.id )
                  obj.id = el.id
                if ( el.tracker.id )
                  obj.trackerId = el.tracker.id
                if ( el.manager.id )
                  obj.managerId = el.manager.id
                if( el.faculty)
                  obj.faculty = el.faculty
                if(el.mentor.id)
                  obj.mentorId = el.mentor.id
                  
                return obj
              })
            ]
          }
        },
        { root: true })
        if( res && res.data.status === 'success')
          return
      })
    },
    async getUsersMentors({commit, dispatch}, data = {}){
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'get', 
          query: `/user/list/get`,
          params: data, 
          cancel: {name: 'getUsersMentors'}}, 
          {root: true})
        if (res.data.status === 'success') 
          commit('setData', {path: 'mentors', value: res.data.data.users ? res.data.data.users : [] })
      })
    },
    setSelectedCourse({ commit, state }, { id, remove = false }){
      let data = _.cloneDeep(state.data.selectedData)
      if( remove )
        data = data.filter( el => el != id)
      else 
        data = data.some( el => el == id) ?  data : [...data, id]
      
      commit('setData', { path:'selectedData', value: data})
    },
    setDataCourseListUser({ commit }, {data, course}){
      commit('setUsersDataList', {course: course, data: data })
    },
  },
  mutations: {
    ...initialMutations,
    addUser(state, {user}) {
      state.data.users.unshift(user)
      state.data.users = [...state.data.users]
    },
    editUser(state, {user}) {
      let data = _.cloneDeep(state.data.users)
      data = data.filter(el => el.id != user.id)
      data.unshift(user)
      state.data.users = data
    },
    addPage(state, value){
      state.data.users = [].concat(state.data.users, value)
    },
    removePage(state){
      const arr = state.data.users
      arr.length = state.data.users.length - 50
      state.data.users = [...arr]
    },
    setUsersDataList( state, { data, course}){
      let oldState = _.cloneDeep(state.data.courseList)
      oldState[course] = {...data}
      state.data.courseList = {...oldState}
    }
  },
}
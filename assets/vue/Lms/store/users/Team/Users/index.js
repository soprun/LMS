import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import tryCatch from "@lms/functions/tryCatch"
import getQueryObject from '@lms/functions/getQueryObject'

export default {
  namespaced: true,
  state: {
    data: {
      team:{},
      teammates: [],
      folder:''
    },
    meta: {
      courseSelect: false,
      gettingUsers: false,
      deletingUser: false,
      isAddingPage:false,
      isLoadUsers:false,
      load:false
    },
    aside: {
      data: {
        user: {},
        users:[],
        stagedUser: {},
        stagedUsers: [],
        notUsual:{
          isCaptain:false,
          isLeader:false,
        }
      },
      meta: {
        canSave: false,
        isOpen: false,
        forceClose: false,
        addingUsers: false,
        step: 1
      }
    }
  },
  actions: {
    ...initialActions,
    async getUsers({commit, dispatch}, {teamId}) {
      await tryCatch(async () => {
        commit('setMeta', {path: 'gettingUsers', value: true})
        let params = {
          page: 1
        }
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/users/team/${teamId}/get`,        
          cancel: {name: 'gettingUsers'}
        }, {root: true})

        if (res.data.status === 'success') {
          if (res.data.data) {
            /* очищаем дата стор перед заполнением новыми данными */
            Object.entries(res.data.data).forEach((entry) => {
              commit('setData', {path: entry[0], value: entry[1]})
            })
            if (!res.data.data.teammates)
              commit('setData', {path: 'teammates', value: []})
          }
        }
        commit('setMeta', {path: 'gettingUsers', value: false})
      })
    },
    async saveTeam({commit, dispatch, state}, {teamId}){
      let obj = {}
      state.aside.data.team.forEach((el, idx) => {
        if(!obj[el.id])
          obj[el.id] = el
        else
          obj[el.id] = el
      })
      
      let array = Object.values(obj).filter(el=> el).map(el=> {

        return { 
          isNew:el.isNew,
          id: el.id,
          isCaptain: el.isCaptain,
          isLeader: el.isLeader,
          isDelete: el.isDelete
        }
      })
      await tryCatch( async () => {
        let res = await dispatch('ajax/axiosSend',{
          type:'post',
          query:`/users/team/${teamId}/save`,
          sendData: {
            users: array
          },
        },{root:true})

        if(res && res.data.status === 'success'){
          dispatch('getUsers', {teamId})
        }
      })
    },
    async searchMethods({commit, dispatch}, {name, id}){
      await tryCatch( async ()=> {
        commit('setMeta', {path: 'isLoadUsers', value: true})
          let res = await dispatch('ajax/axiosSend',{
            type:'get',
            query: `/users/group/${id}/user/get`,
            params:{
              name: name
            },
            cancel:{name:'getting'}
          },{root:true})
          if(res && res.data.status == 'success'){
            Object.entries(res.data.data).forEach((entry) => {
              commit('setAsideData', {path: entry[0], value: entry[1]})
            })
          }
          commit('setMeta',{path:'isLoadUsers', value:false})
      })
    },
    async clearUsers({commit}) {
      await tryCatch(async () => {
        commit('setData', {path: 'teammates', value: []})
      })
    },
    incrementStep({commit, state}){
      commit('setAsideMeta',{path:'step', value:state.aside.meta.step+=1})
    },
    dicrementStep({commit, state}){
      commit('setAsideMeta', {path:'step', value:state.aside.meta.step-=1})
    }
  },
  mutations: {
    ...initialMutations,
    deleteUser(state, userId) {
      state.data.teammates = [...state.data.teammates.filter(el => el.id !== userId)]
    },
    addUsers(state, {newUsers}) {
      state.data.users = [
        ...newUsers,
        ...state.data.users
      ]
    },
    addPage(state, data){
      state.data.users = [].concat(state.data.users, data)
    }
  },
}
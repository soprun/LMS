import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import tryCatch from "@lms/functions/tryCatch"

export default {
  namespaced: true,
  state: {
    data: {
      folders: [],
    },
    meta: {
      isLoadTeams: false
    },
    aside: {
      data: {
        team: {},
        stagedGroup: {},
        folders: []
      },
      meta: {
        canSave: false,
        isOpen: false,
        forceClose: false,
        gettingTeams: false,
      }
    }
  },
  actions: {
    ...initialActions,
    async getListTeams({commit, dispatch}){
      await tryCatch(async () => {
        commit('setMeta', {path: 'isLoadTeams', value: true})

        let res = await dispatch('ajax/axiosSend', {type: 'get', query: `/users/team/list/get`, cancel: {name: 'gettingTeams'}}, {root: true})


        if (res.data.status === 'success') {
          if (res.data.data) {
            /* очищаем дата стор перед заполнением новыми данными */
            Object.entries(res.data.data).forEach((entry) => {
              commit('setData', {path: entry[0], value: entry[1]})
            })
          }
        }
        commit('setMeta', {path: 'isLoadTeams', value: false})
      })
    },
    async addTeam({commit, dispatch, state}) {
      await tryCatch(async () => {
        commit('setAsideMeta', {path: 'addingGroup', value: true})
        commit('setMeta', {path: 'isLoadTeams', value: true})
        let team = _.cloneDeep(state.aside.data.team)
        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/users/team/new`,
          sendData: {team},
        }, {root: true})

        if (res.data.status === 'success') {
          dispatch('getListTeams', null)
          commit('setMeta', {path: 'isLoadTeams', value: false})
        }
        commit('setAsideMeta', {path: 'addingGroup', value: false})
      })
    },
    async editTeam({commit, dispatch, state}) {

      await tryCatch(async () => {
        commit('setAsideMeta', {path: 'addingGroup', value: true})
        let team = state.aside.data.team
        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/users/team/${state.aside.data.team.id}/edit`,
          sendData: {team},
        }, {root: true})

        if (res.data.status === 'success') {
          dispatch('getListTeams',null)
        }
        commit('setAsideMeta', {path: 'addingGroup', value: false})
      })
    },
    async deleteTeam({commit, dispatch, state}, id) {
      await tryCatch(async () => {
        commit('setAsideMeta', {path: 'deletingGroup', value: true})

        let res = await dispatch('ajax/axiosSend', {
          type: 'delete',
          query: `/users/team/${id}/delete`,
        }, {root: true})

        if (res.data.status === 'success') {
          dispatch('getListTeams',null)
        }
        commit('setAsideMeta', {path: 'deletingGroup', value: false})
      })
    },
  },
  mutations: {
    ...initialMutations,
    addGroup(state, {newGroup}) {
      state.data.folders[0].groupArray.unshift(newGroup)
      state.data.folders[0].groupArray = [...state.data.folders[0].groupArray]
    },

    editGroup(state, {group}) {
      let groupIdx = state.data.folders[0].groupArray.findIndex(g => g.id === group.id)

      if(groupIdx !== -1) {
        state.data.folders[0].groupArray[groupIdx] = _.cloneDeep(group)
        state.data.folders[0].groupArray = [...state.data.folders[0].groupArray]
      }
    },
    deleteGroup(state, {groupId}) {
      let groupIdx = state.data.folders[0].groupArray.findIndex(g => g.id === groupId)

      if(groupIdx !== -1) {
        state.data.folders[0].groupArray.splice(groupIdx, 1)
        state.data.folders[0].groupArray = [...state.data.folders[0].groupArray]
      }
    }
  },
}
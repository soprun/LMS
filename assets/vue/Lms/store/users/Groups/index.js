import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import tryCatch from "@lms/functions/tryCatch"
import Vue from "vue";

function transformGroupPermissions(groupPermissions) {
  return {
    isAdmin: groupPermissions.isAdmin,
    folderModuleAdmin: transformModulePermissions(groupPermissions.folderModuleAdmin),
    courseModuleAdmin: transformModulePermissions(groupPermissions.courseModuleAdmin),
    lessonModuleAdmin: transformModulePermissions(groupPermissions.lessonModuleAdmin),
    taskAnswerCheckAdmin: transformModulePermissions(groupPermissions.taskAnswerCheckAdmin),
    userGroupModuleAdmin: transformModulePermissions(groupPermissions.userGroupModuleAdmin),
    userTeamModuleAdmin: transformModulePermissions(groupPermissions.userTeamModuleAdmin),
    analyticsModelAdmin: transformModulePermissions(groupPermissions.analyticsModelAdmin),
  }
}

function transformModulePermissions(modulePermissions) {
  return `${modulePermissions.create}${modulePermissions.read}${modulePermissions.update}${modulePermissions.delete}`
}

export default {
  namespaced: true,
  state: {
    data: {
      folders: [],
    },
    meta: {
      isLoadGroup: false
    },
    aside: {
      data: {
        group: {},
        stagedGroup: {},
        folder: {},
        stagedFolder: {},
        folders: [],
        user: {}
      },
      meta: {
        canSave: false,
        isOpen: false,
        isUserOpen: false,
        forceClose: false,
        gettingGroups: false,
        step: 1,
        courseSelect: false
      }
    }
  },
  actions: {
    ...initialActions,
    async getGroups({commit, dispatch}){
      await tryCatch(async () => {
        commit('setMeta', {path: 'isLoadGroup', value: true})

        let res = await dispatch('ajax/axiosSend', {type: 'get', query: `/users/group/list/get`, cancel: {name: 'gettingGroups'}}, {root: true})


        if (res.data.status === 'success') {
          if (res.data.data) {
            /* очищаем дата стор перед заполнением новыми данными */
            Object.entries(res.data.data).forEach((entry) => {
              commit('setData', {path: entry[0], value: entry[1]})
            })
          }
        }
        commit('setMeta', {path: 'isLoadGroup', value: false})
      })
    },
    async getGroupPermission({commit, dispatch}, groupId) {
      return await tryCatch(async () => {
        if (groupId) {
          commit('setMeta', {path: 'isLoadGroup', value: true})
          const res = await dispatch('ajax/axiosSend', {
            type: 'get',
            query: `/groups/${groupId}`
          }, { root: true })
          commit('setMeta', {path: 'isLoadGroup', value: false})
          if (res.data.status === 'success' && res.data.data) {
            commit('setGroupPermissions', res.data.data)
            return res.data.data
          }
        } else {
          return {}
        }
      })
    },
    async getCourseSelect({commit, dispatch}) {
      await tryCatch(async () => {
        commit('setAsideMeta', {path: 'courseSelect', value: true});
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/courses/select/get`,
          cancel: {name: 'cancelCourseSelect'}
        }, {root: true});
        if (res.data.status === 'success' && res.data.data) {
          commit('setAsideData', {path: 'folders', value: res.data.data.folders})
        }
        
      })
      commit('setAsideMeta', {path: 'courseSelect', value: false});
    },
    async addGroup({commit, dispatch, state}) {
      if(state.aside.meta.addingGroup)
        return

      await tryCatch(async () => {
        commit('setAsideMeta', {path: 'addingGroup', value: true})
        commit('setMeta', {path: 'isLoadGroup', value: true})
        const userGroup = {
          name: state.aside.data.stagedGroup.name,
          courseFolders: state.aside.data.stagedGroup.courseFolders,
          courses: state.aside.data.stagedGroup.courses,
          groupFolder: state.aside.data.stagedGroup.folderId,
          groupPermissions: transformGroupPermissions(state.aside.data.stagedGroup.permissions)
        }
        const res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/users/group/new`,
          sendData: userGroup,
        }, {root: true})

        if (res.data.status === 'success') {
          userGroup.id = res.data.data.id
          // commit('addGroup', userGroup)
          dispatch('getGroups',null)
          commit('setMeta', {path: 'isLoadGroup', value: false})
        }
        commit('setAsideMeta', {path: 'addingGroup', value: false})
      })
    },
    async editGroup({commit, dispatch, state}) {
      if(state.aside.meta.addingGroup)
        return

      commit('setAsideMeta', {path: 'addingGroup', value: true})
      await tryCatch(async () => {
        let userGroup = _.cloneDeep(state.aside.data.stagedGroup)

        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/users/group/${userGroup.id}/edit`,
          sendData: {userGroup},
        }, {root: true})

        if (res.data.status === 'success') {
          // commit('editGroup', {group: userGroup})
          dispatch('getGroups')
        }
      })
      commit('setAsideMeta', {path: 'addingGroup', value: false})
    },
    async deleteGroup({commit, dispatch, state}, {group}) {
      if(state.aside.meta.deletingGroup)
        return

      await tryCatch(async () => {
        commit('setAsideMeta', {path: 'deletingGroup', value: true})

        let res = await dispatch('ajax/axiosSend', {
          type: 'delete',
          query: `/users/group/${group.id}/delete`,
        }, {root: true})

        if (res.data.status === 'success') {
          // commit('deleteGroup', {groupId: group.id})
          dispatch('getGroups',null)
        }
        commit('setAsideMeta', {path: 'deletingGroup', value: false})
      })
    },
    async addFolder({commit, dispatch, state}) {
      if(state.aside.meta.addingFolder)
        return

      commit('setAsideMeta', {path: 'addingFolder', value: true})
      commit('setMeta', {path: 'isLoadGroup', value: true})

      await tryCatch(async () => {
        let groupFolder = _.cloneDeep(state.aside.data.stagedFolder)
        groupFolder.parentId = groupFolder.parentId !== undefined ? groupFolder.parentId : ''

        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/users/groupFolder/add`,
          sendData: {groupFolder},
        }, {root: true})

        if (res.data.status === 'success')
          dispatch('getGroups')
      })

      commit('setAsideMeta', {path: 'addingFolder', value: false})
    },
    async editFolder({commit, dispatch, state}) {
      if(state.aside.meta.editingFolder)
        return

      commit('setAsideMeta', {path: 'editingFolder', value: true})
      commit('setMeta', {path: 'isLoadGroup', value: true})

      await tryCatch(async () => {
        let groupFolder = _.cloneDeep(state.aside.data.stagedFolder)

        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/users/groupFolder/${groupFolder.id}/edit`,
          sendData: {groupFolder},
        }, {root: true})

        if (res.data.status === 'success')
          dispatch('getGroups')
      })

      commit('setAsideMeta', {path: 'editingFolder', value: false})
    },
    async deleteFolder({commit, dispatch, state}, {folder}) {
      if(state.aside.meta.deletingFolder)
        return

      commit('setAsideMeta', {path: 'deletingFolder', value: true})
      commit('setMeta', {path: 'isLoadGroup', value: true})

      await tryCatch(async () => {

        let res = await dispatch('ajax/axiosSend', {
          type: 'delete',
          query: `/users/groupFolder/${folder.id}/delete`,
        }, {root: true})

        if (res.data.status === 'success')
          dispatch('getGroups')
      })

      commit('setAsideMeta', {path: 'deletingFolder', value: false})
    },
    incrementStep({commit, state}){
      commit('setAsideMeta',{path:'step', value:state.aside.meta.step+=1})
    },
    dicrementStep({commit, state}){
      commit('setAsideMeta', {path:'step', value:state.aside.meta.step-=1})
    }
  },
  mutations: {
    ...initialMutations,
    setGroupPermissions(state, permissions) {
      Vue.set(state.aside.data.group, 'permissions', permissions)
    },
    addGroup(state, group) {
      const newFolderGroup = { name: group.name, id: group.id, count: 0 }
      const folderId = state.data.folders.findIndex(f => f.folder.id === group.groupFolder)
      Vue.set(
        state.data.folders[folderId],
        'groupArray',
        [newFolderGroup, ...state.data.folders[folderId].groupArray]
      )
    },
    editGroup(state, {group}) {
      let groupIdx = state.data.folders[0].groupArray.findIndex(g => g.id === group.id)

      if(groupIdx !== -1) {
        state.data.folders[0].groupArray[groupIdx] = _.cloneDeep(group)
        state.data.folders[0].groupArray = [...state.data.folders[0].groupArray]
      }
    },
    deleteGroup(state, {groupId}) {
      let groupIdx = state.data.folders[0].groupArray.findIndex(g => g.id === groupId)

      if(groupIdx !== -1) {
        state.data.folders[0].groupArray.splice(groupIdx, 1)
        state.data.folders[0].groupArray = [...state.data.folders[0].groupArray]
      }
    }
  },
}

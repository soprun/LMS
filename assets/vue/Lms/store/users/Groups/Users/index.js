import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import tryCatch from "@lms/functions/tryCatch"
import createQueryString from '@lms/functions/createQueryString'
import getQueryObject from '@lms/functions/getQueryObject'

export default {
  namespaced: true,
  state: {
    data: {
      users: [],
    },
    meta: {
      courseSelect: false,
      gettingUsers: false,
      deletingUser: false,
      isAddingPage: false,
      load: false
    },
    aside: {
      data: {
        user: {},
        stagedUser: {},
        stagedUsers: []
      },
      meta: {
        canSave: false,
        isOpen: false,
        forceClose: false,
        addingUsers: false,
        step: 1
      }
    }
  },
  actions: {
    ...initialActions,
    async getUsers({commit, dispatch}, {groupId}) {
      let res;
      await tryCatch(async () => {
        commit('setMeta', {path: 'gettingUsers', value: true})
        let params = {
          page: 1
        }
        res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/users/group/${groupId}/user/get`,
          params: params,
          cancel: {name: 'gettingUsers'}
        }, {root: true})

        if (res.data.status === 'success') {
          if (res.data.data) {
            /* очищаем дата стор перед заполнением новыми данными */
            Object.entries(res.data.data).forEach((entry) => {
              commit('setData', {path: entry[0], value: entry[1]})
            })
            if (!res.data.data.users)
              commit('setData', {path: 'users', value: []})
          }
        }
        commit('setMeta', {path: 'gettingUsers', value: false})
      })

      if(res)
        return res.data.data
    },
    async getMoreUsers({commit, dispatch}, {groupId, page}) {
      if (!page || !groupId)
        return {status: error}

      let actionResponse = {};
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/users/group/${groupId}/user/get`,
          params: {
            page
          },
          cancel: {name: 'gettingUsers'}
        }, {root: true})

        if (res.data.status === 'success') {
          if (res.data.data) {
            actionResponse = {status: 'success', ...res.data.data}
          }
        }
      })
      return actionResponse
    },
    async addUsers({commit, dispatch, state}, {groupId, mode}) {
      if (state.aside.meta.addingUsers)
        return

      await tryCatch(async () => {
        commit('setAsideMeta', {path: 'addingUsers', value: true})
        let newUsers = _.cloneDeep(state.aside.data.stagedUsers)

        let userIds = state.aside.data.stagedUsers.map(u => u.id)

        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/users/group/${groupId}/user/add`,
          sendData: {users: userIds},
        }, {root: true})

        if (res && !mode && res.data.status === 'success')
          commit('addUsers', {newUsers})
        else if (res && mode && res.data.status === 'success')
          await dispatch('getUsers', {groupId})

        commit('setAsideMeta', {path: 'addingUsers', value: false})
      })
    },
    async deleteUser({commit, dispatch, state}, {user, groupId}) {
      if (state.aside.meta.deletingUser)
        return

      await tryCatch(async () => {
        commit('setMeta', {path: 'deletingUser', value: true})
        let userId = user.id

        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/users/group/${groupId}/user/remove`,
          sendData: {users: [userId]},
        }, {root: true})

        if (res.data.status === 'success') {
          commit('deleteUser', {userId})
        }
        commit('setMeta', {path: 'deletingUser', value: false})
      })
    },
    async clearUsers({commit}) {
      await tryCatch(async () => {
        commit('setData', {path: 'users', value: []})
      })
    },
    async addPage({commit, dispatch}, page) {
      commit('setMeta', {path: 'isAddingPage', value: true})

      let res = await dispatch('ajax/axiosSend', {
        type: 'get',
        query: `/users/group/${getQueryObject().groupId}/user/get` + createQueryString(),
        cancel: {name: `gettingUsers${getQueryObject().groupId}`},
        params: {
          page: page
        }
      }, {root: true})

      if (res && res.data.status === 'success') {
        /* очищаем дата стор перед заполнением новыми данными */

        commit('addPage', res.data.data.users)

      }
      commit('setMeta', {path: 'isAddingPage', value: false})
    },
    async searchMethods({commit, dispatch}) {
      const objectRoute = getQueryObject()
      await tryCatch(async () => {
        commit('setMeta', {path: 'isAddingPage', value: true})
        commit('setMeta', {path: 'gettingUsers', value: true})
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/users/group/${objectRoute.groupId}/user/get` + createQueryString(),
          params: {
            page: 1
          },
          cancel: {name: 'getting'}
        }, {root: true})
        if (res && res.data.status == 'success') {

          Object.entries(res.data.data).forEach((entry) => {
            commit('setData', {path: entry[0], value: entry[1]})
          })
        }
        commit('setMeta', {path: 'isAddingPage', value: false})
        commit('setMeta', {path: 'gettingUsers', value: false})
      })
    },
    incrementStep({commit, state}) {
      commit('setAsideMeta', {path: 'step', value: state.aside.meta.step += 1})
    },
    dicrementStep({commit, state}) {
      commit('setAsideMeta', {path: 'step', value: state.aside.meta.step -= 1})
    }
  },
  mutations: {
    ...initialMutations,
    deleteUser(state, {userId}) {
      let userIdx = state.data.users.findIndex(u => u.id === userId)

      if (userIdx !== -1) {
        state.data.users.splice(userIdx, 1)
        state.data.users = [...state.data.users]
      }
    },
    addUsers(state, {newUsers}) {
      state.data.users = [
        ...newUsers,
        ...state.data.users
      ]
    },
    addPage(state, data) {
      state.data.users = [].concat(state.data.users, data)
    }
  },
}
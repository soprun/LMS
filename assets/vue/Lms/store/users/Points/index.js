import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import createQueryString from "@lms/functions/createQueryString"
import tryCatch from "@lms/functions/tryCatch"

export default {
  namespaced: true,
  state:{
    data:{
      users:[],
      timeSpent:'',
      parameters:'',
      groupList:[],
    },
    meta:{
      isLoading:false,
      isStartLoading: false,
    }
  },
  actions:{
    ...initialActions,
    async loadUsersPoints({commit,dispatch, state}, date){
      await tryCatch( async () => {
        commit('setMeta', {path:'isLoading', value:true})
        commit('setMeta', {path:'isStartLoading', value:true})
        let res = await dispatch('ajax/axiosSend',{
          type:'get',
          query:'/users/points/get',
          params:{
            groupIds: state.data.groupList.map(el=> el.id).join(','),
            date1:date[0],
            date2:date[1]
          }
        },{root:true})
        if(res && res.data.status === 'success'){
          Object.keys(res.data.data).forEach(el => {
            commit('setData', {path: el, value:res.data.data[el]})
          })
        commit('setMeta', {path:'isLoading', value:false})
        }
      })
    },
    incrementGroup({commit},element){
      commit('incrementGroup', element)
    },
    dicrementGroup({commit}, object){
      commit('dicrementGroup', object)
    },
    clearStoragePoints({commit,state}){
      commit('setMeta', {path:'isStartLoading', value:false})
      commit('setMeta', {path:'isLoading', value:false})
      Object.keys(state.data).forEach(el => {
        commit('setData', {path: el, value:''})
      })
      commit('setData', {path: 'groupList', value:[]})
    }
  },
  mutations:{
    ...initialMutations,
    incrementGroup: (state, object) => {
      
      let equal = state.data.groupList.some(el=> el.id === object.id && el.name === object.name );
      if(!equal){
        state.data.groupList.push(object)
      }
    },
    dicrementGroup: (state, object) => {
      state.data.groupList = state.data.groupList.filter(el=> el.id !== object.id && el.name !== object.name );
      

    },
  }
}
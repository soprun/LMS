import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import createQueryString from "@lms/functions/createQueryString"
import tryCatch from '@lms/functions/tryCatch';

export default {
  namespaced: true,
    state:{
      data:{
        rows: [
          {
            id: 'countUsers',
            title: 'Количество учтенных участников',
            type: 'чел',
            weeks: false,
            blue: false,
          },
          {
            id: 'fullIncomeBefore',
            title: 'Заработок участников ДО курса',
            type: 'руб. / нед.',
            weeks: false,
            blue: false,
          },
          {
            id: 'averagePerUserBefore',
            title: 'В среднем на участника до курса',
            type: 'руб. / нед.',
            weeks: false,
            blue: false,
          },
          {
            id: 'percentWithoutBusiness',
            title: 'Пришли без бизнеса',
            type: '%',
            weeks: false,
            blue: false,
          },
          {
            id: 'percentEarnedResult',
            title: 'Всего заработали',
            weeks: true,
            type: '%',
            blue: false,
          },
          {
            id: 'countEarnedMoreMillion',
            title: 'Заработали больше 1 млн руб',
            weeks: true,
            type: 'чел',
            blue: false,
          },
          {
            id: 'percentEarnedWithoutBusiness',
            title: 'Заработали с 0 из пришедших без бизнеса',
            weeks: true,
            type: 'чел',
            blue: false,
          },
          {
            id: 'countInvestments',
            title: 'Привлекли инвестиции',
            weeks: true,
            type: 'чел',
            blue: false,
          },
          {
            id: 'incomeInvestments',
            title: 'Сумма инвестиций',
            weeks: true,
            type: 'руб',
            blue: false,
          },
          {
            id: 'incomeInvestmentsPerUser',
            title: 'Инвестиций на участника',
            weeks: true,
            type: 'руб',
            blue: false,
          },
          {
            id: 'incomeInvestmentsPerCourse',
            title: 'Сумма инвестиций за курс',
            weeks: false,
            type: 'руб',
            blue: false,
          },
          {
            id: 'incomeInvestmentsPerCoursePerUser',
            title: 'Инвестиций на участника за курс',
            weeks: false,
            type: 'руб',
            blue: false,
          },
          {
            id: 'amountEarnedPerCourse',
            title: 'Заработанная сумма за курс',
            weeks: false,
            type: 'руб',
            blue: false,
          },
          {
            id: 'netProfitPerUser',
            title: 'Чистая прибыль в средн. на одного участника за курс',
            weeks: false,
            type: 'руб',
            blue: false,
          },
          {
            id: 'earnedPerWeek',
            title: 'Заработанная сумма за неделю',
            weeks: true,
            type: '',
            blue: false,
          },
          {
            id: 'earnedPerWeekPerUser',
            title: 'Заработанная сумма в сред. на участника за неделю',
            weeks: true,
            type: '',
            blue: false,
          },
          {
            id: 'increaseIncomePerUserPerWeek',
            title: 'Прирост заработка на одного участника за неделю',
            weeks: true,
            type: '',
            blue: false,
          },
          {
            id: 'fullAmountEarnedPerCourse',
            title: 'Суммарный приход денежных средств за курс',
            weeks: false,
            type: '',
            blue: false,
          },
          {
            id: 'fullIncomePerUserPerCourse',
            title: 'Суммарный приход ден. средств на участника за курс',
            weeks: false,
            type: '',
            blue: false,
          },
          {
            id: 'averageIncreasePerUserPerCourse',
            title: 'Средний прирост прибыли на одного участника',
            weeks: false,
            type: 'за курс',
            blue: false,
          },
          {
            id: 'escapedUsersFromSpeed',
            title: 'Участники, оставившие обучение на Скорости',
            weeks: true,
            type: '%',
            blue: false,
          },
          {
            id: 'weeklyDataAccuracy',
            title: 'Точность данных по неделям',
            weeks: true,
            type: '',
            blue: true,
          },
          {
            id: 'courseDataAccuracy',
            title: 'Точность данных за курс',
            weeks: false,
            type: '%',
            blue: true,
          },
          {
            id: 'countUsersWithZero',
            title: 'Кол-во участников с 0 на неделе',
            weeks: true,
            type: '',
            blue: true,
          },
        ],
        analytics:{},
        faculty: 0,
        pointA: 0,
        revenue: 0,
        csv:'',
      },
      meta:{
        isLoadingCSV: false,
        isLoadedCSV: false,
        isLoading: false,
    },
  },
  actions:{
    ...initialActions,
    async loadAnalytics( {commit, dispatch, state })
    {
      await tryCatch( async () => 
        {
          commit('setMeta', { path: 'isLoading', value: true });
          commit('setData', { path:'csv', value: '' });
          commit('setData', { path:'analytics', value: {} });
          dispatch(
            'ajax/axiosSend', 
            {
              type: 'get',
              query: '/analytics/speed/get' + createQueryString(),
              cancel: {name: 'cancelAnalSpeed'},
            },
            { 
              root: true
            }
          )
          .then( res => 
            {
              if( res.data.status !== 'success')
                throw Error()
                
              commit('setData', { path:'analytics', value: res.data.data.rows || {} });
              commit('setData', { path:'faculty', value: res.data.data.faculty || 0});
              commit('setData', { path:'pointA', value: res.data.data.pointA || 0});
              commit('setData', { path:'revenue', value: res.data.data.revenue || 0});
            }
          ).catch()

          commit('setMeta', { path: 'isLoading', value: false })
          commit('setMeta', { path: 'isLoadedCSV', value: false })
        }
      )
    },
    async loadCSV( {commit, dispatch, state })
    {
      commit('setMeta', { path: 'isLoadingCSV', value: true })
      dispatch(
        'ajax/axiosSend', 
        {
        type: 'get',
        query: '' + createQueryString(),
        cancel: {name: 'cancelCSVAnalSpeed'},
        },
        { root: true }
      )
      .then( res => 
        {
          commit('setMeta', { path: 'isLoadedCSV', value: true })
        }
      )
      commit('setMeta', { path: 'isLoadingCSV', value: false })
    }
  },
  mutations:{
    ...initialMutations,
  }
}
import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import createQueryString from "@lms/functions/createQueryString"
import tryCatch from '@lms/functions/tryCatch';

export default 
{
  namespaced: true,
  state:
  {
    data:
    {
      users: [
      ],
      csv: '',
      page: 1,
      oldQuery:'',
      streams: []
    },
    meta:
    {
      isLoading: false,
      isLoadingPage: false,
      isLoadingCSV: false,
      noMorePage: false,
    }
  },
  actions:
  {
    ...initialActions,
    async getUsersList( { commit, dispatch, state } )
    {
      await tryCatch( async () => 
      {
        let queryString = createQueryString();
        let isOld = queryString !== state.data.oldQuery
        if( isOld ){
          commit('setMeta', {path: 'isLoading', value: true })
          commit('setData', { path:'page', value: 1 }) 
          commit('setMeta', {path: 'noMorePage', value: false })
        }
        else
          commit('setMeta', {path: 'isLoadingPage', value: true })


        let objectPage = {
          pageSize: 25,
          page: state.data.page
        }

        let res = await dispatch('ajax/axiosSend', 
        {
          type:'get',
          query: `/analytics/msa/get${ queryString }`,
          params: objectPage,
          cancel: { name: 'msaGetUsersList'}
        },
        { root: true });
  
        if( !res && res.data.status !== 'success')
          throw new Error;
        
        const dataAnal = res.data.data.analytics;
        let dataUsers = [];
        if(res.data.data.users) {
          dataUsers = res.data.data.users.map( el => {
            const dataAnalUser = dataAnal.find( elem => elem.userId === el.id)
            return {...el, ...dataAnalUser}
          })
        } else {
          commit('setMeta', {path: 'noMorePage', value: true })
        }
        
        commit('setData', {  path: 'users', value: !isOld ? [...state.data.users, ...dataUsers] : dataUsers})
        commit('setData', { path: 'oldQuery', value: queryString})
        commit('setMeta', {path: 'isLoading', value: false })
        commit('setMeta', {path: 'isLoadingPage', value: false })
      })
    },
    clearUsersList({commit}) {
      commit('setData', { path:'page', value: 1 })
      commit('setData', {path: 'users', value: []})
      commit('setData', {path: 'oldQuery', value: ''})
      commit('setMeta', {path: 'noMorePage', value: false })
      commit('setMeta', {path: 'isLoading', value: false })
      commit('setMeta', {path: 'isLoadingPage', value: false })
    },
    async loadingCSV( {commit, dispatch })
    {
      await tryCatch( async () => 
      {
        let res = await dispatch('ajax/axiosSend',
        {
          type: 'get',
          query:''+ createQueryString(),
          cancel:'msaLoadingCSV'
        },
        { root: true })
        
        if( !res && res.data.status !== 'success')
          throw Error
        
        commit('setData', { path: 'csv', value: res.data.data })
        commit('setMeta', { path: 'isLoadingCSV', value: true })
      })
    },
    async loadStreams( {commit, dispatch} )
    {
      await tryCatch( async () => 
      {
        let res = await dispatch('ajax/axiosSend',
        {
          type: 'get',
          query: '/analytics/msa/streams/get',
        },
        { root: true })
        if( !res && res.data.status !== 'success')
          throw new Error;

        commit('setData', { path: 'streams', value: res.data.data.map( el => ({ value: el.id, text: el.name }))})
      })
    }
  },
  mutations: { ...initialMutations }
}
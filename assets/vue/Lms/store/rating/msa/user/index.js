import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import createQueryString from "@lms/functions/createQueryString"
import tryCatch from '@lms/functions/tryCatch';

export default 
{
  namespaced: true,
  state:
  {
    data:
    {
      user:{
          achievement: 0,
          avatar: ''
      },
      info: {},
      months: [],
      years: [],
      actualMonthId: undefined
    },
    meta:
    {
      isLoading: false,
      isLoadingUser: false,
    }
  },
  actions:
  {
    ...initialActions,
    async getUser({ commit, dispatch }, { id, year })
    {
      commit('setMeta', {path:'isLoadingUser', value: true})
      await tryCatch( async () => 
      {
        const res = await dispatch('ajax/axiosSend', 
        {
          type: 'get',
          query: `/analytics/msa/${id}/get`,
          params: {
            year,
          },
          cancel: { name: 'msaGetUser' },
        },
        { root: true });

        if( !res && res.data.status !== 'succes')
          throw new Error
      
        commit('setData', { path: 'user', value: res.data.data.user[0] ?? res.data.data.user});
        commit('setData', { path: 'months', value: res.data.data.months});
        commit('setData', { path: 'actualMonthId', value: res.data.data.actualPeriod});
      })
      commit('setMeta', {path:'isLoadingUser', value: false})
    },
    async getYears({commit, dispatch}, {id}) {
      let res = {}
      await tryCatch(async()=> {
        res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/analytics/msa/${id}/years/get`,
          params: {},
        },
        { root: true });
      })

      if( !res && res.data.status !== 'succes')
        throw new Error
      return res.data.data.years

    },
    async userEdit( { commit, dispatch, state }, data)
    {
      let status
      await tryCatch( async () => 
      {
        let res = await dispatch('ajax/axiosSend',
        {
          type: 'post',
          query: '/analytics/msa/user/edit',
          cancel: 'cancelUserEdit',
          sendData: data
        },
        { root: true })

        let objMessage = {
          message: res.data.status === 'success' ? 'Ваши данные успешно сохранены' : 'Ошибка сервера',
          type: res.data.status,
          error: !res && res.data.status !== 'success',
        }
        dispatch('ajax/addMessage', objMessage, { root: true});

        if(res && res.data.data) {
          res.data.data.forEach(cell => {
            commit('cellEdit', cell)
          })
        }
        status = res.data.status
      })
      return status
    },
    async cellEdit({commit, dispatch}, data) {
      let cell = data[0]
      commit('cellEdit', {id: cell.cellId, profit: cell.profit})

      let res = await dispatch('ajax/axiosSend', {
        type: 'post',
        query: '/analytics/msa/user/cell/edit',
        sendData: data
      }, { root: true })
    }
  },
  mutations: { 
    ...initialMutations,
    cellEdit(state, {id, profit}) {
      let idx = state.data.months.findIndex(({cellId}) => cellId === id)
      if (idx === -1) return
      state.data.months[idx].profit = profit
    }
  }
}

export const miActions = {
  async getNiches({commit, dispatch}, { userId }) {
    dispatch('setMeta', { path: 'isLoadingNiches', value: true });
    const res = await dispatch('ajax/axiosSend', {
      type: 'get',
      query: `/api/v2/user/${userId}/business/niche`
    }, { root: true });
    commit('setNiches', res.data);
    dispatch('setMeta', { path: 'isLoadingNiches', value: false })
  },
  async removeNiche({commit, dispatch}, nicheId) {
    const res = await dispatch('ajax/axiosSend', {
      type: 'delete',
      query: `/api/v2/business/niche/${nicheId}`
    }, { root: true });
    if(res.status === 200) {
      dispatch('ajax/addMessage', { message: 'Ниша удалена', type: 'success' }, { root: true })
      commit('removeNiche', nicheId)
    } else {
      dispatch('ajax/addMessage', { message: 'При удалении ниши произошла ошибка', type: 'error' }, { root: true })
    }
  },
  async addNiche({commit, dispatch}, { niche }) {
    const res = await dispatch('ajax/axiosSend', {
      type: 'post',
      query: '/api/v2/business/niche',
      sendData: {
        name: niche.name,
        description: niche.description,
        hasRivals: niche.hasRivals,
        verified: false
      }
    }, { root: true })
    if (res.data.id) {
      dispatch('ajax/addMessage', { message: 'Данные обновлены', type: 'success' }, { root: true })
      niche.id = res.data.id
      commit('addNiche', niche)
    } else {
      dispatch('ajax/addMessage', { message: 'При обновлении данных произошла ошибка', type: 'error' }, { root: true })
    }
  },
  async editNiche({commit, dispatch}, payload) {
    const res = await dispatch('ajax/axiosSend', {
      type: 'patch',
      query: `/api/v2/business/niche/${payload.niche.id}`,
      sendData: {
        name: payload.niche.name,
        description: payload.niche.description,
        hasRivals: payload.niche.hasRivals
      }
    }, { root: true })
    if (res.data.id) {
      dispatch('ajax/addMessage', { message: 'Данные обновлены', type: 'success' }, { root: true })
      commit('editNiche', res.data)
    } else {
      dispatch('ajax/addMessage', { message: 'При обновлении данных произошла ошибка', type: 'error' }, { root: true })
    }
  },
  getNicheComments({state, commit}, nicheId) {
    commit('setData', { path: 'nicheComments', value: state.data.nichesList.find(n => n.id === nicheId).comments })
  },
  clearNicheComments({commit}) {
    commit('setData', { path: 'nicheComments', value: [] })
  },
};

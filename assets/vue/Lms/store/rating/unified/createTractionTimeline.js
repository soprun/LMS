import moment from 'moment';
import _ from 'lodash';

const beforeCourse = {
  id: -1,
  abstractCourse: {
    name: 'До курса',
    slug: 'before_course'
  },
  name: 'До курса',
};

export function getMonthRange(date) {
  const monthStart = moment(date).startOf('month');
  const monthStartThu = moment(date).startOf('month').isoWeekday(4);
  const monthStartCorrected = monthStart.isSameOrBefore(monthStartThu)
    ? moment(monthStart).startOf('week')
    : moment(monthStart).startOf('week').add(1, 'weeks');

  const monthEnd = moment(date).endOf('month');
  const monthEndThu = moment(date).endOf('month').isoWeekday(4);
  const monthEndCorrected = monthEnd.isBefore(monthEndThu)
    ? moment(monthEnd).endOf('week').subtract(1, 'week')
    : moment(monthEnd).endOf('week');

  return {
    monthStartCorrected,
    monthEndCorrected
  }
}

function findEarliestDate(coursesList) {
  const firstCourse = coursesList[0];
  const startDate = moment(firstCourse.courseStartDate);
  const connectedDate = moment(firstCourse.connectedDate);

  return startDate.isBefore(connectedDate, 'weeks') ? startDate : connectedDate;
}

function getCourseEndDate(course, courseStart, courses) {
  let endDate;

  if (['speed', 'speed_club', 'hundred', 'skills'].includes(course.abstractCourse.slug)) {
    endDate = moment(courseStart).add(course.period - 1, 'weeks').endOf('week');
  } else if (course.abstractCourse.slug === 'msa') {
    endDate = moment(courseStart).add(course.period ?? 0 - 1, 'weeks').endOf('week').add(1, 'years');
  } else if (course.abstractCourse.slug === 'mi') {
    const msaCourse = courses.find(c => c.abstractCourse.slug === 'msa')
    endDate = msaCourse
      ? moment(msaCourse.startDate).subtract(1, 'weeks').startOf('week')
      : moment(courseStart).add(course.period - 1, 'weeks').endOf('week').add(1, 'years');
  } else if (course.abstractCourse.slug === 'fastcash') {
    endDate = moment(course.startDate).add(course.period, 'weeks').subtract(1, 'day').endOf('day')
  }

  return endDate;
}

function addBand(coursesList, metricsList, bandStart) {
  const monthRange = getMonthRange(bandStart);
  const monthStart = monthRange.monthStartCorrected;
  const monthEnd = monthRange.monthEndCorrected;
  let filteredCourses = coursesList.filter(c => {
    return (monthStart.isSameOrAfter(c.courseStartDate) || monthEnd.isSameOrAfter(c.courseStartDate))
      && (monthStart.isSameOrBefore(c.courseEndDate) || monthEnd.isSameOrBefore(c.courseEndDate))
  });

  const bandType = 'month';
  return {
    type: bandType,
    startDate: moment(bandStart).startOf(bandType),
    endDate: moment(bandStart).endOf(bandType),
    metrics: Object.fromEntries(
      metricsList.map(m => [m.id, undefined])
    ),
    courses: filteredCourses,
    disabled: moment().isBefore(bandStart, bandType)
  };
}

function flattenTimeline(bands) {
  const flattenedTimeline = [];
  bands.forEach(b => {
    if (b.hasWeeks) {
      b.children.forEach(w => {
        if (w.hasDays) {
          w.children.forEach(d => {
            flattenedTimeline.push(d);
          })
        } else {
          flattenedTimeline.push(w);
        }
      });
    } else {
      flattenedTimeline.push(b);
    }
  });
  return flattenedTimeline;
}

function splitWeeks(bands, metricsList) {
  bands.forEach(b => {
    if (_.some(b.courses, c => c.abstractCourse.slug === 'fastcash')) {
      const weekStart = moment(b.startDate).startOf('week');
      const weekEnd = moment(b.endDate).endOf('week');
      let currentDate = moment(weekStart).add(1, 'second');
      if (
        _.some(b.courses, c => ['speed', 'speed_club', 'hundred'].includes(c.abstractCourse.slug))
      ) {
        return;
      }
      b.hasDays = true;
      b.children = [];
      while (currentDate.isBefore(weekEnd)) {
        const filteredCourses = b.courses.filter(c => currentDate.isBetween(c.courseStartDate, c.courseEndDate))
        b.children.push({
          type: 'day',
          startDate: moment(currentDate).startOf('day'),
          endDate: moment(currentDate).endOf('day'),
          metrics: Object.fromEntries(
            metricsList.map(m => [m.id, undefined])
          ),
          courses: filteredCourses,
          disabled: moment().isBefore(currentDate, 'day')
        });
        currentDate.add(1, 'day');
      }
    }
  })
}

function splitMonths(bands, metricsList) {
  bands.forEach(b => {
    if (_.some(b.courses, c => ['speed', 'speed_club', 'hundred', 'fastcash', 'mi', 'msa', 'skills'].includes(c.abstractCourse.slug))) {
      const monthRange = getMonthRange(b.startDate);
      const monthStart = monthRange.monthStartCorrected;
      const monthEnd = monthRange.monthEndCorrected;
      let currentDate = moment(monthStart).isoWeekday(4);
      if (
        _.every(b.courses, c => ['mi', 'msa', 'skills'].includes(c.abstractCourse.slug)) &&
        b.courses.filter(c =>
          ['mi', 'msa', 'skills'].includes(c.abstractCourse.slug) &&
          !((monthStart.isSameOrAfter(c.courseStartDate) || monthEnd.isSameOrAfter(c.courseStartDate))
          && (monthStart.isSameOrBefore(c.courseIntensiveEndDate) || monthEnd.isSameOrBefore(c.courseIntensiveEndDate)))
        ).length
      ) {
        return;
      }
      b.hasWeeks = true;
      b.children = [];
      while (currentDate.isSameOrBefore(monthEnd, 'days')) {
        const filteredCourses = b.courses.filter(c =>
          currentDate.isSameOrAfter(c.courseStartDate, 'week') && currentDate.isSameOrBefore(c.courseEndDate, 'week'))
        b.children.push({
          type: 'week',
          startDate: moment(currentDate).startOf('week'),
          endDate: moment(currentDate).endOf('week'),
          metrics: Object.fromEntries(
            metricsList.map(m => [m.id, undefined])
          ),
          courses: filteredCourses,
          disabled: moment().isBefore(currentDate, 'weeks')
        });
        currentDate.add(1, 'week');
      }
      splitWeeks(b.children, metricsList);
    }
  })
  return flattenTimeline(bands);
}

function constructTractionBands(coursesList, metricsList) {
  let tractionBands = [];
  const earliestDate = coursesList.length ? findEarliestDate(coursesList) : moment().subtract(2, 'months').startOf('month');
  const latestDate = coursesList.length && moment(_.last(coursesList).courseEndDate).isAfter(moment())
    ? moment(_.last(coursesList).courseEndDate).endOf('month')
    : moment().add(2, 'month').endOf('week');
  if (coursesList.length && earliestDate.isBefore(coursesList[0].courseStartDate)) {
    beforeCourse.courseStartDate = moment(earliestDate);
    beforeCourse.courseEndDate = moment(coursesList[0].courseStartDate).subtract(1, 'day');
    beforeCourse.courseIntensiveEndDate = moment(coursesList[0].courseStartDate).subtract(1, 'day');
    coursesList.push(beforeCourse);
  }

  let bandStart = moment(earliestDate).startOf('week').add(1, 'second');
  while(bandStart.isBefore(latestDate)) {
    tractionBands = [...tractionBands, addBand(coursesList, metricsList, bandStart)];
    bandStart.add(1, 'months');
  }

  return splitMonths(tractionBands, metricsList)
    .filter(b => b.startDate.isBefore(moment().add(2, 'month')));
}

function mapTractionData(timeline, tractionData) {
  timeline.forEach(b => {
    tractionData.forEach(d => {
      // Совместимость со старым трекшеном
      if (moment(d.date).isAfter('2021-09-26','days')) {
        if (moment(d.date).isSame(b.startDate, 'days')) {
          b.metrics[d.type] = d.value;
        }
      } else {
        if (moment(d.date).isSame(b.startDate, 'weeks')) {
          b.metrics[d.type] = d.value;
        }
      }
    });
  })
}

export function findCurrentBand(bands) {
  return bands.findIndex(b => b.startDate.isSame(moment(), b.type + 's'));
}

export function findCurrentPeriod(bands) {
  return bands.findIndex(b => moment().isBetween(b.from, b.to, 'days', '[]'));
}

export function filterMetrics(metricsList, courses) {
  let filteredMetrics = metricsList
    .filter(m => _.some(courses, c => {
      for (const prop in c.abstractCourse.tractionValueTypes) {
        if (c.abstractCourse.tractionValueTypes[prop].id === m.id) {
          return true;
        }
      }
      return false;
    }))
    .sort((a, b) => a.id < b.id ? -1 : 1);
  if (!filteredMetrics.length || !(filteredMetrics.find(m => m.id === 1 || m.id === 2))) {
    filteredMetrics = [...metricsList.filter(m => m.id === 1 || m.id === 2), ...filteredMetrics];
  }

  return filteredMetrics;
}

export function processCoursesList(coursesList) {
  return coursesList
    .filter(c => c.startDate)
    .map((c, _, arr) => {
      const courseStartDate = c.abstractCourse.slug === 'fastcash'
        ? moment(c.startDate).startOf('day')
        : moment(c.startDate).startOf('week');
      const courseIntensiveEndDate = c.abstractCourse.slug === 'fastcash'
        ? moment(courseStartDate).add(c.period, 'weeks').endOf('day')
        : c.abstractCourse.slug === 'msa'
          ? moment(c.startDate).startOf('week').subtract(1, 'second')
          : moment(courseStartDate).add(c.period - 1, 'weeks').endOf('week');
      const courseEndDate = getCourseEndDate(c, courseStartDate, arr);
      return {
        ...c,
        connectedDate: moment(c.connectedDate),
        courseStartDate,
        courseIntensiveEndDate,
        courseEndDate,
      }
    })
    .sort((a, b) => {
      let result;

      if (a.courseStartDate.isBefore(b.courseStartDate)) {
        result = -1;
      } else {
        result = 1;
      }

      return result;
    });
}

export function mapTraction(tractionData, metrics) {
  const bands = tractionData.map(t => ({
    ...t,
    startDate: moment(t.from),
    endDate: moment(t.to),
    metrics: Object.fromEntries(
      metrics.map(m => [m.id, t.values.find(v => v.type === m.id)?.value])
    ),
    disabled: moment().isBefore(t.from, 'weeks')
  }))
  return {
    bands,
    current: findCurrentPeriod(bands)
  }
}

export function createTractionTimeline(coursesList, metricsList, tractionData) {
  const bands = constructTractionBands(coursesList, metricsList);
  mapTractionData(bands, tractionData);

  return {
    current: findCurrentBand(bands),
    bands: bands
  };
}

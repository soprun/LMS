export default {
  userInfo: {
    id: 101,
    avatar: 'https://ichef.bbci.co.uk/news/976/cpsprodpb/12A9B/production/_111434467_gettyimages-1143489763.jpg',
    name: 'Сергей',
    lastname: 'Нагиев',
    patronymic: 'Васильевич',
    email: 'nagiev345@gmail.com',
    telegram: 'nagiev345',
    phone: '+7 (999) 787 76 77',
    niche: 'Интернет магазин',
    businessCategoryId: 0,
    businessCategory: 'Ремонт, строительство',
    faculty: 'Доставка еды',
    city: 'Москва',
  },
  availableBusiness: [
    'Ремонт, строительство',
    'Красота и здоровье',
    'Информационные технологии'
  ],
  availableStreams: [
    {
      id: 35,
      title: 'Сотка 101'
    },
    {
      id: 46,
      title: 'Скорость 201'
    }
  ],
  availableYears: [ 2020, 2021 ],
  ratingData: {
    currentWeek: 9,
    weeks: [
      {
        weekStart: new Date('2021-06-21'),
        weekEnd: new Date('2021-06-27'),
        types: [{ slug: 'beforeCourse', name: 'До курса' }],
        earnings: 200000,
        profit: 100000
      },
      {
        weekStart: new Date('2021-06-28'),
        weekEnd: new Date('2021-07-04'),
        types: [{ slug: 'beforeCourse', name: 'До курса' }],
        earnings: 300000,
        profit: 200000
      },
      {
        weekStart: new Date('2021-07-05'),
        weekEnd: new Date('2021-07-11'),
        types: [{ slug: 'beforeCourse', name: 'До курса' }],
        earnings: 200000,
        profit: 100000
      },
      {
        weekStart: new Date('2021-07-12'),
        weekEnd: new Date('2021-07-18'),
        types: [{ slug: 'beforeCourse', name: 'До курса' }],
        earnings: 200000,
        profit: 100000
      },
      {
        weekStart: new Date('2021-07-19'),
        weekEnd: new Date('2021-07-25'),
        types: [{ slug: 'beforeCourse', name: 'До курса' }],
        earnings: 300000,
        profit: 10000
      },
      {
        weekStart: new Date('2021-07-26'),
        weekEnd: new Date('2021-08-01'),
        types: [{ slug: 'speed', name: 'Скорость 3' }],
        earnings: 500000,
        profit: 200000
      },
      {
        weekStart: new Date('2021-08-02'),
        weekEnd: new Date('2021-08-08'),
        types: [{ slug: 'speed', name: 'Скорость 3' }],
        earnings: undefined,
        profit: undefined
      },
      {
        weekStart: new Date('2021-08-09'),
        weekEnd: new Date('2021-08-15'),
        types: [{ slug: 'speed', name: 'Скорость 3' }],
        earnings: 200000,
        profit: 100000
      },
      {
        weekStart: new Date('2021-08-16'),
        weekEnd: new Date('2021-08-22'),
        types: [{ slug: 'speed', name: 'Скорость 3' }],
        earnings: 200000,
        profit: 100000
      },
      {
        weekStart: new Date('2021-08-23'),
        weekEnd: new Date('2021-08-29'),
        types: [{ slug: 'speed', name: 'Скорость 3' }],
        earnings: 200000,
        profit: 100000
      },
      {
        weekStart: new Date('2021-08-30'),
        weekEnd: new Date('2021-09-05'),
        types: [{ slug: 'speed', name: 'Скорость 3' }],
        earnings: 200000,
        profit: 100000
      },
      {
        weekStart: new Date('2021-09-06'),
        weekEnd: new Date('2021-09-12'),
        types: [{ slug: 'skills', name: 'Навыки' }, { slug: 'hundred', name: 'Сотка 4' }],
        earnings: 200000,
        profit: 100000
      },
      {
        weekStart: new Date('2021-09-13'),
        weekEnd: new Date('2021-09-19'),
        types: [{ slug: 'skills', name: 'Навыки' }, { slug: 'hundred', name: 'Сотка 4' }],
        earnings: 200000,
        profit: 100000
      },
    ]
  },
  coursesList: [
    {
      "id": 2,
      "abstractCourse": {
        "name": "Скорость",
        "slug": "speed",
        "tractionValueTypes": [
          {
            "id": 1,
            "title": "Прибыль"
          },
          {
            "id": 2,
            "title": "Выручка"
          }
        ]
      },
      "stream": 11,
      "name": "Скорость 11",
      "startDate": null,
      "period": null,
      "connectedDate": "2020-09-04T18:36:47+00:00"
    },
    {
      "id": 4,
      "abstractCourse": {
        "name": "Скорость",
        "slug": "speed",
        "tractionValueTypes": [
          {
            "id": 1,
            "title": "Прибыль"
          },
          {
            "id": 2,
            "title": "Выручка"
          }
        ]
      },
      "stream": 13,
      "name": "Скорость 13",
      "startDate": null,
      "period": null,
      "connectedDate": "2020-10-02T09:22:12+00:00"
    },
    {
      "id": 30,
      "abstractCourse": {
        "name": "Скорость",
        "slug": "speed",
        "tractionValueTypes": [
          {
            "id": 1,
            "title": "Прибыль"
          },
          {
            "id": 2,
            "title": "Выручка"
          }
        ]
      },
      "stream": 20,
      "name": "Скорость Клуб 2",
      "startDate": "2021-08-07T00:00:00+00:00",
      "period": 6,
      "connectedDate": "2020-10-06T14:48:06+00:00"
    },
    {
      "id": 14,
      "abstractCourse": {
        "name": "Скорость",
        "slug": "speed",
        "tractionValueTypes": [
          {
            "id": 1,
            "title": "Прибыль"
          },
          {
            "id": 2,
            "title": "Выручка"
          }
        ]
      },
      "stream": 14,
      "name": "Скорость 14",
      "startDate": null,
      "period": null,
      "connectedDate": "2020-11-09T11:31:44+00:00"
    },
    {
      "id": 15,
      "abstractCourse": {
        "name": "Скорость",
        "slug": "speed",
        "tractionValueTypes": [
          {
            "id": 1,
            "title": "Прибыль"
          },
          {
            "id": 2,
            "title": "Выручка"
          }
        ]
      },
      "stream": 15,
      "name": "Скорость 15",
      "startDate": null,
      "period": null,
      "connectedDate": "2020-12-26T14:40:03+00:00"
    },
    {
      "id": 16,
      "abstractCourse": {
        "name": "Скорость",
        "slug": "speed",
        "tractionValueTypes": [
          {
            "id": 1,
            "title": "Прибыль"
          },
          {
            "id": 2,
            "title": "Выручка"
          }
        ]
      },
      "stream": 16,
      "name": "Скорость 16",
      "startDate": null,
      "period": null,
      "connectedDate": "2021-02-15T13:18:08+00:00"
    },
    {
      "id": 18,
      "abstractCourse": {
        "name": "Скорость",
        "slug": "speed",
        "tractionValueTypes": [
          {
            "id": 1,
            "title": "Прибыль"
          },
          {
            "id": 2,
            "title": "Выручка"
          }
        ]
      },
      "stream": 18,
      "name": "Скорость 18",
      "startDate": "2021-05-21T00:00:00+00:00",
      "period": 6,
      "connectedDate": "2021-05-07T15:51:42+00:00"
    },
    {
      "id": 20,
      "abstractCourse": {
        "name": "Сотка",
        "slug": "hundred",
        "tractionValueTypes": [
          {
            "id": 1,
            "title": "Прибыль"
          },
          {
            "id": 2,
            "title": "Выручка"
          }
        ]
      },
      "stream": 1,
      "name": "Сотка 1",
      "startDate": null,
      "period": null,
      "connectedDate": "2021-02-12T20:34:10+00:00"
    },
    {
      "id": 21,
      "abstractCourse": {
        "name": "Сотка",
        "slug": "hundred",
        "tractionValueTypes": [
          {
            "id": 1,
            "title": "Прибыль"
          },
          {
            "id": 2,
            "title": "Выручка"
          }
        ]
      },
      "stream": 2,
      "name": "Сотка 2",
      "startDate": "2021-04-09T00:00:00+00:00",
      "period": 6,
      "connectedDate": "2021-02-12T20:34:10+00:00"
    },
    {
      "id": 23,
      "abstractCourse": {
        "name": "Сотка",
        "slug": "hundred",
        "tractionValueTypes": [
          {
            "id": 1,
            "title": "Прибыль"
          },
          {
            "id": 2,
            "title": "Выручка"
          }
        ]
      },
      "stream": 4,
      "name": "Сотка 4",
      "startDate": "2021-06-26T00:00:00+00:00",
      "period": 6,
      "connectedDate": "2021-06-19T20:36:30+00:00"
    },
    {
      "id": 36,
      "abstractCourse": {
        "name": "МсА",
        "slug": "msa",
        "tractionValueTypes": []
      },
      "stream": 1,
      "name": "МСА 1",
      "startDate": null,
      "period": null,
      "connectedDate": "2020-12-01T11:15:23+00:00"
    }
  ],
  tractionData: [
    {
      "date": "2021-08-01T00:00:00+00:00",
      "value": 15000,
      "owner": 3474,
      "type": 1
    },
    {
      "date": "2021-08-01T00:00:00+00:00",
      "value": 15000,
      "owner": 3474,
      "type": 2
    },
    {
      "date": "2021-08-08T00:00:00+00:00",
      "value": 4885,
      "owner": 3474,
      "type": 1
    },
    {
      "date": "2021-08-08T00:00:00+00:00",
      "value": 4885,
      "owner": 3474,
      "type": 2
    }
  ],
  tractionMetrics: [
    {
      "id": 1,
      "title": "Прибыль"
    },
    {
      "id": 2,
      "title": "Выручка"
    }
  ]
}

import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import createQueryString from "@lms/functions/createQueryString"
import tryCatch from '@lms/functions/tryCatch';

const ENDPOINT = '/api/traction'

export default
{
  namespaced: true,
  state: {
    data: {
      page: 1,
      folders: [],
      users: [],
      oldQuery: null,
    },
    meta: {
      isLoadingFoulders: false,
      isLoading: false,
      isLoadingPage: false,
      noMorePage: false,
    }
  },
  actions: {
    ...initialActions,
    async getFoulders({ commit, dispatch, state }) {
      commit('setMeta', { path: 'isLoadingFoulders', value: true })
      await tryCatch( async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `${ENDPOINT}/filters`,
        }, { root: true })

        if (!res && res.data.status !== 'success')
          throw Error

        commit('setData', { path: 'folders', value: res.data.data.streams })
      })
      commit('setMeta', { path: 'isLoadingFoulders', value: false })
    },
    async getUserList({ commit, dispatch, state }) {
      await tryCatch( async () => {
        const queryString = createQueryString();
        const isOld = queryString !== state.data.oldQuery
        if (isOld){
          commit('setMeta', { path: 'isLoading', value: true })
          commit('setData', { path:'page', value: 1 })
          commit('setMeta', { path: 'noMorePage', value: false })
        }
        else
          commit('setMeta', { path: 'isLoadingPage', value: true })

        const objectPage = {
          pageSize: 30,
          page: state.data.page,
          courses: ['speed', 'speed_club', 'hundred', 'msa', 'mi', 'fastcash', 'skills']
        }

        let res = await dispatch('ajax/axiosSend', {
          type:'get',
          query: `${ENDPOINT}/users${ queryString }`,
          params: objectPage,
          cancel: { name: 'hundredSpeedGetUsersList'}
        }, { root: true });

        if (!res) {
          commit('setData', { path: 'users', value: [] })
          return
        }

        if( !res && res.data.status !== 'success')
          throw new Error;

        let dataUsers = [];
        if (res.data.data) {
          dataUsers = res.data.data.list.map( el => {
            return {
              ...el,
              info: [{
                channel: 'Выручка',
                channelId: 'revenue',
                fullProfit: el.tractionSums['2'] ?? 0
              }, {
                channel: 'Прибыль',
                channelId: 'profit',
                fullProfit: el.tractionSums['1'] ?? 0
              }]
            }
          })
        } else {
          commit('setMeta', {path: 'noMorePage', value: true })
        }

        commit('setData', { path: 'users', value: !isOld ? [...state.data.users, ...dataUsers] : dataUsers })
        commit('setData', { path: 'oldQuery', value: queryString })
        commit('setMeta', { path: 'isLoading', value: false })
        commit('setMeta', { path: 'isLoadingPage', value: false })
      })
    },
    clearUserList({commit}) {
      commit('setMeta', { path: 'isLoading', value: false })
      commit('setData', { path: 'page', value: 1 })
      commit('setMeta', { path: 'noMorePage', value: false })
      commit('setMeta', { path: 'isLoadingPage', value: false })
      commit('setData', { path: 'users', value: [] })
      commit('setData', { path: 'oldQuery', value: null })
    }
  },
  getters: {
    courseList: state => {
      return state.data.folders.map(f => ({
        title: f.name,
        value: f.id,
        groups: f.userGroups.length > 1
          ? f.userGroups.map(g => ({ title: g.name, value: g.id }))
          : []
      }))
    }

  },
  mutations: { ...initialMutations }
}

import _ from 'lodash';
import moment from 'moment';
import initialActions from "@lms/store/initialActions";
import initialMutations from "@lms/store/initialMutations";

import {
  processCoursesList,
  filterMetrics,
  mapTraction
} from '@lms/store/rating/unified/createTractionTimeline';
import getCoursesRange from '@lms/store/rating/unified/getCoursesRange';
import { miActions } from '@lms/store/rating/unified/miActions';


export default {
  namespaced: true,
  state: {
    data: {
      availableBusiness: [],
      selectedStream: 0,
      userInfo: {
        id: 0,
        name: '',
        lastname: '',
        patronymic: '',
        email: '',
        telegram: '',
        niche: '',
        phone: '',
        businessCategoryName: '',
        businessCategoryId: 0,
        faculty: '',
        city: ''
      },
      tractionData: {},
      tractionStreams: [],
      tractionMetrics: [],
      nichesList: [],
      nicheComments: [],
      checkboxList: [],
    },
    meta: {
      isLoadingTractionData: false,
      isLoadingUserInfo: false,
      isLoadingUserBusiness: false,
      isLoadingCheckboxes: false,
      isLoadingNiches: false,
      isLoadingNicheComments: false,
      hasFastCash: false,
      hasInvestmentsMarathon: false,
      onboarding: false,
      onboardingStep: 0
    }
  },
  actions: {
    ...initialActions,
    ...miActions,
    async getUserInfo({commit, dispatch}, userId) {
      let result = {}
      dispatch('setMeta', { path: 'isLoadingUserInfo', value: true })
      dispatch('setMeta', { path: 'isLoadingRatingData', value: true })
      try {
        const response = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/api/traction/user/${userId}`
        }, {root: true})
        if (response.data.status === 'success' && response.data.data) {
          result = response.data.data
          commit('mapUserData', result)
        }
      } catch (e) {
        console.error(e)
      } finally {
        dispatch('setMeta', { path: 'isLoadingRatingData', value: false })
        dispatch('setMeta', { path: 'isLoadingUserInfo', value: false })
      }
      return result
    },
    async getAvailableBusiness({dispatch, rootState}) {
      let result = {}
      dispatch('setMeta', { path: 'isLoadingUserBusiness', value: true })
      try {
        if (rootState.auth.data.businessTypes.length === 0) {
          await dispatch('auth/getUserBusinessTypes', null, {root: true})
        }
        result = rootState.auth.data.businessTypes
      } catch (e) {
        console.error(e)
      } finally {
        dispatch('setMeta', { path: 'isLoadingUserBusiness', value: false })
        dispatch('setData', { path: 'availableBusiness', value: result })
      }
      return result
    },
    async getTractionData({dispatch, commit}, userId) {
      const coursesSlugs = ['speed', 'speed_club', 'hundred', 'msa', 'mi', 'mi_invensiv', 'fastcash', 'skills'];
      let courses = [];
      let metrics = [];
      let values = [];

      dispatch('setMeta', { path: 'isLoadingTractionData', value: true });
      try {
        const coursesResponse = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/api/traction/course-stream/list/${userId}`,
          params: {
            courses: coursesSlugs
          }
        }, { root: true });
        const coursesRange = getCoursesRange(coursesResponse.data.data);
        const metricsResponse = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: '/api/traction/type/list',
          params: {
            courses: coursesSlugs
          }
        }, { root: true });
        const valuesResponse = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/api/user/${userId}/traction`,
          params: coursesRange
        }, { root: true });

        if (_.every([coursesResponse, metricsResponse], r => r.data.status === 'success') && valuesResponse.data) {
          courses = coursesResponse.data.data;
          metrics = metricsResponse.data.data;
          values = valuesResponse.data;
        }

        commit('setTractionData', { courses, metrics, values });

      } catch (e) {
        console.error(e);
      } finally {
        dispatch('setMeta', { path: 'isLoadingTractionData', value: false });
      }
    },
    async getCheckboxes({commit, dispatch}, { userId, streamId }) {
      let checkboxList = [];
      dispatch('setMeta', { path: 'isLoadingFastCash', value: true });
      try {
        const response = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/api/v2/traction/checkpoints/${userId}/${streamId}`
        }, {root: true})
        checkboxList = response.data.map(ch => ({
          id: ch.id,
          name: ch.name,
          priority: ch.priority,
          value: {
            id: ch.value?.id ?? undefined,
            value: ch.value?.value ?? false,
            filename: ch.value?.filename ?? undefined
          }
        }));
        commit('setCheckboxData', checkboxList);
        dispatch('setMeta', { path: 'isLoadingFastCash', value: false });
      } catch (e) {
        console.error(e);
      }
      finally {
        dispatch('setMeta', { path: 'isLoadingFastCash', value: false });
      }
    },
    async checkTask({dispatch}, data) {
      const formData = new FormData()
      if (data.file) {
        formData.append('file', data.file);
      }
      if (data.value.value) {
        formData.append('value', data.value.value);
      }
      dispatch('setMeta', { path: 'isLoadingCheckboxes', value: true });
      try {
        const response = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/api/v2/traction/checkpoints/${data.id}/value`,
          sendData: formData
        }, {root: true})
        dispatch('ajax/addMessage', { message: 'Задание успешно выполнено', type: 'success' }, { root: true })
        dispatch('setMeta', { path: 'isLoadingCheckboxes', value: false });
        return response
      } catch (e) {
        console.error(e)
        dispatch('ajax/addMessage', { message: e.message, type: 'error' }, { root: true })
        dispatch('setMeta', { path: 'isLoadingCheckboxes', value: false });
      }
    },
    async changeBusinessSphere({state, dispatch}, newBusiness) {
      try {
        const response = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `${process.env.AUTH_SERVER_URL}/auth/user/edit`,
          sendData: {
            user: {
              id: state.data.userInfo.id,
              businessCategoryId: newBusiness.id
            }
          }
        }, {root: true})
        if (response.data.status === 'success') {
          const updatedUser = {
            ...state.data.userInfo,
            businessCategoryId: newBusiness.id,
            businessCategoryName: newBusiness.text
          }
          dispatch('auth/setUserBusiness', {
            businessCategoryName: newBusiness.text,
            businessCategoryId: newBusiness.id
          }, { root: true })
          dispatch('setData', { path: 'userInfo', value: updatedUser })
          dispatch('ajax/addMessage', { message: 'Cфера обновлена', type: 'success' }, { root: true })
        }
      } catch (e) {
        console.error(e)
      }
    },
    async changeSelectedStream({dispatch}, newStream) {
      dispatch('setData', { path: 'selectedStream', value: newStream })
    },
    async changeRatingData({state, commit, dispatch}, { band, key, value }) {
      const oldData = _.cloneDeep(state.data.tractionData)
      try {
        const updatedData = _.cloneDeep(state.data.tractionData)
        updatedData.bands.forEach(b => {
          if (_.isEqual(band, b)) {
            b.metrics[key] = value
          }
        })
        commit('updateTractionData', updatedData)
        const response = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/api/traction/set/${key}/${moment(band.keyDate).format('YYYY-MM-DD')}`,
          sendData: {
            traction: {
              value: value
            }
          }
        }, {root: true})
        if (response.data.status !== 'success') {
          commit('updateTractionData', oldData)
          dispatch('ajax/addMessage', { message: 'При внесении данных произошла ошибка', type: 'success' }, { root: true })
        } else {
          dispatch('ajax/addMessage', { message: 'Данные внесены', type: 'success' }, { root: true })
        }
      } catch (e) {
        commit('updateTractionData', oldData)
        dispatch('ajax/addMessage', { message: 'При внесении данных произошла ошибка', type: 'success' }, { root: true })
        console.error(e)
      }
    },
    storeCleanup({commit}) {
      commit('clearStore')
    }
  },
  mutations: {
    ...initialMutations,
    updateTractionData(state, newData) {
      state.data.tractionData = newData;
    },
    mapUserData(state, data) {
      state.data.userInfo = {
        ...data,
        businessCategoryName: state.data.availableBusiness.find(b => b.id === data.businessCategoryId)?.text ?? '',
        faculty: data.faculties[0]?.name
      };
    },
    setTractionData(state, { courses, metrics, values }) {
      const processedCourses = processCoursesList(courses);
      const filteredMetrics = filterMetrics(metrics, processedCourses);
      state.data.tractionStreams = processedCourses;
      state.data.tractionMetrics = filteredMetrics;
      state.data.tractionData = mapTraction(values, filteredMetrics);
      state.meta.hasFastCash = !!state.data.tractionData.bands[state.data.tractionData.current]?.streams.find(c => c.slug === 'fastcash')
      state.meta.hasInvestmentsMarathon = !!state.data.tractionData.bands[state.data.tractionData.current]?.streams.find(c => c.slug === 'mi')
    },
    setCheckboxData(state, checkboxes) {
      state.data.checkboxList = checkboxes;
    },
    setNiches(state, niches) {
      state.data.nichesList = niches;
    },
    addNiche(state, niche) {
      state.data.nichesList = [...state.data.nichesList, niche];
    },
    editNiche(state, niche) {
      state.data.nichesList.splice(state.data.nichesList.findIndex(n => n.id === niche.id), 1, niche);
    },
    removeNiche(state, nicheId) {
      state.data.nichesList = state.data.nichesList.filter(n => n.id !== nicheId);
    },
    clearStore(state) {
      state.data = {
        availableBusiness: [],
        selectedStream: 0,
        userInfo: {
          id: 0,
          name: '',
          lastname: '',
          patronymic: '',
          email: '',
          telegram: '',
          niche: '',
          phone: '',
          businessCategoryName: '',
          businessCategoryId: 0,
          faculty: '',
          city: ''
        },
        tractionData: {},
        tractionStreams: [],
        tractionMetrics: [],
        nichesList: [],
        nicheComments: [],
        checkboxList: []
      };
      state.meta.hasFastCash = false;
      state.meta.hasInvestmentsMarathon = false;
    }
  },
}

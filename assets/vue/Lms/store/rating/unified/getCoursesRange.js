import moment from 'moment';

export default function getCoursesRange(courses) {
  const range = {
    from: moment().subtract(1, 'months').format('YYYY-MM'),
    to: moment().add(2, 'months').format('YYYY-MM')
  };
  const sortedCourses = courses
    .map(c => ({
      ...c,
      trueStartDate: moment(c.startDate).isBefore(c.connectedDate) ? c.startDate : c.connectedDate,
    }))
    .sort((a, b) => moment(a.trueStartDate).isBefore(b.trueStartDate) ? -1 : 1);

  if (sortedCourses.length) {
    range.from = moment(sortedCourses[0].trueStartDate).format('YYYY-MM')
  }

  return range;
}

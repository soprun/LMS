import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import createQueryString from "@lms/functions/createQueryString"
import tryCatch from '@lms/functions/tryCatch';

const ENDPOINT = '/rating/hundred'

export default
{
  namespaced: true,
  state: {
    data: {
      // Для команд
      settings: {
        folders: [],
        captains: [],
        cities: [],
        seniorCaptains: [],
        canEdit: false,
        faculties: []
      },
      groups: [],
      tractionSummary: {},
      filterList: {
        folders: undefined,
        week: undefined,
        captain: undefined,
        city: undefined,
        search: undefined,
        faculty: undefined
      },
      teamList: [],
    },
    meta: {
      // Для команд
      isSettingLoading: false,
      isGroupLoading: false

    }
  },
  actions: {
    ...initialActions,
    // Для команд

    // Запрос есть на teamGroup/getTeamsSettings
    // но не хочу завязывать работу на другом модуле
    async getSettings({commit, dispatch}){
      await tryCatch( async () => {
        commit('setMeta', {path: 'isSettingLoading', value: true})

        let res = await dispatch('ajax/axiosSend',{
          type: 'get',
          query: `/api/teams/settings`,
        },{ root:true });

        if (res && res.data.status === 'success')
          commit('setData', { path: 'settings', value: res.data.data })

        commit('setMeta', {path: 'isSettingLoading', value: false})
      })
    },

    async getFaculties({commit, dispatch}) {
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend',{
          type: 'get',
          query: `/api/faculty/available-faculties`,
        },{ root:true });

        if (res && res.data.status === 'success')
          commit('setData', { path: 'settings', value: res.data.data })
      })
    },

    async getTeamGroupData({state, commit, dispatch}){
      commit('setMeta', {path: 'isGroupLoading', value: true})
      await tryCatch( async () => {
        let res = await dispatch('ajax/axiosSend',{
          type: 'get',
          query: `/api/teams/list`,
          params: {
            ...state.data.filterList,
            folders: state.data.filterList.folders ? [state.data.filterList.folders] : undefined,
          },
          cancel: {name: 'getTeamGroupData'},
        },{ root:true });

        if (res && res.data.status === 'success'){
          commit('setData', { path: 'groups', value: res.data.data.folders })
          commit('setData', { path: 'tractionSummary', value: res.data.data.tractionSummary })
          commit('setMeta', {path: 'loadedGroup', value: true})
        }
      })
      commit('setMeta', {path: 'isGroupLoading', value: false})
    },
    async setFilter({commit, state},{path, value}) {
      if(path === 'week' && String(state.data.filterList.week) !== String(value))
        commit('clearTeamList')

      commit('setFilter', {path, value})
    },
    async initFilter({commit, state}, filters) {
      commit('initFilter', filters)
    },

    async getTeam({state, commit, dispatch}, teamId){
      let result = {}
      await tryCatch( async () => {
        let res = await dispatch('ajax/axiosSend',{
          type: 'get',
          query: `/api/teams/traction/${teamId}`,
          params: {
            week: state.data.filterList.week,
          }
        },{ root:true });

        if (res && res.data.status === 'success'){
          result.status = 'success'
          res.data.data.id = teamId
          commit('setTeamData', { path: 'teams', value: res.data.data })
        }
      })
      return result
    },
  },
  getters: {
    // Для команд

    teamList(state) {
      let teamList = []
      state.data.groups.forEach(group => teamList.push(...group.teams))
      return teamList
    }

  },
  mutations: {
    ...initialMutations,

    setFilter(state, {path, value}) {
      // Что бы лишний раз не триггерить watch на фильтрах
      if(String(state.data.filterList[path]) === String(value)) return
      state.data.filterList[path] = value
    },
    // Пушим список команд
    setTeamData(state, {value}) {
      // Если команда существует, обновляем её
      let idx = state.data.teamList.findIndex(team => team.id === value.id)
      if(idx === -1)
        state.data.teamList.push(value);
      else
        state.data.teamList.splice(idx, 1, value)
    },
    setFaculties(state, faculties) {
      state.data.settings.faculties = faculties
    },
    clearTeamList(state) {
      state.data.teamList = []
    },
    initFilter(state, filters) {
      state.data.filterList = {
        ...state.data.filterList,
        ...filters,
      }
    }
  }
}

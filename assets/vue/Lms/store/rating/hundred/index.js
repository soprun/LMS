import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import createQueryString from "@lms/functions/createQueryString"
import tryCatch from '@lms/functions/tryCatch';

const ENDPOINT = '/rating/hundred'

export default 
{
  namespaced: true,
  state: {
    data: {
      page: 1,
      folders: [],
      users: [],
      oldQuery: null,
    },
    meta: {
      isLoadingFoulders: false,
      isLoading: false,
      isLoadingPage: false,
      noMorePage: false,
    }
  },
  actions: {
    ...initialActions,
    async getFoulders({ commit, dispatch, state }) {
      commit('setMeta', { path: 'isLoadingFoulders', value: true })
      await tryCatch( async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `${ENDPOINT}/groups/get`,
        }, { root: true })

        if( !res && res.data.status !== 'success')
          throw Error

        commit('setData', { path: 'folders', value: res.data.data })
      })
      commit('setMeta', { path: 'isLoadingFoulders', value: false })
    },
    async getUserList({ commit, dispatch, state }) {
      await tryCatch( async () => {
        let queryString = createQueryString();
        let isOld = queryString !== state.data.oldQuery
        if( isOld ){
          commit('setMeta', {path: 'isLoading', value: true })
          commit('setData', { path:'page', value: 1 }) 
          commit('setMeta', {path: 'noMorePage', value: false })
        }
        else 
          commit('setMeta', {path: 'isLoadingPage', value: true })

        let objectPage = {
          pageSize: 25,
          page: state.data.page
        }

        let res = await dispatch('ajax/axiosSend', {
          type:'get',
          query: `/analytics/hundred/get${ queryString }`,
          params: objectPage,
          cancel: { name: 'hundredGetUsersList'}
        }, { root: true });

        if(!res) {
          commit('setData', {  path: 'users', value: []})
          return
        }
  
        if( !res && res.data.status !== 'success')
          throw new Error;
        
        let dataUsers = [];
        if(res.data.data.users) {
          const dataAnal = res.data.data.analytics;
          dataUsers = res.data.data.users.map( el => {
            const dataAnalUser = dataAnal.find( elem => elem.userId === el.id)
            return {...el, ...dataAnalUser}
          })
        } else {
          commit('setMeta', {path: 'noMorePage', value: true })
        }
       
        commit('setData', {  path: 'users', value: !isOld ? [...state.data.users, ...dataUsers] : dataUsers})
        commit('setData', { path: 'oldQuery', value: queryString})
        commit('setMeta', {path: 'isLoading', value: false })
        commit('setMeta', {path: 'isLoadingPage', value: false })
      })
    },
    clearUserList({commit}) {
      commit('setMeta', {path: 'isLoading', value: false })
      commit('setData', { path:'page', value: 1 }) 
      commit('setMeta', {path: 'noMorePage', value: false })
      commit('setMeta', {path: 'isLoadingPage', value: false })
      commit('setData', {  path: 'users', value: []})
      commit('setData', { path: 'oldQuery', value: null})
    }
  },
  getters: {
    courseList: state => {
      return state.data.folders.map(f => ({ title: f.name, value: f.id }))
    }

  },
  mutations: { ...initialMutations }
}

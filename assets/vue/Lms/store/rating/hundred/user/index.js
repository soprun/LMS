import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import tryCatch from '@lms/functions/tryCatch';

export default 
{
  namespaced: true,
  state: {
    data: {
      user:{
        achievement: 0,
        avatar: ''
      },
      info: {},
      faculty: '',
      userWeekList: [],
      weekList: [],
      userDayList: [],
      streamId: '',
      actualWeekId: 0,
      availableStreams: []
    },
    meta: {
      isLoading: false,
      isLoadingUser: false,
    }
  },
  actions:
  {
    ...initialActions,
    async getUser({ commit, dispatch }, { id, weekId, stream })
    {
      commit('setMeta', {path:'isLoadingUser', value: true})
      await tryCatch( async () => 
      {
        const res = await dispatch('ajax/axiosSend', 
        {
          type: 'get',
          query: `/analytics/hundred/${id}/get`,
          params: {
            stream
          },
          cancel: { name: 'hundredGetUser' },
        },
        { root: true });

        if( !res && res.data.status !== 'success')
          throw new Error
        commit('setData', { path: 'user', value: res.data.data.user[0] ?? res.data.data.user});
        commit('setData', { path: 'faculty', value: res.data.data.faculty });
        commit('setData', { path: 'info', value: res.data.data.info});
        commit('setData', { path: 'actualWeekId', value: res.data.data.actualWeek});
        if(weekId === '')
          commit('setData', { path: 'userWeekList', value: res.data.data.weeks});
        else
          commit('setData', { path: 'userDayList', value: res.data.data.days});
      })
      commit('setMeta', {path:'isLoadingUser', value: false})
    },
    async getWeekList({state, commit }, {id}) {
      commit('setData', {path: 'weekList', value: [-1, 0, 1, 2, 3, 4, 5, 6]})
      return state.data.weekList
    },
    async getAvailableStreams({ commit, dispatch }, id) {
      return await tryCatch(async () => {
        const res = await dispatch('ajax/axiosSend',
          {
            type: 'get',
            query: `/api/users/${id}/hundred/available-streams`,
          },
          {
            root: true
          }
        )
        if(res && res.data.status === 'success') {
          commit('setData', { path: 'availableStreams', value: res.data.data })
        }
        return res
      })
    },
    async userEdit( { commit, dispatch, state }, data)
    {
      let status
      await tryCatch( async () => {
        let res = await dispatch('ajax/axiosSend',
        {
          type: 'post',
          query: '/analytics/hundred/user/edit',
          cancel: 'cancelUserEdit',
          sendData: data
        },
        { root: true })

        let objMessage = {
          message: res.data.status === 'success' ? 'Ваши данные успешно сохранены' : 'Ошибка сервера',
          type: res.data.status,
          error: !res && res.data.status !== 'success',
        }
        dispatch('ajax/addMessage', objMessage, { root: true});

        if(res.data.status ==='success') {
          res.data.data.forEach(cell => {
            commit('cellEdit', {id: cell.cellId, profit: cell.profit})
          })
        }

        status = res.data.status

      })
      return status
    },
    async cellEdit({commit, dispatch}, data) {
      const cell = data
      commit('cellEdit', {id: cell.cellId, profit: cell.profit})

      return await dispatch('ajax/axiosSend', {
        type: 'post',
        query: `/analytics/hundred/cells/${cell.cellId}/edit`,
        sendData: data
      }, { root: true })
    }
  },
  mutations: {
    ...initialMutations,
    cellEdit(state, {id, profit}) {
      let channelIdx = state.data.userWeekList.findIndex(({cellId}) => cellId === id)
      state.data.userWeekList[channelIdx].profit = profit
    }
  }
}

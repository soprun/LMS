import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import createQueryString from "@lms/functions/createQueryString"
import tryCatch from '@lms/functions/tryCatch';
import {
  createTractionTimeline,
  processCoursesList,
  filterMetrics
} from "@lms/store/rating/unified/createTractionTimeline";

export default {
  namespaced: true,
  state: {
    data: {
      page: 1,
      oldQuery: null,
      totalCount: null,
      currentWeek: null,
      revenueInc: null,
      profitInc: null,
      revenueFilled: null,
      revenueAccumulativeFilled: null,
      profitFilled: null,
      profitAccumulativeFilled: null,
      teams: [],
      amoProducts: [],
      courseStreams: [],
      currentStream: null,
      courseList: [],
      faculties: [],
      tractionTypes: [],
    },
    meta: {
      isLoadingTeams: false,
      isLoadingAnalytics: false,
      isLoadingFilters: false,
      isLoadingPage: false,
      noMorePage: false,
    }
  },
  actions: {
    ...initialActions,
    async getAnalyticsData({commit, dispatch, state}) {
      await tryCatch(async () => {
        let queryString = createQueryString();
        commit('setMeta', {path: 'isLoadingAnalytics', value: true })

        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/api/traction-analytics/speed-and-hundred/base${queryString}`,
          cancel: { name: 'getSpeedAndHundredBase' }
        }, {root: true});

        if (!res) {
          return
        }
        if (!res && res.data.status !== 'success')
          throw new Error;

        let data = res.data.data

        // Верхние блоки под фильтрами
        commit('setData', {path: 'totalCount', value: data.total_count})
        commit('setData', {path: 'currentWeek', value: data.current_week})
        commit('setData', {path: 'revenueInc', value: data.revenueInc}) // Выручка
        commit('setData', {path: 'profitInc', value: data.profitInc}) // Прибыль

        // Чарты
        commit('setData', {path: 'revenueFilled', value: data.revenueFilled}) // Заполняемость по выручке
        commit('setData', {path: 'revenueAccumulativeFilled', value: data.revenueAccumulativeFilled}) // Заполняемость по выручке (накопительно)
        commit('setData', {path: 'profitFilled', value: data.profitFilled}) // Заполняемость по прибыли
        commit('setData', {path: 'profitAccumulativeFilled', value: data.profitAccumulativeFilled}) // Заполняемость по прибыли (накопительно)

        commit('setMeta', {path: 'isLoadingAnalytics', value: false})
      })
    },
    async getUsersList({commit, dispatch, state}) {
      await tryCatch(async () => {
        let queryString = createQueryString();
        let isNew = queryString !== state.data.oldQuery
        if( isNew ){
          commit('setMeta', {path: 'isLoadingTeams', value: true })
          commit('setData', { path:'page', value: 1 })
          commit('setMeta', {path: 'noMorePage', value: false })
        } else {
          commit('setMeta', {path: 'isLoadingPage', value: true })
        }

        let objectPage = {
          len: 20,
          page: state.data.page
        }

        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/api/traction-analytics/speed-and-hundred/filter${queryString}`,
          params: objectPage,
          cancel: { name: 'getSpeedAndHundredUsers' }
        }, {root: true});

        if (!res) {
          commit('setData', {path: 'teams', value: []})
          return
        }
        if (!res && res.data.status !== 'success')
          throw new Error;

        if(res.data.data.length === 0) {
          commit('setMeta', {path: 'noMorePage', value: true })
        }

        let data = res.data.data

        commit('setData', {path: 'teams', value: isNew ? data : [...state.data.teams, ...data]})
        commit('setMeta', {path: 'isLoadingTeams', value: false})
        commit('setData', {path: 'oldQuery', value: queryString})
        commit('setMeta', {path: 'isLoadingPage', value: false })
      })
    },
    async getDictionary({commit, dispatch, state}) {
      await tryCatch(async () => {
        const csId = state.data.currentStream?.id ?? 30 // СК 2 - по умолчанию
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/api/traction-analytics/dictionary?csId=${csId}`
        }, {root: true});

        if (!res)
          return
        if (!res && res.data.status !== 'success')
          throw new Error;

        const data = res.data.data

        commit('setData', {path: 'amoProducts', value: data.amoProducts})
        commit('setData', {path: 'courseStreams', value: processCoursesList(data.courseStreams)})
        commit('setData', {path: 'faculties', value: data.faculties})
        commit('setMeta', {path: 'isLoadingFilters', value: false})

        dispatch('getTractionTypes')
      })
    },
    async getTractionTypes({commit, dispatch, state}, courses) {
      try {
        const coursesSlugs = ['speed', 'speed_club', 'hundred', 'msa', 'mi', 'fastcash']
        const resTypes = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/api/traction/type/list`,
          params: {
            courses: coursesSlugs
          }
        }, { root: true })


        if (!resTypes || resTypes.data.status !== 'success')
          throw new Error;

        const metrics = filterMetrics(resTypes.data.data, state.data.courseStreams)

        commit('setData', {path: 'tractionTypes', value: metrics})
      } catch (e) {
        console.error(e)
      }
    },
    sortTeams({commit, dispatch, state}, sort) {
      let teams = state.data.teams

      teams.sort( ( a, b ) => {
        if ( a[sort.sortName] < b[sort.sortName] ) {
          return sort.sortType ? -1 : 1
        }
        if ( a[sort.sortName] > b[sort.sortName] ) {
          return sort.sortType ? 1 : -1
        }
        return 0;
      } );

      commit('setData', {path: 'teams', value: teams})
    },
    clearUserList({commit}) {
      commit('setData', {path: 'totalCount', value: []})
      commit('setData', {path: 'currentWeek', value: []})
      commit('setData', {path: 'revenueInc', value: []})
      commit('setData', {path: 'profitInc', value: []})
      commit('setData', {path: 'revenueFilled', value: []})
      commit('setData', {path: 'revenueAccumulativeFilled', value: []})
      commit('setData', {path: 'profitFilled', value: []})
      commit('setData', {path: 'profitAccumulativeFilled', value: []})
      commit('setData', {path: 'teams', value: []})
      commit('setData', { path: 'oldQuery', value: null})
      commit('setMeta', {path: 'noMorePage', value: false })
      commit('setMeta', {path: 'isLoadingPage', value: false })
      commit('setMeta', {path: 'isLoadingTeams', value: false})
      commit('setMeta', {path: 'isLoadingAnalytics', value: false})
    }
  },
  mutations: {
    ...initialMutations,
    setCurrentStream(state, courseName) {
      state.data.currentStream = state.data.courseStreams?.find(c => c.name === courseName)
    }
  }
}

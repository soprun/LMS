import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import createQueryString from "@lms/functions/createQueryString"
import tryCatch from '@lms/functions/tryCatch';
import moment from "moment";

export default {
  namespaced: true,
  state: {
    data: {
      page: 1,
      folders: [],
      users: [],
      cities: [],
      comments: [],
      oldQuery: null,
      totalCount: 0
    },
    meta: {
      isLoadingFolders: false,
      isLoadingCities: false,
      isLoading: false,
      isLoadingComments: false,
      isLoadingPage: false,
      noMorePage: false,
    }
  },
  actions: {
    ...initialActions,
    async getSettings({commit, dispatch}) {
      commit('setMeta', {path: 'isLoadingFolders', value: true})
      commit('setMeta', {path: 'isLoadingCities', value: true})
      const res = await dispatch('ajax/axiosSend', {
        type: 'get',
        query: '/api/marathon-investment/settings'
      }, {root: true})

      const data = res.data.data
      commit('setData', {path: 'cities', value: data.cities.map(c => ({title: c, value: c}))})
      commit('setData', {path: 'folders', value: data.streams.map(s => ({title: s.name, value: s.id}))})

      commit('setMeta', {path: 'isLoadingFolders', value: false})
      commit('setMeta', {path: 'isLoadingCities', value: false})
    },
    async getUserList({commit, dispatch, state}, { stream, hasVerified, city, name, sort }) {
      try {
        const queryString = createQueryString();
        const isOld = queryString !== state.data.oldQuery
        if (isOld) {
          commit('setMeta', {path: 'isLoading', value: true})
          commit('setData', {path: 'page', value: 1})
          commit('setMeta', {path: 'noMorePage', value: false})
        } else {
          commit('setMeta', {path: 'isLoadingPage', value: true})
        }

        const objectPage = {
          pageSize: 25,
          page: state.data.page
        }

        const sorts = [];
        if (sort.channelId) {
          sorts.push((sort.sortType === 'DESC' ? '-' : '') + sort.channelId);
        }

        const res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/api/v2/marathon-investment/user`,
          params: {
            ...objectPage,
            stream,
            hasVerified: hasVerified ? undefined : false,
            city,
            name,
            sorts
          },
        }, {root: true});

        if (!res) {
          commit('setData', { path: 'users', value: [] });
          commit('setData', { path: 'totalCount', value: 0 });
          return
        }

        const dataUsers = res.data;
        if (!res.data) {
          commit('setMeta', { path: 'noMorePage', value: true });
        }
        commit('setData', {path: 'users', value: !isOld ? [...state.data.users, ...dataUsers] : dataUsers});
        commit('setData', {path: 'oldQuery', value: queryString});
      } catch (e) {
        dispatch('ajax/addMessage', { message: 'При загрузке пользователей произошла ошибка', type: 'error' }, { root: true });
        commit('setData', { path: 'users', value: [] });
        commit('setData', { path: 'totalCount', value: 0 });
        console.error(e);
      } finally {
        commit('setMeta', {path: 'isLoading', value: false});
        commit('setMeta', {path: 'isLoadingPage', value: false});
      }
    },
    async getUsersCount({dispatch, commit}, { stream, hasVerified, city, name }) {
      try {
        const response = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: '/api/v2/marathon-investment/user/count',
          params: {
            stream,
            hasVerified: hasVerified ? undefined : false,
            city,
            name
          }
        }, { root: true });

        commit('setData', { path: 'totalCount', value: response.data });
      } catch (e) {
        console.error(e);
      }
    },
    clearUserList({commit}) {
      commit('setMeta', {path: 'isLoading', value: false})
      commit('setData', {path: 'page', value: 1})
      commit('setMeta', {path: 'noMorePage', value: false})
      commit('setMeta', {path: 'isLoadingPage', value: false})
      commit('setData', {path: 'users', value: []})
      commit('setData', {path: 'oldQuery', value: null})
    },
    async setComments({commit, dispatch}, nicheId) {
      commit('setMeta', { path: 'isLoadingComments', value: true });
      try {
        const response = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/api/v2/business/niche/${nicheId}`
        }, { root: true });

        commit('setData', { path: 'comments', value: response.data.comments });
      } catch (e) {
        dispatch('ajax/addMessage', { message: 'При загрузке комментариев произошла ошибка', type: 'error' }, { root: true });
        console.error(e);
      } finally {
        commit('setMeta', { path: 'isLoadingComments', value: false });
      }
    },
    clearComments({commit}) {
      commit('setData', { path: 'comments', value: [] })
    },
    async postComment({dispatch, commit}, { comment, nicheId }) {
      try {
        const res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/api/v2/business/niche/${nicheId}/comment`,
          sendData: {
            text: comment
          }
        }, { root: true });
        if (res.data.id) {
          dispatch('ajax/addMessage', { message: 'Комментарий отправлен успешно', type: 'success' }, { root: true })
          commit('addComment', { comment: res.data, nicheId })
        }
      } catch (e) {
        dispatch('ajax/addMessage', { message: 'При отправке комментария произошла ошибка', type: 'error' }, { root: true });
        console.error(e)
      }
    },
    async markNiche({dispatch, commit}, payload) {
      await tryCatch(async () => {
        const res = await dispatch('ajax/axiosSend', {
          type: 'patch',
          query: `/api/v2/business/niche/${payload.id}`,
          sendData: {
            verified: !payload.verified
          }
        }, { root: true });
        if(res.data.id) {
          dispatch('ajax/addMessage', { message: 'Статус ниши изменен', type: 'success' }, { root: true })
          commit('markNiche', payload)
        } else {
          dispatch('ajax/addMessage', { message: 'При обновлении данных произошла ошибка', type: 'error' }, { root: true })
        }
      })
    }
  },
  getters: {
    courseList: state => {
      return state.data.folders
    },
    cityList: state => {
      return state.data.cities
    },
    currentUser: (state, getters, rootState) => {
      return rootState.auth.data.user
    }
  },
  mutations: {
    ...initialMutations,
    markNiche(state, payload) {
      payload.verified = !payload.verified
    },
    addComment(state, {comment}) {
      state.data.comments = [ comment, ...state.data.comments ]
    }
  }
}

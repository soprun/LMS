import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import _ from 'lodash'
import tryCatch from "@lms/functions/tryCatch";


export default {
  namespaced: true,
  state: {
    data: {
      localUser: {
        id: 0
      },
      originalUser: {
        id: 0
      },
      cityList: [
        { title: 'Москва', value: 'Москва' },
      ],
      investmentsInfo:{
        pointA: { type: Number, default: undefined },
        pureProfit: { type: Number, default: undefined },
        currentStream: { type: Number },
        niches: [{ id: 0, title: '', description: '', rival: undefined }],
      },
      agreedToTerms: false
    },
    meta: {
      isLoading: false,
      isLoadingUser: false,
      isLoadingNiches: false,
      updatingUser: false
    }
  },
  getters: {
    user(state, getters, rootState) {
      return rootState.auth.data.user
    }
  },
  actions: {
    ...initialActions,
    async getSettings({state, dispatch, commit}) {
      dispatch('setMeta', { path: 'isLoading', value: true })
      const res = tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/api/marathon-investment/configuration`,
        }, { root: true });
        if(res.data.status === 'success' && res.data.data) {
          commit('setCityList', res.data.data.cities)
          commit('setCurrentStream', res.data.data.streams[res.data.data.streams.length - 1].id)
        }
      })
      dispatch('setMeta', { path: 'isLoading', value: false })
    },
    async getUserInfo({state, dispatch}) {
      dispatch('setMeta', { path: 'isLoadingUser', value: true })
      const response = await dispatch('auth/getUserInfo', null, { root: true })
      state.data.originalUser = response
      state.data.localUser = _.cloneDeep(response)
      dispatch('setMeta', { path: 'isLoadingUser', value: false })
      return response
    },
    async getCurrentStream({dispatch, commit}) {
      dispatch('setMeta', { path: 'isLoading', value: true })
      await tryCatch(async () => {
        const res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/api/marathon-investment/current-stream`,
        }, { root: true });
        if(res.data.status === 'success' && res.data.data) {
          commit('setCurrentStream', res.data.data[0].id)
        }
      })
      dispatch('setMeta', { path: 'isLoading', value: false })
    },
    async getInitialProfile({state, commit, dispatch}) {
      dispatch('setMeta', { path: 'isLoading', value: true })
      const res = tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/api/marathon-investment/users/${state.data.originalUser.id}/profile`,
          params: {
            stream: state.data.investmentsInfo.currentStream
          }
        }, { root: true });
        if(res.data.status === 'success' && res.data.data) {
          commit('setProfile', {
            pointA: res.data.data.configuration[0]?.pointA,
            profit: res.data.data.configuration[0]?.profit,
            currentStream: res.data.data.stream,
            niches: res.data.data.niches ?? [{ id: 0, title: '', description: '', rival: undefined }]
          })
        }
      })
      dispatch('getCityList')
      dispatch('setMeta', { path: 'isLoading', value: false })
    },
    async getCityList({commit, dispatch}) {
      dispatch('setMeta', { path: 'isLoading', value: true })
      const res = await dispatch('ajax/axiosSend', {
        type: 'get',
        query: `/api/cities`
      }, { root: true });
      if (res.data.status === 'success' && res.data.data) {
        commit('setCityList', res.data.data)
      }
      dispatch('setMeta', { path: 'isLoading', value: false })
    },
    async updateUserInfo({state, dispatch}) {
      if (!_.isEqual(state.data.localUser, state.data.originalUser)) {
        return await tryCatch(async () => {
          const res = await dispatch('ajax/axiosSend', {
            type: 'post',
            query: `${process.env.AUTH_SERVER_URL}/auth/user/edit`,
            sendData: {
              user: state.data.localUser
            }
          }, { root: true });
          if (res.data.status === 'success' && res.data.data) {
            dispatch('ajax/addMessage', { message: 'Данные обновлены', type: 'success' }, { root: true })
            dispatch('getUserInfo')
            localStorage.removeItem('validMail')
            return true
          } else {
            dispatch('ajax/addMessage', { message: 'При обновлении данных произошла ошибка', type: 'error' }, { root: true })
            dispatch('getUserInfo')
            return false
          }
        })
      } else {
        return true
      }
    },
    async updateNiches({state, dispatch}) {
      try {
        for (const niche of state.data.investmentsInfo.niches) {
          await dispatch('ajax/axiosSend', {
            type: 'post',
            query: `/api/v2/business/niche`,
            sendData: {
              name: niche.name,
              description: niche.description,
              hasRivals: niche.hasRivals,
              main: !!state.data.investmentsInfo.niches.findIndex(n => n.name === niche.name),
              verified: false
            }
          }, { root: true })
        }
        dispatch('ajax/addMessage', { message: 'Данные обновлены', type: 'success' }, { root: true })
        return true
      } catch (e) {
        console.error(e)
        dispatch('ajax/addMessage', { message: 'При обновлении данных произошла ошибка', type: 'error' }, { root: true })
        return false
      }
    },
    async updateProfits({state, dispatch}) {
      return await tryCatch(async () => {
        const res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/api/marathon-investment/configuration`,
          sendData: {
            pointA: state.data.investmentsInfo.pointA,
            profit: state.data.investmentsInfo.pureProfit,
            stream: state.data.investmentsInfo.currentStream
          }
        }, { root: true });
        if (res.data.status === 'success' && res.data.data) {
          dispatch('ajax/addMessage', { message: 'Данные обновлены', type: 'success' }, { root: true })
          return true
        } else {
          dispatch('ajax/addMessage', { message: 'При обновлении данных произошла ошибка', type: 'error' }, { root: true })
          return false
        }
      })
    },
    finishRegistration({commit}) {
      commit('finishRegistration')
    },
    setAgreement({commit}, agreed) {
      commit('setData', { path: 'agreedToTerms', value: agreed })
    }
  },
  mutations: {
    ...initialMutations,
    addEmptyNiche(state) {
      const lastNiche = state.data.investmentsInfo.niches[state.data.investmentsInfo.niches.length - 1]
      state.data.investmentsInfo.niches.push(
        { id: lastNiche.id + 1, title: '', description: '', rival: undefined }
      )
    },
    removeNiche(state, index) {
      state.data.investmentsInfo.niches = state.data.investmentsInfo.niches.filter((v, i) => i !== index)
    },
    setFinishRegistration(state) {
      state.data.localUser.needRegisterMI = false
      state.data.originalUser.needRegisterMI = false
    },
    setCityList(state, list) {
      state.data.cityList = list.map(c => ({ title: c, value: c }))
    },
    setCurrentStream(state, streamId) {
      state.data.investmentsInfo.currentStream = streamId
    },
    setProfile(state, payload) {
      state.data.investmentsInfo.currentStream = payload.currentStream
      state.data.investmentsInfo.pointA = payload.pointA
      state.data.investmentsInfo.pureProfit = payload.profit
      state.data.investmentsInfo.niches = payload.niches
    },
    finishRegistration(state) {
      state.data.originalUser.needRegisterMarathonInvestment = false
    }
  }
}

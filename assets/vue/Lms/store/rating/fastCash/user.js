import _ from 'lodash';
import moment from 'moment';
import initialActions from "@lms/store/initialActions";
import initialMutations from "@lms/store/initialMutations";

import {
  createTractionTimeline,
} from "@lms/store/rating/fastCash/createTractionTimeline";


export default {
  namespaced: true,
  state: {
    data: {
      availableBusiness: [],
      selectedStream: 0,
      userInfo: {
        id: 0,
        name: '',
        lastname: '',
        patronymic: '',
        email: '',
        telegram: '',
        niche: '',
        phone: '',
        businessCategoryName: '',
        businessCategoryId: 0,
        faculty: '',
        city: ''
      },
      tractionData: {},
      tractionStreams: [],
      tractionMetrics: [],
      checkBoxList: []
    },
    meta: {
      isLoadingTractionData: false,
      isLoadingUserInfo: false,
      isLoadingUserBusiness: false,
      isLoadingUserCheckbox: false,
      onboarding: false,
      onboardingStep: 0
    }
  },
  actions: {
    ...initialActions,
    async getUserInfo({commit, dispatch}, userId) {
      let result = {}
      dispatch('setMeta', { path: 'isLoadingUserCheckbox', value: true });
      dispatch('setMeta', { path: 'isLoadingUserInfo', value: true })
      dispatch('setMeta', { path: 'isLoadingRatingData', value: true })
      try {
        const response = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/api/traction/user/${userId}`
        }, {root: true})
        if (response.data.status === 'success' && response.data.data) {
          result = response.data.data
          commit('mapUserData', result)
        }
      } catch (e) {
        console.error(e)
      } finally {
        dispatch('setMeta', { path: 'isLoadingRatingData', value: false })
        dispatch('setMeta', { path: 'isLoadingUserInfo', value: false })
      }
      return result
    },
    async getAvailableBusiness({dispatch, rootState}) {
      let result = {}
      dispatch('setMeta', { path: 'isLoadingUserBusiness', value: true })
      try {
        if (rootState.auth.data.businessTypes.length === 0) {
          await dispatch('auth/getUserBusinessTypes', null, {root: true})
        }
        result = rootState.auth.data.businessTypes
      } catch (e) {
        console.error(e)
      } finally {
        dispatch('setMeta', { path: 'isLoadingUserBusiness', value: false })
        dispatch('setData', { path: 'availableBusiness', value: result })
      }
      return result
    },
    async getTractionData({dispatch, commit}, userId) {
      const coursesSlugs = ['fastcash'];
      let courses = [];
      let metrics = [];
      let values = [];

      dispatch('setMeta', { path: 'isLoadingTractionData', value: true });
      try {
        const coursesResponse = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/api/traction/course-stream/list/${userId}`,
          params: {
            courses: coursesSlugs
          }
        }, { root: true });
        const metricsResponse = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: '/api/traction/type/list',
          params: {
            courses: coursesSlugs
          }
        }, { root: true });
        const valuesResponse = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/api/traction/get/${userId}`
        }, { root: true });
        //console.log(valuesResponse)
        if (_.every([coursesResponse, metricsResponse, valuesResponse], r => r.data.status === 'success')) {
          courses = coursesResponse.data.data;
          metrics = metricsResponse.data.data;
          values = valuesResponse.data.data;
        }

        commit('setTractionData', { courses, metrics, values });

      } catch (e) {
        console.error(e);
      } finally {
        dispatch('setMeta', { path: 'isLoadingTractionData', value: false });
      }
    },
    async changeBusinessSphere({state, dispatch}, newBusiness) {
      try {
        const response = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `${process.env.AUTH_SERVER_URL}/auth/user/edit`,
          sendData: {
            user: {
              id: state.data.userInfo.id,
              businessCategoryId: newBusiness.id
            }
          }
        }, {root: true})
        if (response.data.status === 'success') {
          const updatedUser = {
            ...state.data.userInfo,
            businessCategoryId: newBusiness.id,
            businessCategoryName: newBusiness.text
          }
          dispatch('auth/setUserBusiness', {
            businessCategoryName: newBusiness.text,
            businessCategoryId: newBusiness.id
          }, { root: true })
          dispatch('setData', { path: 'userInfo', value: updatedUser })
          dispatch('ajax/addMessage', { message: 'Cфера обновлена', type: 'success' }, { root: true })
        }
      } catch (e) {
        console.error(e)
      }
    },
    async changeSelectedStream({dispatch}, newStream) {
      dispatch('setData', { path: 'selectedStream', value: newStream })
    },
    async changeRatingData({state, commit, dispatch}, { band, key, value }) {
      const oldData = _.cloneDeep(state.data.tractionData)
      try {
        const updatedData = _.cloneDeep(state.data.tractionData)
        updatedData.bands.forEach(b => {
          if (band.startDate.isSame(b.startDate, 'days')) {
            b.metrics[key] = value
          }
        })
        //console.log(updatedData)
        commit('updateTractionData', updatedData)
        const response = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/api/traction/set/${key}/${moment(band.startDate).format('YYYY-MM-DD')}`,
          sendData: {
            traction: {
              value: value
            }
          }
        }, {root: true})
        if (response.data.status !== 'success') {
          commit('updateTractionData', oldData)
        }
      } catch (e) {
        commit('updateTractionData', oldData)
        console.error(e)
      }
    },
    async getTaskList({commit, dispatch}, userId) {
      let checkbox = [];
      try {
        const response = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/api/traction/checkpoints/50/user/${userId}`,
          params: null
        }, {root: true})
        if (response.data.status === 'success') {
          checkbox = response.data.data;
          commit('setChecboxList', checkbox);
          dispatch('setMeta', { path: 'isLoadingUserCheckbox', value: false });
        }
      } catch (e) {
        console.error(e)
      }
      finally {
        dispatch('setMeta', { path: 'isLoadingUserCheckbox', value: false });
      }
    },
    async checkTask({dispatch}, data) {
      const formData = new FormData()
      if (data.file) {
        formData.append('file', data.file)
      }
      if (data.value.value) {
        formData.append('value', data.value.value)
      }
      try {
        const response = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/api/traction/checkpoint/set/${data.id}`,
          sendData: formData
          
        }, {root: true})
        if (response.data.status === 'success') {
          dispatch('ajax/addMessage', { message: 'Задание успешно выполнено', type: 'success' }, { root: true })
          dispatch('setMeta', { path: 'isLoadingUserCheckbox', value: false });
        }
        return response
      } catch (e) {
        console.error(e)
        dispatch('ajax/addMessage', { message: e.message, type: 'error' }, { root: true })
        dispatch('setMeta', { path: 'isLoadingUserCheckbox', value: false });
      }
      
      
    },
    storeCleanup({commit}) {
      commit('clearStore')
    }
  },
  mutations: {
    ...initialMutations,
    updateTractionData(state, newData) {
      state.data.tractionData = newData;
    },
    setChecboxList(state, data) {
      state.data.checkBoxList = data;
    },
    mapUserData(state, data) {
      state.data.userInfo = {
        ...data,
        businessCategoryName: state.data.availableBusiness.find(b => b.id === data.businessCategoryId)?.text ?? '',
        faculty: data.faculties[0]?.name
      };
    },
    setTractionData(state, { courses, metrics, values }) {
      const fastCashCourse = courses.filter(c => c.abstractCourse.slug === 'fastcash')
      state.data.tractionMetrics = metrics;
      state.data.tractionData = createTractionTimeline(fastCashCourse, metrics, values);
    },
    clearStore(state) {
      state.data = {
        availableBusiness: state.data.availableBusiness,
        selectedStream: 0,
        selectedYear: new Date().getFullYear(),
        userInfo: {
          id: 0,
          name: '',
          lastname: '',
          patronymic: '',
          email: '',
          telegram: '',
          niche: '',
          phone: '',
          businessCategoryName: '',
          businessCategoryId: 0,
          faculty: '',
          city: ''
        },
        tractionStreams: [],
        tractionMetrics: [],
        tractionData: {}
      };
    }
  },
}

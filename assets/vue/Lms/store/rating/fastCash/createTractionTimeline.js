import moment from 'moment';
import _ from 'lodash';

function addBand( metricsList, bandStart, tractionBands ) {

  const band = {
    type: 'day',
    startDate: moment(bandStart).startOf('day'),
    endDate: moment(bandStart).endOf('day'),
    metrics: Object.fromEntries(
      metricsList.map(m => [m.id, undefined])
    ),
    courses: [],
    disabled: moment().isBefore(bandStart, 'days')
  };

  return [...tractionBands, band];
}

function constructTractionBands(hasCourse, metricsList) {
  let tractionBands = [];
  const startOfCourseDate = moment(hasCourse[0]?.startDate).startOf('day');
  let currentDay = moment(startOfCourseDate);
  for (let i = 0; i < 14; i++) {
    tractionBands = addBand(metricsList, currentDay, tractionBands);
    currentDay = moment(currentDay).add(1, 'days');
  }
  return tractionBands;
}

function mapTractionData(timeline, tractionData) {
  timeline.forEach(b => {
    tractionData.forEach(d => {
      if (moment(d.date).isSame(b.startDate, 'days')) {
        b.metrics[d.type] = d.value;
      }
    });
  });
}

function findCurrentBand(bands) {
  return bands.findIndex(b => b.startDate.isSame(moment(), b.type + 's'));
}



export function createTractionTimeline(hasCourse, metrics, tractionData) {
  const bands = constructTractionBands(hasCourse, metrics);
  mapTractionData(bands, tractionData);
  return {
    current: findCurrentBand(bands),
    bands: bands
  };
}

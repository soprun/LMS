import initialActions from "@lms/store/initialActions";
import initialMutations from "@lms/store/initialMutations";
import axios from "axios";
import parseJWT from "@lms/functions/parseJWT";
import cookieMethod from "@lms/functions/cookieMethod";
import tryCatch from "@lms/functions/tryCatch";

const CancelToken = axios.CancelToken;
let cancelObjects = []

export default {
  namespaced: true,
  state: {
    messages: [],
    meta: {
      showPreloader: false,
    }
  },
  actions: {
    ...initialActions,
    async axiosSend(
      {commit, dispatch, state, rootState, rootGetters},
      {
        type = 'post',
        query,
        sendData,
        errorDisplay = true,
        params,
        cancel = {name: ''},
        hasHeaders = true
      }
    ) {
      if (!rootState.auth.data.fingerprint)
        return

      let headers = {}

      // Если первый раз вызвана апишка с cancel
      let idx = -1
      let cancelToken = null
      if (cancel.name) {
        idx = cancelObjects.findIndex(item => item.name === cancel.name)
        if (idx === -1) {
          let source = CancelToken.source()
          cancelObjects.push({name: cancel.name, source})
          cancelToken = source.token
        }
      }

      // Если функция с cancel вызвана несколько раз, отменяем axios, задаем новый токен
      let index = cancelObjects.findIndex(item => item.name === cancel.name)
      if (index !== -1 && idx !== -1) {
        if (cancelObjects[index].source) {
          cancelObjects[index].source.cancel('Cancelled')
          let source = CancelToken.source()
          cancelObjects[index].source = source
          cancelToken = source.token
        }
      }

      if (!query)
        return

      if(hasHeaders) {
        let refreshing;
        // Запрос будет ожидать, если обновляется токен
        await new Promise( resolve => {
          let int = setInterval(() => {
            refreshing = rootGetters['auth/refreshingTokens']

            if(!refreshing){
              resolve()
              clearInterval(int)
            }
          }, 10)
        })

        let isAccessTokenActual = await dispatch('isAccessTokenActual')
        if (rootState.auth.meta.accessTokenExpired || !isAccessTokenActual && !refreshing) // Токен просрочен, надо обновить
          await dispatch('auth/updateTokens', {}, {root: true})
      }

      let options = { // Параметры axios
        method: type,
        url: query,
      }

      if(hasHeaders){
        let accessToken = await cookieMethod({name: 'accessToken', method: 'get'})
        if (accessToken)
          options.headers = {
            authorization: 'Bearer ' + accessToken
          }
      }

      if(sendData)
        options.data = sendData

      if(params)
        options.params = params

      if (cancelToken)
        options.cancelToken = cancelToken
      

      return axios(options)
        .then(response => {
          if (cancelToken)
            cancelObjects.splice(index, 1)

          return response
        }, async (...attrs) => {
          let {response, message} = attrs[0]
          if (response === undefined && message === 'Cancelled') {

            throw('')
          }
          if (response !== undefined && response.status === 302) {
            window.location.replace(window.location.href)
            throw({message: 'Истекло время сессии'})
          }
          if (response !== undefined && response.status === 500) {
            throw({message: 'Ошибка сервера'})
          }
          if (response !== undefined && response.status === 400) { // Если во время запроса устарел accessToken, обновляем токены и отправляем запрос заново
            if (response.data) {
              let error = response.data.errors.find(e => e.message === 'INVALID_TOKEN')
              if (error) {
                await dispatch('ajax/invalidTokenAfterApi', {}, {root: true})
                return response

                // Надо допилиывть, чтобы вызов апишки ожидал результата, и повторно ожидал запрос, если токен помер
                // dispatch('ajax/axiosSend', {type, query, sendData, errorDisplay, cancel, hasHeaders}, {root: true})
              }
            }
          }
          if (response !== undefined && response.status === 404) {
            throw({message: 'Ошибка запроса на сервер'})
          }
          if (response instanceof Error) {
            // Ты тут, если отмена запроса
          } else if (response.data) {

            // Эту строку следует убрать как только мы разберёмся с поломкой доступа
            // Т.к. этот запрос выполняется каждые 30 секунд, периодически отъёбывает доступ с ошибкой accessDenied
            // Пропускать в message это никак нельзя, там срабатывает регулярка и редиректит на курсы
            if(response.data.action === 'getUnreadNotificationsCount')
              return response

            if (errorDisplay)
              Object.values(response.data.errors).forEach((e) => {
                commit('ajax/addMessage', {message: e.message, type: 'error'}, {root: true})
              })
            return response
          }
        })
        .catch((err) => {
          if (err)
            commit('addMessage', {
              message: err.message,
              type: 'error',
              error: err
            })
        });
    },
    removeMessageByIdx({commit}, idx){
      commit('removeMessageByIdx', idx)
    },
    addMessage({commit}, message) {
      commit('addMessage', message)
    },
    async isAccessTokenActual() {
      let accessToken = await cookieMethod({name: 'accessToken', method: 'get'})

      if (accessToken) {
        let token = parseJWT(accessToken)
        let expireDate = new Date(token.exp * 1000)
        let now = new Date()
        now.setSeconds(now.getSeconds() + 2)
        
        return now.getTime() < expireDate.getTime()
      } else
        return false
    },
    async invalidTokenAfterApi({commit, dispatch, state}){
      if (state.meta.refreshingTokens)
        return

      dispatch('setMeta', {path: 'invalidTokenAfterApi', value: true})
      try {
        commit('ajax/addMessage', {message: 'Токен устарел', type: 'error'}, {root: true})
        location.reload()
      } catch (e) {}

      dispatch('setMeta', {path: 'invalidTokenAfterApi', value: false})
    },
    async uploadImage({commit, dispatch}, {formData}) {
      let result;
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/files/img/add`,
          sendData: formData
        }, {root: true})

        if (res.data.status === 'success') {
          result = res.data.data
        }
      })
      return result
    },
    async uploadFile({commit, dispatch}, {formData}) {
      let result;
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/files/add`,
          sendData: formData
        }, {root: true})

        if (res.data.status === 'success') {
          result = res.data.data
        }
      })
      return result
    },
  },
  mutations: {
    ...initialMutations,
    addMessage(state, {message, type, error}) {
      state.messages.push({message, type, error})
    },
    removeMessageByIdx(state, idx){
      state.messages.splice(idx, 1)
    }
  }
}
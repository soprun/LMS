import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import cookieMethod from "@lms/functions/cookieMethod"
import tryCatch from '@lms/functions/tryCatch';
import * as Sentry from "@sentry/vue";

import Vue from 'vue';

import unvalidEmail from '@lms/views/Security/Auth/unvalidEmail/list'

export default {
  namespaced: true,
  state: {
    data: {
      fingerprint: '',
      avatar: '',
      modulePermissions: {
        isAdmin: false
      },
      user: {
        permissions:{
        }
      },
      resetPasswordLink:'',
      courses:[],
      businessTypes: [],
      timezoneList: [],
    },
    meta: {
      checkForTokens: true,
      accessTokenExpired: false,
      refreshingTokens: false,
      refreshing: false,
      loading: false,
      gettingUserInfo: false,
      gettingTimezone: false,
      getUserData: false,
    }
  },
  actions: {
    ...initialActions,

    async logout({commit, dispatch, state}){
      if(state.meta.refreshing)
        return

      commit('setMeta', {path: 'refreshing', value: true})

      let tokens = {accessToken: '', refreshToken: ''}
      await dispatch('setTokens', {tokens})
      Sentry.configureScope(scope => scope.setUser(null));

      window.location.replace('/login')
    },

    async setTokens({commit, dispatch}, {tokens}){
      try {
        let {accessToken, refreshToken} = tokens

        await cookieMethod({name: 'accessToken', value: accessToken, expired: {minutes: 60}, method: 'set'})
        await cookieMethod({name: 'refreshToken', value: refreshToken, expired: {hours: 24*30}, method: 'set'})

        dispatch('setMeta', {path: 'accessTokenExpired', value: false})
      } catch (e) {}
    },

    async updateTokens({dispatch, state}){
      if (state.meta.refreshingTokens)
        return

      dispatch('setMeta', {path: 'refreshingTokens', value: true})
      try {
        let fingerprint = state.data.fingerprint
        let refreshToken = await cookieMethod({name: 'refreshToken', method: 'get'})

        if(refreshToken !== undefined) {
          let res = await dispatch('ajax/axiosSend', {
            type: 'post',
            query: `${process.env.AUTH_SERVER_URL}/auth/refresh`,
            sendData: {refreshToken, fingerprint},
            hasHeaders: false,
            cancel: {name: 'updatingTokens'}
          }, {root: true})
          if (res.data.status === 'success') {
            let tokens = {
              accessToken: res.data.data.accessToken,
              refreshToken: res.data.data.refreshToken
            }
            await dispatch('setTokens', {tokens})
          } else if (res.data.status === 'error')
            console.log('Get new tokens Error', res.data)
        }
      } catch (e) {}

      await dispatch('setMeta', {path: 'refreshingTokens', value: false})
    },

    async userNoAccess({commit, dispatch}, {path}){
      await cookieMethod({name: 'pathUserTriedToEnter', value: path, expired: {minutes: 60}, method: 'set'})
      dispatch('logout')
    },

    async getUserInfo({state, commit, dispatch}){
      commit('setMeta', {path: 'loading', value: true})
      commit('setMeta', {path: 'gettingUserInfo', value: true})
      try {
        let res = await dispatch('ajax/axiosSend', {type: 'get', query: `/user/info/get`}, {root:true})
        if(res.data.status === 'success'){
          Object.entries(res.data.data).forEach((entry) => {
            commit('setData', {path: entry[0], value: entry[1]})
          })
          Sentry.setUser({ id: state.data.user.id, email: state.data.user.email });
        }
      } catch (e) {}
      commit('setMeta', {path: 'loading', value: false})
      commit('setMeta', {path: 'gettingUserInfo', value: false})
      commit('setMeta', {path: 'getUserData', value: true})
      return state.data.user
    },

    async getUserBusinessTypes({commit, dispatch}){
      try {
        let res = await dispatch('ajax/axiosSend', {type: 'get', query: `/api/v2/business/area`, cancel: {name: 'gettingBusinessTypes'}}, {root:true})
        Object.entries(res.data).forEach((entry) => {
          commit('setData', {path: entry[0], value: entry[1]})
        })
      } catch (e) {
        console.error(e)
      }
    },

    async getResetPasswordLink({commit, dispatch}, id){
      await tryCatch (async () => {
        let res = await dispatch('ajax/axiosSend',{
          type:'post',
          query:'/admin/getResetPasswordLink',
          cancel: {name: 'getResetPasswordLink'},
          sendData:{
            authUserId: id
          }
        },{root:true});
        if(res && res.data.status === 'success'){
          commit('setData',{path:'resetPasswordLink', value:res.data.data.link})
          dispatch('ajax/addMessage', {message:'Ссылка скопированна',type:'success'},{root:true})
        }
      })
    },
    async getCoursesForEmail({commit, dispatch,state}){
      await tryCatch( async () =>{
        let res = await dispatch('ajax/axiosSend',{
          type:'get',
          query:'/admin/courses_for_email/get'
        },{root: true})
        if(res && res.data.status){
          commit('setData', {path: 'courses', value: res.data.data.courses})
        }
      })
    },
    setUserAvatar({commit}, {path}) {
      commit('setUserAvatar', {path})
    },
    async getTimezoneList({commit, dispatch}){
      commit('setMeta',{path:'gettingTimezone', value: true})
      await tryCatch (async () => {
        let res = await dispatch('ajax/axiosSend',{
          type:'get',
          query:'/user/timezones/get',
        }, {root:true});

        if(res && res.data.status === 'success'){
          commit('setData',{path:'timezoneList', value: res.data.data.timezones})
        }
      })
      commit('setMeta',{path:'gettingTimezone', value: false})
    },
    setUserBusiness({commit}, business) {
      commit('setUserBusiness', business)
    }
  },
  mutations: {
    ...initialMutations,
    setFranchAttendance(state, {typeId}) {
      state.data.user.franchGameAttendanceTypeId = typeId
      state.data = {...state.data}
    },
    setUserAvatar(state, {path}) {
      state.data.user.avatar = path;
    },
    setUserBusiness(state, { businessCategoryName, businessCategoryId }) {
      state.data.user = {
        ...state.data.user,
        businessCategoryName,
        businessCategoryId
      }
    }
  },
  getters: {
    refreshingTokens(state){
      return state.meta.refreshingTokens
    },
    isValidMail(state) {
      if (!state.data.user.email) return true

      let regexpEmail = new RegExp(`@${unvalidEmail.join('$|@')}$`, 'i')

      let res = regexpEmail.test(state.data.user.email)
      if(res)
        return false
      return true
    }
  },
}

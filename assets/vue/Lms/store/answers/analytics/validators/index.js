import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import createQueryString from "@lms/functions/createQueryString"
import tryCatch from "@lms/functions/tryCatch";

export default {
  namespaced: true,
  state: {
    data: {
      validatorsList: [],        
      users:[],
      tariffs:[],
      validatorsCSV: ''
    },
    meta: {
      gettingValidators: false,
      gettingValidatorsCSV: false,
    }
  },
  actions: {
    ...initialActions,
    async getValidators({commit, dispatch}) {
      commit('setMeta', {path: 'gettingValidators', value: true})
      commit('setData', { path: 'validatorsList', value: [] })
      commit('setData', { path: 'users', value: [] })
      commit('setData', { path: 'tariffs', value: [] })

      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/answers/analytics/validators/get${createQueryString()}`,
          cancel: {name: 'gettingAnswersAnalyticsValidators'}
        }, {root: true})

        if (res && res.data.status === 'success') {
          Object.entries(res.data.data).forEach( el => {
            commit('setData', {path: el[0], value: el[1]})
          })
          
          commit('setMeta', {path: 'gettingValidators', value: false})
        }
      })
    },
    async getCSV({commit, dispatch}){
      await tryCatch( async () => {
        commit('setMeta', {path: 'gettingValidatorsCSV', value: true})
        setTimeout( () => commit('setMeta', {path: 'gettingValidatorsCSV', value: false}), 2000)
      });
    }
  },
  mutations: {
    ...initialMutations
  }
}
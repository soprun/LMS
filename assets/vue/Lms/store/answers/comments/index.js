import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import createQueryString from "@lms/functions/createQueryString"
import tryCatch from "@lms/functions/tryCatch";

export default {
  namespaced: true,
  state: {
    data: {
      comments: {
        comments: [],
        state: [],
        users: [],
      },
    },
    meta: {
      fetchingCommentsList: false,
      hasMoreComments: true,
    }
  },
  actions: {
    ...initialActions,
    async fetchAllCommentsList({commit, dispatch}, params) {
      let result = ''

      commit('setMeta', {path: 'fetchingCommentsList', value: true})

      try {
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/answers/comments/all/get`,
          params,
        }, {root: true})
        if (res && res.data.status === 'success') {
          if (res.data.data.comments.length) {
            if (params.page === 1)
              commit('setCommentList', res.data.data)
            else 
              commit('fetchCommentsList', res.data.data)
          } else {
            commit('setMeta', {path: 'hasMoreComments', value: false})
          }
          result = 'success'
        } else {
          //Остановка в случае ошибки
          commit('setMeta', {path: 'hasMoreComments', value: false})
        }
      } catch (e) {
        commit('setMeta', {path: 'hasMoreComments', value: false})
      }

      commit('setMeta', {path: 'fetchingCommentsList', value: false})
      
      return result
    },
    async fetchCommentsList({commit, dispatch}, params) {
      let result = ''

      commit('setMeta', {path: 'fetchingCommentsList', value: true})

      try {
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/answers/comments/get`,
          params,
        }, {root: true})
        if (res && res.data.status === 'success') {
          if (res.data.data.comments.length) {
            if (params.page === 1)
              commit('setCommentList', res.data.data)
            else 
              commit('fetchCommentsList', res.data.data)
          } else {
            commit('setMeta', {path: 'hasMoreComments', value: false})
          }
          result = 'success'
        } else {
          //Остановка в случае ошибки
          commit('setMeta', {path: 'hasMoreComments', value: false})
        }
      } catch (e) {
        commit('setMeta', {path: 'hasMoreComments', value: false})
      }

      commit('setMeta', {path: 'fetchingCommentsList', value: false})
      
      return result
    },
    clearCommentsList({commit}, data) {
      commit('setCommentList', {
        comments: [],
        state: [],
        users: [],
      })
    },
    async sendComment({commit, dispatch}, data) {
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: '/courses/task/answer/comment/new',
          sendData: data
        }, {root: true})
        if (res.data.status === 'success') {
          dispatch('ajax/addMessage', {message: 'Ответ отправлен', type: 'success'}, {root:true})
          commit('removeComment', {id: data.answer.id})
        }
      })
    },
    async editComment({commit, dispatch}, data) {
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/courses/task/answer/comment/${data.comment.id}/edit`,
          sendData: data
        }, {root: true})
        if (res.data.status === 'success') {
          dispatch('ajax/addMessage', {message: 'Ответ изменён', type: 'success'}, {root:true})
          commit('editComment', {id: data.answer.id, text: data.answer.comments.text})
        }
      })
      commit('editComment', {
        id: data.answer.id,
        text: data.answer.comments.text,
        commentatorFiles: data.answer.comments.commentatorFiles,
      })
    },
    async getLastComments({commit, dispatch}, answer) {
      let result =  {}

      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: '/courses/task/comments/get',
          sendData: {answer}
        }, {root: true})
        if (res.data.status === 'success') {
          res.data.data.comments.pop()
          result = {status:'success', data: res.data.data.comments.reverse()};
        }
      })

      return result;
    },
    async invertCommentState({dispatch}, {id}) {
      let result = {}

      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/courses/task/answer/comment/${id}/mark`,
        }, {root: true})
        if (res.data.status === 'success') {
          result = {status:'success', data: res.data.data.statusId};
        }
      })

      return result;
    }
  },
  mutations: {
    ...initialMutations,
    fetchCommentsList(state, data) {
      state.data.comments.comments = [
        ...state.data.comments.comments,
        ...data.comments,
      ],
      state.data.comments.users = [
        ...state.data.comments.users,
        ...data.users,
      ];
    },
    setCommentList(state, data) {
      state.data.comments = data
    },
    setAnotherComments(state, {id, data}) {
      let idx = state.data.comments.comments.findIndex(comment => {
        return comment.answerId === id
      })
      state.data.comments.comments[idx].anotherComments = data; 
    },
    removeComment(state, {id}) {
      let idx = state.data.comments.comments.findIndex(comment => {
        return comment.answerId === id
      })
      state.data.comments.comments.splice(idx, 1)
    },
    editComment(state, {id, text, commentatorFiles}) {
      let idx = state.data.comments.comments.findIndex(comment => {
        return comment.answerId === id
      })
      state.data.comments.comments[idx].comment = text;
      state.data.comments.comments[idx].commentatorFiles = commentatorFiles;
    },

  }
}
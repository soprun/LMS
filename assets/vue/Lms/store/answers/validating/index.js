import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import tryCatch from "@lms/functions/tryCatch";

export default {
  namespaced: true,
  state: {
    data: {
      selectData: [],
      taskBlocks: [],
      tariffs: [],
      lessonId: '',
      courseData: []
    },
    meta: {
      gettingTask: false,
      gettingTaskList: false,
      isLoadTasks: false
    },
    aside: {}
  },
  actions: {
    ...initialActions,
    async getTaskList({commit, dispatch}) {
      commit('setMeta', {path: 'gettingTaskList', value: true})

      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/answers/validating/courses/list`,
          cancel: {name: 'gettingAnswersTaskList'}
        }, {root: true})
        if (res && res.data.status === 'success') {
            commit('setData', {path: 'selectData', value: res.data.data})
          
          commit('setMeta', {path: 'gettingTaskList', value: false})
        }
      })
    },
    async getTaskListData( { dispatch,commit }, courseId )
    {
      commit( 'setMeta', { path: 'isLoadTasks', value: true})
      commit('setData', { path: 'taskBlocks', value: []})
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/answers/validating/course/${courseId}/list`,
          cancel: {name: 'gettingAnswersTaskListData'}
        }, {root: true})
        if (res && res.data.status === 'success')
          commit('setData', {path: 'courseData', value: res.data.data.tasks ? res.data.data.tasks : [] })

        commit( 'setMeta', { path: 'isLoadTasks', value: false})
      })
    },
    async getTask({commit, dispatch}, {variety, id, lessonId, name, userId}) {
      commit('setMeta', {path: 'gettingTask', value: true})
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/answers/validating/one/random/get`,
          cancel: {name: 'gettingAnswersSelect'},
          params: {
            taskBlock: {variety, id},
            name,
            userId
          }
        }, {root: true});

        if (res.data.status === 'success' && res.data.data) {
          commit('setData', {path: 'lessonId', value: lessonId});
          commit('setData', {path: 'taskBlocks', value: res.data.data.taskBlocks ? res.data.data.taskBlocks : []});
          commit('setData', {path: 'tariffs', value: res.data.data.tariffs ? res.data.data.tariffs : []});

          commit('setMeta', {path: 'gettingTask', value: false})
        }
      })
    },

    async sendTask({commit, dispatch}, {check, taskInfo}) {
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: '/answers/validating/check',
          sendData: {check}
        }, {root: true})
        if (res.data.status === 'success') {
          dispatch('ajax/addMessage', {message: 'Ответ отправлен', type: 'success'}, {root:true})
          dispatch('getTaskList');
          dispatch('getTask', taskInfo);
        }
      })
    },
    async clearTask({commit}){
      await tryCatch( async () =>{
        commit('setData', {path: 'taskBlocks', value: []});
      })
    },
    clearTaskData( {commit} )
    {
      commit('setData', {path: 'courseData', value: []})
    },
  },
  mutations: {
    ...initialMutations,
  },
}
import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import tryCatch from "@lms/functions/tryCatch";
import createQueryString from "@lms/functions/createQueryString";
import router from "@lms/router";

export default {
  namespaced: true,
  state: {
    data: {
      feedTaskList: [],
      taskBlocks: [],
      tariffs: [],
      lessonId: '',
      selectData: [],
      courseData: [],
    },
    meta: {
      gettingFeedTask: false,
      gettingFeedTaskList: false,
      gettingSearchingFeedTask:false,
      addingFeedTask: false,
      isLoadTasks: false,
    },
    aside: {}
  },
  actions: {
    ...initialActions,
    async getTaskListCourse({commit, dispatch}) {
      commit('setMeta', {path: 'gettingTaskList', value: true})

      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/answers/validating/courses/list`,
          cancel: {name: 'gettingAnswersTaskList'}
        }, {root: true})
        if (res && res.data.status === 'success') {
            commit('setData', {path: 'selectData', value: res.data.data})
          
          commit('setMeta', {path: 'gettingTaskList', value: false})
        }
      })
    },
    async getTaskListData( { dispatch,commit }, courseId )
    {
      commit( 'setMeta', { path: 'isLoadTasks', value: true})
      commit('setData', { path: 'taskBlocks', value: []})
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/answers/validating/feed/course/${courseId}/list/get`,
          cancel: {name: 'gettingAnswersTaskListData'}
        }, {root: true})

        if (res && res.data.status === 'success')
          commit('setData', {path: 'courseData', value: res.data.data ? res.data.data.tasks : [] })

        commit( 'setMeta', { path: 'isLoadTasks', value: false})
      })
    },
    async getFeedTasks({commit, dispatch, state}, {page = 1, variety, id, isHasValidator, lessonId}) {

      page == 1 ? commit('setMeta', {path: 'gettingFeedTask', value: true}
        ) : commit('setMeta', {path: 'addingFeedTask', value: true})
      
      let params = {
        page,
        taskBlock :{
          variety: variety, id: id
        },
        isHasValidator
      }
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/answers/validating/feed/get`,
          cancel: {name: 'cancelFeedTask'},
          params: params
        }, {root: true});

        if(res)
          if (res.data.status === 'success' && res.data.data) {
            commit('setData', {path: 'lessonId', value: lessonId});
            if( page == 1)
            commit('setData', {path: 'taskBlocks', value: res.data.data.taskBlocks});
          else 
            commit('setData', 
              { 
                path: 'taskBlocks', value: [...state.data.taskBlocks ,...res.data.data.taskBlocks]
              }
            )
            commit('setData', {path: 'tariffs', value: res.data.data.tariffs ? res.data.data.tariffs : []});
            commit('setMeta', {path: 'gettingFeedTask', value: false})
            commit('setMeta', {path: 'addingFeedTask', value: false})
          }
      })
    },

    async getUserTask({commit, dispatch}, {course}) {
      commit('setMeta', {
        path: 'gettingFeedTask',
        value: true
      });
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/answers/feed/course/${course.id}/get`,
          cancel: {name: 'cancelFeedTask'},
        }, {root: true});

        if(res)
          if (res.data.status === 'success' && res.data.data) {
            commit('setData', {
              path: 'feedTask',
              value: {
                course: res.data.data.course,
                taskBlocks: res.data.data.taskBlocks ? res.data.data.taskBlocks : [],
              }
            });
            commit('setMeta', {
              path: 'gettingFeedTask',
              value: false
            })
          }
      })
    },
    async sendFeedTask({commit, dispatch}, {check}) {
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: '/answers/validating/check',
          sendData: {check}
        }, {root: true})
        if (res.data.status === 'success') {
          dispatch('ajax/addMessage', {message: 'Ответ отправлен', type: 'success'}, {root:true})
          dispatch('getFeedTaskList')
        }
      })
    },
    async clearFeedTaskBlocks({commit}){
      await tryCatch( async () => {
        commit('setData', {path: 'taskBlocks', value: []});
      })
    },
    async searchFeedTask({commit, dispatch}, {lessonId, }){

    },

    async searchFeedTaskEmail({commit,dispatch}, data={courseId: null}){
      let hasName = new URL(location.href).searchParams.get('name')
      if(!hasName) return

      commit('setMeta', {path: 'gettingSearchingFeedTask', value: true});
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/user/answers/get` + createQueryString(),
          params: {
            courseId: data.courseId ? data.courseId : null
          },
          cancel: {name: 'cancelSearchFeedTaskUser'},
        }, {root: true})

        if (res)
          if (res.data.status === 'success' && res.data.data) {
            if (res.data.data.taskBlocks && res.data.data.tariffs) {
              commit('setData', {path: 'taskBlocks', value: res.data.data.taskBlocks});
              commit('setData', {path: 'tariffs', value: res.data.data.tariffs});
            } else
              dispatch('getFeedTasks', {variaty: variaty, id: id})

            commit('setMeta', {path: 'gettingSearchingFeedTask', value: false})
          }
      })
    },
    async sendComment( {commit, dispatch }, data)
    {
      let boolean = false
      await tryCatch( async () => 
      {
        let res = await dispatch('ajax/axiosSend',
        {
          type: 'post',
          query: '/courses/task/answer/comment/new',
          sendData: data,
        },
        { root: true })
        await dispatch('getComments', { id: data.answer.id, variety: data.question.variety, data: data})
        dispatch('ajax/addMessage', {
          message: res.data.status === 'success' ? 'Комментарий успешно отправлен' : res.data.status,
          type: res.data.status,
          error: res.data.status
        }, { root: true })

        boolean = res.data.status === 'success'
      })
      return boolean
    },
    async getComments( { commit, dispatch, state }, { id, variety})
    {
      await tryCatch( async () => 
      {
        let res = await dispatch('ajax/axiosSend',
        {
          type: 'post',
          query: '/courses/task/comments/get',
          sendData: {
            answer: {
              id: id,
              variety: variety,
              }
          },
          cancel: { name:'cancelGetComments'}
        },
        { root: true });
        if( !res || res.data.status !== 'success')
          throw Error
        
        let newData = _.cloneDeep( state.data.taskBlocks).map( el =>
          {
            el.questionArray = el.questionArray.map( elem => 
            {
              if( elem.answer.id == id ) elem.answer.comments = res.data.data.comments
              return elem 
            })
            return el
          })

        commit('setData', { path: "taskBlocks", value:  newData} )
      })
    },
  },
  mutations: {
    ...initialMutations,
  },
}

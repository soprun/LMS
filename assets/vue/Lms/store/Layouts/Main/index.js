import initialActions from "@lms/store/initialActions";
import initialMutations from "@lms/store/initialMutations";

export default {
  namespaced: true,
  state: {
    meta: {
      menuIsOpen: true
    }
  },
  actions: {
    ...initialActions,
    
    toggleMenu({commit, state}, value){
      commit('setMeta', {path: 'menuIsOpen', value: value === undefined ? !state.meta.menuIsOpen : value})
    }
  },
  mutations: {
    ...initialMutations,

  }
}
export default {
  setMeta(state, {path, value}){
    state.meta[path] = value
  },
  setData(state, {path, value}){
    state.data[path] = _.cloneDeep(value)
  },
  spreadData(state){
    state.data = {...state.data}
  },
  setMultipleData(state, multipleData){
    multipleData.forEach(item => {
      state.data[item.path] = _.cloneDeep(item.value)
    })
  },
  setMultipleMeta(state, multipleMeta){
    multipleMeta.forEach(item => {
      state.meta[item.path] = item.value
    })
  },
  /* =======Aside======== */
  setAsideData(state, {path, value}){
    state.aside.data[path] = _.cloneDeep(value)
  },
  setAsideMeta(state, {path, value}){
    state.aside.meta[path] = value
  },
  setAsideMultipleData(state, multipleData){
    multipleData.forEach(item => {
      state.aside.data[item.path] = _.cloneDeep(item.value)
    })
  },
  setAsideMultipleMeta(state, multipleMeta){
    multipleMeta.forEach(item => {
      state.aside.meta[item.path] = item.value
    })
  },
  /* =======AsideInAside======== */
  setAsideAsideData(state, {path, value}){
    state.aside.aside.data[path] = _.cloneDeep(value)
  },
  setAsideAsideMeta(state, {path, value}){
    state.aside.aside.meta[path] = value
  },
  setAsideAsideMultipleData(state, multipleData){
    multipleData.forEach(item => {
      state.aside.aside.data[item.path] = _.cloneDeep(item.value)
    })
  },
  setAsideAsideMultipleMeta(state, multipleMeta){
    multipleMeta.forEach(item => {
      state.aside.aside.meta[item.path] = item.value
    })
  },
  /* ======= Menu ======== */
  setMenuData(state, {path, value}){
    state.menu.data[path] = _.cloneDeep(value)
  },
  setMenuMeta(state, {path, value}){
    state.menu.meta[path] = value
  },
  setMenuMultipleData(state, multipleData){
    multipleData.forEach(item => {
      state.menu.data[item.path] = _.cloneDeep(item.value)
    })
  },
  setMenuMultipleMeta(state, multipleMeta){
    multipleMeta.forEach(item => {
      state.menu.meta[item.path] = item.value
    })
  },
}
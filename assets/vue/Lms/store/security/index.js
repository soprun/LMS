import router from '@lms/router'
import initialMutations from "@lms/store/initialMutations"
import cookieMethod from "@lms/functions/cookieMethod"

export default {
  namespaced: true,
  state: {
    data: {
      companies: {},
      user: {},
      authErrors: []
    },
    meta: {
      loading: false,
      loginIn: false,
      registering: false,
      addingCompany: false,
      status: 'pending'
    }
  },
  actions: {
    async register({commit, dispatch, state, rootState}, user){
      if(state.meta.registering)
        return

      commit('setMeta', {path: 'registering', value: true})
      try {
        let data = _.cloneDeep(user)
        delete data.rememberMe
        data.fingerprint = rootState.auth.data.fingerprint

        let res = await dispatch('ajax/axiosSend', {type: 'post', query: `${process.env.AUTH_SERVER_URL}/auth/register`, sendData: {...data}, errorDisplay: false}, {root:true})
        if(res.data.status === 'success'){
          let tokens = {
            accessToken: res.data.data.accessToken,
            refreshToken: res.data.data.refreshToken,
          }
          dispatch('auth/setTokens', {tokens}, {root: true})

          dispatch('ajax/addMessage', {message: 'Вы успешно зарегистрировались', type: 'success'}, {root:true})
          router.replace('/courses/folder/list')
        } else if(res.data.errors.length > 0)
          commit('setAuthErrors', res.data.errors)
      } catch (e){}
      commit('setMeta', {path: 'registering', value: false})
    },
    async login({commit, dispatch, state, rootState}, user){
      if(state.meta.loginIn)
        return

      commit('setMeta', {path: 'loginIn', value: true})
      try {
        let data = _.cloneDeep(user)
        delete data.rememberMe
        data.fingerprint = rootState.auth.data.fingerprint

        let res = await dispatch('ajax/axiosSend', {type: 'post', query: `${process.env.AUTH_SERVER_URL}/auth/login`, sendData: {...data}, errorDisplay: false}, {root:true})
        if(res.data.status === 'success') {
          let tokens = {
            accessToken: res.data.data.accessToken,
            refreshToken: res.data.data.refreshToken,
          }
          dispatch('auth/setTokens', {tokens}, {root: true})

          let pathUserTriedToEnter = await cookieMethod({name: 'pathUserTriedToEnter', method: 'get'})
          if(pathUserTriedToEnter)
            dispatch('setPathUserTriedToEnter', {pathUserTriedToEnter})
          else
            router.replace('/courses/folder/list')
        } else if(res.data.errors.length > 0) {
          commit('setAuthErrors', res.data.errors)
        }
      } catch (e){}
      commit('setMeta', {path: 'loginIn', value: false})
    },
    async setPathUserTriedToEnter({dispatch}, {pathUserTriedToEnter}){
      try {
        if(pathUserTriedToEnter) {
          await cookieMethod({name: 'pathUserTriedToEnter', value: '', method: 'set'})
          router.replace(pathUserTriedToEnter)
        } else
          router.replace('/courses/folder/list')
      } catch (e) {}
    },
    async sendResetEmail({commit, dispatch, state}, email){
      if(state.meta.loading) return

      commit('setMeta', {path: 'loading', value: true})
      let status = 'error'
      try {
        let res = await dispatch('ajax/axiosSend', {type: 'post', query: `${process.env.AUTH_SERVER_URL}/auth/requestPasswordReset?platform=lms`, sendData: {user: {email}}, errorDisplay: false}, {root:true})
        if(res.data.status === 'success'){
          dispatch('ajax/addMessage', {message: 'Письмо для восстановления отправлено на почту', type: 'success'}, {root:true})
          status = 'success'
        } else if(res.data.errors.length > 0) {
          commit('setAuthErrors', res.data.errors)
        }
      } catch (e){}
      commit('setMeta', {path: 'loading', value: false})
      return status
    },
    async changePassword({commit, dispatch, state, rootState}, {user, hash}){
      if(state.meta.loading) return

      commit('setMeta', {path: 'loading', value: true})
      let status = 'error'
      try {
        let fingerprint = rootState.auth.data.fingerprint
        let res = await dispatch('ajax/axiosSend', {type: 'post', query: `${process.env.AUTH_SERVER_URL}/auth/resetPassword/${hash}`, sendData: {user, fingerprint}}, {root:true})
        if(res.data.status === 'success'){
          let tokens = {
            accessToken: res.data.data.accessToken,
            refreshToken: res.data.data.refreshToken,
          }
          dispatch('auth/setTokens', {tokens}, {root: true})

          dispatch('ajax/addMessage', {message: 'Пароль изменен', type: 'success'}, {root:true})
          status = 'success'
        }
      } catch (e){}
      commit('setMeta', {path: 'loading', value: false})
      return status
    },
    clearAuthErrors({commit}){
      commit('clearAuthErrors')
    },
    removeAuthError({commit}, field){
      commit('removeAuthError', field)
    },
  },
  mutations: {
    ...initialMutations,
    setAuthErrors(state, errors){
      state.data.authErrors = [
        ...state.data.authErrors,
        ...errors
      ]
    },
    clearAuthErrors(state,){
      state.data.authErrors = []
    },
    removeAuthError(state, field){
      state.data.authErrors.splice(state.data.authErrors.findIndex(e => e.field === field), 1)
      state.data.authErrors = [...state.data.authErrors]
    },
  }
}
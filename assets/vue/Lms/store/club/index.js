import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import createQueryString from "@lms/functions/createQueryString"
import tryCatch from '@lms/functions/tryCatch';
import _ from 'lodash';
import moment from 'moment';


export default {
  namespaced: true,
  state: {
    data: {
      page: 1,
      users: [],
      businessCategories: [],
      user: null,
      oldQuery: null,
      filters: null,
      openFilter: false,
      hypothesis: [],
      photos: []
    },
    meta: {
      isLoading: false,
      isLoadingPage: false,
      noMorePage: false,
      isLoadingHypothesis: false,
      isLoadingPhotos: false,
    }
  },
  actions: {
    ...initialActions,
    async getUserList({ commit, dispatch, state }, filters = null) {
      await tryCatch( async () => {
        let queryString = createQueryString();
        let isOld = queryString !== state.data.oldQuery
        if( isOld ){
          commit('setMeta', {path: 'isLoading', value: true })
          commit('setData', { path:'page', value: 1 })
          commit('setMeta', {path: 'noMorePage', value: false })
        }
        else
          commit('setMeta', {path: 'isLoadingPage', value: true })

        let objectPage = {
          len: 18,
          page: state.data.page
        }
        if(filters) {
          objectPage.params = filters
        }

        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/api/community/list${queryString}`,
          params: objectPage,
        }, {root: true});

        if(!res) {
          commit('setData', { path: 'users', value: []})
          commit('setMeta', {path: 'isLoading', value: false })
          return
        }

        if(!res.data.data || !res.data.data.users || !res.data.data.users.length) {
          commit('setData', {path: 'users', value: []})
          commit('setMeta', {path: 'isLoading', value: false})
          return
        }
        if(!res && res.data.status !== 'success')
          throw new Error;

        if(!res.data.data || !res.data.data.users || res.data.data.users.length < 18) {
          commit('setMeta', {path: 'noMorePage', value: true })
        }

        let users = res.data.data.users

        commit('setData', {path: 'users', value: !isOld ? [...state.data.users, ...users] : users})
        commit('setData', {path: 'businessCategories', value: res.data.data.businessCategories})
        commit('setData', {path: 'oldQuery', value: queryString})
        commit('setMeta', {path: 'isLoading', value: false })
        commit('setMeta', {path: 'isLoadingPage', value: false })
      })
    },
    async getUser({ commit, dispatch, state }, userId) {
      await tryCatch( async () => {
        commit('setMeta', {path: 'isLoading', value: true })

        const res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/api/community/profile/${userId}`,
        }, {root: true});

        if(!res) {
          commit('setData', {  path: 'user', value: []})
          return
        }
        if(!res && res.data.status !== 'success')
          throw new Error;

        let data = res.data.data

        commit('setData', {path: 'user', value: data})
        commit('setMeta', {path: 'isLoading', value: false })
      })
    },
    async getFilters({ commit, dispatch, state }) {
      await tryCatch( async () => {
        commit('setMeta', {path: 'isLoadingFilters', value: true })

        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/api/community/list/filters`,
        }, {root: true});

        if(!res) {
          commit('setData', {  path: 'filters', value: []})
          return
        }
        if(!res && res.data.status !== 'success')
          throw new Error;

        let data = res.data.data

        commit('setData', {path: 'filters', value: data})
        commit('setMeta', {path: 'isLoadingFilters', value: false })
      })
    },
    async sendEvent({dispatch}, type) {
      await tryCatch( async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/api/event/create`,
          sendData: {type},
        }, {root: true});
      })
    },
    async getHypothesis({ commit, dispatch, state }, userId) {
      await tryCatch( async () => {
        commit('setMeta', {path: 'isLoadingHypothesis', value: true })

        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `${process.env.ERP_SERVER_URL}/api/hypothesis/list/${userId}`,
        }, {root: true});

        if(!res) {
          commit('setData', {path: 'hypothesis', value: []})
          return
        }
        if(!res && res.data.status !== 'success')
          throw new Error;

        let data = res.data.data

        commit('setData', {path: 'hypothesis', value: data})
        commit('setMeta', {path: 'isLoadingHypothesis', value: false })
      })
    },
    async getPhotos({ commit, dispatch, state }, userId) {
      await tryCatch( async () => {
        commit('setMeta', {path: 'isLoadingPhotos', value: true })

        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/api/business-photo/list/${userId}`,
        }, {root: true});

        if(!res) {
          commit('setData', {  path: 'photos', value: []})
          return
        }
        if(!res && res.data.status !== 'success')
          throw new Error;

        let data = res.data.data

        commit('setData', {path: 'photos', value: data})
        commit('setMeta', {path: 'isLoadingPhotos', value: false })
      })
    },
    openFilter({commit}, value) {
      commit('setData', {path: 'openFilter', value})
    },
    clearUserList({commit}) {
      commit('setData', {path: 'users', value: []})
      commit('setData', {path: 'user', value: null})
      commit('setData', {path: 'oldQuery', value: ''})
      commit('setMeta', {path: 'isLoading', value: false })
      commit('setMeta', {path: 'isLoadingPage', value: false })
      commit('setMeta', {path: 'noMorePage', value: false })
    }
  },
  mutations: {...initialMutations}
}

import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import tryCatch from "@lms/functions/tryCatch";
import router from "@lms/router";

export default {
  namespaced: true,
  state: {
    data: {
      stagedUserMain: {},
      stagedUserCompany: {},
    },
    meta: {
      updatingMain: false,
      updatingCompany: false
    },
    aside: {
      meta: {
        canSave: false,
        isOpen: false,
        forceClose: false,
        copy: false,
      }
    }
  },
  actions: {
    ...initialActions,
    async settingUpdate({ commit, dispatch, state }, { type }) {
      if (type === 'main' && state.meta.updatingMain)
        return
      else if (state.meta.updatingCompany)
        return

      let status
      commit('setMeta', { path: type === 'main' ? 'updatingMain' : 'updatingCompany', value: true })
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `${process.env.AUTH_SERVER_URL}/auth/user/edit`,
          sendData: {
            user: type === 'main' ? state.data.stagedUserMain : state.data.stagedUserCompany
          }
        }, { root: true });
        if (res.data.status === 'success' && res.data.data) {
          status = 'success'
          dispatch('ajax/addMessage', { message: 'Данные обновлены', type: 'success' }, { root: true })
          dispatch('auth/getUserInfo', {}, { root: true })
          localStorage.removeItem('validMail')
        }
      })
      commit('setMeta', { path: type === 'main' ? 'updatingMain' : 'updatingCompany', value: false })
      return status
    },
    async settingLoadAvatar({ commit, dispatch }, formData) {
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `${process.env.AUTH_SERVER_URL}/auth/upload/avatar`,
          cancel: { name: 'settingUpdateAccount' },
          sendData: formData
        }, { root: true });
        if (res.data.status === 'success' && res.data.data) {
          // commit('updateAvatar', { avatar: res.data.data.path })
          dispatch('auth/setUserAvatar', { path: res.data.data.path }, { root: true })
        }
      })
    },
    async settingSavePhoto({ commit, dispatch }, formData) {
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/api/business-photo/upload`,
          sendData: formData
        }, { root: true });
        if (res.data.status === 'success' && res.data.data) {}
      })
    },
    async settingDeletePhoto({ commit, dispatch }, photoId) {
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/api/business-photo/delete/${photoId}`,
        }, { root: true });
        if (res.data.status === 'success' && res.data.data) {}
      })
    },
    async updatePassword({dispatch}, password) {
      let result = '';
      try {
        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `${process.env.AUTH_SERVER_URL}/auth/user/changePassword`,
          sendData: password,
        }, { root: true })

        if (res.data.status === 'success') {
          result = 'success';
          dispatch('ajax/addMessage', {message: 'Пароль успешно изменён', type: 'success'}, {root: true})
        } else {
          result = res.data;
        }
      } catch (e) {
        console.log(e)
      }
      return result
    },
  },
  mutations: {
    ...initialMutations,
    updateAvatar(state, { avatar }) {
      state.data.user.avatar = avatar
      state.data.user = { ...state.data.user }
    }
  }
}

import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import tryCatch from "@lms/functions/tryCatch";
import router from "@lms/router";

export default {
  namespaced: true,
  state: {
    data: {
      course: {
        id: '',
      },
      parts: [],
      folder: {},
      prevLesson: {},
      nextLesson: {},
      lessons: [],
      taskBlocks:[],
      currentLesson: {},
    },
    meta: {
      gettingLesson: false,
      movingLesson: false,
      addingBlock: false,
      deletingBlock: false,
      isLoading: false,
    },
    aside: {
      data: {
        parent: {
          partId: '',
          block: {}
        }
      },
      meta: {
        canSave: false,
        isOpen: false,
        forceClose: false,
      }
    }
  },
  actions: {
    ...initialActions,

    async getLesson({commit, dispatch,state}){
      await tryCatch(async () => {
        commit('setMeta', {path: 'gettingLesson', value: true})

        let res = await dispatch('ajax/axiosSend', {type: 'get', query: `/courses/lesson/${router.currentRoute.params.lessonId}/blocks/get`, cancel: {name: 'gettingLesson'}}, {root: true})

        
        if (res.data.status === 'success') {
          if (res.data.data) {
            /* очищаем дата стор перед заполнением новыми данными */
            commit('setMultipleData', [
              {path: 'course', value: {}},
              {path: 'parts', value: []},
              {path: 'folder', value: {}},
              {path: 'prevLesson', value: {}},
              {path: 'nextLesson', value: {}},
              {path: 'currentLesson', value: {}},
            ])

            Object.entries(res.data.data).forEach((entry) => {

              commit('setData', {path: entry[0], value: entry[1]})
            })

            dispatch('lms/setMenuData', {path: 'course', value: res.data.data.course}, {root: true})
          }
        }
        
        commit('setMeta', {path: 'gettingLesson', value: false})
      })
    },

    async postFiles({commit, dispatch}, {block}) {
      await tryCatch(async () => {
        commit('setMeta', {path: 'postFiles', value: true})
        
        let res = await dispatch('ajax/axiosSend', {
          type: 'get', 
          query: `/courses/file/${block.type}/${block.variety}/${block.id}/add`, 
          sendData: block.content.formData
        }, {root: true})
        
        if (res.data.status === 'success') {
          if (res.data.data) {
            // Object.entries(res.data.data).forEach((entry) => {
            //   commit('setData', {path: entry[0], value: entry[1]})
            // })
          }
        }
        commit('setMeta', {path: 'postFiles', value: false})
      })
    },

    async editLessonsBlock({commit, dispatch}, {block, partId}){
      await tryCatch(async () => {
        commit('setMeta', {path: 'isLoading', value: true})
        
        const updatedLessonsBlock = _.cloneDeep(block)
      
        let res = await dispatch('ajax/axiosSend', {
          type: 'post', 
          query: `/courses/block/edit`,
          sendData: {
            block: updatedLessonsBlock
          }
        }, {root: true})

        if (res.data.status === 'success') {
          if (res.data.data)
            commit('editLessonsBlock', {updatedLessonsBlock, partId})
        }
        commit('setMeta', {path: 'isLoading', value: false})
      })
    },

    async getAndUpdateLessonBlock({commit, dispatch}, {block, partId}){
      await tryCatch(async () => {
        commit('setMeta', {path: 'isLoading', value: true})

        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/courses/block/info/get`,
          params: {
            type: block.type,
            variety: block.variety,
            id: block.id
          }
        }, {root: true})

        if (res.data.status === 'success') {
          if (res.data.data)
            commit('editLessonsBlock', {updatedLessonsBlock: res.data.data, partId})
        }
        commit('setMeta', {path: 'isLoading', value: false})
      })
    },

    async setBlockIdsBeforeAdd({commit, dispatch}, {block, partId}){
      let newBlock;
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/courses/block/info/get`,
          params: {
            type: block.type,
            variety: block.variety,
            id: block.id
          }
        }, {root: true})

        if (res.data.status === 'success') {
          if (res.data.data)
            newBlock = _.cloneDeep(res.data.data)
        }
      })
      return newBlock
    },

    async addBlock({commit, dispatch}, {block, partId}) {
      await tryCatch(async () => {
        commit('setMeta', {path: 'addingBlock', value: true})

        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/courses/part/${partId}/block/new`,
          sendData: {block}
        }, {root: true})

        if (res.data.status === 'success' && res.data.data) {
          let newBlock = _.cloneDeep(block)
          newBlock.id = res.data.data.id

          // Если это задание, нужно получить целый контент с проставленными айдишниками
          if (block.type === 2 && block.variety === 1) {
            let resQuestions = await dispatch('ajax/axiosSend', {
              type: 'get',
              query: `/courses/task/info/get`,
              params: {
                taskBlock: {
                  variety: block.variety,
                  id: res.data.data.id
                }
              }
            }, {root: true})

            // присваиваем контент
            if (resQuestions.data.status === 'success' && resQuestions.data.data)
              newBlock.content = {
                taskBlock: resQuestions.data.data.taskBlock,
                questionArray: resQuestions.data.data.questionArray
              }
          }

          // Если это тест франч, нужно получить целый контент с проставленными айдишниками
          // Теперь для любого блока работает
          if (block.type === 4 && block.variety === 2 || block.getIds) {
            let franchTestRes = await dispatch('setBlockIdsBeforeAdd', {block: newBlock, partId})
            newBlock.content = _.cloneDeep(franchTestRes.content)
          }

          // Обычное добавление блока
          commit('addBlock', {newBlock, partId})

          dispatch('ajax/addMessage', {message: 'Блок добавлен', type: 'success'}, {root: true})
        }
        commit('setMeta', {path: 'addingBlock', value: false})
      })
    },

    async deleteBlock({commit, dispatch}, {block, partId}){
      await tryCatch(async () => {
        commit('setMeta', {path: 'deletingBlock', value: true})

        let res = await dispatch('ajax/axiosSend', {
          type: 'delete',
          query: `/courses/block/delete`,
          sendData: {block}
        }, {root: true})

        if (res.data.status === 'success') {
          commit('deleteBlock', {block, partId})
        }
        commit('setMeta', {path: 'deletingBlock', value: false})
      })
    },

    async moveBlockUp({commit, dispatch, state}, {block, partId}){
      if(state.meta.movingBlockUp)
        return

      await tryCatch(async () => {
        commit('setMeta', {path: 'movingBlockUp', value: true})

        block.blockId = block.id
        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/courses/block/up`,
          sendData: {block}
        }, {root: true})

        if (res.data.status === 'success') {
          delete block.blockId
          commit('moveBlockUp', {block, partId})
        }
        commit('setMeta', {path: 'movingBlockUp', value: false})
      })
    },

    async moveBlockDown({commit, dispatch, state}, {block, partId}){
      if(state.meta.movingBlockDown)
        return

      await tryCatch(async () => {
        commit('setMeta', {path: 'movingBlockDown', value: true})

        block.blockId = block.id
        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/courses/block/down`,
          sendData: {block}
        }, {root: true})

        if (res.data.status === 'success') {
          delete block.blockId
          commit('moveBlockDown', {block, partId})
        }
        commit('setMeta', {path: 'movingBlockDown', value: false})
      })
    },

    async duplicateBlock({commit, dispatch}, {block, partId}){
      await tryCatch(async () => {
        commit('setMeta', {path: 'movingBlockUp', value: true})

        block.blockId = block.id
        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/courses/block/duplicate`,
          sendData: {block}
        }, {root: true})

        if (res.data.status === 'success') {
          delete block.blockId
          let newBlock = _.cloneDeep(block)
          newBlock.id = res.data.data.id
          commit('duplicateBlock', {oldBlock: block, newBlock, partId})
        }
        commit('setMeta', {path: 'movingBlockUp', value: false})
      })
    },

    async deleteFiles({commit, dispatch}, {files}) {
      let result;
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'delete',
          query: `/courses/block/files/delete`,
          sendData: {files}
        }, {root: true})

        if (res.data.status === 'success') {
          result = 'success'
        }
      })
      return result
    },

    async sendAnswers({commit, dispatch, state}, {answers, block, partId}){
      let answersIds;
      await tryCatch(async () => {
        let answersToSend = _.cloneDeep(answers)
        answersToSend.setOfAnswers = answers.setOfAnswers.map(item => {
          if(!item.answer.files)
            item.answer.files = []

          return item
        })
        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/courses/task/answer/new`,
          sendData: {answers: answersToSend}
        }, {root: true})

        if (res.data.status === 'success') {
          // если игра, обновляем меню
          if(router.currentRoute.path.includes('/franch') && res.data.data.numbers)
            await dispatch('lms/setMenuData', {path: 'numbers', value: res.data.data.numbers}, {root: true})

          answersIds = _.cloneDeep(res.data.data.answersIds)

          let resQuestions = await dispatch('ajax/axiosSend', {
            type: 'get',
            query: `/courses/task/info/full/get`,
            params: {
              taskBlock: {
                variety: block.variety,
                id: block.id
              }
            }
          }, {root: true})

          // присваиваем контент
          if (resQuestions.data.status === 'success' && resQuestions.data.data)
            block.content = {
              taskBlock: resQuestions.data.data.taskBlock,
              questionArray: resQuestions.data.data.questionArray
            }

          dispatch('ajax/addMessage', {message: 'Ответ отправлен', type: 'success'}, {root:true})
          commit('updateTask', {answers, block, partId, newQuestionIds: res.data.data.answersIds})
        }
      })
      return answersIds
    },
    
    async editAnswers({commit, dispatch, state}, {answers, block, partId}){
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/courses/task/answer/edit`,
          sendData: {answers}
        }, {root: true})

        if (res.data.status === 'success') {
          dispatch('ajax/addMessage', {message: 'Ответ изменен', type: 'success'}, {root:true})
        }
      })
    },

    async sendTestAnswer({commit, dispatch, state}, {questionId, answerIds}){
      let actionRes;
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/courses/task/test/answer`,
          sendData: {
            questionId,
            answerIds
          }
        }, {root: true})

        if (res.data.status === 'success') {
          actionRes = _.cloneDeep(res.data.data)
        }
      })
      return actionRes
    },

    async sendT5V2({commit, dispatch, state}, {blockId, answer}){
      let actionRes = {}
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/courses/task/type5/answer`,
          sendData: {blockId, answer}
        }, {root: true})

        if (res.data.status === 'success') {
          actionRes = _.cloneDeep(res.data.data)
        }
      })
      return actionRes
    },

    async sendT6V2({commit, dispatch, state}, {blockId, ratedUserId, stars, comment}){
      let actionRes;
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/courses/task/type6/answer`,
          sendData: {blockId, ratedUserId, stars, comment}
        }, {root: true})

        if (res.data.status === 'success') {
          actionRes = 'success'
        }
      })
      return actionRes
    },
    async sendComment( {commit, dispatch, state }, data)
    {
      let status = ''
      await tryCatch( async () => 
      {
        let res = await dispatch('ajax/axiosSend',
        {
          type: 'post',
          query: '/courses/task/answer/comment/new',
          sendData: data,
        },
        { root: true })

        await dispatch('getComments', { id: data.answer.id, variety: data.question.variety })
        dispatch('ajax/addMessage', {
          message: res.data.status === 'success' ? 'Комментарий успешно отправлен' : res.data.status,
          type: res.data.status,
          error: res.data.status
        }, { root: true })
        status = res.data.status === 'success'
      })
      return status
    },
    async getComments( { commit, dispatch, state }, { id, variety})
    {
      await tryCatch( async () => 
      {
        let res = await dispatch('ajax/axiosSend',
        {
          type: 'post',
          query: '/courses/task/comments/get',
          sendData: {
            answer: {
              id: id,
              variety: variety,
              }
          },
          cancel: { name:'cancelGetComments'}
        },
        { root: true });
        if( !res || res.data.status !== 'success')
          throw Error
        let newData = _.cloneDeep( state.data.parts).map( el =>
          {
            el.blocks = el.blocks.map( item => 
              {
                item.content.questionArray = item.content.questionArray.map( elem => 
                  {
                    if( elem.answer.id == id ) 
                      elem.answer.comments = res.data.data.comments

                    return elem 
                  })
                return item
              }
            )
            return el
          });
        commit('setData', { path: "parts", value:  newData} )
      })
    }
  },
  mutations: {
    ...initialMutations,

    editLessonsBlock(state, {updatedLessonsBlock, partId}){
      let partIdx = state.data.parts.findIndex(p => p.id === partId)

      if(partIdx !== -1){
        let blockIdx = state.data.parts[partIdx].blocks.findIndex(l => 
          l.id === updatedLessonsBlock.id
          && l.type === updatedLessonsBlock.type
          && l.variety === updatedLessonsBlock.variety
        )

        if(state.data.parts[partIdx].blocks !== -1) {
          state.data.parts[partIdx].blocks[blockIdx] = {...updatedLessonsBlock}
          state.data.parts[partIdx].blocks = [...state.data.parts[partIdx].blocks]
        }
      }
    },

    addBlock(state, {newBlock, partId}){
      let partIdx = state.data.parts.findIndex(p => p.id === partId)
      newBlock.content.forceEdit = true

      if(partIdx !== -1){
        if(newBlock.prevBlock.blockId) { // блок от родителя
          let blockIdx = state.data.parts[partIdx].blocks.findIndex(b => (
            b.id === newBlock.prevBlock.blockId
            && b.variety === newBlock.prevBlock.variety
            && b.type === newBlock.prevBlock.type
          ))

          if(blockIdx !== -1) {
            delete newBlock.prevBlock
            state.data.parts[partIdx].blocks.splice(blockIdx+1, 0, newBlock)
            state.data.parts = [...state.data.parts]
          }
        } else { // Первый блок
          state.data.parts[partIdx].blocks.push(newBlock)
          state.data.parts = [...state.data.parts]
        }
      }
    },

    deleteBlock(state, {block, partId}){
      let partIdx = state.data.parts.findIndex(p => p.id === partId)

      if(partIdx !== -1){
        let blockIdx = state.data.parts[partIdx].blocks.findIndex(b => (
          b.id === block.id 
          && b.variety === block.variety 
          && b.type === block.type
        ))

        if(blockIdx !== -1) {
          state.data.parts[partIdx].blocks.splice(blockIdx, 1)
          state.data.parts = [...state.data.parts]
        }
      }
    },

    moveBlockUp(state, {block, partId}){
      let partIdx = state.data.parts.findIndex(p => p.id === partId)

      if(partIdx !== -1){
        let blockIdx = state.data.parts[partIdx].blocks.findIndex(b => (
          b.id === block.id
          && b.variety === block.variety
          && b.type === block.type
        ))

        if(blockIdx !== -1 && blockIdx !== 0) {
          let movedBlock = state.data.parts[partIdx].blocks.splice(blockIdx, 1)
          state.data.parts[partIdx].blocks.splice(blockIdx-1, 0, movedBlock[0])
          state.data.parts = [...state.data.parts]
        }
      }
    },

    moveBlockDown(state, {block, partId}){
      let partIdx = state.data.parts.findIndex(p => p.id === partId)

      if(partIdx !== -1){
        let blockIdx = state.data.parts[partIdx].blocks.findIndex(b => (
          b.id === block.id
          && b.variety === block.variety
          && b.type === block.type
        ))

        if(blockIdx !== -1 && blockIdx !== state.data.parts[partIdx].length -1) {
          let movedBlock = state.data.parts[partIdx].blocks.splice(blockIdx, 1)
          state.data.parts[partIdx].blocks.splice(blockIdx+1, 0, movedBlock[0])
          state.data.parts = [...state.data.parts]
        }
      }
    },

    duplicateBlock(state, {oldBlock, newBlock, partId}){
      let partIdx = state.data.parts.findIndex(p => p.id === partId)

      if(partIdx !== -1){
        let oldBlockIdx = state.data.parts[partIdx].blocks.findIndex(b => (
          b.id === oldBlock.id
          && b.variety === oldBlock.variety
          && b.type === oldBlock.type
        ))

        if(oldBlockIdx !== -1) {
          newBlock.content.forceEdit = true
          state.data.parts[partIdx].blocks.splice(oldBlockIdx+1, 0, newBlock)
          state.data.parts = [...state.data.parts]
        }
      }
    },
    updateTask(state, {answers, block, partId, newQuestionIds}){
      let partIdx = state.data.parts.findIndex(p => p.id === partId)

      if(partIdx !== -1){
        let blockIdx = state.data.parts[partIdx].blocks.findIndex(b => (
          b.id === block.id
          && b.variety === block.variety
          && b.type === block.type
        ))

        if(blockIdx !== -1 && blockIdx !== state.data.parts[partIdx].length -1) {
          newQuestionIds.forEach((item, idx) => {
            state.data.parts[partIdx].blocks[blockIdx].content = {...block.content}
            state.data.parts[partIdx].blocks[blockIdx].content.questionArray[idx].answer.id = item.id
            state.data.parts[partIdx].blocks[blockIdx].content.questionArray[idx].answer.createdAt = new Date()
            state.data.parts[partIdx].blocks[blockIdx].content.questionArray[idx].answer.answer = answers.setOfAnswers[idx].answer.answer
          })
          state.data.parts = [...state.data.parts]
        }
      }
    }
  },
}
import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import tryCatch from "@lms/functions/tryCatch";

export default {
    namespaced: true,
    state: {
      data: {},
      aside: {
        data: {
          lesson: {},
          stagedLesson: {},
          course: {},
          idx: ''
        },
        meta: {
          canSave: false,
          isOpen: false,
          forceClose: false,
          copy: false,
        }
      }
    },
    actions:{
        ...initialActions,
        async getUserValidating({commit, dispatch}, {id}){
            await tryCatch( async () => {

            })
        },
        async updateUserValidating({commit, dispatch}, {id}){

        },
        async deleteUserValidating({commit, dispatch},{id}){

        }
    },
    mutations:{
        ...initialMutations,
    }
}
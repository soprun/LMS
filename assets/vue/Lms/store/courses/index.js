import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import tryCatch from "@lms/functions/tryCatch";

export default {
  namespaced: true,
  state: {
    data: {
      courseFolders: [],
    },
    meta: {
      gettingFolders: false
    }
  },
  actions: {
    ...initialActions,
    async getFolders({commit, dispatch}){
      await tryCatch(async () => {
        commit('setMeta', {path: 'gettingFolders', value: true})
        let res = await dispatch('ajax/axiosSend', {type: 'get', query: `/courses/folder/list/get`, cancel: {name: 'gettingFolders'}}, {root: true})
        if (res.data.status === 'success') {
          if(res.data.data.courseFolders)
            commit('setData', {path: 'courseFolders', value: res.data.data.courseFolders})
          commit('setMeta', {path: 'gettingFolders', value: false})
        }
      })
    },
    async removeFolder({commit, dispatch,state}, id){
      let newState  = state.data.courseFolders.filter(el=> el.id != id)
      commit('setData', {path:'courseFolders', value: newState})
    }
  },
  mutations: {
    ...initialMutations,
  },
}
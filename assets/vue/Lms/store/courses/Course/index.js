import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import tryCatch from "@lms/functions/tryCatch";
import router from "@lms/router";

export default {
  namespaced: true,
  state: {
    data: {
      course: {},
      folder: {},
      lessons: [],
      progressBar: 0,
      statsCourseId: null,
      stats: {
        points: null,
        lessons: {},
      },
      lessonArray: '',
      team: ''
    },
    meta: {
      gettingCourse: false,
      movingLesson: false,
      isStatsLoading: false,
    },
    aside: {
      data: {
        lesson: {},
        stagedLesson: {},
        course: {},
        idx: '',
        folderId: ''
      },
      meta: {
        canSave: false,
        isOpen: false,
        forceClose: false,
        copy: false,
      }
    }
  },
  actions: {
    ...initialActions,

    async getCourse({commit, dispatch}, {courseId}) {
      await tryCatch(async () => {
        commit('setMeta', {path: 'gettingCourse', value: true})
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/courses/course/${courseId}/lessons/get`,
          cancel: {name: 'gettingCourse'}
        }, {root: true})
        if (res.data.status === 'success') {
          commit('setData', {path: 'lessons', value: []})
          if (res.data.data) {
            Object.entries(res.data.data).forEach((entry) => {
              commit('setData', {path: entry[0], value: entry[1]})
            })
          }
          commit('setMeta', {path: 'gettingCourse', value: false})
          dispatch('lms/setMenuData', {path: 'course', value: res.data.data.course}, {root: true})
        }
      })
    },
    async createCourse({commit, dispatch, state}, id) {

      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/courses/folder/${id}/course/new`,
          sendData: {
            course: {...state.aside.data.course}
          }
        }, {root: true});
        if (res && res.data.status === 'success') {
          await dispatch('coursesFolder/getFolder', null, {root: true})
          commit('setAsideMeta', {path: 'forceClose', value: true})
        }
        Object.entries(state.aside.data).forEach(el => {
          commit('setAsideData', {path: el[0], value: ''})
        })
      })
    },
    async deleteCourse({commit, dispatch, state}, {folderId}) {
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'delete',
          query: `/courses/course/${state.aside.data.idx}/delete`,
        }, {root: true});
        if (res && res.data.status === 'success')
          await dispatch('coursesFolder/deleteCourse', state.aside.data.idx, {root: true})
      })
    },
    async editCourse({commit, dispatch, state}) {
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/courses/course/${state.aside.data.idx}/edit`,
          sendData: {
            course: state.aside.data.course
          }
        }, {root: true})

        if (res && res.data.status === 'success') {
          await dispatch('coursesFolder/getFolder', null, {root: true})
          commit('setAsideMeta', {path: 'forceClose', value: true})
        }
      })
    },
    async cloneCourse({commit, dispatch, state}) {
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/courses/course/${state.aside.data.idx}/duplicate`,
          sendData: {
            course: {
              ...state.aside.data.course,
              img: state.aside.data.course.img.id,
              coverImg: state.aside.data.course.coverImg.id,
            }
          }
        }, {root: true})
        if (res && res.data.status === 'success')
          await dispatch('coursesFolder/getFolder', null, {root: true})
        // commit('setAsideMeta', {path:'forceClose', value:true})

      })
      Object.entries(state.aside.data).forEach(el => {
        commit('setAsideData', {path: el[0], value: ''})
      })
    },
    async getCourseStats({commit, dispatch, state}, id) {
      // Если курс уже загружен
      if(state.data.statsCourseId === +id) return
      
      commit('setData', {path: 'statsCourseId', value: +id})

      commit('setMeta', {path: 'isStatsLoading', value: true})
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/courses/${id}/stats`
        }, {root: true})
        if (res && res.data.status) {
          commit('setData', {path: 'stats', value: res.data.data})
        }
      })
      commit('setMeta', {path: 'isStatsLoading', value: false})
    },
    async addLesson({commit, dispatch, state}) {
      if (state.aside.meta.addingLesson)
        return

      commit('setAsideMeta', {path: 'addingLesson', value: true})
      let newLesson = _.cloneDeep(state.aside.data.stagedLesson)
      commit('setAsideData', {path: 'stagedLesson', value: {}})
      await tryCatch(async () => {
        let query = `/courses/course/${state.data.course.id}/lesson/new`
        if (newLesson.parentLessonId) { // Если есть поле для дублирования, отправляем на duplicate
          query = `/courses/lesson/${newLesson.parentLessonId}/duplicate`
          delete newLesson.parentLessonId
        }

        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query,
          sendData: {
            lesson: newLesson
          }
        }, {root: true})
        if (res.data.status === 'success') {
          newLesson.id = res.data.data.id
          commit('addLesson', {newLesson})
        }
      })
      commit('setAsideMeta', {path: 'addingLesson', value: false})
    },
    async editLesson({commit, dispatch, state}) {
      if (state.aside.meta.editingLesson)
        return

      commit('setAsideMeta', {path: 'editingLesson', value: true})
      let updatedLesson = _.cloneDeep(state.aside.data.stagedLesson)
      commit('setAsideData', {path: 'stagedLesson', value: {}})
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/courses/lesson/${updatedLesson.id}/edit`,
          sendData: {
            lesson: updatedLesson,
            course: {id: state.data.course.id}
          }
        }, {root: true})
        if (res.data.status === 'success') {
          commit('editLesson', {updatedLesson})
        }
      })
      commit('setAsideMeta', {path: 'editingLesson', value: false})
    },
    async deleteLesson({commit, dispatch, state}, {lesson, idx}) {
      if (state.meta.deletingLesson)
        return

      commit('setMeta', {path: 'deletingLesson', value: true})
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'delete',
          query: `/courses/lesson/${lesson.id}/delete`,
        }, {root: true})
        if (res.data.status === 'success') {
          commit('deleteLesson', {idx})
        }
      })
      commit('setMeta', {path: 'deletingLesson', value: false})
    },
    async moveLesson({commit, dispatch, state}, {id, idx, direction}) {
      if (state.meta.movingLesson)
        return

      commit('setMeta', {path: 'movingLesson', value: true})
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/courses/lesson/${id}/position/${direction}`,
          cancel: {name: 'gettingCourse'}
        }, {root: true})
        if (res.data.status === 'success') {
          commit('moveItem', {idx, direction})
        }
      })
      commit('setMeta', {path: 'movingLesson', value: false})
    },
    openLessonInAside({commit, dispatch, state}, {lesson, idx, copy}) {
      commit('setAsideData', {path: 'lesson', value: lesson})
      commit('setAsideData', {path: 'idx', value: idx})
      commit('setAsideMeta', {path: 'copy', value: copy})
      commit('setAsideMeta', {path: 'isOpen', value: true})
    },
    async destroyPoints({commit}) {
      commit('setData', {path: 'points', value: undefined})
    },
    clearLessons({commit}) {
      commit('setData', {path: 'lessons', value: []})
    }
  },
  mutations: {
    ...initialMutations,

    moveItem(state, {idx, direction}) {
      let movingItem = state.data.lessons.splice(idx, 1)

      if (direction === 'up')
        state.data.lessons.splice(idx - 1, 0, movingItem[0])

      else if (direction === 'down')
        state.data.lessons.splice(idx + 1, 0, movingItem[0])

      else
        console.error('Course direction is not defined')

      state.data.lessons = [...state.data.lessons]
    },
    editLesson(state, {updatedLesson}) {
      let lessonIdx = state.data.lessons.findIndex(l => l.id === updatedLesson.id)
      if (state.data.lessons !== -1) {
        state.data.lessons[lessonIdx] = {...updatedLesson}
        state.data.lessons = [...state.data.lessons]
      }
    },
    addLesson(state, {newLesson}) {
      state.data.lessons.push(newLesson)
    },
    deleteLesson(state, {idx}) {
      state.data.lessons.splice(idx, 1)
    },
  },
}
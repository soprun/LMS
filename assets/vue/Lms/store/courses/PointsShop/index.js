import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import tryCatch from "@lms/functions/tryCatch";

export default {
  namespaced: true,
  state: {
    data: {
      productForPopup: {},
      popupOffer: {},
      courseName: '',
      points: 0,
      balance: 0,
      products: [],
    },
    meta: {
      gettingShop: false,
      purchasing: false,
      shopPromoPopup: false
    }
  },
  actions: {
    ...initialActions,
    async getShop({commit, dispatch}, {courseId}) {
      await tryCatch(async () => {
        commit('setMeta', {path: 'gettingShop', value: true})
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/courses/course/${courseId}/points_shop/get`,
          cancel: {name: 'gettingCourseShop'}
        }, {root: true})
        if (res.data.status === 'success') {
          if (res.data.data) {
            Object.entries(res.data.data).forEach((entry) => {
              commit('setData', {path: entry[0], value: entry[1]})
            })
          }
          commit('setMeta', {path: 'gettingShop', value: false})
        }
      })
    },
    async purchase({commit, dispatch}, {product, courseId}) {
      commit('setMeta', {path: 'purchasing', value: true})
      let actionRes
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/courses/course/${courseId}/points_shop/order`,
          sendData: {
            productId: product.id
          }
        }, {root: true})
        if (res.data.status === 'success') {
          actionRes = res.data.data
          commit('updateBalance', {price: product.price})
        }
      })
      commit('setMeta', {path: 'purchasing', value: false})
      return actionRes
    },
  },
  mutations: {
    ...initialMutations,
    updateBalance(state, {price}) {
      state.data.balance -= +price
    }
  }
}
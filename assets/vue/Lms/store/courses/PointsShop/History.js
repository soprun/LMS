import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import tryCatch from "@lms/functions/tryCatch";

export default {
  namespaced: true,
  state: {
    data: {
      courseName: '',
      orders: [],
    },
    meta: {
      gettingHistory: false
    }
  },
  actions: {
    ...initialActions,
    async getHistory({commit, dispatch}, {courseId}) {
      await tryCatch(async () => {
        commit('setMeta', {path: 'gettingHistory', value: true})
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/courses/course/${courseId}/points_shop/history/get`,
          cancel: {name: 'gettingCourseShopHistory'}
        }, {root: true})
        if (res.data.status === 'success') {
          if (res.data.data) {
            Object.entries(res.data.data).forEach((entry) => {
              commit('setData', {path: entry[0], value: entry[1]})
            })
          }
          commit('setMeta', {path: 'gettingHistory', value: false})
        }
      })
    },
  },
  mutations: {
    ...initialMutations
  }
}
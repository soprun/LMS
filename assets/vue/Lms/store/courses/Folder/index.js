import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import tryCatch from "@lms/functions/tryCatch";
import router from "@lms/router";

export default {
  namespaced: true,
  state: {
    data: {
      folder: {},
      courses: [],
      asideName: 'folderAside'
    },
    meta: {
      gettingFolder: false
    },
  },
  actions: {
    ...initialActions,
    async getFolder({commit, dispatch}) {
      await tryCatch(async () => {
        commit('setMeta', {path: 'gettingFolder', value: true})
        let res = await dispatch('ajax/axiosSend', {
          type: 'get',
          query: `/courses/folder/${router.currentRoute.params.folderId}/courses/get`,
          cancel: {name: 'gettingFolder'}
        }, {root: true})
        if (res.data.status === 'success') {
          if (res.data.data) {
            Object.entries(res.data.data).forEach((entry) => {
              commit('setData', {path: entry[0], value: entry[1]})
            })
          }
          commit('setMeta', {path: 'gettingFolder', value: false})
        }
      })
    },
    async createFolder({commit, dispatch, state}, {folder}) {
      let result

      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/courses/folder/new`,
          sendData: {
            folder
          },
        }, {root:true})

        if(res && res.data.status === 'success')
          result = 'success'
      })

      return result
    },
    async editFolder({commit, dispatch, state}, {folder}) {
      if(folder.id === undefined)
        return

      let result

      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/courses/folder/${folder.id}/edit`,
          sendData: {
            folder
          },
        }, {root: true})
        if(res && res.data.status === 'success')
          result = 'success'
      })

      return result
    },
    async deleteFolder({commit, dispatch}, id) {
      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'delete',
          query: `/courses/folder/${id}/delete`,
        }, {root: true})
        if (res && res.data.status === 'success') {
          dispatch('courses/removeFolder', id, {root: true})
        }
      });
    },
    async cloneFolder({commit, dispatch, state}, {folder}) {

      await tryCatch(async () => {
        let res = await dispatch('ajax/axiosSend', {
          type: 'post',
          query: `/courses/folder/${folder.id}/duplicate`,
          sendData: {
            folder: {
              ...folder,
              img: folder.img !== null ? folder.img.id : null,
              coverImg: folder.coverImg !== null ? folder.coverImg.id : null
            }
          },
        }, {root: true})

        if (res && res.data.status === 'success') {
          await dispatch('courses/getFolders', null, {root: true})
          dispatch('ajax/addMessage', {message: 'Папка скопирована и находится внизу', type: 'success'}, {root:true})
        }
      });

    },
    async deleteCourse({commit, state}, id) {
      commit('setData', {path: "courses", value: state.data.courses.filter(el => el.id !== id)})
    },
    clearCourses({commit}) {
      commit('setData', {path: 'folder', value: {}})
      commit('setData', {path: 'courses', value: []})
    }
  },
  mutations: {
    ...initialMutations,

  },
}
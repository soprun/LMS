import initialActions from "@lms/store/initialActions"
import initialMutations from "@lms/store/initialMutations"
import createQueryString from "@lms/functions/createQueryString"
import tryCatch from '@lms/functions/tryCatch';

export default {
  namespaced: true,
    state:{
      data:{
        query:{
          year:'',
          period:'',
          groupIds:'',
        },
        csv:'',
        users:[],
        analytics:[],
        tariffs:[],
        streams:[],
        searchingUsers: '',
        folders:[],
      },
      meta:{
        isLoadCSV: false,
        isLoadedCSV: false,
        isLoadMsg: false,
        isUpdate: false,
        isSetWeeks: false,
    },
  },
  actions:{
    ...initialActions,
    async getTractionMsgReportsList({commit, dispatch}){
      await tryCatch ( async () => {
        commit('setMeta', {
          path: 'isLoadMsg',
          value: true
        });
        commit('setData', { path:'csv', value:''})
        commit('setMeta', { path:'isLoadedCSV', value: false})
        commit('setData', { path:'analytics', value: [] })
        let res = await dispatch(
          'ajax/axiosSend', 
          {
          type: 'get',
          query: '/analytics/msg/get' + createQueryString(),
          cancel: {name: 'cancelMSGList'},
          },
          { root: true }
        );
        if( res && res.data.status === 'success')
          Object.entries(res.data.data).forEach( el => {
            commit('setData', { path: el[0], value: el[1] })
          })
        
        commit('setMeta', {
          path: 'isLoadMsg',
          value: false
        });
      })
    },
    async setTractionMsgWeeks({commit, dispatch}, col ){
      await tryCatch ( async () => {
        let res = await dispatch(
          'ajax/axiosSend', 
          {
          type: 'post',
          query: '/analytics/msg/user/edit',
          sendData:{
            analytics: col
          }
          },
          { root: true }
        );
      })  
    },
    async setTractionMapTeamWeeks({commit, dispatch}, { id, teamId, weeks }){
      await tryCatch ( async () => {

        let res = await dispatch(
          'ajax/axiosSend', 
          {
            type: 'post',
            query: '/teams/report/user/edit',
            sendData:{
              analytics: {
                id: id,
                teamId: teamId,
                weeks: weeks
              }
            }
          },
          { root: true }
        );
      })
    },
    async updateTractionMapTeamName({commit, dispatch}, { data, id, captain}){
      await tryCatch( async () => {
        let res = await dispatch(
          'ajax/axiosSend',
          {
            type: 'post',
            query: `/users/team/${id}/edit/name`,
            sendData: {
            "team": data
            }
          },
          { root: true }
        );

        if( res && res.data.status === 'success' ){
          commit('setNewTeamName', { value: data.name, id: id, captain: id})
        }
      })
    },
    async setAuthValue({commit, dispatch, rootState}, {id, captain, siteLink, profitPerYear, profitPerLastYear}){
      await tryCatch( async () => {
        let obj = {
          id: id
        }
        if(siteLink)
          obj.siteLink = siteLink
        if(profitPerYear)
          obj.profitPerYear = profitPerYear
        if(profitPerLastYear)
          obj.profitPerLastYear = profitPerLastYear

        let res = await dispatch(
          'ajax/axiosSend',
          {
            type: 'post',
            query: `/admin/user/server/edit`,
            sendData: {
            user: obj,
          }
          },
          { root: true }
        );
        if( res && res.data.status === 'success'){
          delete obj.id
          commit('setNewData', {captain: captain, id: id, data: obj})
        }
      })
    },
    async getGroups({commit, dispatch}){
      await tryCatch(async () => {
        commit('setMeta', {path: 'isLoadGroup', value: true})
        let res = await dispatch('ajax/axiosSend', {type: 'get', query: `/analytics/msg/groups/get`, cancel: {name: 'gettingGroups'}}, {root: true})
        if (res.data.status === 'success') {
          if (res.data.data) {
            /* очищаем дата стор перед заполнением новыми данными */
            Object.entries(res.data.data).forEach((entry) => {
              commit('setData', {path: entry[0], value: entry[1]})
            })
          }
        }
        commit('setMeta', {path: 'isLoadGroup', value: false})
      })
    },
    async getLoadCSV({ commit, dispatch }){
      await tryCatch( async () => {
        commit('setMeta', { path:'isLoadCSV', value: true})
        let res = await dispatch('ajax/axiosSend',
        {
          type:'get',
          query:'/analytics/msg/csv/get'+ createQueryString(),
        },  
        { root: true });
        if(res && res.status === 200){
          commit('setData', { path: 'csv', value: res.data })
          commit('setMeta', { path:'isLoadedCSV', value: true})  
        }
        commit('setMeta', { path:'isLoadCSV', value: false})
      })
    }
  },
  mutations:{
    ...initialMutations,
    setNewTeamName(state, {id, value, captain}){
      state.data.captains = [...state.data.captains.map( el => {
        if( el.captain.user.id == captain)
          el.teams = [...el.teams].map( elem => {
            if(elem.id == id)
              elem.name = data
            return {...elem}
          })
        return el
      })]
    },
    setTeams(state, { data, captainId,id }){
      if(state.data.teams[captainId])
        state.data.teams[captainId][id] = [...data.weeks]
      else {
        state.data.teams[captainId] = {}
        state.data.teams[captainId][id] = [...data.weeks]
      }
    },
    setNewData(state, {id, captain, data}){
      let key = Object.keys(data)[0]
      if(captain){
        state.data.teams[captain][id] = {...state.data.teams[captain][id].map( el => {
          if(el.weeks.user.id == id)
            el.weeks.user[key] = data[key]
          return {...el}
        })} 
      }else {
        state.data.captains = [...state.data.captains.map( el => {
          if(el.captain.user.id == id)
            el.captain.user[key] = data[key]
          return {...el}
        })]
      }
    }
  }
}
import Vue from "vue";
import App from "./App";
import store from "./store";
import router from "./router";
import _ from 'lodash';
import moment from "moment";
import Vuelidate from 'vuelidate'
import VTooltip from 'v-tooltip'
import * as Sentry from "@sentry/vue";
import { Integrations } from "@sentry/tracing";
import './components/index'
// filter
import notificationMessages from "@lms/filters/notificationMessages.filter";
import numberFormat from "@lms/filters/numberFormat.filter";
// mixin
import permissions from './mixins/permission'
//directive
import tinyTextTooltip from "@lms/directives/tinyTextTooltip"

//libra
Vue.use(Vuelidate)
Vue.use(VTooltip, {
  defaultHtml: false,
})
VTooltip.options.defaultTemplate = `<div class="tooltip-lms" role="tooltip">
  <div class="tooltip-arrow"></div><div class="tooltip-inner"></div>
  </div>`
VTooltip.options.popover.defaultWrapperClass = 'wrapper-lms'
VTooltip.options.popover.defaultBaseClass = 'popover-lms'
VTooltip.options.popover.defaultBoundariesElement = document.body
moment.locale('ru')

// sentry
Sentry.init({
  Vue,
  dsn: process.env.APP_DOMAIN !== 'lms.toolbox.wip' ? process.env.SENTRY_DSN : undefined,
  integrations: [
    new Integrations.BrowserTracing({
      routingInstrumentation: Sentry.vueRouterInstrumentation(router),
    }),
  ],
  tracesSampleRate: 1.0,
});


// filters
Vue.filter('notificationMessages', notificationMessages)
Vue.filter('numberFormat', numberFormat)

// mixins
Vue.mixin(permissions)

// directives
Vue.directive('tiny-text-tooltip', tinyTextTooltip)


new Vue({
  components: { App },
  template: "<App/>",
  store,
  router,
}).$mount("#app");

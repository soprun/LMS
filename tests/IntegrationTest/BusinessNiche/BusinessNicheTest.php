<?php

namespace App\Tests\IntegrationTest\BusinessNiche;

use App\Entity\BusinessArea;
use App\Entity\BusinessNiche;
use App\Entity\BusinessNicheComment;
use App\Entity\User;
use App\Service\UserHelper;
use App\Tests\IntegrationTest\Support\WebWithLoginTestCase;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;

final class BusinessNicheTest extends WebWithLoginTestCase
{
    /** @var EntityManagerInterface */
    private $em;
    /** @var BusinessNiche */
    private $niche;

    protected function setUp(): void
    {
        $this->em = $this->getBrowser()->getContainer()->get('doctrine')->getManager();
        $this->em->beginTransaction();

        $area = new BusinessArea();
        $area->setName('Тестовая сфера');
        $this->em->persist($area);

        $user = new User();
        $user->setRoles(['ROLE_USER']);
        $user->setEmail('test@example.com');
        $this->em->persist($user);

        $comment = new BusinessNicheComment();
        $comment->setText('Комментарий');
        $comment->setUser($user);
        $this->em->persist($comment);

        $this->niche = new BusinessNiche();
        $this->niche->setName('Тестовая ниша');
        $this->niche->setDescription('Описание');
        $this->niche->setUser($user);
        $this->niche->setArea($area);
        $this->niche->setHasRivals(true);
        $this->niche->setVerified(true);
        $this->niche->setFranchises('Франшиза');
        $this->niche->setMain(true);
        $this->niche->addComment($comment);

        $this->em->persist($this->niche);
        $this->em->flush();
    }

    public function testGetNiche(): void
    {
        $this->loginUser((new User())->setRoles(['ROLE_USER']));
        $this->getBrowser()->request('GET', '/api/v2/business/niche/' . $this->niche->getId(), [], [], $this->headers);
        self::assertResponseIsSuccessful();
        /** @noinspection JsonEncodingApiUsageInspection */
        self::assertJsonStringEqualsJsonString(
            json_encode($this->getNicheAsArray($this->niche)),
            $this->getBrowser()->getResponse()->getContent()
        );
    }

    public function testGetNicheByUser(): void
    {
        $this->loginUser((new User())->setRoles(['ROLE_USER']));
        $this->getBrowser()->request(
            'GET',
            '/api/v2/user/' . $this->niche->getUser()->getId() . '/business/niche',
            [],
            [],
            $this->headers
        );
        self::assertResponseIsSuccessful();
        /** @noinspection JsonEncodingApiUsageInspection */
        self::assertJsonStringEqualsJsonString(
            json_encode(
                array_map(function (BusinessNiche $n) {
                    return $this->getNicheAsArray($n);
                }, $this->niche->getUser()->getBusinessNiches()->toArray())
            ),
            $this->getBrowser()->getResponse()->getContent()
        );
    }

    /** @dataProvider dataForCreatingProvider */
    public function testAddNiche(array $data, int $responseCode, array $errors): void
    {
        if (isset($data['area']) && $data['area'] === true) {
            $data['area'] = $this->niche->getArea()->getId();
        }
        $this->loginUser($this->niche->getUser());
        $this->getBrowser()->request(
            'POST',
            '/api/v2/business/niche',
            $data,
            [],
            $this->headers
        );

        self::assertResponseStatusCodeSame($responseCode);
        /** @noinspection JsonEncodingApiUsageInspection */
        self::assertEqualsCanonicalizing(
            $errors,
            json_decode($this->getBrowser()->getResponse()->getContent(), true)['errors'] ?? []
        );
    }

    /** @dataProvider dataForEditNicheProvider */
    public function testEditNiche(bool $userIsAdmin): void
    {
        if ($userIsAdmin) {
            $userHelper = $this->getMockBuilder(UserHelper::class)
                ->disableOriginalConstructor()
                ->onlyMethods(['getUserPermissionsByField'])
                ->getMock();
            $userHelper->method('getUserPermissionsByField')->willReturn(['isAdmin' => true]);
            $this->getBrowser()->getContainer()->set(UserHelper::class, $userHelper);
            $this->loginUser((new User())->setRoles(['ROLE_USER'])->setEmail('test@example.com'));
        } else {
            $this->loginUser($this->niche->getUser());
        }
        $this->getBrowser()->request(
            'PATCH',
            '/api/v2/business/niche/' . $this->niche->getId(),
            [
                'description' => 'Новое описание',
            ],
            [],
            $this->headers
        );
        self::assertResponseIsSuccessful();
        /** @noinspection JsonEncodingApiUsageInspection */
        self::assertEquals(
            'Новое описание',
            json_decode($this->getBrowser()->getResponse()->getContent(), true)['description']
        );
    }

    public function dataForEditNicheProvider(): array
    {
        return [
            [false],
            [true],
        ];
    }

    /** @dataProvider dataForDeletingProvider */
    public function testDeleteNiche($user, int $responseCode): void
    {
        if ($user instanceof User) {
            $this->loginUser($user);
        } elseif ($user === 'niche_owner') {
            $this->loginUser($this->niche->getUser());
        }
        $this->getBrowser()->request(
            'DELETE',
            '/api/v2/business/niche/' . $this->niche->getId(),
            [],
            [],
            $this->headers
        );
        self::assertResponseStatusCodeSame($responseCode);
    }

    /** @dataProvider dataForCreatingCommentProvider */
    public function testAddComment(array $data, int $responseCode, array $errors): void
    {
        $userHelper = $this->getMockBuilder(UserHelper::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getUserPermissionsByField'])
            ->getMock();
        $userHelper->method('getUserPermissionsByField')->willReturn(['isAdmin' => true]);
        $this->getBrowser()->getContainer()->set(UserHelper::class, $userHelper);

        $this->loginUser($this->niche->getUser());
        $this->getBrowser()->request(
            'POST',
            '/api/v2/business/niche/' . $this->niche->getId() . '/comment',
            $data,
            [],
            $this->headers
        );
        self::assertResponseStatusCodeSame($responseCode);
        /** @noinspection JsonEncodingApiUsageInspection */
        self::assertEquals(
            $errors,
            json_decode($this->getBrowser()->getResponse()->getContent(), true)['errors'] ?? []
        );
    }

    public function testDeleteComment(): void
    {
        $userHelper = $this->getMockBuilder(UserHelper::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getUserPermissionsByField'])
            ->getMock();
        $userHelper->method('getUserPermissionsByField')->willReturn(['isAdmin' => true]);
        $this->getBrowser()->getContainer()->set(UserHelper::class, $userHelper);

        $this->loginUser($this->niche->getUser());
        $this->getBrowser()->request(
            'DELETE',
            '/api/v2/business/niche/comment/' . $this->niche->getComments()->first()->getId(),
            [],
            [],
            $this->headers
        );
        self::assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function dataForCreatingCommentProvider(): array
    {
        return [
            [
                ['text' => 'Текст комментария'],
                Response::HTTP_CREATED,
                [],
            ],
            [
                [],
                Response::HTTP_BAD_REQUEST,
                [['field' => 'text', 'message' => 'Значение не должно быть пустым.']],
            ],
        ];
    }

    public function dataForDeletingProvider(): array
    {
        return [
            [new User(), Response::HTTP_BAD_REQUEST],
            [(new User())->setRoles(['ROLE_USER']), Response::HTTP_BAD_REQUEST],
            //niche_owner needs manual filling because setUp called after dataProvider
            ['niche_owner', Response::HTTP_OK],
        ];
    }

    public function dataForCreatingProvider(): array
    {
        return [
            [
                [
                    'name' => 'string',
                    'description' => 'string',
                    'franchises' => 'string',
                    'type' => 'offline',
                    'verified' => true,
                    'hasRivals' => true,
                    'area' => true, //area needs manual filling because setUp called after dataProvider
                ],
                Response::HTTP_CREATED,
                [],
            ],
            [
                [
                    'type' => 'wrong type',
                ],
                Response::HTTP_BAD_REQUEST,
                [
                    ['field' => 'type', 'message' => 'Значение недопустимо.'],
                    ['field' => 'name', 'message' => 'Значение не должно быть пустым.'],
                ],
            ],
        ];
    }

    private function getNicheAsArray(BusinessNiche $niche): array
    {
        return [
            'id' => $niche->getId(),
            'name' => $niche->getName(),
            'type' => $niche->getType(),
            'franchises' => $niche->getFranchises(),
            'main' => $niche->isMain(),
            'description' => $niche->getDescription(),
            'user' => [
                'id' => $niche->getUser()->getId(),
                'name' => $niche->getUser()->getName(),
                'lastname' => $niche->getUser()->getLastname(),
                'patronymic' => $niche->getUser()->getPatronymic(),
                'avatar' => $niche->getUser()->getAvatar(),
            ],
            'area' => [
                'id' => $niche->getArea()->getId(),
                'name' => $niche->getArea()->getName(),
            ],
            'verified' => $niche->getVerified(),
            'hasRivals' => $niche->getHasRivals(),
            'comments' => array_map(static function (BusinessNicheComment $c) {
                return [
                    'id' => $c->getId(),
                    'text' => $c->getText(),
                    'user' => [
                        'id' => $c->getUser()->getId(),
                        'name' => $c->getUser()->getName(),
                        'lastname' => $c->getUser()->getLastname(),
                        'patronymic' => $c->getUser()->getPatronymic(),
                        'avatar' => $c->getUser()->getAvatar(),
                    ],
                    'createdAt' => $c->getCreatedAt()->format('c'),
                ];
            }, $niche->getComments()->toArray()),
        ];
    }

    protected function tearDown(): void
    {
        $this->em->rollBack();
    }

}

<?php

namespace App\Tests\IntegrationTest\BusinessNiche;

use App\Entity\BusinessNiche;
use App\Entity\User;
use App\Tests\IntegrationTest\Support\WebWithLoginTestCase;
use JsonException;

final class BusinessTypesTest extends WebWithLoginTestCase
{
    /** @throws JsonException */
    public function testBusinessNicheTypes(): void
    {
        $this->loginUser((new User())->setRoles(['ROLE_USER']));
        $this->getBrowser()->request('GET', '/api/v2/business/type', [], [], $this->headers);
        self::assertResponseIsSuccessful();
        $data = json_decode($this->getBrowser()->getResponse()->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $nicheTypes = [
            'combined' => 'Совмещенный',
            'offline' => 'Оффлайн',
            'online' => 'Онлайн',
        ];
        self::assertEquals($nicheTypes, $data);
    }

}

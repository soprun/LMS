<?php

namespace App\Tests\IntegrationTest\BusinessNiche;

use App\Entity\BusinessArea;
use App\Entity\User;
use App\Tests\IntegrationTest\Support\WebWithLoginTestCase;
use Doctrine\ORM\EntityManagerInterface;

final class BusinessAreasTest extends WebWithLoginTestCase
{
    /** @var EntityManagerInterface */
    private $em;
    /** @var array */
    private $areas;

    protected function setUp(): void
    {
        $this->em = $this->getBrowser()->getContainer()->get('doctrine')->getManager();
        $this->em->beginTransaction();
        $this->areas = [
            (new BusinessArea())->setName('Сфера 1'),
            (new BusinessArea())->setName('Сфера 2'),
        ];
        foreach ($this->areas as $area) {
            $this->em->persist($area);
        }
        $this->em->flush();
    }

    public function testAreas(): void
    {
        $this->loginUser((new User())->setRoles(['ROLE_USER']));
        $this->getBrowser()->request('GET', '/api/v2/business/area', [], [], $this->headers);
        self::assertResponseIsSuccessful();
        /** @noinspection JsonEncodingApiUsageInspection */
        self::assertJsonStringEqualsJsonString(
            json_encode(
                array_map(static function (BusinessArea $a) {
                    return ['id' => $a->getId(), 'name' => $a->getName()];
                }, $this->areas)
            ),
            $this->getBrowser()->getResponse()->getContent()
        );
    }

    protected function tearDown(): void
    {
        $this->em->rollback();
    }

}

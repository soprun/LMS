<?php

namespace App\Tests\IntegrationTest\MarathonInvestment;

use App\Entity\AbstractCourse;
use App\Entity\BusinessNiche;
use App\Entity\BusinessNicheComment;
use App\Entity\CourseStream;
use App\Entity\MarathonInvestment\MarathonInvestmentConfiguration;
use App\Entity\User;
use App\Entity\UserGroup;
use App\Entity\UserGroupUser;
use App\Service\UserHelper;
use App\Tests\IntegrationTest\Support\WebWithLoginTestCase;
use Doctrine\ORM\EntityManagerInterface;
use JsonException;

class MarathonInvesmentMembersTest extends WebWithLoginTestCase
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var array */
    private $members = [];

    protected function setUp(): void
    {
        $this->em = $this->getBrowser()->getContainer()->get('doctrine')->getManager();
        $this->em->beginTransaction();

        $userHelper = $this->getMockBuilder(UserHelper::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getUserPermissionsByField'])
            ->getMock();
        $userHelper->method('getUserPermissionsByField')->willReturn(['isAdmin' => true]);
        $this->getBrowser()->getContainer()->set(UserHelper::class, $userHelper);

        $this->loginUser((new User())->setEmail('test@example.com')->setRoles(['ROLE_USER']));

        $abstractCourse = (new AbstractCourse())->setSlug(AbstractCourse::SLUG_MARATHON_INVESTMENT)
            ->setName('Марафон Инвестиций')->setSettlementPeriod('1 month')->setCertSerial('');
        $this->em->persist($abstractCourse);

        $stream = (new CourseStream())->setAbstractCourse($abstractCourse)->setStream(1)->setName('МИ test');
        $this->em->persist($stream);

        $userGroup = (new UserGroup())->setCourseStream($stream)->setName('Участники МИ');
        $this->em->persist($userGroup);

        for ($i = 0; $i < 13; $i++) {
            $user = new User();
            $this->em->persist($user);

            $userGroupUser = (new UserGroupUser())->setUser($user)->setUserGroup($userGroup);
            $this->em->persist($userGroupUser);

            $conf = (new MarathonInvestmentConfiguration())->setUser($user)->setStream($stream)
                ->setPointA($i)->setProfit($i);
            $this->em->persist($conf);

            $niche = (new BusinessNiche())->setName('Ниша ' . $i)->setUser($user);
            $this->em->persist($niche);

            $comment = (new BusinessNicheComment())->setText('text ' . $i)->setUser($user)->setNiche($niche);
            $this->em->persist($comment);

            $this->members[] = $user;
        }
        $this->em->flush();

        array_walk($this->members, function (User $user) {
            $this->em->refresh($user);
        });
    }

    /**
     * @dataProvider requestDataProvider
     * @throws JsonException
     */
    public function testGetMembers($data): void
    {
        $this->getBrowser()->request('GET', '/api/v2/marathon-investment/user', $data, [], $this->headers);
        self::assertResponseIsSuccessful();

        self::assertJsonStringEqualsJsonString(
            json_encode($this->makefilterAndSortExpectedMembers($data), JSON_THROW_ON_ERROR),
            $this->getBrowser()->getResponse()->getContent()
        );
    }

    private function makefilterAndSortExpectedMembers($data): array
    {
        $members = array_map(function (User $u) {
            return $this->userToArray($u);
        }, $this->members);
        foreach ($data['sorts'] ?? [] as $field) {
            if (str_starts_with($field, '-')) {
                $sort = 'DESC';
                $field = substr($field, 1);
            } else {
                $sort = 'ASC';
            }
            if (in_array($field, ['totalAmount', 'totalPassed'])) {
                uasort($members, static function (array $a, array $b) use ($sort, $field) {
                    if ($sort === 'ASC') {
                        return $a[$field] <=> $b[$field];
                    }

                    return $b[$field] <=> $a[$field];
                });
            } elseif (in_array($field, ['pointA', 'profit'])) {
                uasort($members, function (array $a, array $b) use ($sort, $field) {
                    if ($sort === 'ASC') {
                        return $this->getFieldByPath($a, ['configurations', 0, $field])
                            <=> $this->getFieldByPath($b, ['configurations', 0, $field]);
                    }

                    return $this->getFieldByPath($b, ['configurations', 0, $field])
                        <=> $this->getFieldByPath($a, ['configurations', 0, $field]);
                });
            }
        }

        return array_slice($members, ($data['page'] - 1) * $data['pageSize'], $data['pageSize']);
    }

    private function getFieldByPath($data, $path)
    {
        if (count($path) === 1) {
            return $data[array_shift($path)];
        }

        return $this->getFieldByPath($data[array_shift($path)], $path);
    }

    private function userToArray(User $user): array
    {
        return [
            'businessNiches' =>
                array_map(static function (BusinessNiche $niche) {
                    return [
                        'description' => $niche->getDescription(),
                        'hasRivals' => $niche->getHasRivals(),
                        'id' => $niche->getId(),
                        'main' => $niche->isMain(),
                        'name' => $niche->getName(),
                        'verified' => $niche->getVerified(),
                    ];
                }, $user->getBusinessNiches()->toArray()),
            'city' => $user->getCity(),
            'configurations' => array_map(static function (MarathonInvestmentConfiguration $conf) {
                return [
                    'pointA' => $conf->getPointA(),
                    'profit' => $conf->getProfit(),
                ];
            }, $user->getMarathonConfigurations()->toArray()),
            'id' => $user->getId(),
            'lastname' => $user->getLastname(),
            'name' => $user->getName(),
            'patronymic' => $user->getPatronymic(),
            'totalAmount' => $user->getTotalAmount(),
            'totalPassed' => $user->getTotalPassed(),
            'totalPlanned' => $user->getTotalPlanned(),
        ];
    }

    public function testGetMembersCount(): void
    {
        $this->getBrowser()->request('GET', '/api/v2/marathon-investment/user/count', [], [], $this->headers);
        self::assertResponseIsSuccessful();
        self::assertEquals(count($this->members), $this->getBrowser()->getResponse()->getContent());
    }

    public function requestDataProvider(): array
    {
        return [
            [['page' => 1, 'pageSize' => 10]],
            [['page' => 1, 'pageSize' => 1]],
            [['page' => 1, 'pageSize' => 3, 'sorts' => ['-pointA']]],
            [['page' => 1, 'pageSize' => 4, 'sorts' => ['-totalAmount', 'totalPassed']]],
        ];
    }

    protected function tearDown(): void
    {
        $this->em->rollback();
    }

}

<?php

namespace App\Tests\IntegrationTest\Support;

use App\Entity\User;
use App\Service\AuthServerHelper;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

//todo remove or replace to native client->loginUser since synfony 5.1
abstract class WebWithLoginTestCase extends WebTestCase
{
    /** @var array */
    protected $headers = [
        'HTTP_Authorization' => '',
        'HTTP_CONTENT_TYPE' => 'application/json',
        'HTTP_ACCEPT' => 'application/json',
    ];

    /** @var KernelBrowser */
    private $browserClient;

    protected function getBrowser(): KernelBrowser
    {
        if ($this->browserClient === null) {
            self::ensureKernelShutdown();
            $this->browserClient = self::createClient();
        }

        return $this->browserClient;
    }

    protected function loginUser(User $user): void
    {
        $authMock = $this->getMockBuilder(AuthServerHelper::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getUserByAccessToken'])
            ->getMock();
        $authMock->method('getUserByAccessToken')->willReturn($user);
        $this->getBrowser()->getContainer()->set(AuthServerHelper::class, $authMock);

        $token = new UsernamePasswordToken($user, 'test', 'main', $user->getRoles());
        $this->headers['HTTP_Authorization'] = 'Bearer ' . serialize($token);
    }

}

<?php

namespace App\Tests\IntegrationTest;

use App\Tests\IntegrationTest\Support\WebWithLoginTestCase;

class SwaggerTest extends WebWithLoginTestCase
{
    private const AUTH_HEADERS = [
        'HTTP_php-auth-user' => 'likecoder',
        'HTTP_php-auth-pw' => 'LiKe2020',
    ];

    public function testDoc(): void
    {
        $this->getBrowser()->request('GET', 'api/doc', [], [], self::AUTH_HEADERS);
        self::assertResponseIsSuccessful('swagger doc is down');
    }

    public function testDocJson(): void
    {
        $this->getBrowser()->request('GET', 'api/doc', [], [], self::AUTH_HEADERS);
        self::assertResponseIsSuccessful('swagger json is down');
    }


}

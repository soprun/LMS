<?php

namespace App\Tests\UnitTest\Service\Traction;

use App\DTO\Traction\UserTractionForSettlementPeriod;
use App\Entity\AbstractCourse;
use App\Entity\CourseStream;
use App\Entity\Traction\Value;
use App\Entity\User;
use App\Entity\UserGroup;
use App\Entity\UserGroupUser;
use App\Repository\CourseStreamRepository;
use App\Repository\Traction\ValueRepository;
use App\Service\Traction\TractionService;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;

class TractionServiceTest extends TestCase
{
    /** @dataProvider dataDiapasoneProvider */
    public function testMakeByUserDiapasone($from, $to, $streamsInDiapasone, $expectedPeriods): void
    {
        $valueRepo = $this->createMock(ValueRepository::class);
        $valueRepo->method('getUserValuesByDate')->willReturn([]);

        $streamRepo = $this->createMock(CourseStreamRepository::class);
        $streamRepo->method('getUserStreamsByDates')->willReturn($streamsInDiapasone);

        $service = new TractionService($streamRepo, $valueRepo);
        $traction = $service->makeByUser($this->createMock(User::class), $from, $to);
        self::assertEquals(
            $expectedPeriods,
            array_map(static function (UserTractionForSettlementPeriod $p) {
                return $p->getPeriod();
            }, $traction)
        );
    }

    public function testKeyDatesAndValues(): void
    {
        $keyDate = new DateTime('2021-04-01');

        $values = [(new Value())->setValue(123)];
        $valueRepo = $this->createMock(ValueRepository::class);
        $valueRepo->method('getUserValuesByDate')->willReturnCallback(function ($u, $date) use ($values, $keyDate) {
            return $date == $keyDate ? $values : [];
        });

        $streamRepo = $this->createMock(CourseStreamRepository::class);
        $streamRepo->method('getUserStreamsByDates')->willReturn([
            (new CourseStream())
                ->setStartDate(new DateTime('2021-01-15'))
                ->setPeriod(4)
                ->setAbstractCourse((new AbstractCourse())->setSettlementPeriod('1 month')),
        ]);

        $service = new TractionService($streamRepo, $valueRepo);
        $traction = $service->makeByUser(
            $this->createMock(User::class),
            new DateTime('2021-04'),
            new DateTime('2021-04')
        );
        self::assertCount(1, $traction);
        self::assertEquals($keyDate, $traction[0]->getKeyDate(), 'dates KEY not equals');
        self::assertEquals(new DateTime('2021-03-29'), $traction[0]->getFrom(), 'dates TO not equals');
        self::assertEquals(new DateTime('2021-05-02'), $traction[0]->getTo(), 'dates TO not equals');
        self::assertEquals($values, $traction[0]->getValues());
    }

    public function testMakeByUserWithUserJoinDate(): void
    {
        $valueRepo = $this->createMock(ValueRepository::class);
        $valueRepo->method('getUserValuesByDate')->willReturn([]);

        $stream = $this->createMock(CourseStream::class);
        $stream->method('getStartDate')->willReturn(null);
        $stream->method('getUserJoinStartDate')->willReturn(new DateTime('2021-02-01'));
        $stream->method('getUserJoinEndDate')->willReturn(new DateTime('2021-02-07'));
        $stream->method('getPeriod')->willReturn(1);
        $stream->method('getId')->willReturn(234);
        $stream->method('getAbstractCourse')->willReturn((new AbstractCourse())->setSettlementPeriod('1 month'));

        $streamRepo = $this->createMock(CourseStreamRepository::class);
        $streamRepo->method('getUserStreamsByDates')->willReturn([$stream]);

        $userGroupMock = $this->createMock(UserGroup::class);
        $userGroupMock->method('getCourseStreamId')->willReturn($stream->getId());

        $userMock = $this->createMock(User::class);
        $userMock->method('getId')->willReturn(123);
        $userMock->method('getGroupRelations')->willReturn(new ArrayCollection([new UserGroupUser($userMock)]));


        $service = new TractionService($streamRepo, $valueRepo);
        $traction = $service->makeByUser($userMock, new DateTime('2021-01'), new DateTime('2021-03'));
        self::assertEmpty($traction[0]->getStreams());
        self::assertNotEmpty($traction[1]->getStreams());
        self::assertEquals($stream, $traction[1]->getStreams()[0]);
    }

    public function dataDiapasoneProvider(): array
    {
        return [
            'days in one month' => [
                new DateTime('2021-01'),
                new DateTime('2021-01'),
                [
                    (new CourseStream())
                        ->setStartDate(new DateTime('2021-01-11'))
                        ->setPeriod(2)
                        ->setAbstractCourse((new AbstractCourse())->setSettlementPeriod('1 day')),
                ],
                [
                    '04.01 - 10.01',
                    '11.01',
                    '12.01',
                    '13.01',
                    '14.01',
                    '15.01',
                    '16.01',
                    '17.01',
                    '18.01',
                    '19.01',
                    '20.01',
                    '21.01',
                    '22.01',
                    '23.01',
                    '24.01',
                    '25.01 - 31.01',
                ],
            ],
            'days intersect month' => [
                new DateTime('2020-12'),
                new DateTime('2021-04'),
                [
                    (new CourseStream())
                        ->setStartDate(new DateTime('2021-01-18'))
                        ->setPeriod(4)
                        ->setAbstractCourse((new AbstractCourse())->setSettlementPeriod('1 day')),
                ],
                [
                    'Декабрь',
                    '04.01 - 10.01',
                    '11.01 - 17.01',
                    '18.01',
                    '19.01',
                    '20.01',
                    '21.01',
                    '22.01',
                    '23.01',
                    '24.01',
                    '25.01',
                    '26.01',
                    '27.01',
                    '28.01',
                    '29.01',
                    '30.01',
                    '31.01',
                    '01.02',
                    '02.02',
                    '03.02',
                    '04.02',
                    '05.02',
                    '06.02',
                    '07.02',
                    '08.02',
                    '09.02',
                    '10.02',
                    '11.02',
                    '12.02',
                    '13.02',
                    '14.02',
                    '15.02 - 21.02',
                    '22.02 - 28.02',
                    'Март',
                    'Апрель',
                ],
            ],
            'days in some months' => [
                new DateTime('2020-12'),
                new DateTime('2021-04'),
                [
                    (new CourseStream())
                        ->setStartDate(new DateTime('2021-01-25'))
                        ->setPeriod(2)
                        ->setAbstractCourse((new AbstractCourse())->setSettlementPeriod('1 day')),
                    (new CourseStream())
                        ->setStartDate(new DateTime('2021-02-15'))
                        ->setPeriod(3)
                        ->setAbstractCourse((new AbstractCourse())->setSettlementPeriod('1 day')),
                ],
                [
                    'Декабрь',
                    '04.01 - 10.01',
                    '11.01 - 17.01',
                    '18.01 - 24.01',
                    '25.01',
                    '26.01',
                    '27.01',
                    '28.01',
                    '29.01',
                    '30.01',
                    '31.01',
                    '01.02',
                    '02.02',
                    '03.02',
                    '04.02',
                    '05.02',
                    '06.02',
                    '07.02',
                    '08.02 - 14.02',
                    '15.02',
                    '16.02',
                    '17.02',
                    '18.02',
                    '19.02',
                    '20.02',
                    '21.02',
                    '22.02',
                    '23.02',
                    '24.02',
                    '25.02',
                    '26.02',
                    '27.02',
                    '28.02',
                    '01.03',
                    '02.03',
                    '03.03',
                    '04.03',
                    '05.03',
                    '06.03',
                    '07.03',
                    '08.03 - 14.03',
                    '15.03 - 21.03',
                    '22.03 - 28.03',
                    'Апрель',
                ],
            ],
            'weeks' => [
                new DateTime('2021-01'),
                new DateTime('2021-04'),
                [
                    (new CourseStream())
                        ->setStartDate(new DateTime('2021-01-18'))
                        ->setPeriod(4)
                        ->setAbstractCourse((new AbstractCourse())->setSettlementPeriod('1 week')),
                ],
                [
                    '04.01 - 10.01',
                    '11.01 - 17.01',
                    '18.01 - 24.01',
                    '25.01 - 31.01',
                    '01.02 - 07.02',
                    '08.02 - 14.02',
                    '15.02 - 21.02',
                    '22.02 - 28.02',
                    'Март',
                    'Апрель',
                ],
            ],
            'months' => [
                new DateTime('2020-12'),
                new DateTime('2021-03'),
                [
                    (new CourseStream())
                        ->setStartDate(new DateTime('2021-01-15'))
                        ->setPeriod(4)
                        ->setAbstractCourse((new AbstractCourse())->setSettlementPeriod('1 month')),
                ],
                [
                    'Декабрь',
                    'Январь',
                    'Февраль',
                    'Март',
                ],
            ],
            'without streams' => [
                new DateTime('2021-02'),
                new DateTime('2021-03'),
                [],
                ['Февраль', 'Март',],
            ],
        ];
    }

}

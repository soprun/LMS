<?php

namespace App\Tests\UnitTest;

use App\Entity\AbstractCourse;
use App\Entity\CourseStream;
use App\Entity\User;
use App\Entity\UserCertificates;
use App\Repository\UserCertificatesRepository;
use App\Repository\UserRepository;
use App\Service\Course\CertificateService;
use App\Service\FormHelper;
use App\Service\UserHelper;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use ImagickDrawException;
use ImagickException;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpKernel\KernelInterface;

class CertificateServiceTest extends TestCase
{
    private $em;
    private $userHelper;
    private $certificateService;
    private $kernelInterface;

    protected function setUp(): void
    {
        $em = $this->em = $this->createMock(EntityManagerInterface::class);
        $formHelper = $this->createMock(FormHelper::class);
        $kernelInterface = $this->kernelInterface = $this->createMock(KernelInterface::class);
        $userHelper = $this->userHelper = $this->createMock(UserHelper::class);
        $this->certificateService = new CertificateService($em, $kernelInterface, $formHelper, $userHelper);
    }

    protected function tearDown(): void
    {
        $this->em->rollback();
    }

    public function testItCreatesCertificateByStream(): void
    {
        $user = new User();
        $user->setAuthUserId(1);

        $this->userHelper->method('getUserInfoArrayByIds')
            ->with([$user->getAuthUserId()])
            ->willReturn([['email' => 'test@likebz.ru']]);

        $abstractCourse = new AbstractCourse();
        $abstractCourse->setName('МСА-LITE');
        $abstractCourse->setSlug('msa_lite');
        $abstractCourse->setSettlementPeriod('1 week');
        $abstractCourse->setCertSerial('№ Л-');

        $courseStream = new CourseStream();
        $courseStream->setAbstractCourse($abstractCourse);
        $courseStream->setStream(1);
        $courseStream->setName('Скорость МСА Лайт (бизнес)');
        $courseStream->setStartDate(new \DateTime('2021-12-11'));
        $courseStream->setPeriod(6);

        $this->em->expects($this->exactly(2))->method('getRepository')
            ->willReturnCallback(
                function ($class) use ($user) {
                    if ($class === User::class) {
                        $userRepository = $this->createMock(UserRepository::class);
                        $userRepository->method('find')
                            ->with(1)
                            ->willReturn($user);

                        return $userRepository;
                    }

                    if ($class === UserCertificates::class) {
                        $userCertificateRepository = $this->createMock(UserCertificatesRepository::class);
                        $userCertificateRepository->method('findOneBy')
                            ->willReturn(null);

                        return $userCertificateRepository;
                    }

                    return null;
                }
            );

        $certificate = $this->certificateService->newCertificateByStream(1, $courseStream);

        $this->assertArrayHasKey('serialNumber', $certificate);
        $this->assertSame('test@likebz.ru', $certificate['email']);
        $this->assertFalse($certificate['isSend']);
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function testItGetsCertificateDataByStream(): void
    {
        $user = new User();
        $user->setAuthUserId(1);

        $abstractCourse = new AbstractCourse();
        $abstractCourse->setName('МСА-LITE');
        $abstractCourse->setSlug('msa_lite');
        $abstractCourse->setSettlementPeriod('1 week');
        $abstractCourse->setCertSerial('№ Л-');

        $courseStream = new CourseStream();
        $courseStream->setAbstractCourse($abstractCourse);
        $courseStream->setStream(1);
        $courseStream->setName('Скорость МСА Лайт (бизнес)');
        $courseStream->setStartDate(new \DateTime('2021-12-11'));
        $courseStream->setPeriod(6);

        $userCertificate = new UserCertificates();
        $userCertificate->setUser($user);
        $userCertificate->setCourseStream($courseStream);
        $userCertificate->setSerial('5-3689208');
        $userCertificate->setSerialNumber('IIO43-WXE29-87UVC-01YYZ');
        $userCertificateRepository = $this->createMock(UserCertificatesRepository::class);

        $queryBuilder = $this->createMock(QueryBuilder::class);


        $userCertificateRepository->method('createQueryBuilder')
            ->willReturn($queryBuilder);

        $queryBuilder->method('innerJoin')
            ->willReturnSelf();
        $queryBuilder->method('select')
            ->willReturnSelf();
        $queryBuilder->method('where')
            ->willReturnSelf();
        $queryBuilder->method('orWhere')
            ->willReturnSelf();
        $queryBuilder->method('setParameter')
            ->willReturnSelf();


        $query = $this->createMock(AbstractQuery::class);
        $queryBuilder->method('getQuery')
            ->willReturn($query);
        $query->method('getOneOrNullResult')
            ->willReturn($userCertificate);


        $this->em->method('getRepository')
            ->willReturnCallback(
                function () use ($userCertificateRepository) {
                    return $userCertificateRepository;
                }
            );

        $this->userHelper->method('getUserInfoArrayByIds')
            ->willReturn(
                [
                    [
                        'lastname' => 'lastname',
                        'name' => 'name',
                        'patronymic' => 'patronymic',
                        'email' => 'email@email.ru',
                    ],
                ]
            );

        $endDate = (clone $courseStream->getStartDate())->modify($courseStream->getPeriod() . 'week')->format('d.m.Y');
        $certificateData = $this->certificateService->getCertificate($userCertificate->getSerialNumber());

        $this->assertSame(
            $userCertificate->getCourseStream()->getAbstractCourse()->getCertSerial() . $userCertificate->getSerial(
            ) . ' от ' . $endDate,
            $certificateData['serialString']
        );

        $this->assertSame('lastname name patronymic', $certificateData['studentNameString']);

        $this->assertSame('lastname name patronymic', $certificateData['studentNameString']);

        $this->assertSame(
            'с ' . $courseStream->getStartDate()->format('d.m.Y') . ' г. по ' . $endDate . ' г. прошел/прошла обучение',
            $certificateData['dateString']
        );

        $this->assertSame(
            $this->kernelInterface->getProjectDir() . '/public/lms/img/cert_' . $courseStream->getCourseSlug() . '.jpg',
            $certificateData['path']
        );

        $this->assertSame('email@email.ru', $certificateData['email']);

        $this->assertSame(
            'render' . ucfirst(
                str_replace(' ', '', ucwords(str_replace('_', ' ', $courseStream->getCourseSlug())))
            ) . 'Cert',
            $certificateData['imageHandler']
        );
    }

    /**
     * @throws ImagickException
     * @throws ImagickDrawException
     */
    public function testItCreatesCertificateImage(): void
    {
        $dataCertificate = [
            'path' => __DIR__ . '/TestData/public/lms/img/cert_msa.jpg',
            'serialString' => 'serialString',
            'studentNameString' => 'studentNameString',
            'dateString' => 'dateString',
            'imageHandler' => 'renderMsaCert',
        ];

        $this->kernelInterface->method('getProjectDir')
            ->willReturn(__DIR__ . '/TestData');

        $image = $this->certificateService->createCertificateImg($dataCertificate);
        $this->assertInstanceOf(\Imagick::class, $image);
    }


}

import Vue from 'vue';
import '../public/lms/css/style.css'

import Icon from '../assets/vue/Lms/components/Icon'
import SlideUpDown from 'vue-slide-up-down'
import vClickOutside from 'v-click-outside'

Vue.component('Icon', Icon)
Vue.component('SlideUpDown', SlideUpDown)
Vue.component('vClickOutside', vClickOutside)


//ui-kit

import UiInput from '../assets/vue/Lms/components/UiKit/Input'
import UiButton from '../assets/vue/Lms/components/UiKit/Button'
import UiPopup from '../assets/vue/Lms/components/UiKit/Popup'
import UiCheckbox from '../assets/vue/Lms/components/UiKit/Checkbox'
import UiRadio from '../assets/vue/Lms/components/UiKit/Radio'
import UiSwitcher from '../assets/vue/Lms/components/UiKit/Switcher'
import UiSelect from '../assets/vue/Lms/components/UiKit/Select'
import UiDropdown from '../assets/vue/Lms/components/UiKit/Dropdown'
import UiModal from '../assets/vue/Lms/components/UiKit/Modal'
import UiCustomDropdown from '../assets/vue/Lms/components/UiKit/CustomDropdown'
import UiNestedFilter from '../assets/vue/Lms/components/UiKit/NestedFilter'
import UiSelectFilter from '../assets/vue/Lms/components/UiKit/SelectFilter'
import UiLoaderDots from "../assets/vue/Lms/components/UiKit/LoaderDots/index";

Vue.component('UiInput', UiInput)
Vue.component('UiButton', UiButton)
Vue.component('UiPopup', UiPopup)
Vue.component('UiCheckbox', UiCheckbox)
Vue.component('UiRadio', UiRadio)
Vue.component('UiSwitcher', UiSwitcher)
Vue.component('UiSelect', UiSelect)
Vue.component('UiDropdown', UiDropdown)
Vue.component('UiModal', UiModal)
Vue.component('UiCustomDropdown', UiCustomDropdown)
Vue.component('UiNestedFilter', UiNestedFilter)
Vue.component('UiSelectFilter', UiSelectFilter)
Vue.component('UiLoaderDots', UiLoaderDots)

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  layout: 'centered'
}

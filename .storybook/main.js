const path = require('path');

module.exports = {
  "stories": [
    "../assets/vue/Lms/**/*.stories.mdx",
    "../assets/vue/Lms/**/*.stories.@(js|jsx|ts|tsx)",
  ],
  "addons": [
    "@storybook/addon-links",
    "@storybook/addon-essentials"
  ],
  webpackFinal: (config, { configType }) => {
    // `configType` has a value of 'DEVELOPMENT' or 'PRODUCTION'
    // You can change the configuration based on that.
    // 'PRODUCTION' is used when building the static version of storybook.

    // Make whatever fine-grained changes you need

    // config.resolve.alias = Object.assign(config.resolve.alias, {
    //   '@lms': path.resolve('../assets', 'vue/Lms'),
    // })

    config.resolve.alias['@lms/'] = path.resolve('../assets', 'vue/Lms');

    config.module.rules.push({
      test: /\.pug$/,
      use: [{
        loader: 'pug-plain-loader'
      }],
    });

    // Return the altered config
    return config
  },
}
FROM php:7.3-fpm as soft
WORKDIR /app

# region xdebug
RUN pecl install xdebug-3.1.0 \
    && rm -rf /tmp/pear \
    && docker-php-ext-enable xdebug
#COPY /docker-php-ext-xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "expose_php=Off" >> /usr/local/etc/php/php.ini \
    && echo "xdebug.log=/app/var/log/xdebug.log" >> /usr/local/etc/php/php.ini \
    && echo "xdebug.log_level=3" >> /usr/local/etc/php/php.ini \
    && echo "xdebug.client_host=host.docker.internal" >> /usr/local/etc/php/php.ini \
    && echo "xdebug.client_port=9003" >> /usr/local/etc/php/php.ini \
    && echo "xdebug.idekey=likecentre" >> /usr/local/etc/php/php.ini
# endregion xdebug

RUN apt-get update \
    && apt-get install -y \
    git \
    libpng-dev \
    librabbitmq-dev \
    libmagickwand-dev

RUN docker-php-ext-install \
    intl \
    pdo_mysql \
    exif \
    gd \
    bcmath \
    sockets \
    && pecl install imagick \
    && pecl install amqp \
    && rm -rf /tmp/pear \
    && docker-php-ext-enable amqp \
    && docker-php-ext-enable imagick

RUN pecl install -o -f redis \
    && rm -rf /tmp/pear \
    && docker-php-ext-enable redis

RUN mkdir -p -m 777 var

#########################

FROM soft as builder
RUN apt update && apt install -y git unzip zip libzip-dev && docker-php-ext-install zip
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
RUN echo "memory_limit=4G" >> /usr/local/etc/php/php.ini

################

FROM builder as source
COPY . .
RUN composer install -o

################

FROM soft as backend
COPY --from=source /app .
RUN rm -rf /app/var/cache/*
RUN mkdir -p var/log/ var/cache/
RUN chown -R www-data:www-data var/log/ var/cache/

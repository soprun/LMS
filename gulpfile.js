var gulp          = require('gulp'),
  browserSync     = require('browser-sync'),
  gulpLoadPlugins = require('gulp-load-plugins'),
  plugins         = gulpLoadPlugins();

var paths = {
  sass : {
    src : 'public/lms/sass/**/*.sass',
    dist : 'public/lms/css'
  },
  svg : {
    src : 'public/lms/img/icons/**/*.svg',
    dist : 'public/lms/img'
  },
}

// Custom Styles
gulp.task('styles', function(){
  return gulp.src(paths.sass.src, { sourcemaps: true })
    .pipe(plugins.wait2(500))
    .pipe(plugins.sassGlob())
    .pipe(plugins.sass().on('error', plugins.notify.onError({
      message: "<%= error.message %>",
      title: 'Style'
    })))
    .pipe(plugins.autoprefixer({
      grid: true,
      overrideBroserslist: ['last 15 versions', '> 0.1%', 'ie 8']
    }, {cascade: true}))
    .pipe(plugins.rename('style.css'))
    .pipe(gulp.dest(paths.sass.dist, { sourcemaps: '.' }))
    .pipe(browserSync.stream())
});

gulp.task('watch', function() {
  gulp.watch(paths.sass.src, gulp.parallel('styles'));
});

gulp.task('default', gulp.parallel('styles', 'watch'));